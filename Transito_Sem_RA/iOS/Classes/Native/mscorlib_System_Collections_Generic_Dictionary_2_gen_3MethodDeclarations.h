﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor()
#define Dictionary_2__ctor_m2087(__this, method) (( void (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2__ctor_m10913_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2__ctor_m13442(__this, ___comparer, method) (( void (*) (Dictionary_2_t193 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m10915_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Int32)
#define Dictionary_2__ctor_m13443(__this, ___capacity, method) (( void (*) (Dictionary_2_t193 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m10917_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2__ctor_m13444(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t193 *, SerializationInfo_t652 *, StreamingContext_t653 , const MethodInfo*))Dictionary_2__ctor_m10919_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionary.get_Item(System.Object)
#define Dictionary_2_System_Collections_IDictionary_get_Item_m13445(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t193 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m10921_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_set_Item_m13446(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t193 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m10923_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionary.Add(System.Object,System.Object)
#define Dictionary_2_System_Collections_IDictionary_Add_m13447(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t193 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m10925_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionary.Contains(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Contains_m13448(__this, ___key, method) (( bool (*) (Dictionary_2_t193 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m10927_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionary.Remove(System.Object)
#define Dictionary_2_System_Collections_IDictionary_Remove_m13449(__this, ___key, method) (( void (*) (Dictionary_2_t193 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m10929_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.ICollection.get_IsSynchronized()
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m13450(__this, method) (( bool (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m10931_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.ICollection.get_SyncRoot()
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m13451(__this, method) (( Object_t * (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m10933_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m13452(__this, method) (( bool (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m10935_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m13453(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t193 *, KeyValuePair_2_t2013 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m10937_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m13454(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t193 *, KeyValuePair_2_t2013 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m10939_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m13455(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t193 *, KeyValuePair_2U5BU5D_t2456*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m10941_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m13456(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t193 *, KeyValuePair_2_t2013 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m10943_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Dictionary_2_System_Collections_ICollection_CopyTo_m13457(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t193 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m10945_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerable.GetEnumerator()
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m13458(__this, method) (( Object_t * (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m10947_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m13459(__this, method) (( Object_t* (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m10949_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionary.GetEnumerator()
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m13460(__this, method) (( Object_t * (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m10951_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Count()
#define Dictionary_2_get_Count_m13461(__this, method) (( int32_t (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_get_Count_m10953_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Item(TKey)
#define Dictionary_2_get_Item_m13462(__this, ___key, method) (( List_1_t378 * (*) (Dictionary_2_t193 *, Font_t191 *, const MethodInfo*))Dictionary_2_get_Item_m10955_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::set_Item(TKey,TValue)
#define Dictionary_2_set_Item_m13463(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t193 *, Font_t191 *, List_1_t378 *, const MethodInfo*))Dictionary_2_set_Item_m10957_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
#define Dictionary_2_Init_m13464(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t193 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m10959_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::InitArrays(System.Int32)
#define Dictionary_2_InitArrays_m13465(__this, ___size, method) (( void (*) (Dictionary_2_t193 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m10961_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::CopyToCheck(System.Array,System.Int32)
#define Dictionary_2_CopyToCheck_m13466(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t193 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m10963_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::make_pair(TKey,TValue)
#define Dictionary_2_make_pair_m13467(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t2013  (*) (Object_t * /* static, unused */, Font_t191 *, List_1_t378 *, const MethodInfo*))Dictionary_2_make_pair_m10965_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::pick_value(TKey,TValue)
#define Dictionary_2_pick_value_m13468(__this /* static, unused */, ___key, ___value, method) (( List_1_t378 * (*) (Object_t * /* static, unused */, Font_t191 *, List_1_t378 *, const MethodInfo*))Dictionary_2_pick_value_m10967_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
#define Dictionary_2_CopyTo_m13469(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t193 *, KeyValuePair_2U5BU5D_t2456*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m10969_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Resize()
#define Dictionary_2_Resize_m13470(__this, method) (( void (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_Resize_m10971_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Add(TKey,TValue)
#define Dictionary_2_Add_m13471(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t193 *, Font_t191 *, List_1_t378 *, const MethodInfo*))Dictionary_2_Add_m10973_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Clear()
#define Dictionary_2_Clear_m13472(__this, method) (( void (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_Clear_m10975_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ContainsKey(TKey)
#define Dictionary_2_ContainsKey_m13473(__this, ___key, method) (( bool (*) (Dictionary_2_t193 *, Font_t191 *, const MethodInfo*))Dictionary_2_ContainsKey_m10977_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ContainsValue(TValue)
#define Dictionary_2_ContainsValue_m13474(__this, ___value, method) (( bool (*) (Dictionary_2_t193 *, List_1_t378 *, const MethodInfo*))Dictionary_2_ContainsValue_m10979_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
#define Dictionary_2_GetObjectData_m13475(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t193 *, SerializationInfo_t652 *, StreamingContext_t653 , const MethodInfo*))Dictionary_2_GetObjectData_m10981_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::OnDeserialization(System.Object)
#define Dictionary_2_OnDeserialization_m13476(__this, ___sender, method) (( void (*) (Dictionary_2_t193 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m10983_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Remove(TKey)
#define Dictionary_2_Remove_m13477(__this, ___key, method) (( bool (*) (Dictionary_2_t193 *, Font_t191 *, const MethodInfo*))Dictionary_2_Remove_m10985_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::TryGetValue(TKey,TValue&)
#define Dictionary_2_TryGetValue_m13478(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t193 *, Font_t191 *, List_1_t378 **, const MethodInfo*))Dictionary_2_TryGetValue_m10987_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Values()
#define Dictionary_2_get_Values_m13479(__this, method) (( ValueCollection_t2014 * (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_get_Values_m10989_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToTKey(System.Object)
#define Dictionary_2_ToTKey_m13480(__this, ___key, method) (( Font_t191 * (*) (Dictionary_2_t193 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m10991_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ToTValue(System.Object)
#define Dictionary_2_ToTValue_m13481(__this, ___value, method) (( List_1_t378 * (*) (Dictionary_2_t193 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m10993_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
#define Dictionary_2_ContainsKeyValuePair_m13482(__this, ___pair, method) (( bool (*) (Dictionary_2_t193 *, KeyValuePair_2_t2013 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m10995_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::GetEnumerator()
#define Dictionary_2_GetEnumerator_m13483(__this, method) (( Enumerator_t2015  (*) (Dictionary_2_t193 *, const MethodInfo*))Dictionary_2_GetEnumerator_m10997_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::<CopyTo>m__0(TKey,TValue)
#define Dictionary_2_U3CCopyToU3Em__0_m13484(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1057  (*) (Object_t * /* static, unused */, Font_t191 *, List_1_t378 *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m10999_gshared)(__this /* static, unused */, ___key, ___value, method)
