﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


// System.Object
struct Object_t;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t57;
// UnityEngine.UI.ILayoutElement
struct ILayoutElement_t355;
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t450;
// System.String
struct String_t;
// System.Byte[]
struct ByteU5BU5D_t698;
// System.Collections.Specialized.ListDictionary/DictionaryNode
struct DictionaryNode_t910;
// System.Net.IPAddress
struct IPAddress_t938;
// System.Net.IPv6Address
struct IPv6Address_t940;
// System.UriFormatException
struct UriFormatException_t1048;
// System.Object[]
struct ObjectU5BU5D_t77;
// System.Exception
struct Exception_t68;
// System.MulticastDelegate
struct MulticastDelegate_t227;
// Mono.Globalization.Unicode.Contraction[]
struct ContractionU5BU5D_t1131;
// Mono.Globalization.Unicode.Level2Map[]
struct Level2MapU5BU5D_t1132;
// Mono.Globalization.Unicode.CodePointIndexer
struct CodePointIndexer_t1115;
// Mono.Globalization.Unicode.Contraction
struct Contraction_t1118;
// System.Reflection.MethodBase
struct MethodBase_t679;
// System.Reflection.Module
struct Module_t1285;
// System.Runtime.Serialization.ISurrogateSelector
struct ISurrogateSelector_t1449;
// System.Runtime.Remoting.Messaging.Header[]
struct HeaderU5BU5D_t1712;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t652;
// System.Text.StringBuilder
struct StringBuilder_t356;
// System.Text.EncoderFallbackBuffer
struct EncoderFallbackBuffer_t1603;
// System.Char[]
struct CharU5BU5D_t238;
// System.Text.DecoderFallbackBuffer
struct DecoderFallbackBuffer_t1594;
// System.Int64[]
struct Int64U5BU5D_t1740;
// System.String[]
struct StringU5BU5D_t594;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1912;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t234;
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t217;
// System.Int32[]
struct Int32U5BU5D_t426;
// UnityEngine.Color32[]
struct Color32U5BU5D_t424;
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t216;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t425;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t647;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t648;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1766;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1767;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Object.h"
#include "mscorlib_System_Void.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Boolean.h"
#include "mscorlib_System_Single.h"
#include "mscorlib_System_SByte.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_MoveDirection.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Inp.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData_Fra.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul_0.h"
#include "UnityEngine_UnityEngine_LayerMask.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"
#include "UnityEngine_UI_UnityEngine_UI_ColorBlock.h"
#include "UnityEngine_UI_UnityEngine_UI_DefaultControls_Resources.h"
#include "UnityEngine_UnityEngine_FontStyle.h"
#include "UnityEngine_UnityEngine_TextAnchor.h"
#include "UnityEngine_UnityEngine_HorizontalWrapMode.h"
#include "UnityEngine_UnityEngine_VerticalWrapMode.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicRaycaster_BlockingObjec.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_Type.h"
#include "UnityEngine_UI_UnityEngine_UI_Image_FillMethod.h"
#include "UnityEngine_UnityEngine_Vector4.h"
#include "UnityEngine_UnityEngine_Color32.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_ContentType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_LineType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_InputType.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboardType.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_CharacterValidation.h"
#include "mscorlib_System_Char.h"
#include "mscorlib_System_Int16.h"
#include "UnityEngine_UI_UnityEngine_UI_InputField_EditState.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation_Mode.h"
#include "UnityEngine_UI_UnityEngine_UI_Navigation.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Scrollbar_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_MovementType.h"
#include "UnityEngine_UI_UnityEngine_UI_ScrollRect_ScrollbarVisibility.h"
#include "UnityEngine_UnityEngine_Bounds.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_Transition.h"
#include "UnityEngine_UI_UnityEngine_UI_SpriteState.h"
#include "UnityEngine_UI_UnityEngine_UI_Selectable_SelectionState.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Direction.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider_Axis.h"
#include "UnityEngine_UnityEngine_TextGenerationSettings.h"
#include "UnityEngine_UI_UnityEngine_UI_AspectRatioFitter_AspectMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScaleMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_ScreenMatchMode.h"
#include "UnityEngine_UI_UnityEngine_UI_CanvasScaler_Unit.h"
#include "UnityEngine_UI_UnityEngine_UI_ContentSizeFitter_FitMode.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Corner.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Axis.h"
#include "UnityEngine_UI_UnityEngine_UI_GridLayoutGroup_Constraint.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Double.h"
#include "mscorlib_System_Int64.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcUserPro.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcScoreDa.h"
#include "UnityEngine_UnityEngine_BoneWeight.h"
#include "UnityEngine_UnityEngine_InternalDrawTextureArguments.h"
#include "UnityEngine_UnityEngine_CullingGroupEvent.h"
#include "UnityEngine_UnityEngine_TouchScreenKeyboard_InternalConstruc.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Matrix4x4.h"
#include "UnityEngine_UnityEngine_Ray.h"
#include "UnityEngine_UnityEngine_Rendering_SphericalHarmonicsL2.h"
#include "UnityEngine_UnityEngine_RuntimePlatform.h"
#include "UnityEngine_UnityEngine_CameraClearFlags.h"
#include "UnityEngine_UnityEngine_RenderBuffer.h"
#include "UnityEngine_UnityEngine_TouchPhase.h"
#include "UnityEngine_UnityEngine_WheelHit.h"
#include "UnityEngine_UnityEngine_RaycastHit2D.h"
#include "UnityEngine_UnityEngine_SendMessageOptions.h"
#include "UnityEngine_UnityEngine_AnimatorStateInfo.h"
#include "UnityEngine_UnityEngine_AnimatorClipInfo.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"
#include "UnityEngine_UnityEngine_RenderMode.h"
#include "UnityEngine_UnityEngine_EventType.h"
#include "UnityEngine_UnityEngine_EventModifiers.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "mscorlib_System_DateTime.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserState.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_UserScope.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_Range.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_TimeScope.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "UnityEngine_UnityEngine_Events_PersistentListenerMode.h"
#include "mscorlib_System_UInt32.h"
#include "Mono_Security_Mono_Math_BigInteger_Sign.h"
#include "Mono_Security_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_System_Byte.h"
#include "mscorlib_ArrayTypes.h"
#include "mscorlib_System_Security_Cryptography_DSAParameters.h"
#include "mscorlib_System_Security_Cryptography_RSAParameters.h"
#include "Mono_Security_Mono_Security_X509_X509ChainStatusFlags.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertLevel.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_AlertDescription.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_CipherAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HashAlgorithmType.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ExchangeAlgorithmTy.h"
#include "mscorlib_System_Security_Cryptography_CipherMode.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityProtocolTyp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_SecurityCompression.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_Handshake.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_HandshakeState.h"
#include "mscorlib_System_UInt64.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_ContentType.h"
#include "System_System_Collections_Specialized_ListDictionary_Diction.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"
#include "System_System_Net_IPAddress.h"
#include "System_System_Net_Sockets_AddressFamily.h"
#include "System_System_Net_IPv6Address.h"
#include "mscorlib_System_UInt16.h"
#include "System_System_Net_SecurityProtocolType.h"
#include "System_System_Security_Cryptography_AsnDecodeStatus.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_1.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Rev_0.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Ver.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Key_0.h"
#include "System_System_Text_RegularExpressions_RegexOptions.h"
#include "System_System_Text_RegularExpressions_Category.h"
#include "System_System_Text_RegularExpressions_OpFlags.h"
#include "System_System_Text_RegularExpressions_Interval.h"
#include "System_System_Text_RegularExpressions_Position.h"
#include "System_System_UriHostNameType.h"
#include "System_System_UriFormatException.h"
#include "mscorlib_System_Decimal.h"
#include "mscorlib_System_Exception.h"
#include "mscorlib_System_TypeCode.h"
#include "mscorlib_System_Globalization_UnicodeCategory.h"
#include "mscorlib_System_UIntPtr.h"
#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Reflection_TypeAttributes.h"
#include "mscorlib_System_Reflection_MemberTypes.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "mscorlib_System_RuntimeFieldHandle.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_ExtenderT.h"
#include "mscorlib_Mono_Globalization_Unicode_SimpleCollator_Context.h"
#include "mscorlib_Mono_Globalization_Unicode_Contraction.h"
#include "mscorlib_Mono_Math_Prime_ConfidenceFactor.h"
#include "mscorlib_Mono_Math_BigInteger_Sign.h"
#include "mscorlib_System_Reflection_MethodBase.h"
#include "mscorlib_System_DayOfWeek.h"
#include "mscorlib_System_TimeSpan.h"
#include "mscorlib_System_IO_MonoIOError.h"
#include "mscorlib_System_IO_FileAttributes.h"
#include "mscorlib_System_IO_MonoFileType.h"
#include "mscorlib_System_IO_MonoIOStat.h"
#include "mscorlib_System_Reflection_CallingConventions.h"
#include "mscorlib_System_RuntimeMethodHandle.h"
#include "mscorlib_System_Reflection_MethodAttributes.h"
#include "mscorlib_System_Reflection_Emit_MethodToken.h"
#include "mscorlib_System_Reflection_FieldAttributes.h"
#include "mscorlib_System_Reflection_Emit_OpCode.h"
#include "mscorlib_System_Reflection_Emit_StackBehaviour.h"
#include "mscorlib_System_Reflection_Module.h"
#include "mscorlib_System_Reflection_AssemblyNameFlags.h"
#include "mscorlib_System_Reflection_EventAttributes.h"
#include "mscorlib_System_Reflection_MonoEventInfo.h"
#include "mscorlib_System_Reflection_MonoMethodInfo.h"
#include "mscorlib_System_Reflection_MonoPropertyInfo.h"
#include "mscorlib_System_Reflection_PropertyAttributes.h"
#include "mscorlib_System_Reflection_ParameterAttributes.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceInfo.h"
#include "mscorlib_System_Runtime_InteropServices_GCHandle.h"
#include "mscorlib_System_Runtime_Remoting_WellKnownObjectMode.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_TypeFilterL.h"
#include "mscorlib_System_Runtime_Serialization_SerializationInfo.h"
#include "mscorlib_System_Runtime_Serialization_SerializationEntry.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContextStates.h"
#include "mscorlib_System_Security_Cryptography_CspProviderFlags.h"
#include "mscorlib_System_Security_Cryptography_PaddingMode.h"
#include "mscorlib_System_Text_StringBuilder.h"
#include "mscorlib_System_Text_EncoderFallbackBuffer.h"
#include "mscorlib_System_Text_DecoderFallbackBuffer.h"
#include "mscorlib_System_DateTimeKind.h"
#include "mscorlib_System_DateTimeOffset.h"
#include "mscorlib_System_Nullable_1_gen.h"
#include "mscorlib_System_MonoEnumInfo.h"
#include "mscorlib_System_PlatformID.h"
#include "mscorlib_System_Guid.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_gen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_FloatTween.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
#include "mscorlib_System_Collections_Generic_Link.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
#include "UnityEngine.UI_ArrayTypes.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "UnityEngine_UnityEngine_SocialPlatforms_GameCenter_GcAchieve_0.h"
#include "UnityEngine_UnityEngine_ContactPoint.h"
#include "UnityEngine_UnityEngine_ContactPoint2D.h"
#include "UnityEngine_UnityEngine_Keyframe.h"
#include "mscorlib_System_Reflection_ParameterModifier.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"
#include "Mono_Security_Mono_Security_Protocol_Tls_Handshake_ClientCer.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"
#include "System_System_Text_RegularExpressions_Mark.h"
#include "System_System_Uri_UriScheme.h"
#include "mscorlib_Mono_Globalization_Unicode_CodePointIndexer_TableRa.h"
#include "mscorlib_System_Collections_Hashtable_Slot.h"
#include "mscorlib_System_Collections_SortedList_Slot.h"
#include "mscorlib_System_Reflection_Emit_ILTokenInfo.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelData.h"
#include "mscorlib_System_Reflection_Emit_ILGenerator_LabelFixup.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"
#include "mscorlib_System_Resources_ResourceReader_ResourceCacheItem.h"
#include "mscorlib_System_Runtime_Serialization_Formatters_Binary_Type.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_8.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_29.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_30.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_33.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__13.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_22.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_37.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"

void* RuntimeInvoker_Void_t1083 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, const MethodInfo* method);
	((Func)method->method)(obj, method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return ret;
}

void* RuntimeInvoker_Single_t358_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t358_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_RaycastResult_t139_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t139  p1, RaycastResult_t139  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), *((RaycastResult_t139 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_MoveDirection_t136 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t139  (*Func)(void* obj, const MethodInfo* method);
	RaycastResult_t139  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResult_t139  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_InputButton_t142 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastResult_t139_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t139  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RaycastResult_t139  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t136_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MoveDirection_t136_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_PointerEventDataU26_t2741_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, PointerEventData_t57 ** p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (PointerEventData_t57 **)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Touch_t72_BooleanU26_t2742_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Touch_t72  p1, bool* p2, bool* p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Touch_t72 *)args[0]), (bool*)args[1], (bool*)args[2], method);
	return ret;
}

void* RuntimeInvoker_FramePressState_t143_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360_Vector2_t23_Vector2_t23_Single_t358_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, float p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), *((float*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_InputMode_t152 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_LayerMask_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t155  (*Func)(void* obj, const MethodInfo* method);
	LayerMask_t155  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_LayerMask_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LayerMask_t155  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LayerMask_t155 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_RaycastHit_t100_RaycastHit_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t100  p1, RaycastHit_t100  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t100 *)args[0]), *((RaycastHit_t100 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t83 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t83  (*Func)(void* obj, const MethodInfo* method);
	Color_t83  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Color_t83 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t83 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_ColorTweenMode_t157 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ColorBlock_t174 (const MethodInfo* method, void* obj, void** args)
{
	typedef ColorBlock_t174  (*Func)(void* obj, const MethodInfo* method);
	ColorBlock_t174  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Resources_t175 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Resources_t175  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Resources_t175 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_FontStyle_t564 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextAnchor_t412 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HorizontalWrapMode_t531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_VerticalWrapMode_t532 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector2_t23_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t23  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t23_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t84  (*Func)(void* obj, const MethodInfo* method);
	Rect_t84  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Color_t83_Single_t358_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83  p1, float p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t83 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Color_t83_Single_t358_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83  p1, float p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t83 *)args[0]), *((float*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t83_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t83  (*Func)(void* obj, float p1, const MethodInfo* method);
	Color_t83  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Single_t358_Single_t358_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_BlockingObjects_t202 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Vector2_t23_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Vector2_t23  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Vector2_t23 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Type_t208 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FillMethod_t209 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Color32_t347_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Color32_t347  p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color32_t347 *)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Vector2_t23_Vector2_t23_Color32_t347_Vector2_t23_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, Vector2_t23  p3, Color32_t347  p4, Vector2_t23  p5, Vector2_t23  p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), *((Vector2_t23 *)args[2]), *((Color32_t347 *)args[3]), *((Vector2_t23 *)args[4]), *((Vector2_t23 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t317_Vector4_t317_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, Vector4_t317  p1, Rect_t84  p2, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Rect_t84 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Single_t358_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Single_t358_Single_t358_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, float p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), *((float*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t23_Vector2_t23_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Vector2_t23  p1, Rect_t84  p2, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Rect_t84 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContentType_t218 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LineType_t221 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_InputType_t219 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchScreenKeyboardType_t391 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterValidation_t220 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Vector2_t23_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t23  p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditState_t225_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t358_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_Object_t_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int16_t1078_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Char_t390_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Rect_t84_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t84 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Mode_t246 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Navigation_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef Navigation_t247  (*Func)(void* obj, const MethodInfo* method);
	Navigation_t247  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t84 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Direction_t253 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Single_t358_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Axis_t255 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MovementType_t259 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScrollbarVisibility_t260 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Bounds_t264 (const MethodInfo* method, void* obj, void** args)
{
	typedef Bounds_t264  (*Func)(void* obj, const MethodInfo* method);
	Bounds_t264  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Navigation_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Navigation_t247  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Navigation_t247 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Transition_t265 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ColorBlock_t174 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorBlock_t174  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorBlock_t174 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SpriteState_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef SpriteState_t267  (*Func)(void* obj, const MethodInfo* method);
	SpriteState_t267  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SpriteState_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SpriteState_t267  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((SpriteState_t267 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SelectionState_t266 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Vector3_t12_Object_t_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Color_t83_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83  p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t83 *)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ColorU26_t2744_Color_t83 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t83 * p1, Color_t83  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Color_t83 *)args[0], *((Color_t83 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Direction_t271 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t273 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return ret;
}

void* RuntimeInvoker_TextGenerationSettings_t351_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t351  (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	TextGenerationSettings_t351  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t23_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t84_Object_t_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t84  (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	Rect_t84  ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t84_Rect_t84_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t84  (*Func)(void* obj, Rect_t84  p1, Rect_t84  p2, const MethodInfo* method);
	Rect_t84  ret = ((Func)method->method)(obj, *((Rect_t84 *)args[0]), *((Rect_t84 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Rect_t84_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t84  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Rect_t84  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AspectMode_t286 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScaleMode_t288 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ScreenMatchMode_t289 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Unit_t290 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FitMode_t292 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Corner_t294 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Axis_t295 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Constraint_t296 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Int32_t359_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, float p3, float p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Single_t358_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Object_t_Object_t_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Object_t_Object_t_Single_t358_ILayoutElementU26_t2745 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, Object_t ** p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), (Object_t **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UIVertexU26_t2746_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t239 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertex_t239 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UIVertex_t239_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t239  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Color32_t347_Vector2_t23_Vector2_t23_Vector3_t12_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color32_t347  p2, Vector2_t23  p3, Vector2_t23  p4, Vector3_t12  p5, Vector4_t317  p6, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color32_t347 *)args[1]), *((Vector2_t23 *)args[2]), *((Vector2_t23 *)args[3]), *((Vector3_t12 *)args[4]), *((Vector4_t317 *)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Color32_t347_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color32_t347  p2, Vector2_t23  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color32_t347 *)args[1]), *((Vector2_t23 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertex_t239  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Color32_t347_Int32_t359_Int32_t359_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color32_t347  p2, int32_t p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t347 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_GcAchievementDescriptionData_t580_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementDescriptionData_t580  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementDescriptionData_t580 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_GcUserProfileData_t579_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcUserProfileData_t579  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((GcUserProfileData_t579 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Double_t675_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UserProfileU5BU5DU26_t2747_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t450** p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t450**)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UserProfileU5BU5DU26_t2747_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UserProfileU5BU5D_t450** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UserProfileU5BU5D_t450**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_GcScoreData_t582 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcScoreData_t582  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcScoreData_t582 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_BoneWeight_t455_BoneWeight_t455 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, BoneWeight_t455  p1, BoneWeight_t455  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((BoneWeight_t455 *)args[0]), *((BoneWeight_t455 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_InternalDrawTextureArgumentsU26_t2748 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, InternalDrawTextureArguments_t456 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (InternalDrawTextureArguments_t456 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, IntPtr_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((IntPtr_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Color_t83_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t83  (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	Color_t83  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_IntPtr_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_CullingGroupEvent_t463 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CullingGroupEvent_t463  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CullingGroupEvent_t463 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_CullingGroupEvent_t463_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CullingGroupEvent_t463  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CullingGroupEvent_t463 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Color_t83_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83  p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t83 *)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t2750_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TouchScreenKeyboard_InternalConstructorHelperArguments_t469 * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (TouchScreenKeyboard_InternalConstructorHelperArguments_t469 *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_LayerMask_t155 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LayerMask_t155  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LayerMask_t155 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LayerMask_t155_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef LayerMask_t155  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LayerMask_t155  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector2_t23_Vector2_t23_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Vector2_t23_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t23_Vector2_t23_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Vector2_t23  p1, float p2, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector2_t23_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t23_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, float p3, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Vector3U26_t2749_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, Vector3_t12 * p3, float p4, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (Vector3_t12 *)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Vector3U26_t2749_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, Vector3_t12 * p3, float p4, float p5, float p6, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (Vector3_t12 *)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, float p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Single_t358_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, float p1, Vector3_t12  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t83_Color_t83_Color_t83_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t83  (*Func)(void* obj, Color_t83  p1, Color_t83  p2, float p3, const MethodInfo* method);
	Color_t83  ret = ((Func)method->method)(obj, *((Color_t83 *)args[0]), *((Color_t83 *)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t83_Color_t83_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t83  (*Func)(void* obj, Color_t83  p1, float p2, const MethodInfo* method);
	Color_t83  ret = ((Func)method->method)(obj, *((Color_t83 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317_Color_t83 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, Color_t83  p1, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((Color_t83 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t347_Color_t83 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t347  (*Func)(void* obj, Color_t83  p1, const MethodInfo* method);
	Color32_t347  ret = ((Func)method->method)(obj, *((Color_t83 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color_t83_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color_t83  (*Func)(void* obj, Color32_t347  p1, const MethodInfo* method);
	Color_t83  ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Quaternion_t45_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Quaternion_t45  p1, Quaternion_t45  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), *((Quaternion_t45 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Single_t358_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, float p1, Vector3_t12  p2, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((float*)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Single_t358_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, float p1, Vector3_t12 * p2, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((float*)args[0]), (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Vector3U26_t2749_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, Quaternion_t45  p1, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_QuaternionU26_t2751 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, Quaternion_t45 * p1, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, (Quaternion_t45 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, float p1, float p2, float p3, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Quaternion_t45  p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_QuaternionU26_t2751 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Quaternion_t45 * p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Quaternion_t45 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, Vector3_t12 * p1, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Quaternion_t45_Quaternion_t45_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, Quaternion_t45  p1, Quaternion_t45  p2, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), *((Quaternion_t45 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Quaternion_t45_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Quaternion_t45  p1, Vector3_t12  p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Quaternion_t45_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t45  p1, Quaternion_t45  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), *((Quaternion_t45 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t84  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t84 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Rect_t84_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Rect_t84  p1, Rect_t84  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Rect_t84 *)args[0]), *((Rect_t84 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t401_Matrix4x4_t401 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, Matrix4x4_t401  p1, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, *((Matrix4x4_t401 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t401_Matrix4x4U26_t2752 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, Matrix4x4_t401 * p1, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, (Matrix4x4_t401 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Matrix4x4_t401_Matrix4x4U26_t2752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t401  p1, Matrix4x4_t401 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t401 *)args[0]), (Matrix4x4_t401 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Matrix4x4U26_t2752_Matrix4x4U26_t2752 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t401 * p1, Matrix4x4_t401 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Matrix4x4_t401 *)args[0], (Matrix4x4_t401 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t401 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector4_t317  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector4_t317 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t401_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Quaternion_t45_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Quaternion_t45  p2, Vector3_t12  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Quaternion_t45 *)args[1]), *((Vector3_t12 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Matrix4x4_t401_Vector3_t12_Quaternion_t45_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, Vector3_t12  p1, Quaternion_t45  p2, Vector3_t12  p3, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Quaternion_t45 *)args[1]), *((Vector3_t12 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t401_Vector3U26_t2749_QuaternionU26_t2751_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, Vector3_t12 * p1, Quaternion_t45 * p2, Vector3_t12 * p3, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Quaternion_t45 *)args[1], (Vector3_t12 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t401_Single_t358_Single_t358_Single_t358_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, float p1, float p2, float p3, float p4, float p5, float p6, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t401_Single_t358_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, float p1, float p2, float p3, float p4, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Matrix4x4_t401_Matrix4x4_t401_Matrix4x4_t401 (const MethodInfo* method, void* obj, void** args)
{
	typedef Matrix4x4_t401  (*Func)(void* obj, Matrix4x4_t401  p1, Matrix4x4_t401  p2, const MethodInfo* method);
	Matrix4x4_t401  ret = ((Func)method->method)(obj, *((Matrix4x4_t401 *)args[0]), *((Matrix4x4_t401 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317_Matrix4x4_t401_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, Matrix4x4_t401  p1, Vector4_t317  p2, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((Matrix4x4_t401 *)args[0]), *((Vector4_t317 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Matrix4x4_t401_Matrix4x4_t401 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Matrix4x4_t401  p1, Matrix4x4_t401  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Matrix4x4_t401 *)args[0]), *((Matrix4x4_t401 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Bounds_t264 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Bounds_t264  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Bounds_t264 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Bounds_t264 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t264  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t264 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Bounds_t264_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t264  p1, Vector3_t12  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t264 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_BoundsU26_t2753_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t264 * p1, Vector3_t12 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Bounds_t264 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Bounds_t264_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t264  p1, Vector3_t12  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Bounds_t264 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_BoundsU26_t2753_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Bounds_t264 * p1, Vector3_t12 * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Bounds_t264 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_RayU26_t2754_BoundsU26_t2753_SingleU26_t2755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t365 * p1, Bounds_t264 * p2, float* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Ray_t365 *)args[0], (Bounds_t264 *)args[1], (float*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Ray_t365 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t365  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t365 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Ray_t365_SingleU26_t2755 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t365  p1, float* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t365 *)args[0]), (float*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_BoundsU26_t2753_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Bounds_t264 * p1, Vector3_t12 * p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Bounds_t264 *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Bounds_t264_Bounds_t264 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Bounds_t264  p1, Bounds_t264  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Bounds_t264 *)args[0]), *((Bounds_t264 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Vector4_t317_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t317  p1, Vector4_t317  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Vector4_t317 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Vector4_t317  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317_Vector4_t317_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, Vector4_t317  p1, Vector4_t317  p2, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Vector4_t317 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector4_t317_Vector4_t317_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, Vector4_t317  p1, float p2, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector4_t317_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t317  p1, Vector4_t317  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Vector4_t317 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, float p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Single_t358_Single_t358_SingleU26_t2755_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Single_t358_Single_t358_SingleU26_t2755_Single_t358_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, float p1, float p2, float* p3, float p4, float p5, float p6, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), (float*)args[2], *((float*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_RectU26_t2756 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Rect_t84 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector2U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t23 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SphericalHarmonicsL2U26_t2758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, SphericalHarmonicsL2_t481 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (SphericalHarmonicsL2_t481 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Color_t83_SphericalHarmonicsL2U26_t2758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83  p1, SphericalHarmonicsL2_t481 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Color_t83 *)args[0]), (SphericalHarmonicsL2_t481 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_ColorU26_t2744_SphericalHarmonicsL2U26_t2758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83 * p1, SphericalHarmonicsL2_t481 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t83 *)args[0], (SphericalHarmonicsL2_t481 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Color_t83_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color_t83  p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color_t83 *)args[1]), *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Color_t83_SphericalHarmonicsL2U26_t2758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, Color_t83  p2, SphericalHarmonicsL2_t481 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Color_t83 *)args[1]), (SphericalHarmonicsL2_t481 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3U26_t2749_ColorU26_t2744_SphericalHarmonicsL2U26_t2758 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12 * p1, Color_t83 * p2, SphericalHarmonicsL2_t481 * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t12 *)args[0], (Color_t83 *)args[1], (SphericalHarmonicsL2_t481 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t481  (*Func)(void* obj, SphericalHarmonicsL2_t481  p1, float p2, const MethodInfo* method);
	SphericalHarmonicsL2_t481  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t481 *)args[0]), *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t481_Single_t358_SphericalHarmonicsL2_t481 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t481  (*Func)(void* obj, float p1, SphericalHarmonicsL2_t481  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t481  ret = ((Func)method->method)(obj, *((float*)args[0]), *((SphericalHarmonicsL2_t481 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481 (const MethodInfo* method, void* obj, void** args)
{
	typedef SphericalHarmonicsL2_t481  (*Func)(void* obj, SphericalHarmonicsL2_t481  p1, SphericalHarmonicsL2_t481  p2, const MethodInfo* method);
	SphericalHarmonicsL2_t481  ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t481 *)args[0]), *((SphericalHarmonicsL2_t481 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SphericalHarmonicsL2_t481  p1, SphericalHarmonicsL2_t481  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((SphericalHarmonicsL2_t481 *)args[0]), *((SphericalHarmonicsL2_t481 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector4U26_t2759 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t317 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4_t317 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Vector4_t317_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector4_t317  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector4_t317  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t23_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Vector2U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector2_t23 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t23 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_RuntimePlatform_t437 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_CameraClearFlags_t585 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector3_t12_Object_t_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t365_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t365  (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	Ray_t365  ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t365_Object_t_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t365  (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, const MethodInfo* method);
	Ray_t365  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t365_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t365  p1, float p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t365 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t2754_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t365 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t365 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_RayU26_t2754_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Ray_t365 * p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Ray_t365 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_RenderBuffer_t584 (const MethodInfo* method, void* obj, void** args)
{
	typedef RenderBuffer_t584  (*Func)(void* obj, const MethodInfo* method);
	RenderBuffer_t584  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_RenderBufferU26_t2760_RenderBufferU26_t2760 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, RenderBuffer_t584 * p2, RenderBuffer_t584 * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (RenderBuffer_t584 *)args[1], (RenderBuffer_t584 *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t* p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (int32_t*)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TouchPhase_t492 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t12 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Touch_t72_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t72  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Touch_t72  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t45  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_QuaternionU26_t2751 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Quaternion_t45 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Quaternion_t45 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Matrix4x4U26_t2752 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Matrix4x4_t401 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Matrix4x4_t401 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3_t12_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12  p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12_RaycastHitU26_t2761_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, RaycastHit_t100 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (RaycastHit_t100 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Ray_t365_RaycastHitU26_t2761_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t365  p1, RaycastHit_t100 * p2, float p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t365 *)args[0]), (RaycastHit_t100 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Ray_t365_RaycastHitU26_t2761_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Ray_t365  p1, RaycastHit_t100 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Ray_t365 *)args[0]), (RaycastHit_t100 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Ray_t365_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Ray_t365  p1, float p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Ray_t365 *)args[0]), *((float*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector3U26_t2749_Vector3U26_t2749_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, float p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12_RaycastHitU26_t2761 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, RaycastHit_t100 * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (RaycastHit_t100 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12_RaycastHitU26_t2761_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, RaycastHit_t100 * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (RaycastHit_t100 *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector3U26_t2749_Vector3U26_t2749_RaycastHitU26_t2761_Single_t358_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector3_t12 * p1, Vector3_t12 * p2, RaycastHit_t100 * p3, float p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Vector3_t12 *)args[0], (Vector3_t12 *)args[1], (RaycastHit_t100 *)args[2], *((float*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Vector3U26_t2749_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Vector3_t12 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Vector3_t12 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_WheelHitU26_t2762 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, WheelHit_t75 * p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (WheelHit_t75 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector3U26_t2749_QuaternionU26_t2751 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3_t12 * p1, Quaternion_t45 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3_t12 *)args[0], (Quaternion_t45 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector2_t23_Vector2_t23_Single_t358_Int32_t359_Single_t358_Single_t358_RaycastHit2DU26_t2763 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t369 * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t369 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector2U26_t2757_Vector2U26_t2757_Single_t358_Int32_t359_Single_t358_Single_t358_RaycastHit2DU26_t2763 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23 * p1, Vector2_t23 * p2, float p3, int32_t p4, float p5, float p6, RaycastHit2D_t369 * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t23 *)args[0], (Vector2_t23 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (RaycastHit2D_t369 *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t369_Vector2_t23_Vector2_t23_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t369  (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, float p3, int32_t p4, const MethodInfo* method);
	RaycastHit2D_t369  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t369_Vector2_t23_Vector2_t23_Single_t358_Int32_t359_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t369  (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	RaycastHit2D_t369  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t23_Vector2_t23_Single_t358_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, float p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), *((float*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Vector2U26_t2757_Vector2U26_t2757_Single_t358_Int32_t359_Single_t358_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t23 * p1, Vector2_t23 * p2, float p3, int32_t p4, float p5, float p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Vector2_t23 *)args[0], (Vector2_t23 *)args[1], *((float*)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_SByte_t1077_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_SendMessageOptions_t435 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorStateInfo_t522 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorStateInfo_t522  (*Func)(void* obj, const MethodInfo* method);
	AnimatorStateInfo_t522  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AnimatorClipInfo_t523 (const MethodInfo* method, void* obj, void** args)
{
	typedef AnimatorClipInfo_t523  (*Func)(void* obj, const MethodInfo* method);
	AnimatorClipInfo_t523  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078_CharacterInfoU26_t2764_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t533 * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t533 *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078_CharacterInfoU26_t2764_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t533 * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t533 *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078_CharacterInfoU26_t2764 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, CharacterInfo_t533 * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (CharacterInfo_t533 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Color_t83_Int32_t359_Single_t358_Single_t358_Int32_t359_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_Vector2_t23_Vector2_t23_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t83  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, Vector2_t23  p16, Vector2_t23  p17, int8_t p18, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t83 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((Vector2_t23 *)args[15]), *((Vector2_t23 *)args[16]), *((int8_t*)args[17]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Color_t83_Int32_t359_Single_t358_Single_t358_Int32_t359_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_Single_t358_Single_t358_Single_t358_Single_t358_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Color_t83  p3, int32_t p4, float p5, float p6, int32_t p7, int8_t p8, int8_t p9, int32_t p10, int32_t p11, int32_t p12, int32_t p13, int8_t p14, int32_t p15, float p16, float p17, float p18, float p19, int8_t p20, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((Color_t83 *)args[2]), *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), *((int32_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int8_t*)args[13]), *((int32_t*)args[14]), *((float*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((int8_t*)args[19]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_ColorU26_t2744_Int32_t359_Single_t358_Single_t358_Int32_t359_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_Single_t358_Single_t358_Single_t358_Single_t358_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Color_t83 * p4, int32_t p5, float p6, float p7, int32_t p8, int8_t p9, int8_t p10, int32_t p11, int32_t p12, int32_t p13, int32_t p14, int8_t p15, int32_t p16, float p17, float p18, float p19, float p20, int8_t p21, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Color_t83 *)args[3], *((int32_t*)args[4]), *((float*)args[5]), *((float*)args[6]), *((int32_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), *((int32_t*)args[12]), *((int32_t*)args[13]), *((int8_t*)args[14]), *((int32_t*)args[15]), *((float*)args[16]), *((float*)args[17]), *((float*)args[18]), *((float*)args[19]), *((int8_t*)args[20]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextGenerationSettings_t351_TextGenerationSettings_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef TextGenerationSettings_t351  (*Func)(void* obj, TextGenerationSettings_t351  p1, const MethodInfo* method);
	TextGenerationSettings_t351  ret = ((Func)method->method)(obj, *((TextGenerationSettings_t351 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Object_t_TextGenerationSettings_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t351  p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t351 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_TextGenerationSettings_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, TextGenerationSettings_t351  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((TextGenerationSettings_t351 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RenderMode_t537 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_ColorU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Color_t83 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Color_t83 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_RectU26_t2756 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Rect_t84 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Rect_t84 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Object_t_Vector2_t23_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Vector2U26_t2757_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t23 * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Vector2_t23 *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Vector2_t23_Vector2_t23_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector2_t23  (*Func)(void* obj, Vector2_t23  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Vector2_t23  ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector2_t23_Object_t_Object_t_Vector2U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23  p1, Object_t * p2, Object_t * p3, Vector2_t23 * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector2_t23 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Vector2_t23 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector2U26_t2757_Object_t_Object_t_Vector2U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23 * p1, Object_t * p2, Object_t * p3, Vector2_t23 * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t23 *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Vector2_t23 *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Object_t_Vector2_t23_Object_t_Vector3U26_t2749 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, Object_t * p3, Vector3_t12 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), (Object_t *)args[2], (Vector3_t12 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Vector2_t23_Object_t_Vector2U26_t2757 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, Object_t * p3, Vector2_t23 * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), (Object_t *)args[2], (Vector2_t23 *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Ray_t365_Object_t_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t365  (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, const MethodInfo* method);
	Ray_t365  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Ray_t365 (const MethodInfo* method, void* obj, void** args)
{
	typedef Ray_t365  (*Func)(void* obj, const MethodInfo* method);
	Ray_t365  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Ray_t365 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Ray_t365  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Ray_t365 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_EventType_t539 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventType_t539_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EventModifiers_t540 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyCode_t538 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t546  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t546 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Rect_t84_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t84 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Rect_t84_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84  p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t84 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Rect_t84_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84  p1, Object_t * p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t84 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Rect_t84_Object_t_Int32_t359_SByte_t1077_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Rect_t84  p1, Object_t * p2, int32_t p3, int8_t p4, float p5, const MethodInfo* method);
	((Func)method->method)(obj, *((Rect_t84 *)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int8_t*)args[3]), *((float*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_Int32_t359_Single_t358_Single_t358_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, float p5, float p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((float*)args[4]), *((float*)args[5]), (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_ColorU26_t2744 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color_t83 * p1, const MethodInfo* method);
	((Func)method->method)(obj, (Color_t83 *)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Rect_t84_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Rect_t84  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Rect_t84  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Rect_t84 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t84  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Rect_t84 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_RectU26_t2756 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Rect_t84 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Rect_t84 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358_Single_t358_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float p1, float p2, float p3, float p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, *((float*)args[0]), *((float*)args[1]), *((float*)args[2]), *((float*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_UserState_t601 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Double_t675_SByte_t1077_SByte_t1077_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int8_t p3, int8_t p4, DateTime_t546  p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((DateTime_t546 *)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, double p1, const MethodInfo* method);
	((Func)method->method)(obj, *((double*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Object_t_DateTime_t546_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, DateTime_t546  p4, Object_t * p5, int32_t p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((DateTime_t546 *)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UserScope_t602 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Range_t595 (const MethodInfo* method, void* obj, void** args)
{
	typedef Range_t595  (*Func)(void* obj, const MethodInfo* method);
	Range_t595  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Range_t595 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Range_t595  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Range_t595 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_TimeScope_t603 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_HitInfo_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, HitInfo_t597  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((HitInfo_t597 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_HitInfo_t597_HitInfo_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t597  p1, HitInfo_t597  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t597 *)args[0]), *((HitInfo_t597 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_HitInfo_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, HitInfo_t597  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((HitInfo_t597 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_StringU26_t2765_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_StreamingContext_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, StreamingContext_t653  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t653 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Color_t83_Color_t83 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color_t83  p1, Color_t83  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color_t83 *)args[0]), *((Color_t83 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_TextGenerationSettings_t351 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TextGenerationSettings_t351  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TextGenerationSettings_t351 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PersistentListenerMode_t618 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t677_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_UInt32_t677_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t720_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, Object_t * p7, int32_t p8, int32_t p9, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], *((int32_t*)args[7]), *((int32_t*)args[8]), method);
	return NULL;
}

void* RuntimeInvoker_ConfidenceFactor_t724 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_ByteU26_t2766_Int32U26_t2743_ByteU5BU5DU26_t2767 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, uint8_t* p3, int32_t* p4, ByteU5BU5D_t698** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint8_t*)args[2], (int32_t*)args[3], (ByteU5BU5D_t698**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_DateTime_t546_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DSAParameters_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DSAParameters_t859  p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((DSAParameters_t859 *)args[1]), method);
	return ret;
}

void* RuntimeInvoker_RSAParameters_t831_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t831  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	RSAParameters_t831  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_RSAParameters_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RSAParameters_t831  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RSAParameters_t831 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_DSAParameters_t859_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t859  (*Func)(void* obj, bool* p1, const MethodInfo* method);
	DSAParameters_t859  ret = ((Func)method->method)(obj, (bool*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t546  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t761 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Byte_t664_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_AlertLevel_t780 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AlertDescription_t781 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Int16_t1078_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_CipherAlgorithmType_t783 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HashAlgorithmType_t802 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExchangeAlgorithmType_t800 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CipherMode_t711 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_ByteU5BU5DU26_t2767_ByteU5BU5DU26_t2767 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ByteU5BU5D_t698** p2, ByteU5BU5D_t698** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ByteU5BU5D_t698**)args[1], (ByteU5BU5D_t698**)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1078_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Int16_t1078_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int16_t p10, int8_t p11, int8_t p12, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int16_t*)args[9]), *((int8_t*)args[10]), *((int8_t*)args[11]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_SecurityProtocolType_t817 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityCompressionType_t816 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeType_t834 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HandshakeState_t801 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t817_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t664_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t664_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Byte_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t664_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Int64_t676_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Byte_t664_Byte_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_RSAParameters_t831 (const MethodInfo* method, void* obj, void** args)
{
	typedef RSAParameters_t831  (*Func)(void* obj, const MethodInfo* method);
	RSAParameters_t831  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Byte_t664_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, uint8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), *((uint8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Byte_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, uint8_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t795 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2768 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, DictionaryNode_t910 ** p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (DictionaryNode_t910 **)args[1], method);
	return ret;
}

void* RuntimeInvoker_DictionaryEntry_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_EditorBrowsableState_t921 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_IPAddressU26_t2769 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPAddress_t938 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPAddress_t938 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_AddressFamily_t926 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_IPv6AddressU26_t2770 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, IPv6Address_t940 ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (IPv6Address_t940 **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SecurityProtocolType_t941 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_AsnDecodeStatus_t982_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_X509ChainStatusFlags_t970_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t970_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t970_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatusFlags_t970 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_X509RevocationFlag_t977 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509RevocationMode_t978 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509VerificationFlags_t981 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t975 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509KeyUsageFlags_t975_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t664_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Int16_t1078_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_RegexOptions_t994 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1001_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_UInt16_t1064_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UInt16_t1064_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_Int16_t1078_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_Object_t_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, Object_t * p2, int8_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UInt16_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_UInt16_t1064_UInt16_t1064_UInt16_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_OpFlags_t996_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UInt16_t1064_UInt16_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, uint16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((uint16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Int32U26_t2743_Int32U26_t2743_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, int8_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_UInt16_t1064_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Int32_t359_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t* p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (int32_t*)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1016  (*Func)(void* obj, const MethodInfo* method);
	Interval_t1016  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Interval_t1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Interval_t1016  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Interval_t1016 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Interval_t1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Interval_t1016  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Interval_t1016 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Interval_t1016_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Interval_t1016  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Interval_t1016  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Double_t675_Interval_t1016 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Interval_t1016  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Interval_t1016 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Interval_t1016_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Interval_t1016  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Interval_t1016 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Double_t675_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_RegexOptionsU26_t2771 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_RegexOptionsU26_t2771_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Int32U26_t2743_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Category_t1001 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int16_t1078_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UInt16_t1064_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint16_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((uint16_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_UInt16_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((uint16_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Position_t997 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriHostNameType_t1049_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	((Func)method->method)(obj, (String_t**)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Char_t390_Object_t_Int32U26_t2743_CharU26_t2772 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t* p2, uint16_t* p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (uint16_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_UriFormatExceptionU26_t2773 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, UriFormatException_t1048 ** p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (UriFormatException_t1048 **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_ObjectU5BU5DU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, ObjectU5BU5D_t77** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ObjectU5BU5D_t77**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_ObjectU5BU5DU26_t2774 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t77** p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t77**)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_SByte_t1077_Object_t_Int32_t359_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int32_t p3, Exception_t68 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Exception_t68 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_Int32U26_t2743_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int32_t* p3, Exception_t68 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int32_t*)args[2], (Exception_t68 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_SByte_t1077_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, Exception_t68 ** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (Exception_t68 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32U26_t2743_Object_t_SByte_t1077_SByte_t1077_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int8_t p3, int8_t p4, Exception_t68 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), (Exception_t68 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743_Object_t_Object_t_BooleanU26_t2742_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, bool* p5, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], (bool*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743_Object_t_Object_t_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, bool* p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], (bool*)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Int32U26_t2743_Object_t_Int32U26_t2743_SByte_t1077_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, int32_t* p3, int8_t p4, Exception_t68 ** p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (int32_t*)args[2], *((int8_t*)args[3]), (Exception_t68 **)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32U26_t2743_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_Int32U26_t2743_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, Exception_t68 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], (Exception_t68 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeCode_t1704 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_Int64U26_t2776_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int64_t* p3, Exception_t68 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int64_t*)args[2], (Exception_t68 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_Int64U26_t2776_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int64_t* p5, Exception_t68 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int64_t*)args[4], (Exception_t68 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int64U26_t2776 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_Int64U26_t2776 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int64_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int64_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_UInt32U26_t2777_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, uint32_t* p3, Exception_t68 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (uint32_t*)args[2], (Exception_t68 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_UInt32U26_t2777_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint32_t* p5, Exception_t68 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint32_t*)args[4], (Exception_t68 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_UInt32U26_t2777 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_UInt32U26_t2777 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint32_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_UInt64U26_t2778_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, uint64_t* p5, Exception_t68 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (uint64_t*)args[4], (Exception_t68 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_UInt64U26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint64_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_ByteU26_t2766 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_ByteU26_t2766 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint8_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint8_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_SByteU26_t2779_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t* p3, Exception_t68 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int8_t*)args[2], (Exception_t68 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByteU26_t2779 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int8_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_Int16U26_t2780_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int8_t p2, int16_t* p3, Exception_t68 ** p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (int16_t*)args[2], (Exception_t68 **)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int16U26_t2780 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_UInt16U26_t2781 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, uint16_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (uint16_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_UInt16U26_t2781 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, uint16_t* p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (uint16_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ByteU2AU26_t2782_ByteU2AU26_t2782_DoubleU2AU26_t2783_UInt16U2AU26_t2784_UInt16U2AU26_t2784_UInt16U2AU26_t2784_UInt16U2AU26_t2784 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t** p1, uint8_t** p2, double** p3, uint16_t** p4, uint16_t** p5, uint16_t** p6, uint16_t** p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint8_t**)args[0], (uint8_t**)args[1], (double**)args[2], (uint16_t**)args[3], (uint16_t**)args[4], (uint16_t**)args[5], (uint16_t**)args[6], method);
	return NULL;
}

void* RuntimeInvoker_UnicodeCategory_t1227_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_Int16_t1078_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int16_t p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Char_t390_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, int8_t p6, Object_t * p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), (Object_t *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1078_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_Int32U26_t2743_Int32U26_t2743_BooleanU26_t2742_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t* p3, int32_t* p4, bool* p5, String_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (int32_t*)args[2], (int32_t*)args[3], (bool*)args[4], (String_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, double p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_DoubleU26_t2785_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, double* p5, Exception_t68 ** p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (double*)args[4], (Exception_t68 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_DoubleU26_t2785 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, double* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (double*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t1079_Decimal_t1079_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, Decimal_t1079  p1, Decimal_t1079  p2, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), *((Decimal_t1079 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Decimal_t1079_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1079  p1, Decimal_t1079  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), *((Decimal_t1079 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Decimal_t1079_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079  p1, Decimal_t1079  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), *((Decimal_t1079 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Int32U26_t2743_BooleanU26_t2742_BooleanU26_t2742_Int32U26_t2743_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t* p4, bool* p5, bool* p6, int32_t* p7, int8_t p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (int32_t*)args[3], (bool*)args[4], (bool*)args[5], (int32_t*)args[6], *((int8_t*)args[7]), method);
	return ret;
}

void* RuntimeInvoker_Decimal_t1079_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_DecimalU26_t2786_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Decimal_t1079 * p4, int8_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Decimal_t1079 *)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DecimalU26_t2786_UInt64U26_t2778 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079 * p1, uint64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], (uint64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DecimalU26_t2786_Int64U26_t2776 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079 * p1, int64_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], (int64_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DecimalU26_t2786_DecimalU26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079 * p1, Decimal_t1079 * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], (Decimal_t1079 *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DecimalU26_t2786_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079 * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DecimalU26_t2786_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079 * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_DecimalU26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1079 * p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_DecimalU26_t2786_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1079 * p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Decimal_t1079 *)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_DecimalU26_t2786_DecimalU26_t2786_DecimalU26_t2786 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Decimal_t1079 * p1, Decimal_t1079 * p2, Decimal_t1079 * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Decimal_t1079 *)args[0], (Decimal_t1079 *)args[1], (Decimal_t1079 *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, float p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, double p1, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UInt32_t677 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIntPtr_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIntPtr_t  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIntPtr_t  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2787 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, MulticastDelegate_t227 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (MulticastDelegate_t227 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t676_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int64_t676_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int64_t p3, int64_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int64_t*)args[2]), *((int64_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Object_t_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, int64_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), *((int64_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_TypeAttributes_t1352 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MemberTypes_t1331 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeTypeHandle_t1084 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1084  (*Func)(void* obj, const MethodInfo* method);
	RuntimeTypeHandle_t1084  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_TypeCode_t1704_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RuntimeTypeHandle_t1084 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeTypeHandle_t1084  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeTypeHandle_t1084 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_RuntimeTypeHandle_t1084_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeTypeHandle_t1084  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	RuntimeTypeHandle_t1084  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_RuntimeFieldHandle_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, RuntimeFieldHandle_t1086  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((RuntimeFieldHandle_t1086 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_ContractionU5BU5DU26_t2788_Level2MapU5BU5DU26_t2789 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, ContractionU5BU5D_t1131** p3, Level2MapU5BU5D_t1132** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (ContractionU5BU5D_t1131**)args[2], (Level2MapU5BU5D_t1132**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_CodePointIndexerU26_t2790_ByteU2AU26_t2782_ByteU2AU26_t2782_CodePointIndexerU26_t2790_ByteU2AU26_t2782 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, CodePointIndexer_t1115 ** p2, uint8_t** p3, uint8_t** p4, CodePointIndexer_t1115 ** p5, uint8_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (CodePointIndexer_t1115 **)args[1], (uint8_t**)args[2], (uint8_t**)args[3], (CodePointIndexer_t1115 **)args[4], (uint8_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Byte_t664_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ExtenderType_t1128_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_BooleanU26_t2742_BooleanU26_t2742_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_BooleanU26_t2742_BooleanU26_t2742_SByte_t1077_SByte_t1077_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t p6, bool* p7, bool* p8, int8_t p9, int8_t p10, Context_t1125 * p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int32_t*)args[5]), (bool*)args[6], (bool*)args[7], *((int8_t*)args[8]), *((int8_t*)args[9]), (Context_t1125 *)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_Int32_t359_SByte_t1077_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int8_t p5, Context_t1125 * p6, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), (Context_t1125 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, bool* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (bool*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int16_t1078_Int32_t359_SByte_t1077_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int16_t p5, int32_t p6, int8_t p7, Context_t1125 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int16_t*)args[4]), *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1125 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_Object_t_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, Context_t1125 * p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (Context_t1125 *)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, Context_t1125 * p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), (Context_t1125 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Object_t_SByte_t1077_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, Context_t1125 * p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), (Context_t1125 *)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Object_t_SByte_t1077_Int32_t359_ContractionU26_t2792_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, Object_t * p5, int8_t p6, int32_t p7, Contraction_t1118 ** p8, Context_t1125 * p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int8_t*)args[5]), *((int32_t*)args[6]), (Contraction_t1118 **)args[7], (Context_t1125 *)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, int32_t p4, Object_t * p5, int32_t p6, int8_t p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], *((int32_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Int32_t359_Object_t_SByte_t1077_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, Context_t1125 * p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), (Context_t1125 *)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Int32_t359_Object_t_SByte_t1077_Int32_t359_ContractionU26_t2792_ContextU26_t2791 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, int8_t p7, int32_t p8, Contraction_t1118 ** p9, Context_t1125 * p10, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], *((int8_t*)args[6]), *((int32_t*)args[7]), (Contraction_t1118 **)args[8], (Context_t1125 *)args[9], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, int32_t p8, int32_t p9, int32_t p10, int32_t p11, int32_t p12, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), *((int32_t*)args[7]), *((int32_t*)args[8]), *((int32_t*)args[9]), *((int32_t*)args[10]), *((int32_t*)args[11]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_ByteU5BU5DU26_t2767_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, ByteU5BU5D_t698** p2, int32_t* p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (ByteU5BU5D_t698**)args[1], (int32_t*)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ConfidenceFactor_t1137 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Sign_t1139_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DSAParameters_t859_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef DSAParameters_t859  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DSAParameters_t859  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_DSAParameters_t859 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DSAParameters_t859  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DSAParameters_t859 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int16_t1078_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1078_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Single_t358_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Single_t358_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Single_t358_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, float p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((float*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_SByte_t1077_MethodBaseU26_t2793_Int32U26_t2743_Int32U26_t2743_StringU26_t2765_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, int8_t p2, MethodBase_t679 ** p3, int32_t* p4, int32_t* p5, String_t** p6, int32_t* p7, int32_t* p8, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int8_t*)args[1]), (MethodBase_t679 **)args[2], (int32_t*)args[3], (int32_t*)args[4], (String_t**)args[5], (int32_t*)args[6], (int32_t*)args[7], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t546  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1653_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t546  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int32U26_t2743_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t* p1, int32_t p2, int32_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DayOfWeek_t1653_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32U26_t2743_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t* p2, int32_t* p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], (int32_t*)args[1], (int32_t*)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_DateTime_t546_DateTime_t546_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t546  p1, DateTime_t546  p2, TimeSpan_t969  p3, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((DateTime_t546 *)args[1]), *((TimeSpan_t969 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef Decimal_t1079  (*Func)(void* obj, const MethodInfo* method);
	Decimal_t1079  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_SByte_t1077_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, int32_t p4, int8_t p5, int8_t p6, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int8_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_IntPtr_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int8_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Object_t_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_FileAttributes_t1237_Object_t_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MonoFileType_t1246_IntPtr_t_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_MonoIOStatU26_t2795_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, MonoIOStat_t1245 * p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (MonoIOStat_t1245 *)args[1], (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_IntPtr_t_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_IntPtr_t_Object_t_Int32_t359_Int32_t359_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, int32_t* p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_IntPtr_t_Int64_t676_Int32_t359_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t p3, int32_t* p4, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), *((int32_t*)args[2]), (int32_t*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_IntPtr_t_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, IntPtr_t p1, int32_t* p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_IntPtr_t_Int64_t676_MonoIOErrorU26_t2794 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int64_t p2, int32_t* p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int64_t*)args[1]), (int32_t*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_CallingConventions_t1321 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeMethodHandle_t1696 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeMethodHandle_t1696  (*Func)(void* obj, const MethodInfo* method);
	RuntimeMethodHandle_t1696  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1332 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodToken_t1288 (const MethodInfo* method, void* obj, void** args)
{
	typedef MethodToken_t1288  (*Func)(void* obj, const MethodInfo* method);
	MethodToken_t1288  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_FieldAttributes_t1329 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RuntimeFieldHandle_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef RuntimeFieldHandle_t1086  (*Func)(void* obj, const MethodInfo* method);
	RuntimeFieldHandle_t1086  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_OpCode_t1292 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1292  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1292 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_OpCode_t1292_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, OpCode_t1292  p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((OpCode_t1292 *)args[0]), (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_StackBehaviour_t1296 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int8_t*)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2743_ModuleU26_t2796 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, Object_t * p1, int32_t* p2, Module_t1285 ** p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], (Module_t1285 **)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return ret;
}

void* RuntimeInvoker_AssemblyNameFlags_t1315 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_ObjectU5BU5DU26_t2774_Object_t_Object_t_Object_t_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, ObjectU5BU5D_t77** p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t ** p7, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (ObjectU5BU5D_t77**)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t **)args[6], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_ObjectU5BU5DU26_t2774_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t77** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t77**)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_ObjectU5BU5DU26_t2774_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, ObjectU5BU5D_t77** p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (ObjectU5BU5D_t77**)args[1], (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, Object_t * p3, Object_t * p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_EventAttributes_t1327 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, IntPtr_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeFieldHandle_t1086 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeFieldHandle_t1086  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeFieldHandle_t1086 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, int32_t p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], *((int32_t*)args[4]), (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t653  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t653 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_RuntimeMethodHandle_t1696 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RuntimeMethodHandle_t1696  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RuntimeMethodHandle_t1696 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_MonoEventInfoU26_t2798 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEventInfo_t1336 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEventInfo_t1336 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoEventInfo_t1336_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoEventInfo_t1336  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	MonoEventInfo_t1336  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_MonoMethodInfoU26_t2799 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, MonoMethodInfo_t1340 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (MonoMethodInfo_t1340 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_MonoMethodInfo_t1340_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef MonoMethodInfo_t1340  (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	MonoMethodInfo_t1340  ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_MethodAttributes_t1332_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CallingConventions_t1321_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Exception_t68 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Exception_t68 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_MonoPropertyInfoU26_t2800_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoPropertyInfo_t1341 * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoPropertyInfo_t1341 *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_PropertyAttributes_t1348 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_ParameterAttributes_t1344 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int64_t676_ResourceInfoU26_t2801 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, ResourceInfo_t1356 * p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (ResourceInfo_t1356 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_GCHandle_t1385_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef GCHandle_t1385  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	GCHandle_t1385  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Object_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, Object_t * p2, int32_t p3, int32_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Byte_t664_IntPtr_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, String_t** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (String_t**)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, String_t** p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (String_t**)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TimeSpan_t969  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int8_t p3, Object_t * p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t653_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t653  p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t653 *)args[2]), (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_StreamingContext_t653_ISurrogateSelectorU26_t2802 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, StreamingContext_t653  p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((StreamingContext_t653 *)args[1]), (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, IntPtr_t p2, Object_t * p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((IntPtr_t*)args[1]), (Object_t *)args[2], method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t969_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, String_t** p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (String_t**)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Boolean_t360_Object_t_StringU26_t2765_StringU26_t2765 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, String_t** p2, String_t** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (String_t**)args[1], (String_t**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_WellKnownObjectMode_t1491 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContext_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef StreamingContext_t653  (*Func)(void* obj, const MethodInfo* method);
	StreamingContext_t653  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeFilterLevel_t1507 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Byte_t664_Object_t_SByte_t1077_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Byte_t664_Object_t_SByte_t1077_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t * p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_ObjectU26_t2797_HeaderU5BU5DU26_t2803 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t ** p3, HeaderU5BU5D_t1712** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t **)args[2], (HeaderU5BU5D_t1712**)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Byte_t664_Object_t_SByte_t1077_ObjectU26_t2797_HeaderU5BU5DU26_t2803 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int8_t p3, Object_t ** p4, HeaderU5BU5D_t1712** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), (Object_t **)args[3], (HeaderU5BU5D_t1712**)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Byte_t664_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Byte_t664_Object_t_Int64U26_t2776_ObjectU26_t2797_SerializationInfoU26_t2804 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint8_t p1, Object_t * p2, int64_t* p3, Object_t ** p4, SerializationInfo_t652 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, *((uint8_t*)args[0]), (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], (SerializationInfo_t652 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_SByte_t1077_Int64U26_t2776_ObjectU26_t2797_SerializationInfoU26_t2804 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int8_t p2, int8_t p3, int64_t* p4, Object_t ** p5, SerializationInfo_t652 ** p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), *((int8_t*)args[2]), (int64_t*)args[3], (Object_t **)args[4], (SerializationInfo_t652 **)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64U26_t2776_ObjectU26_t2797_SerializationInfoU26_t2804 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, SerializationInfo_t652 ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], (SerializationInfo_t652 **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64_t676_ObjectU26_t2797_SerializationInfoU26_t2804 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t ** p4, SerializationInfo_t652 ** p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t **)args[3], (SerializationInfo_t652 **)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Object_t_Int64_t676_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64U26_t2776_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t* p2, Object_t ** p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (int64_t*)args[1], (Object_t **)args[2], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64U26_t2776_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t* p3, Object_t ** p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (int64_t*)args[2], (Object_t **)args[3], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64_t676_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, int64_t p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int64_t*)args[2]), (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Int64_t676_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, Object_t * p3, Object_t * p4, Object_t * p5, Object_t * p6, Object_t * p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], (Object_t *)args[5], (Object_t *)args[6], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int64_t676_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int64_t p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, uint8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((uint8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Int32_t359_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int64_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int64_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Object_t_Int64_t676_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, int64_t p4, Object_t * p5, Object_t * p6, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], *((int64_t*)args[3]), (Object_t *)args[4], (Object_t *)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_SByte_t1077_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, Object_t * p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_StreamingContext_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t653  p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t653 *)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_StreamingContext_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, StreamingContext_t653  p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((StreamingContext_t653 *)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_StreamingContext_t653 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StreamingContext_t653  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((StreamingContext_t653 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_StreamingContext_t653_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, StreamingContext_t653  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((StreamingContext_t653 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, int16_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DateTime_t546  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((DateTime_t546 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_SerializationEntry_t1524 (const MethodInfo* method, void* obj, void** args)
{
	typedef SerializationEntry_t1524  (*Func)(void* obj, const MethodInfo* method);
	SerializationEntry_t1524  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_StreamingContextStates_t1527 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CspProviderFlags_t1531 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_UInt32_t677_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UInt32U26_t2777_Int32_t359_UInt32U26_t2777_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint32_t* p1, int32_t p2, uint32_t* p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, (uint32_t*)args[0], *((int32_t*)args[1]), (uint32_t*)args[2], *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, IntPtr_t p1, Object_t * p2, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UInt64_t1076_Int64_t676_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Int64_t676_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, int64_t p2, int64_t p3, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int64_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PaddingMode_t715 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_StringBuilderU26_t2805_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, StringBuilder_t356 ** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (StringBuilder_t356 **)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_EncoderFallbackBufferU26_t2806_CharU5BU5DU26_t2807 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, EncoderFallbackBuffer_t1603 ** p6, CharU5BU5D_t238** p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (EncoderFallbackBuffer_t1603 **)args[5], (CharU5BU5D_t238**)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_DecoderFallbackBufferU26_t2808 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, DecoderFallbackBuffer_t1594 ** p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (DecoderFallbackBuffer_t1594 **)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int16_t1078_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int16_t1078_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int16_t p1, int16_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t* p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (int32_t*)args[0], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int32_t359_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int32_t p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int8_t p4, int32_t p5, int8_t p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int8_t*)args[3]), *((int32_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077_Int32U26_t2743_BooleanU26_t2742_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int8_t p6, int32_t* p7, bool* p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], (bool*)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_CharU26_t2772_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, uint16_t* p4, int8_t p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (uint16_t*)args[3], *((int8_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_CharU26_t2772_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, uint16_t* p3, int8_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (uint16_t*)args[2], *((int8_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_CharU26_t2772_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint16_t* p6, int8_t p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint16_t*)args[5], *((int8_t*)args[6]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_CharU26_t2772_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint16_t* p5, int8_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint16_t*)args[4], *((int8_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, Object_t * p6, DecoderFallbackBuffer_t1594 ** p7, ByteU5BU5D_t698** p8, int8_t p9, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), (Object_t *)args[5], (DecoderFallbackBuffer_t1594 **)args[6], (ByteU5BU5D_t698**)args[7], *((int8_t*)args[8]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, DecoderFallbackBuffer_t1594 ** p6, ByteU5BU5D_t698** p7, int8_t p8, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], (DecoderFallbackBuffer_t1594 **)args[5], (ByteU5BU5D_t698**)args[6], *((int8_t*)args[7]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_Object_t_Int64_t676_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1594 ** p2, ByteU5BU5D_t698** p3, Object_t * p4, int64_t p5, int32_t p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1594 **)args[1], (ByteU5BU5D_t698**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_Object_t_Int64_t676_Int32_t359_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, DecoderFallbackBuffer_t1594 ** p2, ByteU5BU5D_t698** p3, Object_t * p4, int64_t p5, int32_t p6, Object_t * p7, int32_t* p8, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (DecoderFallbackBuffer_t1594 **)args[1], (ByteU5BU5D_t698**)args[2], (Object_t *)args[3], *((int64_t*)args[4]), *((int32_t*)args[5]), (Object_t *)args[6], (int32_t*)args[7], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_UInt32U26_t2777_UInt32U26_t2777_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t p5, uint32_t* p6, uint32_t* p7, Object_t * p8, DecoderFallbackBuffer_t1594 ** p9, ByteU5BU5D_t698** p10, int8_t p11, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int32_t*)args[4]), (uint32_t*)args[5], (uint32_t*)args[6], (Object_t *)args[7], (DecoderFallbackBuffer_t1594 **)args[8], (ByteU5BU5D_t698**)args[9], *((int8_t*)args[10]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_UInt32U26_t2777_UInt32U26_t2777_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int32_t p4, uint32_t* p5, uint32_t* p6, Object_t * p7, DecoderFallbackBuffer_t1594 ** p8, ByteU5BU5D_t698** p9, int8_t p10, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int32_t*)args[3]), (uint32_t*)args[4], (uint32_t*)args[5], (Object_t *)args[6], (DecoderFallbackBuffer_t1594 **)args[7], (ByteU5BU5D_t698**)args[8], *((int8_t*)args[9]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_IntPtr_t_SByte_t1077_Object_t_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, Object_t * p2, bool* p3, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), (Object_t *)args[1], (bool*)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_IntPtr_t_SByte_t1077_SByte_t1077_Object_t_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef IntPtr_t (*Func)(void* obj, int8_t p1, int8_t p2, Object_t * p3, bool* p4, const MethodInfo* method);
	IntPtr_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), (Object_t *)args[2], (bool*)args[3], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_TimeSpan_t969_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t969  p1, TimeSpan_t969  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int64_t676_Int64_t676_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int64_t p1, int64_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_IntPtr_t_Int32_t359_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, IntPtr_t p1, int32_t p2, int8_t p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int8_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Int64_t676_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_IntPtr_t_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, IntPtr_t p1, int32_t p2, int32_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((IntPtr_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Byte_t664_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Byte_t664_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Char_t390_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, float p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, float p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Double_t675_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int16_t1078_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, double p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, float p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_SByte_t1077_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int8_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	int8_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, double p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Single_t358_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef float (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	float ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt16_t1064_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint16_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint16_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt32_t677_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint32_t (*Func)(void* obj, int64_t p1, const MethodInfo* method);
	uint32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int8_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int16_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, double p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, float p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((float*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UInt64_t1076_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint64_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, int32_t p6, int32_t p7, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), *((int32_t*)args[5]), *((int32_t*)args[6]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_SByte_t1077_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int8_t p1, TimeSpan_t969  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int8_t*)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DayOfWeek_t1653 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTimeKind_t1650 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, TimeSpan_t969  p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, double p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DateTime_t546_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTime_t546  p1, DateTime_t546  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((DateTime_t546 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_DateTime_t546_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, DateTime_t546  p1, int32_t p2, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_DateTimeU26_t2809_DateTimeOffsetU26_t2810_SByte_t1077_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, int32_t p3, DateTime_t546 * p4, DateTimeOffset_t1651 * p5, int8_t p6, Exception_t68 ** p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((int32_t*)args[2]), (DateTime_t546 *)args[3], (DateTimeOffset_t1651 *)args[4], *((int8_t*)args[5]), (Exception_t68 **)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Exception_t68 ** p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Exception_t68 **)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, int8_t p5, int8_t p6, int32_t* p7, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Object_t_SByte_t1077_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, Object_t * p4, int8_t p5, int32_t* p6, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Int32_t359_Object_t_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Int32_t359_Object_t_SByte_t1077_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, int32_t* p6, int32_t* p7, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (int32_t*)args[5], (int32_t*)args[6], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, int8_t p4, int32_t* p5, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], *((int8_t*)args[3]), (int32_t*)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_SByte_t1077_DateTimeU26_t2809_DateTimeOffsetU26_t2810_Object_t_Int32_t359_SByte_t1077_BooleanU26_t2742_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int8_t p4, DateTime_t546 * p5, DateTimeOffset_t1651 * p6, Object_t * p7, int32_t p8, int8_t p9, bool* p10, bool* p11, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int8_t*)args[3]), (DateTime_t546 *)args[4], (DateTimeOffset_t1651 *)args[5], (Object_t *)args[6], *((int32_t*)args[7]), *((int8_t*)args[8]), (bool*)args[9], (bool*)args[10], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_Object_t_Object_t_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Int32_t359_DateTimeU26_t2809_SByte_t1077_BooleanU26_t2742_SByte_t1077_ExceptionU26_t2775 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, int32_t p4, DateTime_t546 * p5, int8_t p6, bool* p7, int8_t p8, Exception_t68 ** p9, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], *((int32_t*)args[3]), (DateTime_t546 *)args[4], *((int8_t*)args[5]), (bool*)args[6], *((int8_t*)args[7]), (Exception_t68 **)args[8], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_DateTime_t546_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, DateTime_t546  p1, TimeSpan_t969  p2, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_DateTime_t546_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t546  p1, DateTime_t546  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((DateTime_t546 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_DateTime_t546_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DateTime_t546  p1, TimeSpan_t969  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int64_t676_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int64_t p1, TimeSpan_t969  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int64_t*)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_DateTimeOffset_t1651 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1651  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1651 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_DateTimeOffset_t1651 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1651  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1651 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int16_t p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int16_t*)args[2]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Int16_t1078_Object_t_BooleanU26_t2742_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Int16_t1078_Object_t_BooleanU26_t2742_BooleanU26_t2742_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int16_t p1, Object_t * p2, bool* p3, bool* p4, int8_t p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int16_t*)args[0]), (Object_t *)args[1], (bool*)args[2], (bool*)args[3], *((int8_t*)args[4]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t546_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t546  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_DateTime_t546_Nullable_1_t1757_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, DateTime_t546  p1, Nullable_1_t1757  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((Nullable_1_t1757 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_MonoEnumInfo_t1664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, MonoEnumInfo_t1664  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((MonoEnumInfo_t1664 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_MonoEnumInfoU26_t2811 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, MonoEnumInfo_t1664 * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (MonoEnumInfo_t1664 *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Int16_t1078_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int16_t p1, int16_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int16_t*)args[0]), *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int64_t676_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int64_t p1, int64_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int64_t*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_PlatformID_t1693 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int16_t1078_Int16_t1078_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int16_t p2, int16_t p3, int8_t p4, int8_t p5, int8_t p6, int8_t p7, int8_t p8, int8_t p9, int8_t p10, int8_t p11, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int16_t*)args[1]), *((int16_t*)args[2]), *((int8_t*)args[3]), *((int8_t*)args[4]), *((int8_t*)args[5]), *((int8_t*)args[6]), *((int8_t*)args[7]), *((int8_t*)args[8]), *((int8_t*)args[9]), *((int8_t*)args[10]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Guid_t1673 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1673  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1673 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Guid_t1673 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1673  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1673 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Guid_t1673 (const MethodInfo* method, void* obj, void** args)
{
	typedef Guid_t1673  (*Func)(void* obj, const MethodInfo* method);
	Guid_t1673  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1077_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, int8_t p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), *((int8_t*)args[2]), method);
	return ret;
}

void* RuntimeInvoker_Double_t675_Double_t675_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef double (*Func)(void* obj, double p1, double p2, const MethodInfo* method);
	double ret = ((Func)method->method)(obj, *((double*)args[0]), *((double*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeAttributes_t1352_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_UInt64U2AU26_t2812_Int32U2AU26_t2813_CharU2AU26_t2814_CharU2AU26_t2814_Int64U2AU26_t2815_Int32U2AU26_t2813 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, uint64_t** p1, int32_t** p2, uint16_t** p3, uint16_t** p4, int64_t** p5, int32_t** p6, const MethodInfo* method);
	((Func)method->method)(obj, (uint64_t**)args[0], (int32_t**)args[1], (uint16_t**)args[2], (uint16_t**)args[3], (int64_t**)args[4], (int32_t**)args[5], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int64_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int64_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Double_t675_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, double p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Decimal_t1079  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1079 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_SByte_t1077_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int8_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int16_t1078_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int16_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int16_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Int64_t676_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int64_t p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int64_t*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Single_t358_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, float p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((float*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Double_t675_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, double p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((double*)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Object_t_Decimal_t1079_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Decimal_t1079  p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Decimal_t1079 *)args[1]), (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t358_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Double_t675_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, double p1, Object_t * p2, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((double*)args[0]), (Object_t *)args[1], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Object_t_BooleanU26_t2742_SByte_t1077_Int32U26_t2743_Int32U26_t2743 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, bool* p2, int8_t p3, int32_t* p4, int32_t* p5, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], *((int8_t*)args[2]), (int32_t*)args[3], (int32_t*)args[4], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Object_t_SByte_t1077_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, Object_t * p4, int8_t p5, Object_t * p6, Object_t * p7, Object_t * p8, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), (Object_t *)args[3], *((int8_t*)args[4]), (Object_t *)args[5], (Object_t *)args[6], (Object_t *)args[7], method);
	return ret;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return NULL;
}

void* RuntimeInvoker_Int64_t676_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int64_t (*Func)(void* obj, int32_t p1, int32_t p2, int32_t p3, int32_t p4, int32_t p5, const MethodInfo* method);
	int64_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), *((int32_t*)args[4]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t969_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, TimeSpan_t969  p1, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_TimeSpan_t969_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t969  p1, TimeSpan_t969  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TimeSpan_t969  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TimeSpan_t969  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t969_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, double p1, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((double*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t969_Double_t675_Int64_t676 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, double p1, int64_t p2, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((double*)args[0]), *((int64_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t969_TimeSpan_t969_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, TimeSpan_t969  p1, TimeSpan_t969  p2, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((TimeSpan_t969 *)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t969_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, DateTime_t546  p1, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_DateTime_t546_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTime_t546  p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DateTime_t546_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef DateTime_t546  (*Func)(void* obj, DateTime_t546  p1, const MethodInfo* method);
	DateTime_t546  ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TimeSpan_t969_DateTime_t546_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, DateTime_t546  p1, TimeSpan_t969  p2, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((DateTime_t546 *)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_Int64U5BU5DU26_t2816_StringU5BU5DU26_t2817 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Int64U5BU5D_t1740** p2, StringU5BU5D_t594** p3, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Int64U5BU5D_t1740**)args[1], (StringU5BU5D_t594**)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_ObjectU26_t2797_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ObjectU26_t2797_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t ** p1, Object_t * p2, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t **)args[0], (Object_t *)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Object_t * p1, Object_t * p2, Object_t * p3, Object_t * p4, Object_t * p5, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], (Object_t *)args[2], (Object_t *)args[3], (Object_t *)args[4], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1896 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1896  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1896  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_ObjectU5BU5DU26_t2774_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t77** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t77**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_ObjectU5BU5DU26_t2774_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ObjectU5BU5D_t77** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (ObjectU5BU5D_t77**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_KeyValuePair_2_t1817 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1817  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1817 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_KeyValuePair_2_t1817 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1817  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1817 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1817_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1817  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1817  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1824 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1824  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1824  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1057_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, Object_t * p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1817 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1817  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1817  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1823 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1823  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1823  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, int32_t p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), *((int32_t*)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1842 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1842  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1842  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1955 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1955  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1955  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1956 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1956  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1956  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1952 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1952  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1952  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_FloatTween_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, FloatTween_t163  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((FloatTween_t163 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_ColorTween_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ColorTween_t160  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ColorTween_t160 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_TypeU26_t2818_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_BooleanU26_t2742_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_FillMethodU26_t2819_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_SingleU26_t2755_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_ContentTypeU26_t2820_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_LineTypeU26_t2821_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_InputTypeU26_t2822_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_TouchScreenKeyboardTypeU26_t2823_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_CharacterValidationU26_t2824_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_CharU26_t2772_Int16_t1078 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint16_t* p1, int16_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (uint16_t*)args[0], *((int16_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_DirectionU26_t2825_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_NavigationU26_t2826_Navigation_t247 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Navigation_t247 * p1, Navigation_t247  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Navigation_t247 *)args[0], *((Navigation_t247 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_TransitionU26_t2827_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_ColorBlockU26_t2828_ColorBlock_t174 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ColorBlock_t174 * p1, ColorBlock_t174  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (ColorBlock_t174 *)args[0], *((ColorBlock_t174 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_SpriteStateU26_t2829_SpriteState_t267 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, SpriteState_t267 * p1, SpriteState_t267  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (SpriteState_t267 *)args[0], *((SpriteState_t267 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_DirectionU26_t2830_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_AspectModeU26_t2831_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_FitModeU26_t2832_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_CornerU26_t2833_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_AxisU26_t2834_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector2U26_t2757_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2_t23 * p1, Vector2_t23  p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2_t23 *)args[0], *((Vector2_t23 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_ConstraintU26_t2835_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_SingleU26_t2755_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, float* p1, float p2, const MethodInfo* method);
	((Func)method->method)(obj, (float*)args[0], *((float*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_BooleanU26_t2742_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, bool* p1, int8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (bool*)args[0], *((int8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_TextAnchorU26_t2836_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t* p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (int32_t*)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Object_t_Object_t_Single_t358 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Object_t * p1, Object_t * p2, float p3, const MethodInfo* method);
	((Func)method->method)(obj, (Object_t *)args[0], (Object_t *)args[1], *((float*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1817_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1817  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1817  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_KeyValuePair_2_t1817 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1817  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1817 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t1817 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1817  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1817 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1181_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1181  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Link_t1181  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Link_t1181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Link_t1181  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Link_t1181 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Link_t1181 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Link_t1181  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Link_t1181 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Link_t1181 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Link_t1181  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Link_t1181 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Link_t1181 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Link_t1181  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Link_t1181 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_DictionaryEntry_t1057_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_DictionaryEntry_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, DictionaryEntry_t1057  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((DictionaryEntry_t1057 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_DictionaryEntry_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DictionaryEntry_t1057  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DictionaryEntry_t1057 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DictionaryEntry_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DictionaryEntry_t1057  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DictionaryEntry_t1057 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_DictionaryEntry_t1057 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DictionaryEntry_t1057  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DictionaryEntry_t1057 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Touch_t72 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Touch_t72  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Touch_t72 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Touch_t72 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Touch_t72  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Touch_t72 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Touch_t72 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Touch_t72  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Touch_t72 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Touch_t72 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Touch_t72  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Touch_t72 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Double_t675 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, double p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((double*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Quaternion_t45_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Quaternion_t45  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Quaternion_t45  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Quaternion_t45  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Quaternion_t45  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Quaternion_t45 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Quaternion_t45 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Quaternion_t45  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Quaternion_t45 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_KeyValuePair_2_t1864_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1864  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1864  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_KeyValuePair_2_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1864  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1864 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_KeyValuePair_2_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1864  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1864 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_KeyValuePair_2_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1864  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1864 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1864  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1864 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastResult_t139_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastResult_t139  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastResult_t139  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t139  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t139  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastResult_t139  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastResult_t139 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_RaycastResultU5BU5DU26_t2837_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t1912** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t1912**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_RaycastResultU5BU5DU26_t2837_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastResultU5BU5D_t1912** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (RaycastResultU5BU5D_t1912**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_RaycastResult_t139_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, RaycastResult_t139  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((RaycastResult_t139 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_RaycastResult_t139_RaycastResult_t139_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastResult_t139  p1, RaycastResult_t139  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), *((RaycastResult_t139 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1952_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1952  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t1952  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_KeyValuePair_2_t1952 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t1952  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t1952 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_KeyValuePair_2_t1952 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t1952  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1952 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_KeyValuePair_2_t1952 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t1952  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t1952 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t1952 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t1952  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t1952 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit2D_t369_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t369  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit2D_t369  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_RaycastHit2D_t369 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit2D_t369  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit2D_t369 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_RaycastHit2D_t369 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit2D_t369  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit2D_t369 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_RaycastHit2D_t369 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit2D_t369  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit2D_t369 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_RaycastHit2D_t369 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit2D_t369  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit2D_t369 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_RaycastHit_t100_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t100  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	RaycastHit_t100  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_RaycastHit_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, RaycastHit_t100  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((RaycastHit_t100 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_RaycastHit_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastHit_t100  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastHit_t100 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_RaycastHit_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, RaycastHit_t100  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((RaycastHit_t100 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_RaycastHit_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, RaycastHit_t100  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((RaycastHit_t100 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Vector3_t12_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t12  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector3_t12  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector3_t12 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UIVertex_t239_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t239  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UIVertex_t239  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t239  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t239  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UIVertex_t239  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UIVertex_t239 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UIVertexU5BU5DU26_t2838_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t234** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t234**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UIVertexU5BU5DU26_t2838_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UIVertexU5BU5D_t234** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UIVertexU5BU5D_t234**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_UIVertex_t239_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UIVertex_t239  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UIVertex_t239 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UIVertex_t239_UIVertex_t239_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t239  p1, UIVertex_t239  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), *((UIVertex_t239 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector2_t23  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Vector2_t23  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Vector2_t23 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContentType_t218_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UILineInfo_t394_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t394  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UILineInfo_t394  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfo_t394  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t394  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t394  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UILineInfo_t394  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UILineInfo_t394 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UICharInfo_t396_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t396  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UICharInfo_t396  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfo_t396  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t396  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t396  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UICharInfo_t396  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UICharInfo_t396 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3U5BU5DU26_t2839_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t217** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t217**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector3U5BU5DU26_t2839_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector3U5BU5D_t217** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector3U5BU5D_t217**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Vector3_t12_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector3_t12  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector3_t12 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector3_t12_Vector3_t12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32U5BU5DU26_t2840_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t426** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t426**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32U5BU5DU26_t2840_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Int32U5BU5D_t426** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Int32U5BU5D_t426**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Color32_t347_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t347  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Color32_t347  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32_t347  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Color32_t347 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t347  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t347  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Color32_t347  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Color32_t347 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Color32U5BU5DU26_t2841_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t424** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t424**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Color32U5BU5DU26_t2841_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Color32U5BU5D_t424** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Color32U5BU5D_t424**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Color32_t347_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Color32_t347  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Color32_t347 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Color32_t347_Color32_t347_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t347  p1, Color32_t347  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), *((Color32_t347 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector2U5BU5DU26_t2842_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t216** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t216**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector2U5BU5DU26_t2842_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector2U5BU5D_t216** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector2U5BU5D_t216**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Vector2_t23_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector2_t23  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector2_t23 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector2_t23_Vector2_t23_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4_t317  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Vector4_t317 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Vector4_t317  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t317  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Vector4U5BU5DU26_t2843_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t425** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t425**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Vector4U5BU5DU26_t2843_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Vector4U5BU5D_t425** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (Vector4U5BU5D_t425**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Vector4_t317_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, Vector4_t317  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((Vector4_t317 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector4_t317_Vector4_t317_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t317  p1, Vector4_t317  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Vector4_t317 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcAchievementData_t581_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t581  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcAchievementData_t581  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_GcAchievementData_t581 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, GcAchievementData_t581  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((GcAchievementData_t581 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_GcAchievementData_t581 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcAchievementData_t581  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcAchievementData_t581 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_GcAchievementData_t581 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcAchievementData_t581  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcAchievementData_t581 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_GcAchievementData_t581 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcAchievementData_t581  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcAchievementData_t581 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_GcScoreData_t582_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t582  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	GcScoreData_t582  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_GcScoreData_t582 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, GcScoreData_t582  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((GcScoreData_t582 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_GcScoreData_t582 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, GcScoreData_t582  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((GcScoreData_t582 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_GcScoreData_t582 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, GcScoreData_t582  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((GcScoreData_t582 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_IntPtr_t (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, IntPtr_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((IntPtr_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint_t505_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t505  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint_t505  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ContactPoint_t505 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint_t505  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint_t505 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ContactPoint_t505 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint_t505  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint_t505 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_ContactPoint_t505 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint_t505  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint_t505 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ContactPoint_t505 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint_t505  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint_t505 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ContactPoint2D_t510_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t510  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ContactPoint2D_t510  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ContactPoint2D_t510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ContactPoint2D_t510  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ContactPoint2D_t510 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ContactPoint2D_t510 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ContactPoint2D_t510  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ContactPoint2D_t510 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_ContactPoint2D_t510 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ContactPoint2D_t510  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ContactPoint2D_t510 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ContactPoint2D_t510 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ContactPoint2D_t510  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ContactPoint2D_t510 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Keyframe_t524_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t524  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Keyframe_t524  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Keyframe_t524 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Keyframe_t524  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Keyframe_t524 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Keyframe_t524 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Keyframe_t524  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Keyframe_t524 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Keyframe_t524 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Keyframe_t524  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Keyframe_t524 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Keyframe_t524 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Keyframe_t524  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Keyframe_t524 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CharacterInfo_t533_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t533  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CharacterInfo_t533  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_CharacterInfo_t533 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CharacterInfo_t533  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CharacterInfo_t533 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_CharacterInfo_t533 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CharacterInfo_t533  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CharacterInfo_t533 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_CharacterInfo_t533 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CharacterInfo_t533  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CharacterInfo_t533 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_CharacterInfo_t533 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CharacterInfo_t533  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CharacterInfo_t533 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UICharInfoU5BU5DU26_t2844_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t647** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t647**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UICharInfoU5BU5DU26_t2844_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UICharInfoU5BU5D_t647** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UICharInfoU5BU5D_t647**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_UICharInfo_t396_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UICharInfo_t396  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UICharInfo_t396 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UICharInfo_t396_UICharInfo_t396_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t396  p1, UICharInfo_t396  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), *((UICharInfo_t396 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UILineInfoU5BU5DU26_t2845_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t648** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t648**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_UILineInfoU5BU5DU26_t2845_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UILineInfoU5BU5D_t648** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (UILineInfoU5BU5D_t648**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_UILineInfo_t394_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, UILineInfo_t394  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((UILineInfo_t394 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UILineInfo_t394_UILineInfo_t394_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t394  p1, UILineInfo_t394  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), *((UILineInfo_t394 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ParameterModifier_t1345_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1345  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ParameterModifier_t1345  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ParameterModifier_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ParameterModifier_t1345  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ParameterModifier_t1345 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ParameterModifier_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ParameterModifier_t1345  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ParameterModifier_t1345 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_ParameterModifier_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ParameterModifier_t1345  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ParameterModifier_t1345 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ParameterModifier_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ParameterModifier_t1345  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ParameterModifier_t1345 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_HitInfo_t597_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t597  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	HitInfo_t597  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_HitInfo_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, HitInfo_t597  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((HitInfo_t597 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_HitInfo_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, HitInfo_t597  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((HitInfo_t597 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2270_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2270  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2270  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_KeyValuePair_2_t2270 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2270  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2270 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_KeyValuePair_2_t2270 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2270  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2270 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_KeyValuePair_2_t2270 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2270  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2270 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t2270 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2270  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2270 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TextEditOp_t615_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t833_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2318_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2318  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	KeyValuePair_2_t2318  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_KeyValuePair_2_t2318 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, KeyValuePair_2_t2318  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((KeyValuePair_2_t2318 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_KeyValuePair_2_t2318 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, KeyValuePair_2_t2318  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2318 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_KeyValuePair_2_t2318 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, KeyValuePair_2_t2318  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((KeyValuePair_2_t2318 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t2318 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, KeyValuePair_2_t2318  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((KeyValuePair_2_t2318 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_X509ChainStatus_t966_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t966  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	X509ChainStatus_t966  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_X509ChainStatus_t966 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, X509ChainStatus_t966  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((X509ChainStatus_t966 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_X509ChainStatus_t966 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, X509ChainStatus_t966  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((X509ChainStatus_t966 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_X509ChainStatus_t966 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, X509ChainStatus_t966  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((X509ChainStatus_t966 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_X509ChainStatus_t966 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, X509ChainStatus_t966  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((X509ChainStatus_t966 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, int32_t p3, int32_t p4, Object_t * p5, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), (Object_t *)args[4], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1009_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1009  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Mark_t1009  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Mark_t1009 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Mark_t1009  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Mark_t1009 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Mark_t1009 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Mark_t1009  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Mark_t1009 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Mark_t1009 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Mark_t1009  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Mark_t1009 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Mark_t1009 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Mark_t1009  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Mark_t1009 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_UriScheme_t1046_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1046  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	UriScheme_t1046  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_UriScheme_t1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, UriScheme_t1046  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((UriScheme_t1046 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_UriScheme_t1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UriScheme_t1046  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UriScheme_t1046 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_UriScheme_t1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UriScheme_t1046  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UriScheme_t1046 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_UriScheme_t1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, UriScheme_t1046  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((UriScheme_t1046 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TableRange_t1114_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1114  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TableRange_t1114  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_TableRange_t1114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, TableRange_t1114  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((TableRange_t1114 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_TableRange_t1114 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, TableRange_t1114  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((TableRange_t1114 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_TableRange_t1114 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, TableRange_t1114  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((TableRange_t1114 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_TableRange_t1114 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TableRange_t1114  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TableRange_t1114 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1191_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1191  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1191  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Slot_t1191 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1191  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1191 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Slot_t1191 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1191  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1191 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Slot_t1191 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1191  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1191 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Slot_t1191 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1191  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1191 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Slot_t1199_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1199  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	Slot_t1199  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Slot_t1199 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Slot_t1199  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Slot_t1199 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_Slot_t1199 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Slot_t1199  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Slot_t1199 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Slot_t1199 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Slot_t1199  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Slot_t1199 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Slot_t1199 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Slot_t1199  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Slot_t1199 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ILTokenInfo_t1279_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1279  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ILTokenInfo_t1279  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ILTokenInfo_t1279 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ILTokenInfo_t1279  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ILTokenInfo_t1279 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ILTokenInfo_t1279 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ILTokenInfo_t1279  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ILTokenInfo_t1279 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_ILTokenInfo_t1279 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ILTokenInfo_t1279  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ILTokenInfo_t1279 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ILTokenInfo_t1279 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ILTokenInfo_t1279  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ILTokenInfo_t1279 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelData_t1281_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1281  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelData_t1281  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_LabelData_t1281 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelData_t1281  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelData_t1281 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_LabelData_t1281 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelData_t1281  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelData_t1281 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_LabelData_t1281 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelData_t1281  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelData_t1281 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_LabelData_t1281 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelData_t1281  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelData_t1281 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_LabelFixup_t1280_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1280  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	LabelFixup_t1280  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_LabelFixup_t1280 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, LabelFixup_t1280  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((LabelFixup_t1280 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_LabelFixup_t1280 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, LabelFixup_t1280  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((LabelFixup_t1280 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_LabelFixup_t1280 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, LabelFixup_t1280  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((LabelFixup_t1280 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_LabelFixup_t1280 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, LabelFixup_t1280  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((LabelFixup_t1280 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1326_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1326  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1326  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeTypedArgument_t1326  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeTypedArgument_t1326 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1325_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1325  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1325  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, CustomAttributeNamedArgument_t1325  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((CustomAttributeNamedArgument_t1325 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_CustomAttributeTypedArgumentU5BU5DU26_t2846_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1766** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1766**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_CustomAttributeTypedArgumentU5BU5DU26_t2846_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeTypedArgumentU5BU5D_t1766** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeTypedArgumentU5BU5D_t1766**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_CustomAttributeTypedArgument_t1326_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1326  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1326 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, CustomAttributeTypedArgument_t1326  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), *((CustomAttributeTypedArgument_t1326 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeTypedArgument_t1326  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeTypedArgument_t1326 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_CustomAttributeNamedArgumentU5BU5DU26_t2847_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1767** p1, int32_t p2, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1767**)args[0], *((int32_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_CustomAttributeNamedArgumentU5BU5DU26_t2847_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, CustomAttributeNamedArgumentU5BU5D_t1767** p1, int32_t p2, int32_t p3, const MethodInfo* method);
	((Func)method->method)(obj, (CustomAttributeNamedArgumentU5BU5D_t1767**)args[0], *((int32_t*)args[1]), *((int32_t*)args[2]), method);
	return NULL;
}

void* RuntimeInvoker_Int32_t359_Object_t_CustomAttributeNamedArgument_t1325_Int32_t359_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1325  p2, int32_t p3, int32_t p4, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1325 *)args[1]), *((int32_t*)args[2]), *((int32_t*)args[3]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, CustomAttributeNamedArgument_t1325  p2, Object_t * p3, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), *((CustomAttributeNamedArgument_t1325 *)args[1]), (Object_t *)args[2], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Object_t_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, CustomAttributeNamedArgument_t1325  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((CustomAttributeNamedArgument_t1325 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceInfo_t1356_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1356  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceInfo_t1356  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ResourceInfo_t1356 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceInfo_t1356  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceInfo_t1356 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ResourceInfo_t1356 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceInfo_t1356  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceInfo_t1356 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_ResourceInfo_t1356 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceInfo_t1356  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceInfo_t1356 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ResourceInfo_t1356 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceInfo_t1356  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceInfo_t1356 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_ResourceCacheItem_t1357_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1357  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	ResourceCacheItem_t1357  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_ResourceCacheItem_t1357 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, ResourceCacheItem_t1357  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((ResourceCacheItem_t1357 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Boolean_t360_ResourceCacheItem_t1357 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, ResourceCacheItem_t1357  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1357 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_ResourceCacheItem_t1357 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, ResourceCacheItem_t1357  p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((ResourceCacheItem_t1357 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_ResourceCacheItem_t1357 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, ResourceCacheItem_t1357  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((ResourceCacheItem_t1357 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_DateTime_t546 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, DateTime_t546  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((DateTime_t546 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, Decimal_t1079  p1, const MethodInfo* method);
	((Func)method->method)(obj, *((Decimal_t1079 *)args[0]), method);
	return NULL;
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Decimal_t1079 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, Decimal_t1079  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((Decimal_t1079 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TimeSpan_t969_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef TimeSpan_t969  (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	TimeSpan_t969  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_TimeSpan_t969 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, TimeSpan_t969  p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((TimeSpan_t969 *)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_TypeTag_t1495_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, int32_t p1, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, *((int32_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, uint8_t p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((uint8_t*)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Void_t1083_Int32_t359_Byte_t664 (const MethodInfo* method, void* obj, void** args)
{
	typedef void (*Func)(void* obj, int32_t p1, uint8_t p2, const MethodInfo* method);
	((Func)method->method)(obj, *((int32_t*)args[0]), *((uint8_t*)args[1]), method);
	return NULL;
}

void* RuntimeInvoker_Link_t1181 (const MethodInfo* method, void* obj, void** args)
{
	typedef Link_t1181  (*Func)(void* obj, const MethodInfo* method);
	Link_t1181  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1057_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1817_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1817  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1817  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Touch_t72 (const MethodInfo* method, void* obj, void** args)
{
	typedef Touch_t72  (*Func)(void* obj, const MethodInfo* method);
	Touch_t72  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1864_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1864  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t1864  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1868 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1868  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1868  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1057_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1864 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1864  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t1864  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t1867 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1867  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1867  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1864_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1864  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1864  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastResult_t139_RaycastResult_t139_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t139  p1, RaycastResult_t139  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), *((RaycastResult_t139 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t1914 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t1914  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t1914  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_RaycastResult_t139_RaycastResult_t139 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, RaycastResult_t139  p1, RaycastResult_t139  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), *((RaycastResult_t139 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastResult_t139_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastResult_t139  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastResult_t139 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_KeyValuePair_2_t1952_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1952  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	KeyValuePair_2_t1952  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Int32_t359_ObjectU26_t2797 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int32_t p1, Object_t ** p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t **)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1057_Int32_t359_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, int32_t p1, Object_t * p2, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, *((int32_t*)args[0]), (Object_t *)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t1952_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t1952  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t1952  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_RaycastHit2D_t369 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit2D_t369  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit2D_t369  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_RaycastHit_t100_RaycastHit_t100_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, RaycastHit_t100  p1, RaycastHit_t100  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((RaycastHit_t100 *)args[0]), *((RaycastHit_t100 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_RaycastHit_t100 (const MethodInfo* method, void* obj, void** args)
{
	typedef RaycastHit_t100  (*Func)(void* obj, const MethodInfo* method);
	RaycastHit_t100  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color_t83_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color_t83  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color_t83 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_Single_t358_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, float p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((float*)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Object_t_FloatTween_t163 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, FloatTween_t163  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((FloatTween_t163 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_Object_t_ColorTween_t160 (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, ColorTween_t160  p1, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((ColorTween_t160 *)args[0]), method);
	return ret;
}

void* RuntimeInvoker_UIVertex_t239_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t239  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UIVertex_t239  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2019 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2019  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2019  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef UIVertex_t239  (*Func)(void* obj, const MethodInfo* method);
	UIVertex_t239  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_UIVertex_t239_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UIVertex_t239  p1, UIVertex_t239  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), *((UIVertex_t239 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t239_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t239  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_UIVertex_t239_UIVertex_t239 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UIVertex_t239  p1, UIVertex_t239  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), *((UIVertex_t239 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UIVertex_t239_UIVertex_t239_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UIVertex_t239  p1, UIVertex_t239  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UIVertex_t239 *)args[0]), *((UIVertex_t239 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t394  (*Func)(void* obj, const MethodInfo* method);
	UILineInfo_t394  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t396  (*Func)(void* obj, const MethodInfo* method);
	UICharInfo_t396  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t23_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t23  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Vector3_t12_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Vector3_t12  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Vector3_t12  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2108 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2108  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2108  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Vector3_t12_Vector3_t12 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector3_t12  p1, Vector3_t12  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector3_t12 *)args[0]), *((Vector3_t12 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2118 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2118  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2118  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color32_t347_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t347  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	Color32_t347  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2128 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2128  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2128  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef Color32_t347  (*Func)(void* obj, const MethodInfo* method);
	Color32_t347  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Color32_t347_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Color32_t347  p1, Color32_t347  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), *((Color32_t347 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color32_t347_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t347  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Color32_t347_Color32_t347 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Color32_t347  p1, Color32_t347  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), *((Color32_t347 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Color32_t347_Color32_t347_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Color32_t347  p1, Color32_t347  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Color32_t347 *)args[0]), *((Color32_t347 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2138 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2138  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2138  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Vector2_t23_Vector2_t23 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector2_t23_Vector2_t23_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector2_t23  p1, Vector2_t23  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector2_t23 *)args[0]), *((Vector2_t23 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_Enumerator_t2149 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2149  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2149  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector4_t317_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t317  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_Vector4_t317_Vector4_t317 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Vector4_t317  p1, Vector4_t317  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Vector4_t317 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_Vector4_t317_Vector4_t317_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, Vector4_t317  p1, Vector4_t317  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((Vector4_t317 *)args[0]), *((Vector4_t317 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_GcAchievementData_t581 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcAchievementData_t581  (*Func)(void* obj, const MethodInfo* method);
	GcAchievementData_t581  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_GcScoreData_t582 (const MethodInfo* method, void* obj, void** args)
{
	typedef GcScoreData_t582  (*Func)(void* obj, const MethodInfo* method);
	GcScoreData_t582  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint_t505 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint_t505  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint_t505  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ContactPoint2D_t510 (const MethodInfo* method, void* obj, void** args)
{
	typedef ContactPoint2D_t510  (*Func)(void* obj, const MethodInfo* method);
	ContactPoint2D_t510  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Keyframe_t524 (const MethodInfo* method, void* obj, void** args)
{
	typedef Keyframe_t524  (*Func)(void* obj, const MethodInfo* method);
	Keyframe_t524  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CharacterInfo_t533 (const MethodInfo* method, void* obj, void** args)
{
	typedef CharacterInfo_t533  (*Func)(void* obj, const MethodInfo* method);
	CharacterInfo_t533  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UICharInfo_t396_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UICharInfo_t396  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UICharInfo_t396  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2219 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2219  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2219  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_UICharInfo_t396_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UICharInfo_t396  p1, UICharInfo_t396  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), *((UICharInfo_t396 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t396_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t396  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_UICharInfo_t396_UICharInfo_t396 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UICharInfo_t396  p1, UICharInfo_t396  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), *((UICharInfo_t396 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UICharInfo_t396_UICharInfo_t396_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UICharInfo_t396  p1, UICharInfo_t396  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UICharInfo_t396 *)args[0]), *((UICharInfo_t396 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_UILineInfo_t394_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef UILineInfo_t394  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	UILineInfo_t394  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2228 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2228  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2228  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_UILineInfo_t394_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, UILineInfo_t394  p1, UILineInfo_t394  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), *((UILineInfo_t394 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t394_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t394  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_UILineInfo_t394_UILineInfo_t394 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, UILineInfo_t394  p1, UILineInfo_t394  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), *((UILineInfo_t394 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_UILineInfo_t394_UILineInfo_t394_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, UILineInfo_t394  p1, UILineInfo_t394  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((UILineInfo_t394 *)args[0]), *((UILineInfo_t394 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_ParameterModifier_t1345 (const MethodInfo* method, void* obj, void** args)
{
	typedef ParameterModifier_t1345  (*Func)(void* obj, const MethodInfo* method);
	ParameterModifier_t1345  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_HitInfo_t597 (const MethodInfo* method, void* obj, void** args)
{
	typedef HitInfo_t597  (*Func)(void* obj, const MethodInfo* method);
	HitInfo_t597  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t615_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2270_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2270  (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	KeyValuePair_2_t2270  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t615_Object_t_Int32_t359 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Object_t * p1, int32_t p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int32_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_TextEditOpU26_t2848 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, int32_t* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (int32_t*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2275 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2275  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2275  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2270 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2270  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2270  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TextEditOp_t615 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2274 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2274  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2274  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2270_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2270  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2270  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ClientCertificateType_t833 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2318_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2318  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	KeyValuePair_2_t2318  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Object_t_BooleanU26_t2742 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Object_t * p1, bool* p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, (Object_t *)args[0], (bool*)args[1], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2323 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2323  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2323  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_DictionaryEntry_t1057_Object_t_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef DictionaryEntry_t1057  (*Func)(void* obj, Object_t * p1, int8_t p2, const MethodInfo* method);
	DictionaryEntry_t1057  ret = ((Func)method->method)(obj, (Object_t *)args[0], *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2318 (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2318  (*Func)(void* obj, const MethodInfo* method);
	KeyValuePair_2_t2318  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2322 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2322  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2322  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_KeyValuePair_2_t2318_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef KeyValuePair_2_t2318  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	KeyValuePair_2_t2318  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_SByte_t1077_SByte_t1077 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, int8_t p1, int8_t p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((int8_t*)args[0]), *((int8_t*)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_X509ChainStatus_t966 (const MethodInfo* method, void* obj, void** args)
{
	typedef X509ChainStatus_t966  (*Func)(void* obj, const MethodInfo* method);
	X509ChainStatus_t966  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Mark_t1009 (const MethodInfo* method, void* obj, void** args)
{
	typedef Mark_t1009  (*Func)(void* obj, const MethodInfo* method);
	Mark_t1009  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_UriScheme_t1046 (const MethodInfo* method, void* obj, void** args)
{
	typedef UriScheme_t1046  (*Func)(void* obj, const MethodInfo* method);
	UriScheme_t1046  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TableRange_t1114 (const MethodInfo* method, void* obj, void** args)
{
	typedef TableRange_t1114  (*Func)(void* obj, const MethodInfo* method);
	TableRange_t1114  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1191 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1191  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1191  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Slot_t1199 (const MethodInfo* method, void* obj, void** args)
{
	typedef Slot_t1199  (*Func)(void* obj, const MethodInfo* method);
	Slot_t1199  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ILTokenInfo_t1279 (const MethodInfo* method, void* obj, void** args)
{
	typedef ILTokenInfo_t1279  (*Func)(void* obj, const MethodInfo* method);
	ILTokenInfo_t1279  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelData_t1281 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelData_t1281  (*Func)(void* obj, const MethodInfo* method);
	LabelData_t1281  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_LabelFixup_t1280 (const MethodInfo* method, void* obj, void** args)
{
	typedef LabelFixup_t1280  (*Func)(void* obj, const MethodInfo* method);
	LabelFixup_t1280  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1326  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeTypedArgument_t1326  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1325  (*Func)(void* obj, const MethodInfo* method);
	CustomAttributeNamedArgument_t1325  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_CustomAttributeTypedArgument_t1326_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeTypedArgument_t1326  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeTypedArgument_t1326  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2378 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2378  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2378  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, CustomAttributeTypedArgument_t1326  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), *((CustomAttributeTypedArgument_t1326 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1326_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, CustomAttributeTypedArgument_t1326  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), *((CustomAttributeTypedArgument_t1326 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeTypedArgument_t1326  p1, CustomAttributeTypedArgument_t1326  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeTypedArgument_t1326 *)args[0]), *((CustomAttributeTypedArgument_t1326 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_CustomAttributeNamedArgument_t1325_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef CustomAttributeNamedArgument_t1325  (*Func)(void* obj, Object_t * p1, const MethodInfo* method);
	CustomAttributeNamedArgument_t1325  ret = ((Func)method->method)(obj, (Object_t *)args[0], method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Enumerator_t2389 (const MethodInfo* method, void* obj, void** args)
{
	typedef Enumerator_t2389  (*Func)(void* obj, const MethodInfo* method);
	Enumerator_t2389  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, CustomAttributeNamedArgument_t1325  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), *((CustomAttributeNamedArgument_t1325 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1325_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, Object_t * p2, Object_t * p3, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), (Object_t *)args[1], (Object_t *)args[2], method);
	return ret;
}

void* RuntimeInvoker_Int32_t359_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, CustomAttributeNamedArgument_t1325  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), *((CustomAttributeNamedArgument_t1325 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325_Object_t_Object_t (const MethodInfo* method, void* obj, void** args)
{
	typedef Object_t * (*Func)(void* obj, CustomAttributeNamedArgument_t1325  p1, CustomAttributeNamedArgument_t1325  p2, Object_t * p3, Object_t * p4, const MethodInfo* method);
	Object_t * ret = ((Func)method->method)(obj, *((CustomAttributeNamedArgument_t1325 *)args[0]), *((CustomAttributeNamedArgument_t1325 *)args[1]), (Object_t *)args[2], (Object_t *)args[3], method);
	return ret;
}

void* RuntimeInvoker_ResourceInfo_t1356 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceInfo_t1356  (*Func)(void* obj, const MethodInfo* method);
	ResourceInfo_t1356  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_ResourceCacheItem_t1357 (const MethodInfo* method, void* obj, void** args)
{
	typedef ResourceCacheItem_t1357  (*Func)(void* obj, const MethodInfo* method);
	ResourceCacheItem_t1357  ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_TypeTag_t1495 (const MethodInfo* method, void* obj, void** args)
{
	typedef uint8_t (*Func)(void* obj, const MethodInfo* method);
	uint8_t ret = ((Func)method->method)(obj, method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_DateTimeOffset_t1651_DateTimeOffset_t1651 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, DateTimeOffset_t1651  p1, DateTimeOffset_t1651  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((DateTimeOffset_t1651 *)args[0]), *((DateTimeOffset_t1651 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_DateTimeOffset_t1651_DateTimeOffset_t1651 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, DateTimeOffset_t1651  p1, DateTimeOffset_t1651  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((DateTimeOffset_t1651 *)args[0]), *((DateTimeOffset_t1651 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Nullable_1_t1757 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Nullable_1_t1757  p1, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Nullable_1_t1757 *)args[0]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Int32_t359_Guid_t1673_Guid_t1673 (const MethodInfo* method, void* obj, void** args)
{
	typedef int32_t (*Func)(void* obj, Guid_t1673  p1, Guid_t1673  p2, const MethodInfo* method);
	int32_t ret = ((Func)method->method)(obj, *((Guid_t1673 *)args[0]), *((Guid_t1673 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

void* RuntimeInvoker_Boolean_t360_Guid_t1673_Guid_t1673 (const MethodInfo* method, void* obj, void** args)
{
	typedef bool (*Func)(void* obj, Guid_t1673  p1, Guid_t1673  p2, const MethodInfo* method);
	bool ret = ((Func)method->method)(obj, *((Guid_t1673 *)args[0]), *((Guid_t1673 *)args[1]), method);
	return Box(il2cpp_codegen_class_from_type (method->return_type), &ret);
}

extern const InvokerMethod g_Il2CppInvokerPointers[1524] = 
{
	RuntimeInvoker_Void_t1083,
	RuntimeInvoker_Void_t1083_Object_t,
	RuntimeInvoker_Void_t1083_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t,
	RuntimeInvoker_Object_t_Object_t,
	RuntimeInvoker_Single_t358_Object_t,
	RuntimeInvoker_Single_t358_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Single_t358,
	RuntimeInvoker_Vector3_t12,
	RuntimeInvoker_Void_t1083_Single_t358,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077,
	RuntimeInvoker_Object_t,
	RuntimeInvoker_Boolean_t360,
	RuntimeInvoker_Void_t1083_SByte_t1077,
	RuntimeInvoker_Single_t358,
	RuntimeInvoker_Void_t1083_Vector3_t12,
	RuntimeInvoker_Single_t358_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Single_t358_Single_t358,
	RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Void_t1083_Single_t358_Single_t358,
	RuntimeInvoker_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_RaycastResult_t139_RaycastResult_t139,
	RuntimeInvoker_Boolean_t360_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Object_t,
	RuntimeInvoker_Vector2_t23,
	RuntimeInvoker_Void_t1083_Vector2_t23,
	RuntimeInvoker_MoveDirection_t136,
	RuntimeInvoker_RaycastResult_t139,
	RuntimeInvoker_Void_t1083_RaycastResult_t139,
	RuntimeInvoker_InputButton_t142,
	RuntimeInvoker_RaycastResult_t139_Object_t,
	RuntimeInvoker_MoveDirection_t136_Single_t358_Single_t358,
	RuntimeInvoker_MoveDirection_t136_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Boolean_t360_Int32_t359_PointerEventDataU26_t2741_SByte_t1077,
	RuntimeInvoker_Object_t_Touch_t72_BooleanU26_t2742_BooleanU26_t2742,
	RuntimeInvoker_FramePressState_t143_Int32_t359,
	RuntimeInvoker_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Vector2_t23_Vector2_t23_Single_t358_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_InputMode_t152,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_LayerMask_t155,
	RuntimeInvoker_Void_t1083_LayerMask_t155,
	RuntimeInvoker_Int32_t359_RaycastHit_t100_RaycastHit_t100,
	RuntimeInvoker_Color_t83,
	RuntimeInvoker_Void_t1083_Color_t83,
	RuntimeInvoker_ColorTweenMode_t157,
	RuntimeInvoker_Int32_t359_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_ColorBlock_t174,
	RuntimeInvoker_Object_t_Object_t_Vector2_t23,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Resources_t175,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Object_t_Single_t358,
	RuntimeInvoker_FontStyle_t564,
	RuntimeInvoker_TextAnchor_t412,
	RuntimeInvoker_HorizontalWrapMode_t531,
	RuntimeInvoker_VerticalWrapMode_t532,
	RuntimeInvoker_Boolean_t360_Vector2_t23_Object_t,
	RuntimeInvoker_Vector2_t23_Vector2_t23,
	RuntimeInvoker_Rect_t84,
	RuntimeInvoker_Void_t1083_Color_t83_Single_t358_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Color_t83_Single_t358_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Color_t83_Single_t358,
	RuntimeInvoker_Void_t1083_Single_t358_Single_t358_SByte_t1077,
	RuntimeInvoker_BlockingObjects_t202,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Vector2_t23_Object_t,
	RuntimeInvoker_Type_t208,
	RuntimeInvoker_FillMethod_t209,
	RuntimeInvoker_Vector4_t317_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Color32_t347_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Vector2_t23_Vector2_t23_Color32_t347_Vector2_t23_Vector2_t23,
	RuntimeInvoker_Vector4_t317_Vector4_t317_Rect_t84,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Single_t358_SByte_t1077_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Single_t358_Single_t358_SByte_t1077_Int32_t359,
	RuntimeInvoker_Vector2_t23_Vector2_t23_Rect_t84,
	RuntimeInvoker_ContentType_t218,
	RuntimeInvoker_LineType_t221,
	RuntimeInvoker_InputType_t219,
	RuntimeInvoker_TouchScreenKeyboardType_t391,
	RuntimeInvoker_CharacterValidation_t220,
	RuntimeInvoker_Char_t390,
	RuntimeInvoker_Void_t1083_Int16_t1078,
	RuntimeInvoker_Void_t1083_Int32U26_t2743,
	RuntimeInvoker_Int32_t359_Vector2_t23_Object_t,
	RuntimeInvoker_Int32_t359_Vector2_t23,
	RuntimeInvoker_EditState_t225_Object_t,
	RuntimeInvoker_Boolean_t360_Int16_t1078,
	RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Int32_t359_Int32_t359_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Vector2_t23,
	RuntimeInvoker_Single_t358_Int32_t359_Object_t,
	RuntimeInvoker_Char_t390_Object_t_Int32_t359_Int16_t1078,
	RuntimeInvoker_Void_t1083_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int16_t1078_Object_t_Object_t,
	RuntimeInvoker_Char_t390_Object_t,
	RuntimeInvoker_Void_t1083_Rect_t84_SByte_t1077,
	RuntimeInvoker_Mode_t246,
	RuntimeInvoker_Navigation_t247,
	RuntimeInvoker_Void_t1083_Rect_t84,
	RuntimeInvoker_Direction_t253,
	RuntimeInvoker_Void_t1083_Single_t358_SByte_t1077,
	RuntimeInvoker_Axis_t255,
	RuntimeInvoker_MovementType_t259,
	RuntimeInvoker_ScrollbarVisibility_t260,
	RuntimeInvoker_Void_t1083_Single_t358_Int32_t359,
	RuntimeInvoker_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Bounds_t264,
	RuntimeInvoker_Void_t1083_Navigation_t247,
	RuntimeInvoker_Transition_t265,
	RuntimeInvoker_Void_t1083_ColorBlock_t174,
	RuntimeInvoker_SpriteState_t267,
	RuntimeInvoker_Void_t1083_SpriteState_t267,
	RuntimeInvoker_SelectionState_t266,
	RuntimeInvoker_Object_t_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Object_t_Vector2_t23,
	RuntimeInvoker_Void_t1083_Color_t83_SByte_t1077,
	RuntimeInvoker_Boolean_t360_ColorU26_t2744_Color_t83,
	RuntimeInvoker_Direction_t271,
	RuntimeInvoker_Axis_t273,
	RuntimeInvoker_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_TextGenerationSettings_t351_Vector2_t23,
	RuntimeInvoker_Vector2_t23_Int32_t359,
	RuntimeInvoker_Rect_t84_Object_t_BooleanU26_t2742,
	RuntimeInvoker_Rect_t84_Rect_t84_Rect_t84,
	RuntimeInvoker_Rect_t84_Object_t_Object_t,
	RuntimeInvoker_AspectMode_t286,
	RuntimeInvoker_Single_t358_Single_t358_Int32_t359,
	RuntimeInvoker_ScaleMode_t288,
	RuntimeInvoker_ScreenMatchMode_t289,
	RuntimeInvoker_Unit_t290,
	RuntimeInvoker_FitMode_t292,
	RuntimeInvoker_Corner_t294,
	RuntimeInvoker_Axis_t295,
	RuntimeInvoker_Constraint_t296,
	RuntimeInvoker_Single_t358_Int32_t359,
	RuntimeInvoker_Single_t358_Int32_t359_Single_t358,
	RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Single_t358_Single_t358,
	RuntimeInvoker_Single_t358_Object_t_Int32_t359,
	RuntimeInvoker_Single_t358_Object_t_Object_t_Single_t358,
	RuntimeInvoker_Single_t358_Object_t_Object_t_Single_t358_ILayoutElementU26_t2745,
	RuntimeInvoker_Void_t1083_UIVertexU26_t2746_Int32_t359,
	RuntimeInvoker_Void_t1083_UIVertex_t239_Int32_t359,
	RuntimeInvoker_Void_t1083_Vector3_t12_Color32_t347_Vector2_t23_Vector2_t23_Vector3_t12_Vector4_t317,
	RuntimeInvoker_Void_t1083_Vector3_t12_Color32_t347_Vector2_t23,
	RuntimeInvoker_Void_t1083_UIVertex_t239,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Color32_t347_Int32_t359_Int32_t359_Single_t358_Single_t358,
	RuntimeInvoker_Void_t1083_Object_t_Double_t675,
	RuntimeInvoker_Void_t1083_Int64_t676_Object_t,
	RuntimeInvoker_Void_t1083_GcAchievementDescriptionData_t580_Int32_t359,
	RuntimeInvoker_Void_t1083_GcUserProfileData_t579_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Double_t675_Object_t,
	RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_UserProfileU5BU5DU26_t2747_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_UserProfileU5BU5DU26_t2747_Int32_t359,
	RuntimeInvoker_Void_t1083_GcScoreData_t582,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_BoneWeight_t455_BoneWeight_t455,
	RuntimeInvoker_Void_t1083_InternalDrawTextureArgumentsU26_t2748,
	RuntimeInvoker_Object_t_Object_t_Vector3U26_t2749,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_IntPtr_t,
	RuntimeInvoker_Color_t83_Single_t358_Single_t358,
	RuntimeInvoker_Void_t1083_Object_t_IntPtr_t_Int32_t359,
	RuntimeInvoker_Void_t1083_CullingGroupEvent_t463,
	RuntimeInvoker_Object_t_CullingGroupEvent_t463_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Color_t83_Single_t358,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Object_t,
	RuntimeInvoker_Void_t1083_TouchScreenKeyboard_InternalConstructorHelperArgumentsU26_t2750_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Object_t,
	RuntimeInvoker_Int32_t359_LayerMask_t155,
	RuntimeInvoker_LayerMask_t155_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Single_t358,
	RuntimeInvoker_Vector2_t23_Vector2_t23_Vector2_t23,
	RuntimeInvoker_Single_t358_Vector2_t23_Vector2_t23,
	RuntimeInvoker_Single_t358_Vector2_t23,
	RuntimeInvoker_Vector2_t23_Vector2_t23_Single_t358,
	RuntimeInvoker_Boolean_t360_Vector2_t23_Vector2_t23,
	RuntimeInvoker_Vector2_t23_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Vector2_t23,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Single_t358,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Vector3U26_t2749_Single_t358,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12_Vector3U26_t2749_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Single_t358_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Single_t358,
	RuntimeInvoker_Single_t358_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Vector3_t12_Single_t358_Vector3_t12,
	RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Color_t83_Color_t83_Color_t83_Single_t358,
	RuntimeInvoker_Color_t83_Color_t83_Single_t358,
	RuntimeInvoker_Vector4_t317_Color_t83,
	RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Color32_t347_Color_t83,
	RuntimeInvoker_Color_t83_Color32_t347,
	RuntimeInvoker_Single_t358_Quaternion_t45_Quaternion_t45,
	RuntimeInvoker_Quaternion_t45_Single_t358_Vector3_t12,
	RuntimeInvoker_Quaternion_t45_Single_t358_Vector3U26_t2749,
	RuntimeInvoker_Quaternion_t45_Vector3_t12,
	RuntimeInvoker_Quaternion_t45_Vector3U26_t2749_Vector3U26_t2749,
	RuntimeInvoker_Quaternion_t45_Quaternion_t45,
	RuntimeInvoker_Quaternion_t45_QuaternionU26_t2751,
	RuntimeInvoker_Quaternion_t45_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Vector3_t12_Quaternion_t45,
	RuntimeInvoker_Vector3_t12_QuaternionU26_t2751,
	RuntimeInvoker_Quaternion_t45_Vector3U26_t2749,
	RuntimeInvoker_Quaternion_t45_Quaternion_t45_Quaternion_t45,
	RuntimeInvoker_Vector3_t12_Quaternion_t45_Vector3_t12,
	RuntimeInvoker_Boolean_t360_Quaternion_t45_Quaternion_t45,
	RuntimeInvoker_Boolean_t360_Vector3_t12,
	RuntimeInvoker_Boolean_t360_Rect_t84,
	RuntimeInvoker_Boolean_t360_Rect_t84_Rect_t84,
	RuntimeInvoker_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Single_t358,
	RuntimeInvoker_Matrix4x4_t401_Matrix4x4_t401,
	RuntimeInvoker_Matrix4x4_t401_Matrix4x4U26_t2752,
	RuntimeInvoker_Boolean_t360_Matrix4x4_t401_Matrix4x4U26_t2752,
	RuntimeInvoker_Boolean_t360_Matrix4x4U26_t2752_Matrix4x4U26_t2752,
	RuntimeInvoker_Matrix4x4_t401,
	RuntimeInvoker_Vector4_t317_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Vector4_t317,
	RuntimeInvoker_Matrix4x4_t401_Vector3_t12,
	RuntimeInvoker_Void_t1083_Vector3_t12_Quaternion_t45_Vector3_t12,
	RuntimeInvoker_Matrix4x4_t401_Vector3_t12_Quaternion_t45_Vector3_t12,
	RuntimeInvoker_Matrix4x4_t401_Vector3U26_t2749_QuaternionU26_t2751_Vector3U26_t2749,
	RuntimeInvoker_Matrix4x4_t401_Single_t358_Single_t358_Single_t358_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Matrix4x4_t401_Single_t358_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Matrix4x4_t401_Matrix4x4_t401_Matrix4x4_t401,
	RuntimeInvoker_Vector4_t317_Matrix4x4_t401_Vector4_t317,
	RuntimeInvoker_Boolean_t360_Matrix4x4_t401_Matrix4x4_t401,
	RuntimeInvoker_Void_t1083_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Void_t1083_Bounds_t264,
	RuntimeInvoker_Boolean_t360_Bounds_t264,
	RuntimeInvoker_Boolean_t360_Bounds_t264_Vector3_t12,
	RuntimeInvoker_Boolean_t360_BoundsU26_t2753_Vector3U26_t2749,
	RuntimeInvoker_Single_t358_Bounds_t264_Vector3_t12,
	RuntimeInvoker_Single_t358_BoundsU26_t2753_Vector3U26_t2749,
	RuntimeInvoker_Boolean_t360_RayU26_t2754_BoundsU26_t2753_SingleU26_t2755,
	RuntimeInvoker_Boolean_t360_Ray_t365,
	RuntimeInvoker_Boolean_t360_Ray_t365_SingleU26_t2755,
	RuntimeInvoker_Vector3_t12_BoundsU26_t2753_Vector3U26_t2749,
	RuntimeInvoker_Boolean_t360_Bounds_t264_Bounds_t264,
	RuntimeInvoker_Single_t358_Vector4_t317_Vector4_t317,
	RuntimeInvoker_Single_t358_Vector4_t317,
	RuntimeInvoker_Vector4_t317,
	RuntimeInvoker_Vector4_t317_Vector4_t317_Vector4_t317,
	RuntimeInvoker_Vector4_t317_Vector4_t317_Single_t358,
	RuntimeInvoker_Boolean_t360_Vector4_t317_Vector4_t317,
	RuntimeInvoker_Vector3_t12_Single_t358,
	RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Single_t358,
	RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Single_t358_Single_t358,
	RuntimeInvoker_Single_t358_Single_t358_Single_t358_SingleU26_t2755_Single_t358,
	RuntimeInvoker_Single_t358_Single_t358_Single_t358_SingleU26_t2755_Single_t358_Single_t358_Single_t358,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_RectU26_t2756,
	RuntimeInvoker_Void_t1083_Vector2U26_t2757,
	RuntimeInvoker_Void_t1083_Int32_t359_Single_t358_Single_t358,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_SphericalHarmonicsL2U26_t2758,
	RuntimeInvoker_Void_t1083_Color_t83_SphericalHarmonicsL2U26_t2758,
	RuntimeInvoker_Void_t1083_ColorU26_t2744_SphericalHarmonicsL2U26_t2758,
	RuntimeInvoker_Void_t1083_Vector3_t12_Color_t83_Single_t358,
	RuntimeInvoker_Void_t1083_Vector3_t12_Color_t83_SphericalHarmonicsL2U26_t2758,
	RuntimeInvoker_Void_t1083_Vector3U26_t2749_ColorU26_t2744_SphericalHarmonicsL2U26_t2758,
	RuntimeInvoker_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481_Single_t358,
	RuntimeInvoker_SphericalHarmonicsL2_t481_Single_t358_SphericalHarmonicsL2_t481,
	RuntimeInvoker_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481,
	RuntimeInvoker_Boolean_t360_SphericalHarmonicsL2_t481_SphericalHarmonicsL2_t481,
	RuntimeInvoker_Void_t1083_Vector4U26_t2759,
	RuntimeInvoker_Vector4_t317_Object_t,
	RuntimeInvoker_Vector2_t23_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Vector2U26_t2757,
	RuntimeInvoker_RuntimePlatform_t437,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_CameraClearFlags_t585,
	RuntimeInvoker_Vector3_t12_Object_t_Vector3U26_t2749,
	RuntimeInvoker_Ray_t365_Vector3_t12,
	RuntimeInvoker_Ray_t365_Object_t_Vector3U26_t2749,
	RuntimeInvoker_Object_t_Ray_t365_Single_t358_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_RayU26_t2754_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_RayU26_t2754_Single_t358_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_IntPtr_t,
	RuntimeInvoker_RenderBuffer_t584,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_Void_t1083_IntPtr_t_RenderBufferU26_t2760_RenderBufferU26_t2760,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_TouchPhase_t492,
	RuntimeInvoker_Void_t1083_Vector3U26_t2749,
	RuntimeInvoker_Touch_t72_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t,
	RuntimeInvoker_IntPtr_t,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Object_t,
	RuntimeInvoker_Quaternion_t45,
	RuntimeInvoker_Void_t1083_Quaternion_t45,
	RuntimeInvoker_Void_t1083_QuaternionU26_t2751,
	RuntimeInvoker_Void_t1083_Matrix4x4U26_t2752,
	RuntimeInvoker_Void_t1083_Vector3_t12_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_SByte_t1077_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12_RaycastHitU26_t2761_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Ray_t365_RaycastHitU26_t2761_Single_t358_Int32_t359,
	RuntimeInvoker_Boolean_t360_Ray_t365_RaycastHitU26_t2761_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Ray_t365_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Vector3U26_t2749_Vector3U26_t2749_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12_RaycastHitU26_t2761,
	RuntimeInvoker_Boolean_t360_Vector3_t12_Vector3_t12_RaycastHitU26_t2761_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Vector3U26_t2749_Vector3U26_t2749_RaycastHitU26_t2761_Single_t358_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Vector3U26_t2749_Int32_t359,
	RuntimeInvoker_Boolean_t360_WheelHitU26_t2762,
	RuntimeInvoker_Void_t1083_Vector3U26_t2749_QuaternionU26_t2751,
	RuntimeInvoker_Void_t1083_Vector2_t23_Vector2_t23_Single_t358_Int32_t359_Single_t358_Single_t358_RaycastHit2DU26_t2763,
	RuntimeInvoker_Void_t1083_Vector2U26_t2757_Vector2U26_t2757_Single_t358_Int32_t359_Single_t358_Single_t358_RaycastHit2DU26_t2763,
	RuntimeInvoker_RaycastHit2D_t369_Vector2_t23_Vector2_t23_Single_t358_Int32_t359,
	RuntimeInvoker_RaycastHit2D_t369_Vector2_t23_Vector2_t23_Single_t358_Int32_t359_Single_t358_Single_t358,
	RuntimeInvoker_Object_t_Vector2_t23_Vector2_t23_Single_t358_Int32_t359,
	RuntimeInvoker_Object_t_Vector2U26_t2757_Vector2U26_t2757_Single_t358_Int32_t359_Single_t358_Single_t358,
	RuntimeInvoker_Object_t_SByte_t1077_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Int64_t676,
	RuntimeInvoker_SendMessageOptions_t435,
	RuntimeInvoker_AnimatorStateInfo_t522,
	RuntimeInvoker_AnimatorClipInfo_t523,
	RuntimeInvoker_Boolean_t360_Int16_t1078_CharacterInfoU26_t2764_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int16_t1078_CharacterInfoU26_t2764_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int16_t1078_CharacterInfoU26_t2764,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Color_t83_Int32_t359_Single_t358_Single_t358_Int32_t359_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_Vector2_t23_Vector2_t23_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Color_t83_Int32_t359_Single_t358_Single_t358_Int32_t359_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_Single_t358_Single_t358_Single_t358_Single_t358_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_ColorU26_t2744_Int32_t359_Single_t358_Single_t358_Int32_t359_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_Single_t358_Single_t358_Single_t358_Single_t358_SByte_t1077,
	RuntimeInvoker_TextGenerationSettings_t351_TextGenerationSettings_t351,
	RuntimeInvoker_Single_t358_Object_t_TextGenerationSettings_t351,
	RuntimeInvoker_Boolean_t360_Object_t_TextGenerationSettings_t351,
	RuntimeInvoker_RenderMode_t537,
	RuntimeInvoker_Void_t1083_Object_t_ColorU26_t2744,
	RuntimeInvoker_Void_t1083_Object_t_RectU26_t2756,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Vector2_t23_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Vector2U26_t2757_Object_t,
	RuntimeInvoker_Vector2_t23_Vector2_t23_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Vector2_t23_Object_t_Object_t_Vector2U26_t2757,
	RuntimeInvoker_Void_t1083_Vector2U26_t2757_Object_t_Object_t_Vector2U26_t2757,
	RuntimeInvoker_Boolean_t360_Object_t_Vector2_t23_Object_t_Vector3U26_t2749,
	RuntimeInvoker_Boolean_t360_Object_t_Vector2_t23_Object_t_Vector2U26_t2757,
	RuntimeInvoker_Ray_t365_Object_t_Vector2_t23,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Ray_t365,
	RuntimeInvoker_Void_t1083_Ray_t365,
	RuntimeInvoker_EventType_t539,
	RuntimeInvoker_EventType_t539_Int32_t359,
	RuntimeInvoker_EventModifiers_t540,
	RuntimeInvoker_KeyCode_t538,
	RuntimeInvoker_Void_t1083_DateTime_t546,
	RuntimeInvoker_Void_t1083_Rect_t84_Object_t,
	RuntimeInvoker_Void_t1083_Rect_t84_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Rect_t84_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_Rect_t84_Object_t_Int32_t359_SByte_t1077_Single_t358,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_Int32_t359_Single_t358_Single_t358_Object_t,
	RuntimeInvoker_Void_t1083_ColorU26_t2744,
	RuntimeInvoker_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Rect_t84_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Rect_t84,
	RuntimeInvoker_Void_t1083_Int32_t359_RectU26_t2756,
	RuntimeInvoker_Void_t1083_Single_t358_Single_t358_Single_t358_Single_t358_Object_t,
	RuntimeInvoker_IntPtr_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_Int32_t359_Object_t,
	RuntimeInvoker_UserState_t601,
	RuntimeInvoker_Void_t1083_Object_t_Double_t675_SByte_t1077_SByte_t1077_DateTime_t546,
	RuntimeInvoker_Double_t675,
	RuntimeInvoker_Void_t1083_Double_t675,
	RuntimeInvoker_DateTime_t546,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1077_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Object_t_DateTime_t546_Object_t_Int32_t359,
	RuntimeInvoker_Int64_t676,
	RuntimeInvoker_UserScope_t602,
	RuntimeInvoker_Range_t595,
	RuntimeInvoker_Void_t1083_Range_t595,
	RuntimeInvoker_TimeScope_t603,
	RuntimeInvoker_Void_t1083_Int32_t359_HitInfo_t597,
	RuntimeInvoker_Boolean_t360_HitInfo_t597_HitInfo_t597,
	RuntimeInvoker_Boolean_t360_HitInfo_t597,
	RuntimeInvoker_Void_t1083_Object_t_StringU26_t2765_StringU26_t2765,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_StreamingContext_t653,
	RuntimeInvoker_Boolean_t360_Color_t83_Color_t83,
	RuntimeInvoker_Boolean_t360_TextGenerationSettings_t351,
	RuntimeInvoker_PersistentListenerMode_t618,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_Object_t_Object_t,
	RuntimeInvoker_UInt32_t677_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_UInt32_t677_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359,
	RuntimeInvoker_Sign_t720_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_ConfidenceFactor_t724,
	RuntimeInvoker_Void_t1083_SByte_t1077_Object_t,
	RuntimeInvoker_Byte_t664,
	RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_ByteU26_t2766_Int32U26_t2743_ByteU5BU5DU26_t2767,
	RuntimeInvoker_DateTime_t546_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_DSAParameters_t859,
	RuntimeInvoker_RSAParameters_t831_SByte_t1077,
	RuntimeInvoker_Void_t1083_RSAParameters_t831,
	RuntimeInvoker_Object_t_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t859_BooleanU26_t2742,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Boolean_t360_DateTime_t546,
	RuntimeInvoker_X509ChainStatusFlags_t761,
	RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Byte_t664,
	RuntimeInvoker_Void_t1083_Byte_t664_Byte_t664,
	RuntimeInvoker_AlertLevel_t780,
	RuntimeInvoker_AlertDescription_t781,
	RuntimeInvoker_Object_t_Byte_t664,
	RuntimeInvoker_Void_t1083_Int16_t1078_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Int16_t1078_SByte_t1077_SByte_t1077,
	RuntimeInvoker_CipherAlgorithmType_t783,
	RuntimeInvoker_HashAlgorithmType_t802,
	RuntimeInvoker_ExchangeAlgorithmType_t800,
	RuntimeInvoker_CipherMode_t711,
	RuntimeInvoker_Int16_t1078,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int16_t1078,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_ByteU5BU5DU26_t2767_ByteU5BU5DU26_t2767,
	RuntimeInvoker_Object_t_Byte_t664_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Int16_t1078,
	RuntimeInvoker_Int32_t359_Int16_t1078,
	RuntimeInvoker_Object_t_Int16_t1078_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_Int16_t1078_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_SecurityProtocolType_t817,
	RuntimeInvoker_SecurityCompressionType_t816,
	RuntimeInvoker_HandshakeType_t834,
	RuntimeInvoker_HandshakeState_t801,
	RuntimeInvoker_UInt64_t1076,
	RuntimeInvoker_SecurityProtocolType_t817_Int16_t1078,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t664_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t664_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Byte_t664_Object_t,
	RuntimeInvoker_Object_t_Byte_t664_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Int64_t676_Int64_t676_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Byte_t664_Byte_t664_Object_t,
	RuntimeInvoker_RSAParameters_t831,
	RuntimeInvoker_Void_t1083_Object_t_Byte_t664,
	RuntimeInvoker_Void_t1083_Object_t_Byte_t664_Byte_t664,
	RuntimeInvoker_Void_t1083_Object_t_Byte_t664_Object_t,
	RuntimeInvoker_ContentType_t795,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_DictionaryNodeU26_t2768,
	RuntimeInvoker_DictionaryEntry_t1057,
	RuntimeInvoker_EditorBrowsableState_t921,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Int16_t1078_Int16_t1078,
	RuntimeInvoker_Boolean_t360_Object_t_IPAddressU26_t2769,
	RuntimeInvoker_AddressFamily_t926,
	RuntimeInvoker_Object_t_Int64_t676,
	RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743,
	RuntimeInvoker_Boolean_t360_Object_t_IPv6AddressU26_t2770,
	RuntimeInvoker_UInt16_t1064_Int16_t1078,
	RuntimeInvoker_SecurityProtocolType_t941,
	RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_Int32_t359_SByte_t1077,
	RuntimeInvoker_AsnDecodeStatus_t982_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_SByte_t1077,
	RuntimeInvoker_X509ChainStatusFlags_t970_Object_t,
	RuntimeInvoker_X509ChainStatusFlags_t970_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_X509ChainStatusFlags_t970_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_X509ChainStatusFlags_t970,
	RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_Int32_t359_Int32_t359,
	RuntimeInvoker_X509RevocationFlag_t977,
	RuntimeInvoker_X509RevocationMode_t978,
	RuntimeInvoker_X509VerificationFlags_t981,
	RuntimeInvoker_X509KeyUsageFlags_t975,
	RuntimeInvoker_X509KeyUsageFlags_t975_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Byte_t664_Int16_t1078,
	RuntimeInvoker_Byte_t664_Int16_t1078_Int16_t1078,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_RegexOptions_t994,
	RuntimeInvoker_Category_t1001_Object_t,
	RuntimeInvoker_Boolean_t360_UInt16_t1064_Int16_t1078,
	RuntimeInvoker_Boolean_t360_Int32_t359_Int16_t1078,
	RuntimeInvoker_Void_t1083_Int16_t1078_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_UInt16_t1064_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int16_t1078_Int16_t1078_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int16_t1078_Object_t_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_UInt16_t1064,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_SByte_t1077_Object_t,
	RuntimeInvoker_Void_t1083_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_SByte_t1077_Int32_t359_Object_t,
	RuntimeInvoker_UInt16_t1064_UInt16_t1064_UInt16_t1064,
	RuntimeInvoker_OpFlags_t996_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_UInt16_t1064_UInt16_t1064,
	RuntimeInvoker_Boolean_t360_Int32_t359_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int32_t359_Int32U26_t2743_Int32U26_t2743_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Boolean_t360_UInt16_t1064_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int32_t359_Int32_t359_SByte_t1077_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_SByte_t1077_Int32_t359,
	RuntimeInvoker_Interval_t1016,
	RuntimeInvoker_Boolean_t360_Interval_t1016,
	RuntimeInvoker_Void_t1083_Interval_t1016,
	RuntimeInvoker_Interval_t1016_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Double_t675_Interval_t1016,
	RuntimeInvoker_Object_t_Interval_t1016_Object_t_Object_t,
	RuntimeInvoker_Double_t675_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32U26_t2743,
	RuntimeInvoker_Int32_t359_Object_t_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Int32U26_t2743,
	RuntimeInvoker_Object_t_RegexOptionsU26_t2771,
	RuntimeInvoker_Void_t1083_RegexOptionsU26_t2771_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Int32U26_t2743_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Category_t1001,
	RuntimeInvoker_Int32_t359_Int16_t1078_Int32_t359_Int32_t359,
	RuntimeInvoker_Char_t390_Int16_t1078,
	RuntimeInvoker_Int32_t359_Int32U26_t2743,
	RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_UInt16_t1064_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int16_t1078_Int16_t1078,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_UInt16_t1064,
	RuntimeInvoker_Position_t997,
	RuntimeInvoker_UriHostNameType_t1049_Object_t,
	RuntimeInvoker_Void_t1083_StringU26_t2765,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Char_t390_Object_t_Int32U26_t2743_CharU26_t2772,
	RuntimeInvoker_Void_t1083_Object_t_UriFormatExceptionU26_t2773,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_ObjectU5BU5DU26_t2774,
	RuntimeInvoker_Int32_t359_Object_t_ObjectU5BU5DU26_t2774,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Byte_t664_Object_t,
	RuntimeInvoker_Decimal_t1079_Object_t,
	RuntimeInvoker_Int16_t1078_Object_t,
	RuntimeInvoker_Int64_t676_Object_t,
	RuntimeInvoker_SByte_t1077_Object_t,
	RuntimeInvoker_UInt16_t1064_Object_t,
	RuntimeInvoker_UInt32_t677_Object_t,
	RuntimeInvoker_UInt64_t1076_Object_t,
	RuntimeInvoker_Boolean_t360_SByte_t1077_Object_t_Int32_t359_ExceptionU26_t2775,
	RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_Int32U26_t2743_ExceptionU26_t2775,
	RuntimeInvoker_Boolean_t360_Int32_t359_SByte_t1077_ExceptionU26_t2775,
	RuntimeInvoker_Boolean_t360_Int32U26_t2743_Object_t_SByte_t1077_SByte_t1077_ExceptionU26_t2775,
	RuntimeInvoker_Void_t1083_Int32U26_t2743_Object_t_Object_t_BooleanU26_t2742_BooleanU26_t2742,
	RuntimeInvoker_Void_t1083_Int32U26_t2743_Object_t_Object_t_BooleanU26_t2742,
	RuntimeInvoker_Boolean_t360_Int32U26_t2743_Object_t_Int32U26_t2743_SByte_t1077_ExceptionU26_t2775,
	RuntimeInvoker_Boolean_t360_Int32U26_t2743_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Int16_t1078_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_Int32U26_t2743_ExceptionU26_t2775,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_Int32U26_t2743,
	RuntimeInvoker_TypeCode_t1704,
	RuntimeInvoker_Int32_t359_Int64_t676,
	RuntimeInvoker_Boolean_t360_Int64_t676,
	RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_Int64U26_t2776_ExceptionU26_t2775,
	RuntimeInvoker_Int64_t676_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_Int64U26_t2776_ExceptionU26_t2775,
	RuntimeInvoker_Int64_t676_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int64U26_t2776,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_Int64U26_t2776,
	RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_UInt32U26_t2777_ExceptionU26_t2775,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_UInt32U26_t2777_ExceptionU26_t2775,
	RuntimeInvoker_UInt32_t677_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_UInt32_t677_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_UInt32U26_t2777,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_UInt32U26_t2777,
	RuntimeInvoker_UInt64_t1076_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_UInt64U26_t2778_ExceptionU26_t2775,
	RuntimeInvoker_UInt64_t1076_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_UInt64U26_t2778,
	RuntimeInvoker_Int32_t359_SByte_t1077,
	RuntimeInvoker_Boolean_t360_SByte_t1077,
	RuntimeInvoker_Byte_t664_Object_t_Object_t,
	RuntimeInvoker_Byte_t664_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_ByteU26_t2766,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_ByteU26_t2766,
	RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_SByteU26_t2779_ExceptionU26_t2775,
	RuntimeInvoker_SByte_t1077_Object_t_Object_t,
	RuntimeInvoker_SByte_t1077_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_SByteU26_t2779,
	RuntimeInvoker_Boolean_t360_Object_t_SByte_t1077_Int16U26_t2780_ExceptionU26_t2775,
	RuntimeInvoker_Int16_t1078_Object_t_Object_t,
	RuntimeInvoker_Int16_t1078_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int16U26_t2780,
	RuntimeInvoker_UInt16_t1064_Object_t_Object_t,
	RuntimeInvoker_UInt16_t1064_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_UInt16U26_t2781,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_UInt16U26_t2781,
	RuntimeInvoker_Void_t1083_ByteU2AU26_t2782_ByteU2AU26_t2782_DoubleU2AU26_t2783_UInt16U2AU26_t2784_UInt16U2AU26_t2784_UInt16U2AU26_t2784_UInt16U2AU26_t2784,
	RuntimeInvoker_UnicodeCategory_t1227_Int16_t1078,
	RuntimeInvoker_Char_t390_Int16_t1078_Object_t,
	RuntimeInvoker_Void_t1083_Int16_t1078_Int32_t359,
	RuntimeInvoker_Char_t390_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_SByte_t1077_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359_SByte_t1077_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Int16_t1078_Int32_t359,
	RuntimeInvoker_Object_t_Int32_t359_Int16_t1078,
	RuntimeInvoker_Object_t_Int16_t1078_Int16_t1078,
	RuntimeInvoker_Void_t1083_Object_t_Int32U26_t2743_Int32U26_t2743_Int32U26_t2743_BooleanU26_t2742_StringU26_t2765,
	RuntimeInvoker_Void_t1083_Int32_t359_Int16_t1078,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Object_t_Int16_t1078_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Single_t358,
	RuntimeInvoker_Single_t358_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Double_t675,
	RuntimeInvoker_Boolean_t360_Double_t675,
	RuntimeInvoker_Double_t675_Object_t_Object_t,
	RuntimeInvoker_Double_t675_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_DoubleU26_t2785_ExceptionU26_t2775,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_DoubleU26_t2785,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Object_t_Decimal_t1079,
	RuntimeInvoker_Decimal_t1079_Decimal_t1079_Decimal_t1079,
	RuntimeInvoker_UInt64_t1076_Decimal_t1079,
	RuntimeInvoker_Int64_t676_Decimal_t1079,
	RuntimeInvoker_Boolean_t360_Decimal_t1079_Decimal_t1079,
	RuntimeInvoker_Decimal_t1079_Decimal_t1079,
	RuntimeInvoker_Int32_t359_Decimal_t1079_Decimal_t1079,
	RuntimeInvoker_Int32_t359_Decimal_t1079,
	RuntimeInvoker_Boolean_t360_Decimal_t1079,
	RuntimeInvoker_Decimal_t1079_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Int32U26_t2743_BooleanU26_t2742_BooleanU26_t2742_Int32U26_t2743_SByte_t1077,
	RuntimeInvoker_Decimal_t1079_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_DecimalU26_t2786_SByte_t1077,
	RuntimeInvoker_Int32_t359_DecimalU26_t2786_UInt64U26_t2778,
	RuntimeInvoker_Int32_t359_DecimalU26_t2786_Int64U26_t2776,
	RuntimeInvoker_Int32_t359_DecimalU26_t2786_DecimalU26_t2786,
	RuntimeInvoker_Int32_t359_DecimalU26_t2786_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_DecimalU26_t2786_Int32_t359,
	RuntimeInvoker_Double_t675_DecimalU26_t2786,
	RuntimeInvoker_Void_t1083_DecimalU26_t2786_Int32_t359,
	RuntimeInvoker_Int32_t359_DecimalU26_t2786_DecimalU26_t2786_DecimalU26_t2786,
	RuntimeInvoker_Byte_t664_Decimal_t1079,
	RuntimeInvoker_SByte_t1077_Decimal_t1079,
	RuntimeInvoker_Int16_t1078_Decimal_t1079,
	RuntimeInvoker_UInt16_t1064_Decimal_t1079,
	RuntimeInvoker_UInt32_t677_Decimal_t1079,
	RuntimeInvoker_Decimal_t1079_SByte_t1077,
	RuntimeInvoker_Decimal_t1079_Int16_t1078,
	RuntimeInvoker_Decimal_t1079_Int32_t359,
	RuntimeInvoker_Decimal_t1079_Int64_t676,
	RuntimeInvoker_Decimal_t1079_Single_t358,
	RuntimeInvoker_Decimal_t1079_Double_t675,
	RuntimeInvoker_Single_t358_Decimal_t1079,
	RuntimeInvoker_Double_t675_Decimal_t1079,
	RuntimeInvoker_Boolean_t360_IntPtr_t_IntPtr_t,
	RuntimeInvoker_IntPtr_t_Int64_t676,
	RuntimeInvoker_IntPtr_t_Object_t,
	RuntimeInvoker_Int32_t359_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t,
	RuntimeInvoker_UInt32_t677,
	RuntimeInvoker_UInt64_t1076_IntPtr_t,
	RuntimeInvoker_UInt32_t677_IntPtr_t,
	RuntimeInvoker_UIntPtr_t_Int64_t676,
	RuntimeInvoker_UIntPtr_t_Object_t,
	RuntimeInvoker_UIntPtr_t_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Object_t_MulticastDelegateU26_t2787,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_UInt64_t1076_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Object_t_Int16_t1078,
	RuntimeInvoker_Object_t_Object_t_Int64_t676,
	RuntimeInvoker_Int64_t676_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Object_t_Int64_t676_Int64_t676,
	RuntimeInvoker_Object_t_Int64_t676_Int64_t676_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Int64_t676_Int64_t676,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Object_t_Int64_t676_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64_t676,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_TypeAttributes_t1352,
	RuntimeInvoker_MemberTypes_t1331,
	RuntimeInvoker_RuntimeTypeHandle_t1084,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_TypeCode_t1704_Object_t,
	RuntimeInvoker_Object_t_RuntimeTypeHandle_t1084,
	RuntimeInvoker_RuntimeTypeHandle_t1084_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_RuntimeFieldHandle_t1086,
	RuntimeInvoker_Void_t1083_IntPtr_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_ContractionU5BU5DU26_t2788_Level2MapU5BU5DU26_t2789,
	RuntimeInvoker_Void_t1083_Object_t_CodePointIndexerU26_t2790_ByteU2AU26_t2782_ByteU2AU26_t2782_CodePointIndexerU26_t2790_ByteU2AU26_t2782,
	RuntimeInvoker_Byte_t664_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int32_t359_SByte_t1077,
	RuntimeInvoker_Byte_t664_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int32_t359_Int32_t359,
	RuntimeInvoker_ExtenderType_t1128_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_BooleanU26_t2742_BooleanU26_t2742_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32_t359_BooleanU26_t2742_BooleanU26_t2742_SByte_t1077_SByte_t1077_ContextU26_t2791,
	RuntimeInvoker_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_Int32_t359_SByte_t1077_ContextU26_t2791,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_BooleanU26_t2742,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int16_t1078_Int32_t359_SByte_t1077_ContextU26_t2791,
	RuntimeInvoker_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_Object_t_ContextU26_t2791,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077_ContextU26_t2791,
	RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Object_t_SByte_t1077_ContextU26_t2791,
	RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Object_t_SByte_t1077_Int32_t359_ContractionU26_t2792_ContextU26_t2791,
	RuntimeInvoker_Boolean_t360_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Int32_t359_Object_t_SByte_t1077_ContextU26_t2791,
	RuntimeInvoker_Boolean_t360_Object_t_Int32U26_t2743_Int32_t359_Int32_t359_Int32_t359_Object_t_SByte_t1077_Int32_t359_ContractionU26_t2792_ContextU26_t2791,
	RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Object_t_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Object_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_SByte_t1077_ByteU5BU5DU26_t2767_Int32U26_t2743,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_ConfidenceFactor_t1137,
	RuntimeInvoker_Sign_t1139_Object_t_Object_t,
	RuntimeInvoker_DSAParameters_t859_SByte_t1077,
	RuntimeInvoker_Void_t1083_DSAParameters_t859,
	RuntimeInvoker_Int16_t1078_Object_t_Int32_t359,
	RuntimeInvoker_Double_t675_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_Int16_t1078_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int32_t359_Single_t358_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Single_t358_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Int32_t359_Single_t358_Object_t,
	RuntimeInvoker_Boolean_t360_Int32_t359_SByte_t1077_MethodBaseU26_t2793_Int32U26_t2743_Int32U26_t2743_StringU26_t2765_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Int32_t359_DateTime_t546,
	RuntimeInvoker_DayOfWeek_t1653_DateTime_t546,
	RuntimeInvoker_Int32_t359_Int32U26_t2743_Int32_t359_Int32_t359,
	RuntimeInvoker_DayOfWeek_t1653_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32U26_t2743_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_DateTime_t546_DateTime_t546_TimeSpan_t969,
	RuntimeInvoker_TimeSpan_t969,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Object_t_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32U26_t2743,
	RuntimeInvoker_Decimal_t1079,
	RuntimeInvoker_SByte_t1077,
	RuntimeInvoker_UInt16_t1064,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_SByte_t1077_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_Int32_t359,
	RuntimeInvoker_Int32_t359_IntPtr_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_MonoIOErrorU26_t2794,
	RuntimeInvoker_Object_t_Object_t_Object_t_Int32_t359_Int32_t359_MonoIOErrorU26_t2794,
	RuntimeInvoker_Object_t_MonoIOErrorU26_t2794,
	RuntimeInvoker_FileAttributes_t1237_Object_t_MonoIOErrorU26_t2794,
	RuntimeInvoker_MonoFileType_t1246_IntPtr_t_MonoIOErrorU26_t2794,
	RuntimeInvoker_Boolean_t360_Object_t_MonoIOStatU26_t2795_MonoIOErrorU26_t2794,
	RuntimeInvoker_IntPtr_t_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_MonoIOErrorU26_t2794,
	RuntimeInvoker_Boolean_t360_IntPtr_t_MonoIOErrorU26_t2794,
	RuntimeInvoker_Int32_t359_IntPtr_t_Object_t_Int32_t359_Int32_t359_MonoIOErrorU26_t2794,
	RuntimeInvoker_Int64_t676_IntPtr_t_Int64_t676_Int32_t359_MonoIOErrorU26_t2794,
	RuntimeInvoker_Int64_t676_IntPtr_t_MonoIOErrorU26_t2794,
	RuntimeInvoker_Boolean_t360_IntPtr_t_Int64_t676_MonoIOErrorU26_t2794,
	RuntimeInvoker_Void_t1083_Object_t_Int32_t359_Int32_t359_Object_t_Object_t_Object_t,
	RuntimeInvoker_CallingConventions_t1321,
	RuntimeInvoker_RuntimeMethodHandle_t1696,
	RuntimeInvoker_MethodAttributes_t1332,
	RuntimeInvoker_MethodToken_t1288,
	RuntimeInvoker_FieldAttributes_t1329,
	RuntimeInvoker_RuntimeFieldHandle_t1086,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_OpCode_t1292,
	RuntimeInvoker_Void_t1083_OpCode_t1292_Object_t,
	RuntimeInvoker_StackBehaviour_t1296,
	RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Int32_t359_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_SByte_t1077_Object_t,
	RuntimeInvoker_IntPtr_t_Object_t_Int32U26_t2743_ModuleU26_t2796,
	RuntimeInvoker_Object_t_Object_t_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_AssemblyNameFlags_t1315,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_ObjectU5BU5DU26_t2774_Object_t_Object_t_Object_t_ObjectU26_t2797,
	RuntimeInvoker_Void_t1083_ObjectU5BU5DU26_t2774_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_ObjectU5BU5DU26_t2774_Object_t,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_EventAttributes_t1327,
	RuntimeInvoker_Object_t_IntPtr_t_IntPtr_t,
	RuntimeInvoker_Object_t_RuntimeFieldHandle_t1086,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Object_t_StreamingContext_t653,
	RuntimeInvoker_Object_t_RuntimeMethodHandle_t1696,
	RuntimeInvoker_Void_t1083_Object_t_MonoEventInfoU26_t2798,
	RuntimeInvoker_MonoEventInfo_t1336_Object_t,
	RuntimeInvoker_Void_t1083_IntPtr_t_MonoMethodInfoU26_t2799,
	RuntimeInvoker_MonoMethodInfo_t1340_IntPtr_t,
	RuntimeInvoker_MethodAttributes_t1332_IntPtr_t,
	RuntimeInvoker_CallingConventions_t1321_IntPtr_t,
	RuntimeInvoker_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_ExceptionU26_t2775,
	RuntimeInvoker_Void_t1083_Object_t_MonoPropertyInfoU26_t2800_Int32_t359,
	RuntimeInvoker_PropertyAttributes_t1348,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int32_t359_Object_t_Object_t_Object_t,
	RuntimeInvoker_ParameterAttributes_t1344,
	RuntimeInvoker_Void_t1083_Int64_t676_ResourceInfoU26_t2801,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Int32_t359,
	RuntimeInvoker_GCHandle_t1385_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_IntPtr_t_Object_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Byte_t664_IntPtr_t_Int32_t359,
	RuntimeInvoker_Void_t1083_IntPtr_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Void_t1083_BooleanU26_t2742,
	RuntimeInvoker_Object_t_Object_t_Object_t_StringU26_t2765,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_StringU26_t2765,
	RuntimeInvoker_Void_t1083_SByte_t1077_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_TimeSpan_t969,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_SByte_t1077_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_StreamingContext_t653_Object_t,
	RuntimeInvoker_Object_t_Object_t_StreamingContext_t653_ISurrogateSelectorU26_t2802,
	RuntimeInvoker_Void_t1083_Object_t_IntPtr_t_Object_t,
	RuntimeInvoker_TimeSpan_t969_Object_t,
	RuntimeInvoker_Object_t_StringU26_t2765,
	RuntimeInvoker_Object_t_Object_t_Object_t_ObjectU26_t2797,
	RuntimeInvoker_Boolean_t360_Object_t_StringU26_t2765_StringU26_t2765,
	RuntimeInvoker_WellKnownObjectMode_t1491,
	RuntimeInvoker_StreamingContext_t653,
	RuntimeInvoker_TypeFilterLevel_t1507,
	RuntimeInvoker_Void_t1083_Object_t_BooleanU26_t2742,
	RuntimeInvoker_Object_t_Byte_t664_Object_t_SByte_t1077_Object_t_Object_t,
	RuntimeInvoker_Object_t_Byte_t664_Object_t_SByte_t1077_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_ObjectU26_t2797_HeaderU5BU5DU26_t2803,
	RuntimeInvoker_Void_t1083_Byte_t664_Object_t_SByte_t1077_ObjectU26_t2797_HeaderU5BU5DU26_t2803,
	RuntimeInvoker_Boolean_t360_Byte_t664_Object_t,
	RuntimeInvoker_Void_t1083_Byte_t664_Object_t_Int64U26_t2776_ObjectU26_t2797_SerializationInfoU26_t2804,
	RuntimeInvoker_Void_t1083_Object_t_SByte_t1077_SByte_t1077_Int64U26_t2776_ObjectU26_t2797_SerializationInfoU26_t2804,
	RuntimeInvoker_Void_t1083_Object_t_Int64U26_t2776_ObjectU26_t2797_SerializationInfoU26_t2804,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64_t676_ObjectU26_t2797_SerializationInfoU26_t2804,
	RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Object_t_Int64_t676_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Int64U26_t2776_ObjectU26_t2797,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64U26_t2776_ObjectU26_t2797,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Int64_t676_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Int64_t676_Int64_t676_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Object_t_Int64_t676_Object_t,
	RuntimeInvoker_Object_t_Object_t_Byte_t664,
	RuntimeInvoker_Void_t1083_Int64_t676_Int32_t359_Int64_t676,
	RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_Int64_t676_Object_t_Int64_t676_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_SByte_t1077_Object_t_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_StreamingContext_t653,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_StreamingContext_t653,
	RuntimeInvoker_Void_t1083_StreamingContext_t653,
	RuntimeInvoker_Object_t_StreamingContext_t653_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_Int16_t1078,
	RuntimeInvoker_Void_t1083_Object_t_DateTime_t546,
	RuntimeInvoker_SerializationEntry_t1524,
	RuntimeInvoker_StreamingContextStates_t1527,
	RuntimeInvoker_CspProviderFlags_t1531,
	RuntimeInvoker_UInt32_t677_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Object_t_SByte_t1077,
	RuntimeInvoker_Void_t1083_Int64_t676_Object_t_Int32_t359,
	RuntimeInvoker_UInt32_t677_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_UInt32U26_t2777_Int32_t359_UInt32U26_t2777_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_IntPtr_t_IntPtr_t_Object_t,
	RuntimeInvoker_Void_t1083_Int64_t676_Int64_t676,
	RuntimeInvoker_UInt64_t1076_Int64_t676_Int32_t359,
	RuntimeInvoker_UInt64_t1076_Int64_t676_Int64_t676_Int64_t676,
	RuntimeInvoker_UInt64_t1076_Int64_t676,
	RuntimeInvoker_PaddingMode_t715,
	RuntimeInvoker_Void_t1083_StringBuilderU26_t2805_Int32_t359,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_EncoderFallbackBufferU26_t2806_CharU5BU5DU26_t2807,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_DecoderFallbackBufferU26_t2808,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int16_t1078_Int32_t359,
	RuntimeInvoker_Boolean_t360_Int16_t1078_Int16_t1078_Int32_t359,
	RuntimeInvoker_Void_t1083_Int16_t1078_Int16_t1078_Int32_t359,
	RuntimeInvoker_Object_t_Int32U26_t2743,
	RuntimeInvoker_Object_t_Int32_t359_Object_t_Int32_t359,
	RuntimeInvoker_Void_t1083_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_SByte_t1077_Int32_t359_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_SByte_t1077_Int32U26_t2743_BooleanU26_t2742_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_Int32U26_t2743,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_CharU26_t2772_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_CharU26_t2772_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_CharU26_t2772_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_CharU26_t2772_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_Object_t_Int64_t676_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_Object_t_Int64_t676_Int32_t359_Object_t_Int32U26_t2743,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Object_t_Int32_t359_UInt32U26_t2777_UInt32U26_t2777_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Int32_t359_UInt32U26_t2777_UInt32U26_t2777_Object_t_DecoderFallbackBufferU26_t2808_ByteU5BU5DU26_t2767_SByte_t1077,
	RuntimeInvoker_Void_t1083_SByte_t1077_Int32_t359,
	RuntimeInvoker_IntPtr_t_SByte_t1077_Object_t_BooleanU26_t2742,
	RuntimeInvoker_Boolean_t360_IntPtr_t,
	RuntimeInvoker_IntPtr_t_SByte_t1077_SByte_t1077_Object_t_BooleanU26_t2742,
	RuntimeInvoker_Boolean_t360_TimeSpan_t969_TimeSpan_t969,
	RuntimeInvoker_Boolean_t360_Int64_t676_Int64_t676_SByte_t1077,
	RuntimeInvoker_Boolean_t360_IntPtr_t_Int32_t359_SByte_t1077,
	RuntimeInvoker_Int64_t676_Double_t675,
	RuntimeInvoker_Object_t_Double_t675,
	RuntimeInvoker_Int64_t676_Object_t_Int32_t359,
	RuntimeInvoker_Object_t_IntPtr_t_Int32_t359_Int32_t359,
	RuntimeInvoker_Byte_t664_SByte_t1077,
	RuntimeInvoker_Byte_t664_Double_t675,
	RuntimeInvoker_Byte_t664_Single_t358,
	RuntimeInvoker_Byte_t664_Int64_t676,
	RuntimeInvoker_Char_t390_SByte_t1077,
	RuntimeInvoker_Char_t390_Int64_t676,
	RuntimeInvoker_Char_t390_Single_t358,
	RuntimeInvoker_Char_t390_Object_t_Object_t,
	RuntimeInvoker_DateTime_t546_Object_t_Object_t,
	RuntimeInvoker_DateTime_t546_Int16_t1078,
	RuntimeInvoker_DateTime_t546_Int32_t359,
	RuntimeInvoker_DateTime_t546_Int64_t676,
	RuntimeInvoker_DateTime_t546_Single_t358,
	RuntimeInvoker_DateTime_t546_SByte_t1077,
	RuntimeInvoker_Double_t675_SByte_t1077,
	RuntimeInvoker_Double_t675_Double_t675,
	RuntimeInvoker_Double_t675_Single_t358,
	RuntimeInvoker_Double_t675_Int32_t359,
	RuntimeInvoker_Double_t675_Int64_t676,
	RuntimeInvoker_Double_t675_Int16_t1078,
	RuntimeInvoker_Int16_t1078_SByte_t1077,
	RuntimeInvoker_Int16_t1078_Double_t675,
	RuntimeInvoker_Int16_t1078_Single_t358,
	RuntimeInvoker_Int16_t1078_Int32_t359,
	RuntimeInvoker_Int16_t1078_Int64_t676,
	RuntimeInvoker_Int64_t676_SByte_t1077,
	RuntimeInvoker_Int64_t676_Int16_t1078,
	RuntimeInvoker_Int64_t676_Single_t358,
	RuntimeInvoker_Int64_t676_Int64_t676,
	RuntimeInvoker_SByte_t1077_SByte_t1077,
	RuntimeInvoker_SByte_t1077_Int16_t1078,
	RuntimeInvoker_SByte_t1077_Double_t675,
	RuntimeInvoker_SByte_t1077_Single_t358,
	RuntimeInvoker_SByte_t1077_Int32_t359,
	RuntimeInvoker_SByte_t1077_Int64_t676,
	RuntimeInvoker_Single_t358_SByte_t1077,
	RuntimeInvoker_Single_t358_Double_t675,
	RuntimeInvoker_Single_t358_Int64_t676,
	RuntimeInvoker_Single_t358_Int16_t1078,
	RuntimeInvoker_UInt16_t1064_SByte_t1077,
	RuntimeInvoker_UInt16_t1064_Double_t675,
	RuntimeInvoker_UInt16_t1064_Single_t358,
	RuntimeInvoker_UInt16_t1064_Int32_t359,
	RuntimeInvoker_UInt16_t1064_Int64_t676,
	RuntimeInvoker_UInt32_t677_SByte_t1077,
	RuntimeInvoker_UInt32_t677_Int16_t1078,
	RuntimeInvoker_UInt32_t677_Double_t675,
	RuntimeInvoker_UInt32_t677_Single_t358,
	RuntimeInvoker_UInt32_t677_Int64_t676,
	RuntimeInvoker_UInt64_t1076_SByte_t1077,
	RuntimeInvoker_UInt64_t1076_Int16_t1078,
	RuntimeInvoker_UInt64_t1076_Double_t675,
	RuntimeInvoker_UInt64_t1076_Single_t358,
	RuntimeInvoker_UInt64_t1076_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_SByte_t1077_TimeSpan_t969,
	RuntimeInvoker_Void_t1083_Int64_t676_Int32_t359,
	RuntimeInvoker_DayOfWeek_t1653,
	RuntimeInvoker_DateTimeKind_t1650,
	RuntimeInvoker_DateTime_t546_TimeSpan_t969,
	RuntimeInvoker_DateTime_t546_Double_t675,
	RuntimeInvoker_Int32_t359_DateTime_t546_DateTime_t546,
	RuntimeInvoker_DateTime_t546_DateTime_t546_Int32_t359,
	RuntimeInvoker_DateTime_t546_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Int32_t359_DateTimeU26_t2809_DateTimeOffsetU26_t2810_SByte_t1077_ExceptionU26_t2775,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_ExceptionU26_t2775,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_SByte_t1077_SByte_t1077_Int32U26_t2743,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Object_t_Object_t_SByte_t1077_Int32U26_t2743,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Int32_t359_Object_t_Int32U26_t2743,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Int32_t359_Object_t_SByte_t1077_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_Boolean_t360_Object_t_Int32_t359_Object_t_SByte_t1077_Int32U26_t2743,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_SByte_t1077_DateTimeU26_t2809_DateTimeOffsetU26_t2810_Object_t_Int32_t359_SByte_t1077_BooleanU26_t2742_BooleanU26_t2742,
	RuntimeInvoker_DateTime_t546_Object_t_Object_t_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_Object_t_Object_t_Int32_t359_DateTimeU26_t2809_SByte_t1077_BooleanU26_t2742_SByte_t1077_ExceptionU26_t2775,
	RuntimeInvoker_DateTime_t546_DateTime_t546_TimeSpan_t969,
	RuntimeInvoker_Boolean_t360_DateTime_t546_DateTime_t546,
	RuntimeInvoker_Void_t1083_DateTime_t546_TimeSpan_t969,
	RuntimeInvoker_Void_t1083_Int64_t676_TimeSpan_t969,
	RuntimeInvoker_Int32_t359_DateTimeOffset_t1651,
	RuntimeInvoker_Boolean_t360_DateTimeOffset_t1651,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int16_t1078,
	RuntimeInvoker_Object_t_Int16_t1078_Object_t_BooleanU26_t2742_BooleanU26_t2742,
	RuntimeInvoker_Object_t_Int16_t1078_Object_t_BooleanU26_t2742_BooleanU26_t2742_SByte_t1077,
	RuntimeInvoker_Object_t_DateTime_t546_Object_t_Object_t,
	RuntimeInvoker_Object_t_DateTime_t546_Nullable_1_t1757_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_MonoEnumInfo_t1664,
	RuntimeInvoker_Void_t1083_Object_t_MonoEnumInfoU26_t2811,
	RuntimeInvoker_Int32_t359_Int16_t1078_Int16_t1078,
	RuntimeInvoker_Int32_t359_Int64_t676_Int64_t676,
	RuntimeInvoker_PlatformID_t1693,
	RuntimeInvoker_Void_t1083_Int32_t359_Int16_t1078_Int16_t1078_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Int32_t359_Guid_t1673,
	RuntimeInvoker_Boolean_t360_Guid_t1673,
	RuntimeInvoker_Guid_t1673,
	RuntimeInvoker_Object_t_SByte_t1077_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Double_t675_Double_t675_Double_t675,
	RuntimeInvoker_TypeAttributes_t1352_Object_t,
	RuntimeInvoker_Object_t_SByte_t1077_SByte_t1077,
	RuntimeInvoker_Void_t1083_UInt64U2AU26_t2812_Int32U2AU26_t2813_CharU2AU26_t2814_CharU2AU26_t2814_Int64U2AU26_t2815_Int32U2AU26_t2813,
	RuntimeInvoker_Void_t1083_Int32_t359_Int64_t676,
	RuntimeInvoker_Void_t1083_Object_t_Double_t675_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Decimal_t1079,
	RuntimeInvoker_Object_t_Object_t_SByte_t1077_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int16_t1078_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Object_t,
	RuntimeInvoker_Object_t_Object_t_Int64_t676_Object_t,
	RuntimeInvoker_Object_t_Object_t_Single_t358_Object_t,
	RuntimeInvoker_Object_t_Object_t_Double_t675_Object_t,
	RuntimeInvoker_Object_t_Object_t_Decimal_t1079_Object_t,
	RuntimeInvoker_Object_t_Single_t358_Object_t,
	RuntimeInvoker_Object_t_Double_t675_Object_t,
	RuntimeInvoker_Void_t1083_Object_t_BooleanU26_t2742_SByte_t1077_Int32U26_t2743_Int32U26_t2743,
	RuntimeInvoker_Object_t_Object_t_Int32_t359_Int32_t359_Object_t_SByte_t1077_Object_t_Object_t_Object_t,
	RuntimeInvoker_Void_t1083_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_Int64_t676_Int32_t359_Int32_t359_Int32_t359_Int32_t359_Int32_t359,
	RuntimeInvoker_TimeSpan_t969_TimeSpan_t969,
	RuntimeInvoker_Int32_t359_TimeSpan_t969_TimeSpan_t969,
	RuntimeInvoker_Int32_t359_TimeSpan_t969,
	RuntimeInvoker_Boolean_t360_TimeSpan_t969,
	RuntimeInvoker_TimeSpan_t969_Double_t675,
	RuntimeInvoker_TimeSpan_t969_Double_t675_Int64_t676,
	RuntimeInvoker_TimeSpan_t969_TimeSpan_t969_TimeSpan_t969,
	RuntimeInvoker_TimeSpan_t969_DateTime_t546,
	RuntimeInvoker_Boolean_t360_DateTime_t546_Object_t,
	RuntimeInvoker_DateTime_t546_DateTime_t546,
	RuntimeInvoker_TimeSpan_t969_DateTime_t546_TimeSpan_t969,
	RuntimeInvoker_Boolean_t360_Int32_t359_Int64U5BU5DU26_t2816_StringU5BU5DU26_t2817,
	RuntimeInvoker_Boolean_t360_ObjectU26_t2797_Object_t,
	RuntimeInvoker_Void_t1083_ObjectU26_t2797_Object_t,
	RuntimeInvoker_Object_t_Object_t_Object_t_Object_t_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1896,
	RuntimeInvoker_Void_t1083_Int32_t359_ObjectU26_t2797,
	RuntimeInvoker_Void_t1083_ObjectU5BU5DU26_t2774_Int32_t359,
	RuntimeInvoker_Void_t1083_ObjectU5BU5DU26_t2774_Int32_t359_Int32_t359,
	RuntimeInvoker_Void_t1083_KeyValuePair_2_t1817,
	RuntimeInvoker_Boolean_t360_KeyValuePair_2_t1817,
	RuntimeInvoker_KeyValuePair_2_t1817_Object_t_Object_t,
	RuntimeInvoker_Boolean_t360_Object_t_ObjectU26_t2797,
	RuntimeInvoker_Enumerator_t1824,
	RuntimeInvoker_DictionaryEntry_t1057_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1817,
	RuntimeInvoker_Enumerator_t1823,
	RuntimeInvoker_Int32_t359_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Enumerator_t1842,
	RuntimeInvoker_Enumerator_t1955,
	RuntimeInvoker_Enumerator_t1956,
	RuntimeInvoker_KeyValuePair_2_t1952,
	RuntimeInvoker_Void_t1083_FloatTween_t163,
	RuntimeInvoker_Void_t1083_ColorTween_t160,
	RuntimeInvoker_Boolean_t360_TypeU26_t2818_Int32_t359,
	RuntimeInvoker_Boolean_t360_BooleanU26_t2742_SByte_t1077,
	RuntimeInvoker_Boolean_t360_FillMethodU26_t2819_Int32_t359,
	RuntimeInvoker_Boolean_t360_SingleU26_t2755_Single_t358,
	RuntimeInvoker_Boolean_t360_ContentTypeU26_t2820_Int32_t359,
	RuntimeInvoker_Boolean_t360_LineTypeU26_t2821_Int32_t359,
	RuntimeInvoker_Boolean_t360_InputTypeU26_t2822_Int32_t359,
	RuntimeInvoker_Boolean_t360_TouchScreenKeyboardTypeU26_t2823_Int32_t359,
	RuntimeInvoker_Boolean_t360_CharacterValidationU26_t2824_Int32_t359,
	RuntimeInvoker_Boolean_t360_CharU26_t2772_Int16_t1078,
	RuntimeInvoker_Boolean_t360_DirectionU26_t2825_Int32_t359,
	RuntimeInvoker_Boolean_t360_NavigationU26_t2826_Navigation_t247,
	RuntimeInvoker_Boolean_t360_TransitionU26_t2827_Int32_t359,
	RuntimeInvoker_Boolean_t360_ColorBlockU26_t2828_ColorBlock_t174,
	RuntimeInvoker_Boolean_t360_SpriteStateU26_t2829_SpriteState_t267,
	RuntimeInvoker_Boolean_t360_DirectionU26_t2830_Int32_t359,
	RuntimeInvoker_Boolean_t360_AspectModeU26_t2831_Int32_t359,
	RuntimeInvoker_Boolean_t360_FitModeU26_t2832_Int32_t359,
	RuntimeInvoker_Void_t1083_CornerU26_t2833_Int32_t359,
	RuntimeInvoker_Void_t1083_AxisU26_t2834_Int32_t359,
	RuntimeInvoker_Void_t1083_Vector2U26_t2757_Vector2_t23,
	RuntimeInvoker_Void_t1083_ConstraintU26_t2835_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32U26_t2743_Int32_t359,
	RuntimeInvoker_Void_t1083_SingleU26_t2755_Single_t358,
	RuntimeInvoker_Void_t1083_BooleanU26_t2742_SByte_t1077,
	RuntimeInvoker_Void_t1083_TextAnchorU26_t2836_Int32_t359,
	RuntimeInvoker_Void_t1083_Object_t_Object_t_Single_t358,
	RuntimeInvoker_KeyValuePair_2_t1817_Int32_t359,
	RuntimeInvoker_Int32_t359_KeyValuePair_2_t1817,
	RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t1817,
	RuntimeInvoker_Link_t1181_Int32_t359,
	RuntimeInvoker_Void_t1083_Link_t1181,
	RuntimeInvoker_Boolean_t360_Link_t1181,
	RuntimeInvoker_Int32_t359_Link_t1181,
	RuntimeInvoker_Void_t1083_Int32_t359_Link_t1181,
	RuntimeInvoker_DictionaryEntry_t1057_Int32_t359,
	RuntimeInvoker_Void_t1083_DictionaryEntry_t1057,
	RuntimeInvoker_Boolean_t360_DictionaryEntry_t1057,
	RuntimeInvoker_Int32_t359_DictionaryEntry_t1057,
	RuntimeInvoker_Void_t1083_Int32_t359_DictionaryEntry_t1057,
	RuntimeInvoker_Void_t1083_Touch_t72,
	RuntimeInvoker_Boolean_t360_Touch_t72,
	RuntimeInvoker_Int32_t359_Touch_t72,
	RuntimeInvoker_Void_t1083_Int32_t359_Touch_t72,
	RuntimeInvoker_Void_t1083_Int32_t359_Double_t675,
	RuntimeInvoker_Quaternion_t45_Int32_t359,
	RuntimeInvoker_Boolean_t360_Quaternion_t45,
	RuntimeInvoker_Int32_t359_Quaternion_t45,
	RuntimeInvoker_Void_t1083_Int32_t359_Quaternion_t45,
	RuntimeInvoker_KeyValuePair_2_t1864_Int32_t359,
	RuntimeInvoker_Void_t1083_KeyValuePair_2_t1864,
	RuntimeInvoker_Boolean_t360_KeyValuePair_2_t1864,
	RuntimeInvoker_Int32_t359_KeyValuePair_2_t1864,
	RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t1864,
	RuntimeInvoker_RaycastResult_t139_Int32_t359,
	RuntimeInvoker_Boolean_t360_RaycastResult_t139,
	RuntimeInvoker_Int32_t359_RaycastResult_t139,
	RuntimeInvoker_Void_t1083_Int32_t359_RaycastResult_t139,
	RuntimeInvoker_Void_t1083_RaycastResultU5BU5DU26_t2837_Int32_t359,
	RuntimeInvoker_Void_t1083_RaycastResultU5BU5DU26_t2837_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_RaycastResult_t139_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_RaycastResult_t139_RaycastResult_t139_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1952_Int32_t359,
	RuntimeInvoker_Void_t1083_KeyValuePair_2_t1952,
	RuntimeInvoker_Boolean_t360_KeyValuePair_2_t1952,
	RuntimeInvoker_Int32_t359_KeyValuePair_2_t1952,
	RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t1952,
	RuntimeInvoker_RaycastHit2D_t369_Int32_t359,
	RuntimeInvoker_Void_t1083_RaycastHit2D_t369,
	RuntimeInvoker_Boolean_t360_RaycastHit2D_t369,
	RuntimeInvoker_Int32_t359_RaycastHit2D_t369,
	RuntimeInvoker_Void_t1083_Int32_t359_RaycastHit2D_t369,
	RuntimeInvoker_RaycastHit_t100_Int32_t359,
	RuntimeInvoker_Void_t1083_RaycastHit_t100,
	RuntimeInvoker_Boolean_t360_RaycastHit_t100,
	RuntimeInvoker_Int32_t359_RaycastHit_t100,
	RuntimeInvoker_Void_t1083_Int32_t359_RaycastHit_t100,
	RuntimeInvoker_Vector3_t12_Int32_t359,
	RuntimeInvoker_Int32_t359_Vector3_t12,
	RuntimeInvoker_Void_t1083_Int32_t359_Vector3_t12,
	RuntimeInvoker_UIVertex_t239_Int32_t359,
	RuntimeInvoker_Boolean_t360_UIVertex_t239,
	RuntimeInvoker_Int32_t359_UIVertex_t239,
	RuntimeInvoker_Void_t1083_Int32_t359_UIVertex_t239,
	RuntimeInvoker_Void_t1083_UIVertexU5BU5DU26_t2838_Int32_t359,
	RuntimeInvoker_Void_t1083_UIVertexU5BU5DU26_t2838_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_UIVertex_t239_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_UIVertex_t239_UIVertex_t239_Object_t,
	RuntimeInvoker_Boolean_t360_Vector2_t23,
	RuntimeInvoker_Void_t1083_Int32_t359_Vector2_t23,
	RuntimeInvoker_ContentType_t218_Int32_t359,
	RuntimeInvoker_UILineInfo_t394_Int32_t359,
	RuntimeInvoker_Void_t1083_UILineInfo_t394,
	RuntimeInvoker_Boolean_t360_UILineInfo_t394,
	RuntimeInvoker_Int32_t359_UILineInfo_t394,
	RuntimeInvoker_Void_t1083_Int32_t359_UILineInfo_t394,
	RuntimeInvoker_UICharInfo_t396_Int32_t359,
	RuntimeInvoker_Void_t1083_UICharInfo_t396,
	RuntimeInvoker_Boolean_t360_UICharInfo_t396,
	RuntimeInvoker_Int32_t359_UICharInfo_t396,
	RuntimeInvoker_Void_t1083_Int32_t359_UICharInfo_t396,
	RuntimeInvoker_Void_t1083_Vector3U5BU5DU26_t2839_Int32_t359,
	RuntimeInvoker_Void_t1083_Vector3U5BU5DU26_t2839_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Vector3_t12_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Vector3_t12_Vector3_t12_Object_t,
	RuntimeInvoker_Void_t1083_Int32U5BU5DU26_t2840_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32U5BU5DU26_t2840_Int32_t359_Int32_t359,
	RuntimeInvoker_Color32_t347_Int32_t359,
	RuntimeInvoker_Void_t1083_Color32_t347,
	RuntimeInvoker_Boolean_t360_Color32_t347,
	RuntimeInvoker_Int32_t359_Color32_t347,
	RuntimeInvoker_Void_t1083_Int32_t359_Color32_t347,
	RuntimeInvoker_Void_t1083_Color32U5BU5DU26_t2841_Int32_t359,
	RuntimeInvoker_Void_t1083_Color32U5BU5DU26_t2841_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Color32_t347_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Color32_t347_Color32_t347_Object_t,
	RuntimeInvoker_Void_t1083_Vector2U5BU5DU26_t2842_Int32_t359,
	RuntimeInvoker_Void_t1083_Vector2U5BU5DU26_t2842_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Vector2_t23_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Vector2_t23_Vector2_t23_Object_t,
	RuntimeInvoker_Void_t1083_Vector4_t317,
	RuntimeInvoker_Boolean_t360_Vector4_t317,
	RuntimeInvoker_Int32_t359_Vector4_t317,
	RuntimeInvoker_Void_t1083_Vector4U5BU5DU26_t2843_Int32_t359,
	RuntimeInvoker_Void_t1083_Vector4U5BU5DU26_t2843_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_Vector4_t317_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Vector4_t317_Vector4_t317_Object_t,
	RuntimeInvoker_GcAchievementData_t581_Int32_t359,
	RuntimeInvoker_Void_t1083_GcAchievementData_t581,
	RuntimeInvoker_Boolean_t360_GcAchievementData_t581,
	RuntimeInvoker_Int32_t359_GcAchievementData_t581,
	RuntimeInvoker_Void_t1083_Int32_t359_GcAchievementData_t581,
	RuntimeInvoker_GcScoreData_t582_Int32_t359,
	RuntimeInvoker_Boolean_t360_GcScoreData_t582,
	RuntimeInvoker_Int32_t359_GcScoreData_t582,
	RuntimeInvoker_Void_t1083_Int32_t359_GcScoreData_t582,
	RuntimeInvoker_Void_t1083_Int32_t359_IntPtr_t,
	RuntimeInvoker_ContactPoint_t505_Int32_t359,
	RuntimeInvoker_Void_t1083_ContactPoint_t505,
	RuntimeInvoker_Boolean_t360_ContactPoint_t505,
	RuntimeInvoker_Int32_t359_ContactPoint_t505,
	RuntimeInvoker_Void_t1083_Int32_t359_ContactPoint_t505,
	RuntimeInvoker_ContactPoint2D_t510_Int32_t359,
	RuntimeInvoker_Void_t1083_ContactPoint2D_t510,
	RuntimeInvoker_Boolean_t360_ContactPoint2D_t510,
	RuntimeInvoker_Int32_t359_ContactPoint2D_t510,
	RuntimeInvoker_Void_t1083_Int32_t359_ContactPoint2D_t510,
	RuntimeInvoker_Keyframe_t524_Int32_t359,
	RuntimeInvoker_Void_t1083_Keyframe_t524,
	RuntimeInvoker_Boolean_t360_Keyframe_t524,
	RuntimeInvoker_Int32_t359_Keyframe_t524,
	RuntimeInvoker_Void_t1083_Int32_t359_Keyframe_t524,
	RuntimeInvoker_CharacterInfo_t533_Int32_t359,
	RuntimeInvoker_Void_t1083_CharacterInfo_t533,
	RuntimeInvoker_Boolean_t360_CharacterInfo_t533,
	RuntimeInvoker_Int32_t359_CharacterInfo_t533,
	RuntimeInvoker_Void_t1083_Int32_t359_CharacterInfo_t533,
	RuntimeInvoker_Void_t1083_UICharInfoU5BU5DU26_t2844_Int32_t359,
	RuntimeInvoker_Void_t1083_UICharInfoU5BU5DU26_t2844_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_UICharInfo_t396_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_UICharInfo_t396_UICharInfo_t396_Object_t,
	RuntimeInvoker_Void_t1083_UILineInfoU5BU5DU26_t2845_Int32_t359,
	RuntimeInvoker_Void_t1083_UILineInfoU5BU5DU26_t2845_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_UILineInfo_t394_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_UILineInfo_t394_UILineInfo_t394_Object_t,
	RuntimeInvoker_ParameterModifier_t1345_Int32_t359,
	RuntimeInvoker_Void_t1083_ParameterModifier_t1345,
	RuntimeInvoker_Boolean_t360_ParameterModifier_t1345,
	RuntimeInvoker_Int32_t359_ParameterModifier_t1345,
	RuntimeInvoker_Void_t1083_Int32_t359_ParameterModifier_t1345,
	RuntimeInvoker_HitInfo_t597_Int32_t359,
	RuntimeInvoker_Void_t1083_HitInfo_t597,
	RuntimeInvoker_Int32_t359_HitInfo_t597,
	RuntimeInvoker_KeyValuePair_2_t2270_Int32_t359,
	RuntimeInvoker_Void_t1083_KeyValuePair_2_t2270,
	RuntimeInvoker_Boolean_t360_KeyValuePair_2_t2270,
	RuntimeInvoker_Int32_t359_KeyValuePair_2_t2270,
	RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t2270,
	RuntimeInvoker_TextEditOp_t615_Int32_t359,
	RuntimeInvoker_ClientCertificateType_t833_Int32_t359,
	RuntimeInvoker_KeyValuePair_2_t2318_Int32_t359,
	RuntimeInvoker_Void_t1083_KeyValuePair_2_t2318,
	RuntimeInvoker_Boolean_t360_KeyValuePair_2_t2318,
	RuntimeInvoker_Int32_t359_KeyValuePair_2_t2318,
	RuntimeInvoker_Void_t1083_Int32_t359_KeyValuePair_2_t2318,
	RuntimeInvoker_X509ChainStatus_t966_Int32_t359,
	RuntimeInvoker_Void_t1083_X509ChainStatus_t966,
	RuntimeInvoker_Boolean_t360_X509ChainStatus_t966,
	RuntimeInvoker_Int32_t359_X509ChainStatus_t966,
	RuntimeInvoker_Void_t1083_Int32_t359_X509ChainStatus_t966,
	RuntimeInvoker_Int32_t359_Object_t_Int32_t359_Int32_t359_Int32_t359_Object_t,
	RuntimeInvoker_Mark_t1009_Int32_t359,
	RuntimeInvoker_Void_t1083_Mark_t1009,
	RuntimeInvoker_Boolean_t360_Mark_t1009,
	RuntimeInvoker_Int32_t359_Mark_t1009,
	RuntimeInvoker_Void_t1083_Int32_t359_Mark_t1009,
	RuntimeInvoker_UriScheme_t1046_Int32_t359,
	RuntimeInvoker_Void_t1083_UriScheme_t1046,
	RuntimeInvoker_Boolean_t360_UriScheme_t1046,
	RuntimeInvoker_Int32_t359_UriScheme_t1046,
	RuntimeInvoker_Void_t1083_Int32_t359_UriScheme_t1046,
	RuntimeInvoker_TableRange_t1114_Int32_t359,
	RuntimeInvoker_Void_t1083_TableRange_t1114,
	RuntimeInvoker_Boolean_t360_TableRange_t1114,
	RuntimeInvoker_Int32_t359_TableRange_t1114,
	RuntimeInvoker_Void_t1083_Int32_t359_TableRange_t1114,
	RuntimeInvoker_Slot_t1191_Int32_t359,
	RuntimeInvoker_Void_t1083_Slot_t1191,
	RuntimeInvoker_Boolean_t360_Slot_t1191,
	RuntimeInvoker_Int32_t359_Slot_t1191,
	RuntimeInvoker_Void_t1083_Int32_t359_Slot_t1191,
	RuntimeInvoker_Slot_t1199_Int32_t359,
	RuntimeInvoker_Void_t1083_Slot_t1199,
	RuntimeInvoker_Boolean_t360_Slot_t1199,
	RuntimeInvoker_Int32_t359_Slot_t1199,
	RuntimeInvoker_Void_t1083_Int32_t359_Slot_t1199,
	RuntimeInvoker_ILTokenInfo_t1279_Int32_t359,
	RuntimeInvoker_Void_t1083_ILTokenInfo_t1279,
	RuntimeInvoker_Boolean_t360_ILTokenInfo_t1279,
	RuntimeInvoker_Int32_t359_ILTokenInfo_t1279,
	RuntimeInvoker_Void_t1083_Int32_t359_ILTokenInfo_t1279,
	RuntimeInvoker_LabelData_t1281_Int32_t359,
	RuntimeInvoker_Void_t1083_LabelData_t1281,
	RuntimeInvoker_Boolean_t360_LabelData_t1281,
	RuntimeInvoker_Int32_t359_LabelData_t1281,
	RuntimeInvoker_Void_t1083_Int32_t359_LabelData_t1281,
	RuntimeInvoker_LabelFixup_t1280_Int32_t359,
	RuntimeInvoker_Void_t1083_LabelFixup_t1280,
	RuntimeInvoker_Boolean_t360_LabelFixup_t1280,
	RuntimeInvoker_Int32_t359_LabelFixup_t1280,
	RuntimeInvoker_Void_t1083_Int32_t359_LabelFixup_t1280,
	RuntimeInvoker_CustomAttributeTypedArgument_t1326_Int32_t359,
	RuntimeInvoker_Void_t1083_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_Boolean_t360_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_Int32_t359_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_Void_t1083_Int32_t359_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_CustomAttributeNamedArgument_t1325_Int32_t359,
	RuntimeInvoker_Void_t1083_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_Boolean_t360_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_Int32_t359_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_Void_t1083_Int32_t359_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_Void_t1083_CustomAttributeTypedArgumentU5BU5DU26_t2846_Int32_t359,
	RuntimeInvoker_Void_t1083_CustomAttributeTypedArgumentU5BU5DU26_t2846_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_CustomAttributeTypedArgument_t1326_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_Void_t1083_CustomAttributeNamedArgumentU5BU5DU26_t2847_Int32_t359,
	RuntimeInvoker_Void_t1083_CustomAttributeNamedArgumentU5BU5DU26_t2847_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_Object_t_CustomAttributeNamedArgument_t1325_Int32_t359_Int32_t359,
	RuntimeInvoker_Int32_t359_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325_Object_t,
	RuntimeInvoker_Int32_t359_Object_t_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_ResourceInfo_t1356_Int32_t359,
	RuntimeInvoker_Void_t1083_ResourceInfo_t1356,
	RuntimeInvoker_Boolean_t360_ResourceInfo_t1356,
	RuntimeInvoker_Int32_t359_ResourceInfo_t1356,
	RuntimeInvoker_Void_t1083_Int32_t359_ResourceInfo_t1356,
	RuntimeInvoker_ResourceCacheItem_t1357_Int32_t359,
	RuntimeInvoker_Void_t1083_ResourceCacheItem_t1357,
	RuntimeInvoker_Boolean_t360_ResourceCacheItem_t1357,
	RuntimeInvoker_Int32_t359_ResourceCacheItem_t1357,
	RuntimeInvoker_Void_t1083_Int32_t359_ResourceCacheItem_t1357,
	RuntimeInvoker_Void_t1083_Int32_t359_DateTime_t546,
	RuntimeInvoker_Void_t1083_Decimal_t1079,
	RuntimeInvoker_Void_t1083_Int32_t359_Decimal_t1079,
	RuntimeInvoker_TimeSpan_t969_Int32_t359,
	RuntimeInvoker_Void_t1083_Int32_t359_TimeSpan_t969,
	RuntimeInvoker_TypeTag_t1495_Int32_t359,
	RuntimeInvoker_Boolean_t360_Byte_t664,
	RuntimeInvoker_Int32_t359_Byte_t664,
	RuntimeInvoker_Void_t1083_Int32_t359_Byte_t664,
	RuntimeInvoker_Link_t1181,
	RuntimeInvoker_DictionaryEntry_t1057_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1817_Object_t,
	RuntimeInvoker_Touch_t72,
	RuntimeInvoker_KeyValuePair_2_t1864_Object_t_Int32_t359,
	RuntimeInvoker_Enumerator_t1868,
	RuntimeInvoker_DictionaryEntry_t1057_Object_t_Int32_t359,
	RuntimeInvoker_KeyValuePair_2_t1864,
	RuntimeInvoker_Enumerator_t1867,
	RuntimeInvoker_KeyValuePair_2_t1864_Object_t,
	RuntimeInvoker_Object_t_RaycastResult_t139_RaycastResult_t139_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t1914,
	RuntimeInvoker_Boolean_t360_RaycastResult_t139_RaycastResult_t139,
	RuntimeInvoker_Object_t_RaycastResult_t139_Object_t_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1952_Int32_t359_Object_t,
	RuntimeInvoker_Boolean_t360_Int32_t359_ObjectU26_t2797,
	RuntimeInvoker_DictionaryEntry_t1057_Int32_t359_Object_t,
	RuntimeInvoker_KeyValuePair_2_t1952_Object_t,
	RuntimeInvoker_RaycastHit2D_t369,
	RuntimeInvoker_Object_t_RaycastHit_t100_RaycastHit_t100_Object_t_Object_t,
	RuntimeInvoker_RaycastHit_t100,
	RuntimeInvoker_Object_t_Color_t83_Object_t_Object_t,
	RuntimeInvoker_Object_t_Single_t358_Object_t_Object_t,
	RuntimeInvoker_Object_t_FloatTween_t163,
	RuntimeInvoker_Object_t_ColorTween_t160,
	RuntimeInvoker_UIVertex_t239_Object_t,
	RuntimeInvoker_Enumerator_t2019,
	RuntimeInvoker_UIVertex_t239,
	RuntimeInvoker_Boolean_t360_UIVertex_t239_UIVertex_t239,
	RuntimeInvoker_Object_t_UIVertex_t239_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_UIVertex_t239_UIVertex_t239,
	RuntimeInvoker_Object_t_UIVertex_t239_UIVertex_t239_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t394,
	RuntimeInvoker_UICharInfo_t396,
	RuntimeInvoker_Object_t_Vector2_t23_Object_t_Object_t,
	RuntimeInvoker_Vector3_t12_Object_t,
	RuntimeInvoker_Enumerator_t2108,
	RuntimeInvoker_Object_t_Vector3_t12_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Vector3_t12_Vector3_t12,
	RuntimeInvoker_Object_t_Vector3_t12_Vector3_t12_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2118,
	RuntimeInvoker_Color32_t347_Object_t,
	RuntimeInvoker_Enumerator_t2128,
	RuntimeInvoker_Color32_t347,
	RuntimeInvoker_Boolean_t360_Color32_t347_Color32_t347,
	RuntimeInvoker_Object_t_Color32_t347_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Color32_t347_Color32_t347,
	RuntimeInvoker_Object_t_Color32_t347_Color32_t347_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2138,
	RuntimeInvoker_Int32_t359_Vector2_t23_Vector2_t23,
	RuntimeInvoker_Object_t_Vector2_t23_Vector2_t23_Object_t_Object_t,
	RuntimeInvoker_Enumerator_t2149,
	RuntimeInvoker_Object_t_Vector4_t317_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_Vector4_t317_Vector4_t317,
	RuntimeInvoker_Object_t_Vector4_t317_Vector4_t317_Object_t_Object_t,
	RuntimeInvoker_GcAchievementData_t581,
	RuntimeInvoker_GcScoreData_t582,
	RuntimeInvoker_ContactPoint_t505,
	RuntimeInvoker_ContactPoint2D_t510,
	RuntimeInvoker_Keyframe_t524,
	RuntimeInvoker_CharacterInfo_t533,
	RuntimeInvoker_UICharInfo_t396_Object_t,
	RuntimeInvoker_Enumerator_t2219,
	RuntimeInvoker_Boolean_t360_UICharInfo_t396_UICharInfo_t396,
	RuntimeInvoker_Object_t_UICharInfo_t396_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_UICharInfo_t396_UICharInfo_t396,
	RuntimeInvoker_Object_t_UICharInfo_t396_UICharInfo_t396_Object_t_Object_t,
	RuntimeInvoker_UILineInfo_t394_Object_t,
	RuntimeInvoker_Enumerator_t2228,
	RuntimeInvoker_Boolean_t360_UILineInfo_t394_UILineInfo_t394,
	RuntimeInvoker_Object_t_UILineInfo_t394_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_UILineInfo_t394_UILineInfo_t394,
	RuntimeInvoker_Object_t_UILineInfo_t394_UILineInfo_t394_Object_t_Object_t,
	RuntimeInvoker_ParameterModifier_t1345,
	RuntimeInvoker_HitInfo_t597,
	RuntimeInvoker_TextEditOp_t615_Object_t,
	RuntimeInvoker_KeyValuePair_2_t2270_Object_t_Int32_t359,
	RuntimeInvoker_TextEditOp_t615_Object_t_Int32_t359,
	RuntimeInvoker_Boolean_t360_Object_t_TextEditOpU26_t2848,
	RuntimeInvoker_Enumerator_t2275,
	RuntimeInvoker_KeyValuePair_2_t2270,
	RuntimeInvoker_TextEditOp_t615,
	RuntimeInvoker_Enumerator_t2274,
	RuntimeInvoker_KeyValuePair_2_t2270_Object_t,
	RuntimeInvoker_ClientCertificateType_t833,
	RuntimeInvoker_KeyValuePair_2_t2318_Object_t_SByte_t1077,
	RuntimeInvoker_Boolean_t360_Object_t_BooleanU26_t2742,
	RuntimeInvoker_Enumerator_t2323,
	RuntimeInvoker_DictionaryEntry_t1057_Object_t_SByte_t1077,
	RuntimeInvoker_KeyValuePair_2_t2318,
	RuntimeInvoker_Enumerator_t2322,
	RuntimeInvoker_KeyValuePair_2_t2318_Object_t,
	RuntimeInvoker_Boolean_t360_SByte_t1077_SByte_t1077,
	RuntimeInvoker_X509ChainStatus_t966,
	RuntimeInvoker_Mark_t1009,
	RuntimeInvoker_UriScheme_t1046,
	RuntimeInvoker_TableRange_t1114,
	RuntimeInvoker_Slot_t1191,
	RuntimeInvoker_Slot_t1199,
	RuntimeInvoker_ILTokenInfo_t1279,
	RuntimeInvoker_LabelData_t1281,
	RuntimeInvoker_LabelFixup_t1280,
	RuntimeInvoker_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_CustomAttributeTypedArgument_t1326_Object_t,
	RuntimeInvoker_Enumerator_t2378,
	RuntimeInvoker_Boolean_t360_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1326_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326,
	RuntimeInvoker_Object_t_CustomAttributeTypedArgument_t1326_CustomAttributeTypedArgument_t1326_Object_t_Object_t,
	RuntimeInvoker_CustomAttributeNamedArgument_t1325_Object_t,
	RuntimeInvoker_Enumerator_t2389,
	RuntimeInvoker_Boolean_t360_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1325_Object_t_Object_t,
	RuntimeInvoker_Int32_t359_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325,
	RuntimeInvoker_Object_t_CustomAttributeNamedArgument_t1325_CustomAttributeNamedArgument_t1325_Object_t_Object_t,
	RuntimeInvoker_ResourceInfo_t1356,
	RuntimeInvoker_ResourceCacheItem_t1357,
	RuntimeInvoker_TypeTag_t1495,
	RuntimeInvoker_Int32_t359_DateTimeOffset_t1651_DateTimeOffset_t1651,
	RuntimeInvoker_Boolean_t360_DateTimeOffset_t1651_DateTimeOffset_t1651,
	RuntimeInvoker_Boolean_t360_Nullable_1_t1757,
	RuntimeInvoker_Int32_t359_Guid_t1673_Guid_t1673,
	RuntimeInvoker_Boolean_t360_Guid_t1673_Guid_t1673,
};
