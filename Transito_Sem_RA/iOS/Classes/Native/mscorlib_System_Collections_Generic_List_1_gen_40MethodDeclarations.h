﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>
struct List_1_t2377;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2508;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ICollection_1_t1770;
// System.Collections.Generic.IEnumerable`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerable_1_t2509;
// System.Collections.ObjectModel.ReadOnlyCollection`1<System.Reflection.CustomAttributeTypedArgument>
struct ReadOnlyCollection_1_t1768;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1766;
// System.Predicate`1<System.Reflection.CustomAttributeTypedArgument>
struct Predicate_1_t2381;
// System.Comparison`1<System.Reflection.CustomAttributeTypedArgument>
struct Comparison_1_t2384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_37.h"

// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void List_1__ctor_m17948_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1__ctor_m17948(__this, method) (( void (*) (List_1_t2377 *, const MethodInfo*))List_1__ctor_m17948_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(System.Int32)
extern "C" void List_1__ctor_m17949_gshared (List_1_t2377 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m17949(__this, ___capacity, method) (( void (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1__ctor_m17949_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::.cctor()
extern "C" void List_1__cctor_m17950_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m17950(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m17950_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17951_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17951(__this, method) (( Object_t* (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m17951_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m17952_gshared (List_1_t2377 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m17952(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t2377 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m17952_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m17953_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m17953(__this, method) (( Object_t * (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m17953_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m17954_gshared (List_1_t2377 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m17954(__this, ___item, method) (( int32_t (*) (List_1_t2377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m17954_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m17955_gshared (List_1_t2377 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m17955(__this, ___item, method) (( bool (*) (List_1_t2377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m17955_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m17956_gshared (List_1_t2377 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m17956(__this, ___item, method) (( int32_t (*) (List_1_t2377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m17956_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m17957_gshared (List_1_t2377 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m17957(__this, ___index, ___item, method) (( void (*) (List_1_t2377 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m17957_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m17958_gshared (List_1_t2377 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m17958(__this, ___item, method) (( void (*) (List_1_t2377 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m17958_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17959_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17959(__this, method) (( bool (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17959_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m17960_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m17960(__this, method) (( bool (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m17960_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m17961_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m17961(__this, method) (( Object_t * (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m17961_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m17962_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m17962(__this, method) (( bool (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m17962_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m17963_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m17963(__this, method) (( bool (*) (List_1_t2377 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m17963_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m17964_gshared (List_1_t2377 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m17964(__this, ___index, method) (( Object_t * (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m17964_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m17965_gshared (List_1_t2377 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m17965(__this, ___index, ___value, method) (( void (*) (List_1_t2377 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m17965_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void List_1_Add_m17966_gshared (List_1_t2377 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define List_1_Add_m17966(__this, ___item, method) (( void (*) (List_1_t2377 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))List_1_Add_m17966_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m17967_gshared (List_1_t2377 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m17967(__this, ___newCount, method) (( void (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m17967_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m17968_gshared (List_1_t2377 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m17968(__this, ___collection, method) (( void (*) (List_1_t2377 *, Object_t*, const MethodInfo*))List_1_AddCollection_m17968_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m17969_gshared (List_1_t2377 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m17969(__this, ___enumerable, method) (( void (*) (List_1_t2377 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m17969_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m17970_gshared (List_1_t2377 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m17970(__this, ___collection, method) (( void (*) (List_1_t2377 *, Object_t*, const MethodInfo*))List_1_AddRange_m17970_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1768 * List_1_AsReadOnly_m17971_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m17971(__this, method) (( ReadOnlyCollection_1_t1768 * (*) (List_1_t2377 *, const MethodInfo*))List_1_AsReadOnly_m17971_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void List_1_Clear_m17972_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_Clear_m17972(__this, method) (( void (*) (List_1_t2377 *, const MethodInfo*))List_1_Clear_m17972_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool List_1_Contains_m17973_gshared (List_1_t2377 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define List_1_Contains_m17973(__this, ___item, method) (( bool (*) (List_1_t2377 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))List_1_Contains_m17973_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m17974_gshared (List_1_t2377 * __this, CustomAttributeTypedArgumentU5BU5D_t1766* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m17974(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t2377 *, CustomAttributeTypedArgumentU5BU5D_t1766*, int32_t, const MethodInfo*))List_1_CopyTo_m17974_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Find(System.Predicate`1<T>)
extern "C" CustomAttributeTypedArgument_t1326  List_1_Find_m17975_gshared (List_1_t2377 * __this, Predicate_1_t2381 * ___match, const MethodInfo* method);
#define List_1_Find_m17975(__this, ___match, method) (( CustomAttributeTypedArgument_t1326  (*) (List_1_t2377 *, Predicate_1_t2381 *, const MethodInfo*))List_1_Find_m17975_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m17976_gshared (Object_t * __this /* static, unused */, Predicate_1_t2381 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m17976(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2381 *, const MethodInfo*))List_1_CheckMatch_m17976_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m17977_gshared (List_1_t2377 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2381 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m17977(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t2377 *, int32_t, int32_t, Predicate_1_t2381 *, const MethodInfo*))List_1_GetIndex_m17977_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Enumerator_t2378  List_1_GetEnumerator_m17978_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m17978(__this, method) (( Enumerator_t2378  (*) (List_1_t2377 *, const MethodInfo*))List_1_GetEnumerator_m17978_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m17979_gshared (List_1_t2377 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define List_1_IndexOf_m17979(__this, ___item, method) (( int32_t (*) (List_1_t2377 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))List_1_IndexOf_m17979_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m17980_gshared (List_1_t2377 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m17980(__this, ___start, ___delta, method) (( void (*) (List_1_t2377 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m17980_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m17981_gshared (List_1_t2377 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m17981(__this, ___index, method) (( void (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1_CheckIndex_m17981_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m17982_gshared (List_1_t2377 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define List_1_Insert_m17982(__this, ___index, ___item, method) (( void (*) (List_1_t2377 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))List_1_Insert_m17982_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m17983_gshared (List_1_t2377 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m17983(__this, ___collection, method) (( void (*) (List_1_t2377 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m17983_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool List_1_Remove_m17984_gshared (List_1_t2377 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define List_1_Remove_m17984(__this, ___item, method) (( bool (*) (List_1_t2377 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))List_1_Remove_m17984_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m17985_gshared (List_1_t2377 * __this, Predicate_1_t2381 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m17985(__this, ___match, method) (( int32_t (*) (List_1_t2377 *, Predicate_1_t2381 *, const MethodInfo*))List_1_RemoveAll_m17985_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m17986_gshared (List_1_t2377 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m17986(__this, ___index, method) (( void (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1_RemoveAt_m17986_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Reverse()
extern "C" void List_1_Reverse_m17987_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_Reverse_m17987(__this, method) (( void (*) (List_1_t2377 *, const MethodInfo*))List_1_Reverse_m17987_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort()
extern "C" void List_1_Sort_m17988_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_Sort_m17988(__this, method) (( void (*) (List_1_t2377 *, const MethodInfo*))List_1_Sort_m17988_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m17989_gshared (List_1_t2377 * __this, Comparison_1_t2384 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m17989(__this, ___comparison, method) (( void (*) (List_1_t2377 *, Comparison_1_t2384 *, const MethodInfo*))List_1_Sort_m17989_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::ToArray()
extern "C" CustomAttributeTypedArgumentU5BU5D_t1766* List_1_ToArray_m17990_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_ToArray_m17990(__this, method) (( CustomAttributeTypedArgumentU5BU5D_t1766* (*) (List_1_t2377 *, const MethodInfo*))List_1_ToArray_m17990_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::TrimExcess()
extern "C" void List_1_TrimExcess_m17991_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m17991(__this, method) (( void (*) (List_1_t2377 *, const MethodInfo*))List_1_TrimExcess_m17991_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m17992_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m17992(__this, method) (( int32_t (*) (List_1_t2377 *, const MethodInfo*))List_1_get_Capacity_m17992_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m17993_gshared (List_1_t2377 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m17993(__this, ___value, method) (( void (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1_set_Capacity_m17993_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t List_1_get_Count_m17994_gshared (List_1_t2377 * __this, const MethodInfo* method);
#define List_1_get_Count_m17994(__this, method) (( int32_t (*) (List_1_t2377 *, const MethodInfo*))List_1_get_Count_m17994_gshared)(__this, method)
// T System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1326  List_1_get_Item_m17995_gshared (List_1_t2377 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m17995(__this, ___index, method) (( CustomAttributeTypedArgument_t1326  (*) (List_1_t2377 *, int32_t, const MethodInfo*))List_1_get_Item_m17995_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m17996_gshared (List_1_t2377 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___value, const MethodInfo* method);
#define List_1_set_Item_m17996(__this, ___index, ___value, method) (( void (*) (List_1_t2377 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))List_1_set_Item_m17996_gshared)(__this, ___index, ___value, method)
