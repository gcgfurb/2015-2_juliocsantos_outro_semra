﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityStandardAssets.Vehicles.Car.CarController
struct CarController_t29;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// UnityStandardAssets.Vehicles.Car.CarUserControl
struct  CarUserControl_t47  : public MonoBehaviour_t2
{
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.CarUserControl::m_Car
	CarController_t29 * ___m_Car_2;
};
