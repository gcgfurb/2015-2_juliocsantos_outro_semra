﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Component
struct Component_t78;
// System.IAsyncResult
struct IAsyncResult_t228;
// System.AsyncCallback
struct AsyncCallback_t229;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// System.Predicate`1<UnityEngine.Component>
struct  Predicate_1_t307  : public MulticastDelegate_t227
{
};
