﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>
struct Collection_1_t2376;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1766;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2508;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeTypedArgument>
struct IList_1_t1323;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void Collection_1__ctor_m17912_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1__ctor_m17912(__this, method) (( void (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1__ctor_m17912_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17913_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17913(__this, method) (( bool (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m17913_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m17914_gshared (Collection_1_t2376 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m17914(__this, ___array, ___index, method) (( void (*) (Collection_1_t2376 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m17914_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m17915_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m17915(__this, method) (( Object_t * (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m17915_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m17916_gshared (Collection_1_t2376 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m17916(__this, ___value, method) (( int32_t (*) (Collection_1_t2376 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m17916_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m17917_gshared (Collection_1_t2376 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m17917(__this, ___value, method) (( bool (*) (Collection_1_t2376 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m17917_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m17918_gshared (Collection_1_t2376 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m17918(__this, ___value, method) (( int32_t (*) (Collection_1_t2376 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m17918_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m17919_gshared (Collection_1_t2376 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m17919(__this, ___index, ___value, method) (( void (*) (Collection_1_t2376 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m17919_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m17920_gshared (Collection_1_t2376 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m17920(__this, ___value, method) (( void (*) (Collection_1_t2376 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m17920_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m17921_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m17921(__this, method) (( bool (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m17921_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m17922_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m17922(__this, method) (( Object_t * (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m17922_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m17923_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m17923(__this, method) (( bool (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m17923_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m17924_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m17924(__this, method) (( bool (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m17924_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m17925_gshared (Collection_1_t2376 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m17925(__this, ___index, method) (( Object_t * (*) (Collection_1_t2376 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m17925_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m17926_gshared (Collection_1_t2376 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m17926(__this, ___index, ___value, method) (( void (*) (Collection_1_t2376 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m17926_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void Collection_1_Add_m17927_gshared (Collection_1_t2376 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_Add_m17927(__this, ___item, method) (( void (*) (Collection_1_t2376 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_Add_m17927_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void Collection_1_Clear_m17928_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_Clear_m17928(__this, method) (( void (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_Clear_m17928_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m17929_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m17929(__this, method) (( void (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_ClearItems_m17929_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m17930_gshared (Collection_1_t2376 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_Contains_m17930(__this, ___item, method) (( bool (*) (Collection_1_t2376 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_Contains_m17930_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m17931_gshared (Collection_1_t2376 * __this, CustomAttributeTypedArgumentU5BU5D_t1766* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m17931(__this, ___array, ___index, method) (( void (*) (Collection_1_t2376 *, CustomAttributeTypedArgumentU5BU5D_t1766*, int32_t, const MethodInfo*))Collection_1_CopyTo_m17931_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m17932_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m17932(__this, method) (( Object_t* (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_GetEnumerator_m17932_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m17933_gshared (Collection_1_t2376 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m17933(__this, ___item, method) (( int32_t (*) (Collection_1_t2376 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_IndexOf_m17933_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m17934_gshared (Collection_1_t2376 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_Insert_m17934(__this, ___index, ___item, method) (( void (*) (Collection_1_t2376 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_Insert_m17934_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m17935_gshared (Collection_1_t2376 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m17935(__this, ___index, ___item, method) (( void (*) (Collection_1_t2376 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_InsertItem_m17935_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m17936_gshared (Collection_1_t2376 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_Remove_m17936(__this, ___item, method) (( bool (*) (Collection_1_t2376 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_Remove_m17936_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m17937_gshared (Collection_1_t2376 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m17937(__this, ___index, method) (( void (*) (Collection_1_t2376 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m17937_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m17938_gshared (Collection_1_t2376 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m17938(__this, ___index, method) (( void (*) (Collection_1_t2376 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m17938_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m17939_gshared (Collection_1_t2376 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m17939(__this, method) (( int32_t (*) (Collection_1_t2376 *, const MethodInfo*))Collection_1_get_Count_m17939_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1326  Collection_1_get_Item_m17940_gshared (Collection_1_t2376 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m17940(__this, ___index, method) (( CustomAttributeTypedArgument_t1326  (*) (Collection_1_t2376 *, int32_t, const MethodInfo*))Collection_1_get_Item_m17940_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m17941_gshared (Collection_1_t2376 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m17941(__this, ___index, ___value, method) (( void (*) (Collection_1_t2376 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_set_Item_m17941_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m17942_gshared (Collection_1_t2376 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m17942(__this, ___index, ___item, method) (( void (*) (Collection_1_t2376 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))Collection_1_SetItem_m17942_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m17943_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m17943(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m17943_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeTypedArgument_t1326  Collection_1_ConvertItem_m17944_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m17944(__this /* static, unused */, ___item, method) (( CustomAttributeTypedArgument_t1326  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m17944_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m17945_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m17945(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m17945_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m17946_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m17946(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m17946_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeTypedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m17947_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m17947(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m17947_gshared)(__this /* static, unused */, ___list, method)
