﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// Car_Health
struct Car_Health_t87;

#include "mscorlib_System_Object.h"

// Car_Health/<Arrumar>c__Iterator0
struct  U3CArrumarU3Ec__Iterator0_t91  : public Object_t
{
	// System.Int32 Car_Health/<Arrumar>c__Iterator0::$PC
	int32_t ___U24PC_0;
	// System.Object Car_Health/<Arrumar>c__Iterator0::$current
	Object_t * ___U24current_1;
	// Car_Health Car_Health/<Arrumar>c__Iterator0::<>f__this
	Car_Health_t87 * ___U3CU3Ef__this_2;
};
