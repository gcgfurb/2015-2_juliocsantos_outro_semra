﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform
struct Transform_t33;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// OrbitCamera
struct  OrbitCamera_t85  : public MonoBehaviour_t2
{
	// UnityEngine.Transform OrbitCamera::target
	Transform_t33 * ___target_2;
	// System.Boolean OrbitCamera::autoRotateOn
	bool ___autoRotateOn_3;
	// System.Boolean OrbitCamera::autoRotateReverse
	bool ___autoRotateReverse_4;
	// System.Single OrbitCamera::autoRotateSpeed
	float ___autoRotateSpeed_5;
	// System.Single OrbitCamera::originalAutoRotateSpeed
	float ___originalAutoRotateSpeed_6;
	// System.Single OrbitCamera::autoRotateSpeedFast
	float ___autoRotateSpeedFast_7;
	// System.Single OrbitCamera::autoRotateValue
	float ___autoRotateValue_8;
	// System.Single OrbitCamera::distance
	float ___distance_9;
	// System.Single OrbitCamera::xSpeed
	float ___xSpeed_10;
	// System.Single OrbitCamera::ySpeed
	float ___ySpeed_11;
	// System.Single OrbitCamera::yMinLimit
	float ___yMinLimit_12;
	// System.Single OrbitCamera::yMaxLimit
	float ___yMaxLimit_13;
	// System.Single OrbitCamera::distanceMin
	float ___distanceMin_14;
	// System.Single OrbitCamera::distanceMax
	float ___distanceMax_15;
	// System.Single OrbitCamera::smoothTime
	float ___smoothTime_16;
	// System.Single OrbitCamera::autoTimer
	float ___autoTimer_17;
	// System.Single OrbitCamera::rotationYAxis
	float ___rotationYAxis_18;
	// System.Single OrbitCamera::rotationXAxis
	float ___rotationXAxis_19;
	// System.Single OrbitCamera::velocityX
	float ___velocityX_20;
	// System.Single OrbitCamera::velocityY
	float ___velocityY_21;
	// System.Boolean OrbitCamera::faster
	bool ___faster_22;
	// System.Boolean OrbitCamera::rkeyActive
	bool ___rkeyActive_23;
};
