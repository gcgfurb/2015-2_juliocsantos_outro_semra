﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m14938(__this, ___l, method) (( void (*) (Enumerator_t2103 *, List_1_t303 *, const MethodInfo*))Enumerator__ctor_m11324_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m14939(__this, method) (( void (*) (Enumerator_t2103 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11325_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14940(__this, method) (( Object_t * (*) (Enumerator_t2103 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11326_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::Dispose()
#define Enumerator_Dispose_m14941(__this, method) (( void (*) (Enumerator_t2103 *, const MethodInfo*))Enumerator_Dispose_m11327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::VerifyState()
#define Enumerator_VerifyState_m14942(__this, method) (( void (*) (Enumerator_t2103 *, const MethodInfo*))Enumerator_VerifyState_m11328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::MoveNext()
#define Enumerator_MoveNext_m14943(__this, method) (( bool (*) (Enumerator_t2103 *, const MethodInfo*))Enumerator_MoveNext_m11329_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.RectTransform>::get_Current()
#define Enumerator_get_Current_m14944(__this, method) (( RectTransform_t95 * (*) (Enumerator_t2103 *, const MethodInfo*))Enumerator_get_Current_m11330_gshared)(__this, method)
