﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// Car_Health
struct Car_Health_t87;

#include "mscorlib_System_Object.h"

// Car_Health/<Abastecer>c__Iterator1
struct  U3CAbastecerU3Ec__Iterator1_t92  : public Object_t
{
	// System.Int32 Car_Health/<Abastecer>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object Car_Health/<Abastecer>c__Iterator1::$current
	Object_t * ___U24current_1;
	// Car_Health Car_Health/<Abastecer>c__Iterator1::<>f__this
	Car_Health_t87 * ___U3CU3Ef__this_2;
};
