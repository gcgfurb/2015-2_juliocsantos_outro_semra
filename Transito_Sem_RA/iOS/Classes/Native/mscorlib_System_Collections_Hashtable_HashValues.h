﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Collections.Hashtable
struct Hashtable_t794;

#include "mscorlib_System_Object.h"

// System.Collections.Hashtable/HashValues
struct  HashValues_t1196  : public Object_t
{
	// System.Collections.Hashtable System.Collections.Hashtable/HashValues::host
	Hashtable_t794 * ___host_0;
};
