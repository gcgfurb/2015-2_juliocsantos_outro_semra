﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t345;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t2458;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UIVertex>
struct ICollection_1_t408;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UIVertex>
struct IEnumerable_1_t2459;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UIVertex>
struct ReadOnlyCollection_1_t2020;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t234;
// System.Predicate`1<UnityEngine.UIVertex>
struct Predicate_1_t2024;
// System.Comparison`1<UnityEngine.UIVertex>
struct Comparison_1_t2027;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_17.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor()
extern "C" void List_1__ctor_m13629_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1__ctor_m13629(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1__ctor_m13629_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.ctor(System.Int32)
extern "C" void List_1__ctor_m3611_gshared (List_1_t345 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m3611(__this, ___capacity, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1__ctor_m3611_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::.cctor()
extern "C" void List_1__cctor_m13630_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m13630(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m13630_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13631_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13631(__this, method) (( Object_t* (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m13631_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m13632_gshared (List_1_t345 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m13632(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t345 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m13632_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m13633_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m13633(__this, method) (( Object_t * (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m13633_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m13634_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m13634(__this, ___item, method) (( int32_t (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m13634_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m13635_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m13635(__this, ___item, method) (( bool (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m13635_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m13636_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m13636(__this, ___item, method) (( int32_t (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m13636_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m13637_gshared (List_1_t345 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m13637(__this, ___index, ___item, method) (( void (*) (List_1_t345 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m13637_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m13638_gshared (List_1_t345 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m13638(__this, ___item, method) (( void (*) (List_1_t345 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m13638_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13639_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13639(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13639_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m13640_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m13640(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m13640_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m13641_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m13641(__this, method) (( Object_t * (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m13641_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m13642_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m13642(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m13642_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m13643_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m13643(__this, method) (( bool (*) (List_1_t345 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m13643_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m13644_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m13644(__this, ___index, method) (( Object_t * (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m13644_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m13645_gshared (List_1_t345 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m13645(__this, ___index, ___value, method) (( void (*) (List_1_t345 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m13645_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Add(T)
extern "C" void List_1_Add_m13646_gshared (List_1_t345 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define List_1_Add_m13646(__this, ___item, method) (( void (*) (List_1_t345 *, UIVertex_t239 , const MethodInfo*))List_1_Add_m13646_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m13647_gshared (List_1_t345 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m13647(__this, ___newCount, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m13647_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m13648_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m13648(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_AddCollection_m13648_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m13649_gshared (List_1_t345 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m13649(__this, ___enumerable, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m13649_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m13650_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m13650(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_AddRange_m13650_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2020 * List_1_AsReadOnly_m13651_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m13651(__this, method) (( ReadOnlyCollection_1_t2020 * (*) (List_1_t345 *, const MethodInfo*))List_1_AsReadOnly_m13651_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Clear()
extern "C" void List_1_Clear_m13652_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_Clear_m13652(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_Clear_m13652_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool List_1_Contains_m13653_gshared (List_1_t345 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define List_1_Contains_m13653(__this, ___item, method) (( bool (*) (List_1_t345 *, UIVertex_t239 , const MethodInfo*))List_1_Contains_m13653_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m13654_gshared (List_1_t345 * __this, UIVertexU5BU5D_t234* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m13654(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t345 *, UIVertexU5BU5D_t234*, int32_t, const MethodInfo*))List_1_CopyTo_m13654_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::Find(System.Predicate`1<T>)
extern "C" UIVertex_t239  List_1_Find_m13655_gshared (List_1_t345 * __this, Predicate_1_t2024 * ___match, const MethodInfo* method);
#define List_1_Find_m13655(__this, ___match, method) (( UIVertex_t239  (*) (List_1_t345 *, Predicate_1_t2024 *, const MethodInfo*))List_1_Find_m13655_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m13656_gshared (Object_t * __this /* static, unused */, Predicate_1_t2024 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m13656(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2024 *, const MethodInfo*))List_1_CheckMatch_m13656_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m13657_gshared (List_1_t345 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2024 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m13657(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t345 *, int32_t, int32_t, Predicate_1_t2024 *, const MethodInfo*))List_1_GetIndex_m13657_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Enumerator_t2019  List_1_GetEnumerator_m13658_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m13658(__this, method) (( Enumerator_t2019  (*) (List_1_t345 *, const MethodInfo*))List_1_GetEnumerator_m13658_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m13659_gshared (List_1_t345 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define List_1_IndexOf_m13659(__this, ___item, method) (( int32_t (*) (List_1_t345 *, UIVertex_t239 , const MethodInfo*))List_1_IndexOf_m13659_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m13660_gshared (List_1_t345 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m13660(__this, ___start, ___delta, method) (( void (*) (List_1_t345 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m13660_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m13661_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m13661(__this, ___index, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_CheckIndex_m13661_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m13662_gshared (List_1_t345 * __this, int32_t ___index, UIVertex_t239  ___item, const MethodInfo* method);
#define List_1_Insert_m13662(__this, ___index, ___item, method) (( void (*) (List_1_t345 *, int32_t, UIVertex_t239 , const MethodInfo*))List_1_Insert_m13662_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m13663_gshared (List_1_t345 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m13663(__this, ___collection, method) (( void (*) (List_1_t345 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m13663_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool List_1_Remove_m13664_gshared (List_1_t345 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define List_1_Remove_m13664(__this, ___item, method) (( bool (*) (List_1_t345 *, UIVertex_t239 , const MethodInfo*))List_1_Remove_m13664_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m13665_gshared (List_1_t345 * __this, Predicate_1_t2024 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m13665(__this, ___match, method) (( int32_t (*) (List_1_t345 *, Predicate_1_t2024 *, const MethodInfo*))List_1_RemoveAll_m13665_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m13666_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m13666(__this, ___index, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_RemoveAt_m13666_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Reverse()
extern "C" void List_1_Reverse_m13667_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_Reverse_m13667(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_Reverse_m13667_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort()
extern "C" void List_1_Sort_m13668_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_Sort_m13668(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_Sort_m13668_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m13669_gshared (List_1_t345 * __this, Comparison_1_t2027 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m13669(__this, ___comparison, method) (( void (*) (List_1_t345 *, Comparison_1_t2027 *, const MethodInfo*))List_1_Sort_m13669_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UIVertex>::ToArray()
extern "C" UIVertexU5BU5D_t234* List_1_ToArray_m13670_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_ToArray_m13670(__this, method) (( UIVertexU5BU5D_t234* (*) (List_1_t345 *, const MethodInfo*))List_1_ToArray_m13670_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::TrimExcess()
extern "C" void List_1_TrimExcess_m13671_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m13671(__this, method) (( void (*) (List_1_t345 *, const MethodInfo*))List_1_TrimExcess_m13671_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m2441_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m2441(__this, method) (( int32_t (*) (List_1_t345 *, const MethodInfo*))List_1_get_Capacity_m2441_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m2442_gshared (List_1_t345 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m2442(__this, ___value, method) (( void (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_set_Capacity_m2442_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t List_1_get_Count_m13672_gshared (List_1_t345 * __this, const MethodInfo* method);
#define List_1_get_Count_m13672(__this, method) (( int32_t (*) (List_1_t345 *, const MethodInfo*))List_1_get_Count_m13672_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t239  List_1_get_Item_m13673_gshared (List_1_t345 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m13673(__this, ___index, method) (( UIVertex_t239  (*) (List_1_t345 *, int32_t, const MethodInfo*))List_1_get_Item_m13673_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m13674_gshared (List_1_t345 * __this, int32_t ___index, UIVertex_t239  ___value, const MethodInfo* method);
#define List_1_set_Item_m13674(__this, ___index, ___value, method) (( void (*) (List_1_t345 *, int32_t, UIVertex_t239 , const MethodInfo*))List_1_set_Item_m13674_gshared)(__this, ___index, ___value, method)
