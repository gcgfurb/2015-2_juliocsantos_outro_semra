﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Color32>
struct List_1_t313;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_28.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15309_gshared (Enumerator_t2128 * __this, List_1_t313 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15309(__this, ___l, method) (( void (*) (Enumerator_t2128 *, List_1_t313 *, const MethodInfo*))Enumerator__ctor_m15309_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15310_gshared (Enumerator_t2128 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15310(__this, method) (( void (*) (Enumerator_t2128 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15310_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15311_gshared (Enumerator_t2128 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15311(__this, method) (( Object_t * (*) (Enumerator_t2128 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15311_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::Dispose()
extern "C" void Enumerator_Dispose_m15312_gshared (Enumerator_t2128 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15312(__this, method) (( void (*) (Enumerator_t2128 *, const MethodInfo*))Enumerator_Dispose_m15312_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::VerifyState()
extern "C" void Enumerator_VerifyState_m15313_gshared (Enumerator_t2128 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15313(__this, method) (( void (*) (Enumerator_t2128 *, const MethodInfo*))Enumerator_VerifyState_m15313_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15314_gshared (Enumerator_t2128 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15314(__this, method) (( bool (*) (Enumerator_t2128 *, const MethodInfo*))Enumerator_MoveNext_m15314_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Color32>::get_Current()
extern "C" Color32_t347  Enumerator_get_Current_m15315_gshared (Enumerator_t2128 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15315(__this, method) (( Color32_t347  (*) (Enumerator_t2128 *, const MethodInfo*))Enumerator_get_Current_m15315_gshared)(__this, method)
