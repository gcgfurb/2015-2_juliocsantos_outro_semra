﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.Mudguard
struct Mudguard_t48;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.Mudguard::.ctor()
extern "C" void Mudguard__ctor_m181 (Mudguard_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Start()
extern "C" void Mudguard_Start_m182 (Mudguard_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Update()
extern "C" void Mudguard_Update_m183 (Mudguard_t48 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
