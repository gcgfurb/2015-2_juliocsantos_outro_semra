﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ClientActivatedIdentity
struct ClientActivatedIdentity_t1484;
// System.MarshalByRefObject
struct MarshalByRefObject_t947;

#include "codegen/il2cpp-codegen.h"

// System.MarshalByRefObject System.Runtime.Remoting.ClientActivatedIdentity::GetServerObject()
extern "C" MarshalByRefObject_t947 * ClientActivatedIdentity_GetServerObject_m9044 (ClientActivatedIdentity_t1484 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
