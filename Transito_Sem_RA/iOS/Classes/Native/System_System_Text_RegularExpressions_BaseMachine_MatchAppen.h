﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Match
struct Match_t902;
// System.Text.StringBuilder
struct StringBuilder_t356;
// System.IAsyncResult
struct IAsyncResult_t228;
// System.AsyncCallback
struct AsyncCallback_t229;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// System.Text.RegularExpressions.BaseMachine/MatchAppendEvaluator
struct  MatchAppendEvaluator_t984  : public MulticastDelegate_t227
{
};
