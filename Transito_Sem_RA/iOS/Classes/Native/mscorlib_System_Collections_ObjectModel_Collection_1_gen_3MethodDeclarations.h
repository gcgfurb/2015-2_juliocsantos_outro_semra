﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Int32>
struct Collection_1_t2121;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Int32[]
struct Int32U5BU5D_t426;
// System.Collections.Generic.IEnumerator`1<System.Int32>
struct IEnumerator_1_t2443;
// System.Collections.Generic.IList`1<System.Int32>
struct IList_1_t2120;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::.ctor()
extern "C" void Collection_1__ctor_m15203_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15203(__this, method) (( void (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1__ctor_m15203_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15204_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15204(__this, method) (( bool (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15204_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15205_gshared (Collection_1_t2121 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15205(__this, ___array, ___index, method) (( void (*) (Collection_1_t2121 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15205_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15206_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15206(__this, method) (( Object_t * (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15206_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15207_gshared (Collection_1_t2121 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15207(__this, ___value, method) (( int32_t (*) (Collection_1_t2121 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15207_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15208_gshared (Collection_1_t2121 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15208(__this, ___value, method) (( bool (*) (Collection_1_t2121 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15208_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15209_gshared (Collection_1_t2121 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15209(__this, ___value, method) (( int32_t (*) (Collection_1_t2121 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15209_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15210_gshared (Collection_1_t2121 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15210(__this, ___index, ___value, method) (( void (*) (Collection_1_t2121 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15210_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15211_gshared (Collection_1_t2121 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15211(__this, ___value, method) (( void (*) (Collection_1_t2121 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15211_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15212_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15212(__this, method) (( bool (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15212_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15213_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15213(__this, method) (( Object_t * (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15213_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15214_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15214(__this, method) (( bool (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15214_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15215_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15215(__this, method) (( bool (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15215_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15216_gshared (Collection_1_t2121 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15216(__this, ___index, method) (( Object_t * (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15216_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15217_gshared (Collection_1_t2121 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15217(__this, ___index, ___value, method) (( void (*) (Collection_1_t2121 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15217_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Add(T)
extern "C" void Collection_1_Add_m15218_gshared (Collection_1_t2121 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Add_m15218(__this, ___item, method) (( void (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_Add_m15218_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Clear()
extern "C" void Collection_1_Clear_m15219_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15219(__this, method) (( void (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_Clear_m15219_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::ClearItems()
extern "C" void Collection_1_ClearItems_m15220_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15220(__this, method) (( void (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_ClearItems_m15220_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Contains(T)
extern "C" bool Collection_1_Contains_m15221_gshared (Collection_1_t2121 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Contains_m15221(__this, ___item, method) (( bool (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_Contains_m15221_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15222_gshared (Collection_1_t2121 * __this, Int32U5BU5D_t426* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15222(__this, ___array, ___index, method) (( void (*) (Collection_1_t2121 *, Int32U5BU5D_t426*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15222_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Int32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15223_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15223(__this, method) (( Object_t* (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_GetEnumerator_m15223_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15224_gshared (Collection_1_t2121 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15224(__this, ___item, method) (( int32_t (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_IndexOf_m15224_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15225_gshared (Collection_1_t2121 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_Insert_m15225(__this, ___index, ___item, method) (( void (*) (Collection_1_t2121 *, int32_t, int32_t, const MethodInfo*))Collection_1_Insert_m15225_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15226_gshared (Collection_1_t2121 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15226(__this, ___index, ___item, method) (( void (*) (Collection_1_t2121 *, int32_t, int32_t, const MethodInfo*))Collection_1_InsertItem_m15226_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::Remove(T)
extern "C" bool Collection_1_Remove_m15227_gshared (Collection_1_t2121 * __this, int32_t ___item, const MethodInfo* method);
#define Collection_1_Remove_m15227(__this, ___item, method) (( bool (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_Remove_m15227_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15228_gshared (Collection_1_t2121 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15228(__this, ___index, method) (( void (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15228_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15229_gshared (Collection_1_t2121 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15229(__this, ___index, method) (( void (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15229_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Int32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15230_gshared (Collection_1_t2121 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15230(__this, method) (( int32_t (*) (Collection_1_t2121 *, const MethodInfo*))Collection_1_get_Count_m15230_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::get_Item(System.Int32)
extern "C" int32_t Collection_1_get_Item_m15231_gshared (Collection_1_t2121 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15231(__this, ___index, method) (( int32_t (*) (Collection_1_t2121 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15231_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15232_gshared (Collection_1_t2121 * __this, int32_t ___index, int32_t ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15232(__this, ___index, ___value, method) (( void (*) (Collection_1_t2121 *, int32_t, int32_t, const MethodInfo*))Collection_1_set_Item_m15232_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15233_gshared (Collection_1_t2121 * __this, int32_t ___index, int32_t ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15233(__this, ___index, ___item, method) (( void (*) (Collection_1_t2121 *, int32_t, int32_t, const MethodInfo*))Collection_1_SetItem_m15233_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15234_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15234(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15234_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Int32>::ConvertItem(System.Object)
extern "C" int32_t Collection_1_ConvertItem_m15235_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15235(__this /* static, unused */, ___item, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15235_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Int32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15236_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15236(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15236_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15237_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15237(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15237_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Int32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15238_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15238(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15238_gshared)(__this /* static, unused */, ___list, method)
