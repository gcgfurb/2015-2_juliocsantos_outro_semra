﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.EventSystems.IDragHandler
struct IDragHandler_t331;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t107;
// System.IAsyncResult
struct IAsyncResult_t228;
// System.AsyncCallback
struct AsyncCallback_t229;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"
#include "mscorlib_System_Void.h"

// UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IDragHandler>
struct  EventFunction_1_t123  : public MulticastDelegate_t227
{
};
