﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Threading.ThreadAbortException
struct ThreadAbortException_t1625;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t652;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.Threading.ThreadAbortException::.ctor()
extern "C" void ThreadAbortException__ctor_m9969 (ThreadAbortException_t1625 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Threading.ThreadAbortException::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void ThreadAbortException__ctor_m9970 (ThreadAbortException_t1625 * __this, SerializationInfo_t652 * ___info, StreamingContext_t653  ___sc, const MethodInfo* method) IL2CPP_METHOD_ATTR;
