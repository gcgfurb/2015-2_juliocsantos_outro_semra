﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.OperatingSystem
struct OperatingSystem_t1667;

#include "mscorlib_System_Object.h"

// System.Environment
struct  Environment_t1666  : public Object_t
{
};
struct Environment_t1666_StaticFields{
	// System.OperatingSystem System.Environment::os
	OperatingSystem_t1667 * ___os_0;
};
