﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::.ctor()
#define Stack_1__ctor_m15801(__this, method) (( void (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1__ctor_m11915_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m15802(__this, method) (( bool (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m11916_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m15803(__this, method) (( Object_t * (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m11917_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m15804(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t2177 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m11918_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m15805(__this, method) (( Object_t* (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11919_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m15806(__this, method) (( Object_t * (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m11920_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::Peek()
#define Stack_1_Peek_m15807(__this, method) (( List_1_t316 * (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_Peek_m11921_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::Pop()
#define Stack_1_Pop_m15808(__this, method) (( List_1_t316 * (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_Pop_m11922_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::Push(T)
#define Stack_1_Push_m15809(__this, ___t, method) (( void (*) (Stack_1_t2177 *, List_1_t316 *, const MethodInfo*))Stack_1_Push_m11923_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::get_Count()
#define Stack_1_get_Count_m15810(__this, method) (( int32_t (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_get_Count_m11924_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<System.Int32>>::GetEnumerator()
#define Stack_1_GetEnumerator_m15811(__this, method) (( Enumerator_t2488  (*) (Stack_1_t2177 *, const MethodInfo*))Stack_1_GetEnumerator_m11925_gshared)(__this, method)
