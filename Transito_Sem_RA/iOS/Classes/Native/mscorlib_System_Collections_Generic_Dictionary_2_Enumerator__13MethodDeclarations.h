﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2268;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__13.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17073_gshared (Enumerator_t2275 * __this, Dictionary_2_t2268 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17073(__this, ___dictionary, method) (( void (*) (Enumerator_t2275 *, Dictionary_2_t2268 *, const MethodInfo*))Enumerator__ctor_m17073_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17074_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17074(__this, method) (( Object_t * (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17074_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17075_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17075(__this, method) (( void (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17075_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1057  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17076_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17076(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17076_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17077_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17077(__this, method) (( Object_t * (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17077_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17078_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17078(__this, method) (( Object_t * (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17079_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17079(__this, method) (( bool (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_MoveNext_m17079_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" KeyValuePair_2_t2270  Enumerator_get_Current_m17080_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17080(__this, method) (( KeyValuePair_2_t2270  (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_get_Current_m17080_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m17081_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17081(__this, method) (( Object_t * (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_get_CurrentKey_m17081_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
extern "C" int32_t Enumerator_get_CurrentValue_m17082_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17082(__this, method) (( int32_t (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_get_CurrentValue_m17082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void Enumerator_Reset_m17083_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_Reset_m17083(__this, method) (( void (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_Reset_m17083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyState()
extern "C" void Enumerator_VerifyState_m17084_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17084(__this, method) (( void (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_VerifyState_m17084_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17085_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17085(__this, method) (( void (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_VerifyCurrent_m17085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m17086_gshared (Enumerator_t2275 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17086(__this, method) (( void (*) (Enumerator_t2275 *, const MethodInfo*))Enumerator_Dispose_m17086_gshared)(__this, method)
