﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>
struct ArrayReadOnlyList_1_t2385;
// System.Reflection.CustomAttributeTypedArgument[]
struct CustomAttributeTypedArgumentU5BU5D_t1766;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeTypedArgument>
struct IEnumerator_1_t2508;
// System.Exception
struct Exception_t68;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m18026_gshared (ArrayReadOnlyList_1_t2385 * __this, CustomAttributeTypedArgumentU5BU5D_t1766* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m18026(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, CustomAttributeTypedArgumentU5BU5D_t1766*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m18026_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18027_gshared (ArrayReadOnlyList_1_t2385 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18027(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2385 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m18027_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeTypedArgument_t1326  ArrayReadOnlyList_1_get_Item_m18028_gshared (ArrayReadOnlyList_1_t2385 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m18028(__this, ___index, method) (( CustomAttributeTypedArgument_t1326  (*) (ArrayReadOnlyList_1_t2385 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m18028_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m18029_gshared (ArrayReadOnlyList_1_t2385 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m18029(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))ArrayReadOnlyList_1_set_Item_m18029_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m18030_gshared (ArrayReadOnlyList_1_t2385 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m18030(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2385 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m18030_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m18031_gshared (ArrayReadOnlyList_1_t2385 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m18031(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2385 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m18031_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m18032_gshared (ArrayReadOnlyList_1_t2385 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m18032(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))ArrayReadOnlyList_1_Add_m18032_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m18033_gshared (ArrayReadOnlyList_1_t2385 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m18033(__this, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m18033_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m18034_gshared (ArrayReadOnlyList_1_t2385 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m18034(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2385 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))ArrayReadOnlyList_1_Contains_m18034_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m18035_gshared (ArrayReadOnlyList_1_t2385 * __this, CustomAttributeTypedArgumentU5BU5D_t1766* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m18035(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, CustomAttributeTypedArgumentU5BU5D_t1766*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m18035_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m18036_gshared (ArrayReadOnlyList_1_t2385 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m18036(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2385 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m18036_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m18037_gshared (ArrayReadOnlyList_1_t2385 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m18037(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2385 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m18037_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m18038_gshared (ArrayReadOnlyList_1_t2385 * __this, int32_t ___index, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m18038(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, int32_t, CustomAttributeTypedArgument_t1326 , const MethodInfo*))ArrayReadOnlyList_1_Insert_m18038_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m18039_gshared (ArrayReadOnlyList_1_t2385 * __this, CustomAttributeTypedArgument_t1326  ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m18039(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2385 *, CustomAttributeTypedArgument_t1326 , const MethodInfo*))ArrayReadOnlyList_1_Remove_m18039_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m18040_gshared (ArrayReadOnlyList_1_t2385 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m18040(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2385 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m18040_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Reflection.CustomAttributeTypedArgument>::ReadOnlyError()
extern "C" Exception_t68 * ArrayReadOnlyList_1_ReadOnlyError_m18041_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m18041(__this /* static, unused */, method) (( Exception_t68 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m18041_gshared)(__this /* static, unused */, method)
