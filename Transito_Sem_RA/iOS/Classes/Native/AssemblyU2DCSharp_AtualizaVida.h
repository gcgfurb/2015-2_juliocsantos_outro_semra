﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityStandardAssets.Vehicles.Car.CarController
struct CarController_t29;
// Car_Health
struct Car_Health_t87;
// System.Collections.Generic.Dictionary`2<System.String,System.Int32>
struct Dictionary_2_t88;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// AtualizaVida
struct  AtualizaVida_t86  : public MonoBehaviour_t2
{
	// UnityStandardAssets.Vehicles.Car.CarController AtualizaVida::m_Carro
	CarController_t29 * ___m_Carro_2;
	// Car_Health AtualizaVida::m_CarHealth
	Car_Health_t87 * ___m_CarHealth_3;
	// System.Single AtualizaVida::m_KmLitros
	float ___m_KmLitros_4;
	// System.Single AtualizaVida::multiplicadorKML
	float ___multiplicadorKML_5;
};
struct AtualizaVida_t86_StaticFields{
	// System.Collections.Generic.Dictionary`2<System.String,System.Int32> AtualizaVida::<>f__switch$map0
	Dictionary_2_t88 * ___U3CU3Ef__switchU24map0_6;
};
