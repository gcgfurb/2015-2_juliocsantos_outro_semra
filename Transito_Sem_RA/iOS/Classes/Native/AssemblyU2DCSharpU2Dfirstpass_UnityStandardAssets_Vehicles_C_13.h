﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// UnityStandardAssets.Vehicles.Car.WheelEffects
struct WheelEffects_t54;

#include "mscorlib_System_Object.h"

// UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1
struct  U3CStartSkidTrailU3Ec__Iterator1_t53  : public Object_t
{
	// System.Int32 UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::$PC
	int32_t ___U24PC_0;
	// System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::$current
	Object_t * ___U24current_1;
	// UnityStandardAssets.Vehicles.Car.WheelEffects UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::<>f__this
	WheelEffects_t54 * ___U3CU3Ef__this_2;
};
