﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>
struct U3CGetEnumeratorU3Ec__Iterator0_t2397;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0__ctor_m18208_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2397 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0__ctor_m18208(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2397 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0__ctor_m18208_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.IEnumerator<T>.get_Current()
extern "C" CustomAttributeNamedArgument_t1325  U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18209_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2397 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18209(__this, method) (( CustomAttributeNamedArgument_t1325  (*) (U3CGetEnumeratorU3Ec__Iterator0_t2397 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CTU3E_get_Current_m18209_gshared)(__this, method)
// System.Object System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18210_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2397 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18210(__this, method) (( Object_t * (*) (U3CGetEnumeratorU3Ec__Iterator0_t2397 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m18210_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18211_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2397 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18211(__this, method) (( bool (*) (U3CGetEnumeratorU3Ec__Iterator0_t2397 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_MoveNext_m18211_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18212_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2397 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18212(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2397 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Dispose_m18212_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1/<GetEnumerator>c__Iterator0<System.Reflection.CustomAttributeNamedArgument>::Reset()
extern "C" void U3CGetEnumeratorU3Ec__Iterator0_Reset_m18213_gshared (U3CGetEnumeratorU3Ec__Iterator0_t2397 * __this, const MethodInfo* method);
#define U3CGetEnumeratorU3Ec__Iterator0_Reset_m18213(__this, method) (( void (*) (U3CGetEnumeratorU3Ec__Iterator0_t2397 *, const MethodInfo*))U3CGetEnumeratorU3Ec__Iterator0_Reset_m18213_gshared)(__this, method)
