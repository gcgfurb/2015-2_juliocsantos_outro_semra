﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WheelHit.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityEngine.Vector3 UnityEngine.WheelHit::get_normal()
extern "C" Vector3_t12  WheelHit_get_normal_m316 (WheelHit_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelHit::get_forwardSlip()
extern "C" float WheelHit_get_forwardSlip_m325 (WheelHit_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.WheelHit::get_sidewaysSlip()
extern "C" float WheelHit_get_sidewaysSlip_m326 (WheelHit_t75 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
