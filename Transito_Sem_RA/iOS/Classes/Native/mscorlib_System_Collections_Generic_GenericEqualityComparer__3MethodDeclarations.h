﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.Int32>
struct GenericEqualityComparer_1_t1873;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m11655_gshared (GenericEqualityComparer_1_t1873 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m11655(__this, method) (( void (*) (GenericEqualityComparer_1_t1873 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m11655_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m11656_gshared (GenericEqualityComparer_1_t1873 * __this, int32_t ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m11656(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1873 *, int32_t, const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m11656_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.Int32>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m11657_gshared (GenericEqualityComparer_1_t1873 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m11657(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1873 *, int32_t, int32_t, const MethodInfo*))GenericEqualityComparer_1_Equals_m11657_gshared)(__this, ___x, ___y, method)
