﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Car_Health
struct Car_Health_t87;
// System.Collections.IEnumerator
struct IEnumerator_t59;

#include "codegen/il2cpp-codegen.h"

// System.Void Car_Health::.ctor()
extern "C" void Car_Health__ctor_m389 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Car_Health::GetCombustivelAtual()
extern "C" float Car_Health_GetCombustivelAtual_m390 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Car_Health::GetMecanicaAtual()
extern "C" float Car_Health_GetMecanicaAtual_m391 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Car_Health::IsMorto()
extern "C" bool Car_Health_IsMorto_m392 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Car_Health::IsCombustivelBaixo()
extern "C" bool Car_Health_IsCombustivelBaixo_m393 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Car_Health::IsMecanicaBaixa()
extern "C" bool Car_Health_IsMecanicaBaixa_m394 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::Start()
extern "C" void Car_Health_Start_m395 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::OnEnable()
extern "C" void Car_Health_OnEnable_m396 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::Update()
extern "C" void Car_Health_Update_m397 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::GastaMecanica(System.Single)
extern "C" void Car_Health_GastaMecanica_m398 (Car_Health_t87 * __this, float ___quantidade, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::AddMecanica()
extern "C" void Car_Health_AddMecanica_m399 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Car_Health::Arrumar()
extern "C" Object_t * Car_Health_Arrumar_m400 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::GastaCombustivel(System.Single)
extern "C" void Car_Health_GastaCombustivel_m401 (Car_Health_t87 * __this, float ___quantidade, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::AddCombustivel()
extern "C" void Car_Health_AddCombustivel_m402 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator Car_Health::Abastecer()
extern "C" Object_t * Car_Health_Abastecer_m403 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::SetHealthUI()
extern "C" void Car_Health_SetHealthUI_m404 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health::OnDeath()
extern "C" void Car_Health_OnDeath_m405 (Car_Health_t87 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
