﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void RSAParameters_t831_marshal(const RSAParameters_t831& unmarshaled, RSAParameters_t831_marshaled& marshaled);
extern "C" void RSAParameters_t831_marshal_back(const RSAParameters_t831_marshaled& marshaled, RSAParameters_t831& unmarshaled);
extern "C" void RSAParameters_t831_marshal_cleanup(RSAParameters_t831_marshaled& marshaled);
