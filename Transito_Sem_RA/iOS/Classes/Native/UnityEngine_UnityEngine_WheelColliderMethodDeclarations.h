﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.WheelCollider
struct WheelCollider_t56;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_WheelHit.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Single UnityEngine.WheelCollider::get_radius()
extern "C" float WheelCollider_get_radius_m338 (WheelCollider_t56 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_motorTorque(System.Single)
extern "C" void WheelCollider_set_motorTorque_m314 (WheelCollider_t56 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_brakeTorque(System.Single)
extern "C" void WheelCollider_set_brakeTorque_m311 (WheelCollider_t56 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::set_steerAngle(System.Single)
extern "C" void WheelCollider_set_steerAngle_m310 (WheelCollider_t56 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.WheelCollider::GetGroundHit(UnityEngine.WheelHit&)
extern "C" bool WheelCollider_GetGroundHit_m315 (WheelCollider_t56 * __this, WheelHit_t75 * ___hit, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.WheelCollider::GetWorldPose(UnityEngine.Vector3&,UnityEngine.Quaternion&)
extern "C" void WheelCollider_GetWorldPose_m308 (WheelCollider_t56 * __this, Vector3_t12 * ___pos, Quaternion_t45 * ___quat, const MethodInfo* method) IL2CPP_METHOD_ATTR;
