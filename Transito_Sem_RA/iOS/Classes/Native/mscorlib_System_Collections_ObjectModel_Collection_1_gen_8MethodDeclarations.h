﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>
struct Collection_1_t2230;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t648;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UILineInfo>
struct IEnumerator_1_t2493;
// System.Collections.Generic.IList`1<UnityEngine.UILineInfo>
struct IList_1_t395;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::.ctor()
extern "C" void Collection_1__ctor_m16479_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16479(__this, method) (( void (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1__ctor_m16479_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16480_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16480(__this, method) (( bool (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16480_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16481_gshared (Collection_1_t2230 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16481(__this, ___array, ___index, method) (( void (*) (Collection_1_t2230 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16481_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16482_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16482(__this, method) (( Object_t * (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16482_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16483_gshared (Collection_1_t2230 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16483(__this, ___value, method) (( int32_t (*) (Collection_1_t2230 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16483_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16484_gshared (Collection_1_t2230 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16484(__this, ___value, method) (( bool (*) (Collection_1_t2230 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16484_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16485_gshared (Collection_1_t2230 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16485(__this, ___value, method) (( int32_t (*) (Collection_1_t2230 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16485_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16486_gshared (Collection_1_t2230 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16486(__this, ___index, ___value, method) (( void (*) (Collection_1_t2230 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16486_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16487_gshared (Collection_1_t2230 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16487(__this, ___value, method) (( void (*) (Collection_1_t2230 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16487_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16488_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16488(__this, method) (( bool (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16488_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16489_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16489(__this, method) (( Object_t * (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16489_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16490_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16490(__this, method) (( bool (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16490_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16491_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16491(__this, method) (( bool (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16491_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16492_gshared (Collection_1_t2230 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16492(__this, ___index, method) (( Object_t * (*) (Collection_1_t2230 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16492_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16493_gshared (Collection_1_t2230 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16493(__this, ___index, ___value, method) (( void (*) (Collection_1_t2230 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16493_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Add(T)
extern "C" void Collection_1_Add_m16494_gshared (Collection_1_t2230 * __this, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_Add_m16494(__this, ___item, method) (( void (*) (Collection_1_t2230 *, UILineInfo_t394 , const MethodInfo*))Collection_1_Add_m16494_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Clear()
extern "C" void Collection_1_Clear_m16495_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16495(__this, method) (( void (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_Clear_m16495_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m16496_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16496(__this, method) (( void (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_ClearItems_m16496_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m16497_gshared (Collection_1_t2230 * __this, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_Contains_m16497(__this, ___item, method) (( bool (*) (Collection_1_t2230 *, UILineInfo_t394 , const MethodInfo*))Collection_1_Contains_m16497_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16498_gshared (Collection_1_t2230 * __this, UILineInfoU5BU5D_t648* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16498(__this, ___array, ___index, method) (( void (*) (Collection_1_t2230 *, UILineInfoU5BU5D_t648*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16498_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16499_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16499(__this, method) (( Object_t* (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_GetEnumerator_m16499_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16500_gshared (Collection_1_t2230 * __this, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16500(__this, ___item, method) (( int32_t (*) (Collection_1_t2230 *, UILineInfo_t394 , const MethodInfo*))Collection_1_IndexOf_m16500_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16501_gshared (Collection_1_t2230 * __this, int32_t ___index, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_Insert_m16501(__this, ___index, ___item, method) (( void (*) (Collection_1_t2230 *, int32_t, UILineInfo_t394 , const MethodInfo*))Collection_1_Insert_m16501_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16502_gshared (Collection_1_t2230 * __this, int32_t ___index, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16502(__this, ___index, ___item, method) (( void (*) (Collection_1_t2230 *, int32_t, UILineInfo_t394 , const MethodInfo*))Collection_1_InsertItem_m16502_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m16503_gshared (Collection_1_t2230 * __this, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_Remove_m16503(__this, ___item, method) (( bool (*) (Collection_1_t2230 *, UILineInfo_t394 , const MethodInfo*))Collection_1_Remove_m16503_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16504_gshared (Collection_1_t2230 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16504(__this, ___index, method) (( void (*) (Collection_1_t2230 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16504_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16505_gshared (Collection_1_t2230 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16505(__this, ___index, method) (( void (*) (Collection_1_t2230 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16505_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16506_gshared (Collection_1_t2230 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16506(__this, method) (( int32_t (*) (Collection_1_t2230 *, const MethodInfo*))Collection_1_get_Count_m16506_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::get_Item(System.Int32)
extern "C" UILineInfo_t394  Collection_1_get_Item_m16507_gshared (Collection_1_t2230 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16507(__this, ___index, method) (( UILineInfo_t394  (*) (Collection_1_t2230 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16507_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16508_gshared (Collection_1_t2230 * __this, int32_t ___index, UILineInfo_t394  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16508(__this, ___index, ___value, method) (( void (*) (Collection_1_t2230 *, int32_t, UILineInfo_t394 , const MethodInfo*))Collection_1_set_Item_m16508_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16509_gshared (Collection_1_t2230 * __this, int32_t ___index, UILineInfo_t394  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16509(__this, ___index, ___item, method) (( void (*) (Collection_1_t2230 *, int32_t, UILineInfo_t394 , const MethodInfo*))Collection_1_SetItem_m16509_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16510_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16510(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16510_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::ConvertItem(System.Object)
extern "C" UILineInfo_t394  Collection_1_ConvertItem_m16511_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16511(__this /* static, unused */, ___item, method) (( UILineInfo_t394  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16511_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16512_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16512(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16512_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16513_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16513(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16513_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UILineInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16514_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16514(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16514_gshared)(__this /* static, unused */, ___list, method)
