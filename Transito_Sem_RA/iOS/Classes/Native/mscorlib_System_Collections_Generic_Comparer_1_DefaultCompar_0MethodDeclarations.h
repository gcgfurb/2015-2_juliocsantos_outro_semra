﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>
struct DefaultComparer_t1922;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void DefaultComparer__ctor_m12226_gshared (DefaultComparer_t1922 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m12226(__this, method) (( void (*) (DefaultComparer_t1922 *, const MethodInfo*))DefaultComparer__ctor_m12226_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.EventSystems.RaycastResult>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m12227_gshared (DefaultComparer_t1922 * __this, RaycastResult_t139  ___x, RaycastResult_t139  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m12227(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t1922 *, RaycastResult_t139 , RaycastResult_t139 , const MethodInfo*))DefaultComparer_Compare_m12227_gshared)(__this, ___x, ___y, method)
