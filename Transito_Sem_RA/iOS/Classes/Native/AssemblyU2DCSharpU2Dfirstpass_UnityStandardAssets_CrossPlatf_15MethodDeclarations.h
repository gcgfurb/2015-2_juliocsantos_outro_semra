﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.CrossPlatformInput.TiltInput
struct TiltInput_t19;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern "C" void TiltInput__ctor_m105 (TiltInput_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern "C" void TiltInput_OnEnable_m106 (TiltInput_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern "C" void TiltInput_Update_m107 (TiltInput_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern "C" void TiltInput_OnDisable_m108 (TiltInput_t19 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
