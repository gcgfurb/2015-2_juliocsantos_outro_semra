﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Collision
struct Collision_t58;
// UnityEngine.Rigidbody
struct Rigidbody_t34;

#include "codegen/il2cpp-codegen.h"

// UnityEngine.Rigidbody UnityEngine.Collision::get_rigidbody()
extern "C" Rigidbody_t34 * Collision_get_rigidbody_m284 (Collision_t58 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
