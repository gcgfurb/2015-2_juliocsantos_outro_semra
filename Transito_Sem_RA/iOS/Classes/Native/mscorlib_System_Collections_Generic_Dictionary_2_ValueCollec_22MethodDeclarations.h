﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2316;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_22.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17547_gshared (Enumerator_t2322 * __this, Dictionary_2_t2316 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17547(__this, ___host, method) (( void (*) (Enumerator_t2322 *, Dictionary_2_t2316 *, const MethodInfo*))Enumerator__ctor_m17547_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17548_gshared (Enumerator_t2322 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17548(__this, method) (( Object_t * (*) (Enumerator_t2322 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17548_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17549_gshared (Enumerator_t2322 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17549(__this, method) (( void (*) (Enumerator_t2322 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17549_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m17550_gshared (Enumerator_t2322 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17550(__this, method) (( void (*) (Enumerator_t2322 *, const MethodInfo*))Enumerator_Dispose_m17550_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17551_gshared (Enumerator_t2322 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17551(__this, method) (( bool (*) (Enumerator_t2322 *, const MethodInfo*))Enumerator_MoveNext_m17551_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" bool Enumerator_get_Current_m17552_gshared (Enumerator_t2322 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17552(__this, method) (( bool (*) (Enumerator_t2322 *, const MethodInfo*))Enumerator_get_Current_m17552_gshared)(__this, method)
