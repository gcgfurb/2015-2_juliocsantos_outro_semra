﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Security.Cryptography.AsymmetricAlgorithm
struct AsymmetricAlgorithm_t847;
// System.Security.Cryptography.X509Certificates.X509Certificate
struct X509Certificate_t829;
// System.String
struct String_t;
// System.IAsyncResult
struct IAsyncResult_t228;
// System.AsyncCallback
struct AsyncCallback_t229;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// Mono.Security.Protocol.Tls.PrivateKeySelectionCallback
struct  PrivateKeySelectionCallback_t806  : public MulticastDelegate_t227
{
};
