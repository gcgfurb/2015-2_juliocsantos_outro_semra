﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>
struct List_1_t145;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t2447;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.EventSystems.RaycastResult>
struct ICollection_1_t2448;
// System.Collections.Generic.IEnumerable`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerable_1_t2449;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.EventSystems.RaycastResult>
struct ReadOnlyCollection_1_t1915;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1912;
// System.Predicate`1<UnityEngine.EventSystems.RaycastResult>
struct Predicate_1_t1920;
// System.Comparison`1<UnityEngine.EventSystems.RaycastResult>
struct Comparison_1_t108;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_7.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void List_1__ctor_m1905_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1__ctor_m1905(__this, method) (( void (*) (List_1_t145 *, const MethodInfo*))List_1__ctor_m1905_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Int32)
extern "C" void List_1__ctor_m12084_gshared (List_1_t145 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m12084(__this, ___capacity, method) (( void (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1__ctor_m12084_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::.cctor()
extern "C" void List_1__cctor_m12085_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m12085(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m12085_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12086_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12086(__this, method) (( Object_t* (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12086_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m12087_gshared (List_1_t145 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m12087(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t145 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m12087_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m12088_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12088(__this, method) (( Object_t * (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m12088_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m12089_gshared (List_1_t145 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m12089(__this, ___item, method) (( int32_t (*) (List_1_t145 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m12089_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m12090_gshared (List_1_t145 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m12090(__this, ___item, method) (( bool (*) (List_1_t145 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m12090_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m12091_gshared (List_1_t145 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m12091(__this, ___item, method) (( int32_t (*) (List_1_t145 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m12091_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m12092_gshared (List_1_t145 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m12092(__this, ___index, ___item, method) (( void (*) (List_1_t145 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m12092_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m12093_gshared (List_1_t145 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m12093(__this, ___item, method) (( void (*) (List_1_t145 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m12093_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12094_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12094(__this, method) (( bool (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12094_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m12095_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12095(__this, method) (( bool (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m12095_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m12096_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m12096(__this, method) (( Object_t * (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m12096_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m12097_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m12097(__this, method) (( bool (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m12097_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m12098_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m12098(__this, method) (( bool (*) (List_1_t145 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m12098_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m12099_gshared (List_1_t145 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m12099(__this, ___index, method) (( Object_t * (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m12099_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m12100_gshared (List_1_t145 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m12100(__this, ___index, ___value, method) (( void (*) (List_1_t145 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m12100_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void List_1_Add_m12101_gshared (List_1_t145 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define List_1_Add_m12101(__this, ___item, method) (( void (*) (List_1_t145 *, RaycastResult_t139 , const MethodInfo*))List_1_Add_m12101_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m12102_gshared (List_1_t145 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m12102(__this, ___newCount, method) (( void (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m12102_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m12103_gshared (List_1_t145 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m12103(__this, ___collection, method) (( void (*) (List_1_t145 *, Object_t*, const MethodInfo*))List_1_AddCollection_m12103_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m12104_gshared (List_1_t145 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m12104(__this, ___enumerable, method) (( void (*) (List_1_t145 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m12104_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m12105_gshared (List_1_t145 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m12105(__this, ___collection, method) (( void (*) (List_1_t145 *, Object_t*, const MethodInfo*))List_1_AddRange_m12105_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t1915 * List_1_AsReadOnly_m12106_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m12106(__this, method) (( ReadOnlyCollection_1_t1915 * (*) (List_1_t145 *, const MethodInfo*))List_1_AsReadOnly_m12106_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void List_1_Clear_m12107_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_Clear_m12107(__this, method) (( void (*) (List_1_t145 *, const MethodInfo*))List_1_Clear_m12107_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool List_1_Contains_m12108_gshared (List_1_t145 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define List_1_Contains_m12108(__this, ___item, method) (( bool (*) (List_1_t145 *, RaycastResult_t139 , const MethodInfo*))List_1_Contains_m12108_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m12109_gshared (List_1_t145 * __this, RaycastResultU5BU5D_t1912* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m12109(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t145 *, RaycastResultU5BU5D_t1912*, int32_t, const MethodInfo*))List_1_CopyTo_m12109_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Find(System.Predicate`1<T>)
extern "C" RaycastResult_t139  List_1_Find_m12110_gshared (List_1_t145 * __this, Predicate_1_t1920 * ___match, const MethodInfo* method);
#define List_1_Find_m12110(__this, ___match, method) (( RaycastResult_t139  (*) (List_1_t145 *, Predicate_1_t1920 *, const MethodInfo*))List_1_Find_m12110_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m12111_gshared (Object_t * __this /* static, unused */, Predicate_1_t1920 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m12111(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1920 *, const MethodInfo*))List_1_CheckMatch_m12111_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m12112_gshared (List_1_t145 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t1920 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m12112(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t145 *, int32_t, int32_t, Predicate_1_t1920 *, const MethodInfo*))List_1_GetIndex_m12112_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Enumerator_t1914  List_1_GetEnumerator_m12113_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m12113(__this, method) (( Enumerator_t1914  (*) (List_1_t145 *, const MethodInfo*))List_1_GetEnumerator_m12113_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m12114_gshared (List_1_t145 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define List_1_IndexOf_m12114(__this, ___item, method) (( int32_t (*) (List_1_t145 *, RaycastResult_t139 , const MethodInfo*))List_1_IndexOf_m12114_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m12115_gshared (List_1_t145 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m12115(__this, ___start, ___delta, method) (( void (*) (List_1_t145 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m12115_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m12116_gshared (List_1_t145 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m12116(__this, ___index, method) (( void (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1_CheckIndex_m12116_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m12117_gshared (List_1_t145 * __this, int32_t ___index, RaycastResult_t139  ___item, const MethodInfo* method);
#define List_1_Insert_m12117(__this, ___index, ___item, method) (( void (*) (List_1_t145 *, int32_t, RaycastResult_t139 , const MethodInfo*))List_1_Insert_m12117_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m12118_gshared (List_1_t145 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m12118(__this, ___collection, method) (( void (*) (List_1_t145 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m12118_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool List_1_Remove_m12119_gshared (List_1_t145 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define List_1_Remove_m12119(__this, ___item, method) (( bool (*) (List_1_t145 *, RaycastResult_t139 , const MethodInfo*))List_1_Remove_m12119_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m12120_gshared (List_1_t145 * __this, Predicate_1_t1920 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m12120(__this, ___match, method) (( int32_t (*) (List_1_t145 *, Predicate_1_t1920 *, const MethodInfo*))List_1_RemoveAll_m12120_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m12121_gshared (List_1_t145 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m12121(__this, ___index, method) (( void (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1_RemoveAt_m12121_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Reverse()
extern "C" void List_1_Reverse_m12122_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_Reverse_m12122(__this, method) (( void (*) (List_1_t145 *, const MethodInfo*))List_1_Reverse_m12122_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort()
extern "C" void List_1_Sort_m12123_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_Sort_m12123(__this, method) (( void (*) (List_1_t145 *, const MethodInfo*))List_1_Sort_m12123_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m1868_gshared (List_1_t145 * __this, Comparison_1_t108 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m1868(__this, ___comparison, method) (( void (*) (List_1_t145 *, Comparison_1_t108 *, const MethodInfo*))List_1_Sort_m1868_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::ToArray()
extern "C" RaycastResultU5BU5D_t1912* List_1_ToArray_m12124_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_ToArray_m12124(__this, method) (( RaycastResultU5BU5D_t1912* (*) (List_1_t145 *, const MethodInfo*))List_1_ToArray_m12124_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::TrimExcess()
extern "C" void List_1_TrimExcess_m12125_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m12125(__this, method) (( void (*) (List_1_t145 *, const MethodInfo*))List_1_TrimExcess_m12125_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m12126_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m12126(__this, method) (( int32_t (*) (List_1_t145 *, const MethodInfo*))List_1_get_Capacity_m12126_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m12127_gshared (List_1_t145 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m12127(__this, ___value, method) (( void (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1_set_Capacity_m12127_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t List_1_get_Count_m12128_gshared (List_1_t145 * __this, const MethodInfo* method);
#define List_1_get_Count_m12128(__this, method) (( int32_t (*) (List_1_t145 *, const MethodInfo*))List_1_get_Count_m12128_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t139  List_1_get_Item_m12129_gshared (List_1_t145 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m12129(__this, ___index, method) (( RaycastResult_t139  (*) (List_1_t145 *, int32_t, const MethodInfo*))List_1_get_Item_m12129_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m12130_gshared (List_1_t145 * __this, int32_t ___index, RaycastResult_t139  ___value, const MethodInfo* method);
#define List_1_set_Item_m12130(__this, ___index, ___value, method) (( void (*) (List_1_t145 *, int32_t, RaycastResult_t139 , const MethodInfo*))List_1_set_Item_m12130_gshared)(__this, ___index, ___value, method)
