﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m15722(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t2163 *, UnityAction_1_t2164 *, UnityAction_1_t2164 *, const MethodInfo*))ObjectPool_1__ctor_m11902_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::get_countAll()
#define ObjectPool_1_get_countAll_m15723(__this, method) (( int32_t (*) (ObjectPool_1_t2163 *, const MethodInfo*))ObjectPool_1_get_countAll_m11904_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m15724(__this, ___value, method) (( void (*) (ObjectPool_1_t2163 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11906_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::get_countActive()
#define ObjectPool_1_get_countActive_m15725(__this, method) (( int32_t (*) (ObjectPool_1_t2163 *, const MethodInfo*))ObjectPool_1_get_countActive_m11908_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::get_countInactive()
#define ObjectPool_1_get_countInactive_m15726(__this, method) (( int32_t (*) (ObjectPool_1_t2163 *, const MethodInfo*))ObjectPool_1_get_countInactive_m11910_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::Get()
#define ObjectPool_1_Get_m15727(__this, method) (( List_1_t313 * (*) (ObjectPool_1_t2163 *, const MethodInfo*))ObjectPool_1_Get_m11912_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<System.Collections.Generic.List`1<UnityEngine.Color32>>::Release(T)
#define ObjectPool_1_Release_m15728(__this, ___element, method) (( void (*) (ObjectPool_1_t2163 *, List_1_t313 *, const MethodInfo*))ObjectPool_1_Release_m11914_gshared)(__this, ___element, method)
