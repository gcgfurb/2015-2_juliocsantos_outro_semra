﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m18246(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2404 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10902_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m18247(__this, method) (( void (*) (InternalEnumerator_1_t2404 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m10904_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m18248(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2404 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10906_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::Dispose()
#define InternalEnumerator_1_Dispose_m18249(__this, method) (( void (*) (InternalEnumerator_1_t2404 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10908_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::MoveNext()
#define InternalEnumerator_1_MoveNext_m18250(__this, method) (( bool (*) (InternalEnumerator_1_t2404 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10910_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Runtime.Remoting.Services.ITrackingHandler>::get_Current()
#define InternalEnumerator_1_get_Current_m18251(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2404 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10912_gshared)(__this, method)
