﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_54.h"
#include "UnityEngine_UnityEngine_SendMouseEvents_HitInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16935_gshared (InternalEnumerator_1_t2262 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16935(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2262 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16935_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16936_gshared (InternalEnumerator_1_t2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16936(__this, method) (( void (*) (InternalEnumerator_1_t2262 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16936_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16937_gshared (InternalEnumerator_1_t2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16937(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2262 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16937_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16938_gshared (InternalEnumerator_1_t2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16938(__this, method) (( void (*) (InternalEnumerator_1_t2262 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16938_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16939_gshared (InternalEnumerator_1_t2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16939(__this, method) (( bool (*) (InternalEnumerator_1_t2262 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16939_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SendMouseEvents/HitInfo>::get_Current()
extern "C" HitInfo_t597  InternalEnumerator_1_get_Current_m16940_gshared (InternalEnumerator_1_t2262 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16940(__this, method) (( HitInfo_t597  (*) (InternalEnumerator_1_t2262 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16940_gshared)(__this, method)
