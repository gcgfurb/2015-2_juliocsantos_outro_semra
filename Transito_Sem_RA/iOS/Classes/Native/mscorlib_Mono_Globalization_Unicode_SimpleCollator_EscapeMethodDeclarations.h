﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void Escape_t1127_marshal(const Escape_t1127& unmarshaled, Escape_t1127_marshaled& marshaled);
extern "C" void Escape_t1127_marshal_back(const Escape_t1127_marshaled& marshaled, Escape_t1127& unmarshaled);
extern "C" void Escape_t1127_marshal_cleanup(Escape_t1127_marshaled& marshaled);
