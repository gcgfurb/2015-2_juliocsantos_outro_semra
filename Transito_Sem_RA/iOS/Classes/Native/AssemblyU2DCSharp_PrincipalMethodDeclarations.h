﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Principal
struct Principal_t96;
// System.String
struct String_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Principal::.ctor()
extern "C" void Principal__ctor_m413 (Principal_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::Start()
extern "C" void Principal_Start_m414 (Principal_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::Update()
extern "C" void Principal_Update_m415 (Principal_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::CombustivelBaixo()
extern "C" void Principal_CombustivelBaixo_m416 (Principal_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::MecanicaBaixa()
extern "C" void Principal_MecanicaBaixa_m417 (Principal_t96 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::ExibirMesagemPadrao(System.String)
extern "C" void Principal_ExibirMesagemPadrao_m418 (Principal_t96 * __this, String_t* ___mensagem, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::ExibirMesagemAlerta(System.String)
extern "C" void Principal_ExibirMesagemAlerta_m419 (Principal_t96 * __this, String_t* ___mensagem, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Principal::ExibirMesagemPerigo(System.String)
extern "C" void Principal_ExibirMesagemPerigo_m420 (Principal_t96 * __this, String_t* ___mensagem, const MethodInfo* method) IL2CPP_METHOD_ATTR;
