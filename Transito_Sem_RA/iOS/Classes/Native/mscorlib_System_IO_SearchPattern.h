﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t238;

#include "mscorlib_System_Object.h"

// System.IO.SearchPattern
struct  SearchPattern_t1250  : public Object_t
{
};
struct SearchPattern_t1250_StaticFields{
	// System.Char[] System.IO.SearchPattern::WildcardChars
	CharU5BU5D_t238* ___WildcardChars_0;
	// System.Char[] System.IO.SearchPattern::InvalidChars
	CharU5BU5D_t238* ___InvalidChars_1;
};
