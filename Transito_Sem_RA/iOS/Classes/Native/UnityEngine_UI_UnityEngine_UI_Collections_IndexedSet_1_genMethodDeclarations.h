﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::.ctor()
#define IndexedSet_1__ctor_m1989(__this, method) (( void (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1__ctor_m12980_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12981(__this, method) (( Object_t * (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12982_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Add(T)
#define IndexedSet_1_Add_m12983(__this, ___item, method) (( void (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m12984_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Remove(T)
#define IndexedSet_1_Remove_m12985(__this, ___item, method) (( bool (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m12986_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m12987(__this, method) (( Object_t* (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m12988_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Clear()
#define IndexedSet_1_Clear_m12989(__this, method) (( void (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_Clear_m12990_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Contains(T)
#define IndexedSet_1_Contains_m12991(__this, ___item, method) (( bool (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m12992_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m12993(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t172 *, ICanvasElementU5BU5D_t1972*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m12994_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Count()
#define IndexedSet_1_get_Count_m12995(__this, method) (( int32_t (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_get_Count_m12996_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m12997(__this, method) (( bool (*) (IndexedSet_1_t172 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m12998_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::IndexOf(T)
#define IndexedSet_1_IndexOf_m12999(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t172 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m13000_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m13001(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t172 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m13002_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m13003(__this, ___index, method) (( void (*) (IndexedSet_1_t172 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m13004_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m13005(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t172 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m13006_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m13007(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t172 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m13008_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m13009(__this, ___match, method) (( void (*) (IndexedSet_1_t172 *, Predicate_1_t1975 *, const MethodInfo*))IndexedSet_1_RemoveAll_m13010_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.ICanvasElement>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m1993(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t172 *, Comparison_1_t173 *, const MethodInfo*))IndexedSet_1_Sort_m13011_gshared)(__this, ___sortLayoutFunction, method)
