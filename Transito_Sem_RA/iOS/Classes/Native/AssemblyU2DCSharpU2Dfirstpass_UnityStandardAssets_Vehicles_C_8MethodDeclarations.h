﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.CarUserControl
struct CarUserControl_t47;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::.ctor()
extern "C" void CarUserControl__ctor_m177 (CarUserControl_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::Start()
extern "C" void CarUserControl_Start_m178 (CarUserControl_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::Awake()
extern "C" void CarUserControl_Awake_m179 (CarUserControl_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::FixedUpdate()
extern "C" void CarUserControl_FixedUpdate_m180 (CarUserControl_t47 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
