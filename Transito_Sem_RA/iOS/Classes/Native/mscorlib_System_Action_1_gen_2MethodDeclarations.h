﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Action_1_gen_5MethodDeclarations.h"

// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::.ctor(System.Object,System.IntPtr)
#define Action_1__ctor_m15852(__this, ___object, ___method, method) (( void (*) (Action_1_t446 *, Object_t *, IntPtr_t, const MethodInfo*))Action_1__ctor_m13616_gshared)(__this, ___object, ___method, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::Invoke(T)
#define Action_1_Invoke_m3586(__this, ___obj, method) (( void (*) (Action_1_t446 *, IAchievementU5BU5D_t659*, const MethodInfo*))Action_1_Invoke_m13617_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Action_1_BeginInvoke_m15853(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Action_1_t446 *, IAchievementU5BU5D_t659*, AsyncCallback_t229 *, Object_t *, const MethodInfo*))Action_1_BeginInvoke_m13619_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Void System.Action`1<UnityEngine.SocialPlatforms.IAchievement[]>::EndInvoke(System.IAsyncResult)
#define Action_1_EndInvoke_m15854(__this, ___result, method) (( void (*) (Action_1_t446 *, Object_t *, const MethodInfo*))Action_1_EndInvoke_m13621_gshared)(__this, ___result, method)
