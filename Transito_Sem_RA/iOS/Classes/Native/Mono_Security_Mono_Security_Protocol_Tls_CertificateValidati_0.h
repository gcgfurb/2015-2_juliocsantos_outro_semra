﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// Mono.Security.Protocol.Tls.ValidationResult
struct ValidationResult_t819;
// Mono.Security.X509.X509CertificateCollection
struct X509CertificateCollection_t753;
// System.IAsyncResult
struct IAsyncResult_t228;
// System.AsyncCallback
struct AsyncCallback_t229;
// System.Object
struct Object_t;

#include "mscorlib_System_MulticastDelegate.h"

// Mono.Security.Protocol.Tls.CertificateValidationCallback2
struct  CertificateValidationCallback2_t822  : public MulticastDelegate_t227
{
};
