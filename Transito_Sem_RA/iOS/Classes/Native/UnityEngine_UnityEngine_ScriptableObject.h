﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_Object.h"

// UnityEngine.ScriptableObject
struct  ScriptableObject_t441  : public Object_t62
{
};
// Native definition for marshalling of: UnityEngine.ScriptableObject
struct ScriptableObject_t441_marshaled
{
};
