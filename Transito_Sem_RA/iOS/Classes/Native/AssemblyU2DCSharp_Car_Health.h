﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Slider
struct Slider_t93;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Car_Health
struct  Car_Health_t87  : public MonoBehaviour_t2
{
	// System.Single Car_Health::m_CombutivelMax
	float ___m_CombutivelMax_2;
	// System.Single Car_Health::m_MecanicaMax
	float ___m_MecanicaMax_3;
	// UnityEngine.UI.Slider Car_Health::m_BarraCombustivel
	Slider_t93 * ___m_BarraCombustivel_4;
	// UnityEngine.UI.Slider Car_Health::m_BarraMecanica
	Slider_t93 * ___m_BarraMecanica_5;
	// System.Single Car_Health::m_CombustivelAtual
	float ___m_CombustivelAtual_6;
	// System.Single Car_Health::m_MecanicaAtual
	float ___m_MecanicaAtual_7;
	// System.Boolean Car_Health::m_Morto
	bool ___m_Morto_8;
	// System.Boolean Car_Health::m_CombustivelBaixo
	bool ___m_CombustivelBaixo_9;
	// System.Boolean Car_Health::m_MecanicaBaixa
	bool ___m_MecanicaBaixa_10;
	// System.Boolean Car_Health::m_AguardarCombustivel
	bool ___m_AguardarCombustivel_11;
	// System.Boolean Car_Health::m_AguardarMecanica
	bool ___m_AguardarMecanica_12;
};
