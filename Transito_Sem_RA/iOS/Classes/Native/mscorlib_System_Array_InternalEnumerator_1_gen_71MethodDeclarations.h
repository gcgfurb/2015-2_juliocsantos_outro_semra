﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_71.h"
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17653_gshared (InternalEnumerator_1_t2336 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17653(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2336 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17653_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17654_gshared (InternalEnumerator_1_t2336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17654(__this, method) (( void (*) (InternalEnumerator_1_t2336 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17654_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17655_gshared (InternalEnumerator_1_t2336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17655(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2336 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17655_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17656_gshared (InternalEnumerator_1_t2336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17656(__this, method) (( void (*) (InternalEnumerator_1_t2336 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17656_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17657_gshared (InternalEnumerator_1_t2336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17657(__this, method) (( bool (*) (InternalEnumerator_1_t2336 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17657_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Text.RegularExpressions.Mark>::get_Current()
extern "C" Mark_t1009  InternalEnumerator_1_get_Current_m17658_gshared (InternalEnumerator_1_t2336 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17658(__this, method) (( Mark_t1009  (*) (InternalEnumerator_1_t2336 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17658_gshared)(__this, method)
