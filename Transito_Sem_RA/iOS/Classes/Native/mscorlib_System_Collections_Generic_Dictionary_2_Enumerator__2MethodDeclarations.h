﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m11208(__this, ___dictionary, method) (( void (*) (Enumerator_t1839 *, Dictionary_2_t26 *, const MethodInfo*))Enumerator__ctor_m11050_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m11209(__this, method) (( Object_t * (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m11210(__this, method) (( void (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11052_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11211(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11053_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11212(__this, method) (( Object_t * (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11213(__this, method) (( Object_t * (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::MoveNext()
#define Enumerator_MoveNext_m11214(__this, method) (( bool (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_MoveNext_m11056_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Current()
#define Enumerator_get_Current_m11215(__this, method) (( KeyValuePair_2_t1837  (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_get_Current_m11057_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m11216(__this, method) (( String_t* (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_get_CurrentKey_m11058_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m11217(__this, method) (( VirtualButton_t6 * (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_get_CurrentValue_m11059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::Reset()
#define Enumerator_Reset_m11218(__this, method) (( void (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_Reset_m11060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::VerifyState()
#define Enumerator_VerifyState_m11219(__this, method) (( void (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_VerifyState_m11061_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m11220(__this, method) (( void (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_VerifyCurrent_m11062_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::Dispose()
#define Enumerator_Dispose_m11221(__this, method) (( void (*) (Enumerator_t1839 *, const MethodInfo*))Enumerator_Dispose_m11063_gshared)(__this, method)
