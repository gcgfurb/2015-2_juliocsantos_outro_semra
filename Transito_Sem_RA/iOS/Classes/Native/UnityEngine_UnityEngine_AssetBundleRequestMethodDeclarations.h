﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.AssetBundleRequest
struct AssetBundleRequest_t433;
// UnityEngine.Object
struct Object_t62;
struct Object_t62_marshaled;
// UnityEngine.Object[]
struct ObjectU5BU5D_t61;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.AssetBundleRequest::.ctor()
extern "C" void AssetBundleRequest__ctor_m2450 (AssetBundleRequest_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object UnityEngine.AssetBundleRequest::get_asset()
extern "C" Object_t62 * AssetBundleRequest_get_asset_m2451 (AssetBundleRequest_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Object[] UnityEngine.AssetBundleRequest::get_allAssets()
extern "C" ObjectU5BU5D_t61* AssetBundleRequest_get_allAssets_m2452 (AssetBundleRequest_t433 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
