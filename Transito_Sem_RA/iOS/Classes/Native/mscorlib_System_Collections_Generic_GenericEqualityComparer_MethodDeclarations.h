﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>
struct GenericEqualityComparer_1_t1799;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::.ctor()
extern "C" void GenericEqualityComparer_1__ctor_m10889_gshared (GenericEqualityComparer_1_t1799 * __this, const MethodInfo* method);
#define GenericEqualityComparer_1__ctor_m10889(__this, method) (( void (*) (GenericEqualityComparer_1_t1799 *, const MethodInfo*))GenericEqualityComparer_1__ctor_m10889_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::GetHashCode(T)
extern "C" int32_t GenericEqualityComparer_1_GetHashCode_m18388_gshared (GenericEqualityComparer_1_t1799 * __this, DateTime_t546  ___obj, const MethodInfo* method);
#define GenericEqualityComparer_1_GetHashCode_m18388(__this, ___obj, method) (( int32_t (*) (GenericEqualityComparer_1_t1799 *, DateTime_t546 , const MethodInfo*))GenericEqualityComparer_1_GetHashCode_m18388_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.GenericEqualityComparer`1<System.DateTime>::Equals(T,T)
extern "C" bool GenericEqualityComparer_1_Equals_m18389_gshared (GenericEqualityComparer_1_t1799 * __this, DateTime_t546  ___x, DateTime_t546  ___y, const MethodInfo* method);
#define GenericEqualityComparer_1_Equals_m18389(__this, ___x, ___y, method) (( bool (*) (GenericEqualityComparer_1_t1799 *, DateTime_t546 , DateTime_t546 , const MethodInfo*))GenericEqualityComparer_1_Equals_m18389_gshared)(__this, ___x, ___y, method)
