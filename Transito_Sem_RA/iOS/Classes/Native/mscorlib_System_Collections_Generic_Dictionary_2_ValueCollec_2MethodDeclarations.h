﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_4MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m16799(__this, ___host, method) (( void (*) (Enumerator_t669 *, Dictionary_2_t562 *, const MethodInfo*))Enumerator__ctor_m11044_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16800(__this, method) (( Object_t * (*) (Enumerator_t669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11045_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16801(__this, method) (( void (*) (Enumerator_t669 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11046_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::Dispose()
#define Enumerator_Dispose_m16802(__this, method) (( void (*) (Enumerator_t669 *, const MethodInfo*))Enumerator_Dispose_m11047_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::MoveNext()
#define Enumerator_MoveNext_m16803(__this, method) (( bool (*) (Enumerator_t669 *, const MethodInfo*))Enumerator_MoveNext_m11048_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.String,UnityEngine.GUIStyle>::get_Current()
#define Enumerator_get_Current_m16804(__this, method) (( GUIStyle_t553 * (*) (Enumerator_t669 *, const MethodInfo*))Enumerator_get_Current_m11049_gshared)(__this, method)
