﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// AtualizaVida
struct AtualizaVida_t86;
// UnityEngine.Collision
struct Collision_t58;
// UnityEngine.Collider
struct Collider_t98;

#include "codegen/il2cpp-codegen.h"

// System.Void AtualizaVida::.ctor()
extern "C" void AtualizaVida__ctor_m364 (AtualizaVida_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AtualizaVida::Start()
extern "C" void AtualizaVida_Start_m365 (AtualizaVida_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AtualizaVida::Update()
extern "C" void AtualizaVida_Update_m366 (AtualizaVida_t86 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AtualizaVida::OnCollisionEnter(UnityEngine.Collision)
extern "C" void AtualizaVida_OnCollisionEnter_m367 (AtualizaVida_t86 * __this, Collision_t58 * ___ObjetoColidido, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void AtualizaVida::OnTriggerStay(UnityEngine.Collider)
extern "C" void AtualizaVida_OnTriggerStay_m368 (AtualizaVida_t86 * __this, Collider_t98 * ___ObjetoColidido, const MethodInfo* method) IL2CPP_METHOD_ATTR;
