﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_37MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor()
#define List_1__ctor_m3608(__this, method) (( void (*) (List_1_t508 *, const MethodInfo*))List_1__ctor_m11228_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.ctor(System.Int32)
#define List_1__ctor_m16134(__this, ___capacity, method) (( void (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1__ctor_m11230_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::.cctor()
#define List_1__cctor_m16135(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11232_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16136(__this, method) (( Object_t* (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11234_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m16137(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t508 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m11236_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16138(__this, method) (( Object_t * (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m11238_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m16139(__this, ___item, method) (( int32_t (*) (List_1_t508 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m11240_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m16140(__this, ___item, method) (( bool (*) (List_1_t508 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m11242_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m16141(__this, ___item, method) (( int32_t (*) (List_1_t508 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m11244_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m16142(__this, ___index, ___item, method) (( void (*) (List_1_t508 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m11246_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m16143(__this, ___item, method) (( void (*) (List_1_t508 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m11248_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16144(__this, method) (( bool (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11250_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16145(__this, method) (( bool (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m11252_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m16146(__this, method) (( Object_t * (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m11254_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m16147(__this, method) (( bool (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m11256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m16148(__this, method) (( bool (*) (List_1_t508 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m11258_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m16149(__this, ___index, method) (( Object_t * (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m11260_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m16150(__this, ___index, ___value, method) (( void (*) (List_1_t508 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m11262_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Add(T)
#define List_1_Add_m16151(__this, ___item, method) (( void (*) (List_1_t508 *, Rigidbody2D_t509 *, const MethodInfo*))List_1_Add_m11264_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m16152(__this, ___newCount, method) (( void (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11266_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m16153(__this, ___collection, method) (( void (*) (List_1_t508 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11268_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m16154(__this, ___enumerable, method) (( void (*) (List_1_t508 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11270_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m16155(__this, ___collection, method) (( void (*) (List_1_t508 *, Object_t*, const MethodInfo*))List_1_AddRange_m11272_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::AsReadOnly()
#define List_1_AsReadOnly_m16156(__this, method) (( ReadOnlyCollection_1_t2210 * (*) (List_1_t508 *, const MethodInfo*))List_1_AsReadOnly_m11274_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Clear()
#define List_1_Clear_m16157(__this, method) (( void (*) (List_1_t508 *, const MethodInfo*))List_1_Clear_m11276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Contains(T)
#define List_1_Contains_m16158(__this, ___item, method) (( bool (*) (List_1_t508 *, Rigidbody2D_t509 *, const MethodInfo*))List_1_Contains_m11278_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m16159(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t508 *, Rigidbody2DU5BU5D_t2209*, int32_t, const MethodInfo*))List_1_CopyTo_m11280_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Find(System.Predicate`1<T>)
#define List_1_Find_m16160(__this, ___match, method) (( Rigidbody2D_t509 * (*) (List_1_t508 *, Predicate_1_t2212 *, const MethodInfo*))List_1_Find_m11282_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m16161(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2212 *, const MethodInfo*))List_1_CheckMatch_m11284_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m16162(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t508 *, int32_t, int32_t, Predicate_1_t2212 *, const MethodInfo*))List_1_GetIndex_m11286_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::GetEnumerator()
#define List_1_GetEnumerator_m16163(__this, method) (( Enumerator_t2213  (*) (List_1_t508 *, const MethodInfo*))List_1_GetEnumerator_m11288_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::IndexOf(T)
#define List_1_IndexOf_m16164(__this, ___item, method) (( int32_t (*) (List_1_t508 *, Rigidbody2D_t509 *, const MethodInfo*))List_1_IndexOf_m11290_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m16165(__this, ___start, ___delta, method) (( void (*) (List_1_t508 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11292_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m16166(__this, ___index, method) (( void (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11294_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Insert(System.Int32,T)
#define List_1_Insert_m16167(__this, ___index, ___item, method) (( void (*) (List_1_t508 *, int32_t, Rigidbody2D_t509 *, const MethodInfo*))List_1_Insert_m11296_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m16168(__this, ___collection, method) (( void (*) (List_1_t508 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11298_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Remove(T)
#define List_1_Remove_m16169(__this, ___item, method) (( bool (*) (List_1_t508 *, Rigidbody2D_t509 *, const MethodInfo*))List_1_Remove_m11300_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m16170(__this, ___match, method) (( int32_t (*) (List_1_t508 *, Predicate_1_t2212 *, const MethodInfo*))List_1_RemoveAll_m11302_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m16171(__this, ___index, method) (( void (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1_RemoveAt_m11304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Reverse()
#define List_1_Reverse_m16172(__this, method) (( void (*) (List_1_t508 *, const MethodInfo*))List_1_Reverse_m11306_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Sort()
#define List_1_Sort_m16173(__this, method) (( void (*) (List_1_t508 *, const MethodInfo*))List_1_Sort_m11308_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m16174(__this, ___comparison, method) (( void (*) (List_1_t508 *, Comparison_1_t2214 *, const MethodInfo*))List_1_Sort_m11310_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::ToArray()
#define List_1_ToArray_m16175(__this, method) (( Rigidbody2DU5BU5D_t2209* (*) (List_1_t508 *, const MethodInfo*))List_1_ToArray_m11311_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::TrimExcess()
#define List_1_TrimExcess_m16176(__this, method) (( void (*) (List_1_t508 *, const MethodInfo*))List_1_TrimExcess_m11313_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Capacity()
#define List_1_get_Capacity_m16177(__this, method) (( int32_t (*) (List_1_t508 *, const MethodInfo*))List_1_get_Capacity_m11315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m16178(__this, ___value, method) (( void (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11317_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Count()
#define List_1_get_Count_m16179(__this, method) (( int32_t (*) (List_1_t508 *, const MethodInfo*))List_1_get_Count_m11319_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::get_Item(System.Int32)
#define List_1_get_Item_m16180(__this, ___index, method) (( Rigidbody2D_t509 * (*) (List_1_t508 *, int32_t, const MethodInfo*))List_1_get_Item_m11321_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.Rigidbody2D>::set_Item(System.Int32,T)
#define List_1_set_Item_m16181(__this, ___index, ___value, method) (( void (*) (List_1_t508 *, int32_t, Rigidbody2D_t509 *, const MethodInfo*))List_1_set_Item_m11323_gshared)(__this, ___index, ___value, method)
