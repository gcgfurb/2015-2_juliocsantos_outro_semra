﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>
struct ShimEnumerator_t1959;
// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1950;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m12808_gshared (ShimEnumerator_t1959 * __this, Dictionary_2_t1950 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m12808(__this, ___host, method) (( void (*) (ShimEnumerator_t1959 *, Dictionary_2_t1950 *, const MethodInfo*))ShimEnumerator__ctor_m12808_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m12809_gshared (ShimEnumerator_t1959 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m12809(__this, method) (( bool (*) (ShimEnumerator_t1959 *, const MethodInfo*))ShimEnumerator_MoveNext_m12809_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Entry()
extern "C" DictionaryEntry_t1057  ShimEnumerator_get_Entry_m12810_gshared (ShimEnumerator_t1959 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m12810(__this, method) (( DictionaryEntry_t1057  (*) (ShimEnumerator_t1959 *, const MethodInfo*))ShimEnumerator_get_Entry_m12810_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m12811_gshared (ShimEnumerator_t1959 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m12811(__this, method) (( Object_t * (*) (ShimEnumerator_t1959 *, const MethodInfo*))ShimEnumerator_get_Key_m12811_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m12812_gshared (ShimEnumerator_t1959 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m12812(__this, method) (( Object_t * (*) (ShimEnumerator_t1959 *, const MethodInfo*))ShimEnumerator_get_Value_m12812_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m12813_gshared (ShimEnumerator_t1959 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m12813(__this, method) (( Object_t * (*) (ShimEnumerator_t1959 *, const MethodInfo*))ShimEnumerator_get_Current_m12813_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Int32,System.Object>::Reset()
extern "C" void ShimEnumerator_Reset_m12814_gshared (ShimEnumerator_t1959 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m12814(__this, method) (( void (*) (ShimEnumerator_t1959 *, const MethodInfo*))ShimEnumerator_Reset_m12814_gshared)(__this, method)
