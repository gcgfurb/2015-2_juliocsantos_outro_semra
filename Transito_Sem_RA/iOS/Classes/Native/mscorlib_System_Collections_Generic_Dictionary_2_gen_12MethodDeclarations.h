﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Int32>
struct Dictionary_2_t1862;
// System.Collections.Generic.IEqualityComparer`1<System.Object>
struct IEqualityComparer_1_t1816;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t652;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>[]
struct KeyValuePair_2U5BU5D_t2441;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Object,System.Int32>>
struct IEnumerator_1_t2442;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t869;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Object,System.Int32>
struct ValueCollection_t1866;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor()
extern "C" void Dictionary_2__ctor_m11499_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m11499(__this, method) (( void (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2__ctor_m11499_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m11501_gshared (Dictionary_2_t1862 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m11501(__this, ___comparer, method) (( void (*) (Dictionary_2_t1862 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m11501_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m11502_gshared (Dictionary_2_t1862 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m11502(__this, ___capacity, method) (( void (*) (Dictionary_2_t1862 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m11502_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m11504_gshared (Dictionary_2_t1862 * __this, SerializationInfo_t652 * ___info, StreamingContext_t653  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m11504(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1862 *, SerializationInfo_t652 *, StreamingContext_t653 , const MethodInfo*))Dictionary_2__ctor_m11504_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m11506_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m11506(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m11506_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m11508_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m11508(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1862 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m11508_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m11510_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m11510(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1862 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m11510_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m11512_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m11512(__this, ___key, method) (( bool (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m11512_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m11514_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m11514(__this, ___key, method) (( void (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m11514_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m11516_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m11516(__this, method) (( bool (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m11516_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m11518_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m11518(__this, method) (( Object_t * (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m11518_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m11520_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m11520(__this, method) (( bool (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m11520_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m11522_gshared (Dictionary_2_t1862 * __this, KeyValuePair_2_t1864  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m11522(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1862 *, KeyValuePair_2_t1864 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m11522_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m11524_gshared (Dictionary_2_t1862 * __this, KeyValuePair_2_t1864  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m11524(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1862 *, KeyValuePair_2_t1864 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m11524_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m11526_gshared (Dictionary_2_t1862 * __this, KeyValuePair_2U5BU5D_t2441* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m11526(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1862 *, KeyValuePair_2U5BU5D_t2441*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m11526_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m11528_gshared (Dictionary_2_t1862 * __this, KeyValuePair_2_t1864  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m11528(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1862 *, KeyValuePair_2_t1864 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m11528_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m11530_gshared (Dictionary_2_t1862 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m11530(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1862 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m11530_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m11532_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m11532(__this, method) (( Object_t * (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m11532_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m11534_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m11534(__this, method) (( Object_t* (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m11534_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m11536_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m11536(__this, method) (( Object_t * (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m11536_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m11538_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m11538(__this, method) (( int32_t (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_get_Count_m11538_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Item(TKey)
extern "C" int32_t Dictionary_2_get_Item_m11540_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m11540(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_get_Item_m11540_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m11542_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m11542(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1862 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_set_Item_m11542_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m11544_gshared (Dictionary_2_t1862 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m11544(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1862 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m11544_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m11546_gshared (Dictionary_2_t1862 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m11546(__this, ___size, method) (( void (*) (Dictionary_2_t1862 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m11546_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m11548_gshared (Dictionary_2_t1862 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m11548(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1862 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m11548_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1864  Dictionary_2_make_pair_m11550_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m11550(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1864  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_make_pair_m11550_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::pick_value(TKey,TValue)
extern "C" int32_t Dictionary_2_pick_value_m11552_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m11552(__this /* static, unused */, ___key, ___value, method) (( int32_t (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_pick_value_m11552_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m11554_gshared (Dictionary_2_t1862 * __this, KeyValuePair_2U5BU5D_t2441* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m11554(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1862 *, KeyValuePair_2U5BU5D_t2441*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m11554_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Resize()
extern "C" void Dictionary_2_Resize_m11556_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m11556(__this, method) (( void (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_Resize_m11556_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m11558_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_Add_m11558(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1862 *, Object_t *, int32_t, const MethodInfo*))Dictionary_2_Add_m11558_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Clear()
extern "C" void Dictionary_2_Clear_m11560_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m11560(__this, method) (( void (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_Clear_m11560_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m11562_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m11562(__this, ___key, method) (( bool (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsKey_m11562_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m11564_gshared (Dictionary_2_t1862 * __this, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m11564(__this, ___value, method) (( bool (*) (Dictionary_2_t1862 *, int32_t, const MethodInfo*))Dictionary_2_ContainsValue_m11564_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m11566_gshared (Dictionary_2_t1862 * __this, SerializationInfo_t652 * ___info, StreamingContext_t653  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m11566(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1862 *, SerializationInfo_t652 *, StreamingContext_t653 , const MethodInfo*))Dictionary_2_GetObjectData_m11566_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m11568_gshared (Dictionary_2_t1862 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m11568(__this, ___sender, method) (( void (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m11568_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m11570_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m11570(__this, ___key, method) (( bool (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_Remove_m11570_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m11572_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, int32_t* ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m11572(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1862 *, Object_t *, int32_t*, const MethodInfo*))Dictionary_2_TryGetValue_m11572_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::get_Values()
extern "C" ValueCollection_t1866 * Dictionary_2_get_Values_m11574_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m11574(__this, method) (( ValueCollection_t1866 * (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_get_Values_m11574_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTKey(System.Object)
extern "C" Object_t * Dictionary_2_ToTKey_m11576_gshared (Dictionary_2_t1862 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m11576(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m11576_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ToTValue(System.Object)
extern "C" int32_t Dictionary_2_ToTValue_m11578_gshared (Dictionary_2_t1862 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m11578(__this, ___value, method) (( int32_t (*) (Dictionary_2_t1862 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m11578_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m11580_gshared (Dictionary_2_t1862 * __this, KeyValuePair_2_t1864  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m11580(__this, ___pair, method) (( bool (*) (Dictionary_2_t1862 *, KeyValuePair_2_t1864 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m11580_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::GetEnumerator()
extern "C" Enumerator_t1868  Dictionary_2_GetEnumerator_m11582_gshared (Dictionary_2_t1862 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m11582(__this, method) (( Enumerator_t1868  (*) (Dictionary_2_t1862 *, const MethodInfo*))Dictionary_2_GetEnumerator_m11582_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Object,System.Int32>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1057  Dictionary_2_U3CCopyToU3Em__0_m11584_gshared (Object_t * __this /* static, unused */, Object_t * ___key, int32_t ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m11584(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1057  (*) (Object_t * /* static, unused */, Object_t *, int32_t, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m11584_gshared)(__this /* static, unused */, ___key, ___value, method)
