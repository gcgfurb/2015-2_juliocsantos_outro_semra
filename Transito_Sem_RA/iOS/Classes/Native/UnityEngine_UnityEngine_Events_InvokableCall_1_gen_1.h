﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Events.UnityAction`1<System.Single>
struct UnityAction_1_t343;

#include "UnityEngine_UnityEngine_Events_BaseInvokableCall.h"

// UnityEngine.Events.InvokableCall`1<System.Single>
struct  InvokableCall_1_t1969  : public BaseInvokableCall_t620
{
	// UnityEngine.Events.UnityAction`1<T1> UnityEngine.Events.InvokableCall`1<System.Single>::Delegate
	UnityAction_1_t343 * ___Delegate_0;
};
