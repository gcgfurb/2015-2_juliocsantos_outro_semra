﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Comparison_1_gen_3MethodDeclarations.h"

// System.Void System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::.ctor(System.Object,System.IntPtr)
#define Comparison_1__ctor_m12410(__this, ___object, ___method, method) (( void (*) (Comparison_1_t1934 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m11419_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::Invoke(T,T)
#define Comparison_1_Invoke_m12411(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t1934 *, Entry_t111 *, Entry_t111 *, const MethodInfo*))Comparison_1_Invoke_m11420_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
#define Comparison_1_BeginInvoke_m12412(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t1934 *, Entry_t111 *, Entry_t111 *, AsyncCallback_t229 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m11421_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.EventSystems.EventTrigger/Entry>::EndInvoke(System.IAsyncResult)
#define Comparison_1_EndInvoke_m12413(__this, ___result, method) (( int32_t (*) (Comparison_1_t1934 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m11422_gshared)(__this, ___result, method)
