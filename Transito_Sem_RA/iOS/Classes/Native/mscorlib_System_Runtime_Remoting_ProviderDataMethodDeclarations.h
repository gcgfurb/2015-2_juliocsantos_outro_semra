﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Runtime.Remoting.ProviderData
struct ProviderData_t1479;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Runtime.Remoting.ProviderData::.ctor()
extern "C" void ProviderData__ctor_m9010 (ProviderData_t1479 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void System.Runtime.Remoting.ProviderData::CopyFrom(System.Runtime.Remoting.ProviderData)
extern "C" void ProviderData_CopyFrom_m9011 (ProviderData_t1479 * __this, ProviderData_t1479 * ___other, const MethodInfo* method) IL2CPP_METHOD_ATTR;
