﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>
struct DefaultComparer_t1874;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::.ctor()
extern "C" void DefaultComparer__ctor_m11658_gshared (DefaultComparer_t1874 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m11658(__this, method) (( void (*) (DefaultComparer_t1874 *, const MethodInfo*))DefaultComparer__ctor_m11658_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m11659_gshared (DefaultComparer_t1874 * __this, int32_t ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m11659(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t1874 *, int32_t, const MethodInfo*))DefaultComparer_GetHashCode_m11659_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<System.Int32>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m11660_gshared (DefaultComparer_t1874 * __this, int32_t ___x, int32_t ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m11660(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t1874 *, int32_t, int32_t, const MethodInfo*))DefaultComparer_Equals_m11660_gshared)(__this, ___x, ___y, method)
