﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17621(__this, ___dictionary, method) (( void (*) (Enumerator_t2332 *, Dictionary_2_t943 *, const MethodInfo*))Enumerator__ctor_m17553_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17622(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17554_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17623(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17555_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17624(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17556_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17625(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17557_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17626(__this, method) (( Object_t * (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::MoveNext()
#define Enumerator_MoveNext_m17627(__this, method) (( bool (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_MoveNext_m17559_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_Current()
#define Enumerator_get_Current_m17628(__this, method) (( KeyValuePair_2_t2330  (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_Current_m17560_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17629(__this, method) (( String_t* (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_CurrentKey_m17561_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17630(__this, method) (( bool (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_get_CurrentValue_m17562_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Reset()
#define Enumerator_Reset_m17631(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_Reset_m17563_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyState()
#define Enumerator_VerifyState_m17632(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_VerifyState_m17564_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17633(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_VerifyCurrent_m17565_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.String,System.Boolean>::Dispose()
#define Enumerator_Dispose_m17634(__this, method) (( void (*) (Enumerator_t2332 *, const MethodInfo*))Enumerator_Dispose_m17566_gshared)(__this, method)
