﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Predicate_1_gen_2MethodDeclarations.h"

// System.Void System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::.ctor(System.Object,System.IntPtr)
#define Predicate_1__ctor_m14580(__this, ___object, ___method, method) (( void (*) (Predicate_1_t2080 *, Object_t *, IntPtr_t, const MethodInfo*))Predicate_1__ctor_m11397_gshared)(__this, ___object, ___method, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::Invoke(T)
#define Predicate_1_Invoke_m14581(__this, ___obj, method) (( bool (*) (Predicate_1_t2080 *, MatEntry_t274 *, const MethodInfo*))Predicate_1_Invoke_m11398_gshared)(__this, ___obj, method)
// System.IAsyncResult System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::BeginInvoke(T,System.AsyncCallback,System.Object)
#define Predicate_1_BeginInvoke_m14582(__this, ___obj, ___callback, ___object, method) (( Object_t * (*) (Predicate_1_t2080 *, MatEntry_t274 *, AsyncCallback_t229 *, Object_t *, const MethodInfo*))Predicate_1_BeginInvoke_m11399_gshared)(__this, ___obj, ___callback, ___object, method)
// System.Boolean System.Predicate`1<UnityEngine.UI.StencilMaterial/MatEntry>::EndInvoke(System.IAsyncResult)
#define Predicate_1_EndInvoke_m14583(__this, ___result, method) (( bool (*) (Predicate_1_t2080 *, Object_t *, const MethodInfo*))Predicate_1_EndInvoke_m11400_gshared)(__this, ___result, method)
