﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Events_UnityAction_1_gen_5MethodDeclarations.h"

// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::.ctor(System.Object,System.IntPtr)
#define UnityAction_1__ctor_m15836(__this, ___object, ___method, method) (( void (*) (UnityAction_1_t2180 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAction_1__ctor_m11932_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::Invoke(T0)
#define UnityAction_1_Invoke_m15837(__this, ___arg0, method) (( void (*) (UnityAction_1_t2180 *, List_1_t345 *, const MethodInfo*))UnityAction_1_Invoke_m11933_gshared)(__this, ___arg0, method)
// System.IAsyncResult UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::BeginInvoke(T0,System.AsyncCallback,System.Object)
#define UnityAction_1_BeginInvoke_m15838(__this, ___arg0, ___callback, ___object, method) (( Object_t * (*) (UnityAction_1_t2180 *, List_1_t345 *, AsyncCallback_t229 *, Object_t *, const MethodInfo*))UnityAction_1_BeginInvoke_m11934_gshared)(__this, ___arg0, ___callback, ___object, method)
// System.Void UnityEngine.Events.UnityAction`1<System.Collections.Generic.List`1<UnityEngine.UIVertex>>::EndInvoke(System.IAsyncResult)
#define UnityAction_1_EndInvoke_m15839(__this, ___result, method) (( void (*) (UnityAction_1_t2180 *, Object_t *, const MethodInfo*))UnityAction_1_EndInvoke_m11935_gshared)(__this, ___result, method)
