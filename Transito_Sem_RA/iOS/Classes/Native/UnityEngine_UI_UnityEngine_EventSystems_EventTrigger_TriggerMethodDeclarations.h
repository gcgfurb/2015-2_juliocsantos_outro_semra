﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.EventSystems.EventTrigger/TriggerEvent
struct TriggerEvent_t109;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.EventSystems.EventTrigger/TriggerEvent::.ctor()
extern "C" void TriggerEvent__ctor_m486 (TriggerEvent_t109 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
