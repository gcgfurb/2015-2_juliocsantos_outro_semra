﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m16624(__this, ___l, method) (( void (*) (Enumerator_t666 *, List_1_t555 *, const MethodInfo*))Enumerator__ctor_m11324_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16625(__this, method) (( void (*) (Enumerator_t666 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11325_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16626(__this, method) (( Object_t * (*) (Enumerator_t666 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11326_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::Dispose()
#define Enumerator_Dispose_m16627(__this, method) (( void (*) (Enumerator_t666 *, const MethodInfo*))Enumerator_Dispose_m11327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::VerifyState()
#define Enumerator_VerifyState_m16628(__this, method) (( void (*) (Enumerator_t666 *, const MethodInfo*))Enumerator_VerifyState_m11328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::MoveNext()
#define Enumerator_MoveNext_m3622(__this, method) (( bool (*) (Enumerator_t666 *, const MethodInfo*))Enumerator_MoveNext_m11329_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.GUILayoutEntry>::get_Current()
#define Enumerator_get_Current_m3621(__this, method) (( GUILayoutEntry_t554 * (*) (Enumerator_t666 *, const MethodInfo*))Enumerator_get_Current_m11330_gshared)(__this, method)
