﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array/ArrayReadOnlyList`1<System.Object>
struct ArrayReadOnlyList_1_t2344;
// System.Object[]
struct ObjectU5BU5D_t77;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2089;
// System.Exception
struct Exception_t68;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::.ctor(T[])
extern "C" void ArrayReadOnlyList_1__ctor_m17699_gshared (ArrayReadOnlyList_1_t2344 * __this, ObjectU5BU5D_t77* ___array, const MethodInfo* method);
#define ArrayReadOnlyList_1__ctor_m17699(__this, ___array, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, ObjectU5BU5D_t77*, const MethodInfo*))ArrayReadOnlyList_1__ctor_m17699_gshared)(__this, ___array, method)
// System.Collections.IEnumerator System.Array/ArrayReadOnlyList`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m17700_gshared (ArrayReadOnlyList_1_t2344 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m17700(__this, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2344 *, const MethodInfo*))ArrayReadOnlyList_1_System_Collections_IEnumerable_GetEnumerator_m17700_gshared)(__this, method)
// T System.Array/ArrayReadOnlyList`1<System.Object>::get_Item(System.Int32)
extern "C" Object_t * ArrayReadOnlyList_1_get_Item_m17701_gshared (ArrayReadOnlyList_1_t2344 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Item_m17701(__this, ___index, method) (( Object_t * (*) (ArrayReadOnlyList_1_t2344 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_get_Item_m17701_gshared)(__this, ___index, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::set_Item(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_set_Item_m17702_gshared (ArrayReadOnlyList_1_t2344 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define ArrayReadOnlyList_1_set_Item_m17702(__this, ___index, ___value, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_set_Item_m17702_gshared)(__this, ___index, ___value, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::get_Count()
extern "C" int32_t ArrayReadOnlyList_1_get_Count_m17703_gshared (ArrayReadOnlyList_1_t2344 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_Count_m17703(__this, method) (( int32_t (*) (ArrayReadOnlyList_1_t2344 *, const MethodInfo*))ArrayReadOnlyList_1_get_Count_m17703_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::get_IsReadOnly()
extern "C" bool ArrayReadOnlyList_1_get_IsReadOnly_m17704_gshared (ArrayReadOnlyList_1_t2344 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_get_IsReadOnly_m17704(__this, method) (( bool (*) (ArrayReadOnlyList_1_t2344 *, const MethodInfo*))ArrayReadOnlyList_1_get_IsReadOnly_m17704_gshared)(__this, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Add(T)
extern "C" void ArrayReadOnlyList_1_Add_m17705_gshared (ArrayReadOnlyList_1_t2344 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Add_m17705(__this, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Add_m17705_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Clear()
extern "C" void ArrayReadOnlyList_1_Clear_m17706_gshared (ArrayReadOnlyList_1_t2344 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_Clear_m17706(__this, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, const MethodInfo*))ArrayReadOnlyList_1_Clear_m17706_gshared)(__this, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Contains(T)
extern "C" bool ArrayReadOnlyList_1_Contains_m17707_gshared (ArrayReadOnlyList_1_t2344 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Contains_m17707(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2344 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Contains_m17707_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::CopyTo(T[],System.Int32)
extern "C" void ArrayReadOnlyList_1_CopyTo_m17708_gshared (ArrayReadOnlyList_1_t2344 * __this, ObjectU5BU5D_t77* ___array, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_CopyTo_m17708(__this, ___array, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, ObjectU5BU5D_t77*, int32_t, const MethodInfo*))ArrayReadOnlyList_1_CopyTo_m17708_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Array/ArrayReadOnlyList`1<System.Object>::GetEnumerator()
extern "C" Object_t* ArrayReadOnlyList_1_GetEnumerator_m17709_gshared (ArrayReadOnlyList_1_t2344 * __this, const MethodInfo* method);
#define ArrayReadOnlyList_1_GetEnumerator_m17709(__this, method) (( Object_t* (*) (ArrayReadOnlyList_1_t2344 *, const MethodInfo*))ArrayReadOnlyList_1_GetEnumerator_m17709_gshared)(__this, method)
// System.Int32 System.Array/ArrayReadOnlyList`1<System.Object>::IndexOf(T)
extern "C" int32_t ArrayReadOnlyList_1_IndexOf_m17710_gshared (ArrayReadOnlyList_1_t2344 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_IndexOf_m17710(__this, ___item, method) (( int32_t (*) (ArrayReadOnlyList_1_t2344 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_IndexOf_m17710_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::Insert(System.Int32,T)
extern "C" void ArrayReadOnlyList_1_Insert_m17711_gshared (ArrayReadOnlyList_1_t2344 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Insert_m17711(__this, ___index, ___item, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, int32_t, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Insert_m17711_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Array/ArrayReadOnlyList`1<System.Object>::Remove(T)
extern "C" bool ArrayReadOnlyList_1_Remove_m17712_gshared (ArrayReadOnlyList_1_t2344 * __this, Object_t * ___item, const MethodInfo* method);
#define ArrayReadOnlyList_1_Remove_m17712(__this, ___item, method) (( bool (*) (ArrayReadOnlyList_1_t2344 *, Object_t *, const MethodInfo*))ArrayReadOnlyList_1_Remove_m17712_gshared)(__this, ___item, method)
// System.Void System.Array/ArrayReadOnlyList`1<System.Object>::RemoveAt(System.Int32)
extern "C" void ArrayReadOnlyList_1_RemoveAt_m17713_gshared (ArrayReadOnlyList_1_t2344 * __this, int32_t ___index, const MethodInfo* method);
#define ArrayReadOnlyList_1_RemoveAt_m17713(__this, ___index, method) (( void (*) (ArrayReadOnlyList_1_t2344 *, int32_t, const MethodInfo*))ArrayReadOnlyList_1_RemoveAt_m17713_gshared)(__this, ___index, method)
// System.Exception System.Array/ArrayReadOnlyList`1<System.Object>::ReadOnlyError()
extern "C" Exception_t68 * ArrayReadOnlyList_1_ReadOnlyError_m17714_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ArrayReadOnlyList_1_ReadOnlyError_m17714(__this /* static, unused */, method) (( Exception_t68 * (*) (Object_t * /* static, unused */, const MethodInfo*))ArrayReadOnlyList_1_ReadOnlyError_m17714_gshared)(__this /* static, unused */, method)
