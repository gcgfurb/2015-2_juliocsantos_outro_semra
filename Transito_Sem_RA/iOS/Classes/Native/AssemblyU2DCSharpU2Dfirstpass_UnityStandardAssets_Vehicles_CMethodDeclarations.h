﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.BrakeLight
struct BrakeLight_t28;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::.ctor()
extern "C" void BrakeLight__ctor_m131 (BrakeLight_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Start()
extern "C" void BrakeLight_Start_m132 (BrakeLight_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Update()
extern "C" void BrakeLight_Update_m133 (BrakeLight_t28 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
