﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Gradient
struct Gradient_t468;
struct Gradient_t468_marshaled;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.Gradient::.ctor()
extern "C" void Gradient__ctor_m2594 (Gradient_t468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Init()
extern "C" void Gradient_Init_m2595 (Gradient_t468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Cleanup()
extern "C" void Gradient_Cleanup_m2596 (Gradient_t468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.Gradient::Finalize()
extern "C" void Gradient_Finalize_m2597 (Gradient_t468 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
extern "C" void Gradient_t468_marshal(const Gradient_t468& unmarshaled, Gradient_t468_marshaled& marshaled);
extern "C" void Gradient_t468_marshal_back(const Gradient_t468_marshaled& marshaled, Gradient_t468& unmarshaled);
extern "C" void Gradient_t468_marshal_cleanup(Gradient_t468_marshaled& marshaled);
