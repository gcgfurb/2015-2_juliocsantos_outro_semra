﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__3MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m14070(__this, ___dictionary, method) (( void (*) (Enumerator_t2045 *, Dictionary_2_t387 *, const MethodInfo*))Enumerator__ctor_m11617_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m14071(__this, method) (( Object_t * (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11618_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m14072(__this, method) (( void (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11619_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m14073(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11620_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m14074(__this, method) (( Object_t * (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11621_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m14075(__this, method) (( Object_t * (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11622_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::MoveNext()
#define Enumerator_MoveNext_m14076(__this, method) (( bool (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_MoveNext_m11623_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_Current()
#define Enumerator_get_Current_m14077(__this, method) (( KeyValuePair_2_t2043  (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_get_Current_m11624_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14078(__this, method) (( Object_t * (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_get_CurrentKey_m11625_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14079(__this, method) (( int32_t (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_get_CurrentValue_m11626_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Reset()
#define Enumerator_Reset_m14080(__this, method) (( void (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_Reset_m11627_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyState()
#define Enumerator_VerifyState_m14081(__this, method) (( void (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_VerifyState_m11628_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14082(__this, method) (( void (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_VerifyCurrent_m11629_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.UI.ICanvasElement,System.Int32>::Dispose()
#define Enumerator_Dispose_m14083(__this, method) (( void (*) (Enumerator_t2045 *, const MethodInfo*))Enumerator_Dispose_m11630_gshared)(__this, method)
