﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m16610(__this, ___dictionary, method) (( void (*) (Enumerator_t2242 *, Dictionary_2_t552 *, const MethodInfo*))Enumerator__ctor_m12782_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16611(__this, method) (( Object_t * (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12783_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16612(__this, method) (( void (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12784_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m16613(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12785_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m16614(__this, method) (( Object_t * (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12786_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m16615(__this, method) (( Object_t * (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::MoveNext()
#define Enumerator_MoveNext_m16616(__this, method) (( bool (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_MoveNext_m12788_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Current()
#define Enumerator_get_Current_m16617(__this, method) (( KeyValuePair_2_t2240  (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_get_Current_m12789_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m16618(__this, method) (( int32_t (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_get_CurrentKey_m12790_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m16619(__this, method) (( LayoutCache_t549 * (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_get_CurrentValue_m12791_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Reset()
#define Enumerator_Reset_m16620(__this, method) (( void (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_Reset_m12792_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyState()
#define Enumerator_VerifyState_m16621(__this, method) (( void (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_VerifyState_m12793_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m16622(__this, method) (( void (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_VerifyCurrent_m12794_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::Dispose()
#define Enumerator_Dispose_m16623(__this, method) (( void (*) (Enumerator_t2242 *, const MethodInfo*))Enumerator_Dispose_m12795_gshared)(__this, method)
