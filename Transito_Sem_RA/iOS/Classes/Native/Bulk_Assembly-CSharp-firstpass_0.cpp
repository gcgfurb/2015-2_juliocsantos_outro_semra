﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// UnityStandardAssets.CrossPlatformInput.AxisTouchButton
struct AxisTouchButton_t1;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t57;
// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct ButtonHandler_t4;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis
struct VirtualAxis_t3;
// System.String
struct String_t;
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton
struct VirtualButton_t6;
// UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar
struct InputAxisScrollbar_t9;
// UnityStandardAssets.CrossPlatformInput.Joystick
struct Joystick_t11;
// UnityStandardAssets.CrossPlatformInput.MobileControlRig
struct MobileControlRig_t13;
// UnityEngine.EventSystems.EventSystem
struct EventSystem_t65;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.StandaloneInputModule
struct StandaloneInputModule_t66;
// UnityEngine.EventSystems.TouchInputModule
struct TouchInputModule_t67;
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput
struct MobileInput_t14;
// UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput
struct StandaloneInput_t15;
// UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping
struct AxisMapping_t18;
// UnityStandardAssets.CrossPlatformInput.TiltInput
struct TiltInput_t19;
// UnityStandardAssets.CrossPlatformInput.TouchPad
struct TouchPad_t22;
// UnityEngine.UI.Image
struct Image_t24;
// UnityStandardAssets.CrossPlatformInput.VirtualInput
struct VirtualInput_t8;
// UnityStandardAssets.Vehicles.Car.BrakeLight
struct BrakeLight_t28;
// UnityEngine.Renderer
struct Renderer_t30;
// UnityStandardAssets.Vehicles.Car.CarAIControl
struct CarAIControl_t32;
// UnityStandardAssets.Vehicles.Car.CarController
struct CarController_t29;
// UnityEngine.Rigidbody
struct Rigidbody_t34;
// UnityEngine.Collision
struct Collision_t58;
// UnityEngine.Transform
struct Transform_t33;
// UnityStandardAssets.Vehicles.Car.CarAudio
struct CarAudio_t36;
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t73;
// System.Object[]
struct ObjectU5BU5D_t77;
// UnityEngine.AudioSource
struct AudioSource_t38;
// UnityEngine.AudioClip
struct AudioClip_t37;
// UnityStandardAssets.Vehicles.Car.CarSelfRighting
struct CarSelfRighting_t46;
// UnityStandardAssets.Vehicles.Car.CarUserControl
struct CarUserControl_t47;
// UnityStandardAssets.Vehicles.Car.Mudguard
struct Mudguard_t48;
// UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t49;
// UnityStandardAssets.Vehicles.Car.SkidTrail
struct SkidTrail_t50;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// UnityStandardAssets.Vehicles.Car.Suspension
struct Suspension_t51;
// UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1
struct U3CStartSkidTrailU3Ec__Iterator1_t53;
// UnityStandardAssets.Vehicles.Car.WheelEffects
struct WheelEffects_t54;
// UnityEngine.ParticleSystem
struct ParticleSystem_t55;
// UnityEngine.WheelCollider
struct WheelCollider_t56;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3E.h"
#include "AssemblyU2DCSharpU2Dfirstpass_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatfMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_2MethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_2.h"
#include "mscorlib_System_TypeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "mscorlib_System_StringMethodDeclarations.h"
#include "Assembly-CSharp-firstpass_ArrayTypes.h"
#include "mscorlib_System_Int32.h"
#include "mscorlib_System_Type.h"
#include "mscorlib_System_RuntimeTypeHandle.h"
#include "UnityEngine_ArrayTypes.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventData.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_0MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_1.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_1MethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_3.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_3MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_4.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_9MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_11MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_9.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_10.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_11.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_10MethodDeclarations.h"
#include "mscorlib_System_ArgumentNullExceptionMethodDeclarations.h"
#include "mscorlib_System_ArgumentNullException.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_5.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_5MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_6.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_6MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_7.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_7MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_PointerEventDataMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_8.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_8MethodDeclarations.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_EventSystem.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_StandaloneInputModul.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_TouchInputModule.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_0MethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "mscorlib_System_ExceptionMethodDeclarations.h"
#include "mscorlib_System_Exception.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_12.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_12MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_13.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_13MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_14.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_14MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_15.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_15MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ScreenMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_16.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_16MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_17.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_17MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_18.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_CrossPlatf_18MethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Image.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_TouchMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Touch.h"
#include "mscorlib_System_Collections_Generic_List_1_genMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_List_1_gen.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_CMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Renderer.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_6MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RendererMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_6.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_0.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_0MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_1.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RandomMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Collision.h"
#include "UnityEngine_UnityEngine_CollisionMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_2.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_2MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_3.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioClip.h"
#include "UnityEngine_UnityEngine_AudioSource.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_AudioSourceMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_AudioClipMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_4.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_4MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_5.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_5MethodDeclarations.h"
#include "UnityEngine_UnityEngine_WheelCollider.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_14.h"
#include "UnityEngine_UnityEngine_ColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_WheelColliderMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WheelHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WheelHit.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_14MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_7.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_7MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_8.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_8MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_9.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_9MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_10.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_10MethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_11.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_11MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_12.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_12MethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_13.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_13MethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystemMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ParticleSystem.h"
#include "UnityEngine_UnityEngine_Coroutine.h"

// !!0 UnityEngine.Object::FindObjectOfType<System.Object>()
extern "C" Object_t * Object_FindObjectOfType_TisObject_t_m348_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define Object_FindObjectOfType_TisObject_t_m348(__this /* static, unused */, method) (( Object_t * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m348_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.Object::FindObjectOfType<UnityEngine.EventSystems.EventSystem>()
#define Object_FindObjectOfType_TisEventSystem_t65_m232(__this /* static, unused */, method) (( EventSystem_t65 * (*) (Object_t * /* static, unused */, const MethodInfo*))Object_FindObjectOfType_TisObject_t_m348_gshared)(__this /* static, unused */, method)
// !!0 UnityEngine.GameObject::AddComponent<System.Object>()
extern "C" Object_t * GameObject_AddComponent_TisObject_t_m349_gshared (GameObject_t52 * __this, const MethodInfo* method);
#define GameObject_AddComponent_TisObject_t_m349(__this, method) (( Object_t * (*) (GameObject_t52 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m349_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.EventSystem>()
#define GameObject_AddComponent_TisEventSystem_t65_m234(__this, method) (( EventSystem_t65 * (*) (GameObject_t52 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m349_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.StandaloneInputModule>()
#define GameObject_AddComponent_TisStandaloneInputModule_t66_m235(__this, method) (( StandaloneInputModule_t66 * (*) (GameObject_t52 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m349_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.EventSystems.TouchInputModule>()
#define GameObject_AddComponent_TisTouchInputModule_t67_m236(__this, method) (( TouchInputModule_t67 * (*) (GameObject_t52 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m349_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m350_gshared (Component_t78 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m350(__this, method) (( Object_t * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.UI.Image>()
#define Component_GetComponent_TisImage_t24_m250(__this, method) (( Image_t24 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Renderer>()
#define Component_GetComponent_TisRenderer_t30_m264(__this, method) (( Renderer_t30 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Vehicles.Car.CarController>()
#define Component_GetComponent_TisCarController_t29_m266(__this, method) (( CarController_t29 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t34_m268(__this, method) (( Rigidbody_t34 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityStandardAssets.Vehicles.Car.CarAIControl>()
#define Component_GetComponent_TisCarAIControl_t32_m285(__this, method) (( CarAIControl_t32 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<System.Object>()
extern "C" ObjectU5BU5D_t77* Component_GetComponents_TisObject_t_m351_gshared (Component_t78 * __this, const MethodInfo* method);
#define Component_GetComponents_TisObject_t_m351(__this, method) (( ObjectU5BU5D_t77* (*) (Component_t78 *, const MethodInfo*))Component_GetComponents_TisObject_t_m351_gshared)(__this, method)
// !!0[] UnityEngine.Component::GetComponents<UnityEngine.AudioSource>()
#define Component_GetComponents_TisAudioSource_t38_m286(__this, method) (( AudioSourceU5BU5D_t73* (*) (Component_t78 *, const MethodInfo*))Component_GetComponents_TisObject_t_m351_gshared)(__this, method)
// !!0 UnityEngine.GameObject::AddComponent<UnityEngine.AudioSource>()
#define GameObject_AddComponent_TisAudioSource_t38_m294(__this, method) (( AudioSource_t38 * (*) (GameObject_t52 *, const MethodInfo*))GameObject_AddComponent_TisObject_t_m349_gshared)(__this, method)
// !!0 UnityEngine.Object::Instantiate<System.Object>(!!0)
extern "C" Object_t * Object_Instantiate_TisObject_t_m352_gshared (Object_t * __this /* static, unused */, Object_t * p0, const MethodInfo* method);
#define Object_Instantiate_TisObject_t_m352(__this /* static, unused */, p0, method) (( Object_t * (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Object_Instantiate_TisObject_t_m352_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Object::Instantiate<UnityEngine.Transform>(!!0)
#define Object_Instantiate_TisTransform_t33_m336(__this /* static, unused */, p0, method) (( Transform_t33 * (*) (Object_t * /* static, unused */, Transform_t33 *, const MethodInfo*))Object_Instantiate_TisObject_t_m352_gshared)(__this /* static, unused */, p0, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m353_gshared (Component_t78 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m353(__this, method) (( Object_t * (*) (Component_t78 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m353_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.ParticleSystem>()
#define Component_GetComponentInChildren_TisParticleSystem_t55_m340(__this, method) (( ParticleSystem_t55 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m353_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.WheelCollider>()
#define Component_GetComponent_TisWheelCollider_t56_m343(__this, method) (( WheelCollider_t56 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.AudioSource>()
#define Component_GetComponent_TisAudioSource_t38_m344(__this, method) (( AudioSource_t38 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::.ctor()
extern Il2CppCodeGenString* _stringLiteral0;
extern "C" void AxisTouchButton__ctor_m0 (AxisTouchButton_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___axisName_2 = _stringLiteral0;
		__this->___axisValue_3 = (1.0f);
		__this->___responseSpeed_4 = (3.0f);
		__this->___returnToCentreSpeed_5 = (3.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnEnable()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern TypeInfo* VirtualAxis_t3_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_OnEnable_m1 (AxisTouchButton_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		VirtualAxis_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___axisName_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		bool L_1 = CrossPlatformInputManager_AxisExists_m38(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		String_t* L_2 = (__this->___axisName_2);
		VirtualAxis_t3 * L_3 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_3, L_2, /*hidden argument*/NULL);
		__this->___m_Axis_7 = L_3;
		VirtualAxis_t3 * L_4 = (__this->___m_Axis_7);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		goto IL_0042;
	}

IL_0031:
	{
		String_t* L_5 = (__this->___axisName_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualAxis_t3 * L_6 = CrossPlatformInputManager_VirtualAxisReference_m44(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___m_Axis_7 = L_6;
	}

IL_0042:
	{
		AxisTouchButton_FindPairedButton_m2(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::FindPairedButton()
extern const Il2CppType* AxisTouchButton_t1_0_0_0_var;
extern TypeInfo* Type_t_il2cpp_TypeInfo_var;
extern TypeInfo* AxisTouchButtonU5BU5D_t60_il2cpp_TypeInfo_var;
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_FindPairedButton_m2 (AxisTouchButton_t1 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AxisTouchButton_t1_0_0_0_var = il2cpp_codegen_type_from_index(3);
		Type_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(4);
		AxisTouchButtonU5BU5D_t60_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(5);
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	AxisTouchButtonU5BU5D_t60* V_0 = {0};
	int32_t V_1 = 0;
	{
		IL2CPP_RUNTIME_CLASS_INIT(Type_t_il2cpp_TypeInfo_var);
		Type_t * L_0 = Type_GetTypeFromHandle_m213(NULL /*static, unused*/, LoadTypeToken(AxisTouchButton_t1_0_0_0_var), /*hidden argument*/NULL);
		ObjectU5BU5D_t61* L_1 = Object_FindObjectsOfType_m214(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		V_0 = ((AxisTouchButtonU5BU5D_t60*)IsInst(L_1, AxisTouchButtonU5BU5D_t60_il2cpp_TypeInfo_var));
		AxisTouchButtonU5BU5D_t60* L_2 = V_0;
		if (!L_2)
		{
			goto IL_005e;
		}
	}
	{
		V_1 = 0;
		goto IL_0055;
	}

IL_0022:
	{
		AxisTouchButtonU5BU5D_t60* L_3 = V_0;
		int32_t L_4 = V_1;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(AxisTouchButton_t1 **)(AxisTouchButton_t1 **)SZArrayLdElema(L_3, L_5, sizeof(AxisTouchButton_t1 *))));
		String_t* L_6 = ((*(AxisTouchButton_t1 **)(AxisTouchButton_t1 **)SZArrayLdElema(L_3, L_5, sizeof(AxisTouchButton_t1 *)))->___axisName_2);
		String_t* L_7 = (__this->___axisName_2);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		bool L_8 = String_op_Equality_m215(NULL /*static, unused*/, L_6, L_7, /*hidden argument*/NULL);
		if (!L_8)
		{
			goto IL_0051;
		}
	}
	{
		AxisTouchButtonU5BU5D_t60* L_9 = V_0;
		int32_t L_10 = V_1;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		bool L_12 = Object_op_Inequality_m216(NULL /*static, unused*/, (*(AxisTouchButton_t1 **)(AxisTouchButton_t1 **)SZArrayLdElema(L_9, L_11, sizeof(AxisTouchButton_t1 *))), __this, /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_0051;
		}
	}
	{
		AxisTouchButtonU5BU5D_t60* L_13 = V_0;
		int32_t L_14 = V_1;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		__this->___m_PairedWith_6 = (*(AxisTouchButton_t1 **)(AxisTouchButton_t1 **)SZArrayLdElema(L_13, L_15, sizeof(AxisTouchButton_t1 *)));
	}

IL_0051:
	{
		int32_t L_16 = V_1;
		V_1 = ((int32_t)((int32_t)L_16+(int32_t)1));
	}

IL_0055:
	{
		int32_t L_17 = V_1;
		AxisTouchButtonU5BU5D_t60* L_18 = V_0;
		NullCheck(L_18);
		if ((((int32_t)L_17) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_18)->max_length)))))))
		{
			goto IL_0022;
		}
	}

IL_005e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnDisable()
extern "C" void AxisTouchButton_OnDisable_m3 (AxisTouchButton_t1 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t3 * L_0 = (__this->___m_Axis_7);
		NullCheck(L_0);
		VirtualAxis_Remove_m20(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_OnPointerDown_m4 (AxisTouchButton_t1 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisTouchButton_t1 * L_0 = (__this->___m_PairedWith_6);
		bool L_1 = Object_op_Equality_m217(NULL /*static, unused*/, L_0, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0017;
		}
	}
	{
		AxisTouchButton_FindPairedButton_m2(__this, /*hidden argument*/NULL);
	}

IL_0017:
	{
		VirtualAxis_t3 * L_2 = (__this->___m_Axis_7);
		VirtualAxis_t3 * L_3 = (__this->___m_Axis_7);
		NullCheck(L_3);
		float L_4 = VirtualAxis_get_GetValue_m22(L_3, /*hidden argument*/NULL);
		float L_5 = (__this->___axisValue_3);
		float L_6 = (__this->___responseSpeed_4);
		float L_7 = Time_get_deltaTime_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_8 = Mathf_MoveTowards_m219(NULL /*static, unused*/, L_4, L_5, ((float)((float)L_6*(float)L_7)), /*hidden argument*/NULL);
		NullCheck(L_2);
		VirtualAxis_Update_m21(L_2, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.AxisTouchButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void AxisTouchButton_OnPointerUp_m5 (AxisTouchButton_t1 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		VirtualAxis_t3 * L_0 = (__this->___m_Axis_7);
		VirtualAxis_t3 * L_1 = (__this->___m_Axis_7);
		NullCheck(L_1);
		float L_2 = VirtualAxis_get_GetValue_m22(L_1, /*hidden argument*/NULL);
		float L_3 = (__this->___responseSpeed_4);
		float L_4 = Time_get_deltaTime_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_5 = Mathf_MoveTowards_m219(NULL /*static, unused*/, L_2, (0.0f), ((float)((float)L_3*(float)L_4)), /*hidden argument*/NULL);
		NullCheck(L_0);
		VirtualAxis_Update_m21(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern "C" void ButtonHandler__ctor_m6 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern "C" void ButtonHandler_OnEnable_m7 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetDownState_m8 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetButtonDown_m51(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetUpState_m9 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetButtonUp_m52(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetAxisPositiveState_m10 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxisPositive_m53(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetAxisNeutralState_m11 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxisZero_m55(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void ButtonHandler_SetAxisNegativeState_m12 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___Name_2);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxisNegative_m54(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern "C" void ButtonHandler_Update_m13 (ButtonHandler_t4 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String)
extern "C" void VirtualAxis__ctor_m14 (VirtualAxis_t3 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		VirtualAxis__ctor_m15(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::.ctor(System.String,System.Boolean)
extern "C" void VirtualAxis__ctor_m15 (VirtualAxis_t3 * __this, String_t* ___name, bool ___matchToInputSettings, const MethodInfo* method)
{
	{
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		VirtualAxis_set_name_m17(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___matchToInputSettings;
		VirtualAxis_set_matchWithInputManager_m19(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_name()
extern "C" String_t* VirtualAxis_get_name_m16 (VirtualAxis_t3 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_1);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_name(System.String)
extern "C" void VirtualAxis_set_name_m17 (VirtualAxis_t3 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_1 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_matchWithInputManager()
extern "C" bool VirtualAxis_get_matchWithInputManager_m18 (VirtualAxis_t3 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CmatchWithInputManagerU3Ek__BackingField_2);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::set_matchWithInputManager(System.Boolean)
extern "C" void VirtualAxis_set_matchWithInputManager_m19 (VirtualAxis_t3 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CmatchWithInputManagerU3Ek__BackingField_2 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Remove()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void VirtualAxis_Remove_m20 (VirtualAxis_t3 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtualAxis_get_name_m16(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualAxis_m42(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::Update(System.Single)
extern "C" void VirtualAxis_Update_m21 (VirtualAxis_t3 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___m_Value_0 = L_0;
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValue()
extern "C" float VirtualAxis_get_GetValue_m22 (VirtualAxis_t3 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Value_0);
		return L_0;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis::get_GetValueRaw()
extern "C" float VirtualAxis_get_GetValueRaw_m23 (VirtualAxis_t3 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Value_0);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String)
extern "C" void VirtualButton__ctor_m24 (VirtualButton_t6 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		String_t* L_0 = ___name;
		VirtualButton__ctor_m25(__this, L_0, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::.ctor(System.String,System.Boolean)
extern "C" void VirtualButton__ctor_m25 (VirtualButton_t6 * __this, String_t* ___name, bool ___matchToInputSettings, const MethodInfo* method)
{
	{
		__this->___m_LastPressedFrame_0 = ((int32_t)-5);
		__this->___m_ReleasedFrame_1 = ((int32_t)-5);
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		String_t* L_0 = ___name;
		VirtualButton_set_name_m27(__this, L_0, /*hidden argument*/NULL);
		bool L_1 = ___matchToInputSettings;
		VirtualButton_set_matchWithInputManager_m29(__this, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.String UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_name()
extern "C" String_t* VirtualButton_get_name_m26 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		String_t* L_0 = (__this->___U3CnameU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_name(System.String)
extern "C" void VirtualButton_set_name_m27 (VirtualButton_t6 * __this, String_t* ___value, const MethodInfo* method)
{
	{
		String_t* L_0 = ___value;
		__this->___U3CnameU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_matchWithInputManager()
extern "C" bool VirtualButton_get_matchWithInputManager_m28 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CmatchWithInputManagerU3Ek__BackingField_4);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::set_matchWithInputManager(System.Boolean)
extern "C" void VirtualButton_set_matchWithInputManager_m29 (VirtualButton_t6 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CmatchWithInputManagerU3Ek__BackingField_4 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Pressed()
extern "C" void VirtualButton_Pressed_m30 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Pressed_2);
		if (!L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		__this->___m_Pressed_2 = 1;
		int32_t L_1 = Time_get_frameCount_m221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastPressedFrame_0 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Released()
extern "C" void VirtualButton_Released_m31 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		__this->___m_Pressed_2 = 0;
		int32_t L_0 = Time_get_frameCount_m221(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_ReleasedFrame_1 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::Remove()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void VirtualButton_Remove_m32 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = VirtualButton_get_name_m26(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualButton_m43(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButton()
extern "C" bool VirtualButton_get_GetButton_m33 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Pressed_2);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonDown()
extern "C" bool VirtualButton_get_GetButtonDown_m34 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_LastPressedFrame_0);
		int32_t L_1 = Time_get_frameCount_m221(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)((int32_t)((int32_t)L_0-(int32_t)L_1))) == ((int32_t)(-1)))? 1 : 0);
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton::get_GetButtonUp()
extern "C" bool VirtualButton_get_GetButtonUp_m35 (VirtualButton_t6 * __this, const MethodInfo* method)
{
	{
		int32_t L_0 = (__this->___m_ReleasedFrame_1);
		int32_t L_1 = Time_get_frameCount_m221(NULL /*static, unused*/, /*hidden argument*/NULL);
		return ((((int32_t)L_0) == ((int32_t)((int32_t)((int32_t)L_1-(int32_t)1))))? 1 : 0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::.cctor()
extern TypeInfo* MobileInput_t14_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern TypeInfo* StandaloneInput_t15_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager__cctor_m36 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		MobileInput_t14_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(8);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		StandaloneInput_t15_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		MobileInput_t14 * L_0 = (MobileInput_t14 *)il2cpp_codegen_object_new (MobileInput_t14_il2cpp_TypeInfo_var);
		MobileInput__ctor_m78(L_0, /*hidden argument*/NULL);
		((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___s_TouchInput_1 = L_0;
		StandaloneInput_t15 * L_1 = (StandaloneInput_t15 *)il2cpp_codegen_object_new (StandaloneInput_t15_il2cpp_TypeInfo_var);
		StandaloneInput__ctor_m92(L_1, /*hidden argument*/NULL);
		((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___s_HardwareInput_2 = L_1;
		VirtualInput_t8 * L_2 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___s_TouchInput_1;
		((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0 = L_2;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SwitchActiveInputMethod(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/ActiveInputMethod)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SwitchActiveInputMethod_m37 (Object_t * __this /* static, unused */, int32_t ___activeInputMethod, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = {0};
	{
		int32_t L_0 = ___activeInputMethod;
		V_0 = L_0;
		int32_t L_1 = V_0;
		if (!L_1)
		{
			goto IL_0014;
		}
	}
	{
		int32_t L_2 = V_0;
		if ((((int32_t)L_2) == ((int32_t)1)))
		{
			goto IL_0023;
		}
	}
	{
		goto IL_0032;
	}

IL_0014:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_3 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___s_HardwareInput_2;
		((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0 = L_3;
		goto IL_0032;
	}

IL_0023:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_4 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___s_TouchInput_1;
		((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0 = L_4;
		goto IL_0032;
	}

IL_0032:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::AxisExists(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_AxisExists_m38 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = VirtualInput_AxisExists_m121(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::ButtonExists(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_ButtonExists_m39 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = VirtualInput_ButtonExists_m122(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_RegisterVirtualAxis_m40 (Object_t * __this /* static, unused */, VirtualAxis_t3 * ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		VirtualAxis_t3 * L_1 = ___axis;
		NullCheck(L_0);
		VirtualInput_RegisterVirtualAxis_m123(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_RegisterVirtualButton_m41 (Object_t * __this /* static, unused */, VirtualButton_t6 * ___button, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		VirtualButton_t6 * L_1 = ___button;
		NullCheck(L_0);
		VirtualInput_RegisterVirtualButton_m124(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualAxis(System.String)
extern TypeInfo* ArgumentNullException_t64_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral1;
extern "C" void CrossPlatformInputManager_UnRegisterVirtualAxis_m42 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		ArgumentNullException_t64_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(10);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral1 = il2cpp_codegen_string_literal_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		if (L_0)
		{
			goto IL_0011;
		}
	}
	{
		ArgumentNullException_t64 * L_1 = (ArgumentNullException_t64 *)il2cpp_codegen_object_new (ArgumentNullException_t64_il2cpp_TypeInfo_var);
		ArgumentNullException__ctor_m222(L_1, _stringLiteral1, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_1);
	}

IL_0011:
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_2 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_3 = ___name;
		NullCheck(L_2);
		VirtualInput_UnRegisterVirtualAxis_m125(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::UnRegisterVirtualButton(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_UnRegisterVirtualButton_m43 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtualInput_UnRegisterVirtualButton_m126(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::VirtualAxisReference(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" VirtualAxis_t3 * CrossPlatformInputManager_VirtualAxisReference_m44 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtualAxis_t3 * L_2 = VirtualInput_VirtualAxisReference_m127(L_0, L_1, /*hidden argument*/NULL);
		return L_2;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" float CrossPlatformInputManager_GetAxis_m45 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		float L_1 = CrossPlatformInputManager_GetAxis_m47(NULL /*static, unused*/, L_0, 0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxisRaw(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" float CrossPlatformInputManager_GetAxisRaw_m46 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		float L_1 = CrossPlatformInputManager_GetAxis_m47(NULL /*static, unused*/, L_0, 1, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetAxis(System.String,System.Boolean)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" float CrossPlatformInputManager_GetAxis_m47 (Object_t * __this /* static, unused */, String_t* ___name, bool ___raw, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		bool L_2 = ___raw;
		NullCheck(L_0);
		float L_3 = (float)VirtFuncInvoker2< float, String_t*, bool >::Invoke(4 /* System.Single UnityStandardAssets.CrossPlatformInput.VirtualInput::GetAxis(System.String,System.Boolean) */, L_0, L_1, L_2);
		return L_3;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButton(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_GetButton_m48 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(5 /* System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButton(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonDown(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_GetButtonDown_m49 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(6 /* System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonDown(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::GetButtonUp(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" bool CrossPlatformInputManager_GetButtonUp_m50 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(7 /* System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::GetButtonUp(System.String) */, L_0, L_1);
		return L_2;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonDown(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetButtonDown_m51 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(8 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonDown(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetButtonUp(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetButtonUp_m52 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(9 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetButtonUp(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisPositive(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxisPositive_m53 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(10 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisPositive(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisNegative(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxisNegative_m54 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(11 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisNegative(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxisZero(System.String)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxisZero_m55 (Object_t * __this /* static, unused */, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		NullCheck(L_0);
		VirtActionInvoker1< String_t* >::Invoke(12 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxisZero(System.String) */, L_0, L_1);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetAxis(System.String,System.Single)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetAxis_m56 (Object_t * __this /* static, unused */, String_t* ___name, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		String_t* L_1 = ___name;
		float L_2 = ___value;
		NullCheck(L_0);
		VirtActionInvoker2< String_t*, float >::Invoke(13 /* System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetAxis(System.String,System.Single) */, L_0, L_1, L_2);
		return;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::get_mousePosition()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" Vector3_t12  CrossPlatformInputManager_get_mousePosition_m57 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		NullCheck(L_0);
		Vector3_t12  L_1 = (Vector3_t12 )VirtFuncInvoker0< Vector3_t12  >::Invoke(14 /* UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::MousePosition() */, L_0);
		return L_1;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionX(System.Single)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionX_m58 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		float L_1 = ___f;
		NullCheck(L_0);
		VirtualInput_SetVirtualMousePositionX_m128(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionY(System.Single)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionY_m59 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		float L_1 = ___f;
		NullCheck(L_0);
		VirtualInput_SetVirtualMousePositionY_m129(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager::SetVirtualMousePositionZ(System.Single)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionZ_m60 (Object_t * __this /* static, unused */, float ___f, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		VirtualInput_t8 * L_0 = ((CrossPlatformInputManager_t7_StaticFields*)CrossPlatformInputManager_t7_il2cpp_TypeInfo_var->static_fields)->___activeInput_0;
		float L_1 = ___f;
		NullCheck(L_0);
		VirtualInput_SetVirtualMousePositionZ_m130(L_0, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::.ctor()
extern "C" void InputAxisScrollbar__ctor_m61 (InputAxisScrollbar_t9 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::Update()
extern "C" void InputAxisScrollbar_Update_m62 (InputAxisScrollbar_t9 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.InputAxisScrollbar::HandleInput(System.Single)
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void InputAxisScrollbar_HandleInput_m63 (InputAxisScrollbar_t9 * __this, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___axis_2);
		float L_1 = ___value;
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetAxis_m56(NULL /*static, unused*/, L_0, ((float)((float)((float)((float)L_1*(float)(2.0f)))-(float)(1.0f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::.ctor()
extern Il2CppCodeGenString* _stringLiteral0;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void Joystick__ctor_m64 (Joystick_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___MovementRange_2 = ((int32_t)100);
		__this->___horizontalAxisName_4 = _stringLiteral0;
		__this->___verticalAxisName_5 = _stringLiteral2;
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnEnable()
extern "C" void Joystick_OnEnable_m65 (Joystick_t11 * __this, const MethodInfo* method)
{
	{
		Joystick_CreateVirtualAxes_m68(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::Start()
extern "C" void Joystick_Start_m66 (Joystick_t11 * __this, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t12  L_1 = Transform_get_position_m224(L_0, /*hidden argument*/NULL);
		__this->___m_StartPos_6 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::UpdateVirtualAxes(UnityEngine.Vector3)
extern "C" void Joystick_UpdateVirtualAxes_m67 (Joystick_t11 * __this, Vector3_t12  ___value, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	{
		Vector3_t12  L_0 = (__this->___m_StartPos_6);
		Vector3_t12  L_1 = ___value;
		Vector3_t12  L_2 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___y_2);
		(&V_0)->___y_2 = ((-L_3));
		Vector3_t12  L_4 = V_0;
		int32_t L_5 = (__this->___MovementRange_2);
		Vector3_t12  L_6 = Vector3_op_Division_m226(NULL /*static, unused*/, L_4, (((float)((float)L_5))), /*hidden argument*/NULL);
		V_0 = L_6;
		bool L_7 = (__this->___m_UseX_7);
		if (!L_7)
		{
			goto IL_0048;
		}
	}
	{
		VirtualAxis_t3 * L_8 = (__this->___m_HorizontalVirtualAxis_9);
		float L_9 = ((&V_0)->___x_1);
		NullCheck(L_8);
		VirtualAxis_Update_m21(L_8, ((-L_9)), /*hidden argument*/NULL);
	}

IL_0048:
	{
		bool L_10 = (__this->___m_UseY_8);
		if (!L_10)
		{
			goto IL_0065;
		}
	}
	{
		VirtualAxis_t3 * L_11 = (__this->___m_VerticalVirtualAxis_10);
		float L_12 = ((&V_0)->___y_2);
		NullCheck(L_11);
		VirtualAxis_Update_m21(L_11, L_12, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::CreateVirtualAxes()
extern TypeInfo* VirtualAxis_t3_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void Joystick_CreateVirtualAxes_m68 (Joystick_t11 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	Joystick_t11 * G_B2_0 = {0};
	Joystick_t11 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	Joystick_t11 * G_B3_1 = {0};
	Joystick_t11 * G_B5_0 = {0};
	Joystick_t11 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	Joystick_t11 * G_B6_1 = {0};
	{
		int32_t L_0 = (__this->___axesToUse_3);
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = (__this->___axesToUse_3);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0018:
	{
		NullCheck(G_B3_1);
		G_B3_1->___m_UseX_7 = G_B3_0;
		int32_t L_2 = (__this->___axesToUse_3);
		G_B4_0 = __this;
		if (!L_2)
		{
			G_B5_0 = __this;
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = (__this->___axesToUse_3);
		G_B6_0 = ((((int32_t)L_3) == ((int32_t)2))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_0035;
	}

IL_0034:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0035:
	{
		NullCheck(G_B6_1);
		G_B6_1->___m_UseY_8 = G_B6_0;
		bool L_4 = (__this->___m_UseX_7);
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_5 = (__this->___horizontalAxisName_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_6, L_5, /*hidden argument*/NULL);
		__this->___m_HorizontalVirtualAxis_9 = L_6;
		VirtualAxis_t3 * L_7 = (__this->___m_HorizontalVirtualAxis_9);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = (__this->___m_UseY_8);
		if (!L_8)
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_9 = (__this->___verticalAxisName_5);
		VirtualAxis_t3 * L_10 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_10, L_9, /*hidden argument*/NULL);
		__this->___m_VerticalVirtualAxis_10 = L_10;
		VirtualAxis_t3 * L_11 = (__this->___m_VerticalVirtualAxis_10);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDrag(UnityEngine.EventSystems.PointerEventData)
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void Joystick_OnDrag_m69 (Joystick_t11 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t12  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector2_t23  V_3 = {0};
	Vector2_t23  V_4 = {0};
	{
		Vector3_t12  L_0 = Vector3_get_zero_m227(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_0 = L_0;
		bool L_1 = (__this->___m_UseX_7);
		if (!L_1)
		{
			goto IL_004a;
		}
	}
	{
		PointerEventData_t57 * L_2 = ___data;
		NullCheck(L_2);
		Vector2_t23  L_3 = PointerEventData_get_position_m228(L_2, /*hidden argument*/NULL);
		V_3 = L_3;
		float L_4 = ((&V_3)->___x_1);
		Vector3_t12 * L_5 = &(__this->___m_StartPos_6);
		float L_6 = (L_5->___x_1);
		V_1 = (((int32_t)((int32_t)((float)((float)L_4-(float)L_6)))));
		int32_t L_7 = V_1;
		int32_t L_8 = (__this->___MovementRange_2);
		int32_t L_9 = (__this->___MovementRange_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		int32_t L_10 = Mathf_Clamp_m229(NULL /*static, unused*/, L_7, ((-L_8)), L_9, /*hidden argument*/NULL);
		V_1 = L_10;
		int32_t L_11 = V_1;
		(&V_0)->___x_1 = (((float)((float)L_11)));
	}

IL_004a:
	{
		bool L_12 = (__this->___m_UseY_8);
		if (!L_12)
		{
			goto IL_008f;
		}
	}
	{
		PointerEventData_t57 * L_13 = ___data;
		NullCheck(L_13);
		Vector2_t23  L_14 = PointerEventData_get_position_m228(L_13, /*hidden argument*/NULL);
		V_4 = L_14;
		float L_15 = ((&V_4)->___y_2);
		Vector3_t12 * L_16 = &(__this->___m_StartPos_6);
		float L_17 = (L_16->___y_2);
		V_2 = (((int32_t)((int32_t)((float)((float)L_15-(float)L_17)))));
		int32_t L_18 = V_2;
		int32_t L_19 = (__this->___MovementRange_2);
		int32_t L_20 = (__this->___MovementRange_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		int32_t L_21 = Mathf_Clamp_m229(NULL /*static, unused*/, L_18, ((-L_19)), L_20, /*hidden argument*/NULL);
		V_2 = L_21;
		int32_t L_22 = V_2;
		(&V_0)->___y_2 = (((float)((float)L_22)));
	}

IL_008f:
	{
		Transform_t33 * L_23 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12 * L_24 = &(__this->___m_StartPos_6);
		float L_25 = (L_24->___x_1);
		float L_26 = ((&V_0)->___x_1);
		Vector3_t12 * L_27 = &(__this->___m_StartPos_6);
		float L_28 = (L_27->___y_2);
		float L_29 = ((&V_0)->___y_2);
		Vector3_t12 * L_30 = &(__this->___m_StartPos_6);
		float L_31 = (L_30->___z_3);
		float L_32 = ((&V_0)->___z_3);
		Vector3_t12  L_33 = {0};
		Vector3__ctor_m230(&L_33, ((float)((float)L_25+(float)L_26)), ((float)((float)L_28+(float)L_29)), ((float)((float)L_31+(float)L_32)), /*hidden argument*/NULL);
		NullCheck(L_23);
		Transform_set_position_m231(L_23, L_33, /*hidden argument*/NULL);
		Transform_t33 * L_34 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_34);
		Vector3_t12  L_35 = Transform_get_position_m224(L_34, /*hidden argument*/NULL);
		Joystick_UpdateVirtualAxes_m67(__this, L_35, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void Joystick_OnPointerUp_m70 (Joystick_t11 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12  L_1 = (__this->___m_StartPos_6);
		NullCheck(L_0);
		Transform_set_position_m231(L_0, L_1, /*hidden argument*/NULL);
		Vector3_t12  L_2 = (__this->___m_StartPos_6);
		Joystick_UpdateVirtualAxes_m67(__this, L_2, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void Joystick_OnPointerDown_m71 (Joystick_t11 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.Joystick::OnDisable()
extern "C" void Joystick_OnDisable_m72 (Joystick_t11 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_UseX_7);
		if (!L_0)
		{
			goto IL_0016;
		}
	}
	{
		VirtualAxis_t3 * L_1 = (__this->___m_HorizontalVirtualAxis_9);
		NullCheck(L_1);
		VirtualAxis_Remove_m20(L_1, /*hidden argument*/NULL);
	}

IL_0016:
	{
		bool L_2 = (__this->___m_UseY_8);
		if (!L_2)
		{
			goto IL_002c;
		}
	}
	{
		VirtualAxis_t3 * L_3 = (__this->___m_VerticalVirtualAxis_10);
		NullCheck(L_3);
		VirtualAxis_Remove_m20(L_3, /*hidden argument*/NULL);
	}

IL_002c:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::.ctor()
extern "C" void MobileControlRig__ctor_m73 (MobileControlRig_t13 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::OnEnable()
extern "C" void MobileControlRig_OnEnable_m74 (MobileControlRig_t13 * __this, const MethodInfo* method)
{
	{
		MobileControlRig_CheckEnableControlRig_m76(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::Start()
extern TypeInfo* GameObject_t52_il2cpp_TypeInfo_var;
extern const MethodInfo* Object_FindObjectOfType_TisEventSystem_t65_m232_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisEventSystem_t65_m234_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisStandaloneInputModule_t66_m235_MethodInfo_var;
extern const MethodInfo* GameObject_AddComponent_TisTouchInputModule_t67_m236_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral3;
extern "C" void MobileControlRig_Start_m75 (MobileControlRig_t13 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_t52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Object_FindObjectOfType_TisEventSystem_t65_m232_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483649);
		GameObject_AddComponent_TisEventSystem_t65_m234_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483650);
		GameObject_AddComponent_TisStandaloneInputModule_t66_m235_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483651);
		GameObject_AddComponent_TisTouchInputModule_t67_m236_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483652);
		_stringLiteral3 = il2cpp_codegen_string_literal_from_index(3);
		s_Il2CppMethodIntialized = true;
	}
	EventSystem_t65 * V_0 = {0};
	GameObject_t52 * V_1 = {0};
	{
		EventSystem_t65 * L_0 = Object_FindObjectOfType_TisEventSystem_t65_m232(NULL /*static, unused*/, /*hidden argument*/Object_FindObjectOfType_TisEventSystem_t65_m232_MethodInfo_var);
		V_0 = L_0;
		EventSystem_t65 * L_1 = V_0;
		bool L_2 = Object_op_Equality_m217(NULL /*static, unused*/, L_1, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0032;
		}
	}
	{
		GameObject_t52 * L_3 = (GameObject_t52 *)il2cpp_codegen_object_new (GameObject_t52_il2cpp_TypeInfo_var);
		GameObject__ctor_m233(L_3, _stringLiteral3, /*hidden argument*/NULL);
		V_1 = L_3;
		GameObject_t52 * L_4 = V_1;
		NullCheck(L_4);
		GameObject_AddComponent_TisEventSystem_t65_m234(L_4, /*hidden argument*/GameObject_AddComponent_TisEventSystem_t65_m234_MethodInfo_var);
		GameObject_t52 * L_5 = V_1;
		NullCheck(L_5);
		GameObject_AddComponent_TisStandaloneInputModule_t66_m235(L_5, /*hidden argument*/GameObject_AddComponent_TisStandaloneInputModule_t66_m235_MethodInfo_var);
		GameObject_t52 * L_6 = V_1;
		NullCheck(L_6);
		GameObject_AddComponent_TisTouchInputModule_t67_m236(L_6, /*hidden argument*/GameObject_AddComponent_TisTouchInputModule_t67_m236_MethodInfo_var);
	}

IL_0032:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::CheckEnableControlRig()
extern "C" void MobileControlRig_CheckEnableControlRig_m76 (MobileControlRig_t13 * __this, const MethodInfo* method)
{
	{
		MobileControlRig_EnableControlRig_m77(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.MobileControlRig::EnableControlRig(System.Boolean)
extern TypeInfo* IEnumerator_t59_il2cpp_TypeInfo_var;
extern TypeInfo* Transform_t33_il2cpp_TypeInfo_var;
extern TypeInfo* IDisposable_t69_il2cpp_TypeInfo_var;
extern "C" void MobileControlRig_EnableControlRig_m77 (MobileControlRig_t13 * __this, bool ___enabled, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		IEnumerator_t59_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(15);
		Transform_t33_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(16);
		IDisposable_t69_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	Transform_t33 * V_0 = {0};
	Object_t * V_1 = {0};
	Object_t * V_2 = {0};
	Exception_t68 * __last_unhandled_exception = 0;
	NO_UNUSED_WARNING (__last_unhandled_exception);
	Exception_t68 * __exception_local = 0;
	NO_UNUSED_WARNING (__exception_local);
	int32_t __leave_target = 0;
	NO_UNUSED_WARNING (__leave_target);
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Object_t * L_1 = (Object_t *)VirtFuncInvoker0< Object_t * >::Invoke(4 /* System.Collections.IEnumerator UnityEngine.Transform::GetEnumerator() */, L_0);
		V_1 = L_1;
	}

IL_000c:
	try
	{ // begin try (depth: 1)
		{
			goto IL_0029;
		}

IL_0011:
		{
			Object_t * L_2 = V_1;
			NullCheck(L_2);
			Object_t * L_3 = (Object_t *)InterfaceFuncInvoker0< Object_t * >::Invoke(0 /* System.Object System.Collections.IEnumerator::get_Current() */, IEnumerator_t59_il2cpp_TypeInfo_var, L_2);
			V_0 = ((Transform_t33 *)CastclassClass(L_3, Transform_t33_il2cpp_TypeInfo_var));
			Transform_t33 * L_4 = V_0;
			NullCheck(L_4);
			GameObject_t52 * L_5 = Component_get_gameObject_m237(L_4, /*hidden argument*/NULL);
			bool L_6 = ___enabled;
			NullCheck(L_5);
			GameObject_SetActive_m238(L_5, L_6, /*hidden argument*/NULL);
		}

IL_0029:
		{
			Object_t * L_7 = V_1;
			NullCheck(L_7);
			bool L_8 = (bool)InterfaceFuncInvoker0< bool >::Invoke(1 /* System.Boolean System.Collections.IEnumerator::MoveNext() */, IEnumerator_t59_il2cpp_TypeInfo_var, L_7);
			if (L_8)
			{
				goto IL_0011;
			}
		}

IL_0034:
		{
			IL2CPP_LEAVE(0x4B, FINALLY_0039);
		}
	} // end try (depth: 1)
	catch(Il2CppExceptionWrapper& e)
	{
		__last_unhandled_exception = (Exception_t68 *)e.ex;
		goto FINALLY_0039;
	}

FINALLY_0039:
	{ // begin finally (depth: 1)
		{
			Object_t * L_9 = V_1;
			V_2 = ((Object_t *)IsInst(L_9, IDisposable_t69_il2cpp_TypeInfo_var));
			Object_t * L_10 = V_2;
			if (L_10)
			{
				goto IL_0044;
			}
		}

IL_0043:
		{
			IL2CPP_END_FINALLY(57)
		}

IL_0044:
		{
			Object_t * L_11 = V_2;
			NullCheck(L_11);
			InterfaceActionInvoker0::Invoke(0 /* System.Void System.IDisposable::Dispose() */, IDisposable_t69_il2cpp_TypeInfo_var, L_11);
			IL2CPP_END_FINALLY(57)
		}
	} // end finally (depth: 1)
	IL2CPP_CLEANUP(57)
	{
		IL2CPP_JUMP_TBL(0x4B, IL_004b)
		IL2CPP_RETHROW_IF_UNHANDLED(Exception_t68 *)
	}

IL_004b:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::.ctor()
extern "C" void MobileInput__ctor_m78 (MobileInput_t14 * __this, const MethodInfo* method)
{
	{
		VirtualInput__ctor_m118(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddButton(System.String)
extern TypeInfo* VirtualButton_t6_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void MobileInput_AddButton_m79 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualButton_t6_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(18);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		VirtualButton_t6 * L_1 = (VirtualButton_t6 *)il2cpp_codegen_object_new (VirtualButton_t6_il2cpp_TypeInfo_var);
		VirtualButton__ctor_m24(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualButton_m41(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::AddAxes(System.String)
extern TypeInfo* VirtualAxis_t3_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void MobileInput_AddAxes_m80 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		VirtualAxis_t3 * L_1 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_1, L_0, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetAxis(System.String,System.Boolean)
extern "C" float MobileInput_GetAxis_m81 (MobileInput_t14 * __this, String_t* ___name, bool ___raw, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m80(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t25 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)VirtFuncInvoker1< VirtualAxis_t3 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		float L_7 = VirtualAxis_get_GetValue_m22(L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonDown(System.String)
extern "C" void MobileInput_SetButtonDown_m82 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddButton_m79(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t26 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualButton_t6 * L_6 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualButton_Pressed_m30(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetButtonUp(System.String)
extern "C" void MobileInput_SetButtonUp_m83 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddButton_m79(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t26 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualButton_t6 * L_6 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualButton_Released_m31(L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisPositive(System.String)
extern "C" void MobileInput_SetAxisPositive_m84 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m80(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t25 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)VirtFuncInvoker1< VirtualAxis_t3 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualAxis_Update_m21(L_6, (1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisNegative(System.String)
extern "C" void MobileInput_SetAxisNegative_m85 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m80(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t25 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)VirtFuncInvoker1< VirtualAxis_t3 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualAxis_Update_m21(L_6, (-1.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxisZero(System.String)
extern "C" void MobileInput_SetAxisZero_m86 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m80(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t25 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)VirtFuncInvoker1< VirtualAxis_t3 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		NullCheck(L_6);
		VirtualAxis_Update_m21(L_6, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::SetAxis(System.String,System.Single)
extern "C" void MobileInput_SetAxis_m87 (MobileInput_t14 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (L_2)
		{
			goto IL_0018;
		}
	}
	{
		String_t* L_3 = ___name;
		MobileInput_AddAxes_m80(__this, L_3, /*hidden argument*/NULL);
	}

IL_0018:
	{
		Dictionary_2_t25 * L_4 = (((VirtualInput_t8 *)__this)->___m_VirtualAxes_0);
		String_t* L_5 = ___name;
		NullCheck(L_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)VirtFuncInvoker1< VirtualAxis_t3 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_4, L_5);
		float L_7 = ___value;
		NullCheck(L_6);
		VirtualAxis_Update_m21(L_6, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonDown(System.String)
extern "C" bool MobileInput_GetButtonDown_m88 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t26 * L_3 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualButton_t6 * L_5 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		bool L_6 = VirtualButton_get_GetButtonDown_m34(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		String_t* L_7 = ___name;
		MobileInput_AddButton_m79(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t26 * L_8 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_9 = ___name;
		NullCheck(L_8);
		VirtualButton_t6 * L_10 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_8, L_9);
		NullCheck(L_10);
		bool L_11 = VirtualButton_get_GetButtonDown_m34(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButtonUp(System.String)
extern "C" bool MobileInput_GetButtonUp_m89 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t26 * L_3 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualButton_t6 * L_5 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		bool L_6 = VirtualButton_get_GetButtonUp_m35(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		String_t* L_7 = ___name;
		MobileInput_AddButton_m79(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t26 * L_8 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_9 = ___name;
		NullCheck(L_8);
		VirtualButton_t6 * L_10 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_8, L_9);
		NullCheck(L_10);
		bool L_11 = VirtualButton_get_GetButtonUp_m35(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::GetButton(System.String)
extern "C" bool MobileInput_GetButton_m90 (MobileInput_t14 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0023;
		}
	}
	{
		Dictionary_2_t26 * L_3 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualButton_t6 * L_5 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_3, L_4);
		NullCheck(L_5);
		bool L_6 = VirtualButton_get_GetButton_m33(L_5, /*hidden argument*/NULL);
		return L_6;
	}

IL_0023:
	{
		String_t* L_7 = ___name;
		MobileInput_AddButton_m79(__this, L_7, /*hidden argument*/NULL);
		Dictionary_2_t26 * L_8 = (((VirtualInput_t8 *)__this)->___m_VirtualButtons_1);
		String_t* L_9 = ___name;
		NullCheck(L_8);
		VirtualButton_t6 * L_10 = (VirtualButton_t6 *)VirtFuncInvoker1< VirtualButton_t6 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::get_Item(!0) */, L_8, L_9);
		NullCheck(L_10);
		bool L_11 = VirtualButton_get_GetButton_m33(L_10, /*hidden argument*/NULL);
		return L_11;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.MobileInput::MousePosition()
extern "C" Vector3_t12  MobileInput_MousePosition_m91 (MobileInput_t14 * __this, const MethodInfo* method)
{
	{
		Vector3_t12  L_0 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::.ctor()
extern "C" void StandaloneInput__ctor_m92 (StandaloneInput_t15 * __this, const MethodInfo* method)
{
	{
		VirtualInput__ctor_m118(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetAxis(System.String,System.Boolean)
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern "C" float StandaloneInput_GetAxis_m93 (StandaloneInput_t15 * __this, String_t* ___name, bool ___raw, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	float G_B3_0 = 0.0f;
	{
		bool L_0 = ___raw;
		if (!L_0)
		{
			goto IL_0011;
		}
	}
	{
		String_t* L_1 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		float L_2 = Input_GetAxisRaw_m239(NULL /*static, unused*/, L_1, /*hidden argument*/NULL);
		G_B3_0 = L_2;
		goto IL_0017;
	}

IL_0011:
	{
		String_t* L_3 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		float L_4 = Input_GetAxis_m240(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		G_B3_0 = L_4;
	}

IL_0017:
	{
		return G_B3_0;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButton(System.String)
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern "C" bool StandaloneInput_GetButton_m94 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButton_m241(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonDown(System.String)
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern "C" bool StandaloneInput_GetButtonDown_m95 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonDown_m242(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::GetButtonUp(System.String)
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern "C" bool StandaloneInput_GetButtonUp_m96 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = ___name;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_1 = Input_GetButtonUp_m243(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		return L_1;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonDown(System.String)
extern TypeInfo* Exception_t68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void StandaloneInput_SetButtonDown_m97 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t68 * L_0 = (Exception_t68 *)il2cpp_codegen_object_new (Exception_t68_il2cpp_TypeInfo_var);
		Exception__ctor_m244(L_0, _stringLiteral4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetButtonUp(System.String)
extern TypeInfo* Exception_t68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void StandaloneInput_SetButtonUp_m98 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t68 * L_0 = (Exception_t68 *)il2cpp_codegen_object_new (Exception_t68_il2cpp_TypeInfo_var);
		Exception__ctor_m244(L_0, _stringLiteral4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisPositive(System.String)
extern TypeInfo* Exception_t68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void StandaloneInput_SetAxisPositive_m99 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t68 * L_0 = (Exception_t68 *)il2cpp_codegen_object_new (Exception_t68_il2cpp_TypeInfo_var);
		Exception__ctor_m244(L_0, _stringLiteral4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisNegative(System.String)
extern TypeInfo* Exception_t68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void StandaloneInput_SetAxisNegative_m100 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t68 * L_0 = (Exception_t68 *)il2cpp_codegen_object_new (Exception_t68_il2cpp_TypeInfo_var);
		Exception__ctor_m244(L_0, _stringLiteral4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxisZero(System.String)
extern TypeInfo* Exception_t68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void StandaloneInput_SetAxisZero_m101 (StandaloneInput_t15 * __this, String_t* ___name, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t68 * L_0 = (Exception_t68 *)il2cpp_codegen_object_new (Exception_t68_il2cpp_TypeInfo_var);
		Exception__ctor_m244(L_0, _stringLiteral4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::SetAxis(System.String,System.Single)
extern TypeInfo* Exception_t68_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral4;
extern "C" void StandaloneInput_SetAxis_m102 (StandaloneInput_t15 * __this, String_t* ___name, float ___value, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Exception_t68_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(20);
		_stringLiteral4 = il2cpp_codegen_string_literal_from_index(4);
		s_Il2CppMethodIntialized = true;
	}
	{
		Exception_t68 * L_0 = (Exception_t68 *)il2cpp_codegen_object_new (Exception_t68_il2cpp_TypeInfo_var);
		Exception__ctor_m244(L_0, _stringLiteral4, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.PlatformSpecific.StandaloneInput::MousePosition()
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern "C" Vector3_t12  StandaloneInput_MousePosition_m103 (StandaloneInput_t15 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		Vector3_t12  L_0 = Input_get_mousePosition_m245(NULL /*static, unused*/, /*hidden argument*/NULL);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput/AxisMapping::.ctor()
extern "C" void AxisMapping__ctor_m104 (AxisMapping_t18 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::.ctor()
extern "C" void TiltInput__ctor_m105 (TiltInput_t19 * __this, const MethodInfo* method)
{
	{
		__this->___fullTiltAngle_4 = (25.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnEnable()
extern TypeInfo* VirtualAxis_t3_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void TiltInput_OnEnable_m106 (TiltInput_t19 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		AxisMapping_t18 * L_0 = (__this->___mapping_2);
		NullCheck(L_0);
		int32_t L_1 = (L_0->___type_0);
		if (L_1)
		{
			goto IL_0031;
		}
	}
	{
		AxisMapping_t18 * L_2 = (__this->___mapping_2);
		NullCheck(L_2);
		String_t* L_3 = (L_2->___axisName_1);
		VirtualAxis_t3 * L_4 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_4, L_3, /*hidden argument*/NULL);
		__this->___m_SteerAxis_6 = L_4;
		VirtualAxis_t3 * L_5 = (__this->___m_SteerAxis_6);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0031:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::Update()
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void TiltInput_Update_m107 (TiltInput_t19 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	int32_t V_2 = {0};
	Vector3_t12  V_3 = {0};
	Vector3_t12  V_4 = {0};
	Vector3_t12  V_5 = {0};
	Vector3_t12  V_6 = {0};
	int32_t V_7 = {0};
	{
		V_0 = (0.0f);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		Vector3_t12  L_0 = Input_get_acceleration_m246(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t12  L_1 = Vector3_get_zero_m227(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_2 = Vector3_op_Inequality_m247(NULL /*static, unused*/, L_0, L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_009c;
		}
	}
	{
		int32_t L_3 = (__this->___tiltAroundAxis_3);
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_0033;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_0067;
		}
	}
	{
		goto IL_009c;
	}

IL_0033:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		Vector3_t12  L_6 = Input_get_acceleration_m246(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_3 = L_6;
		float L_7 = ((&V_3)->___x_1);
		Vector3_t12  L_8 = Input_get_acceleration_m246(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_4 = L_8;
		float L_9 = ((&V_4)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_10 = atan2f(L_7, ((-L_9)));
		float L_11 = (__this->___centreAngleOffset_5);
		V_0 = ((float)((float)((float)((float)L_10*(float)(57.29578f)))+(float)L_11));
		goto IL_009c;
	}

IL_0067:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		Vector3_t12  L_12 = Input_get_acceleration_m246(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_5 = L_12;
		float L_13 = ((&V_5)->___z_3);
		Vector3_t12  L_14 = Input_get_acceleration_m246(NULL /*static, unused*/, /*hidden argument*/NULL);
		V_6 = L_14;
		float L_15 = ((&V_6)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_16 = atan2f(L_13, ((-L_15)));
		float L_17 = (__this->___centreAngleOffset_5);
		V_0 = ((float)((float)((float)((float)L_16*(float)(57.29578f)))+(float)L_17));
		goto IL_009c;
	}

IL_009c:
	{
		float L_18 = (__this->___fullTiltAngle_4);
		float L_19 = (__this->___fullTiltAngle_4);
		float L_20 = V_0;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_21 = Mathf_InverseLerp_m248(NULL /*static, unused*/, ((-L_18)), L_19, L_20, /*hidden argument*/NULL);
		V_1 = ((float)((float)((float)((float)L_21*(float)(2.0f)))-(float)(1.0f)));
		AxisMapping_t18 * L_22 = (__this->___mapping_2);
		NullCheck(L_22);
		int32_t L_23 = (L_22->___type_0);
		V_7 = L_23;
		int32_t L_24 = V_7;
		if (L_24 == 0)
		{
			goto IL_00e5;
		}
		if (L_24 == 1)
		{
			goto IL_00f6;
		}
		if (L_24 == 2)
		{
			goto IL_0108;
		}
		if (L_24 == 3)
		{
			goto IL_011a;
		}
	}
	{
		goto IL_012c;
	}

IL_00e5:
	{
		VirtualAxis_t3 * L_25 = (__this->___m_SteerAxis_6);
		float L_26 = V_1;
		NullCheck(L_25);
		VirtualAxis_Update_m21(L_25, L_26, /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_00f6:
	{
		float L_27 = V_1;
		int32_t L_28 = Screen_get_width_m249(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetVirtualMousePositionX_m58(NULL /*static, unused*/, ((float)((float)L_27*(float)(((float)((float)L_28))))), /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_0108:
	{
		float L_29 = V_1;
		int32_t L_30 = Screen_get_width_m249(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetVirtualMousePositionY_m59(NULL /*static, unused*/, ((float)((float)L_29*(float)(((float)((float)L_30))))), /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_011a:
	{
		float L_31 = V_1;
		int32_t L_32 = Screen_get_width_m249(NULL /*static, unused*/, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_SetVirtualMousePositionZ_m60(NULL /*static, unused*/, ((float)((float)L_31*(float)(((float)((float)L_32))))), /*hidden argument*/NULL);
		goto IL_012c;
	}

IL_012c:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TiltInput::OnDisable()
extern "C" void TiltInput_OnDisable_m108 (TiltInput_t19 * __this, const MethodInfo* method)
{
	{
		VirtualAxis_t3 * L_0 = (__this->___m_SteerAxis_6);
		NullCheck(L_0);
		VirtualAxis_Remove_m20(L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::.ctor()
extern Il2CppCodeGenString* _stringLiteral0;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void TouchPad__ctor_m109 (TouchPad_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___horizontalAxisName_4 = _stringLiteral0;
		__this->___verticalAxisName_5 = _stringLiteral2;
		__this->___Xsensitivity_6 = (1.0f);
		__this->___Ysensitivity_7 = (1.0f);
		__this->___m_Id_16 = (-1);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnEnable()
extern "C" void TouchPad_OnEnable_m110 (TouchPad_t22 * __this, const MethodInfo* method)
{
	{
		TouchPad_CreateVirtualAxes_m112(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Start()
extern const MethodInfo* Component_GetComponent_TisImage_t24_m250_MethodInfo_var;
extern "C" void TouchPad_Start_m111 (TouchPad_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisImage_t24_m250_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483653);
		s_Il2CppMethodIntialized = true;
	}
	{
		Image_t24 * L_0 = Component_GetComponent_TisImage_t24_m250(__this, /*hidden argument*/Component_GetComponent_TisImage_t24_m250_MethodInfo_var);
		__this->___m_Image_19 = L_0;
		Image_t24 * L_1 = (__this->___m_Image_19);
		NullCheck(L_1);
		Transform_t33 * L_2 = Component_get_transform_m223(L_1, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t12  L_3 = Transform_get_position_m224(L_2, /*hidden argument*/NULL);
		__this->___m_Center_18 = L_3;
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::CreateVirtualAxes()
extern TypeInfo* VirtualAxis_t3_il2cpp_TypeInfo_var;
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void TouchPad_CreateVirtualAxes_m112 (TouchPad_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		VirtualAxis_t3_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(2);
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	TouchPad_t22 * G_B2_0 = {0};
	TouchPad_t22 * G_B1_0 = {0};
	int32_t G_B3_0 = 0;
	TouchPad_t22 * G_B3_1 = {0};
	TouchPad_t22 * G_B5_0 = {0};
	TouchPad_t22 * G_B4_0 = {0};
	int32_t G_B6_0 = 0;
	TouchPad_t22 * G_B6_1 = {0};
	{
		int32_t L_0 = (__this->___axesToUse_2);
		G_B1_0 = __this;
		if (!L_0)
		{
			G_B2_0 = __this;
			goto IL_0017;
		}
	}
	{
		int32_t L_1 = (__this->___axesToUse_2);
		G_B3_0 = ((((int32_t)L_1) == ((int32_t)1))? 1 : 0);
		G_B3_1 = G_B1_0;
		goto IL_0018;
	}

IL_0017:
	{
		G_B3_0 = 1;
		G_B3_1 = G_B2_0;
	}

IL_0018:
	{
		NullCheck(G_B3_1);
		G_B3_1->___m_UseX_11 = G_B3_0;
		int32_t L_2 = (__this->___axesToUse_2);
		G_B4_0 = __this;
		if (!L_2)
		{
			G_B5_0 = __this;
			goto IL_0034;
		}
	}
	{
		int32_t L_3 = (__this->___axesToUse_2);
		G_B6_0 = ((((int32_t)L_3) == ((int32_t)2))? 1 : 0);
		G_B6_1 = G_B4_0;
		goto IL_0035;
	}

IL_0034:
	{
		G_B6_0 = 1;
		G_B6_1 = G_B5_0;
	}

IL_0035:
	{
		NullCheck(G_B6_1);
		G_B6_1->___m_UseY_12 = G_B6_0;
		bool L_4 = (__this->___m_UseX_11);
		if (!L_4)
		{
			goto IL_0061;
		}
	}
	{
		String_t* L_5 = (__this->___horizontalAxisName_4);
		VirtualAxis_t3 * L_6 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_6, L_5, /*hidden argument*/NULL);
		__this->___m_HorizontalVirtualAxis_13 = L_6;
		VirtualAxis_t3 * L_7 = (__this->___m_HorizontalVirtualAxis_13);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
	}

IL_0061:
	{
		bool L_8 = (__this->___m_UseY_12);
		if (!L_8)
		{
			goto IL_0088;
		}
	}
	{
		String_t* L_9 = (__this->___verticalAxisName_5);
		VirtualAxis_t3 * L_10 = (VirtualAxis_t3 *)il2cpp_codegen_object_new (VirtualAxis_t3_il2cpp_TypeInfo_var);
		VirtualAxis__ctor_m14(L_10, L_9, /*hidden argument*/NULL);
		__this->___m_VerticalVirtualAxis_14 = L_10;
		VirtualAxis_t3 * L_11 = (__this->___m_VerticalVirtualAxis_14);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_RegisterVirtualAxis_m40(NULL /*static, unused*/, L_11, /*hidden argument*/NULL);
	}

IL_0088:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::UpdateVirtualAxes(UnityEngine.Vector3)
extern "C" void TouchPad_UpdateVirtualAxes_m113 (TouchPad_t22 * __this, Vector3_t12  ___value, const MethodInfo* method)
{
	{
		Vector3_t12  L_0 = Vector3_get_normalized_m251((&___value), /*hidden argument*/NULL);
		___value = L_0;
		bool L_1 = (__this->___m_UseX_11);
		if (!L_1)
		{
			goto IL_0026;
		}
	}
	{
		VirtualAxis_t3 * L_2 = (__this->___m_HorizontalVirtualAxis_13);
		float L_3 = ((&___value)->___x_1);
		NullCheck(L_2);
		VirtualAxis_Update_m21(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0026:
	{
		bool L_4 = (__this->___m_UseY_12);
		if (!L_4)
		{
			goto IL_0043;
		}
	}
	{
		VirtualAxis_t3 * L_5 = (__this->___m_VerticalVirtualAxis_14);
		float L_6 = ((&___value)->___y_2);
		NullCheck(L_5);
		VirtualAxis_Update_m21(L_5, L_6, /*hidden argument*/NULL);
	}

IL_0043:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern "C" void TouchPad_OnPointerDown_m114 (TouchPad_t22 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	{
		__this->___m_Dragging_15 = 1;
		PointerEventData_t57 * L_0 = ___data;
		NullCheck(L_0);
		int32_t L_1 = PointerEventData_get_pointerId_m252(L_0, /*hidden argument*/NULL);
		__this->___m_Id_16 = L_1;
		int32_t L_2 = (__this->___controlStyle_3);
		if (!L_2)
		{
			goto IL_002f;
		}
	}
	{
		PointerEventData_t57 * L_3 = ___data;
		NullCheck(L_3);
		Vector2_t23  L_4 = PointerEventData_get_position_m228(L_3, /*hidden argument*/NULL);
		Vector3_t12  L_5 = Vector2_op_Implicit_m253(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		__this->___m_Center_18 = L_5;
	}

IL_002f:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::Update()
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern "C" void TouchPad_Update_m115 (TouchPad_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	Vector2_t23  V_0 = {0};
	Vector2_t23  V_1 = {0};
	Vector2_t23  V_2 = {0};
	Vector2_t23  V_3 = {0};
	{
		bool L_0 = (__this->___m_Dragging_15);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		int32_t L_1 = Input_get_touchCount_m254(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_2 = (__this->___m_Id_16);
		if ((((int32_t)L_1) < ((int32_t)((int32_t)((int32_t)L_2+(int32_t)1)))))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_3 = (__this->___m_Id_16);
		if ((((int32_t)L_3) == ((int32_t)(-1))))
		{
			goto IL_0109;
		}
	}
	{
		int32_t L_4 = (__this->___controlStyle_3);
		if ((!(((uint32_t)L_4) == ((uint32_t)2))))
		{
			goto IL_0062;
		}
	}
	{
		Vector2_t23  L_5 = (__this->___m_PreviousTouchPos_17);
		Vector3_t12  L_6 = Vector2_op_Implicit_m253(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
		__this->___m_Center_18 = L_6;
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		TouchU5BU5D_t71* L_7 = Input_get_touches_m255(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_8 = (__this->___m_Id_16);
		NullCheck(L_7);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_7, L_8);
		Vector2_t23  L_9 = Touch_get_position_m256(((Touch_t72 *)(Touch_t72 *)SZArrayLdElema(L_7, L_8, sizeof(Touch_t72 ))), /*hidden argument*/NULL);
		__this->___m_PreviousTouchPos_17 = L_9;
	}

IL_0062:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		TouchU5BU5D_t71* L_10 = Input_get_touches_m255(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_11 = (__this->___m_Id_16);
		NullCheck(L_10);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_10, L_11);
		Vector2_t23  L_12 = Touch_get_position_m256(((Touch_t72 *)(Touch_t72 *)SZArrayLdElema(L_10, L_11, sizeof(Touch_t72 ))), /*hidden argument*/NULL);
		V_2 = L_12;
		float L_13 = ((&V_2)->___x_1);
		Vector3_t12 * L_14 = &(__this->___m_Center_18);
		float L_15 = (L_14->___x_1);
		TouchU5BU5D_t71* L_16 = Input_get_touches_m255(NULL /*static, unused*/, /*hidden argument*/NULL);
		int32_t L_17 = (__this->___m_Id_16);
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		Vector2_t23  L_18 = Touch_get_position_m256(((Touch_t72 *)(Touch_t72 *)SZArrayLdElema(L_16, L_17, sizeof(Touch_t72 ))), /*hidden argument*/NULL);
		V_3 = L_18;
		float L_19 = ((&V_3)->___y_2);
		Vector3_t12 * L_20 = &(__this->___m_Center_18);
		float L_21 = (L_20->___y_2);
		Vector2__ctor_m257((&V_1), ((float)((float)L_13-(float)L_15)), ((float)((float)L_19-(float)L_21)), /*hidden argument*/NULL);
		Vector2_t23  L_22 = Vector2_get_normalized_m258((&V_1), /*hidden argument*/NULL);
		V_0 = L_22;
		Vector2_t23 * L_23 = (&V_0);
		float L_24 = (L_23->___x_1);
		float L_25 = (__this->___Xsensitivity_6);
		L_23->___x_1 = ((float)((float)L_24*(float)L_25));
		Vector2_t23 * L_26 = (&V_0);
		float L_27 = (L_26->___y_2);
		float L_28 = (__this->___Ysensitivity_7);
		L_26->___y_2 = ((float)((float)L_27*(float)L_28));
		float L_29 = ((&V_0)->___x_1);
		float L_30 = ((&V_0)->___y_2);
		Vector3_t12  L_31 = {0};
		Vector3__ctor_m230(&L_31, L_29, L_30, (0.0f), /*hidden argument*/NULL);
		TouchPad_UpdateVirtualAxes_m113(__this, L_31, /*hidden argument*/NULL);
	}

IL_0109:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern "C" void TouchPad_OnPointerUp_m116 (TouchPad_t22 * __this, PointerEventData_t57 * ___data, const MethodInfo* method)
{
	{
		__this->___m_Dragging_15 = 0;
		__this->___m_Id_16 = (-1);
		Vector3_t12  L_0 = Vector3_get_zero_m227(NULL /*static, unused*/, /*hidden argument*/NULL);
		TouchPad_UpdateVirtualAxes_m113(__this, L_0, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.TouchPad::OnDisable()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern "C" void TouchPad_OnDisable_m117 (TouchPad_t22 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		s_Il2CppMethodIntialized = true;
	}
	{
		String_t* L_0 = (__this->___horizontalAxisName_4);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		bool L_1 = CrossPlatformInputManager_AxisExists_m38(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_001b;
		}
	}
	{
		String_t* L_2 = (__this->___horizontalAxisName_4);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualAxis_m42(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
	}

IL_001b:
	{
		String_t* L_3 = (__this->___verticalAxisName_5);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		bool L_4 = CrossPlatformInputManager_AxisExists_m38(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		String_t* L_5 = (__this->___verticalAxisName_5);
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		CrossPlatformInputManager_UnRegisterVirtualAxis_m42(NULL /*static, unused*/, L_5, /*hidden argument*/NULL);
	}

IL_0036:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::.ctor()
extern TypeInfo* Dictionary_2_t25_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t26_il2cpp_TypeInfo_var;
extern TypeInfo* List_1_t27_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m259_MethodInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m260_MethodInfo_var;
extern const MethodInfo* List_1__ctor_m261_MethodInfo_var;
extern "C" void VirtualInput__ctor_m118 (VirtualInput_t8 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Dictionary_2_t25_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(22);
		Dictionary_2_t26_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(23);
		List_1_t27_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(24);
		Dictionary_2__ctor_m259_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483654);
		Dictionary_2__ctor_m260_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483655);
		List_1__ctor_m261_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483656);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t25 * L_0 = (Dictionary_2_t25 *)il2cpp_codegen_object_new (Dictionary_2_t25_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m259(L_0, /*hidden argument*/Dictionary_2__ctor_m259_MethodInfo_var);
		__this->___m_VirtualAxes_0 = L_0;
		Dictionary_2_t26 * L_1 = (Dictionary_2_t26 *)il2cpp_codegen_object_new (Dictionary_2_t26_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m260(L_1, /*hidden argument*/Dictionary_2__ctor_m260_MethodInfo_var);
		__this->___m_VirtualButtons_1 = L_1;
		List_1_t27 * L_2 = (List_1_t27 *)il2cpp_codegen_object_new (List_1_t27_il2cpp_TypeInfo_var);
		List_1__ctor_m261(L_2, /*hidden argument*/List_1__ctor_m261_MethodInfo_var);
		__this->___m_AlwaysUseVirtual_2 = L_2;
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		return;
	}
}
// UnityEngine.Vector3 UnityStandardAssets.CrossPlatformInput.VirtualInput::get_virtualMousePosition()
extern "C" Vector3_t12  VirtualInput_get_virtualMousePosition_m119 (VirtualInput_t8 * __this, const MethodInfo* method)
{
	{
		Vector3_t12  L_0 = (__this->___U3CvirtualMousePositionU3Ek__BackingField_3);
		return L_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::set_virtualMousePosition(UnityEngine.Vector3)
extern "C" void VirtualInput_set_virtualMousePosition_m120 (VirtualInput_t8 * __this, Vector3_t12  ___value, const MethodInfo* method)
{
	{
		Vector3_t12  L_0 = ___value;
		__this->___U3CvirtualMousePositionU3Ek__BackingField_3 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::AxisExists(System.String)
extern "C" bool VirtualInput_AxisExists_m121 (VirtualInput_t8 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (__this->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Boolean UnityStandardAssets.CrossPlatformInput.VirtualInput::ButtonExists(System.String)
extern "C" bool VirtualInput_ButtonExists_m122 (VirtualInput_t8 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (__this->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		return L_2;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualAxis(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral5;
extern Il2CppCodeGenString* _stringLiteral6;
extern "C" void VirtualInput_RegisterVirtualAxis_m123 (VirtualInput_t8 * __this, VirtualAxis_t3 * ___axis, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral5 = il2cpp_codegen_string_literal_from_index(5);
		_stringLiteral6 = il2cpp_codegen_string_literal_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t25 * L_0 = (__this->___m_VirtualAxes_0);
		VirtualAxis_t3 * L_1 = ___axis;
		NullCheck(L_1);
		String_t* L_2 = VirtualAxis_get_name_m16(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_2);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		VirtualAxis_t3 * L_4 = ___axis;
		NullCheck(L_4);
		String_t* L_5 = VirtualAxis_get_name_m16(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m262(NULL /*static, unused*/, _stringLiteral5, L_5, _stringLiteral6, /*hidden argument*/NULL);
		Debug_LogError_m263(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0035:
	{
		Dictionary_2_t25 * L_7 = (__this->___m_VirtualAxes_0);
		VirtualAxis_t3 * L_8 = ___axis;
		NullCheck(L_8);
		String_t* L_9 = VirtualAxis_get_name_m16(L_8, /*hidden argument*/NULL);
		VirtualAxis_t3 * L_10 = ___axis;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, VirtualAxis_t3 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::Add(!0,!1) */, L_7, L_9, L_10);
		VirtualAxis_t3 * L_11 = ___axis;
		NullCheck(L_11);
		bool L_12 = VirtualAxis_get_matchWithInputManager_m18(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0063;
		}
	}
	{
		List_1_t27 * L_13 = (__this->___m_AlwaysUseVirtual_2);
		VirtualAxis_t3 * L_14 = ___axis;
		NullCheck(L_14);
		String_t* L_15 = VirtualAxis_get_name_m16(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_13, L_15);
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::RegisterVirtualButton(UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton)
extern TypeInfo* String_t_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral7;
extern Il2CppCodeGenString* _stringLiteral6;
extern "C" void VirtualInput_RegisterVirtualButton_m124 (VirtualInput_t8 * __this, VirtualButton_t6 * ___button, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		String_t_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(6);
		_stringLiteral7 = il2cpp_codegen_string_literal_from_index(7);
		_stringLiteral6 = il2cpp_codegen_string_literal_from_index(6);
		s_Il2CppMethodIntialized = true;
	}
	{
		Dictionary_2_t26 * L_0 = (__this->___m_VirtualButtons_1);
		VirtualButton_t6 * L_1 = ___button;
		NullCheck(L_1);
		String_t* L_2 = VirtualButton_get_name_m26(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		bool L_3 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_2);
		if (!L_3)
		{
			goto IL_0035;
		}
	}
	{
		VirtualButton_t6 * L_4 = ___button;
		NullCheck(L_4);
		String_t* L_5 = VirtualButton_get_name_m26(L_4, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(String_t_il2cpp_TypeInfo_var);
		String_t* L_6 = String_Concat_m262(NULL /*static, unused*/, _stringLiteral7, L_5, _stringLiteral6, /*hidden argument*/NULL);
		Debug_LogError_m263(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		goto IL_0063;
	}

IL_0035:
	{
		Dictionary_2_t26 * L_7 = (__this->___m_VirtualButtons_1);
		VirtualButton_t6 * L_8 = ___button;
		NullCheck(L_8);
		String_t* L_9 = VirtualButton_get_name_m26(L_8, /*hidden argument*/NULL);
		VirtualButton_t6 * L_10 = ___button;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, VirtualButton_t6 * >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::Add(!0,!1) */, L_7, L_9, L_10);
		VirtualButton_t6 * L_11 = ___button;
		NullCheck(L_11);
		bool L_12 = VirtualButton_get_matchWithInputManager_m28(L_11, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_0063;
		}
	}
	{
		List_1_t27 * L_13 = (__this->___m_AlwaysUseVirtual_2);
		VirtualButton_t6 * L_14 = ___button;
		NullCheck(L_14);
		String_t* L_15 = VirtualButton_get_name_m26(L_14, /*hidden argument*/NULL);
		NullCheck(L_13);
		VirtActionInvoker1< String_t* >::Invoke(22 /* System.Void System.Collections.Generic.List`1<System.String>::Add(!0) */, L_13, L_15);
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualAxis(System.String)
extern "C" void VirtualInput_UnRegisterVirtualAxis_m125 (VirtualInput_t8 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t25 * L_0 = (__this->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t25 * L_3 = (__this->___m_VirtualAxes_0);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::UnRegisterVirtualButton(System.String)
extern "C" void VirtualInput_UnRegisterVirtualButton_m126 (VirtualInput_t8 * __this, String_t* ___name, const MethodInfo* method)
{
	{
		Dictionary_2_t26 * L_0 = (__this->___m_VirtualButtons_1);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_001e;
		}
	}
	{
		Dictionary_2_t26 * L_3 = (__this->___m_VirtualButtons_1);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtFuncInvoker1< bool, String_t* >::Invoke(31 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton>::Remove(!0) */, L_3, L_4);
	}

IL_001e:
	{
		return;
	}
}
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis UnityStandardAssets.CrossPlatformInput.VirtualInput::VirtualAxisReference(System.String)
extern "C" VirtualAxis_t3 * VirtualInput_VirtualAxisReference_m127 (VirtualInput_t8 * __this, String_t* ___name, const MethodInfo* method)
{
	VirtualAxis_t3 * G_B3_0 = {0};
	{
		Dictionary_2_t25 * L_0 = (__this->___m_VirtualAxes_0);
		String_t* L_1 = ___name;
		NullCheck(L_0);
		bool L_2 = (bool)VirtFuncInvoker1< bool, String_t* >::Invoke(28 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ContainsKey(!0) */, L_0, L_1);
		if (!L_2)
		{
			goto IL_0022;
		}
	}
	{
		Dictionary_2_t25 * L_3 = (__this->___m_VirtualAxes_0);
		String_t* L_4 = ___name;
		NullCheck(L_3);
		VirtualAxis_t3 * L_5 = (VirtualAxis_t3 *)VirtFuncInvoker1< VirtualAxis_t3 *, String_t* >::Invoke(25 /* !1 System.Collections.Generic.Dictionary`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Item(!0) */, L_3, L_4);
		G_B3_0 = L_5;
		goto IL_0023;
	}

IL_0022:
	{
		G_B3_0 = ((VirtualAxis_t3 *)(NULL));
	}

IL_0023:
	{
		return G_B3_0;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionX(System.Single)
extern "C" void VirtualInput_SetVirtualMousePositionX_m128 (VirtualInput_t8 * __this, float ___f, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	Vector3_t12  V_1 = {0};
	{
		float L_0 = ___f;
		Vector3_t12  L_1 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___y_2);
		Vector3_t12  L_3 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = ((&V_1)->___z_3);
		Vector3_t12  L_5 = {0};
		Vector3__ctor_m230(&L_5, L_0, L_2, L_4, /*hidden argument*/NULL);
		VirtualInput_set_virtualMousePosition_m120(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionY(System.Single)
extern "C" void VirtualInput_SetVirtualMousePositionY_m129 (VirtualInput_t8 * __this, float ___f, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	Vector3_t12  V_1 = {0};
	{
		Vector3_t12  L_0 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		float L_2 = ___f;
		Vector3_t12  L_3 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		V_1 = L_3;
		float L_4 = ((&V_1)->___z_3);
		Vector3_t12  L_5 = {0};
		Vector3__ctor_m230(&L_5, L_1, L_2, L_4, /*hidden argument*/NULL);
		VirtualInput_set_virtualMousePosition_m120(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.CrossPlatformInput.VirtualInput::SetVirtualMousePositionZ(System.Single)
extern "C" void VirtualInput_SetVirtualMousePositionZ_m130 (VirtualInput_t8 * __this, float ___f, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	Vector3_t12  V_1 = {0};
	{
		Vector3_t12  L_0 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = ((&V_0)->___x_1);
		Vector3_t12  L_2 = VirtualInput_get_virtualMousePosition_m119(__this, /*hidden argument*/NULL);
		V_1 = L_2;
		float L_3 = ((&V_1)->___y_2);
		float L_4 = ___f;
		Vector3_t12  L_5 = {0};
		Vector3__ctor_m230(&L_5, L_1, L_3, L_4, /*hidden argument*/NULL);
		VirtualInput_set_virtualMousePosition_m120(__this, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::.ctor()
extern "C" void BrakeLight__ctor_m131 (BrakeLight_t28 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Start()
extern const MethodInfo* Component_GetComponent_TisRenderer_t30_m264_MethodInfo_var;
extern "C" void BrakeLight_Start_m132 (BrakeLight_t28 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRenderer_t30_m264_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483657);
		s_Il2CppMethodIntialized = true;
	}
	{
		Renderer_t30 * L_0 = Component_GetComponent_TisRenderer_t30_m264(__this, /*hidden argument*/Component_GetComponent_TisRenderer_t30_m264_MethodInfo_var);
		__this->___m_Renderer_3 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.BrakeLight::Update()
extern "C" void BrakeLight_Update_m133 (BrakeLight_t28 * __this, const MethodInfo* method)
{
	{
		Renderer_t30 * L_0 = (__this->___m_Renderer_3);
		CarController_t29 * L_1 = (__this->___car_2);
		NullCheck(L_1);
		float L_2 = CarController_get_BrakeInput_m149(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Renderer_set_enabled_m265(L_0, ((((float)L_2) > ((float)(0.0f)))? 1 : 0), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::.ctor()
extern "C" void CarAIControl__ctor_m134 (CarAIControl_t32 * __this, const MethodInfo* method)
{
	{
		__this->___m_CautiousSpeedFactor_2 = (0.05f);
		__this->___m_CautiousMaxAngle_3 = (50.0f);
		__this->___m_CautiousMaxDistance_4 = (100.0f);
		__this->___m_CautiousAngularVelocityFactor_5 = (30.0f);
		__this->___m_SteerSensitivity_6 = (0.05f);
		__this->___m_AccelSensitivity_7 = (0.04f);
		__this->___m_BrakeSensitivity_8 = (1.0f);
		__this->___m_LateralWanderDistance_9 = (3.0f);
		__this->___m_LateralWanderSpeed_10 = (0.1f);
		__this->___m_AccelWanderAmount_11 = (0.1f);
		__this->___m_AccelWanderSpeed_12 = (0.1f);
		__this->___m_BrakeCondition_13 = 2;
		__this->___m_ReachTargetThreshold_17 = (2.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::Awake()
extern const MethodInfo* Component_GetComponent_TisCarController_t29_m266_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var;
extern "C" void CarAIControl_Awake_m135 (CarAIControl_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCarController_t29_m266_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		CarController_t29 * L_0 = Component_GetComponent_TisCarController_t29_m266(__this, /*hidden argument*/Component_GetComponent_TisCarController_t29_m266_MethodInfo_var);
		__this->___m_CarController_19 = L_0;
		float L_1 = Random_get_value_m267(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_RandomPerlin_18 = ((float)((float)L_1*(float)(100.0f)));
		Rigidbody_t34 * L_2 = Component_GetComponent_TisRigidbody_t34_m268(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var);
		__this->___m_Rigidbody_23 = L_2;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::FixedUpdate()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CarAIControl_FixedUpdate_m136 (CarAIControl_t32 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t12  V_0 = {0};
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	Vector3_t12  V_5 = {0};
	float V_6 = 0.0f;
	float V_7 = 0.0f;
	float V_8 = 0.0f;
	Vector3_t12  V_9 = {0};
	float V_10 = 0.0f;
	float V_11 = 0.0f;
	Vector3_t12  V_12 = {0};
	float V_13 = 0.0f;
	float V_14 = 0.0f;
	Vector3_t12  V_15 = {0};
	int32_t V_16 = {0};
	Vector3_t12  V_17 = {0};
	Vector3_t12  V_18 = {0};
	float G_B16_0 = 0.0f;
	{
		Transform_t33 * L_0 = (__this->___m_Target_15);
		bool L_1 = Object_op_Equality_m217(NULL /*static, unused*/, L_0, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (L_1)
		{
			goto IL_001c;
		}
	}
	{
		bool L_2 = (__this->___m_Driving_14);
		if (L_2)
		{
			goto IL_0040;
		}
	}

IL_001c:
	{
		CarController_t29 * L_3 = (__this->___m_CarController_19);
		NullCheck(L_3);
		CarController_Move_m164(L_3, (0.0f), (0.0f), (-1.0f), (1.0f), /*hidden argument*/NULL);
		goto IL_0351;
	}

IL_0040:
	{
		Transform_t33 * L_4 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t12  L_5 = Transform_get_forward_m269(L_4, /*hidden argument*/NULL);
		V_0 = L_5;
		Rigidbody_t34 * L_6 = (__this->___m_Rigidbody_23);
		NullCheck(L_6);
		Vector3_t12  L_7 = Rigidbody_get_velocity_m270(L_6, /*hidden argument*/NULL);
		V_15 = L_7;
		float L_8 = Vector3_get_magnitude_m271((&V_15), /*hidden argument*/NULL);
		CarController_t29 * L_9 = (__this->___m_CarController_19);
		NullCheck(L_9);
		float L_10 = CarController_get_MaxSpeed_m153(L_9, /*hidden argument*/NULL);
		if ((!(((float)L_8) > ((float)((float)((float)L_10*(float)(0.1f)))))))
		{
			goto IL_0082;
		}
	}
	{
		Rigidbody_t34 * L_11 = (__this->___m_Rigidbody_23);
		NullCheck(L_11);
		Vector3_t12  L_12 = Rigidbody_get_velocity_m270(L_11, /*hidden argument*/NULL);
		V_0 = L_12;
	}

IL_0082:
	{
		CarController_t29 * L_13 = (__this->___m_CarController_19);
		NullCheck(L_13);
		float L_14 = CarController_get_MaxSpeed_m153(L_13, /*hidden argument*/NULL);
		V_1 = L_14;
		int32_t L_15 = (__this->___m_BrakeCondition_13);
		V_16 = L_15;
		int32_t L_16 = V_16;
		if (L_16 == 0)
		{
			goto IL_01b7;
		}
		if (L_16 == 1)
		{
			goto IL_00ae;
		}
		if (L_16 == 2)
		{
			goto IL_011f;
		}
	}
	{
		goto IL_01bc;
	}

IL_00ae:
	{
		Transform_t33 * L_17 = (__this->___m_Target_15);
		NullCheck(L_17);
		Vector3_t12  L_18 = Transform_get_forward_m269(L_17, /*hidden argument*/NULL);
		Vector3_t12  L_19 = V_0;
		float L_20 = Vector3_Angle_m272(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_2 = L_20;
		Rigidbody_t34 * L_21 = (__this->___m_Rigidbody_23);
		NullCheck(L_21);
		Vector3_t12  L_22 = Rigidbody_get_angularVelocity_m273(L_21, /*hidden argument*/NULL);
		V_17 = L_22;
		float L_23 = Vector3_get_magnitude_m271((&V_17), /*hidden argument*/NULL);
		float L_24 = (__this->___m_CautiousAngularVelocityFactor_5);
		V_3 = ((float)((float)L_23*(float)L_24));
		float L_25 = (__this->___m_CautiousMaxAngle_3);
		float L_26 = V_3;
		float L_27 = V_2;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_28 = Mathf_Max_m274(NULL /*static, unused*/, L_26, L_27, /*hidden argument*/NULL);
		float L_29 = Mathf_InverseLerp_m248(NULL /*static, unused*/, (0.0f), L_25, L_28, /*hidden argument*/NULL);
		V_4 = L_29;
		CarController_t29 * L_30 = (__this->___m_CarController_19);
		NullCheck(L_30);
		float L_31 = CarController_get_MaxSpeed_m153(L_30, /*hidden argument*/NULL);
		CarController_t29 * L_32 = (__this->___m_CarController_19);
		NullCheck(L_32);
		float L_33 = CarController_get_MaxSpeed_m153(L_32, /*hidden argument*/NULL);
		float L_34 = (__this->___m_CautiousSpeedFactor_2);
		float L_35 = V_4;
		float L_36 = Mathf_Lerp_m275(NULL /*static, unused*/, L_31, ((float)((float)L_33*(float)L_34)), L_35, /*hidden argument*/NULL);
		V_1 = L_36;
		goto IL_01bc;
	}

IL_011f:
	{
		Transform_t33 * L_37 = (__this->___m_Target_15);
		NullCheck(L_37);
		Vector3_t12  L_38 = Transform_get_position_m224(L_37, /*hidden argument*/NULL);
		Transform_t33 * L_39 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_39);
		Vector3_t12  L_40 = Transform_get_position_m224(L_39, /*hidden argument*/NULL);
		Vector3_t12  L_41 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_38, L_40, /*hidden argument*/NULL);
		V_5 = L_41;
		float L_42 = (__this->___m_CautiousMaxDistance_4);
		float L_43 = Vector3_get_magnitude_m271((&V_5), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_44 = Mathf_InverseLerp_m248(NULL /*static, unused*/, L_42, (0.0f), L_43, /*hidden argument*/NULL);
		V_6 = L_44;
		Rigidbody_t34 * L_45 = (__this->___m_Rigidbody_23);
		NullCheck(L_45);
		Vector3_t12  L_46 = Rigidbody_get_angularVelocity_m273(L_45, /*hidden argument*/NULL);
		V_18 = L_46;
		float L_47 = Vector3_get_magnitude_m271((&V_18), /*hidden argument*/NULL);
		float L_48 = (__this->___m_CautiousAngularVelocityFactor_5);
		V_7 = ((float)((float)L_47*(float)L_48));
		float L_49 = (__this->___m_CautiousMaxAngle_3);
		float L_50 = V_7;
		float L_51 = Mathf_InverseLerp_m248(NULL /*static, unused*/, (0.0f), L_49, L_50, /*hidden argument*/NULL);
		float L_52 = V_6;
		float L_53 = Mathf_Max_m274(NULL /*static, unused*/, L_51, L_52, /*hidden argument*/NULL);
		V_8 = L_53;
		CarController_t29 * L_54 = (__this->___m_CarController_19);
		NullCheck(L_54);
		float L_55 = CarController_get_MaxSpeed_m153(L_54, /*hidden argument*/NULL);
		CarController_t29 * L_56 = (__this->___m_CarController_19);
		NullCheck(L_56);
		float L_57 = CarController_get_MaxSpeed_m153(L_56, /*hidden argument*/NULL);
		float L_58 = (__this->___m_CautiousSpeedFactor_2);
		float L_59 = V_8;
		float L_60 = Mathf_Lerp_m275(NULL /*static, unused*/, L_55, ((float)((float)L_57*(float)L_58)), L_59, /*hidden argument*/NULL);
		V_1 = L_60;
		goto IL_01bc;
	}

IL_01b7:
	{
		goto IL_01bc;
	}

IL_01bc:
	{
		Transform_t33 * L_61 = (__this->___m_Target_15);
		NullCheck(L_61);
		Vector3_t12  L_62 = Transform_get_position_m224(L_61, /*hidden argument*/NULL);
		V_9 = L_62;
		float L_63 = Time_get_time_m276(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_64 = (__this->___m_AvoidOtherCarTime_20);
		if ((!(((float)L_63) < ((float)L_64))))
		{
			goto IL_0206;
		}
	}
	{
		float L_65 = V_1;
		float L_66 = (__this->___m_AvoidOtherCarSlowdown_21);
		V_1 = ((float)((float)L_65*(float)L_66));
		Vector3_t12  L_67 = V_9;
		Transform_t33 * L_68 = (__this->___m_Target_15);
		NullCheck(L_68);
		Vector3_t12  L_69 = Transform_get_right_m277(L_68, /*hidden argument*/NULL);
		float L_70 = (__this->___m_AvoidPathOffset_22);
		Vector3_t12  L_71 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_69, L_70, /*hidden argument*/NULL);
		Vector3_t12  L_72 = Vector3_op_Addition_m279(NULL /*static, unused*/, L_67, L_71, /*hidden argument*/NULL);
		V_9 = L_72;
		goto IL_024d;
	}

IL_0206:
	{
		Vector3_t12  L_73 = V_9;
		Transform_t33 * L_74 = (__this->___m_Target_15);
		NullCheck(L_74);
		Vector3_t12  L_75 = Transform_get_right_m277(L_74, /*hidden argument*/NULL);
		float L_76 = Time_get_time_m276(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_77 = (__this->___m_LateralWanderSpeed_10);
		float L_78 = (__this->___m_RandomPerlin_18);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_79 = Mathf_PerlinNoise_m280(NULL /*static, unused*/, ((float)((float)L_76*(float)L_77)), L_78, /*hidden argument*/NULL);
		Vector3_t12  L_80 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_75, ((float)((float)((float)((float)L_79*(float)(2.0f)))-(float)(1.0f))), /*hidden argument*/NULL);
		float L_81 = (__this->___m_LateralWanderDistance_9);
		Vector3_t12  L_82 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_80, L_81, /*hidden argument*/NULL);
		Vector3_t12  L_83 = Vector3_op_Addition_m279(NULL /*static, unused*/, L_73, L_82, /*hidden argument*/NULL);
		V_9 = L_83;
	}

IL_024d:
	{
		float L_84 = V_1;
		CarController_t29 * L_85 = (__this->___m_CarController_19);
		NullCheck(L_85);
		float L_86 = CarController_get_CurrentSpeed_m152(L_85, /*hidden argument*/NULL);
		if ((!(((float)L_84) < ((float)L_86))))
		{
			goto IL_0269;
		}
	}
	{
		float L_87 = (__this->___m_BrakeSensitivity_8);
		G_B16_0 = L_87;
		goto IL_026f;
	}

IL_0269:
	{
		float L_88 = (__this->___m_AccelSensitivity_7);
		G_B16_0 = L_88;
	}

IL_026f:
	{
		V_10 = G_B16_0;
		float L_89 = V_1;
		CarController_t29 * L_90 = (__this->___m_CarController_19);
		NullCheck(L_90);
		float L_91 = CarController_get_CurrentSpeed_m152(L_90, /*hidden argument*/NULL);
		float L_92 = V_10;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_93 = Mathf_Clamp_m281(NULL /*static, unused*/, ((float)((float)((float)((float)L_89-(float)L_91))*(float)L_92)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		V_11 = L_93;
		float L_94 = V_11;
		float L_95 = (__this->___m_AccelWanderAmount_11);
		float L_96 = Time_get_time_m276(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_97 = (__this->___m_AccelWanderSpeed_12);
		float L_98 = (__this->___m_RandomPerlin_18);
		float L_99 = Mathf_PerlinNoise_m280(NULL /*static, unused*/, ((float)((float)L_96*(float)L_97)), L_98, /*hidden argument*/NULL);
		float L_100 = (__this->___m_AccelWanderAmount_11);
		V_11 = ((float)((float)L_94*(float)((float)((float)((float)((float)(1.0f)-(float)L_95))+(float)((float)((float)L_99*(float)L_100))))));
		Transform_t33 * L_101 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12  L_102 = V_9;
		NullCheck(L_101);
		Vector3_t12  L_103 = Transform_InverseTransformPoint_m282(L_101, L_102, /*hidden argument*/NULL);
		V_12 = L_103;
		float L_104 = ((&V_12)->___x_1);
		float L_105 = ((&V_12)->___z_3);
		float L_106 = atan2f(L_104, L_105);
		V_13 = ((float)((float)L_106*(float)(57.29578f)));
		float L_107 = V_13;
		float L_108 = (__this->___m_SteerSensitivity_6);
		float L_109 = Mathf_Clamp_m281(NULL /*static, unused*/, ((float)((float)L_107*(float)L_108)), (-1.0f), (1.0f), /*hidden argument*/NULL);
		CarController_t29 * L_110 = (__this->___m_CarController_19);
		NullCheck(L_110);
		float L_111 = CarController_get_CurrentSpeed_m152(L_110, /*hidden argument*/NULL);
		float L_112 = Mathf_Sign_m283(NULL /*static, unused*/, L_111, /*hidden argument*/NULL);
		V_14 = ((float)((float)L_109*(float)L_112));
		CarController_t29 * L_113 = (__this->___m_CarController_19);
		float L_114 = V_14;
		float L_115 = V_11;
		float L_116 = V_11;
		NullCheck(L_113);
		CarController_Move_m164(L_113, L_114, L_115, L_116, (0.0f), /*hidden argument*/NULL);
		bool L_117 = (__this->___m_StopWhenTargetReached_16);
		if (!L_117)
		{
			goto IL_0351;
		}
	}
	{
		float L_118 = Vector3_get_magnitude_m271((&V_12), /*hidden argument*/NULL);
		float L_119 = (__this->___m_ReachTargetThreshold_17);
		if ((!(((float)L_118) < ((float)L_119))))
		{
			goto IL_0351;
		}
	}
	{
		__this->___m_Driving_14 = 0;
	}

IL_0351:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::OnCollisionStay(UnityEngine.Collision)
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisCarAIControl_t32_m285_MethodInfo_var;
extern "C" void CarAIControl_OnCollisionStay_m137 (CarAIControl_t32 * __this, Collision_t58 * ___col, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		Component_GetComponent_TisCarAIControl_t32_m285_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483660);
		s_Il2CppMethodIntialized = true;
	}
	CarAIControl_t32 * V_0 = {0};
	Vector3_t12  V_1 = {0};
	float V_2 = 0.0f;
	{
		Collision_t58 * L_0 = ___col;
		NullCheck(L_0);
		Rigidbody_t34 * L_1 = Collision_get_rigidbody_m284(L_0, /*hidden argument*/NULL);
		bool L_2 = Object_op_Inequality_m216(NULL /*static, unused*/, L_1, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_00c9;
		}
	}
	{
		Collision_t58 * L_3 = ___col;
		NullCheck(L_3);
		Rigidbody_t34 * L_4 = Collision_get_rigidbody_m284(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		CarAIControl_t32 * L_5 = Component_GetComponent_TisCarAIControl_t32_m285(L_4, /*hidden argument*/Component_GetComponent_TisCarAIControl_t32_m285_MethodInfo_var);
		V_0 = L_5;
		CarAIControl_t32 * L_6 = V_0;
		bool L_7 = Object_op_Inequality_m216(NULL /*static, unused*/, L_6, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_00c9;
		}
	}
	{
		float L_8 = Time_get_time_m276(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_AvoidOtherCarTime_20 = ((float)((float)L_8+(float)(1.0f)));
		Transform_t33 * L_9 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t12  L_10 = Transform_get_forward_m269(L_9, /*hidden argument*/NULL);
		CarAIControl_t32 * L_11 = V_0;
		NullCheck(L_11);
		Transform_t33 * L_12 = Component_get_transform_m223(L_11, /*hidden argument*/NULL);
		NullCheck(L_12);
		Vector3_t12  L_13 = Transform_get_position_m224(L_12, /*hidden argument*/NULL);
		Transform_t33 * L_14 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_14);
		Vector3_t12  L_15 = Transform_get_position_m224(L_14, /*hidden argument*/NULL);
		Vector3_t12  L_16 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_13, L_15, /*hidden argument*/NULL);
		float L_17 = Vector3_Angle_m272(NULL /*static, unused*/, L_10, L_16, /*hidden argument*/NULL);
		if ((!(((float)L_17) < ((float)(90.0f)))))
		{
			goto IL_007f;
		}
	}
	{
		__this->___m_AvoidOtherCarSlowdown_21 = (0.5f);
		goto IL_008a;
	}

IL_007f:
	{
		__this->___m_AvoidOtherCarSlowdown_21 = (1.0f);
	}

IL_008a:
	{
		Transform_t33 * L_18 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		CarAIControl_t32 * L_19 = V_0;
		NullCheck(L_19);
		Transform_t33 * L_20 = Component_get_transform_m223(L_19, /*hidden argument*/NULL);
		NullCheck(L_20);
		Vector3_t12  L_21 = Transform_get_position_m224(L_20, /*hidden argument*/NULL);
		NullCheck(L_18);
		Vector3_t12  L_22 = Transform_InverseTransformPoint_m282(L_18, L_21, /*hidden argument*/NULL);
		V_1 = L_22;
		float L_23 = ((&V_1)->___x_1);
		float L_24 = ((&V_1)->___z_3);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_25 = atan2f(L_23, L_24);
		V_2 = L_25;
		float L_26 = (__this->___m_LateralWanderDistance_9);
		float L_27 = V_2;
		float L_28 = Mathf_Sign_m283(NULL /*static, unused*/, L_27, /*hidden argument*/NULL);
		__this->___m_AvoidPathOffset_22 = ((float)((float)L_26*(float)((-L_28))));
	}

IL_00c9:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::SetTarget(UnityEngine.Transform)
extern "C" void CarAIControl_SetTarget_m138 (CarAIControl_t32 * __this, Transform_t33 * ___target, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = ___target;
		__this->___m_Target_15 = L_0;
		__this->___m_Driving_14 = 1;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAudio::.ctor()
extern "C" void CarAudio__ctor_m139 (CarAudio_t36 * __this, const MethodInfo* method)
{
	{
		__this->___engineSoundStyle_2 = 1;
		__this->___pitchMultiplier_7 = (1.0f);
		__this->___lowPitchMin_8 = (1.0f);
		__this->___lowPitchMax_9 = (6.0f);
		__this->___highPitchMultiplier_10 = (0.25f);
		__this->___maxRolloffDistance_11 = (500.0f);
		__this->___dopplerLevel_12 = (1.0f);
		__this->___useDoppler_13 = 1;
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAudio::StartSound()
extern const MethodInfo* Component_GetComponent_TisCarController_t29_m266_MethodInfo_var;
extern "C" void CarAudio_StartSound_m140 (CarAudio_t36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCarController_t29_m266_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		s_Il2CppMethodIntialized = true;
	}
	{
		CarController_t29 * L_0 = Component_GetComponent_TisCarController_t29_m266(__this, /*hidden argument*/Component_GetComponent_TisCarController_t29_m266_MethodInfo_var);
		__this->___m_CarController_19 = L_0;
		AudioClip_t37 * L_1 = (__this->___highAccelClip_5);
		AudioSource_t38 * L_2 = CarAudio_SetUpEngineAudioSource_m143(__this, L_1, /*hidden argument*/NULL);
		__this->___m_HighAccel_16 = L_2;
		int32_t L_3 = (__this->___engineSoundStyle_2);
		if ((!(((uint32_t)L_3) == ((uint32_t)1))))
		{
			goto IL_0060;
		}
	}
	{
		AudioClip_t37 * L_4 = (__this->___lowAccelClip_3);
		AudioSource_t38 * L_5 = CarAudio_SetUpEngineAudioSource_m143(__this, L_4, /*hidden argument*/NULL);
		__this->___m_LowAccel_14 = L_5;
		AudioClip_t37 * L_6 = (__this->___lowDecelClip_4);
		AudioSource_t38 * L_7 = CarAudio_SetUpEngineAudioSource_m143(__this, L_6, /*hidden argument*/NULL);
		__this->___m_LowDecel_15 = L_7;
		AudioClip_t37 * L_8 = (__this->___highDecelClip_6);
		AudioSource_t38 * L_9 = CarAudio_SetUpEngineAudioSource_m143(__this, L_8, /*hidden argument*/NULL);
		__this->___m_HighDecel_17 = L_9;
	}

IL_0060:
	{
		__this->___m_StartedSound_18 = 1;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAudio::StopSound()
extern const MethodInfo* Component_GetComponents_TisAudioSource_t38_m286_MethodInfo_var;
extern "C" void CarAudio_StopSound_m141 (CarAudio_t36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponents_TisAudioSource_t38_m286_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483661);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t38 * V_0 = {0};
	AudioSourceU5BU5D_t73* V_1 = {0};
	int32_t V_2 = 0;
	{
		AudioSourceU5BU5D_t73* L_0 = Component_GetComponents_TisAudioSource_t38_m286(__this, /*hidden argument*/Component_GetComponents_TisAudioSource_t38_m286_MethodInfo_var);
		V_1 = L_0;
		V_2 = 0;
		goto IL_001c;
	}

IL_000e:
	{
		AudioSourceU5BU5D_t73* L_1 = V_1;
		int32_t L_2 = V_2;
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, L_2);
		int32_t L_3 = L_2;
		V_0 = (*(AudioSource_t38 **)(AudioSource_t38 **)SZArrayLdElema(L_1, L_3, sizeof(AudioSource_t38 *)));
		AudioSource_t38 * L_4 = V_0;
		Object_Destroy_m287(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		int32_t L_5 = V_2;
		V_2 = ((int32_t)((int32_t)L_5+(int32_t)1));
	}

IL_001c:
	{
		int32_t L_6 = V_2;
		AudioSourceU5BU5D_t73* L_7 = V_1;
		NullCheck(L_7);
		if ((((int32_t)L_6) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_7)->max_length)))))))
		{
			goto IL_000e;
		}
	}
	{
		__this->___m_StartedSound_18 = 0;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarAudio::Update()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CarAudio_Update_m142 (CarAudio_t36 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	Vector3_t12  V_6 = {0};
	AudioSource_t38 * G_B10_0 = {0};
	AudioSource_t38 * G_B9_0 = {0};
	float G_B11_0 = 0.0f;
	AudioSource_t38 * G_B11_1 = {0};
	AudioSource_t38 * G_B14_0 = {0};
	AudioSource_t38 * G_B13_0 = {0};
	float G_B15_0 = 0.0f;
	AudioSource_t38 * G_B15_1 = {0};
	AudioSource_t38 * G_B17_0 = {0};
	AudioSource_t38 * G_B16_0 = {0};
	float G_B18_0 = 0.0f;
	AudioSource_t38 * G_B18_1 = {0};
	AudioSource_t38 * G_B20_0 = {0};
	AudioSource_t38 * G_B19_0 = {0};
	float G_B21_0 = 0.0f;
	AudioSource_t38 * G_B21_1 = {0};
	AudioSource_t38 * G_B23_0 = {0};
	AudioSource_t38 * G_B22_0 = {0};
	float G_B24_0 = 0.0f;
	AudioSource_t38 * G_B24_1 = {0};
	{
		Camera_t74 * L_0 = Camera_get_main_m288(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t33 * L_1 = Component_get_transform_m223(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t12  L_2 = Transform_get_position_m224(L_1, /*hidden argument*/NULL);
		Transform_t33 * L_3 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t12  L_4 = Transform_get_position_m224(L_3, /*hidden argument*/NULL);
		Vector3_t12  L_5 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_2, L_4, /*hidden argument*/NULL);
		V_6 = L_5;
		float L_6 = Vector3_get_sqrMagnitude_m289((&V_6), /*hidden argument*/NULL);
		V_0 = L_6;
		bool L_7 = (__this->___m_StartedSound_18);
		if (!L_7)
		{
			goto IL_004d;
		}
	}
	{
		float L_8 = V_0;
		float L_9 = (__this->___maxRolloffDistance_11);
		float L_10 = (__this->___maxRolloffDistance_11);
		if ((!(((float)L_8) > ((float)((float)((float)L_9*(float)L_10))))))
		{
			goto IL_004d;
		}
	}
	{
		CarAudio_StopSound_m141(__this, /*hidden argument*/NULL);
	}

IL_004d:
	{
		bool L_11 = (__this->___m_StartedSound_18);
		if (L_11)
		{
			goto IL_0071;
		}
	}
	{
		float L_12 = V_0;
		float L_13 = (__this->___maxRolloffDistance_11);
		float L_14 = (__this->___maxRolloffDistance_11);
		if ((!(((float)L_12) < ((float)((float)((float)L_13*(float)L_14))))))
		{
			goto IL_0071;
		}
	}
	{
		CarAudio_StartSound_m140(__this, /*hidden argument*/NULL);
	}

IL_0071:
	{
		bool L_15 = (__this->___m_StartedSound_18);
		if (!L_15)
		{
			goto IL_02d1;
		}
	}
	{
		float L_16 = (__this->___lowPitchMin_8);
		float L_17 = (__this->___lowPitchMax_9);
		CarController_t29 * L_18 = (__this->___m_CarController_19);
		NullCheck(L_18);
		float L_19 = CarController_get_Revs_m154(L_18, /*hidden argument*/NULL);
		float L_20 = CarAudio_ULerp_m144(NULL /*static, unused*/, L_16, L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		float L_21 = (__this->___lowPitchMax_9);
		float L_22 = V_1;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_23 = Mathf_Min_m290(NULL /*static, unused*/, L_21, L_22, /*hidden argument*/NULL);
		V_1 = L_23;
		int32_t L_24 = (__this->___engineSoundStyle_2);
		if (L_24)
		{
			goto IL_0106;
		}
	}
	{
		AudioSource_t38 * L_25 = (__this->___m_HighAccel_16);
		float L_26 = V_1;
		float L_27 = (__this->___pitchMultiplier_7);
		float L_28 = (__this->___highPitchMultiplier_10);
		NullCheck(L_25);
		AudioSource_set_pitch_m291(L_25, ((float)((float)((float)((float)L_26*(float)L_27))*(float)L_28)), /*hidden argument*/NULL);
		AudioSource_t38 * L_29 = (__this->___m_HighAccel_16);
		bool L_30 = (__this->___useDoppler_13);
		G_B9_0 = L_29;
		if (!L_30)
		{
			G_B10_0 = L_29;
			goto IL_00e7;
		}
	}
	{
		float L_31 = (__this->___dopplerLevel_12);
		G_B11_0 = L_31;
		G_B11_1 = G_B9_0;
		goto IL_00ec;
	}

IL_00e7:
	{
		G_B11_0 = (0.0f);
		G_B11_1 = G_B10_0;
	}

IL_00ec:
	{
		NullCheck(G_B11_1);
		AudioSource_set_dopplerLevel_m292(G_B11_1, G_B11_0, /*hidden argument*/NULL);
		AudioSource_t38 * L_32 = (__this->___m_HighAccel_16);
		NullCheck(L_32);
		AudioSource_set_volume_m293(L_32, (1.0f), /*hidden argument*/NULL);
		goto IL_02d1;
	}

IL_0106:
	{
		AudioSource_t38 * L_33 = (__this->___m_LowAccel_14);
		float L_34 = V_1;
		float L_35 = (__this->___pitchMultiplier_7);
		NullCheck(L_33);
		AudioSource_set_pitch_m291(L_33, ((float)((float)L_34*(float)L_35)), /*hidden argument*/NULL);
		AudioSource_t38 * L_36 = (__this->___m_LowDecel_15);
		float L_37 = V_1;
		float L_38 = (__this->___pitchMultiplier_7);
		NullCheck(L_36);
		AudioSource_set_pitch_m291(L_36, ((float)((float)L_37*(float)L_38)), /*hidden argument*/NULL);
		AudioSource_t38 * L_39 = (__this->___m_HighAccel_16);
		float L_40 = V_1;
		float L_41 = (__this->___highPitchMultiplier_10);
		float L_42 = (__this->___pitchMultiplier_7);
		NullCheck(L_39);
		AudioSource_set_pitch_m291(L_39, ((float)((float)((float)((float)L_40*(float)L_41))*(float)L_42)), /*hidden argument*/NULL);
		AudioSource_t38 * L_43 = (__this->___m_HighDecel_17);
		float L_44 = V_1;
		float L_45 = (__this->___highPitchMultiplier_10);
		float L_46 = (__this->___pitchMultiplier_7);
		NullCheck(L_43);
		AudioSource_set_pitch_m291(L_43, ((float)((float)((float)((float)L_44*(float)L_45))*(float)L_46)), /*hidden argument*/NULL);
		CarController_t29 * L_47 = (__this->___m_CarController_19);
		NullCheck(L_47);
		float L_48 = CarController_get_AccelInput_m156(L_47, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_49 = fabsf(L_48);
		V_2 = L_49;
		float L_50 = V_2;
		V_3 = ((float)((float)(1.0f)-(float)L_50));
		CarController_t29 * L_51 = (__this->___m_CarController_19);
		NullCheck(L_51);
		float L_52 = CarController_get_Revs_m154(L_51, /*hidden argument*/NULL);
		float L_53 = Mathf_InverseLerp_m248(NULL /*static, unused*/, (0.2f), (0.8f), L_52, /*hidden argument*/NULL);
		V_4 = L_53;
		float L_54 = V_4;
		V_5 = ((float)((float)(1.0f)-(float)L_54));
		float L_55 = V_4;
		float L_56 = V_4;
		V_4 = ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(1.0f)-(float)L_55))*(float)((float)((float)(1.0f)-(float)L_56))))));
		float L_57 = V_5;
		float L_58 = V_5;
		V_5 = ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(1.0f)-(float)L_57))*(float)((float)((float)(1.0f)-(float)L_58))))));
		float L_59 = V_2;
		float L_60 = V_2;
		V_2 = ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(1.0f)-(float)L_59))*(float)((float)((float)(1.0f)-(float)L_60))))));
		float L_61 = V_3;
		float L_62 = V_3;
		V_3 = ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(1.0f)-(float)L_61))*(float)((float)((float)(1.0f)-(float)L_62))))));
		AudioSource_t38 * L_63 = (__this->___m_LowAccel_14);
		float L_64 = V_5;
		float L_65 = V_2;
		NullCheck(L_63);
		AudioSource_set_volume_m293(L_63, ((float)((float)L_64*(float)L_65)), /*hidden argument*/NULL);
		AudioSource_t38 * L_66 = (__this->___m_LowDecel_15);
		float L_67 = V_5;
		float L_68 = V_3;
		NullCheck(L_66);
		AudioSource_set_volume_m293(L_66, ((float)((float)L_67*(float)L_68)), /*hidden argument*/NULL);
		AudioSource_t38 * L_69 = (__this->___m_HighAccel_16);
		float L_70 = V_4;
		float L_71 = V_2;
		NullCheck(L_69);
		AudioSource_set_volume_m293(L_69, ((float)((float)L_70*(float)L_71)), /*hidden argument*/NULL);
		AudioSource_t38 * L_72 = (__this->___m_HighDecel_17);
		float L_73 = V_4;
		float L_74 = V_3;
		NullCheck(L_72);
		AudioSource_set_volume_m293(L_72, ((float)((float)L_73*(float)L_74)), /*hidden argument*/NULL);
		AudioSource_t38 * L_75 = (__this->___m_HighAccel_16);
		bool L_76 = (__this->___useDoppler_13);
		G_B13_0 = L_75;
		if (!L_76)
		{
			G_B14_0 = L_75;
			goto IL_0255;
		}
	}
	{
		float L_77 = (__this->___dopplerLevel_12);
		G_B15_0 = L_77;
		G_B15_1 = G_B13_0;
		goto IL_025a;
	}

IL_0255:
	{
		G_B15_0 = (0.0f);
		G_B15_1 = G_B14_0;
	}

IL_025a:
	{
		NullCheck(G_B15_1);
		AudioSource_set_dopplerLevel_m292(G_B15_1, G_B15_0, /*hidden argument*/NULL);
		AudioSource_t38 * L_78 = (__this->___m_LowAccel_14);
		bool L_79 = (__this->___useDoppler_13);
		G_B16_0 = L_78;
		if (!L_79)
		{
			G_B17_0 = L_78;
			goto IL_027b;
		}
	}
	{
		float L_80 = (__this->___dopplerLevel_12);
		G_B18_0 = L_80;
		G_B18_1 = G_B16_0;
		goto IL_0280;
	}

IL_027b:
	{
		G_B18_0 = (0.0f);
		G_B18_1 = G_B17_0;
	}

IL_0280:
	{
		NullCheck(G_B18_1);
		AudioSource_set_dopplerLevel_m292(G_B18_1, G_B18_0, /*hidden argument*/NULL);
		AudioSource_t38 * L_81 = (__this->___m_HighDecel_17);
		bool L_82 = (__this->___useDoppler_13);
		G_B19_0 = L_81;
		if (!L_82)
		{
			G_B20_0 = L_81;
			goto IL_02a1;
		}
	}
	{
		float L_83 = (__this->___dopplerLevel_12);
		G_B21_0 = L_83;
		G_B21_1 = G_B19_0;
		goto IL_02a6;
	}

IL_02a1:
	{
		G_B21_0 = (0.0f);
		G_B21_1 = G_B20_0;
	}

IL_02a6:
	{
		NullCheck(G_B21_1);
		AudioSource_set_dopplerLevel_m292(G_B21_1, G_B21_0, /*hidden argument*/NULL);
		AudioSource_t38 * L_84 = (__this->___m_LowDecel_15);
		bool L_85 = (__this->___useDoppler_13);
		G_B22_0 = L_84;
		if (!L_85)
		{
			G_B23_0 = L_84;
			goto IL_02c7;
		}
	}
	{
		float L_86 = (__this->___dopplerLevel_12);
		G_B24_0 = L_86;
		G_B24_1 = G_B22_0;
		goto IL_02cc;
	}

IL_02c7:
	{
		G_B24_0 = (0.0f);
		G_B24_1 = G_B23_0;
	}

IL_02cc:
	{
		NullCheck(G_B24_1);
		AudioSource_set_dopplerLevel_m292(G_B24_1, G_B24_0, /*hidden argument*/NULL);
	}

IL_02d1:
	{
		return;
	}
}
// UnityEngine.AudioSource UnityStandardAssets.Vehicles.Car.CarAudio::SetUpEngineAudioSource(UnityEngine.AudioClip)
extern const MethodInfo* GameObject_AddComponent_TisAudioSource_t38_m294_MethodInfo_var;
extern "C" AudioSource_t38 * CarAudio_SetUpEngineAudioSource_m143 (CarAudio_t36 * __this, AudioClip_t37 * ___clip, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GameObject_AddComponent_TisAudioSource_t38_m294_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483662);
		s_Il2CppMethodIntialized = true;
	}
	AudioSource_t38 * V_0 = {0};
	{
		GameObject_t52 * L_0 = Component_get_gameObject_m237(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		AudioSource_t38 * L_1 = GameObject_AddComponent_TisAudioSource_t38_m294(L_0, /*hidden argument*/GameObject_AddComponent_TisAudioSource_t38_m294_MethodInfo_var);
		V_0 = L_1;
		AudioSource_t38 * L_2 = V_0;
		AudioClip_t37 * L_3 = ___clip;
		NullCheck(L_2);
		AudioSource_set_clip_m295(L_2, L_3, /*hidden argument*/NULL);
		AudioSource_t38 * L_4 = V_0;
		NullCheck(L_4);
		AudioSource_set_volume_m293(L_4, (0.0f), /*hidden argument*/NULL);
		AudioSource_t38 * L_5 = V_0;
		NullCheck(L_5);
		AudioSource_set_loop_m296(L_5, 1, /*hidden argument*/NULL);
		AudioSource_t38 * L_6 = V_0;
		AudioClip_t37 * L_7 = ___clip;
		NullCheck(L_7);
		float L_8 = AudioClip_get_length_m297(L_7, /*hidden argument*/NULL);
		float L_9 = Random_Range_m298(NULL /*static, unused*/, (0.0f), L_8, /*hidden argument*/NULL);
		NullCheck(L_6);
		AudioSource_set_time_m299(L_6, L_9, /*hidden argument*/NULL);
		AudioSource_t38 * L_10 = V_0;
		NullCheck(L_10);
		AudioSource_Play_m300(L_10, /*hidden argument*/NULL);
		AudioSource_t38 * L_11 = V_0;
		NullCheck(L_11);
		AudioSource_set_minDistance_m301(L_11, (5.0f), /*hidden argument*/NULL);
		AudioSource_t38 * L_12 = V_0;
		float L_13 = (__this->___maxRolloffDistance_11);
		NullCheck(L_12);
		AudioSource_set_maxDistance_m302(L_12, L_13, /*hidden argument*/NULL);
		AudioSource_t38 * L_14 = V_0;
		NullCheck(L_14);
		AudioSource_set_dopplerLevel_m292(L_14, (0.0f), /*hidden argument*/NULL);
		AudioSource_t38 * L_15 = V_0;
		return L_15;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarAudio::ULerp(System.Single,System.Single,System.Single)
extern "C" float CarAudio_ULerp_m144 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___from;
		float L_2 = ___value;
		float L_3 = ___to;
		return ((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_0))*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::.ctor()
extern TypeInfo* WheelColliderU5BU5D_t41_il2cpp_TypeInfo_var;
extern TypeInfo* GameObjectU5BU5D_t42_il2cpp_TypeInfo_var;
extern TypeInfo* WheelEffectsU5BU5D_t43_il2cpp_TypeInfo_var;
extern "C" void CarController__ctor_m145 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WheelColliderU5BU5D_t41_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(30);
		GameObjectU5BU5D_t42_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(32);
		WheelEffectsU5BU5D_t43_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(33);
		s_Il2CppMethodIntialized = true;
	}
	{
		__this->___m_CarDriveType_3 = 2;
		__this->___m_WheelColliders_4 = ((WheelColliderU5BU5D_t41*)SZArrayNew(WheelColliderU5BU5D_t41_il2cpp_TypeInfo_var, 4));
		__this->___m_WheelMeshes_5 = ((GameObjectU5BU5D_t42*)SZArrayNew(GameObjectU5BU5D_t42_il2cpp_TypeInfo_var, 4));
		__this->___m_WheelEffects_6 = ((WheelEffectsU5BU5D_t43*)SZArrayNew(WheelEffectsU5BU5D_t43_il2cpp_TypeInfo_var, 4));
		__this->___m_Downforce_14 = (100.0f);
		__this->___m_Topspeed_16 = (200.0f);
		__this->___m_RevRangeBoundary_18 = (1.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::.cctor()
extern TypeInfo* CarController_t29_il2cpp_TypeInfo_var;
extern "C" void CarController__cctor_m146 (Object_t * __this /* static, unused */, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CarController_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	{
		((CarController_t29_StaticFields*)CarController_t29_il2cpp_TypeInfo_var->static_fields)->___NoOfGears_17 = 5;
		return;
	}
}
// System.Boolean UnityStandardAssets.Vehicles.Car.CarController::get_Skidding()
extern "C" bool CarController_get_Skidding_m147 (CarController_t29 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CSkiddingU3Ek__BackingField_30);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Skidding(System.Boolean)
extern "C" void CarController_set_Skidding_m148 (CarController_t29 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CSkiddingU3Ek__BackingField_30 = L_0;
		return;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_BrakeInput()
extern "C" float CarController_get_BrakeInput_m149 (CarController_t29 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CBrakeInputU3Ek__BackingField_31);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_BrakeInput(System.Single)
extern "C" void CarController_set_BrakeInput_m150 (CarController_t29 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CBrakeInputU3Ek__BackingField_31 = L_0;
		return;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSteerAngle()
extern "C" float CarController_get_CurrentSteerAngle_m151 (CarController_t29 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_SteerAngle_24);
		return L_0;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSpeed()
extern "C" float CarController_get_CurrentSpeed_m152 (CarController_t29 * __this, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	{
		Rigidbody_t34 * L_0 = (__this->___m_Rigidbody_29);
		NullCheck(L_0);
		Vector3_t12  L_1 = Rigidbody_get_velocity_m270(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = Vector3_get_magnitude_m271((&V_0), /*hidden argument*/NULL);
		return ((float)((float)L_2*(float)(2.23693633f)));
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_MaxSpeed()
extern "C" float CarController_get_MaxSpeed_m153 (CarController_t29 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_Topspeed_16);
		return L_0;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_Revs()
extern "C" float CarController_get_Revs_m154 (CarController_t29 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CRevsU3Ek__BackingField_32);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Revs(System.Single)
extern "C" void CarController_set_Revs_m155 (CarController_t29 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CRevsU3Ek__BackingField_32 = L_0;
		return;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_AccelInput()
extern "C" float CarController_get_AccelInput_m156 (CarController_t29 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___U3CAccelInputU3Ek__BackingField_33);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_AccelInput(System.Single)
extern "C" void CarController_set_AccelInput_m157 (CarController_t29 * __this, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		__this->___U3CAccelInputU3Ek__BackingField_33 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::Start()
extern TypeInfo* QuaternionU5BU5D_t44_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var;
extern "C" void CarController_Start_m158 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		QuaternionU5BU5D_t44_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(35);
		Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	{
		__this->___m_WheelMeshLocalRotations_21 = ((QuaternionU5BU5D_t44*)SZArrayNew(QuaternionU5BU5D_t44_il2cpp_TypeInfo_var, 4));
		V_0 = 0;
		goto IL_0061;
	}

IL_0013:
	{
		int32_t L_0 = V_0;
		if (L_0)
		{
			goto IL_003a;
		}
	}
	{
		GameObjectU5BU5D_t42* L_1 = (__this->___m_WheelMeshes_5);
		NullCheck(L_1);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_1, 0);
		int32_t L_2 = 0;
		NullCheck((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_1, L_2, sizeof(GameObject_t52 *))));
		Transform_t33 * L_3 = GameObject_get_transform_m303((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_1, L_2, sizeof(GameObject_t52 *))), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_Rotate_m304(L_3, (0.0f), (180.0f), (0.0f), /*hidden argument*/NULL);
	}

IL_003a:
	{
		QuaternionU5BU5D_t44* L_4 = (__this->___m_WheelMeshLocalRotations_21);
		int32_t L_5 = V_0;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		GameObjectU5BU5D_t42* L_6 = (__this->___m_WheelMeshes_5);
		int32_t L_7 = V_0;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_6, L_8, sizeof(GameObject_t52 *))));
		Transform_t33 * L_9 = GameObject_get_transform_m303((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_6, L_8, sizeof(GameObject_t52 *))), /*hidden argument*/NULL);
		NullCheck(L_9);
		Quaternion_t45  L_10 = Transform_get_localRotation_m305(L_9, /*hidden argument*/NULL);
		(*(Quaternion_t45 *)((Quaternion_t45 *)(Quaternion_t45 *)SZArrayLdElema(L_4, L_5, sizeof(Quaternion_t45 )))) = L_10;
		int32_t L_11 = V_0;
		V_0 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0061:
	{
		int32_t L_12 = V_0;
		if ((((int32_t)L_12) < ((int32_t)4)))
		{
			goto IL_0013;
		}
	}
	{
		WheelColliderU5BU5D_t41* L_13 = (__this->___m_WheelColliders_4);
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, 0);
		int32_t L_14 = 0;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_13, L_14, sizeof(WheelCollider_t56 *))));
		Rigidbody_t34 * L_15 = Collider_get_attachedRigidbody_m306((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_13, L_14, sizeof(WheelCollider_t56 *))), /*hidden argument*/NULL);
		Vector3_t12  L_16 = (__this->___m_CentreOfMassOffset_7);
		NullCheck(L_15);
		Rigidbody_set_centerOfMass_m307(L_15, L_16, /*hidden argument*/NULL);
		__this->___m_MaxHandbrakeTorque_13 = (std::numeric_limits<float>::max());
		Rigidbody_t34 * L_17 = Component_GetComponent_TisRigidbody_t34_m268(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var);
		__this->___m_Rigidbody_29 = L_17;
		float L_18 = (__this->___m_FullTorqueOverAllWheels_11);
		float L_19 = (__this->___m_TractionControl_10);
		float L_20 = (__this->___m_FullTorqueOverAllWheels_11);
		__this->___m_CurrentTorque_28 = ((float)((float)L_18-(float)((float)((float)L_19*(float)L_20))));
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::GearChanging()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern TypeInfo* CarController_t29_il2cpp_TypeInfo_var;
extern "C" void CarController_GearChanging_m159 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		CarController_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		float L_0 = CarController_get_CurrentSpeed_m152(__this, /*hidden argument*/NULL);
		float L_1 = CarController_get_MaxSpeed_m153(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_2 = fabsf(((float)((float)L_0/(float)L_1)));
		V_0 = L_2;
		IL2CPP_RUNTIME_CLASS_INIT(CarController_t29_il2cpp_TypeInfo_var);
		int32_t L_3 = ((CarController_t29_StaticFields*)CarController_t29_il2cpp_TypeInfo_var->static_fields)->___NoOfGears_17;
		int32_t L_4 = (__this->___m_GearNum_25);
		V_1 = ((float)((float)((float)((float)(1.0f)/(float)(((float)((float)L_3)))))*(float)(((float)((float)((int32_t)((int32_t)L_4+(int32_t)1)))))));
		int32_t L_5 = ((CarController_t29_StaticFields*)CarController_t29_il2cpp_TypeInfo_var->static_fields)->___NoOfGears_17;
		int32_t L_6 = (__this->___m_GearNum_25);
		V_2 = ((float)((float)((float)((float)(1.0f)/(float)(((float)((float)L_5)))))*(float)(((float)((float)L_6)))));
		int32_t L_7 = (__this->___m_GearNum_25);
		if ((((int32_t)L_7) <= ((int32_t)0)))
		{
			goto IL_0060;
		}
	}
	{
		float L_8 = V_0;
		float L_9 = V_2;
		if ((!(((float)L_8) < ((float)L_9))))
		{
			goto IL_0060;
		}
	}
	{
		int32_t L_10 = (__this->___m_GearNum_25);
		__this->___m_GearNum_25 = ((int32_t)((int32_t)L_10-(int32_t)1));
	}

IL_0060:
	{
		float L_11 = V_0;
		float L_12 = V_1;
		if ((!(((float)L_11) > ((float)L_12))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_13 = (__this->___m_GearNum_25);
		IL2CPP_RUNTIME_CLASS_INIT(CarController_t29_il2cpp_TypeInfo_var);
		int32_t L_14 = ((CarController_t29_StaticFields*)CarController_t29_il2cpp_TypeInfo_var->static_fields)->___NoOfGears_17;
		if ((((int32_t)L_13) >= ((int32_t)((int32_t)((int32_t)L_14-(int32_t)1)))))
		{
			goto IL_0087;
		}
	}
	{
		int32_t L_15 = (__this->___m_GearNum_25);
		__this->___m_GearNum_25 = ((int32_t)((int32_t)L_15+(int32_t)1));
	}

IL_0087:
	{
		return;
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::CurveFactor(System.Single)
extern "C" float CarController_CurveFactor_m160 (Object_t * __this /* static, unused */, float ___factor, const MethodInfo* method)
{
	{
		float L_0 = ___factor;
		float L_1 = ___factor;
		return ((float)((float)(1.0f)-(float)((float)((float)((float)((float)(1.0f)-(float)L_0))*(float)((float)((float)(1.0f)-(float)L_1))))));
	}
}
// System.Single UnityStandardAssets.Vehicles.Car.CarController::ULerp(System.Single,System.Single,System.Single)
extern "C" float CarController_ULerp_m161 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___value, const MethodInfo* method)
{
	{
		float L_0 = ___value;
		float L_1 = ___from;
		float L_2 = ___value;
		float L_3 = ___to;
		return ((float)((float)((float)((float)((float)((float)(1.0f)-(float)L_0))*(float)L_1))+(float)((float)((float)L_2*(float)L_3))));
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateGearFactor()
extern TypeInfo* CarController_t29_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CarController_CalculateGearFactor_m162 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CarController_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CarController_t29_il2cpp_TypeInfo_var);
		int32_t L_0 = ((CarController_t29_StaticFields*)CarController_t29_il2cpp_TypeInfo_var->static_fields)->___NoOfGears_17;
		V_0 = ((float)((float)(1.0f)/(float)(((float)((float)L_0)))));
		float L_1 = V_0;
		int32_t L_2 = (__this->___m_GearNum_25);
		float L_3 = V_0;
		int32_t L_4 = (__this->___m_GearNum_25);
		float L_5 = CarController_get_CurrentSpeed_m152(__this, /*hidden argument*/NULL);
		float L_6 = CarController_get_MaxSpeed_m153(__this, /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_7 = fabsf(((float)((float)L_5/(float)L_6)));
		float L_8 = Mathf_InverseLerp_m248(NULL /*static, unused*/, ((float)((float)L_1*(float)(((float)((float)L_2))))), ((float)((float)L_3*(float)(((float)((float)((int32_t)((int32_t)L_4+(int32_t)1))))))), L_7, /*hidden argument*/NULL);
		V_1 = L_8;
		float L_9 = (__this->___m_GearFactor_26);
		float L_10 = V_1;
		float L_11 = Time_get_deltaTime_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_12 = Mathf_Lerp_m275(NULL /*static, unused*/, L_9, L_10, ((float)((float)L_11*(float)(5.0f))), /*hidden argument*/NULL);
		__this->___m_GearFactor_26 = L_12;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateRevs()
extern TypeInfo* CarController_t29_il2cpp_TypeInfo_var;
extern "C" void CarController_CalculateRevs_m163 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CarController_t29_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(26);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	{
		CarController_CalculateGearFactor_m162(__this, /*hidden argument*/NULL);
		int32_t L_0 = (__this->___m_GearNum_25);
		IL2CPP_RUNTIME_CLASS_INIT(CarController_t29_il2cpp_TypeInfo_var);
		int32_t L_1 = ((CarController_t29_StaticFields*)CarController_t29_il2cpp_TypeInfo_var->static_fields)->___NoOfGears_17;
		V_0 = ((float)((float)(((float)((float)L_0)))/(float)(((float)((float)L_1)))));
		float L_2 = (__this->___m_RevRangeBoundary_18);
		float L_3 = V_0;
		float L_4 = CarController_CurveFactor_m160(NULL /*static, unused*/, L_3, /*hidden argument*/NULL);
		float L_5 = CarController_ULerp_m161(NULL /*static, unused*/, (0.0f), L_2, L_4, /*hidden argument*/NULL);
		V_1 = L_5;
		float L_6 = (__this->___m_RevRangeBoundary_18);
		float L_7 = V_0;
		float L_8 = CarController_ULerp_m161(NULL /*static, unused*/, L_6, (1.0f), L_7, /*hidden argument*/NULL);
		V_2 = L_8;
		float L_9 = V_1;
		float L_10 = V_2;
		float L_11 = (__this->___m_GearFactor_26);
		float L_12 = CarController_ULerp_m161(NULL /*static, unused*/, L_9, L_10, L_11, /*hidden argument*/NULL);
		CarController_set_Revs_m155(__this, L_12, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::Move(System.Single,System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CarController_Move_m164 (CarController_t29 * __this, float ___steering, float ___accel, float ___footbrake, float ___handbrake, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	Quaternion_t45  V_1 = {0};
	Vector3_t12  V_2 = {0};
	float V_3 = 0.0f;
	{
		V_0 = 0;
		goto IL_0042;
	}

IL_0007:
	{
		WheelColliderU5BU5D_t41* L_0 = (__this->___m_WheelColliders_4);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_2, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetWorldPose_m308((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_2, sizeof(WheelCollider_t56 *))), (&V_2), (&V_1), /*hidden argument*/NULL);
		GameObjectU5BU5D_t42* L_3 = (__this->___m_WheelMeshes_5);
		int32_t L_4 = V_0;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_3, L_5, sizeof(GameObject_t52 *))));
		Transform_t33 * L_6 = GameObject_get_transform_m303((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_3, L_5, sizeof(GameObject_t52 *))), /*hidden argument*/NULL);
		Vector3_t12  L_7 = V_2;
		NullCheck(L_6);
		Transform_set_position_m231(L_6, L_7, /*hidden argument*/NULL);
		GameObjectU5BU5D_t42* L_8 = (__this->___m_WheelMeshes_5);
		int32_t L_9 = V_0;
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, L_9);
		int32_t L_10 = L_9;
		NullCheck((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_8, L_10, sizeof(GameObject_t52 *))));
		Transform_t33 * L_11 = GameObject_get_transform_m303((*(GameObject_t52 **)(GameObject_t52 **)SZArrayLdElema(L_8, L_10, sizeof(GameObject_t52 *))), /*hidden argument*/NULL);
		Quaternion_t45  L_12 = V_1;
		NullCheck(L_11);
		Transform_set_rotation_m309(L_11, L_12, /*hidden argument*/NULL);
		int32_t L_13 = V_0;
		V_0 = ((int32_t)((int32_t)L_13+(int32_t)1));
	}

IL_0042:
	{
		int32_t L_14 = V_0;
		if ((((int32_t)L_14) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		float L_15 = ___steering;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_16 = Mathf_Clamp_m281(NULL /*static, unused*/, L_15, (-1.0f), (1.0f), /*hidden argument*/NULL);
		___steering = L_16;
		float L_17 = ___accel;
		float L_18 = Mathf_Clamp_m281(NULL /*static, unused*/, L_17, (0.0f), (1.0f), /*hidden argument*/NULL);
		float L_19 = L_18;
		___accel = L_19;
		CarController_set_AccelInput_m157(__this, L_19, /*hidden argument*/NULL);
		float L_20 = ___footbrake;
		float L_21 = Mathf_Clamp_m281(NULL /*static, unused*/, L_20, (-1.0f), (0.0f), /*hidden argument*/NULL);
		float L_22 = ((float)((float)(-1.0f)*(float)L_21));
		___footbrake = L_22;
		CarController_set_BrakeInput_m150(__this, L_22, /*hidden argument*/NULL);
		float L_23 = ___handbrake;
		float L_24 = Mathf_Clamp_m281(NULL /*static, unused*/, L_23, (0.0f), (1.0f), /*hidden argument*/NULL);
		___handbrake = L_24;
		float L_25 = ___steering;
		float L_26 = (__this->___m_MaximumSteerAngle_8);
		__this->___m_SteerAngle_24 = ((float)((float)L_25*(float)L_26));
		WheelColliderU5BU5D_t41* L_27 = (__this->___m_WheelColliders_4);
		NullCheck(L_27);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_27, 0);
		int32_t L_28 = 0;
		float L_29 = (__this->___m_SteerAngle_24);
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_27, L_28, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_steerAngle_m310((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_27, L_28, sizeof(WheelCollider_t56 *))), L_29, /*hidden argument*/NULL);
		WheelColliderU5BU5D_t41* L_30 = (__this->___m_WheelColliders_4);
		NullCheck(L_30);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_30, 1);
		int32_t L_31 = 1;
		float L_32 = (__this->___m_SteerAngle_24);
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_30, L_31, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_steerAngle_m310((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_30, L_31, sizeof(WheelCollider_t56 *))), L_32, /*hidden argument*/NULL);
		CarController_SteerHelper_m167(__this, /*hidden argument*/NULL);
		float L_33 = ___accel;
		float L_34 = ___footbrake;
		CarController_ApplyDrive_m166(__this, L_33, L_34, /*hidden argument*/NULL);
		CarController_CapSpeed_m165(__this, /*hidden argument*/NULL);
		float L_35 = ___handbrake;
		if ((!(((float)L_35) > ((float)(0.0f)))))
		{
			goto IL_0120;
		}
	}
	{
		float L_36 = ___handbrake;
		float L_37 = (__this->___m_MaxHandbrakeTorque_13);
		V_3 = ((float)((float)L_36*(float)L_37));
		WheelColliderU5BU5D_t41* L_38 = (__this->___m_WheelColliders_4);
		NullCheck(L_38);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_38, 2);
		int32_t L_39 = 2;
		float L_40 = V_3;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_38, L_39, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_brakeTorque_m311((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_38, L_39, sizeof(WheelCollider_t56 *))), L_40, /*hidden argument*/NULL);
		WheelColliderU5BU5D_t41* L_41 = (__this->___m_WheelColliders_4);
		NullCheck(L_41);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_41, 3);
		int32_t L_42 = 3;
		float L_43 = V_3;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_41, L_42, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_brakeTorque_m311((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_41, L_42, sizeof(WheelCollider_t56 *))), L_43, /*hidden argument*/NULL);
	}

IL_0120:
	{
		CarController_CalculateRevs_m163(__this, /*hidden argument*/NULL);
		CarController_GearChanging_m159(__this, /*hidden argument*/NULL);
		CarController_AddDownForce_m168(__this, /*hidden argument*/NULL);
		CarController_CheckForWheelSpin_m169(__this, /*hidden argument*/NULL);
		CarController_TractionControl_m170(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CapSpeed()
extern "C" void CarController_CapSpeed_m165 (CarController_t29 * __this, const MethodInfo* method)
{
	float V_0 = 0.0f;
	Vector3_t12  V_1 = {0};
	int32_t V_2 = {0};
	Vector3_t12  V_3 = {0};
	Vector3_t12  V_4 = {0};
	{
		Rigidbody_t34 * L_0 = (__this->___m_Rigidbody_29);
		NullCheck(L_0);
		Vector3_t12  L_1 = Rigidbody_get_velocity_m270(L_0, /*hidden argument*/NULL);
		V_1 = L_1;
		float L_2 = Vector3_get_magnitude_m271((&V_1), /*hidden argument*/NULL);
		V_0 = L_2;
		int32_t L_3 = (__this->___m_SpeedType_15);
		V_2 = L_3;
		int32_t L_4 = V_2;
		if (!L_4)
		{
			goto IL_002d;
		}
	}
	{
		int32_t L_5 = V_2;
		if ((((int32_t)L_5) == ((int32_t)1)))
		{
			goto IL_0075;
		}
	}
	{
		goto IL_00be;
	}

IL_002d:
	{
		float L_6 = V_0;
		V_0 = ((float)((float)L_6*(float)(2.23693633f)));
		float L_7 = V_0;
		float L_8 = (__this->___m_Topspeed_16);
		if ((!(((float)L_7) > ((float)L_8))))
		{
			goto IL_0070;
		}
	}
	{
		Rigidbody_t34 * L_9 = (__this->___m_Rigidbody_29);
		float L_10 = (__this->___m_Topspeed_16);
		Rigidbody_t34 * L_11 = (__this->___m_Rigidbody_29);
		NullCheck(L_11);
		Vector3_t12  L_12 = Rigidbody_get_velocity_m270(L_11, /*hidden argument*/NULL);
		V_3 = L_12;
		Vector3_t12  L_13 = Vector3_get_normalized_m251((&V_3), /*hidden argument*/NULL);
		Vector3_t12  L_14 = Vector3_op_Multiply_m312(NULL /*static, unused*/, ((float)((float)L_10/(float)(2.23693633f))), L_13, /*hidden argument*/NULL);
		NullCheck(L_9);
		Rigidbody_set_velocity_m313(L_9, L_14, /*hidden argument*/NULL);
	}

IL_0070:
	{
		goto IL_00be;
	}

IL_0075:
	{
		float L_15 = V_0;
		V_0 = ((float)((float)L_15*(float)(3.6f)));
		float L_16 = V_0;
		float L_17 = (__this->___m_Topspeed_16);
		if ((!(((float)L_16) > ((float)L_17))))
		{
			goto IL_00b9;
		}
	}
	{
		Rigidbody_t34 * L_18 = (__this->___m_Rigidbody_29);
		float L_19 = (__this->___m_Topspeed_16);
		Rigidbody_t34 * L_20 = (__this->___m_Rigidbody_29);
		NullCheck(L_20);
		Vector3_t12  L_21 = Rigidbody_get_velocity_m270(L_20, /*hidden argument*/NULL);
		V_4 = L_21;
		Vector3_t12  L_22 = Vector3_get_normalized_m251((&V_4), /*hidden argument*/NULL);
		Vector3_t12  L_23 = Vector3_op_Multiply_m312(NULL /*static, unused*/, ((float)((float)L_19/(float)(3.6f))), L_22, /*hidden argument*/NULL);
		NullCheck(L_18);
		Rigidbody_set_velocity_m313(L_18, L_23, /*hidden argument*/NULL);
	}

IL_00b9:
	{
		goto IL_00be;
	}

IL_00be:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::ApplyDrive(System.Single,System.Single)
extern "C" void CarController_ApplyDrive_m166 (CarController_t29 * __this, float ___accel, float ___footbrake, const MethodInfo* method)
{
	float V_0 = 0.0f;
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	int32_t V_3 = {0};
	float V_4 = 0.0f;
	{
		int32_t L_0 = (__this->___m_CarDriveType_3);
		V_3 = L_0;
		int32_t L_1 = V_3;
		if (L_1 == 0)
		{
			goto IL_0052;
		}
		if (L_1 == 1)
		{
			goto IL_0087;
		}
		if (L_1 == 2)
		{
			goto IL_001e;
		}
	}
	{
		goto IL_00bc;
	}

IL_001e:
	{
		float L_2 = ___accel;
		float L_3 = (__this->___m_CurrentTorque_28);
		V_0 = ((float)((float)L_2*(float)((float)((float)L_3/(float)(4.0f)))));
		V_1 = 0;
		goto IL_0046;
	}

IL_0034:
	{
		WheelColliderU5BU5D_t41* L_4 = (__this->___m_WheelColliders_4);
		int32_t L_5 = V_1;
		NullCheck(L_4);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_4, L_5);
		int32_t L_6 = L_5;
		float L_7 = V_0;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_4, L_6, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_motorTorque_m314((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_4, L_6, sizeof(WheelCollider_t56 *))), L_7, /*hidden argument*/NULL);
		int32_t L_8 = V_1;
		V_1 = ((int32_t)((int32_t)L_8+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_9 = V_1;
		if ((((int32_t)L_9) < ((int32_t)4)))
		{
			goto IL_0034;
		}
	}
	{
		goto IL_00bc;
	}

IL_0052:
	{
		float L_10 = ___accel;
		float L_11 = (__this->___m_CurrentTorque_28);
		V_0 = ((float)((float)L_10*(float)((float)((float)L_11/(float)(2.0f)))));
		WheelColliderU5BU5D_t41* L_12 = (__this->___m_WheelColliders_4);
		NullCheck(L_12);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_12, 0);
		int32_t L_13 = 0;
		float L_14 = V_0;
		V_4 = L_14;
		WheelColliderU5BU5D_t41* L_15 = (__this->___m_WheelColliders_4);
		NullCheck(L_15);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_15, 1);
		int32_t L_16 = 1;
		float L_17 = V_4;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_15, L_16, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_motorTorque_m314((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_15, L_16, sizeof(WheelCollider_t56 *))), L_17, /*hidden argument*/NULL);
		float L_18 = V_4;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_12, L_13, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_motorTorque_m314((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_12, L_13, sizeof(WheelCollider_t56 *))), L_18, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_0087:
	{
		float L_19 = ___accel;
		float L_20 = (__this->___m_CurrentTorque_28);
		V_0 = ((float)((float)L_19*(float)((float)((float)L_20/(float)(2.0f)))));
		WheelColliderU5BU5D_t41* L_21 = (__this->___m_WheelColliders_4);
		NullCheck(L_21);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_21, 2);
		int32_t L_22 = 2;
		float L_23 = V_0;
		V_4 = L_23;
		WheelColliderU5BU5D_t41* L_24 = (__this->___m_WheelColliders_4);
		NullCheck(L_24);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_24, 3);
		int32_t L_25 = 3;
		float L_26 = V_4;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_24, L_25, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_motorTorque_m314((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_24, L_25, sizeof(WheelCollider_t56 *))), L_26, /*hidden argument*/NULL);
		float L_27 = V_4;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_21, L_22, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_motorTorque_m314((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_21, L_22, sizeof(WheelCollider_t56 *))), L_27, /*hidden argument*/NULL);
		goto IL_00bc;
	}

IL_00bc:
	{
		V_2 = 0;
		goto IL_0149;
	}

IL_00c3:
	{
		float L_28 = CarController_get_CurrentSpeed_m152(__this, /*hidden argument*/NULL);
		if ((!(((float)L_28) > ((float)(5.0f)))))
		{
			goto IL_0112;
		}
	}
	{
		Transform_t33 * L_29 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t12  L_30 = Transform_get_forward_m269(L_29, /*hidden argument*/NULL);
		Rigidbody_t34 * L_31 = (__this->___m_Rigidbody_29);
		NullCheck(L_31);
		Vector3_t12  L_32 = Rigidbody_get_velocity_m270(L_31, /*hidden argument*/NULL);
		float L_33 = Vector3_Angle_m272(NULL /*static, unused*/, L_30, L_32, /*hidden argument*/NULL);
		if ((!(((float)L_33) < ((float)(50.0f)))))
		{
			goto IL_0112;
		}
	}
	{
		WheelColliderU5BU5D_t41* L_34 = (__this->___m_WheelColliders_4);
		int32_t L_35 = V_2;
		NullCheck(L_34);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_34, L_35);
		int32_t L_36 = L_35;
		float L_37 = (__this->___m_BrakeTorque_20);
		float L_38 = ___footbrake;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_34, L_36, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_brakeTorque_m311((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_34, L_36, sizeof(WheelCollider_t56 *))), ((float)((float)L_37*(float)L_38)), /*hidden argument*/NULL);
		goto IL_0145;
	}

IL_0112:
	{
		float L_39 = ___footbrake;
		if ((!(((float)L_39) > ((float)(0.0f)))))
		{
			goto IL_0145;
		}
	}
	{
		WheelColliderU5BU5D_t41* L_40 = (__this->___m_WheelColliders_4);
		int32_t L_41 = V_2;
		NullCheck(L_40);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_40, L_41);
		int32_t L_42 = L_41;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_40, L_42, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_brakeTorque_m311((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_40, L_42, sizeof(WheelCollider_t56 *))), (0.0f), /*hidden argument*/NULL);
		WheelColliderU5BU5D_t41* L_43 = (__this->___m_WheelColliders_4);
		int32_t L_44 = V_2;
		NullCheck(L_43);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_43, L_44);
		int32_t L_45 = L_44;
		float L_46 = (__this->___m_ReverseTorque_12);
		float L_47 = ___footbrake;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_43, L_45, sizeof(WheelCollider_t56 *))));
		WheelCollider_set_motorTorque_m314((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_43, L_45, sizeof(WheelCollider_t56 *))), ((float)((float)((-L_46))*(float)L_47)), /*hidden argument*/NULL);
	}

IL_0145:
	{
		int32_t L_48 = V_2;
		V_2 = ((int32_t)((int32_t)L_48+(int32_t)1));
	}

IL_0149:
	{
		int32_t L_49 = V_2;
		if ((((int32_t)L_49) < ((int32_t)4)))
		{
			goto IL_00c3;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::SteerHelper()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CarController_SteerHelper_m167 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	WheelHit_t75  V_1 = {0};
	float V_2 = 0.0f;
	Quaternion_t45  V_3 = {0};
	Vector3_t12  V_4 = {0};
	Vector3_t12  V_5 = {0};
	Vector3_t12  V_6 = {0};
	{
		V_0 = 0;
		goto IL_0032;
	}

IL_0007:
	{
		WheelColliderU5BU5D_t41* L_0 = (__this->___m_WheelColliders_4);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_2, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_2, sizeof(WheelCollider_t56 *))), (&V_1), /*hidden argument*/NULL);
		Vector3_t12  L_3 = WheelHit_get_normal_m316((&V_1), /*hidden argument*/NULL);
		Vector3_t12  L_4 = Vector3_get_zero_m227(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_5 = Vector3_op_Equality_m317(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_002e;
		}
	}
	{
		return;
	}

IL_002e:
	{
		int32_t L_6 = V_0;
		V_0 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0032:
	{
		int32_t L_7 = V_0;
		if ((((int32_t)L_7) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		float L_8 = (__this->___m_OldRotation_27);
		Transform_t33 * L_9 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_9);
		Vector3_t12  L_10 = Transform_get_eulerAngles_m318(L_9, /*hidden argument*/NULL);
		V_4 = L_10;
		float L_11 = ((&V_4)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_12 = fabsf(((float)((float)L_8-(float)L_11)));
		if ((!(((float)L_12) < ((float)(10.0f)))))
		{
			goto IL_00ae;
		}
	}
	{
		Transform_t33 * L_13 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_13);
		Vector3_t12  L_14 = Transform_get_eulerAngles_m318(L_13, /*hidden argument*/NULL);
		V_5 = L_14;
		float L_15 = ((&V_5)->___y_2);
		float L_16 = (__this->___m_OldRotation_27);
		float L_17 = (__this->___m_SteerHelper_9);
		V_2 = ((float)((float)((float)((float)L_15-(float)L_16))*(float)L_17));
		float L_18 = V_2;
		Vector3_t12  L_19 = Vector3_get_up_m319(NULL /*static, unused*/, /*hidden argument*/NULL);
		Quaternion_t45  L_20 = Quaternion_AngleAxis_m320(NULL /*static, unused*/, L_18, L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		Rigidbody_t34 * L_21 = (__this->___m_Rigidbody_29);
		Quaternion_t45  L_22 = V_3;
		Rigidbody_t34 * L_23 = (__this->___m_Rigidbody_29);
		NullCheck(L_23);
		Vector3_t12  L_24 = Rigidbody_get_velocity_m270(L_23, /*hidden argument*/NULL);
		Vector3_t12  L_25 = Quaternion_op_Multiply_m321(NULL /*static, unused*/, L_22, L_24, /*hidden argument*/NULL);
		NullCheck(L_21);
		Rigidbody_set_velocity_m313(L_21, L_25, /*hidden argument*/NULL);
	}

IL_00ae:
	{
		Transform_t33 * L_26 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_26);
		Vector3_t12  L_27 = Transform_get_eulerAngles_m318(L_26, /*hidden argument*/NULL);
		V_6 = L_27;
		float L_28 = ((&V_6)->___y_2);
		__this->___m_OldRotation_27 = L_28;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::AddDownForce()
extern "C" void CarController_AddDownForce_m168 (CarController_t29 * __this, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	{
		WheelColliderU5BU5D_t41* L_0 = (__this->___m_WheelColliders_4);
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, 0);
		int32_t L_1 = 0;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_1, sizeof(WheelCollider_t56 *))));
		Rigidbody_t34 * L_2 = Collider_get_attachedRigidbody_m306((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_1, sizeof(WheelCollider_t56 *))), /*hidden argument*/NULL);
		Transform_t33 * L_3 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t12  L_4 = Transform_get_up_m322(L_3, /*hidden argument*/NULL);
		Vector3_t12  L_5 = Vector3_op_UnaryNegation_m323(NULL /*static, unused*/, L_4, /*hidden argument*/NULL);
		float L_6 = (__this->___m_Downforce_14);
		Vector3_t12  L_7 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_5, L_6, /*hidden argument*/NULL);
		WheelColliderU5BU5D_t41* L_8 = (__this->___m_WheelColliders_4);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 0);
		int32_t L_9 = 0;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_8, L_9, sizeof(WheelCollider_t56 *))));
		Rigidbody_t34 * L_10 = Collider_get_attachedRigidbody_m306((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_8, L_9, sizeof(WheelCollider_t56 *))), /*hidden argument*/NULL);
		NullCheck(L_10);
		Vector3_t12  L_11 = Rigidbody_get_velocity_m270(L_10, /*hidden argument*/NULL);
		V_0 = L_11;
		float L_12 = Vector3_get_magnitude_m271((&V_0), /*hidden argument*/NULL);
		Vector3_t12  L_13 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_7, L_12, /*hidden argument*/NULL);
		NullCheck(L_2);
		Rigidbody_AddForce_m324(L_2, L_13, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CheckForWheelSpin()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CarController_CheckForWheelSpin_m169 (CarController_t29 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	int32_t V_0 = 0;
	WheelHit_t75  V_1 = {0};
	{
		V_0 = 0;
		goto IL_009f;
	}

IL_0007:
	{
		WheelColliderU5BU5D_t41* L_0 = (__this->___m_WheelColliders_4);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_2, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_0, L_2, sizeof(WheelCollider_t56 *))), (&V_1), /*hidden argument*/NULL);
		float L_3 = WheelHit_get_forwardSlip_m325((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_4 = fabsf(L_3);
		float L_5 = (__this->___m_SlipLimit_19);
		if ((((float)L_4) >= ((float)L_5)))
		{
			goto IL_0045;
		}
	}
	{
		float L_6 = WheelHit_get_sidewaysSlip_m326((&V_1), /*hidden argument*/NULL);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_7 = fabsf(L_6);
		float L_8 = (__this->___m_SlipLimit_19);
		if ((!(((float)L_7) >= ((float)L_8))))
		{
			goto IL_006f;
		}
	}

IL_0045:
	{
		WheelEffectsU5BU5D_t43* L_9 = (__this->___m_WheelEffects_6);
		int32_t L_10 = V_0;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_9, L_11, sizeof(WheelEffects_t54 *))));
		WheelEffects_EmitTyreSmoke_m207((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_9, L_11, sizeof(WheelEffects_t54 *))), /*hidden argument*/NULL);
		bool L_12 = CarController_AnySkidSoundPlaying_m172(__this, /*hidden argument*/NULL);
		if (L_12)
		{
			goto IL_006a;
		}
	}
	{
		WheelEffectsU5BU5D_t43* L_13 = (__this->___m_WheelEffects_6);
		int32_t L_14 = V_0;
		NullCheck(L_13);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_13, L_14);
		int32_t L_15 = L_14;
		NullCheck((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_13, L_15, sizeof(WheelEffects_t54 *))));
		WheelEffects_PlayAudio_m208((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_13, L_15, sizeof(WheelEffects_t54 *))), /*hidden argument*/NULL);
	}

IL_006a:
	{
		goto IL_009b;
	}

IL_006f:
	{
		WheelEffectsU5BU5D_t43* L_16 = (__this->___m_WheelEffects_6);
		int32_t L_17 = V_0;
		NullCheck(L_16);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_16, L_17);
		int32_t L_18 = L_17;
		NullCheck((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_16, L_18, sizeof(WheelEffects_t54 *))));
		bool L_19 = WheelEffects_get_PlayingAudio_m204((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_16, L_18, sizeof(WheelEffects_t54 *))), /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_008e;
		}
	}
	{
		WheelEffectsU5BU5D_t43* L_20 = (__this->___m_WheelEffects_6);
		int32_t L_21 = V_0;
		NullCheck(L_20);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_20, L_21);
		int32_t L_22 = L_21;
		NullCheck((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_20, L_22, sizeof(WheelEffects_t54 *))));
		WheelEffects_StopAudio_m209((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_20, L_22, sizeof(WheelEffects_t54 *))), /*hidden argument*/NULL);
	}

IL_008e:
	{
		WheelEffectsU5BU5D_t43* L_23 = (__this->___m_WheelEffects_6);
		int32_t L_24 = V_0;
		NullCheck(L_23);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_23, L_24);
		int32_t L_25 = L_24;
		NullCheck((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_23, L_25, sizeof(WheelEffects_t54 *))));
		WheelEffects_EndSkidTrail_m211((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_23, L_25, sizeof(WheelEffects_t54 *))), /*hidden argument*/NULL);
	}

IL_009b:
	{
		int32_t L_26 = V_0;
		V_0 = ((int32_t)((int32_t)L_26+(int32_t)1));
	}

IL_009f:
	{
		int32_t L_27 = V_0;
		if ((((int32_t)L_27) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::TractionControl()
extern "C" void CarController_TractionControl_m170 (CarController_t29 * __this, const MethodInfo* method)
{
	WheelHit_t75  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = {0};
	{
		int32_t L_0 = (__this->___m_CarDriveType_3);
		V_2 = L_0;
		int32_t L_1 = V_2;
		if (L_1 == 0)
		{
			goto IL_0091;
		}
		if (L_1 == 1)
		{
			goto IL_0052;
		}
		if (L_1 == 2)
		{
			goto IL_001e;
		}
	}
	{
		goto IL_00d0;
	}

IL_001e:
	{
		V_1 = 0;
		goto IL_0046;
	}

IL_0025:
	{
		WheelColliderU5BU5D_t41* L_2 = (__this->___m_WheelColliders_4);
		int32_t L_3 = V_1;
		NullCheck(L_2);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_2, L_3);
		int32_t L_4 = L_3;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_2, L_4, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_2, L_4, sizeof(WheelCollider_t56 *))), (&V_0), /*hidden argument*/NULL);
		float L_5 = WheelHit_get_forwardSlip_m325((&V_0), /*hidden argument*/NULL);
		CarController_AdjustTorque_m171(__this, L_5, /*hidden argument*/NULL);
		int32_t L_6 = V_1;
		V_1 = ((int32_t)((int32_t)L_6+(int32_t)1));
	}

IL_0046:
	{
		int32_t L_7 = V_1;
		if ((((int32_t)L_7) < ((int32_t)4)))
		{
			goto IL_0025;
		}
	}
	{
		goto IL_00d0;
	}

IL_0052:
	{
		WheelColliderU5BU5D_t41* L_8 = (__this->___m_WheelColliders_4);
		NullCheck(L_8);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_8, 2);
		int32_t L_9 = 2;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_8, L_9, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_8, L_9, sizeof(WheelCollider_t56 *))), (&V_0), /*hidden argument*/NULL);
		float L_10 = WheelHit_get_forwardSlip_m325((&V_0), /*hidden argument*/NULL);
		CarController_AdjustTorque_m171(__this, L_10, /*hidden argument*/NULL);
		WheelColliderU5BU5D_t41* L_11 = (__this->___m_WheelColliders_4);
		NullCheck(L_11);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_11, 3);
		int32_t L_12 = 3;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_11, L_12, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_11, L_12, sizeof(WheelCollider_t56 *))), (&V_0), /*hidden argument*/NULL);
		float L_13 = WheelHit_get_forwardSlip_m325((&V_0), /*hidden argument*/NULL);
		CarController_AdjustTorque_m171(__this, L_13, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_0091:
	{
		WheelColliderU5BU5D_t41* L_14 = (__this->___m_WheelColliders_4);
		NullCheck(L_14);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_14, 0);
		int32_t L_15 = 0;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_14, L_15, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_14, L_15, sizeof(WheelCollider_t56 *))), (&V_0), /*hidden argument*/NULL);
		float L_16 = WheelHit_get_forwardSlip_m325((&V_0), /*hidden argument*/NULL);
		CarController_AdjustTorque_m171(__this, L_16, /*hidden argument*/NULL);
		WheelColliderU5BU5D_t41* L_17 = (__this->___m_WheelColliders_4);
		NullCheck(L_17);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_17, 1);
		int32_t L_18 = 1;
		NullCheck((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_17, L_18, sizeof(WheelCollider_t56 *))));
		WheelCollider_GetGroundHit_m315((*(WheelCollider_t56 **)(WheelCollider_t56 **)SZArrayLdElema(L_17, L_18, sizeof(WheelCollider_t56 *))), (&V_0), /*hidden argument*/NULL);
		float L_19 = WheelHit_get_forwardSlip_m325((&V_0), /*hidden argument*/NULL);
		CarController_AdjustTorque_m171(__this, L_19, /*hidden argument*/NULL);
		goto IL_00d0;
	}

IL_00d0:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarController::AdjustTorque(System.Single)
extern "C" void CarController_AdjustTorque_m171 (CarController_t29 * __this, float ___forwardSlip, const MethodInfo* method)
{
	{
		float L_0 = ___forwardSlip;
		float L_1 = (__this->___m_SlipLimit_19);
		if ((!(((float)L_0) >= ((float)L_1))))
		{
			goto IL_003a;
		}
	}
	{
		float L_2 = (__this->___m_CurrentTorque_28);
		if ((!(((float)L_2) >= ((float)(0.0f)))))
		{
			goto IL_003a;
		}
	}
	{
		float L_3 = (__this->___m_CurrentTorque_28);
		float L_4 = (__this->___m_TractionControl_10);
		__this->___m_CurrentTorque_28 = ((float)((float)L_3-(float)((float)((float)(10.0f)*(float)L_4))));
		goto IL_0070;
	}

IL_003a:
	{
		float L_5 = (__this->___m_CurrentTorque_28);
		float L_6 = (__this->___m_TractionControl_10);
		__this->___m_CurrentTorque_28 = ((float)((float)L_5+(float)((float)((float)(10.0f)*(float)L_6))));
		float L_7 = (__this->___m_CurrentTorque_28);
		float L_8 = (__this->___m_FullTorqueOverAllWheels_11);
		if ((!(((float)L_7) > ((float)L_8))))
		{
			goto IL_0070;
		}
	}
	{
		float L_9 = (__this->___m_FullTorqueOverAllWheels_11);
		__this->___m_CurrentTorque_28 = L_9;
	}

IL_0070:
	{
		return;
	}
}
// System.Boolean UnityStandardAssets.Vehicles.Car.CarController::AnySkidSoundPlaying()
extern "C" bool CarController_AnySkidSoundPlaying_m172 (CarController_t29 * __this, const MethodInfo* method)
{
	int32_t V_0 = 0;
	{
		V_0 = 0;
		goto IL_001f;
	}

IL_0007:
	{
		WheelEffectsU5BU5D_t43* L_0 = (__this->___m_WheelEffects_6);
		int32_t L_1 = V_0;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_0, L_2, sizeof(WheelEffects_t54 *))));
		bool L_3 = WheelEffects_get_PlayingAudio_m204((*(WheelEffects_t54 **)(WheelEffects_t54 **)SZArrayLdElema(L_0, L_2, sizeof(WheelEffects_t54 *))), /*hidden argument*/NULL);
		if (!L_3)
		{
			goto IL_001b;
		}
	}
	{
		return 1;
	}

IL_001b:
	{
		int32_t L_4 = V_0;
		V_0 = ((int32_t)((int32_t)L_4+(int32_t)1));
	}

IL_001f:
	{
		int32_t L_5 = V_0;
		if ((((int32_t)L_5) < ((int32_t)4)))
		{
			goto IL_0007;
		}
	}
	{
		return 0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::.ctor()
extern "C" void CarSelfRighting__ctor_m173 (CarSelfRighting_t46 * __this, const MethodInfo* method)
{
	{
		__this->___m_WaitTime_2 = (3.0f);
		__this->___m_VelocityThreshold_3 = (1.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var;
extern "C" void CarSelfRighting_Start_m174 (CarSelfRighting_t46 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	{
		Rigidbody_t34 * L_0 = Component_GetComponent_TisRigidbody_t34_m268(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var);
		__this->___m_Rigidbody_5 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Update()
extern "C" void CarSelfRighting_Update_m175 (CarSelfRighting_t46 * __this, const MethodInfo* method)
{
	Vector3_t12  V_0 = {0};
	Vector3_t12  V_1 = {0};
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Vector3_t12  L_1 = Transform_get_up_m322(L_0, /*hidden argument*/NULL);
		V_0 = L_1;
		float L_2 = ((&V_0)->___y_2);
		if ((((float)L_2) > ((float)(0.0f))))
		{
			goto IL_003b;
		}
	}
	{
		Rigidbody_t34 * L_3 = (__this->___m_Rigidbody_5);
		NullCheck(L_3);
		Vector3_t12  L_4 = Rigidbody_get_velocity_m270(L_3, /*hidden argument*/NULL);
		V_1 = L_4;
		float L_5 = Vector3_get_magnitude_m271((&V_1), /*hidden argument*/NULL);
		float L_6 = (__this->___m_VelocityThreshold_3);
		if ((!(((float)L_5) > ((float)L_6))))
		{
			goto IL_0046;
		}
	}

IL_003b:
	{
		float L_7 = Time_get_time_m276(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___m_LastOkTime_4 = L_7;
	}

IL_0046:
	{
		float L_8 = Time_get_time_m276(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_9 = (__this->___m_LastOkTime_4);
		float L_10 = (__this->___m_WaitTime_2);
		if ((!(((float)L_8) > ((float)((float)((float)L_9+(float)L_10))))))
		{
			goto IL_0063;
		}
	}
	{
		CarSelfRighting_RightCar_m176(__this, /*hidden argument*/NULL);
	}

IL_0063:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::RightCar()
extern "C" void CarSelfRighting_RightCar_m176 (CarSelfRighting_t46 * __this, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Transform_t33 * L_1 = L_0;
		NullCheck(L_1);
		Vector3_t12  L_2 = Transform_get_position_m224(L_1, /*hidden argument*/NULL);
		Vector3_t12  L_3 = Vector3_get_up_m319(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t12  L_4 = Vector3_op_Addition_m279(NULL /*static, unused*/, L_2, L_3, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m231(L_1, L_4, /*hidden argument*/NULL);
		Transform_t33 * L_5 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Transform_t33 * L_6 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_6);
		Vector3_t12  L_7 = Transform_get_forward_m269(L_6, /*hidden argument*/NULL);
		Quaternion_t45  L_8 = Quaternion_LookRotation_m327(NULL /*static, unused*/, L_7, /*hidden argument*/NULL);
		NullCheck(L_5);
		Transform_set_rotation_m309(L_5, L_8, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::.ctor()
extern "C" void CarUserControl__ctor_m177 (CarUserControl_t47 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::Start()
extern "C" void CarUserControl_Start_m178 (CarUserControl_t47 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::Awake()
extern const MethodInfo* Component_GetComponent_TisCarController_t29_m266_MethodInfo_var;
extern "C" void CarUserControl_Awake_m179 (CarUserControl_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCarController_t29_m266_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483658);
		s_Il2CppMethodIntialized = true;
	}
	{
		CarController_t29 * L_0 = Component_GetComponent_TisCarController_t29_m266(__this, /*hidden argument*/Component_GetComponent_TisCarController_t29_m266_MethodInfo_var);
		__this->___m_Car_2 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.CarUserControl::FixedUpdate()
extern TypeInfo* CrossPlatformInputManager_t7_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral0;
extern Il2CppCodeGenString* _stringLiteral2;
extern "C" void CarUserControl_FixedUpdate_m180 (CarUserControl_t47 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		CrossPlatformInputManager_t7_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(1);
		_stringLiteral0 = il2cpp_codegen_string_literal_from_index(0);
		_stringLiteral2 = il2cpp_codegen_string_literal_from_index(2);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	{
		IL2CPP_RUNTIME_CLASS_INIT(CrossPlatformInputManager_t7_il2cpp_TypeInfo_var);
		float L_0 = CrossPlatformInputManager_GetAxis_m45(NULL /*static, unused*/, _stringLiteral0, /*hidden argument*/NULL);
		V_0 = L_0;
		float L_1 = CrossPlatformInputManager_GetAxis_m45(NULL /*static, unused*/, _stringLiteral2, /*hidden argument*/NULL);
		V_1 = L_1;
		CarController_t29 * L_2 = (__this->___m_Car_2);
		float L_3 = V_0;
		float L_4 = V_1;
		float L_5 = V_1;
		NullCheck(L_2);
		CarController_Move_m164(L_2, L_3, L_4, L_5, (0.0f), /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.Mudguard::.ctor()
extern "C" void Mudguard__ctor_m181 (Mudguard_t48 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Start()
extern "C" void Mudguard_Start_m182 (Mudguard_t48 * __this, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Quaternion_t45  L_1 = Transform_get_localRotation_m305(L_0, /*hidden argument*/NULL);
		__this->___m_OriginalRotation_3 = L_1;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.Mudguard::Update()
extern "C" void Mudguard_Update_m183 (Mudguard_t48 * __this, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Quaternion_t45  L_1 = (__this->___m_OriginalRotation_3);
		CarController_t29 * L_2 = (__this->___carController_2);
		NullCheck(L_2);
		float L_3 = CarController_get_CurrentSteerAngle_m151(L_2, /*hidden argument*/NULL);
		Quaternion_t45  L_4 = Quaternion_Euler_m328(NULL /*static, unused*/, (0.0f), L_3, (0.0f), /*hidden argument*/NULL);
		Quaternion_t45  L_5 = Quaternion_op_Multiply_m329(NULL /*static, unused*/, L_1, L_4, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localRotation_m330(L_0, L_5, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m184 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m185 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m186 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m187 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method)
{
	uint32_t V_0 = 0;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0034;
		}
	}
	{
		goto IL_007b;
	}

IL_0021:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_007d;
	}

IL_0034:
	{
		SkidTrail_t50 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		Transform_t33 * L_3 = Component_get_transform_m223(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_t33 * L_4 = Transform_get_parent_m331(L_3, /*hidden argument*/NULL);
		NullCheck(L_4);
		Transform_t33 * L_5 = Transform_get_parent_m331(L_4, /*hidden argument*/NULL);
		bool L_6 = Object_op_Equality_m217(NULL /*static, unused*/, L_5, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_6)
		{
			goto IL_006f;
		}
	}
	{
		SkidTrail_t50 * L_7 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_7);
		GameObject_t52 * L_8 = Component_get_gameObject_m237(L_7, /*hidden argument*/NULL);
		SkidTrail_t50 * L_9 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_9);
		float L_10 = (L_9->___m_PersistTime_2);
		Object_Destroy_m332(NULL /*static, unused*/, L_8, L_10, /*hidden argument*/NULL);
	}

IL_006f:
	{
		goto IL_0021;
	}
	// Dead block : IL_0074: ldarg.0

IL_007b:
	{
		return 0;
	}

IL_007d:
	{
		return 1;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m188 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t76_il2cpp_TypeInfo_var;
extern "C" void U3CStartU3Ec__Iterator0_Reset_m189 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t76_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t76 * L_0 = (NotSupportedException_t76 *)il2cpp_codegen_object_new (NotSupportedException_t76_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m333(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail::.ctor()
extern "C" void SkidTrail__ctor_m190 (SkidTrail_t50 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.SkidTrail::Start()
extern TypeInfo* U3CStartU3Ec__Iterator0_t49_il2cpp_TypeInfo_var;
extern "C" Object_t * SkidTrail_Start_m191 (SkidTrail_t50 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartU3Ec__Iterator0_t49_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(38);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartU3Ec__Iterator0_t49 * V_0 = {0};
	{
		U3CStartU3Ec__Iterator0_t49 * L_0 = (U3CStartU3Ec__Iterator0_t49 *)il2cpp_codegen_object_new (U3CStartU3Ec__Iterator0_t49_il2cpp_TypeInfo_var);
		U3CStartU3Ec__Iterator0__ctor_m184(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartU3Ec__Iterator0_t49 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CStartU3Ec__Iterator0_t49 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.Suspension::.ctor()
extern "C" void Suspension__ctor_m192 (Suspension_t51 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.Suspension::Start()
extern "C" void Suspension_Start_m193 (Suspension_t51 * __this, const MethodInfo* method)
{
	{
		GameObject_t52 * L_0 = (__this->___wheel_2);
		NullCheck(L_0);
		Transform_t33 * L_1 = GameObject_get_transform_m303(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t12  L_2 = Transform_get_localPosition_m334(L_1, /*hidden argument*/NULL);
		__this->___m_TargetOriginalPosition_3 = L_2;
		Transform_t33 * L_3 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t12  L_4 = Transform_get_localPosition_m334(L_3, /*hidden argument*/NULL);
		__this->___m_Origin_4 = L_4;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.Suspension::Update()
extern "C" void Suspension_Update_m194 (Suspension_t51 * __this, const MethodInfo* method)
{
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12  L_1 = (__this->___m_Origin_4);
		GameObject_t52 * L_2 = (__this->___wheel_2);
		NullCheck(L_2);
		Transform_t33 * L_3 = GameObject_get_transform_m303(L_2, /*hidden argument*/NULL);
		NullCheck(L_3);
		Vector3_t12  L_4 = Transform_get_localPosition_m334(L_3, /*hidden argument*/NULL);
		Vector3_t12  L_5 = (__this->___m_TargetOriginalPosition_3);
		Vector3_t12  L_6 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_4, L_5, /*hidden argument*/NULL);
		Vector3_t12  L_7 = Vector3_op_Addition_m279(NULL /*static, unused*/, L_1, L_6, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_localPosition_m335(L_0, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::.ctor()
extern "C" void U3CStartSkidTrailU3Ec__Iterator1__ctor_m195 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartSkidTrailU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m196 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartSkidTrailU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m197 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::MoveNext()
extern const MethodInfo* Object_Instantiate_TisTransform_t33_m336_MethodInfo_var;
extern "C" bool U3CStartSkidTrailU3Ec__Iterator1_MoveNext_m198 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Object_Instantiate_TisTransform_t33_m336_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483663);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_00c7;
	}

IL_0021:
	{
		WheelEffects_t54 * L_2 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_2);
		WheelEffects_set_skidding_m203(L_2, 1, /*hidden argument*/NULL);
		WheelEffects_t54 * L_3 = (__this->___U3CU3Ef__this_2);
		WheelEffects_t54 * L_4 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_4);
		Transform_t33 * L_5 = (L_4->___SkidTrailPrefab_2);
		Transform_t33 * L_6 = Object_Instantiate_TisTransform_t33_m336(NULL /*static, unused*/, L_5, /*hidden argument*/Object_Instantiate_TisTransform_t33_m336_MethodInfo_var);
		NullCheck(L_3);
		L_3->___m_SkidTrail_6 = L_6;
		goto IL_0060;
	}

IL_004d:
	{
		__this->___U24current_1 = NULL;
		__this->___U24PC_0 = 1;
		goto IL_00c9;
	}

IL_0060:
	{
		WheelEffects_t54 * L_7 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_7);
		Transform_t33 * L_8 = (L_7->___m_SkidTrail_6);
		bool L_9 = Object_op_Equality_m217(NULL /*static, unused*/, L_8, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (L_9)
		{
			goto IL_004d;
		}
	}
	{
		WheelEffects_t54 * L_10 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_10);
		Transform_t33 * L_11 = (L_10->___m_SkidTrail_6);
		WheelEffects_t54 * L_12 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_12);
		Transform_t33 * L_13 = Component_get_transform_m223(L_12, /*hidden argument*/NULL);
		NullCheck(L_11);
		Transform_set_parent_m337(L_11, L_13, /*hidden argument*/NULL);
		WheelEffects_t54 * L_14 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_14);
		Transform_t33 * L_15 = (L_14->___m_SkidTrail_6);
		Vector3_t12  L_16 = Vector3_get_up_m319(NULL /*static, unused*/, /*hidden argument*/NULL);
		Vector3_t12  L_17 = Vector3_op_UnaryNegation_m323(NULL /*static, unused*/, L_16, /*hidden argument*/NULL);
		WheelEffects_t54 * L_18 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_18);
		WheelCollider_t56 * L_19 = (L_18->___m_WheelCollider_7);
		NullCheck(L_19);
		float L_20 = WheelCollider_get_radius_m338(L_19, /*hidden argument*/NULL);
		Vector3_t12  L_21 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_17, L_20, /*hidden argument*/NULL);
		NullCheck(L_15);
		Transform_set_localPosition_m335(L_15, L_21, /*hidden argument*/NULL);
		__this->___U24PC_0 = (-1);
	}

IL_00c7:
	{
		return 0;
	}

IL_00c9:
	{
		return 1;
	}
	// Dead block : IL_00cb: ldloc.1
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::Dispose()
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_Dispose_m199 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t76_il2cpp_TypeInfo_var;
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_Reset_m200 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t76_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t76 * L_0 = (NotSupportedException_t76 *)il2cpp_codegen_object_new (NotSupportedException_t76_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m333(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::.ctor()
extern "C" void WheelEffects__ctor_m201 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_skidding()
extern "C" bool WheelEffects_get_skidding_m202 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CskiddingU3Ek__BackingField_8);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_skidding(System.Boolean)
extern "C" void WheelEffects_set_skidding_m203 (WheelEffects_t54 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CskiddingU3Ek__BackingField_8 = L_0;
		return;
	}
}
// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_PlayingAudio()
extern "C" bool WheelEffects_get_PlayingAudio_m204 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___U3CPlayingAudioU3Ek__BackingField_9);
		return L_0;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_PlayingAudio(System.Boolean)
extern "C" void WheelEffects_set_PlayingAudio_m205 (WheelEffects_t54 * __this, bool ___value, const MethodInfo* method)
{
	{
		bool L_0 = ___value;
		__this->___U3CPlayingAudioU3Ek__BackingField_9 = L_0;
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::Start()
extern TypeInfo* WheelEffects_t54_il2cpp_TypeInfo_var;
extern TypeInfo* GameObject_t52_il2cpp_TypeInfo_var;
extern const MethodInfo* Component_GetComponentInChildren_TisParticleSystem_t55_m340_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisWheelCollider_t56_m343_MethodInfo_var;
extern const MethodInfo* Component_GetComponent_TisAudioSource_t38_m344_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral8;
extern Il2CppCodeGenString* _stringLiteral9;
extern "C" void WheelEffects_Start_m206 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WheelEffects_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		GameObject_t52_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(14);
		Component_GetComponentInChildren_TisParticleSystem_t55_m340_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483664);
		Component_GetComponent_TisWheelCollider_t56_m343_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483665);
		Component_GetComponent_TisAudioSource_t38_m344_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483666);
		_stringLiteral8 = il2cpp_codegen_string_literal_from_index(8);
		_stringLiteral9 = il2cpp_codegen_string_literal_from_index(9);
		s_Il2CppMethodIntialized = true;
	}
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_t33 * L_1 = Transform_get_root_m339(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		ParticleSystem_t55 * L_2 = Component_GetComponentInChildren_TisParticleSystem_t55_m340(L_1, /*hidden argument*/Component_GetComponentInChildren_TisParticleSystem_t55_m340_MethodInfo_var);
		__this->___skidParticles_4 = L_2;
		ParticleSystem_t55 * L_3 = (__this->___skidParticles_4);
		bool L_4 = Object_op_Equality_m217(NULL /*static, unused*/, L_3, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_4)
		{
			goto IL_0036;
		}
	}
	{
		Debug_LogWarning_m341(NULL /*static, unused*/, _stringLiteral8, /*hidden argument*/NULL);
		goto IL_0041;
	}

IL_0036:
	{
		ParticleSystem_t55 * L_5 = (__this->___skidParticles_4);
		NullCheck(L_5);
		ParticleSystem_Stop_m342(L_5, /*hidden argument*/NULL);
	}

IL_0041:
	{
		WheelCollider_t56 * L_6 = Component_GetComponent_TisWheelCollider_t56_m343(__this, /*hidden argument*/Component_GetComponent_TisWheelCollider_t56_m343_MethodInfo_var);
		__this->___m_WheelCollider_7 = L_6;
		AudioSource_t38 * L_7 = Component_GetComponent_TisAudioSource_t38_m344(__this, /*hidden argument*/Component_GetComponent_TisAudioSource_t38_m344_MethodInfo_var);
		__this->___m_AudioSource_5 = L_7;
		WheelEffects_set_PlayingAudio_m205(__this, 0, /*hidden argument*/NULL);
		Transform_t33 * L_8 = ((WheelEffects_t54_StaticFields*)WheelEffects_t54_il2cpp_TypeInfo_var->static_fields)->___skidTrailsDetachedParent_3;
		bool L_9 = Object_op_Equality_m217(NULL /*static, unused*/, L_8, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_0084;
		}
	}
	{
		GameObject_t52 * L_10 = (GameObject_t52 *)il2cpp_codegen_object_new (GameObject_t52_il2cpp_TypeInfo_var);
		GameObject__ctor_m233(L_10, _stringLiteral9, /*hidden argument*/NULL);
		NullCheck(L_10);
		Transform_t33 * L_11 = GameObject_get_transform_m303(L_10, /*hidden argument*/NULL);
		((WheelEffects_t54_StaticFields*)WheelEffects_t54_il2cpp_TypeInfo_var->static_fields)->___skidTrailsDetachedParent_3 = L_11;
	}

IL_0084:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EmitTyreSmoke()
extern "C" void WheelEffects_EmitTyreSmoke_m207 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	{
		ParticleSystem_t55 * L_0 = (__this->___skidParticles_4);
		NullCheck(L_0);
		Transform_t33 * L_1 = Component_get_transform_m223(L_0, /*hidden argument*/NULL);
		Transform_t33 * L_2 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Vector3_t12  L_3 = Transform_get_position_m224(L_2, /*hidden argument*/NULL);
		Transform_t33 * L_4 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_4);
		Vector3_t12  L_5 = Transform_get_up_m322(L_4, /*hidden argument*/NULL);
		WheelCollider_t56 * L_6 = (__this->___m_WheelCollider_7);
		NullCheck(L_6);
		float L_7 = WheelCollider_get_radius_m338(L_6, /*hidden argument*/NULL);
		Vector3_t12  L_8 = Vector3_op_Multiply_m278(NULL /*static, unused*/, L_5, L_7, /*hidden argument*/NULL);
		Vector3_t12  L_9 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_3, L_8, /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_position_m231(L_1, L_9, /*hidden argument*/NULL);
		ParticleSystem_t55 * L_10 = (__this->___skidParticles_4);
		NullCheck(L_10);
		ParticleSystem_Emit_m345(L_10, 1, /*hidden argument*/NULL);
		bool L_11 = WheelEffects_get_skidding_m202(__this, /*hidden argument*/NULL);
		if (L_11)
		{
			goto IL_005f;
		}
	}
	{
		Object_t * L_12 = WheelEffects_StartSkidTrail_m210(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m346(__this, L_12, /*hidden argument*/NULL);
	}

IL_005f:
	{
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::PlayAudio()
extern "C" void WheelEffects_PlayAudio_m208 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	{
		AudioSource_t38 * L_0 = (__this->___m_AudioSource_5);
		NullCheck(L_0);
		AudioSource_Play_m300(L_0, /*hidden argument*/NULL);
		WheelEffects_set_PlayingAudio_m205(__this, 1, /*hidden argument*/NULL);
		return;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::StopAudio()
extern "C" void WheelEffects_StopAudio_m209 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	{
		AudioSource_t38 * L_0 = (__this->___m_AudioSource_5);
		NullCheck(L_0);
		AudioSource_Stop_m347(L_0, /*hidden argument*/NULL);
		WheelEffects_set_PlayingAudio_m205(__this, 0, /*hidden argument*/NULL);
		return;
	}
}
// System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.WheelEffects::StartSkidTrail()
extern TypeInfo* U3CStartSkidTrailU3Ec__Iterator1_t53_il2cpp_TypeInfo_var;
extern "C" Object_t * WheelEffects_StartSkidTrail_m210 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CStartSkidTrailU3Ec__Iterator1_t53_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(40);
		s_Il2CppMethodIntialized = true;
	}
	U3CStartSkidTrailU3Ec__Iterator1_t53 * V_0 = {0};
	{
		U3CStartSkidTrailU3Ec__Iterator1_t53 * L_0 = (U3CStartSkidTrailU3Ec__Iterator1_t53 *)il2cpp_codegen_object_new (U3CStartSkidTrailU3Ec__Iterator1_t53_il2cpp_TypeInfo_var);
		U3CStartSkidTrailU3Ec__Iterator1__ctor_m195(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CStartSkidTrailU3Ec__Iterator1_t53 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CStartSkidTrailU3Ec__Iterator1_t53 * L_2 = V_0;
		return L_2;
	}
}
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EndSkidTrail()
extern TypeInfo* WheelEffects_t54_il2cpp_TypeInfo_var;
extern "C" void WheelEffects_EndSkidTrail_m211 (WheelEffects_t54 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WheelEffects_t54_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(34);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = WheelEffects_get_skidding_m202(__this, /*hidden argument*/NULL);
		if (L_0)
		{
			goto IL_000c;
		}
	}
	{
		return;
	}

IL_000c:
	{
		WheelEffects_set_skidding_m203(__this, 0, /*hidden argument*/NULL);
		Transform_t33 * L_1 = (__this->___m_SkidTrail_6);
		Transform_t33 * L_2 = ((WheelEffects_t54_StaticFields*)WheelEffects_t54_il2cpp_TypeInfo_var->static_fields)->___skidTrailsDetachedParent_3;
		NullCheck(L_1);
		Transform_set_parent_m337(L_1, L_2, /*hidden argument*/NULL);
		Transform_t33 * L_3 = (__this->___m_SkidTrail_6);
		NullCheck(L_3);
		GameObject_t52 * L_4 = Component_get_gameObject_m237(L_3, /*hidden argument*/NULL);
		Object_Destroy_m332(NULL /*static, unused*/, L_4, (10.0f), /*hidden argument*/NULL);
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
