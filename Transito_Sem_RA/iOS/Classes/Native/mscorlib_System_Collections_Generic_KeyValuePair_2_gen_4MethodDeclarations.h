﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_3MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m11665(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1875 *, String_t*, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m11591_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Key()
#define KeyValuePair_2_get_Key_m11666(__this, method) (( String_t* (*) (KeyValuePair_2_t1875 *, const MethodInfo*))KeyValuePair_2_get_Key_m11592_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m11667(__this, ___value, method) (( void (*) (KeyValuePair_2_t1875 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m11593_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::get_Value()
#define KeyValuePair_2_get_Value_m11668(__this, method) (( int32_t (*) (KeyValuePair_2_t1875 *, const MethodInfo*))KeyValuePair_2_get_Value_m11594_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m11669(__this, ___value, method) (( void (*) (KeyValuePair_2_t1875 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m11595_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,System.Int32>::ToString()
#define KeyValuePair_2_ToString_m11670(__this, method) (( String_t* (*) (KeyValuePair_2_t1875 *, const MethodInfo*))KeyValuePair_2_ToString_m11596_gshared)(__this, method)
