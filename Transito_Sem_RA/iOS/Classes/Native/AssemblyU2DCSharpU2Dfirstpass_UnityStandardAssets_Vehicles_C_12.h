﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.GameObject
struct GameObject_t52;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// UnityStandardAssets.Vehicles.Car.Suspension
struct  Suspension_t51  : public MonoBehaviour_t2
{
	// UnityEngine.GameObject UnityStandardAssets.Vehicles.Car.Suspension::wheel
	GameObject_t52 * ___wheel_2;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.Suspension::m_TargetOriginalPosition
	Vector3_t12  ___m_TargetOriginalPosition_3;
	// UnityEngine.Vector3 UnityStandardAssets.Vehicles.Car.Suspension::m_Origin
	Vector3_t12  ___m_Origin_4;
};
