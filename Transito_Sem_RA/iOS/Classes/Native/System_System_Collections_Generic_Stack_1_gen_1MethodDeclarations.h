﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::.ctor()
#define Stack_1__ctor_m11936(__this, method) (( void (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1__ctor_m11915_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m11937(__this, method) (( bool (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m11916_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m11938(__this, method) (( Object_t * (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m11917_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m11939(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t1892 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m11918_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11940(__this, method) (( Object_t* (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11919_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m11941(__this, method) (( Object_t * (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m11920_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Peek()
#define Stack_1_Peek_m11942(__this, method) (( List_1_t323 * (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_Peek_m11921_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Pop()
#define Stack_1_Pop_m11943(__this, method) (( List_1_t323 * (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_Pop_m11922_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::Push(T)
#define Stack_1_Push_m11944(__this, ___t, method) (( void (*) (Stack_1_t1892 *, List_1_t323 *, const MethodInfo*))Stack_1_Push_m11923_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::get_Count()
#define Stack_1_get_Count_m11945(__this, method) (( int32_t (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_get_Count_m11924_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Collections.Generic.List`1<UnityEngine.EventSystems.IEventSystemHandler>>::GetEnumerator()
#define Stack_1_GetEnumerator_m11946(__this, method) (( Enumerator_t2445  (*) (Stack_1_t1892 *, const MethodInfo*))Stack_1_GetEnumerator_m11925_gshared)(__this, method)
