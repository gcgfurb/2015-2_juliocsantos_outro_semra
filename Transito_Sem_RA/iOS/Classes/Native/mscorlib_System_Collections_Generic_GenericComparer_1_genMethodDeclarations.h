﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.GenericComparer`1<System.DateTime>
struct GenericComparer_1_t1798;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_DateTime.h"

// System.Void System.Collections.Generic.GenericComparer`1<System.DateTime>::.ctor()
extern "C" void GenericComparer_1__ctor_m10888_gshared (GenericComparer_1_t1798 * __this, const MethodInfo* method);
#define GenericComparer_1__ctor_m10888(__this, method) (( void (*) (GenericComparer_1_t1798 *, const MethodInfo*))GenericComparer_1__ctor_m10888_gshared)(__this, method)
// System.Int32 System.Collections.Generic.GenericComparer`1<System.DateTime>::Compare(T,T)
extern "C" int32_t GenericComparer_1_Compare_m18381_gshared (GenericComparer_1_t1798 * __this, DateTime_t546  ___x, DateTime_t546  ___y, const MethodInfo* method);
#define GenericComparer_1_Compare_m18381(__this, ___x, ___y, method) (( int32_t (*) (GenericComparer_1_t1798 *, DateTime_t546 , DateTime_t546 , const MethodInfo*))GenericComparer_1_Compare_m18381_gshared)(__this, ___x, ___y, method)
