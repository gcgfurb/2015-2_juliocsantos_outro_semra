﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Transform_1_9MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,System.Collections.DictionaryEntry>::.ctor(System.Object,System.IntPtr)
#define Transform_1__ctor_m16586(__this, ___object, ___method, method) (( void (*) (Transform_1_t2239 *, Object_t *, IntPtr_t, const MethodInfo*))Transform_1__ctor_m12800_gshared)(__this, ___object, ___method, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,System.Collections.DictionaryEntry>::Invoke(TKey,TValue)
#define Transform_1_Invoke_m16587(__this, ___key, ___value, method) (( DictionaryEntry_t1057  (*) (Transform_1_t2239 *, int32_t, LayoutCache_t549 *, const MethodInfo*))Transform_1_Invoke_m12801_gshared)(__this, ___key, ___value, method)
// System.IAsyncResult System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,System.Collections.DictionaryEntry>::BeginInvoke(TKey,TValue,System.AsyncCallback,System.Object)
#define Transform_1_BeginInvoke_m16588(__this, ___key, ___value, ___callback, ___object, method) (( Object_t * (*) (Transform_1_t2239 *, int32_t, LayoutCache_t549 *, AsyncCallback_t229 *, Object_t *, const MethodInfo*))Transform_1_BeginInvoke_m12802_gshared)(__this, ___key, ___value, ___callback, ___object, method)
// TRet System.Collections.Generic.Dictionary`2/Transform`1<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache,System.Collections.DictionaryEntry>::EndInvoke(System.IAsyncResult)
#define Transform_1_EndInvoke_m16589(__this, ___result, method) (( DictionaryEntry_t1057  (*) (Transform_1_t2239 *, Object_t *, const MethodInfo*))Transform_1_EndInvoke_m12803_gshared)(__this, ___result, method)
