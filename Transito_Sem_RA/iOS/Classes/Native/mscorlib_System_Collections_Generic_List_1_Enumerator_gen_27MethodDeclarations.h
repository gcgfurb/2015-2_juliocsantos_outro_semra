﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Int32>
struct List_1_t316;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_27.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15166_gshared (Enumerator_t2118 * __this, List_1_t316 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15166(__this, ___l, method) (( void (*) (Enumerator_t2118 *, List_1_t316 *, const MethodInfo*))Enumerator__ctor_m15166_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15167_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15167(__this, method) (( void (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15167_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Int32>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15168_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15168(__this, method) (( Object_t * (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15168_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::Dispose()
extern "C" void Enumerator_Dispose_m15169_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15169(__this, method) (( void (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_Dispose_m15169_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Int32>::VerifyState()
extern "C" void Enumerator_VerifyState_m15170_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15170(__this, method) (( void (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_VerifyState_m15170_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Int32>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15171_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15171(__this, method) (( bool (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_MoveNext_m15171_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Int32>::get_Current()
extern "C" int32_t Enumerator_get_Current_m15172_gshared (Enumerator_t2118 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15172(__this, method) (( int32_t (*) (Enumerator_t2118 *, const MethodInfo*))Enumerator_get_Current_m15172_gshared)(__this, method)
