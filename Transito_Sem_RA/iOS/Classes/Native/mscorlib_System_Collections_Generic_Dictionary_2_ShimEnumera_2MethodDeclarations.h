﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>
struct ShimEnumerator_t2278;
// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2268;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void ShimEnumerator__ctor_m17099_gshared (ShimEnumerator_t2278 * __this, Dictionary_2_t2268 * ___host, const MethodInfo* method);
#define ShimEnumerator__ctor_m17099(__this, ___host, method) (( void (*) (ShimEnumerator_t2278 *, Dictionary_2_t2268 *, const MethodInfo*))ShimEnumerator__ctor_m17099_gshared)(__this, ___host, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool ShimEnumerator_MoveNext_m17100_gshared (ShimEnumerator_t2278 * __this, const MethodInfo* method);
#define ShimEnumerator_MoveNext_m17100(__this, method) (( bool (*) (ShimEnumerator_t2278 *, const MethodInfo*))ShimEnumerator_MoveNext_m17100_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Entry()
extern "C" DictionaryEntry_t1057  ShimEnumerator_get_Entry_m17101_gshared (ShimEnumerator_t2278 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Entry_m17101(__this, method) (( DictionaryEntry_t1057  (*) (ShimEnumerator_t2278 *, const MethodInfo*))ShimEnumerator_get_Entry_m17101_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Key()
extern "C" Object_t * ShimEnumerator_get_Key_m17102_gshared (ShimEnumerator_t2278 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Key_m17102(__this, method) (( Object_t * (*) (ShimEnumerator_t2278 *, const MethodInfo*))ShimEnumerator_get_Key_m17102_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Value()
extern "C" Object_t * ShimEnumerator_get_Value_m17103_gshared (ShimEnumerator_t2278 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Value_m17103(__this, method) (( Object_t * (*) (ShimEnumerator_t2278 *, const MethodInfo*))ShimEnumerator_get_Value_m17103_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" Object_t * ShimEnumerator_get_Current_m17104_gshared (ShimEnumerator_t2278 * __this, const MethodInfo* method);
#define ShimEnumerator_get_Current_m17104(__this, method) (( Object_t * (*) (ShimEnumerator_t2278 *, const MethodInfo*))ShimEnumerator_get_Current_m17104_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ShimEnumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Reset()
extern "C" void ShimEnumerator_Reset_m17105_gshared (ShimEnumerator_t2278 * __this, const MethodInfo* method);
#define ShimEnumerator_Reset_m17105(__this, method) (( void (*) (ShimEnumerator_t2278 *, const MethodInfo*))ShimEnumerator_Reset_m17105_gshared)(__this, method)
