﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.WheelEffects
struct WheelEffects_t54;
// System.Collections.IEnumerator
struct IEnumerator_t59;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::.ctor()
extern "C" void WheelEffects__ctor_m201 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_skidding()
extern "C" bool WheelEffects_get_skidding_m202 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_skidding(System.Boolean)
extern "C" void WheelEffects_set_skidding_m203 (WheelEffects_t54 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects::get_PlayingAudio()
extern "C" bool WheelEffects_get_PlayingAudio_m204 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::set_PlayingAudio(System.Boolean)
extern "C" void WheelEffects_set_PlayingAudio_m205 (WheelEffects_t54 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::Start()
extern "C" void WheelEffects_Start_m206 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EmitTyreSmoke()
extern "C" void WheelEffects_EmitTyreSmoke_m207 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::PlayAudio()
extern "C" void WheelEffects_PlayAudio_m208 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::StopAudio()
extern "C" void WheelEffects_StopAudio_m209 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.WheelEffects::StartSkidTrail()
extern "C" Object_t * WheelEffects_StartSkidTrail_m210 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects::EndSkidTrail()
extern "C" void WheelEffects_EndSkidTrail_m211 (WheelEffects_t54 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
