﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_ExecuteEvents_EventF_16MethodDeclarations.h"

// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>::.ctor(System.Object,System.IntPtr)
#define EventFunction_1__ctor_m1891(__this, ___object, ___method, method) (( void (*) (EventFunction_1_t130 *, Object_t *, IntPtr_t, const MethodInfo*))EventFunction_1__ctor_m11801_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>::Invoke(T1,UnityEngine.EventSystems.BaseEventData)
#define EventFunction_1_Invoke_m12470(__this, ___handler, ___eventData, method) (( void (*) (EventFunction_1_t130 *, Object_t *, BaseEventData_t107 *, const MethodInfo*))EventFunction_1_Invoke_m11803_gshared)(__this, ___handler, ___eventData, method)
// System.IAsyncResult UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>::BeginInvoke(T1,UnityEngine.EventSystems.BaseEventData,System.AsyncCallback,System.Object)
#define EventFunction_1_BeginInvoke_m12471(__this, ___handler, ___eventData, ___callback, ___object, method) (( Object_t * (*) (EventFunction_1_t130 *, Object_t *, BaseEventData_t107 *, AsyncCallback_t229 *, Object_t *, const MethodInfo*))EventFunction_1_BeginInvoke_m11805_gshared)(__this, ___handler, ___eventData, ___callback, ___object, method)
// System.Void UnityEngine.EventSystems.ExecuteEvents/EventFunction`1<UnityEngine.EventSystems.IMoveHandler>::EndInvoke(System.IAsyncResult)
#define EventFunction_1_EndInvoke_m12472(__this, ___result, method) (( void (*) (EventFunction_1_t130 *, Object_t *, const MethodInfo*))EventFunction_1_EndInvoke_m11807_gshared)(__this, ___result, method)
