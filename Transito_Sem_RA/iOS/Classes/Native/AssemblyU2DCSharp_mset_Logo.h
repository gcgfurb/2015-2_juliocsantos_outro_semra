﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Texture2D
struct Texture2D_t82;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "AssemblyU2DCSharp_mset_Corner.h"
#include "UnityEngine_UnityEngine_Rect.h"

// mset.Logo
struct  Logo_t81  : public MonoBehaviour_t2
{
	// UnityEngine.Texture2D mset.Logo::logoTexture
	Texture2D_t82 * ___logoTexture_2;
	// UnityEngine.Color mset.Logo::color
	Color_t83  ___color_3;
	// UnityEngine.Vector2 mset.Logo::logoPixelOffset
	Vector2_t23  ___logoPixelOffset_4;
	// UnityEngine.Vector2 mset.Logo::logoPercentOffset
	Vector2_t23  ___logoPercentOffset_5;
	// mset.Corner mset.Logo::placement
	int32_t ___placement_6;
	// UnityEngine.Rect mset.Logo::texRect
	Rect_t84  ___texRect_7;
};
