﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t569;
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t570;
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t571;

#include "mscorlib_System_Object.h"

// UnityEngine.AttributeHelperEngine
struct  AttributeHelperEngine_t568  : public Object_t
{
};
struct AttributeHelperEngine_t568_StaticFields{
	// UnityEngine.DisallowMultipleComponent[] UnityEngine.AttributeHelperEngine::_disallowMultipleComponentArray
	DisallowMultipleComponentU5BU5D_t569* ____disallowMultipleComponentArray_0;
	// UnityEngine.ExecuteInEditMode[] UnityEngine.AttributeHelperEngine::_executeInEditModeArray
	ExecuteInEditModeU5BU5D_t570* ____executeInEditModeArray_1;
	// UnityEngine.RequireComponent[] UnityEngine.AttributeHelperEngine::_requireComponentArray
	RequireComponentU5BU5D_t571* ____requireComponentArray_2;
};
