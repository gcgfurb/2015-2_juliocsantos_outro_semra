﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IScore>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m15864(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2185 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10902_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IScore>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m15865(__this, method) (( void (*) (InternalEnumerator_1_t2185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m10904_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IScore>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m15866(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2185 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10906_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IScore>::Dispose()
#define InternalEnumerator_1_Dispose_m15867(__this, method) (( void (*) (InternalEnumerator_1_t2185 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10908_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IScore>::MoveNext()
#define InternalEnumerator_1_MoveNext_m15868(__this, method) (( bool (*) (InternalEnumerator_1_t2185 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10910_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.SocialPlatforms.IScore>::get_Current()
#define InternalEnumerator_1_get_Current_m15869(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2185 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10912_gshared)(__this, method)
