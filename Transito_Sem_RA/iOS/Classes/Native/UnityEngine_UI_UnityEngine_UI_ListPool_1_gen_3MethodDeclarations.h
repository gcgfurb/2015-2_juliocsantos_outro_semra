﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t314;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::.cctor()
extern "C" void ListPool_1__cctor_m15744_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m15744(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m15744_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Get()
extern "C" List_1_t314 * ListPool_1_Get_m2409_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m2409(__this /* static, unused */, method) (( List_1_t314 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m2409_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m2434_gshared (Object_t * __this /* static, unused */, List_1_t314 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m2434(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t314 *, const MethodInfo*))ListPool_1_Release_m2434_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.Vector2>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m15745_gshared (Object_t * __this /* static, unused */, List_1_t314 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__14_m15745(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t314 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__14_m15745_gshared)(__this /* static, unused */, ___l, method)
