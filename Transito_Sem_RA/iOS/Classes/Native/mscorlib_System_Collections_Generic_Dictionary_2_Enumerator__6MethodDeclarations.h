﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m13602(__this, ___dictionary, method) (( void (*) (Enumerator_t2015 *, Dictionary_2_t193 *, const MethodInfo*))Enumerator__ctor_m11050_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13603(__this, method) (( Object_t * (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m13604(__this, method) (( void (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11052_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13605(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11053_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13606(__this, method) (( Object_t * (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13607(__this, method) (( Object_t * (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::MoveNext()
#define Enumerator_MoveNext_m13608(__this, method) (( bool (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_MoveNext_m11056_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_Current()
#define Enumerator_get_Current_m13609(__this, method) (( KeyValuePair_2_t2013  (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_get_Current_m11057_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m13610(__this, method) (( Font_t191 * (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_get_CurrentKey_m11058_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m13611(__this, method) (( List_1_t378 * (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_get_CurrentValue_m11059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Reset()
#define Enumerator_Reset_m13612(__this, method) (( void (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_Reset_m11060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyState()
#define Enumerator_VerifyState_m13613(__this, method) (( void (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_VerifyState_m11061_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m13614(__this, method) (( void (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_VerifyCurrent_m11062_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Font,System.Collections.Generic.List`1<UnityEngine.UI.Text>>::Dispose()
#define Enumerator_Dispose_m13615(__this, method) (( void (*) (Enumerator_t2015 *, const MethodInfo*))Enumerator_Dispose_m11063_gshared)(__this, method)
