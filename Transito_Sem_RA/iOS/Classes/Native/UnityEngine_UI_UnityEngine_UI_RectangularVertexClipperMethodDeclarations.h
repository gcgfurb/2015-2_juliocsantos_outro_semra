﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.RectangularVertexClipper
struct RectangularVertexClipper_t250;
// UnityEngine.RectTransform
struct RectTransform_t95;
// UnityEngine.Canvas
struct Canvas_t197;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Rect.h"

// System.Void UnityEngine.UI.RectangularVertexClipper::.ctor()
extern "C" void RectangularVertexClipper__ctor_m1623 (RectangularVertexClipper_t250 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Rect UnityEngine.UI.RectangularVertexClipper::GetCanvasRect(UnityEngine.RectTransform,UnityEngine.Canvas)
extern "C" Rect_t84  RectangularVertexClipper_GetCanvasRect_m1624 (RectangularVertexClipper_t250 * __this, RectTransform_t95 * ___t, Canvas_t197 * ___c, const MethodInfo* method) IL2CPP_METHOD_ATTR;
