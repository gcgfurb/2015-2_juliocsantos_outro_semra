﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_46.h"
#include "UnityEngine_UnityEngine_Keyframe.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16239_gshared (InternalEnumerator_1_t2217 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16239(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2217 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16239_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16240_gshared (InternalEnumerator_1_t2217 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16240(__this, method) (( void (*) (InternalEnumerator_1_t2217 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16240_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16241_gshared (InternalEnumerator_1_t2217 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16241(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2217 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16241_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16242_gshared (InternalEnumerator_1_t2217 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16242(__this, method) (( void (*) (InternalEnumerator_1_t2217 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16242_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16243_gshared (InternalEnumerator_1_t2217 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16243(__this, method) (( bool (*) (InternalEnumerator_1_t2217 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16243_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Keyframe>::get_Current()
extern "C" Keyframe_t524  InternalEnumerator_1_get_Current_m16244_gshared (InternalEnumerator_1_t2217 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16244(__this, method) (( Keyframe_t524  (*) (InternalEnumerator_1_t2217 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16244_gshared)(__this, method)
