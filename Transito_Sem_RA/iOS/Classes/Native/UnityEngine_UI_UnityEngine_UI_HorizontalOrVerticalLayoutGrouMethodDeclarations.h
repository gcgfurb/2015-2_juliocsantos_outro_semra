﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.HorizontalOrVerticalLayoutGroup
struct HorizontalOrVerticalLayoutGroup_t300;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::.ctor()
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m1707 (HorizontalOrVerticalLayoutGroup_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_spacing()
extern "C" float HorizontalOrVerticalLayoutGroup_get_spacing_m1708 (HorizontalOrVerticalLayoutGroup_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_spacing(System.Single)
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m1709 (HorizontalOrVerticalLayoutGroup_t300 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandWidth()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1710 (HorizontalOrVerticalLayoutGroup_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandWidth(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1711 (HorizontalOrVerticalLayoutGroup_t300 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.HorizontalOrVerticalLayoutGroup::get_childForceExpandHeight()
extern "C" bool HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1712 (HorizontalOrVerticalLayoutGroup_t300 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::set_childForceExpandHeight(System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1713 (HorizontalOrVerticalLayoutGroup_t300 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::CalcAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1714 (HorizontalOrVerticalLayoutGroup_t300 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.HorizontalOrVerticalLayoutGroup::SetChildrenAlongAxis(System.Int32,System.Boolean)
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1715 (HorizontalOrVerticalLayoutGroup_t300 * __this, int32_t ___axis, bool ___isVertical, const MethodInfo* method) IL2CPP_METHOD_ATTR;
