﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m16005(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2198 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10902_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16006(__this, method) (( void (*) (InternalEnumerator_1_t2198 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m10904_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Camera>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16007(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2198 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10906_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Camera>::Dispose()
#define InternalEnumerator_1_Dispose_m16008(__this, method) (( void (*) (InternalEnumerator_1_t2198 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10908_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Camera>::MoveNext()
#define InternalEnumerator_1_MoveNext_m16009(__this, method) (( bool (*) (InternalEnumerator_1_t2198 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10910_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Camera>::get_Current()
#define InternalEnumerator_1_get_Current_m16010(__this, method) (( Camera_t74 * (*) (InternalEnumerator_1_t2198 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10912_gshared)(__this, method)
