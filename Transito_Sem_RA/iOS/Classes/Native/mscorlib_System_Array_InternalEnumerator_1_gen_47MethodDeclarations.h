﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_47.h"
#include "UnityEngine_UnityEngine_CharacterInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m16245_gshared (InternalEnumerator_1_t2218 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m16245(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2218 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m16245_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16246_gshared (InternalEnumerator_1_t2218 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16246(__this, method) (( void (*) (InternalEnumerator_1_t2218 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16246_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16247_gshared (InternalEnumerator_1_t2218 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16247(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2218 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16247_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m16248_gshared (InternalEnumerator_1_t2218 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m16248(__this, method) (( void (*) (InternalEnumerator_1_t2218 *, const MethodInfo*))InternalEnumerator_1_Dispose_m16248_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m16249_gshared (InternalEnumerator_1_t2218 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m16249(__this, method) (( bool (*) (InternalEnumerator_1_t2218 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m16249_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.CharacterInfo>::get_Current()
extern "C" CharacterInfo_t533  InternalEnumerator_1_get_Current_m16250_gshared (InternalEnumerator_1_t2218 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m16250(__this, method) (( CharacterInfo_t533  (*) (InternalEnumerator_1_t2218 *, const MethodInfo*))InternalEnumerator_1_get_Current_m16250_gshared)(__this, method)
