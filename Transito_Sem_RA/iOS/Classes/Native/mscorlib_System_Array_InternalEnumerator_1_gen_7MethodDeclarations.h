﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_7.h"
#include "UnityEngine_UnityEngine_Touch.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11222_gshared (InternalEnumerator_1_t1841 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11222(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1841 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11222_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11223_gshared (InternalEnumerator_1_t1841 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11223(__this, method) (( void (*) (InternalEnumerator_1_t1841 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11223_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Touch>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11224_gshared (InternalEnumerator_1_t1841 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11224(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1841 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11224_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Touch>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11225_gshared (InternalEnumerator_1_t1841 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11225(__this, method) (( void (*) (InternalEnumerator_1_t1841 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11225_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Touch>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11226_gshared (InternalEnumerator_1_t1841 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11226(__this, method) (( bool (*) (InternalEnumerator_1_t1841 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11226_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Touch>::get_Current()
extern "C" Touch_t72  InternalEnumerator_1_get_Current_m11227_gshared (InternalEnumerator_1_t1841 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11227(__this, method) (( Touch_t72  (*) (InternalEnumerator_1_t1841 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11227_gshared)(__this, method)
