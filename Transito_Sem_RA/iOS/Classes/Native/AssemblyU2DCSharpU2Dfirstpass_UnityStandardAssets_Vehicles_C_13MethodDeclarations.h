﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1
struct U3CStartSkidTrailU3Ec__Iterator1_t53;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::.ctor()
extern "C" void U3CStartSkidTrailU3Ec__Iterator1__ctor_m195 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartSkidTrailU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m196 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartSkidTrailU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m197 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::MoveNext()
extern "C" bool U3CStartSkidTrailU3Ec__Iterator1_MoveNext_m198 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::Dispose()
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_Dispose_m199 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.WheelEffects/<StartSkidTrail>c__Iterator1::Reset()
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_Reset_m200 (U3CStartSkidTrailU3Ec__Iterator1_t53 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
