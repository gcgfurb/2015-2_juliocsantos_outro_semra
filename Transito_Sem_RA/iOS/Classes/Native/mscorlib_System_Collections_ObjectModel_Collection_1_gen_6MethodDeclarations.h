﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>
struct Collection_1_t2152;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t425;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Vector4>
struct IEnumerator_1_t2481;
// System.Collections.Generic.IList`1<UnityEngine.Vector4>
struct IList_1_t2151;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Vector4.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::.ctor()
extern "C" void Collection_1__ctor_m15638_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15638(__this, method) (( void (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1__ctor_m15638_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15639_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15639(__this, method) (( bool (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15639_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15640_gshared (Collection_1_t2152 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15640(__this, ___array, ___index, method) (( void (*) (Collection_1_t2152 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15640_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15641_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15641(__this, method) (( Object_t * (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15641_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15642_gshared (Collection_1_t2152 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15642(__this, ___value, method) (( int32_t (*) (Collection_1_t2152 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15642_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15643_gshared (Collection_1_t2152 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15643(__this, ___value, method) (( bool (*) (Collection_1_t2152 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15643_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15644_gshared (Collection_1_t2152 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15644(__this, ___value, method) (( int32_t (*) (Collection_1_t2152 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15644_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15645_gshared (Collection_1_t2152 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15645(__this, ___index, ___value, method) (( void (*) (Collection_1_t2152 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15645_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15646_gshared (Collection_1_t2152 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15646(__this, ___value, method) (( void (*) (Collection_1_t2152 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15646_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15647_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15647(__this, method) (( bool (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15647_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15648_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15648(__this, method) (( Object_t * (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15648_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15649_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15649(__this, method) (( bool (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15649_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15650_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15650(__this, method) (( bool (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15650_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15651_gshared (Collection_1_t2152 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15651(__this, ___index, method) (( Object_t * (*) (Collection_1_t2152 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15651_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15652_gshared (Collection_1_t2152 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15652(__this, ___index, ___value, method) (( void (*) (Collection_1_t2152 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15652_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Add(T)
extern "C" void Collection_1_Add_m15653_gshared (Collection_1_t2152 * __this, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_Add_m15653(__this, ___item, method) (( void (*) (Collection_1_t2152 *, Vector4_t317 , const MethodInfo*))Collection_1_Add_m15653_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Clear()
extern "C" void Collection_1_Clear_m15654_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15654(__this, method) (( void (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_Clear_m15654_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ClearItems()
extern "C" void Collection_1_ClearItems_m15655_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15655(__this, method) (( void (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_ClearItems_m15655_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Contains(T)
extern "C" bool Collection_1_Contains_m15656_gshared (Collection_1_t2152 * __this, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_Contains_m15656(__this, ___item, method) (( bool (*) (Collection_1_t2152 *, Vector4_t317 , const MethodInfo*))Collection_1_Contains_m15656_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15657_gshared (Collection_1_t2152 * __this, Vector4U5BU5D_t425* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15657(__this, ___array, ___index, method) (( void (*) (Collection_1_t2152 *, Vector4U5BU5D_t425*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15657_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15658_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15658(__this, method) (( Object_t* (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_GetEnumerator_m15658_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15659_gshared (Collection_1_t2152 * __this, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15659(__this, ___item, method) (( int32_t (*) (Collection_1_t2152 *, Vector4_t317 , const MethodInfo*))Collection_1_IndexOf_m15659_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15660_gshared (Collection_1_t2152 * __this, int32_t ___index, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_Insert_m15660(__this, ___index, ___item, method) (( void (*) (Collection_1_t2152 *, int32_t, Vector4_t317 , const MethodInfo*))Collection_1_Insert_m15660_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15661_gshared (Collection_1_t2152 * __this, int32_t ___index, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15661(__this, ___index, ___item, method) (( void (*) (Collection_1_t2152 *, int32_t, Vector4_t317 , const MethodInfo*))Collection_1_InsertItem_m15661_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::Remove(T)
extern "C" bool Collection_1_Remove_m15662_gshared (Collection_1_t2152 * __this, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_Remove_m15662(__this, ___item, method) (( bool (*) (Collection_1_t2152 *, Vector4_t317 , const MethodInfo*))Collection_1_Remove_m15662_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15663_gshared (Collection_1_t2152 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15663(__this, ___index, method) (( void (*) (Collection_1_t2152 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15663_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15664_gshared (Collection_1_t2152 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15664(__this, ___index, method) (( void (*) (Collection_1_t2152 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15664_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15665_gshared (Collection_1_t2152 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15665(__this, method) (( int32_t (*) (Collection_1_t2152 *, const MethodInfo*))Collection_1_get_Count_m15665_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::get_Item(System.Int32)
extern "C" Vector4_t317  Collection_1_get_Item_m15666_gshared (Collection_1_t2152 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15666(__this, ___index, method) (( Vector4_t317  (*) (Collection_1_t2152 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15666_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15667_gshared (Collection_1_t2152 * __this, int32_t ___index, Vector4_t317  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15667(__this, ___index, ___value, method) (( void (*) (Collection_1_t2152 *, int32_t, Vector4_t317 , const MethodInfo*))Collection_1_set_Item_m15667_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15668_gshared (Collection_1_t2152 * __this, int32_t ___index, Vector4_t317  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15668(__this, ___index, ___item, method) (( void (*) (Collection_1_t2152 *, int32_t, Vector4_t317 , const MethodInfo*))Collection_1_SetItem_m15668_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15669_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15669(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15669_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::ConvertItem(System.Object)
extern "C" Vector4_t317  Collection_1_ConvertItem_m15670_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15670(__this /* static, unused */, ___item, method) (( Vector4_t317  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15670_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15671_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15671(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15671_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15672_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15672(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15672_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Vector4>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15673_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15673(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15673_gshared)(__this /* static, unused */, ___list, method)
