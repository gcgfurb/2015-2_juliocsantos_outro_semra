﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Object>
struct Dictionary_2_t1814;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m11050_gshared (Enumerator_t1824 * __this, Dictionary_2_t1814 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m11050(__this, ___dictionary, method) (( void (*) (Enumerator_t1824 *, Dictionary_2_t1814 *, const MethodInfo*))Enumerator__ctor_m11050_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m11051_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m11051(__this, method) (( Object_t * (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m11052_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m11052(__this, method) (( void (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11052_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1057  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11053_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11053(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11053_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11054_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11054(__this, method) (( Object_t * (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11055_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11055(__this, method) (( Object_t * (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::MoveNext()
extern "C" bool Enumerator_MoveNext_m11056_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m11056(__this, method) (( bool (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_MoveNext_m11056_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_Current()
extern "C" KeyValuePair_2_t1817  Enumerator_get_Current_m11057_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m11057(__this, method) (( KeyValuePair_2_t1817  (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_get_Current_m11057_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m11058_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m11058(__this, method) (( Object_t * (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_get_CurrentKey_m11058_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::get_CurrentValue()
extern "C" Object_t * Enumerator_get_CurrentValue_m11059_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m11059(__this, method) (( Object_t * (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_get_CurrentValue_m11059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Reset()
extern "C" void Enumerator_Reset_m11060_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_Reset_m11060(__this, method) (( void (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_Reset_m11060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyState()
extern "C" void Enumerator_VerifyState_m11061_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m11061(__this, method) (( void (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_VerifyState_m11061_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m11062_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m11062(__this, method) (( void (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_VerifyCurrent_m11062_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Object>::Dispose()
extern "C" void Enumerator_Dispose_m11063_gshared (Enumerator_t1824 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m11063(__this, method) (( void (*) (Enumerator_t1824 *, const MethodInfo*))Enumerator_Dispose_m11063_gshared)(__this, method)
