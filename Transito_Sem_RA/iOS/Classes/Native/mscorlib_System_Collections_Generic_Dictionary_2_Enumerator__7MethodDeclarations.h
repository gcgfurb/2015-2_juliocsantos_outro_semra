﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__0MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m13994(__this, ___dictionary, method) (( void (*) (Enumerator_t2039 *, Dictionary_2_t207 *, const MethodInfo*))Enumerator__ctor_m11050_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m13995(__this, method) (( Object_t * (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11051_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m13996(__this, method) (( void (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11052_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m13997(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m11053_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m13998(__this, method) (( Object_t * (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m11054_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m13999(__this, method) (( Object_t * (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m11055_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::MoveNext()
#define Enumerator_MoveNext_m14000(__this, method) (( bool (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_MoveNext_m11056_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Current()
#define Enumerator_get_Current_m14001(__this, method) (( KeyValuePair_2_t2037  (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_get_Current_m11057_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m14002(__this, method) (( Canvas_t197 * (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_get_CurrentKey_m11058_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m14003(__this, method) (( IndexedSet_1_t388 * (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_get_CurrentValue_m11059_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Reset()
#define Enumerator_Reset_m14004(__this, method) (( void (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_Reset_m11060_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyState()
#define Enumerator_VerifyState_m14005(__this, method) (( void (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_VerifyState_m11061_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m14006(__this, method) (( void (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_VerifyCurrent_m11062_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::Dispose()
#define Enumerator_Dispose_m14007(__this, method) (( void (*) (Enumerator_t2039 *, const MethodInfo*))Enumerator_Dispose_m11063_gshared)(__this, method)
