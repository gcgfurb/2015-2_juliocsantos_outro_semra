﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m16011(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2199 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10902_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m16012(__this, method) (( void (*) (InternalEnumerator_1_t2199 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m10904_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Display>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m16013(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2199 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10906_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Display>::Dispose()
#define InternalEnumerator_1_Dispose_m16014(__this, method) (( void (*) (InternalEnumerator_1_t2199 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10908_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Display>::MoveNext()
#define InternalEnumerator_1_MoveNext_m16015(__this, method) (( bool (*) (InternalEnumerator_1_t2199 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10910_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Display>::get_Current()
#define InternalEnumerator_1_get_Current_m16016(__this, method) (( Display_t490 * (*) (InternalEnumerator_1_t2199 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10912_gshared)(__this, method)
