﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>
struct Collection_1_t2387;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Reflection.CustomAttributeNamedArgument[]
struct CustomAttributeNamedArgumentU5BU5D_t1767;
// System.Collections.Generic.IEnumerator`1<System.Reflection.CustomAttributeNamedArgument>
struct IEnumerator_1_t2510;
// System.Collections.Generic.IList`1<System.Reflection.CustomAttributeNamedArgument>
struct IList_1_t1324;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::.ctor()
extern "C" void Collection_1__ctor_m18078_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1__ctor_m18078(__this, method) (( void (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1__ctor_m18078_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18079_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18079(__this, method) (( bool (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m18079_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m18080_gshared (Collection_1_t2387 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m18080(__this, ___array, ___index, method) (( void (*) (Collection_1_t2387 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m18080_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m18081_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m18081(__this, method) (( Object_t * (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m18081_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m18082_gshared (Collection_1_t2387 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m18082(__this, ___value, method) (( int32_t (*) (Collection_1_t2387 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m18082_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m18083_gshared (Collection_1_t2387 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m18083(__this, ___value, method) (( bool (*) (Collection_1_t2387 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m18083_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m18084_gshared (Collection_1_t2387 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m18084(__this, ___value, method) (( int32_t (*) (Collection_1_t2387 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m18084_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m18085_gshared (Collection_1_t2387 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m18085(__this, ___index, ___value, method) (( void (*) (Collection_1_t2387 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m18085_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m18086_gshared (Collection_1_t2387 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m18086(__this, ___value, method) (( void (*) (Collection_1_t2387 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m18086_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m18087_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m18087(__this, method) (( bool (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m18087_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m18088_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m18088(__this, method) (( Object_t * (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m18088_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m18089_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m18089(__this, method) (( bool (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m18089_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m18090_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m18090(__this, method) (( bool (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m18090_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m18091_gshared (Collection_1_t2387 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m18091(__this, ___index, method) (( Object_t * (*) (Collection_1_t2387 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m18091_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m18092_gshared (Collection_1_t2387 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m18092(__this, ___index, ___value, method) (( void (*) (Collection_1_t2387 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m18092_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Add(T)
extern "C" void Collection_1_Add_m18093_gshared (Collection_1_t2387 * __this, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_Add_m18093(__this, ___item, method) (( void (*) (Collection_1_t2387 *, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_Add_m18093_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Clear()
extern "C" void Collection_1_Clear_m18094_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_Clear_m18094(__this, method) (( void (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_Clear_m18094_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ClearItems()
extern "C" void Collection_1_ClearItems_m18095_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m18095(__this, method) (( void (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_ClearItems_m18095_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Contains(T)
extern "C" bool Collection_1_Contains_m18096_gshared (Collection_1_t2387 * __this, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_Contains_m18096(__this, ___item, method) (( bool (*) (Collection_1_t2387 *, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_Contains_m18096_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m18097_gshared (Collection_1_t2387 * __this, CustomAttributeNamedArgumentU5BU5D_t1767* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m18097(__this, ___array, ___index, method) (( void (*) (Collection_1_t2387 *, CustomAttributeNamedArgumentU5BU5D_t1767*, int32_t, const MethodInfo*))Collection_1_CopyTo_m18097_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m18098_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m18098(__this, method) (( Object_t* (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_GetEnumerator_m18098_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m18099_gshared (Collection_1_t2387 * __this, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m18099(__this, ___item, method) (( int32_t (*) (Collection_1_t2387 *, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_IndexOf_m18099_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m18100_gshared (Collection_1_t2387 * __this, int32_t ___index, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_Insert_m18100(__this, ___index, ___item, method) (( void (*) (Collection_1_t2387 *, int32_t, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_Insert_m18100_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m18101_gshared (Collection_1_t2387 * __this, int32_t ___index, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m18101(__this, ___index, ___item, method) (( void (*) (Collection_1_t2387 *, int32_t, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_InsertItem_m18101_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::Remove(T)
extern "C" bool Collection_1_Remove_m18102_gshared (Collection_1_t2387 * __this, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_Remove_m18102(__this, ___item, method) (( bool (*) (Collection_1_t2387 *, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_Remove_m18102_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m18103_gshared (Collection_1_t2387 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m18103(__this, ___index, method) (( void (*) (Collection_1_t2387 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m18103_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m18104_gshared (Collection_1_t2387 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m18104(__this, ___index, method) (( void (*) (Collection_1_t2387 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m18104_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Count()
extern "C" int32_t Collection_1_get_Count_m18105_gshared (Collection_1_t2387 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m18105(__this, method) (( int32_t (*) (Collection_1_t2387 *, const MethodInfo*))Collection_1_get_Count_m18105_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::get_Item(System.Int32)
extern "C" CustomAttributeNamedArgument_t1325  Collection_1_get_Item_m18106_gshared (Collection_1_t2387 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m18106(__this, ___index, method) (( CustomAttributeNamedArgument_t1325  (*) (Collection_1_t2387 *, int32_t, const MethodInfo*))Collection_1_get_Item_m18106_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m18107_gshared (Collection_1_t2387 * __this, int32_t ___index, CustomAttributeNamedArgument_t1325  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m18107(__this, ___index, ___value, method) (( void (*) (Collection_1_t2387 *, int32_t, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_set_Item_m18107_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m18108_gshared (Collection_1_t2387 * __this, int32_t ___index, CustomAttributeNamedArgument_t1325  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m18108(__this, ___index, ___item, method) (( void (*) (Collection_1_t2387 *, int32_t, CustomAttributeNamedArgument_t1325 , const MethodInfo*))Collection_1_SetItem_m18108_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m18109_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m18109(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m18109_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::ConvertItem(System.Object)
extern "C" CustomAttributeNamedArgument_t1325  Collection_1_ConvertItem_m18110_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m18110(__this /* static, unused */, ___item, method) (( CustomAttributeNamedArgument_t1325  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m18110_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m18111_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m18111(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m18111_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m18112_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m18112(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m18112_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<System.Reflection.CustomAttributeNamedArgument>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m18113_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m18113(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m18113_gshared)(__this /* static, unused */, ___list, method)
