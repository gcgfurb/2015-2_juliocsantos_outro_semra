﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_Enumerator_genMethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::.ctor(System.Collections.Generic.Stack`1<T>)
#define Enumerator__ctor_m16831(__this, ___t, method) (( void (*) (Enumerator_t2255 *, Stack_1_t670 *, const MethodInfo*))Enumerator__ctor_m11926_gshared)(__this, ___t, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16832(__this, method) (( void (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11927_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1/Enumerator<System.Type>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16833(__this, method) (( Object_t * (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11928_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1/Enumerator<System.Type>::Dispose()
#define Enumerator_Dispose_m16834(__this, method) (( void (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_Dispose_m11929_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1/Enumerator<System.Type>::MoveNext()
#define Enumerator_MoveNext_m16835(__this, method) (( bool (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_MoveNext_m11930_gshared)(__this, method)
// T System.Collections.Generic.Stack`1/Enumerator<System.Type>::get_Current()
#define Enumerator_get_Current_m16836(__this, method) (( Type_t * (*) (Enumerator_t2255 *, const MethodInfo*))Enumerator_get_Current_m11931_gshared)(__this, method)
