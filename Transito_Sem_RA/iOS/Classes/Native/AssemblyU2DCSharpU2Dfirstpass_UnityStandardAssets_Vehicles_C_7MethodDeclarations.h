﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.CarSelfRighting
struct CarSelfRighting_t46;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::.ctor()
extern "C" void CarSelfRighting__ctor_m173 (CarSelfRighting_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Start()
extern "C" void CarSelfRighting_Start_m174 (CarSelfRighting_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::Update()
extern "C" void CarSelfRighting_Update_m175 (CarSelfRighting_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarSelfRighting::RightCar()
extern "C" void CarSelfRighting_RightCar_m176 (CarSelfRighting_t46 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
