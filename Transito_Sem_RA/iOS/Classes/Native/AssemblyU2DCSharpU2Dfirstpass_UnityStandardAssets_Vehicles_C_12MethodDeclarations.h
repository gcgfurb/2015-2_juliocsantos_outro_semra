﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.Suspension
struct Suspension_t51;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.Suspension::.ctor()
extern "C" void Suspension__ctor_m192 (Suspension_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.Suspension::Start()
extern "C" void Suspension_Start_m193 (Suspension_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.Suspension::Update()
extern "C" void Suspension_Update_m194 (Suspension_t51 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
