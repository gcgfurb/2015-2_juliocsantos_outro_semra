﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_11MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m12847(__this, ___host, method) (( void (*) (Enumerator_t361 *, Dictionary_2_t151 *, const MethodInfo*))Enumerator__ctor_m12776_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m12848(__this, method) (( Object_t * (*) (Enumerator_t361 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12777_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m12849(__this, method) (( void (*) (Enumerator_t361 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12778_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m12850(__this, method) (( void (*) (Enumerator_t361 *, const MethodInfo*))Enumerator_Dispose_m12779_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m1926(__this, method) (( bool (*) (Enumerator_t361 *, const MethodInfo*))Enumerator_MoveNext_m12780_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m1925(__this, method) (( PointerEventData_t57 * (*) (Enumerator_t361 *, const MethodInfo*))Enumerator_get_Current_m12781_gshared)(__this, method)
