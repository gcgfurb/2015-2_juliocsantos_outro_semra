﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>
struct DefaultComparer_t2383;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Reflection_CustomAttributeTypedArgument.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::.ctor()
extern "C" void DefaultComparer__ctor_m18020_gshared (DefaultComparer_t2383 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m18020(__this, method) (( void (*) (DefaultComparer_t2383 *, const MethodInfo*))DefaultComparer__ctor_m18020_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<System.Reflection.CustomAttributeTypedArgument>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m18021_gshared (DefaultComparer_t2383 * __this, CustomAttributeTypedArgument_t1326  ___x, CustomAttributeTypedArgument_t1326  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m18021(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2383 *, CustomAttributeTypedArgument_t1326 , CustomAttributeTypedArgument_t1326 , const MethodInfo*))DefaultComparer_Compare_m18021_gshared)(__this, ___x, ___y, method)
