﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.CarAIControl
struct CarAIControl_t32;
// UnityEngine.Collision
struct Collision_t58;
// UnityEngine.Transform
struct Transform_t33;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::.ctor()
extern "C" void CarAIControl__ctor_m134 (CarAIControl_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::Awake()
extern "C" void CarAIControl_Awake_m135 (CarAIControl_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::FixedUpdate()
extern "C" void CarAIControl_FixedUpdate_m136 (CarAIControl_t32 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::OnCollisionStay(UnityEngine.Collision)
extern "C" void CarAIControl_OnCollisionStay_m137 (CarAIControl_t32 * __this, Collision_t58 * ___col, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarAIControl::SetTarget(UnityEngine.Transform)
extern "C" void CarAIControl_SetTarget_m138 (CarAIControl_t32 * __this, Transform_t33 * ___target, const MethodInfo* method) IL2CPP_METHOD_ATTR;
