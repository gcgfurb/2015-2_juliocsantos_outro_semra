﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void AxisTouchButton__ctor_m0 ();
extern "C" void AxisTouchButton_OnEnable_m1 ();
extern "C" void AxisTouchButton_FindPairedButton_m2 ();
extern "C" void AxisTouchButton_OnDisable_m3 ();
extern "C" void AxisTouchButton_OnPointerDown_m4 ();
extern "C" void AxisTouchButton_OnPointerUp_m5 ();
extern "C" void ButtonHandler__ctor_m6 ();
extern "C" void ButtonHandler_OnEnable_m7 ();
extern "C" void ButtonHandler_SetDownState_m8 ();
extern "C" void ButtonHandler_SetUpState_m9 ();
extern "C" void ButtonHandler_SetAxisPositiveState_m10 ();
extern "C" void ButtonHandler_SetAxisNeutralState_m11 ();
extern "C" void ButtonHandler_SetAxisNegativeState_m12 ();
extern "C" void ButtonHandler_Update_m13 ();
extern "C" void VirtualAxis__ctor_m14 ();
extern "C" void VirtualAxis__ctor_m15 ();
extern "C" void VirtualAxis_get_name_m16 ();
extern "C" void VirtualAxis_set_name_m17 ();
extern "C" void VirtualAxis_get_matchWithInputManager_m18 ();
extern "C" void VirtualAxis_set_matchWithInputManager_m19 ();
extern "C" void VirtualAxis_Remove_m20 ();
extern "C" void VirtualAxis_Update_m21 ();
extern "C" void VirtualAxis_get_GetValue_m22 ();
extern "C" void VirtualAxis_get_GetValueRaw_m23 ();
extern "C" void VirtualButton__ctor_m24 ();
extern "C" void VirtualButton__ctor_m25 ();
extern "C" void VirtualButton_get_name_m26 ();
extern "C" void VirtualButton_set_name_m27 ();
extern "C" void VirtualButton_get_matchWithInputManager_m28 ();
extern "C" void VirtualButton_set_matchWithInputManager_m29 ();
extern "C" void VirtualButton_Pressed_m30 ();
extern "C" void VirtualButton_Released_m31 ();
extern "C" void VirtualButton_Remove_m32 ();
extern "C" void VirtualButton_get_GetButton_m33 ();
extern "C" void VirtualButton_get_GetButtonDown_m34 ();
extern "C" void VirtualButton_get_GetButtonUp_m35 ();
extern "C" void CrossPlatformInputManager__cctor_m36 ();
extern "C" void CrossPlatformInputManager_SwitchActiveInputMethod_m37 ();
extern "C" void CrossPlatformInputManager_AxisExists_m38 ();
extern "C" void CrossPlatformInputManager_ButtonExists_m39 ();
extern "C" void CrossPlatformInputManager_RegisterVirtualAxis_m40 ();
extern "C" void CrossPlatformInputManager_RegisterVirtualButton_m41 ();
extern "C" void CrossPlatformInputManager_UnRegisterVirtualAxis_m42 ();
extern "C" void CrossPlatformInputManager_UnRegisterVirtualButton_m43 ();
extern "C" void CrossPlatformInputManager_VirtualAxisReference_m44 ();
extern "C" void CrossPlatformInputManager_GetAxis_m45 ();
extern "C" void CrossPlatformInputManager_GetAxisRaw_m46 ();
extern "C" void CrossPlatformInputManager_GetAxis_m47 ();
extern "C" void CrossPlatformInputManager_GetButton_m48 ();
extern "C" void CrossPlatformInputManager_GetButtonDown_m49 ();
extern "C" void CrossPlatformInputManager_GetButtonUp_m50 ();
extern "C" void CrossPlatformInputManager_SetButtonDown_m51 ();
extern "C" void CrossPlatformInputManager_SetButtonUp_m52 ();
extern "C" void CrossPlatformInputManager_SetAxisPositive_m53 ();
extern "C" void CrossPlatformInputManager_SetAxisNegative_m54 ();
extern "C" void CrossPlatformInputManager_SetAxisZero_m55 ();
extern "C" void CrossPlatformInputManager_SetAxis_m56 ();
extern "C" void CrossPlatformInputManager_get_mousePosition_m57 ();
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionX_m58 ();
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionY_m59 ();
extern "C" void CrossPlatformInputManager_SetVirtualMousePositionZ_m60 ();
extern "C" void InputAxisScrollbar__ctor_m61 ();
extern "C" void InputAxisScrollbar_Update_m62 ();
extern "C" void InputAxisScrollbar_HandleInput_m63 ();
extern "C" void Joystick__ctor_m64 ();
extern "C" void Joystick_OnEnable_m65 ();
extern "C" void Joystick_Start_m66 ();
extern "C" void Joystick_UpdateVirtualAxes_m67 ();
extern "C" void Joystick_CreateVirtualAxes_m68 ();
extern "C" void Joystick_OnDrag_m69 ();
extern "C" void Joystick_OnPointerUp_m70 ();
extern "C" void Joystick_OnPointerDown_m71 ();
extern "C" void Joystick_OnDisable_m72 ();
extern "C" void MobileControlRig__ctor_m73 ();
extern "C" void MobileControlRig_OnEnable_m74 ();
extern "C" void MobileControlRig_Start_m75 ();
extern "C" void MobileControlRig_CheckEnableControlRig_m76 ();
extern "C" void MobileControlRig_EnableControlRig_m77 ();
extern "C" void MobileInput__ctor_m78 ();
extern "C" void MobileInput_AddButton_m79 ();
extern "C" void MobileInput_AddAxes_m80 ();
extern "C" void MobileInput_GetAxis_m81 ();
extern "C" void MobileInput_SetButtonDown_m82 ();
extern "C" void MobileInput_SetButtonUp_m83 ();
extern "C" void MobileInput_SetAxisPositive_m84 ();
extern "C" void MobileInput_SetAxisNegative_m85 ();
extern "C" void MobileInput_SetAxisZero_m86 ();
extern "C" void MobileInput_SetAxis_m87 ();
extern "C" void MobileInput_GetButtonDown_m88 ();
extern "C" void MobileInput_GetButtonUp_m89 ();
extern "C" void MobileInput_GetButton_m90 ();
extern "C" void MobileInput_MousePosition_m91 ();
extern "C" void StandaloneInput__ctor_m92 ();
extern "C" void StandaloneInput_GetAxis_m93 ();
extern "C" void StandaloneInput_GetButton_m94 ();
extern "C" void StandaloneInput_GetButtonDown_m95 ();
extern "C" void StandaloneInput_GetButtonUp_m96 ();
extern "C" void StandaloneInput_SetButtonDown_m97 ();
extern "C" void StandaloneInput_SetButtonUp_m98 ();
extern "C" void StandaloneInput_SetAxisPositive_m99 ();
extern "C" void StandaloneInput_SetAxisNegative_m100 ();
extern "C" void StandaloneInput_SetAxisZero_m101 ();
extern "C" void StandaloneInput_SetAxis_m102 ();
extern "C" void StandaloneInput_MousePosition_m103 ();
extern "C" void AxisMapping__ctor_m104 ();
extern "C" void TiltInput__ctor_m105 ();
extern "C" void TiltInput_OnEnable_m106 ();
extern "C" void TiltInput_Update_m107 ();
extern "C" void TiltInput_OnDisable_m108 ();
extern "C" void TouchPad__ctor_m109 ();
extern "C" void TouchPad_OnEnable_m110 ();
extern "C" void TouchPad_Start_m111 ();
extern "C" void TouchPad_CreateVirtualAxes_m112 ();
extern "C" void TouchPad_UpdateVirtualAxes_m113 ();
extern "C" void TouchPad_OnPointerDown_m114 ();
extern "C" void TouchPad_Update_m115 ();
extern "C" void TouchPad_OnPointerUp_m116 ();
extern "C" void TouchPad_OnDisable_m117 ();
extern "C" void VirtualInput__ctor_m118 ();
extern "C" void VirtualInput_get_virtualMousePosition_m119 ();
extern "C" void VirtualInput_set_virtualMousePosition_m120 ();
extern "C" void VirtualInput_AxisExists_m121 ();
extern "C" void VirtualInput_ButtonExists_m122 ();
extern "C" void VirtualInput_RegisterVirtualAxis_m123 ();
extern "C" void VirtualInput_RegisterVirtualButton_m124 ();
extern "C" void VirtualInput_UnRegisterVirtualAxis_m125 ();
extern "C" void VirtualInput_UnRegisterVirtualButton_m126 ();
extern "C" void VirtualInput_VirtualAxisReference_m127 ();
extern "C" void VirtualInput_SetVirtualMousePositionX_m128 ();
extern "C" void VirtualInput_SetVirtualMousePositionY_m129 ();
extern "C" void VirtualInput_SetVirtualMousePositionZ_m130 ();
extern "C" void BrakeLight__ctor_m131 ();
extern "C" void BrakeLight_Start_m132 ();
extern "C" void BrakeLight_Update_m133 ();
extern "C" void CarAIControl__ctor_m134 ();
extern "C" void CarAIControl_Awake_m135 ();
extern "C" void CarAIControl_FixedUpdate_m136 ();
extern "C" void CarAIControl_OnCollisionStay_m137 ();
extern "C" void CarAIControl_SetTarget_m138 ();
extern "C" void CarAudio__ctor_m139 ();
extern "C" void CarAudio_StartSound_m140 ();
extern "C" void CarAudio_StopSound_m141 ();
extern "C" void CarAudio_Update_m142 ();
extern "C" void CarAudio_SetUpEngineAudioSource_m143 ();
extern "C" void CarAudio_ULerp_m144 ();
extern "C" void CarController__ctor_m145 ();
extern "C" void CarController__cctor_m146 ();
extern "C" void CarController_get_Skidding_m147 ();
extern "C" void CarController_set_Skidding_m148 ();
extern "C" void CarController_get_BrakeInput_m149 ();
extern "C" void CarController_set_BrakeInput_m150 ();
extern "C" void CarController_get_CurrentSteerAngle_m151 ();
extern "C" void CarController_get_CurrentSpeed_m152 ();
extern "C" void CarController_get_MaxSpeed_m153 ();
extern "C" void CarController_get_Revs_m154 ();
extern "C" void CarController_set_Revs_m155 ();
extern "C" void CarController_get_AccelInput_m156 ();
extern "C" void CarController_set_AccelInput_m157 ();
extern "C" void CarController_Start_m158 ();
extern "C" void CarController_GearChanging_m159 ();
extern "C" void CarController_CurveFactor_m160 ();
extern "C" void CarController_ULerp_m161 ();
extern "C" void CarController_CalculateGearFactor_m162 ();
extern "C" void CarController_CalculateRevs_m163 ();
extern "C" void CarController_Move_m164 ();
extern "C" void CarController_CapSpeed_m165 ();
extern "C" void CarController_ApplyDrive_m166 ();
extern "C" void CarController_SteerHelper_m167 ();
extern "C" void CarController_AddDownForce_m168 ();
extern "C" void CarController_CheckForWheelSpin_m169 ();
extern "C" void CarController_TractionControl_m170 ();
extern "C" void CarController_AdjustTorque_m171 ();
extern "C" void CarController_AnySkidSoundPlaying_m172 ();
extern "C" void CarSelfRighting__ctor_m173 ();
extern "C" void CarSelfRighting_Start_m174 ();
extern "C" void CarSelfRighting_Update_m175 ();
extern "C" void CarSelfRighting_RightCar_m176 ();
extern "C" void CarUserControl__ctor_m177 ();
extern "C" void CarUserControl_Start_m178 ();
extern "C" void CarUserControl_Awake_m179 ();
extern "C" void CarUserControl_FixedUpdate_m180 ();
extern "C" void Mudguard__ctor_m181 ();
extern "C" void Mudguard_Start_m182 ();
extern "C" void Mudguard_Update_m183 ();
extern "C" void U3CStartU3Ec__Iterator0__ctor_m184 ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m185 ();
extern "C" void U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m186 ();
extern "C" void U3CStartU3Ec__Iterator0_MoveNext_m187 ();
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m188 ();
extern "C" void U3CStartU3Ec__Iterator0_Reset_m189 ();
extern "C" void SkidTrail__ctor_m190 ();
extern "C" void SkidTrail_Start_m191 ();
extern "C" void Suspension__ctor_m192 ();
extern "C" void Suspension_Start_m193 ();
extern "C" void Suspension_Update_m194 ();
extern "C" void U3CStartSkidTrailU3Ec__Iterator1__ctor_m195 ();
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m196 ();
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m197 ();
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_MoveNext_m198 ();
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_Dispose_m199 ();
extern "C" void U3CStartSkidTrailU3Ec__Iterator1_Reset_m200 ();
extern "C" void WheelEffects__ctor_m201 ();
extern "C" void WheelEffects_get_skidding_m202 ();
extern "C" void WheelEffects_set_skidding_m203 ();
extern "C" void WheelEffects_get_PlayingAudio_m204 ();
extern "C" void WheelEffects_set_PlayingAudio_m205 ();
extern "C" void WheelEffects_Start_m206 ();
extern "C" void WheelEffects_EmitTyreSmoke_m207 ();
extern "C" void WheelEffects_PlayAudio_m208 ();
extern "C" void WheelEffects_StopAudio_m209 ();
extern "C" void WheelEffects_StartSkidTrail_m210 ();
extern "C" void WheelEffects_EndSkidTrail_m211 ();
extern "C" void Logo__ctor_m354 ();
extern "C" void Logo_Reset_m355 ();
extern "C" void Logo_Start_m356 ();
extern "C" void Logo_updateTexRect_m357 ();
extern "C" void Logo_OnGUI_m358 ();
extern "C" void OrbitCamera__ctor_m359 ();
extern "C" void OrbitCamera_Start_m360 ();
extern "C" void OrbitCamera_Update_m361 ();
extern "C" void OrbitCamera_LateUpdate_m362 ();
extern "C" void OrbitCamera_ClampAngle_m363 ();
extern "C" void AtualizaVida__ctor_m364 ();
extern "C" void AtualizaVida_Start_m365 ();
extern "C" void AtualizaVida_Update_m366 ();
extern "C" void AtualizaVida_OnCollisionEnter_m367 ();
extern "C" void AtualizaVida_OnTriggerStay_m368 ();
extern "C" void CameraControl__ctor_m369 ();
extern "C" void CameraControl_Awake_m370 ();
extern "C" void CameraControl_FixedUpdate_m371 ();
extern "C" void CameraControl_Move_m372 ();
extern "C" void CameraControl_FindAveragePosition_m373 ();
extern "C" void CameraControl_Zoom_m374 ();
extern "C" void CameraControl_FindRequiredSize_m375 ();
extern "C" void CameraControl_SetStartPositionAndSize_m376 ();
extern "C" void U3CArrumarU3Ec__Iterator0__ctor_m377 ();
extern "C" void U3CArrumarU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m378 ();
extern "C" void U3CArrumarU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m379 ();
extern "C" void U3CArrumarU3Ec__Iterator0_MoveNext_m380 ();
extern "C" void U3CArrumarU3Ec__Iterator0_Dispose_m381 ();
extern "C" void U3CArrumarU3Ec__Iterator0_Reset_m382 ();
extern "C" void U3CAbastecerU3Ec__Iterator1__ctor_m383 ();
extern "C" void U3CAbastecerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m384 ();
extern "C" void U3CAbastecerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m385 ();
extern "C" void U3CAbastecerU3Ec__Iterator1_MoveNext_m386 ();
extern "C" void U3CAbastecerU3Ec__Iterator1_Dispose_m387 ();
extern "C" void U3CAbastecerU3Ec__Iterator1_Reset_m388 ();
extern "C" void Car_Health__ctor_m389 ();
extern "C" void Car_Health_GetCombustivelAtual_m390 ();
extern "C" void Car_Health_GetMecanicaAtual_m391 ();
extern "C" void Car_Health_IsMorto_m392 ();
extern "C" void Car_Health_IsCombustivelBaixo_m393 ();
extern "C" void Car_Health_IsMecanicaBaixa_m394 ();
extern "C" void Car_Health_Start_m395 ();
extern "C" void Car_Health_OnEnable_m396 ();
extern "C" void Car_Health_Update_m397 ();
extern "C" void Car_Health_GastaMecanica_m398 ();
extern "C" void Car_Health_AddMecanica_m399 ();
extern "C" void Car_Health_Arrumar_m400 ();
extern "C" void Car_Health_GastaCombustivel_m401 ();
extern "C" void Car_Health_AddCombustivel_m402 ();
extern "C" void Car_Health_Abastecer_m403 ();
extern "C" void Car_Health_SetHealthUI_m404 ();
extern "C" void Car_Health_OnDeath_m405 ();
extern "C" void Funcoes_Botoes__ctor_m406 ();
extern "C" void Funcoes_Botoes_Start_m407 ();
extern "C" void Funcoes_Botoes_Update_m408 ();
extern "C" void Funcoes_Botoes_Reiniciar_m409 ();
extern "C" void Funcoes_Botoes_Sair_m410 ();
extern "C" void Funcoes_Botoes_VoltarInicio_m411 ();
extern "C" void Funcoes_Botoes_Pausar_m412 ();
extern "C" void Principal__ctor_m413 ();
extern "C" void Principal_Start_m414 ();
extern "C" void Principal_Update_m415 ();
extern "C" void Principal_CombustivelBaixo_m416 ();
extern "C" void Principal_MecanicaBaixa_m417 ();
extern "C" void Principal_ExibirMesagemPadrao_m418 ();
extern "C" void Principal_ExibirMesagemAlerta_m419 ();
extern "C" void Principal_ExibirMesagemPerigo_m420 ();
extern "C" void EventSystem__ctor_m458 ();
extern "C" void EventSystem__cctor_m459 ();
extern "C" void EventSystem_get_current_m460 ();
extern "C" void EventSystem_set_current_m461 ();
extern "C" void EventSystem_get_sendNavigationEvents_m462 ();
extern "C" void EventSystem_set_sendNavigationEvents_m463 ();
extern "C" void EventSystem_get_pixelDragThreshold_m464 ();
extern "C" void EventSystem_set_pixelDragThreshold_m465 ();
extern "C" void EventSystem_get_currentInputModule_m466 ();
extern "C" void EventSystem_get_firstSelectedGameObject_m467 ();
extern "C" void EventSystem_set_firstSelectedGameObject_m468 ();
extern "C" void EventSystem_get_currentSelectedGameObject_m469 ();
extern "C" void EventSystem_get_lastSelectedGameObject_m470 ();
extern "C" void EventSystem_UpdateModules_m471 ();
extern "C" void EventSystem_get_alreadySelecting_m472 ();
extern "C" void EventSystem_SetSelectedGameObject_m473 ();
extern "C" void EventSystem_get_baseEventDataCache_m474 ();
extern "C" void EventSystem_SetSelectedGameObject_m475 ();
extern "C" void EventSystem_RaycastComparer_m476 ();
extern "C" void EventSystem_RaycastAll_m477 ();
extern "C" void EventSystem_IsPointerOverGameObject_m478 ();
extern "C" void EventSystem_IsPointerOverGameObject_m479 ();
extern "C" void EventSystem_OnEnable_m480 ();
extern "C" void EventSystem_OnDisable_m481 ();
extern "C" void EventSystem_TickModules_m482 ();
extern "C" void EventSystem_Update_m483 ();
extern "C" void EventSystem_ChangeEventModule_m484 ();
extern "C" void EventSystem_ToString_m485 ();
extern "C" void TriggerEvent__ctor_m486 ();
extern "C" void Entry__ctor_m487 ();
extern "C" void EventTrigger__ctor_m488 ();
extern "C" void EventTrigger_get_triggers_m489 ();
extern "C" void EventTrigger_set_triggers_m490 ();
extern "C" void EventTrigger_Execute_m491 ();
extern "C" void EventTrigger_OnPointerEnter_m492 ();
extern "C" void EventTrigger_OnPointerExit_m493 ();
extern "C" void EventTrigger_OnDrag_m494 ();
extern "C" void EventTrigger_OnDrop_m495 ();
extern "C" void EventTrigger_OnPointerDown_m496 ();
extern "C" void EventTrigger_OnPointerUp_m497 ();
extern "C" void EventTrigger_OnPointerClick_m498 ();
extern "C" void EventTrigger_OnSelect_m499 ();
extern "C" void EventTrigger_OnDeselect_m500 ();
extern "C" void EventTrigger_OnScroll_m501 ();
extern "C" void EventTrigger_OnMove_m502 ();
extern "C" void EventTrigger_OnUpdateSelected_m503 ();
extern "C" void EventTrigger_OnInitializePotentialDrag_m504 ();
extern "C" void EventTrigger_OnBeginDrag_m505 ();
extern "C" void EventTrigger_OnEndDrag_m506 ();
extern "C" void EventTrigger_OnSubmit_m507 ();
extern "C" void EventTrigger_OnCancel_m508 ();
extern "C" void ExecuteEvents__cctor_m509 ();
extern "C" void ExecuteEvents_Execute_m510 ();
extern "C" void ExecuteEvents_Execute_m511 ();
extern "C" void ExecuteEvents_Execute_m512 ();
extern "C" void ExecuteEvents_Execute_m513 ();
extern "C" void ExecuteEvents_Execute_m514 ();
extern "C" void ExecuteEvents_Execute_m515 ();
extern "C" void ExecuteEvents_Execute_m516 ();
extern "C" void ExecuteEvents_Execute_m517 ();
extern "C" void ExecuteEvents_Execute_m518 ();
extern "C" void ExecuteEvents_Execute_m519 ();
extern "C" void ExecuteEvents_Execute_m520 ();
extern "C" void ExecuteEvents_Execute_m521 ();
extern "C" void ExecuteEvents_Execute_m522 ();
extern "C" void ExecuteEvents_Execute_m523 ();
extern "C" void ExecuteEvents_Execute_m524 ();
extern "C" void ExecuteEvents_Execute_m525 ();
extern "C" void ExecuteEvents_Execute_m526 ();
extern "C" void ExecuteEvents_get_pointerEnterHandler_m527 ();
extern "C" void ExecuteEvents_get_pointerExitHandler_m528 ();
extern "C" void ExecuteEvents_get_pointerDownHandler_m529 ();
extern "C" void ExecuteEvents_get_pointerUpHandler_m530 ();
extern "C" void ExecuteEvents_get_pointerClickHandler_m531 ();
extern "C" void ExecuteEvents_get_initializePotentialDrag_m532 ();
extern "C" void ExecuteEvents_get_beginDragHandler_m533 ();
extern "C" void ExecuteEvents_get_dragHandler_m534 ();
extern "C" void ExecuteEvents_get_endDragHandler_m535 ();
extern "C" void ExecuteEvents_get_dropHandler_m536 ();
extern "C" void ExecuteEvents_get_scrollHandler_m537 ();
extern "C" void ExecuteEvents_get_updateSelectedHandler_m538 ();
extern "C" void ExecuteEvents_get_selectHandler_m539 ();
extern "C" void ExecuteEvents_get_deselectHandler_m540 ();
extern "C" void ExecuteEvents_get_moveHandler_m541 ();
extern "C" void ExecuteEvents_get_submitHandler_m542 ();
extern "C" void ExecuteEvents_get_cancelHandler_m543 ();
extern "C" void ExecuteEvents_GetEventChain_m544 ();
extern "C" void ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m545 ();
extern "C" void RaycasterManager__cctor_m546 ();
extern "C" void RaycasterManager_AddRaycaster_m547 ();
extern "C" void RaycasterManager_GetRaycasters_m548 ();
extern "C" void RaycasterManager_RemoveRaycasters_m549 ();
extern "C" void RaycastResult_get_gameObject_m550 ();
extern "C" void RaycastResult_set_gameObject_m551 ();
extern "C" void RaycastResult_get_isValid_m552 ();
extern "C" void RaycastResult_Clear_m553 ();
extern "C" void RaycastResult_ToString_m554 ();
extern "C" void UIBehaviour__ctor_m555 ();
extern "C" void UIBehaviour_Awake_m556 ();
extern "C" void UIBehaviour_OnEnable_m557 ();
extern "C" void UIBehaviour_Start_m558 ();
extern "C" void UIBehaviour_OnDisable_m559 ();
extern "C" void UIBehaviour_OnDestroy_m560 ();
extern "C" void UIBehaviour_IsActive_m561 ();
extern "C" void UIBehaviour_OnRectTransformDimensionsChange_m562 ();
extern "C" void UIBehaviour_OnBeforeTransformParentChanged_m563 ();
extern "C" void UIBehaviour_OnTransformParentChanged_m564 ();
extern "C" void UIBehaviour_OnDidApplyAnimationProperties_m565 ();
extern "C" void UIBehaviour_OnCanvasGroupChanged_m566 ();
extern "C" void UIBehaviour_OnCanvasHierarchyChanged_m567 ();
extern "C" void UIBehaviour_IsDestroyed_m568 ();
extern "C" void AxisEventData__ctor_m569 ();
extern "C" void AxisEventData_get_moveVector_m570 ();
extern "C" void AxisEventData_set_moveVector_m571 ();
extern "C" void AxisEventData_get_moveDir_m572 ();
extern "C" void AxisEventData_set_moveDir_m573 ();
extern "C" void BaseEventData__ctor_m574 ();
extern "C" void BaseEventData_Reset_m575 ();
extern "C" void BaseEventData_Use_m576 ();
extern "C" void BaseEventData_get_used_m577 ();
extern "C" void BaseEventData_get_currentInputModule_m578 ();
extern "C" void BaseEventData_get_selectedObject_m579 ();
extern "C" void BaseEventData_set_selectedObject_m580 ();
extern "C" void PointerEventData__ctor_m581 ();
extern "C" void PointerEventData_get_pointerEnter_m582 ();
extern "C" void PointerEventData_set_pointerEnter_m583 ();
extern "C" void PointerEventData_get_lastPress_m584 ();
extern "C" void PointerEventData_set_lastPress_m585 ();
extern "C" void PointerEventData_get_rawPointerPress_m586 ();
extern "C" void PointerEventData_set_rawPointerPress_m587 ();
extern "C" void PointerEventData_get_pointerDrag_m588 ();
extern "C" void PointerEventData_set_pointerDrag_m589 ();
extern "C" void PointerEventData_get_pointerCurrentRaycast_m590 ();
extern "C" void PointerEventData_set_pointerCurrentRaycast_m591 ();
extern "C" void PointerEventData_get_pointerPressRaycast_m592 ();
extern "C" void PointerEventData_set_pointerPressRaycast_m593 ();
extern "C" void PointerEventData_get_eligibleForClick_m594 ();
extern "C" void PointerEventData_set_eligibleForClick_m595 ();
extern "C" void PointerEventData_get_pointerId_m252 ();
extern "C" void PointerEventData_set_pointerId_m596 ();
extern "C" void PointerEventData_get_position_m228 ();
extern "C" void PointerEventData_set_position_m597 ();
extern "C" void PointerEventData_get_delta_m598 ();
extern "C" void PointerEventData_set_delta_m599 ();
extern "C" void PointerEventData_get_pressPosition_m600 ();
extern "C" void PointerEventData_set_pressPosition_m601 ();
extern "C" void PointerEventData_get_worldPosition_m602 ();
extern "C" void PointerEventData_set_worldPosition_m603 ();
extern "C" void PointerEventData_get_worldNormal_m604 ();
extern "C" void PointerEventData_set_worldNormal_m605 ();
extern "C" void PointerEventData_get_clickTime_m606 ();
extern "C" void PointerEventData_set_clickTime_m607 ();
extern "C" void PointerEventData_get_clickCount_m608 ();
extern "C" void PointerEventData_set_clickCount_m609 ();
extern "C" void PointerEventData_get_scrollDelta_m610 ();
extern "C" void PointerEventData_set_scrollDelta_m611 ();
extern "C" void PointerEventData_get_useDragThreshold_m612 ();
extern "C" void PointerEventData_set_useDragThreshold_m613 ();
extern "C" void PointerEventData_get_dragging_m614 ();
extern "C" void PointerEventData_set_dragging_m615 ();
extern "C" void PointerEventData_get_button_m616 ();
extern "C" void PointerEventData_set_button_m617 ();
extern "C" void PointerEventData_IsPointerMoving_m618 ();
extern "C" void PointerEventData_IsScrolling_m619 ();
extern "C" void PointerEventData_get_enterEventCamera_m620 ();
extern "C" void PointerEventData_get_pressEventCamera_m621 ();
extern "C" void PointerEventData_get_pointerPress_m622 ();
extern "C" void PointerEventData_set_pointerPress_m623 ();
extern "C" void PointerEventData_ToString_m624 ();
extern "C" void BaseInputModule__ctor_m625 ();
extern "C" void BaseInputModule_get_eventSystem_m626 ();
extern "C" void BaseInputModule_OnEnable_m627 ();
extern "C" void BaseInputModule_OnDisable_m628 ();
extern "C" void BaseInputModule_FindFirstRaycast_m629 ();
extern "C" void BaseInputModule_DetermineMoveDirection_m630 ();
extern "C" void BaseInputModule_DetermineMoveDirection_m631 ();
extern "C" void BaseInputModule_FindCommonRoot_m632 ();
extern "C" void BaseInputModule_HandlePointerExitAndEnter_m633 ();
extern "C" void BaseInputModule_GetAxisEventData_m634 ();
extern "C" void BaseInputModule_GetBaseEventData_m635 ();
extern "C" void BaseInputModule_IsPointerOverGameObject_m636 ();
extern "C" void BaseInputModule_ShouldActivateModule_m637 ();
extern "C" void BaseInputModule_DeactivateModule_m638 ();
extern "C" void BaseInputModule_ActivateModule_m639 ();
extern "C" void BaseInputModule_UpdateModule_m640 ();
extern "C" void BaseInputModule_IsModuleSupported_m641 ();
extern "C" void ButtonState__ctor_m642 ();
extern "C" void ButtonState_get_eventData_m643 ();
extern "C" void ButtonState_set_eventData_m644 ();
extern "C" void ButtonState_get_button_m645 ();
extern "C" void ButtonState_set_button_m646 ();
extern "C" void MouseState__ctor_m647 ();
extern "C" void MouseState_AnyPressesThisFrame_m648 ();
extern "C" void MouseState_AnyReleasesThisFrame_m649 ();
extern "C" void MouseState_GetButtonState_m650 ();
extern "C" void MouseState_SetButtonState_m651 ();
extern "C" void MouseButtonEventData__ctor_m652 ();
extern "C" void MouseButtonEventData_PressedThisFrame_m653 ();
extern "C" void MouseButtonEventData_ReleasedThisFrame_m654 ();
extern "C" void PointerInputModule__ctor_m655 ();
extern "C" void PointerInputModule_GetPointerData_m656 ();
extern "C" void PointerInputModule_RemovePointerData_m657 ();
extern "C" void PointerInputModule_GetTouchPointerEventData_m658 ();
extern "C" void PointerInputModule_CopyFromTo_m659 ();
extern "C" void PointerInputModule_StateForMouseButton_m660 ();
extern "C" void PointerInputModule_GetMousePointerEventData_m661 ();
extern "C" void PointerInputModule_GetMousePointerEventData_m662 ();
extern "C" void PointerInputModule_GetLastPointerEventData_m663 ();
extern "C" void PointerInputModule_ShouldStartDrag_m664 ();
extern "C" void PointerInputModule_ProcessMove_m665 ();
extern "C" void PointerInputModule_ProcessDrag_m666 ();
extern "C" void PointerInputModule_IsPointerOverGameObject_m667 ();
extern "C" void PointerInputModule_ClearSelection_m668 ();
extern "C" void PointerInputModule_ToString_m669 ();
extern "C" void PointerInputModule_DeselectIfSelectionChanged_m670 ();
extern "C" void StandaloneInputModule__ctor_m671 ();
extern "C" void StandaloneInputModule_get_inputMode_m672 ();
extern "C" void StandaloneInputModule_get_allowActivationOnMobileDevice_m673 ();
extern "C" void StandaloneInputModule_set_allowActivationOnMobileDevice_m674 ();
extern "C" void StandaloneInputModule_get_forceModuleActive_m675 ();
extern "C" void StandaloneInputModule_set_forceModuleActive_m676 ();
extern "C" void StandaloneInputModule_get_inputActionsPerSecond_m677 ();
extern "C" void StandaloneInputModule_set_inputActionsPerSecond_m678 ();
extern "C" void StandaloneInputModule_get_repeatDelay_m679 ();
extern "C" void StandaloneInputModule_set_repeatDelay_m680 ();
extern "C" void StandaloneInputModule_get_horizontalAxis_m681 ();
extern "C" void StandaloneInputModule_set_horizontalAxis_m682 ();
extern "C" void StandaloneInputModule_get_verticalAxis_m683 ();
extern "C" void StandaloneInputModule_set_verticalAxis_m684 ();
extern "C" void StandaloneInputModule_get_submitButton_m685 ();
extern "C" void StandaloneInputModule_set_submitButton_m686 ();
extern "C" void StandaloneInputModule_get_cancelButton_m687 ();
extern "C" void StandaloneInputModule_set_cancelButton_m688 ();
extern "C" void StandaloneInputModule_UpdateModule_m689 ();
extern "C" void StandaloneInputModule_IsModuleSupported_m690 ();
extern "C" void StandaloneInputModule_ShouldActivateModule_m691 ();
extern "C" void StandaloneInputModule_ActivateModule_m692 ();
extern "C" void StandaloneInputModule_DeactivateModule_m693 ();
extern "C" void StandaloneInputModule_Process_m694 ();
extern "C" void StandaloneInputModule_SendSubmitEventToSelectedObject_m695 ();
extern "C" void StandaloneInputModule_GetRawMoveVector_m696 ();
extern "C" void StandaloneInputModule_SendMoveEventToSelectedObject_m697 ();
extern "C" void StandaloneInputModule_ProcessMouseEvent_m698 ();
extern "C" void StandaloneInputModule_ProcessMouseEvent_m699 ();
extern "C" void StandaloneInputModule_SendUpdateEventToSelectedObject_m700 ();
extern "C" void StandaloneInputModule_ProcessMousePress_m701 ();
extern "C" void TouchInputModule__ctor_m702 ();
extern "C" void TouchInputModule_get_allowActivationOnStandalone_m703 ();
extern "C" void TouchInputModule_set_allowActivationOnStandalone_m704 ();
extern "C" void TouchInputModule_get_forceModuleActive_m705 ();
extern "C" void TouchInputModule_set_forceModuleActive_m706 ();
extern "C" void TouchInputModule_UpdateModule_m707 ();
extern "C" void TouchInputModule_IsModuleSupported_m708 ();
extern "C" void TouchInputModule_ShouldActivateModule_m709 ();
extern "C" void TouchInputModule_UseFakeInput_m710 ();
extern "C" void TouchInputModule_Process_m711 ();
extern "C" void TouchInputModule_FakeTouches_m712 ();
extern "C" void TouchInputModule_ProcessTouchEvents_m713 ();
extern "C" void TouchInputModule_ProcessTouchPress_m714 ();
extern "C" void TouchInputModule_DeactivateModule_m715 ();
extern "C" void TouchInputModule_ToString_m716 ();
extern "C" void BaseRaycaster__ctor_m717 ();
extern "C" void BaseRaycaster_get_priority_m718 ();
extern "C" void BaseRaycaster_get_sortOrderPriority_m719 ();
extern "C" void BaseRaycaster_get_renderOrderPriority_m720 ();
extern "C" void BaseRaycaster_ToString_m721 ();
extern "C" void BaseRaycaster_OnEnable_m722 ();
extern "C" void BaseRaycaster_OnDisable_m723 ();
extern "C" void Physics2DRaycaster__ctor_m724 ();
extern "C" void Physics2DRaycaster_Raycast_m725 ();
extern "C" void PhysicsRaycaster__ctor_m726 ();
extern "C" void PhysicsRaycaster_get_eventCamera_m727 ();
extern "C" void PhysicsRaycaster_get_depth_m728 ();
extern "C" void PhysicsRaycaster_get_finalEventMask_m729 ();
extern "C" void PhysicsRaycaster_get_eventMask_m730 ();
extern "C" void PhysicsRaycaster_set_eventMask_m731 ();
extern "C" void PhysicsRaycaster_Raycast_m732 ();
extern "C" void PhysicsRaycaster_U3CRaycastU3Em__1_m733 ();
extern "C" void ColorTweenCallback__ctor_m734 ();
extern "C" void ColorTween_get_startColor_m735 ();
extern "C" void ColorTween_set_startColor_m736 ();
extern "C" void ColorTween_get_targetColor_m737 ();
extern "C" void ColorTween_set_targetColor_m738 ();
extern "C" void ColorTween_get_tweenMode_m739 ();
extern "C" void ColorTween_set_tweenMode_m740 ();
extern "C" void ColorTween_get_duration_m741 ();
extern "C" void ColorTween_set_duration_m742 ();
extern "C" void ColorTween_get_ignoreTimeScale_m743 ();
extern "C" void ColorTween_set_ignoreTimeScale_m744 ();
extern "C" void ColorTween_TweenValue_m745 ();
extern "C" void ColorTween_AddOnChangedCallback_m746 ();
extern "C" void ColorTween_GetIgnoreTimescale_m747 ();
extern "C" void ColorTween_GetDuration_m748 ();
extern "C" void ColorTween_ValidTarget_m749 ();
extern "C" void FloatTweenCallback__ctor_m750 ();
extern "C" void FloatTween_get_startValue_m751 ();
extern "C" void FloatTween_set_startValue_m752 ();
extern "C" void FloatTween_get_targetValue_m753 ();
extern "C" void FloatTween_set_targetValue_m754 ();
extern "C" void FloatTween_get_duration_m755 ();
extern "C" void FloatTween_set_duration_m756 ();
extern "C" void FloatTween_get_ignoreTimeScale_m757 ();
extern "C" void FloatTween_set_ignoreTimeScale_m758 ();
extern "C" void FloatTween_TweenValue_m759 ();
extern "C" void FloatTween_AddOnChangedCallback_m760 ();
extern "C" void FloatTween_GetIgnoreTimescale_m761 ();
extern "C" void FloatTween_GetDuration_m762 ();
extern "C" void FloatTween_ValidTarget_m763 ();
extern "C" void AnimationTriggers__ctor_m764 ();
extern "C" void AnimationTriggers_get_normalTrigger_m765 ();
extern "C" void AnimationTriggers_set_normalTrigger_m766 ();
extern "C" void AnimationTriggers_get_highlightedTrigger_m767 ();
extern "C" void AnimationTriggers_set_highlightedTrigger_m768 ();
extern "C" void AnimationTriggers_get_pressedTrigger_m769 ();
extern "C" void AnimationTriggers_set_pressedTrigger_m770 ();
extern "C" void AnimationTriggers_get_disabledTrigger_m771 ();
extern "C" void AnimationTriggers_set_disabledTrigger_m772 ();
extern "C" void ButtonClickedEvent__ctor_m773 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1__ctor_m774 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m775 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m776 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m777 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Dispose_m778 ();
extern "C" void U3COnFinishSubmitU3Ec__Iterator1_Reset_m779 ();
extern "C" void Button__ctor_m780 ();
extern "C" void Button_get_onClick_m781 ();
extern "C" void Button_set_onClick_m782 ();
extern "C" void Button_Press_m783 ();
extern "C" void Button_OnPointerClick_m784 ();
extern "C" void Button_OnSubmit_m785 ();
extern "C" void Button_OnFinishSubmit_m786 ();
extern "C" void CanvasUpdateRegistry__ctor_m787 ();
extern "C" void CanvasUpdateRegistry__cctor_m788 ();
extern "C" void CanvasUpdateRegistry_get_instance_m789 ();
extern "C" void CanvasUpdateRegistry_ObjectValidForUpdate_m790 ();
extern "C" void CanvasUpdateRegistry_CleanInvalidItems_m791 ();
extern "C" void CanvasUpdateRegistry_PerformUpdate_m792 ();
extern "C" void CanvasUpdateRegistry_ParentCount_m793 ();
extern "C" void CanvasUpdateRegistry_SortLayoutList_m794 ();
extern "C" void CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m795 ();
extern "C" void CanvasUpdateRegistry_TryRegisterCanvasElementForLayoutRebuild_m796 ();
extern "C" void CanvasUpdateRegistry_InternalRegisterCanvasElementForLayoutRebuild_m797 ();
extern "C" void CanvasUpdateRegistry_RegisterCanvasElementForGraphicRebuild_m798 ();
extern "C" void CanvasUpdateRegistry_TryRegisterCanvasElementForGraphicRebuild_m799 ();
extern "C" void CanvasUpdateRegistry_InternalRegisterCanvasElementForGraphicRebuild_m800 ();
extern "C" void CanvasUpdateRegistry_UnRegisterCanvasElementForRebuild_m801 ();
extern "C" void CanvasUpdateRegistry_InternalUnRegisterCanvasElementForLayoutRebuild_m802 ();
extern "C" void CanvasUpdateRegistry_InternalUnRegisterCanvasElementForGraphicRebuild_m803 ();
extern "C" void CanvasUpdateRegistry_IsRebuildingLayout_m804 ();
extern "C" void CanvasUpdateRegistry_IsRebuildingGraphics_m805 ();
extern "C" void ColorBlock_get_normalColor_m806 ();
extern "C" void ColorBlock_set_normalColor_m807 ();
extern "C" void ColorBlock_get_highlightedColor_m808 ();
extern "C" void ColorBlock_set_highlightedColor_m809 ();
extern "C" void ColorBlock_get_pressedColor_m810 ();
extern "C" void ColorBlock_set_pressedColor_m811 ();
extern "C" void ColorBlock_get_disabledColor_m812 ();
extern "C" void ColorBlock_set_disabledColor_m813 ();
extern "C" void ColorBlock_get_colorMultiplier_m814 ();
extern "C" void ColorBlock_set_colorMultiplier_m815 ();
extern "C" void ColorBlock_get_fadeDuration_m816 ();
extern "C" void ColorBlock_set_fadeDuration_m817 ();
extern "C" void ColorBlock_get_defaultColorBlock_m818 ();
extern "C" void DefaultControls__cctor_m819 ();
extern "C" void DefaultControls_CreateUIElementRoot_m820 ();
extern "C" void DefaultControls_CreateUIObject_m821 ();
extern "C" void DefaultControls_SetDefaultTextValues_m822 ();
extern "C" void DefaultControls_SetDefaultColorTransitionValues_m823 ();
extern "C" void DefaultControls_SetParentAndAlign_m824 ();
extern "C" void DefaultControls_SetLayerRecursively_m825 ();
extern "C" void DefaultControls_CreatePanel_m826 ();
extern "C" void DefaultControls_CreateButton_m827 ();
extern "C" void DefaultControls_CreateText_m828 ();
extern "C" void DefaultControls_CreateImage_m829 ();
extern "C" void DefaultControls_CreateRawImage_m830 ();
extern "C" void DefaultControls_CreateSlider_m831 ();
extern "C" void DefaultControls_CreateScrollbar_m832 ();
extern "C" void DefaultControls_CreateToggle_m833 ();
extern "C" void DefaultControls_CreateInputField_m834 ();
extern "C" void DefaultControls_CreateDropdown_m835 ();
extern "C" void DefaultControls_CreateScrollView_m836 ();
extern "C" void DropdownItem__ctor_m837 ();
extern "C" void DropdownItem_get_text_m838 ();
extern "C" void DropdownItem_set_text_m839 ();
extern "C" void DropdownItem_get_image_m840 ();
extern "C" void DropdownItem_set_image_m841 ();
extern "C" void DropdownItem_get_rectTransform_m842 ();
extern "C" void DropdownItem_set_rectTransform_m843 ();
extern "C" void DropdownItem_get_toggle_m844 ();
extern "C" void DropdownItem_set_toggle_m845 ();
extern "C" void DropdownItem_OnPointerEnter_m846 ();
extern "C" void DropdownItem_OnCancel_m847 ();
extern "C" void OptionData__ctor_m848 ();
extern "C" void OptionData__ctor_m849 ();
extern "C" void OptionData__ctor_m850 ();
extern "C" void OptionData__ctor_m851 ();
extern "C" void OptionData_get_text_m852 ();
extern "C" void OptionData_set_text_m853 ();
extern "C" void OptionData_get_image_m854 ();
extern "C" void OptionData_set_image_m855 ();
extern "C" void OptionDataList__ctor_m856 ();
extern "C" void OptionDataList_get_options_m857 ();
extern "C" void OptionDataList_set_options_m858 ();
extern "C" void DropdownEvent__ctor_m859 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2__ctor_m860 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m861 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m862 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_MoveNext_m863 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_Dispose_m864 ();
extern "C" void U3CDelayedDestroyDropdownListU3Ec__Iterator2_Reset_m865 ();
extern "C" void U3CShowU3Ec__AnonStorey6__ctor_m866 ();
extern "C" void U3CShowU3Ec__AnonStorey6_U3CU3Em__2_m867 ();
extern "C" void Dropdown__ctor_m868 ();
extern "C" void Dropdown_get_template_m869 ();
extern "C" void Dropdown_set_template_m870 ();
extern "C" void Dropdown_get_captionText_m871 ();
extern "C" void Dropdown_set_captionText_m872 ();
extern "C" void Dropdown_get_captionImage_m873 ();
extern "C" void Dropdown_set_captionImage_m874 ();
extern "C" void Dropdown_get_itemText_m875 ();
extern "C" void Dropdown_set_itemText_m876 ();
extern "C" void Dropdown_get_itemImage_m877 ();
extern "C" void Dropdown_set_itemImage_m878 ();
extern "C" void Dropdown_get_options_m879 ();
extern "C" void Dropdown_set_options_m880 ();
extern "C" void Dropdown_get_onValueChanged_m881 ();
extern "C" void Dropdown_set_onValueChanged_m882 ();
extern "C" void Dropdown_get_value_m883 ();
extern "C" void Dropdown_set_value_m884 ();
extern "C" void Dropdown_Awake_m885 ();
extern "C" void Dropdown_Refresh_m886 ();
extern "C" void Dropdown_SetupTemplate_m887 ();
extern "C" void Dropdown_OnPointerClick_m888 ();
extern "C" void Dropdown_OnSubmit_m889 ();
extern "C" void Dropdown_OnCancel_m890 ();
extern "C" void Dropdown_Show_m891 ();
extern "C" void Dropdown_CreateBlocker_m892 ();
extern "C" void Dropdown_DestroyBlocker_m893 ();
extern "C" void Dropdown_CreateDropdownList_m894 ();
extern "C" void Dropdown_DestroyDropdownList_m895 ();
extern "C" void Dropdown_CreateItem_m896 ();
extern "C" void Dropdown_DestroyItem_m897 ();
extern "C" void Dropdown_AddItem_m898 ();
extern "C" void Dropdown_AlphaFadeList_m899 ();
extern "C" void Dropdown_AlphaFadeList_m900 ();
extern "C" void Dropdown_SetAlpha_m901 ();
extern "C" void Dropdown_Hide_m902 ();
extern "C" void Dropdown_DelayedDestroyDropdownList_m903 ();
extern "C" void Dropdown_OnSelectItem_m904 ();
extern "C" void FontData__ctor_m905 ();
extern "C" void FontData_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m906 ();
extern "C" void FontData_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m907 ();
extern "C" void FontData_get_defaultFontData_m908 ();
extern "C" void FontData_get_font_m909 ();
extern "C" void FontData_set_font_m910 ();
extern "C" void FontData_get_fontSize_m911 ();
extern "C" void FontData_set_fontSize_m912 ();
extern "C" void FontData_get_fontStyle_m913 ();
extern "C" void FontData_set_fontStyle_m914 ();
extern "C" void FontData_get_bestFit_m915 ();
extern "C" void FontData_set_bestFit_m916 ();
extern "C" void FontData_get_minSize_m917 ();
extern "C" void FontData_set_minSize_m918 ();
extern "C" void FontData_get_maxSize_m919 ();
extern "C" void FontData_set_maxSize_m920 ();
extern "C" void FontData_get_alignment_m921 ();
extern "C" void FontData_set_alignment_m922 ();
extern "C" void FontData_get_richText_m923 ();
extern "C" void FontData_set_richText_m924 ();
extern "C" void FontData_get_horizontalOverflow_m925 ();
extern "C" void FontData_set_horizontalOverflow_m926 ();
extern "C" void FontData_get_verticalOverflow_m927 ();
extern "C" void FontData_set_verticalOverflow_m928 ();
extern "C" void FontData_get_lineSpacing_m929 ();
extern "C" void FontData_set_lineSpacing_m930 ();
extern "C" void FontUpdateTracker__cctor_m931 ();
extern "C" void FontUpdateTracker_TrackText_m932 ();
extern "C" void FontUpdateTracker_RebuildForFont_m933 ();
extern "C" void FontUpdateTracker_UntrackText_m934 ();
extern "C" void Graphic__ctor_m935 ();
extern "C" void Graphic__cctor_m936 ();
extern "C" void Graphic_get_defaultGraphicMaterial_m937 ();
extern "C" void Graphic_get_color_m938 ();
extern "C" void Graphic_set_color_m457 ();
extern "C" void Graphic_get_raycastTarget_m939 ();
extern "C" void Graphic_set_raycastTarget_m940 ();
extern "C" void Graphic_get_useLegacyMeshGeneration_m941 ();
extern "C" void Graphic_set_useLegacyMeshGeneration_m942 ();
extern "C" void Graphic_SetAllDirty_m943 ();
extern "C" void Graphic_SetLayoutDirty_m944 ();
extern "C" void Graphic_SetVerticesDirty_m945 ();
extern "C" void Graphic_SetMaterialDirty_m946 ();
extern "C" void Graphic_OnRectTransformDimensionsChange_m947 ();
extern "C" void Graphic_OnBeforeTransformParentChanged_m948 ();
extern "C" void Graphic_OnTransformParentChanged_m949 ();
extern "C" void Graphic_get_depth_m950 ();
extern "C" void Graphic_get_rectTransform_m951 ();
extern "C" void Graphic_get_canvas_m952 ();
extern "C" void Graphic_CacheCanvas_m953 ();
extern "C" void Graphic_get_canvasRenderer_m954 ();
extern "C" void Graphic_get_defaultMaterial_m955 ();
extern "C" void Graphic_get_material_m956 ();
extern "C" void Graphic_set_material_m957 ();
extern "C" void Graphic_get_materialForRendering_m958 ();
extern "C" void Graphic_get_mainTexture_m959 ();
extern "C" void Graphic_OnEnable_m960 ();
extern "C" void Graphic_OnDisable_m961 ();
extern "C" void Graphic_OnCanvasHierarchyChanged_m962 ();
extern "C" void Graphic_Rebuild_m963 ();
extern "C" void Graphic_LayoutComplete_m964 ();
extern "C" void Graphic_GraphicUpdateComplete_m965 ();
extern "C" void Graphic_UpdateMaterial_m966 ();
extern "C" void Graphic_UpdateGeometry_m967 ();
extern "C" void Graphic_DoMeshGeneration_m968 ();
extern "C" void Graphic_DoLegacyMeshGeneration_m969 ();
extern "C" void Graphic_get_workerMesh_m970 ();
extern "C" void Graphic_OnFillVBO_m971 ();
extern "C" void Graphic_OnPopulateMesh_m972 ();
extern "C" void Graphic_OnPopulateMesh_m973 ();
extern "C" void Graphic_OnDidApplyAnimationProperties_m974 ();
extern "C" void Graphic_SetNativeSize_m975 ();
extern "C" void Graphic_Raycast_m976 ();
extern "C" void Graphic_PixelAdjustPoint_m977 ();
extern "C" void Graphic_GetPixelAdjustedRect_m978 ();
extern "C" void Graphic_CrossFadeColor_m979 ();
extern "C" void Graphic_CrossFadeColor_m980 ();
extern "C" void Graphic_CreateColorFromAlpha_m981 ();
extern "C" void Graphic_CrossFadeAlpha_m982 ();
extern "C" void Graphic_RegisterDirtyLayoutCallback_m983 ();
extern "C" void Graphic_UnregisterDirtyLayoutCallback_m984 ();
extern "C" void Graphic_RegisterDirtyVerticesCallback_m985 ();
extern "C" void Graphic_UnregisterDirtyVerticesCallback_m986 ();
extern "C" void Graphic_RegisterDirtyMaterialCallback_m987 ();
extern "C" void Graphic_UnregisterDirtyMaterialCallback_m988 ();
extern "C" void Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m989 ();
extern "C" void Graphic_UnityEngine_UI_ICanvasElement_get_transform_m990 ();
extern "C" void GraphicRaycaster__ctor_m991 ();
extern "C" void GraphicRaycaster__cctor_m992 ();
extern "C" void GraphicRaycaster_get_sortOrderPriority_m993 ();
extern "C" void GraphicRaycaster_get_renderOrderPriority_m994 ();
extern "C" void GraphicRaycaster_get_ignoreReversedGraphics_m995 ();
extern "C" void GraphicRaycaster_set_ignoreReversedGraphics_m996 ();
extern "C" void GraphicRaycaster_get_blockingObjects_m997 ();
extern "C" void GraphicRaycaster_set_blockingObjects_m998 ();
extern "C" void GraphicRaycaster_get_canvas_m999 ();
extern "C" void GraphicRaycaster_Raycast_m1000 ();
extern "C" void GraphicRaycaster_get_eventCamera_m1001 ();
extern "C" void GraphicRaycaster_Raycast_m1002 ();
extern "C" void GraphicRaycaster_U3CRaycastU3Em__3_m1003 ();
extern "C" void GraphicRegistry__ctor_m1004 ();
extern "C" void GraphicRegistry__cctor_m1005 ();
extern "C" void GraphicRegistry_get_instance_m1006 ();
extern "C" void GraphicRegistry_RegisterGraphicForCanvas_m1007 ();
extern "C" void GraphicRegistry_UnregisterGraphicForCanvas_m1008 ();
extern "C" void GraphicRegistry_GetGraphicsForCanvas_m1009 ();
extern "C" void Image__ctor_m1010 ();
extern "C" void Image__cctor_m1011 ();
extern "C" void Image_get_sprite_m1012 ();
extern "C" void Image_set_sprite_m1013 ();
extern "C" void Image_get_overrideSprite_m1014 ();
extern "C" void Image_set_overrideSprite_m1015 ();
extern "C" void Image_get_type_m1016 ();
extern "C" void Image_set_type_m1017 ();
extern "C" void Image_get_preserveAspect_m1018 ();
extern "C" void Image_set_preserveAspect_m1019 ();
extern "C" void Image_get_fillCenter_m1020 ();
extern "C" void Image_set_fillCenter_m1021 ();
extern "C" void Image_get_fillMethod_m1022 ();
extern "C" void Image_set_fillMethod_m1023 ();
extern "C" void Image_get_fillAmount_m1024 ();
extern "C" void Image_set_fillAmount_m1025 ();
extern "C" void Image_get_fillClockwise_m1026 ();
extern "C" void Image_set_fillClockwise_m1027 ();
extern "C" void Image_get_fillOrigin_m1028 ();
extern "C" void Image_set_fillOrigin_m1029 ();
extern "C" void Image_get_eventAlphaThreshold_m1030 ();
extern "C" void Image_set_eventAlphaThreshold_m1031 ();
extern "C" void Image_get_mainTexture_m1032 ();
extern "C" void Image_get_hasBorder_m1033 ();
extern "C" void Image_get_pixelsPerUnit_m1034 ();
extern "C" void Image_OnBeforeSerialize_m1035 ();
extern "C" void Image_OnAfterDeserialize_m1036 ();
extern "C" void Image_GetDrawingDimensions_m1037 ();
extern "C" void Image_SetNativeSize_m1038 ();
extern "C" void Image_OnPopulateMesh_m1039 ();
extern "C" void Image_GenerateSimpleSprite_m1040 ();
extern "C" void Image_GenerateSlicedSprite_m1041 ();
extern "C" void Image_GenerateTiledSprite_m1042 ();
extern "C" void Image_AddQuad_m1043 ();
extern "C" void Image_AddQuad_m1044 ();
extern "C" void Image_GetAdjustedBorders_m1045 ();
extern "C" void Image_GenerateFilledSprite_m1046 ();
extern "C" void Image_RadialCut_m1047 ();
extern "C" void Image_RadialCut_m1048 ();
extern "C" void Image_CalculateLayoutInputHorizontal_m1049 ();
extern "C" void Image_CalculateLayoutInputVertical_m1050 ();
extern "C" void Image_get_minWidth_m1051 ();
extern "C" void Image_get_preferredWidth_m1052 ();
extern "C" void Image_get_flexibleWidth_m1053 ();
extern "C" void Image_get_minHeight_m1054 ();
extern "C" void Image_get_preferredHeight_m1055 ();
extern "C" void Image_get_flexibleHeight_m1056 ();
extern "C" void Image_get_layoutPriority_m1057 ();
extern "C" void Image_IsRaycastLocationValid_m1058 ();
extern "C" void Image_MapCoordinate_m1059 ();
extern "C" void SubmitEvent__ctor_m1060 ();
extern "C" void OnChangeEvent__ctor_m1061 ();
extern "C" void OnValidateInput__ctor_m1062 ();
extern "C" void OnValidateInput_Invoke_m1063 ();
extern "C" void OnValidateInput_BeginInvoke_m1064 ();
extern "C" void OnValidateInput_EndInvoke_m1065 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3__ctor_m1066 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1067 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1068 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_MoveNext_m1069 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_Dispose_m1070 ();
extern "C" void U3CCaretBlinkU3Ec__Iterator3_Reset_m1071 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4__ctor_m1072 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1073 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1074 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_MoveNext_m1075 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_Dispose_m1076 ();
extern "C" void U3CMouseDragOutsideRectU3Ec__Iterator4_Reset_m1077 ();
extern "C" void InputField__ctor_m1078 ();
extern "C" void InputField__cctor_m1079 ();
extern "C" void InputField_get_mesh_m1080 ();
extern "C" void InputField_get_cachedInputTextGenerator_m1081 ();
extern "C" void InputField_set_shouldHideMobileInput_m1082 ();
extern "C" void InputField_get_shouldHideMobileInput_m1083 ();
extern "C" void InputField_get_text_m1084 ();
extern "C" void InputField_set_text_m1085 ();
extern "C" void InputField_get_isFocused_m1086 ();
extern "C" void InputField_get_caretBlinkRate_m1087 ();
extern "C" void InputField_set_caretBlinkRate_m1088 ();
extern "C" void InputField_get_textComponent_m1089 ();
extern "C" void InputField_set_textComponent_m1090 ();
extern "C" void InputField_get_placeholder_m1091 ();
extern "C" void InputField_set_placeholder_m1092 ();
extern "C" void InputField_get_selectionColor_m1093 ();
extern "C" void InputField_set_selectionColor_m1094 ();
extern "C" void InputField_get_onEndEdit_m1095 ();
extern "C" void InputField_set_onEndEdit_m1096 ();
extern "C" void InputField_get_onValueChange_m1097 ();
extern "C" void InputField_set_onValueChange_m1098 ();
extern "C" void InputField_get_onValidateInput_m1099 ();
extern "C" void InputField_set_onValidateInput_m1100 ();
extern "C" void InputField_get_characterLimit_m1101 ();
extern "C" void InputField_set_characterLimit_m1102 ();
extern "C" void InputField_get_contentType_m1103 ();
extern "C" void InputField_set_contentType_m1104 ();
extern "C" void InputField_get_lineType_m1105 ();
extern "C" void InputField_set_lineType_m1106 ();
extern "C" void InputField_get_inputType_m1107 ();
extern "C" void InputField_set_inputType_m1108 ();
extern "C" void InputField_get_keyboardType_m1109 ();
extern "C" void InputField_set_keyboardType_m1110 ();
extern "C" void InputField_get_characterValidation_m1111 ();
extern "C" void InputField_set_characterValidation_m1112 ();
extern "C" void InputField_get_multiLine_m1113 ();
extern "C" void InputField_get_asteriskChar_m1114 ();
extern "C" void InputField_set_asteriskChar_m1115 ();
extern "C" void InputField_get_wasCanceled_m1116 ();
extern "C" void InputField_ClampPos_m1117 ();
extern "C" void InputField_get_caretPositionInternal_m1118 ();
extern "C" void InputField_set_caretPositionInternal_m1119 ();
extern "C" void InputField_get_caretSelectPositionInternal_m1120 ();
extern "C" void InputField_set_caretSelectPositionInternal_m1121 ();
extern "C" void InputField_get_hasSelection_m1122 ();
extern "C" void InputField_get_caretPosition_m1123 ();
extern "C" void InputField_set_caretPosition_m1124 ();
extern "C" void InputField_get_selectionAnchorPosition_m1125 ();
extern "C" void InputField_set_selectionAnchorPosition_m1126 ();
extern "C" void InputField_get_selectionFocusPosition_m1127 ();
extern "C" void InputField_set_selectionFocusPosition_m1128 ();
extern "C" void InputField_OnEnable_m1129 ();
extern "C" void InputField_OnDisable_m1130 ();
extern "C" void InputField_CaretBlink_m1131 ();
extern "C" void InputField_SetCaretVisible_m1132 ();
extern "C" void InputField_SetCaretActive_m1133 ();
extern "C" void InputField_OnFocus_m1134 ();
extern "C" void InputField_SelectAll_m1135 ();
extern "C" void InputField_MoveTextEnd_m1136 ();
extern "C" void InputField_MoveTextStart_m1137 ();
extern "C" void InputField_get_clipboard_m1138 ();
extern "C" void InputField_set_clipboard_m1139 ();
extern "C" void InputField_InPlaceEditing_m1140 ();
extern "C" void InputField_LateUpdate_m1141 ();
extern "C" void InputField_ScreenToLocal_m1142 ();
extern "C" void InputField_GetUnclampedCharacterLineFromPosition_m1143 ();
extern "C" void InputField_GetCharacterIndexFromPosition_m1144 ();
extern "C" void InputField_MayDrag_m1145 ();
extern "C" void InputField_OnBeginDrag_m1146 ();
extern "C" void InputField_OnDrag_m1147 ();
extern "C" void InputField_MouseDragOutsideRect_m1148 ();
extern "C" void InputField_OnEndDrag_m1149 ();
extern "C" void InputField_OnPointerDown_m1150 ();
extern "C" void InputField_KeyPressed_m1151 ();
extern "C" void InputField_IsValidChar_m1152 ();
extern "C" void InputField_ProcessEvent_m1153 ();
extern "C" void InputField_OnUpdateSelected_m1154 ();
extern "C" void InputField_GetSelectedString_m1155 ();
extern "C" void InputField_FindtNextWordBegin_m1156 ();
extern "C" void InputField_MoveRight_m1157 ();
extern "C" void InputField_FindtPrevWordBegin_m1158 ();
extern "C" void InputField_MoveLeft_m1159 ();
extern "C" void InputField_DetermineCharacterLine_m1160 ();
extern "C" void InputField_LineUpCharacterPosition_m1161 ();
extern "C" void InputField_LineDownCharacterPosition_m1162 ();
extern "C" void InputField_MoveDown_m1163 ();
extern "C" void InputField_MoveDown_m1164 ();
extern "C" void InputField_MoveUp_m1165 ();
extern "C" void InputField_MoveUp_m1166 ();
extern "C" void InputField_Delete_m1167 ();
extern "C" void InputField_ForwardSpace_m1168 ();
extern "C" void InputField_Backspace_m1169 ();
extern "C" void InputField_Insert_m1170 ();
extern "C" void InputField_SendOnValueChangedAndUpdateLabel_m1171 ();
extern "C" void InputField_SendOnValueChanged_m1172 ();
extern "C" void InputField_SendOnSubmit_m1173 ();
extern "C" void InputField_Append_m1174 ();
extern "C" void InputField_Append_m1175 ();
extern "C" void InputField_UpdateLabel_m1176 ();
extern "C" void InputField_IsSelectionVisible_m1177 ();
extern "C" void InputField_GetLineStartPosition_m1178 ();
extern "C" void InputField_GetLineEndPosition_m1179 ();
extern "C" void InputField_SetDrawRangeToContainCaretPosition_m1180 ();
extern "C" void InputField_MarkGeometryAsDirty_m1181 ();
extern "C" void InputField_Rebuild_m1182 ();
extern "C" void InputField_LayoutComplete_m1183 ();
extern "C" void InputField_GraphicUpdateComplete_m1184 ();
extern "C" void InputField_UpdateGeometry_m1185 ();
extern "C" void InputField_AssignPositioningIfNeeded_m1186 ();
extern "C" void InputField_OnFillVBO_m1187 ();
extern "C" void InputField_GenerateCursor_m1188 ();
extern "C" void InputField_CreateCursorVerts_m1189 ();
extern "C" void InputField_SumLineHeights_m1190 ();
extern "C" void InputField_GenerateHightlight_m1191 ();
extern "C" void InputField_Validate_m1192 ();
extern "C" void InputField_ActivateInputField_m1193 ();
extern "C" void InputField_ActivateInputFieldInternal_m1194 ();
extern "C" void InputField_OnSelect_m1195 ();
extern "C" void InputField_OnPointerClick_m1196 ();
extern "C" void InputField_DeactivateInputField_m1197 ();
extern "C" void InputField_OnDeselect_m1198 ();
extern "C" void InputField_OnSubmit_m1199 ();
extern "C" void InputField_EnforceContentType_m1200 ();
extern "C" void InputField_SetToCustomIfContentTypeIsNot_m1201 ();
extern "C" void InputField_SetToCustom_m1202 ();
extern "C" void InputField_DoStateTransition_m1203 ();
extern "C" void InputField_UnityEngine_UI_ICanvasElement_IsDestroyed_m1204 ();
extern "C" void InputField_UnityEngine_UI_ICanvasElement_get_transform_m1205 ();
extern "C" void Mask__ctor_m1206 ();
extern "C" void Mask_get_rectTransform_m1207 ();
extern "C" void Mask_get_showMaskGraphic_m1208 ();
extern "C" void Mask_set_showMaskGraphic_m1209 ();
extern "C" void Mask_get_graphic_m1210 ();
extern "C" void Mask_MaskEnabled_m1211 ();
extern "C" void Mask_OnSiblingGraphicEnabledDisabled_m1212 ();
extern "C" void Mask_OnEnable_m1213 ();
extern "C" void Mask_OnDisable_m1214 ();
extern "C" void Mask_IsRaycastLocationValid_m1215 ();
extern "C" void Mask_GetModifiedMaterial_m1216 ();
extern "C" void CullStateChangedEvent__ctor_m1217 ();
extern "C" void MaskableGraphic__ctor_m1218 ();
extern "C" void MaskableGraphic_get_onCullStateChanged_m1219 ();
extern "C" void MaskableGraphic_set_onCullStateChanged_m1220 ();
extern "C" void MaskableGraphic_get_maskable_m1221 ();
extern "C" void MaskableGraphic_set_maskable_m1222 ();
extern "C" void MaskableGraphic_GetModifiedMaterial_m1223 ();
extern "C" void MaskableGraphic_Cull_m1224 ();
extern "C" void MaskableGraphic_SetClipRect_m1225 ();
extern "C" void MaskableGraphic_OnEnable_m1226 ();
extern "C" void MaskableGraphic_OnDisable_m1227 ();
extern "C" void MaskableGraphic_OnTransformParentChanged_m1228 ();
extern "C" void MaskableGraphic_ParentMaskStateChanged_m1229 ();
extern "C" void MaskableGraphic_OnCanvasHierarchyChanged_m1230 ();
extern "C" void MaskableGraphic_get_canvasRect_m1231 ();
extern "C" void MaskableGraphic_UpdateClipParent_m1232 ();
extern "C" void MaskableGraphic_RecalculateClipping_m1233 ();
extern "C" void MaskableGraphic_RecalculateMasking_m1234 ();
extern "C" void MaskableGraphic_UnityEngine_UI_IClippable_get_rectTransform_m1235 ();
extern "C" void MaskUtilities__ctor_m1236 ();
extern "C" void MaskUtilities_Notify2DMaskStateChanged_m1237 ();
extern "C" void MaskUtilities_NotifyStencilStateChanged_m1238 ();
extern "C" void MaskUtilities_FindRootSortOverrideCanvas_m1239 ();
extern "C" void MaskUtilities_GetStencilDepth_m1240 ();
extern "C" void MaskUtilities_GetRectMaskForClippable_m1241 ();
extern "C" void MaskUtilities_GetRectMasksForClip_m1242 ();
extern "C" void Misc_Destroy_m1243 ();
extern "C" void Misc_DestroyImmediate_m1244 ();
extern "C" void Navigation_get_mode_m1245 ();
extern "C" void Navigation_set_mode_m1246 ();
extern "C" void Navigation_get_selectOnUp_m1247 ();
extern "C" void Navigation_set_selectOnUp_m1248 ();
extern "C" void Navigation_get_selectOnDown_m1249 ();
extern "C" void Navigation_set_selectOnDown_m1250 ();
extern "C" void Navigation_get_selectOnLeft_m1251 ();
extern "C" void Navigation_set_selectOnLeft_m1252 ();
extern "C" void Navigation_get_selectOnRight_m1253 ();
extern "C" void Navigation_set_selectOnRight_m1254 ();
extern "C" void Navigation_get_defaultNavigation_m1255 ();
extern "C" void RawImage__ctor_m1256 ();
extern "C" void RawImage_get_mainTexture_m1257 ();
extern "C" void RawImage_get_texture_m1258 ();
extern "C" void RawImage_set_texture_m1259 ();
extern "C" void RawImage_get_uvRect_m1260 ();
extern "C" void RawImage_set_uvRect_m1261 ();
extern "C" void RawImage_SetNativeSize_m1262 ();
extern "C" void RawImage_OnPopulateMesh_m1263 ();
extern "C" void RectMask2D__ctor_m1264 ();
extern "C" void RectMask2D_get_canvasRect_m1265 ();
extern "C" void RectMask2D_get_rectTransform_m1266 ();
extern "C" void RectMask2D_OnEnable_m1267 ();
extern "C" void RectMask2D_OnDisable_m1268 ();
extern "C" void RectMask2D_IsRaycastLocationValid_m1269 ();
extern "C" void RectMask2D_PerformClipping_m1270 ();
extern "C" void RectMask2D_AddClippable_m1271 ();
extern "C" void RectMask2D_RemoveClippable_m1272 ();
extern "C" void RectMask2D_OnTransformParentChanged_m1273 ();
extern "C" void RectMask2D_OnCanvasHierarchyChanged_m1274 ();
extern "C" void ScrollEvent__ctor_m1275 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5__ctor_m1276 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1277 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1278 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_MoveNext_m1279 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_Dispose_m1280 ();
extern "C" void U3CClickRepeatU3Ec__Iterator5_Reset_m1281 ();
extern "C" void Scrollbar__ctor_m1282 ();
extern "C" void Scrollbar_get_handleRect_m1283 ();
extern "C" void Scrollbar_set_handleRect_m1284 ();
extern "C" void Scrollbar_get_direction_m1285 ();
extern "C" void Scrollbar_set_direction_m1286 ();
extern "C" void Scrollbar_get_value_m1287 ();
extern "C" void Scrollbar_set_value_m1288 ();
extern "C" void Scrollbar_get_size_m1289 ();
extern "C" void Scrollbar_set_size_m1290 ();
extern "C" void Scrollbar_get_numberOfSteps_m1291 ();
extern "C" void Scrollbar_set_numberOfSteps_m1292 ();
extern "C" void Scrollbar_get_onValueChanged_m1293 ();
extern "C" void Scrollbar_set_onValueChanged_m1294 ();
extern "C" void Scrollbar_get_stepSize_m1295 ();
extern "C" void Scrollbar_Rebuild_m1296 ();
extern "C" void Scrollbar_LayoutComplete_m1297 ();
extern "C" void Scrollbar_GraphicUpdateComplete_m1298 ();
extern "C" void Scrollbar_OnEnable_m1299 ();
extern "C" void Scrollbar_OnDisable_m1300 ();
extern "C" void Scrollbar_UpdateCachedReferences_m1301 ();
extern "C" void Scrollbar_Set_m1302 ();
extern "C" void Scrollbar_Set_m1303 ();
extern "C" void Scrollbar_OnRectTransformDimensionsChange_m1304 ();
extern "C" void Scrollbar_get_axis_m1305 ();
extern "C" void Scrollbar_get_reverseValue_m1306 ();
extern "C" void Scrollbar_UpdateVisuals_m1307 ();
extern "C" void Scrollbar_UpdateDrag_m1308 ();
extern "C" void Scrollbar_MayDrag_m1309 ();
extern "C" void Scrollbar_OnBeginDrag_m1310 ();
extern "C" void Scrollbar_OnDrag_m1311 ();
extern "C" void Scrollbar_OnPointerDown_m1312 ();
extern "C" void Scrollbar_ClickRepeat_m1313 ();
extern "C" void Scrollbar_OnPointerUp_m1314 ();
extern "C" void Scrollbar_OnMove_m1315 ();
extern "C" void Scrollbar_FindSelectableOnLeft_m1316 ();
extern "C" void Scrollbar_FindSelectableOnRight_m1317 ();
extern "C" void Scrollbar_FindSelectableOnUp_m1318 ();
extern "C" void Scrollbar_FindSelectableOnDown_m1319 ();
extern "C" void Scrollbar_OnInitializePotentialDrag_m1320 ();
extern "C" void Scrollbar_SetDirection_m1321 ();
extern "C" void Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m1322 ();
extern "C" void Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m1323 ();
extern "C" void ScrollRectEvent__ctor_m1324 ();
extern "C" void ScrollRect__ctor_m1325 ();
extern "C" void ScrollRect_get_content_m1326 ();
extern "C" void ScrollRect_set_content_m1327 ();
extern "C" void ScrollRect_get_horizontal_m1328 ();
extern "C" void ScrollRect_set_horizontal_m1329 ();
extern "C" void ScrollRect_get_vertical_m1330 ();
extern "C" void ScrollRect_set_vertical_m1331 ();
extern "C" void ScrollRect_get_movementType_m1332 ();
extern "C" void ScrollRect_set_movementType_m1333 ();
extern "C" void ScrollRect_get_elasticity_m1334 ();
extern "C" void ScrollRect_set_elasticity_m1335 ();
extern "C" void ScrollRect_get_inertia_m1336 ();
extern "C" void ScrollRect_set_inertia_m1337 ();
extern "C" void ScrollRect_get_decelerationRate_m1338 ();
extern "C" void ScrollRect_set_decelerationRate_m1339 ();
extern "C" void ScrollRect_get_scrollSensitivity_m1340 ();
extern "C" void ScrollRect_set_scrollSensitivity_m1341 ();
extern "C" void ScrollRect_get_viewport_m1342 ();
extern "C" void ScrollRect_set_viewport_m1343 ();
extern "C" void ScrollRect_get_horizontalScrollbar_m1344 ();
extern "C" void ScrollRect_set_horizontalScrollbar_m1345 ();
extern "C" void ScrollRect_get_verticalScrollbar_m1346 ();
extern "C" void ScrollRect_set_verticalScrollbar_m1347 ();
extern "C" void ScrollRect_get_horizontalScrollbarVisibility_m1348 ();
extern "C" void ScrollRect_set_horizontalScrollbarVisibility_m1349 ();
extern "C" void ScrollRect_get_verticalScrollbarVisibility_m1350 ();
extern "C" void ScrollRect_set_verticalScrollbarVisibility_m1351 ();
extern "C" void ScrollRect_get_horizontalScrollbarSpacing_m1352 ();
extern "C" void ScrollRect_set_horizontalScrollbarSpacing_m1353 ();
extern "C" void ScrollRect_get_verticalScrollbarSpacing_m1354 ();
extern "C" void ScrollRect_set_verticalScrollbarSpacing_m1355 ();
extern "C" void ScrollRect_get_onValueChanged_m1356 ();
extern "C" void ScrollRect_set_onValueChanged_m1357 ();
extern "C" void ScrollRect_get_viewRect_m1358 ();
extern "C" void ScrollRect_get_velocity_m1359 ();
extern "C" void ScrollRect_set_velocity_m1360 ();
extern "C" void ScrollRect_get_rectTransform_m1361 ();
extern "C" void ScrollRect_Rebuild_m1362 ();
extern "C" void ScrollRect_LayoutComplete_m1363 ();
extern "C" void ScrollRect_GraphicUpdateComplete_m1364 ();
extern "C" void ScrollRect_UpdateCachedData_m1365 ();
extern "C" void ScrollRect_OnEnable_m1366 ();
extern "C" void ScrollRect_OnDisable_m1367 ();
extern "C" void ScrollRect_IsActive_m1368 ();
extern "C" void ScrollRect_EnsureLayoutHasRebuilt_m1369 ();
extern "C" void ScrollRect_StopMovement_m1370 ();
extern "C" void ScrollRect_OnScroll_m1371 ();
extern "C" void ScrollRect_OnInitializePotentialDrag_m1372 ();
extern "C" void ScrollRect_OnBeginDrag_m1373 ();
extern "C" void ScrollRect_OnEndDrag_m1374 ();
extern "C" void ScrollRect_OnDrag_m1375 ();
extern "C" void ScrollRect_SetContentAnchoredPosition_m1376 ();
extern "C" void ScrollRect_LateUpdate_m1377 ();
extern "C" void ScrollRect_UpdatePrevData_m1378 ();
extern "C" void ScrollRect_UpdateScrollbars_m1379 ();
extern "C" void ScrollRect_get_normalizedPosition_m1380 ();
extern "C" void ScrollRect_set_normalizedPosition_m1381 ();
extern "C" void ScrollRect_get_horizontalNormalizedPosition_m1382 ();
extern "C" void ScrollRect_set_horizontalNormalizedPosition_m1383 ();
extern "C" void ScrollRect_get_verticalNormalizedPosition_m1384 ();
extern "C" void ScrollRect_set_verticalNormalizedPosition_m1385 ();
extern "C" void ScrollRect_SetHorizontalNormalizedPosition_m1386 ();
extern "C" void ScrollRect_SetVerticalNormalizedPosition_m1387 ();
extern "C" void ScrollRect_SetNormalizedPosition_m1388 ();
extern "C" void ScrollRect_RubberDelta_m1389 ();
extern "C" void ScrollRect_OnRectTransformDimensionsChange_m1390 ();
extern "C" void ScrollRect_get_hScrollingNeeded_m1391 ();
extern "C" void ScrollRect_get_vScrollingNeeded_m1392 ();
extern "C" void ScrollRect_CalculateLayoutInputHorizontal_m1393 ();
extern "C" void ScrollRect_CalculateLayoutInputVertical_m1394 ();
extern "C" void ScrollRect_get_minWidth_m1395 ();
extern "C" void ScrollRect_get_preferredWidth_m1396 ();
extern "C" void ScrollRect_get_flexibleWidth_m1397 ();
extern "C" void ScrollRect_set_flexibleWidth_m1398 ();
extern "C" void ScrollRect_get_minHeight_m1399 ();
extern "C" void ScrollRect_get_preferredHeight_m1400 ();
extern "C" void ScrollRect_get_flexibleHeight_m1401 ();
extern "C" void ScrollRect_get_layoutPriority_m1402 ();
extern "C" void ScrollRect_SetLayoutHorizontal_m1403 ();
extern "C" void ScrollRect_SetLayoutVertical_m1404 ();
extern "C" void ScrollRect_UpdateScrollbarVisibility_m1405 ();
extern "C" void ScrollRect_UpdateScrollbarLayout_m1406 ();
extern "C" void ScrollRect_UpdateBounds_m1407 ();
extern "C" void ScrollRect_GetBounds_m1408 ();
extern "C" void ScrollRect_CalculateOffset_m1409 ();
extern "C" void ScrollRect_SetDirty_m1410 ();
extern "C" void ScrollRect_SetDirtyCaching_m1411 ();
extern "C" void ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1412 ();
extern "C" void ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1413 ();
extern "C" void Selectable__ctor_m1414 ();
extern "C" void Selectable__cctor_m1415 ();
extern "C" void Selectable_get_allSelectables_m1416 ();
extern "C" void Selectable_get_navigation_m1417 ();
extern "C" void Selectable_set_navigation_m1418 ();
extern "C" void Selectable_get_transition_m1419 ();
extern "C" void Selectable_set_transition_m1420 ();
extern "C" void Selectable_get_colors_m1421 ();
extern "C" void Selectable_set_colors_m1422 ();
extern "C" void Selectable_get_spriteState_m1423 ();
extern "C" void Selectable_set_spriteState_m1424 ();
extern "C" void Selectable_get_animationTriggers_m1425 ();
extern "C" void Selectable_set_animationTriggers_m1426 ();
extern "C" void Selectable_get_targetGraphic_m1427 ();
extern "C" void Selectable_set_targetGraphic_m1428 ();
extern "C" void Selectable_get_interactable_m1429 ();
extern "C" void Selectable_set_interactable_m1430 ();
extern "C" void Selectable_get_isPointerInside_m1431 ();
extern "C" void Selectable_set_isPointerInside_m1432 ();
extern "C" void Selectable_get_isPointerDown_m1433 ();
extern "C" void Selectable_set_isPointerDown_m1434 ();
extern "C" void Selectable_get_hasSelection_m1435 ();
extern "C" void Selectable_set_hasSelection_m1436 ();
extern "C" void Selectable_get_image_m1437 ();
extern "C" void Selectable_set_image_m1438 ();
extern "C" void Selectable_get_animator_m1439 ();
extern "C" void Selectable_Awake_m1440 ();
extern "C" void Selectable_OnCanvasGroupChanged_m1441 ();
extern "C" void Selectable_IsInteractable_m1442 ();
extern "C" void Selectable_OnDidApplyAnimationProperties_m1443 ();
extern "C" void Selectable_OnEnable_m1444 ();
extern "C" void Selectable_OnSetProperty_m1445 ();
extern "C" void Selectable_OnDisable_m1446 ();
extern "C" void Selectable_get_currentSelectionState_m1447 ();
extern "C" void Selectable_InstantClearState_m1448 ();
extern "C" void Selectable_DoStateTransition_m1449 ();
extern "C" void Selectable_FindSelectable_m1450 ();
extern "C" void Selectable_GetPointOnRectEdge_m1451 ();
extern "C" void Selectable_Navigate_m1452 ();
extern "C" void Selectable_FindSelectableOnLeft_m1453 ();
extern "C" void Selectable_FindSelectableOnRight_m1454 ();
extern "C" void Selectable_FindSelectableOnUp_m1455 ();
extern "C" void Selectable_FindSelectableOnDown_m1456 ();
extern "C" void Selectable_OnMove_m1457 ();
extern "C" void Selectable_StartColorTween_m1458 ();
extern "C" void Selectable_DoSpriteSwap_m1459 ();
extern "C" void Selectable_TriggerAnimation_m1460 ();
extern "C" void Selectable_IsHighlighted_m1461 ();
extern "C" void Selectable_IsPressed_m1462 ();
extern "C" void Selectable_IsPressed_m1463 ();
extern "C" void Selectable_UpdateSelectionState_m1464 ();
extern "C" void Selectable_EvaluateAndTransitionToSelectionState_m1465 ();
extern "C" void Selectable_InternalEvaluateAndTransitionToSelectionState_m1466 ();
extern "C" void Selectable_OnPointerDown_m1467 ();
extern "C" void Selectable_OnPointerUp_m1468 ();
extern "C" void Selectable_OnPointerEnter_m1469 ();
extern "C" void Selectable_OnPointerExit_m1470 ();
extern "C" void Selectable_OnSelect_m1471 ();
extern "C" void Selectable_OnDeselect_m1472 ();
extern "C" void Selectable_Select_m1473 ();
extern "C" void SetPropertyUtility_SetColor_m1474 ();
extern "C" void SliderEvent__ctor_m1475 ();
extern "C" void Slider__ctor_m1476 ();
extern "C" void Slider_get_fillRect_m1477 ();
extern "C" void Slider_set_fillRect_m1478 ();
extern "C" void Slider_get_handleRect_m1479 ();
extern "C" void Slider_set_handleRect_m1480 ();
extern "C" void Slider_get_direction_m1481 ();
extern "C" void Slider_set_direction_m1482 ();
extern "C" void Slider_get_minValue_m1483 ();
extern "C" void Slider_set_minValue_m1484 ();
extern "C" void Slider_get_maxValue_m1485 ();
extern "C" void Slider_set_maxValue_m1486 ();
extern "C" void Slider_get_wholeNumbers_m1487 ();
extern "C" void Slider_set_wholeNumbers_m1488 ();
extern "C" void Slider_get_value_m1489 ();
extern "C" void Slider_set_value_m1490 ();
extern "C" void Slider_get_normalizedValue_m1491 ();
extern "C" void Slider_set_normalizedValue_m1492 ();
extern "C" void Slider_get_onValueChanged_m1493 ();
extern "C" void Slider_set_onValueChanged_m1494 ();
extern "C" void Slider_get_stepSize_m1495 ();
extern "C" void Slider_Rebuild_m1496 ();
extern "C" void Slider_LayoutComplete_m1497 ();
extern "C" void Slider_GraphicUpdateComplete_m1498 ();
extern "C" void Slider_OnEnable_m1499 ();
extern "C" void Slider_OnDisable_m1500 ();
extern "C" void Slider_OnDidApplyAnimationProperties_m1501 ();
extern "C" void Slider_UpdateCachedReferences_m1502 ();
extern "C" void Slider_ClampValue_m1503 ();
extern "C" void Slider_Set_m1504 ();
extern "C" void Slider_Set_m1505 ();
extern "C" void Slider_OnRectTransformDimensionsChange_m1506 ();
extern "C" void Slider_get_axis_m1507 ();
extern "C" void Slider_get_reverseValue_m1508 ();
extern "C" void Slider_UpdateVisuals_m1509 ();
extern "C" void Slider_UpdateDrag_m1510 ();
extern "C" void Slider_MayDrag_m1511 ();
extern "C" void Slider_OnPointerDown_m1512 ();
extern "C" void Slider_OnDrag_m1513 ();
extern "C" void Slider_OnMove_m1514 ();
extern "C" void Slider_FindSelectableOnLeft_m1515 ();
extern "C" void Slider_FindSelectableOnRight_m1516 ();
extern "C" void Slider_FindSelectableOnUp_m1517 ();
extern "C" void Slider_FindSelectableOnDown_m1518 ();
extern "C" void Slider_OnInitializePotentialDrag_m1519 ();
extern "C" void Slider_SetDirection_m1520 ();
extern "C" void Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521 ();
extern "C" void Slider_UnityEngine_UI_ICanvasElement_get_transform_m1522 ();
extern "C" void SpriteState_get_highlightedSprite_m1523 ();
extern "C" void SpriteState_set_highlightedSprite_m1524 ();
extern "C" void SpriteState_get_pressedSprite_m1525 ();
extern "C" void SpriteState_set_pressedSprite_m1526 ();
extern "C" void SpriteState_get_disabledSprite_m1527 ();
extern "C" void SpriteState_set_disabledSprite_m1528 ();
extern "C" void MatEntry__ctor_m1529 ();
extern "C" void StencilMaterial__cctor_m1530 ();
extern "C" void StencilMaterial_Add_m1531 ();
extern "C" void StencilMaterial_Add_m1532 ();
extern "C" void StencilMaterial_Add_m1533 ();
extern "C" void StencilMaterial_Remove_m1534 ();
extern "C" void StencilMaterial_ClearAll_m1535 ();
extern "C" void Text__ctor_m1536 ();
extern "C" void Text__cctor_m1537 ();
extern "C" void Text_get_cachedTextGenerator_m1538 ();
extern "C" void Text_get_cachedTextGeneratorForLayout_m1539 ();
extern "C" void Text_get_mainTexture_m1540 ();
extern "C" void Text_FontTextureChanged_m1541 ();
extern "C" void Text_get_font_m1542 ();
extern "C" void Text_set_font_m1543 ();
extern "C" void Text_get_text_m1544 ();
extern "C" void Text_set_text_m1545 ();
extern "C" void Text_get_supportRichText_m1546 ();
extern "C" void Text_set_supportRichText_m1547 ();
extern "C" void Text_get_resizeTextForBestFit_m1548 ();
extern "C" void Text_set_resizeTextForBestFit_m1549 ();
extern "C" void Text_get_resizeTextMinSize_m1550 ();
extern "C" void Text_set_resizeTextMinSize_m1551 ();
extern "C" void Text_get_resizeTextMaxSize_m1552 ();
extern "C" void Text_set_resizeTextMaxSize_m1553 ();
extern "C" void Text_get_alignment_m1554 ();
extern "C" void Text_set_alignment_m1555 ();
extern "C" void Text_get_fontSize_m1556 ();
extern "C" void Text_set_fontSize_m1557 ();
extern "C" void Text_get_horizontalOverflow_m1558 ();
extern "C" void Text_set_horizontalOverflow_m1559 ();
extern "C" void Text_get_verticalOverflow_m1560 ();
extern "C" void Text_set_verticalOverflow_m1561 ();
extern "C" void Text_get_lineSpacing_m1562 ();
extern "C" void Text_set_lineSpacing_m1563 ();
extern "C" void Text_get_fontStyle_m1564 ();
extern "C" void Text_set_fontStyle_m1565 ();
extern "C" void Text_get_pixelsPerUnit_m1566 ();
extern "C" void Text_OnEnable_m1567 ();
extern "C" void Text_OnDisable_m1568 ();
extern "C" void Text_UpdateGeometry_m1569 ();
extern "C" void Text_GetGenerationSettings_m1570 ();
extern "C" void Text_GetTextAnchorPivot_m1571 ();
extern "C" void Text_OnPopulateMesh_m1572 ();
extern "C" void Text_CalculateLayoutInputHorizontal_m1573 ();
extern "C" void Text_CalculateLayoutInputVertical_m1574 ();
extern "C" void Text_get_minWidth_m1575 ();
extern "C" void Text_get_preferredWidth_m1576 ();
extern "C" void Text_get_flexibleWidth_m1577 ();
extern "C" void Text_get_minHeight_m1578 ();
extern "C" void Text_get_preferredHeight_m1579 ();
extern "C" void Text_get_flexibleHeight_m1580 ();
extern "C" void Text_get_layoutPriority_m1581 ();
extern "C" void ToggleEvent__ctor_m1582 ();
extern "C" void Toggle__ctor_m1583 ();
extern "C" void Toggle_get_group_m1584 ();
extern "C" void Toggle_set_group_m1585 ();
extern "C" void Toggle_Rebuild_m1586 ();
extern "C" void Toggle_LayoutComplete_m1587 ();
extern "C" void Toggle_GraphicUpdateComplete_m1588 ();
extern "C" void Toggle_OnEnable_m1589 ();
extern "C" void Toggle_OnDisable_m1590 ();
extern "C" void Toggle_OnDidApplyAnimationProperties_m1591 ();
extern "C" void Toggle_SetToggleGroup_m1592 ();
extern "C" void Toggle_get_isOn_m1593 ();
extern "C" void Toggle_set_isOn_m1594 ();
extern "C" void Toggle_Set_m1595 ();
extern "C" void Toggle_Set_m1596 ();
extern "C" void Toggle_PlayEffect_m1597 ();
extern "C" void Toggle_Start_m1598 ();
extern "C" void Toggle_InternalToggle_m1599 ();
extern "C" void Toggle_OnPointerClick_m1600 ();
extern "C" void Toggle_OnSubmit_m1601 ();
extern "C" void Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1602 ();
extern "C" void Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1603 ();
extern "C" void ToggleGroup__ctor_m1604 ();
extern "C" void ToggleGroup_get_allowSwitchOff_m1605 ();
extern "C" void ToggleGroup_set_allowSwitchOff_m1606 ();
extern "C" void ToggleGroup_ValidateToggleIsInGroup_m1607 ();
extern "C" void ToggleGroup_NotifyToggleOn_m1608 ();
extern "C" void ToggleGroup_UnregisterToggle_m1609 ();
extern "C" void ToggleGroup_RegisterToggle_m1610 ();
extern "C" void ToggleGroup_AnyTogglesOn_m1611 ();
extern "C" void ToggleGroup_ActiveToggles_m1612 ();
extern "C" void ToggleGroup_SetAllTogglesOff_m1613 ();
extern "C" void ToggleGroup_U3CAnyTogglesOnU3Em__4_m1614 ();
extern "C" void ToggleGroup_U3CActiveTogglesU3Em__5_m1615 ();
extern "C" void ClipperRegistry__ctor_m1616 ();
extern "C" void ClipperRegistry_get_instance_m1617 ();
extern "C" void ClipperRegistry_Cull_m1618 ();
extern "C" void ClipperRegistry_Register_m1619 ();
extern "C" void ClipperRegistry_Unregister_m1620 ();
extern "C" void Clipping_FindCullAndClipWorldRect_m1621 ();
extern "C" void Clipping_RectIntersect_m1622 ();
extern "C" void RectangularVertexClipper__ctor_m1623 ();
extern "C" void RectangularVertexClipper_GetCanvasRect_m1624 ();
extern "C" void AspectRatioFitter__ctor_m1625 ();
extern "C" void AspectRatioFitter_get_aspectMode_m1626 ();
extern "C" void AspectRatioFitter_set_aspectMode_m1627 ();
extern "C" void AspectRatioFitter_get_aspectRatio_m1628 ();
extern "C" void AspectRatioFitter_set_aspectRatio_m1629 ();
extern "C" void AspectRatioFitter_get_rectTransform_m1630 ();
extern "C" void AspectRatioFitter_OnEnable_m1631 ();
extern "C" void AspectRatioFitter_OnDisable_m1632 ();
extern "C" void AspectRatioFitter_OnRectTransformDimensionsChange_m1633 ();
extern "C" void AspectRatioFitter_UpdateRect_m1634 ();
extern "C" void AspectRatioFitter_GetSizeDeltaToProduceSize_m1635 ();
extern "C" void AspectRatioFitter_GetParentSize_m1636 ();
extern "C" void AspectRatioFitter_SetLayoutHorizontal_m1637 ();
extern "C" void AspectRatioFitter_SetLayoutVertical_m1638 ();
extern "C" void AspectRatioFitter_SetDirty_m1639 ();
extern "C" void CanvasScaler__ctor_m1640 ();
extern "C" void CanvasScaler_get_uiScaleMode_m1641 ();
extern "C" void CanvasScaler_set_uiScaleMode_m1642 ();
extern "C" void CanvasScaler_get_referencePixelsPerUnit_m1643 ();
extern "C" void CanvasScaler_set_referencePixelsPerUnit_m1644 ();
extern "C" void CanvasScaler_get_scaleFactor_m1645 ();
extern "C" void CanvasScaler_set_scaleFactor_m1646 ();
extern "C" void CanvasScaler_get_referenceResolution_m1647 ();
extern "C" void CanvasScaler_set_referenceResolution_m1648 ();
extern "C" void CanvasScaler_get_screenMatchMode_m1649 ();
extern "C" void CanvasScaler_set_screenMatchMode_m1650 ();
extern "C" void CanvasScaler_get_matchWidthOrHeight_m1651 ();
extern "C" void CanvasScaler_set_matchWidthOrHeight_m1652 ();
extern "C" void CanvasScaler_get_physicalUnit_m1653 ();
extern "C" void CanvasScaler_set_physicalUnit_m1654 ();
extern "C" void CanvasScaler_get_fallbackScreenDPI_m1655 ();
extern "C" void CanvasScaler_set_fallbackScreenDPI_m1656 ();
extern "C" void CanvasScaler_get_defaultSpriteDPI_m1657 ();
extern "C" void CanvasScaler_set_defaultSpriteDPI_m1658 ();
extern "C" void CanvasScaler_get_dynamicPixelsPerUnit_m1659 ();
extern "C" void CanvasScaler_set_dynamicPixelsPerUnit_m1660 ();
extern "C" void CanvasScaler_OnEnable_m1661 ();
extern "C" void CanvasScaler_OnDisable_m1662 ();
extern "C" void CanvasScaler_Update_m1663 ();
extern "C" void CanvasScaler_Handle_m1664 ();
extern "C" void CanvasScaler_HandleWorldCanvas_m1665 ();
extern "C" void CanvasScaler_HandleConstantPixelSize_m1666 ();
extern "C" void CanvasScaler_HandleScaleWithScreenSize_m1667 ();
extern "C" void CanvasScaler_HandleConstantPhysicalSize_m1668 ();
extern "C" void CanvasScaler_SetScaleFactor_m1669 ();
extern "C" void CanvasScaler_SetReferencePixelsPerUnit_m1670 ();
extern "C" void ContentSizeFitter__ctor_m1671 ();
extern "C" void ContentSizeFitter_get_horizontalFit_m1672 ();
extern "C" void ContentSizeFitter_set_horizontalFit_m1673 ();
extern "C" void ContentSizeFitter_get_verticalFit_m1674 ();
extern "C" void ContentSizeFitter_set_verticalFit_m1675 ();
extern "C" void ContentSizeFitter_get_rectTransform_m1676 ();
extern "C" void ContentSizeFitter_OnEnable_m1677 ();
extern "C" void ContentSizeFitter_OnDisable_m1678 ();
extern "C" void ContentSizeFitter_OnRectTransformDimensionsChange_m1679 ();
extern "C" void ContentSizeFitter_HandleSelfFittingAlongAxis_m1680 ();
extern "C" void ContentSizeFitter_SetLayoutHorizontal_m1681 ();
extern "C" void ContentSizeFitter_SetLayoutVertical_m1682 ();
extern "C" void ContentSizeFitter_SetDirty_m1683 ();
extern "C" void GridLayoutGroup__ctor_m1684 ();
extern "C" void GridLayoutGroup_get_startCorner_m1685 ();
extern "C" void GridLayoutGroup_set_startCorner_m1686 ();
extern "C" void GridLayoutGroup_get_startAxis_m1687 ();
extern "C" void GridLayoutGroup_set_startAxis_m1688 ();
extern "C" void GridLayoutGroup_get_cellSize_m1689 ();
extern "C" void GridLayoutGroup_set_cellSize_m1690 ();
extern "C" void GridLayoutGroup_get_spacing_m1691 ();
extern "C" void GridLayoutGroup_set_spacing_m1692 ();
extern "C" void GridLayoutGroup_get_constraint_m1693 ();
extern "C" void GridLayoutGroup_set_constraint_m1694 ();
extern "C" void GridLayoutGroup_get_constraintCount_m1695 ();
extern "C" void GridLayoutGroup_set_constraintCount_m1696 ();
extern "C" void GridLayoutGroup_CalculateLayoutInputHorizontal_m1697 ();
extern "C" void GridLayoutGroup_CalculateLayoutInputVertical_m1698 ();
extern "C" void GridLayoutGroup_SetLayoutHorizontal_m1699 ();
extern "C" void GridLayoutGroup_SetLayoutVertical_m1700 ();
extern "C" void GridLayoutGroup_SetCellsAlongAxis_m1701 ();
extern "C" void HorizontalLayoutGroup__ctor_m1702 ();
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1703 ();
extern "C" void HorizontalLayoutGroup_CalculateLayoutInputVertical_m1704 ();
extern "C" void HorizontalLayoutGroup_SetLayoutHorizontal_m1705 ();
extern "C" void HorizontalLayoutGroup_SetLayoutVertical_m1706 ();
extern "C" void HorizontalOrVerticalLayoutGroup__ctor_m1707 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_spacing_m1708 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_spacing_m1709 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1710 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1711 ();
extern "C" void HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1712 ();
extern "C" void HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1713 ();
extern "C" void HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1714 ();
extern "C" void HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1715 ();
extern "C" void LayoutElement__ctor_m1716 ();
extern "C" void LayoutElement_get_ignoreLayout_m1717 ();
extern "C" void LayoutElement_set_ignoreLayout_m1718 ();
extern "C" void LayoutElement_CalculateLayoutInputHorizontal_m1719 ();
extern "C" void LayoutElement_CalculateLayoutInputVertical_m1720 ();
extern "C" void LayoutElement_get_minWidth_m1721 ();
extern "C" void LayoutElement_set_minWidth_m1722 ();
extern "C" void LayoutElement_get_minHeight_m1723 ();
extern "C" void LayoutElement_set_minHeight_m1724 ();
extern "C" void LayoutElement_get_preferredWidth_m1725 ();
extern "C" void LayoutElement_set_preferredWidth_m1726 ();
extern "C" void LayoutElement_get_preferredHeight_m1727 ();
extern "C" void LayoutElement_set_preferredHeight_m1728 ();
extern "C" void LayoutElement_get_flexibleWidth_m1729 ();
extern "C" void LayoutElement_set_flexibleWidth_m1730 ();
extern "C" void LayoutElement_get_flexibleHeight_m1731 ();
extern "C" void LayoutElement_set_flexibleHeight_m1732 ();
extern "C" void LayoutElement_get_layoutPriority_m1733 ();
extern "C" void LayoutElement_OnEnable_m1734 ();
extern "C" void LayoutElement_OnTransformParentChanged_m1735 ();
extern "C" void LayoutElement_OnDisable_m1736 ();
extern "C" void LayoutElement_OnDidApplyAnimationProperties_m1737 ();
extern "C" void LayoutElement_OnBeforeTransformParentChanged_m1738 ();
extern "C" void LayoutElement_SetDirty_m1739 ();
extern "C" void LayoutGroup__ctor_m1740 ();
extern "C" void LayoutGroup_get_padding_m1741 ();
extern "C" void LayoutGroup_set_padding_m1742 ();
extern "C" void LayoutGroup_get_childAlignment_m1743 ();
extern "C" void LayoutGroup_set_childAlignment_m1744 ();
extern "C" void LayoutGroup_get_rectTransform_m1745 ();
extern "C" void LayoutGroup_get_rectChildren_m1746 ();
extern "C" void LayoutGroup_CalculateLayoutInputHorizontal_m1747 ();
extern "C" void LayoutGroup_get_minWidth_m1748 ();
extern "C" void LayoutGroup_get_preferredWidth_m1749 ();
extern "C" void LayoutGroup_get_flexibleWidth_m1750 ();
extern "C" void LayoutGroup_get_minHeight_m1751 ();
extern "C" void LayoutGroup_get_preferredHeight_m1752 ();
extern "C" void LayoutGroup_get_flexibleHeight_m1753 ();
extern "C" void LayoutGroup_get_layoutPriority_m1754 ();
extern "C" void LayoutGroup_OnEnable_m1755 ();
extern "C" void LayoutGroup_OnDisable_m1756 ();
extern "C" void LayoutGroup_OnDidApplyAnimationProperties_m1757 ();
extern "C" void LayoutGroup_GetTotalMinSize_m1758 ();
extern "C" void LayoutGroup_GetTotalPreferredSize_m1759 ();
extern "C" void LayoutGroup_GetTotalFlexibleSize_m1760 ();
extern "C" void LayoutGroup_GetStartOffset_m1761 ();
extern "C" void LayoutGroup_SetLayoutInputForAxis_m1762 ();
extern "C" void LayoutGroup_SetChildAlongAxis_m1763 ();
extern "C" void LayoutGroup_get_isRootLayoutGroup_m1764 ();
extern "C" void LayoutGroup_OnRectTransformDimensionsChange_m1765 ();
extern "C" void LayoutGroup_OnTransformChildrenChanged_m1766 ();
extern "C" void LayoutGroup_SetDirty_m1767 ();
extern "C" void LayoutRebuilder__ctor_m1768 ();
extern "C" void LayoutRebuilder__cctor_m1769 ();
extern "C" void LayoutRebuilder_Initialize_m1770 ();
extern "C" void LayoutRebuilder_Clear_m1771 ();
extern "C" void LayoutRebuilder_ReapplyDrivenProperties_m1772 ();
extern "C" void LayoutRebuilder_get_transform_m1773 ();
extern "C" void LayoutRebuilder_IsDestroyed_m1774 ();
extern "C" void LayoutRebuilder_StripDisabledBehavioursFromList_m1775 ();
extern "C" void LayoutRebuilder_ForceRebuildLayoutImmediate_m1776 ();
extern "C" void LayoutRebuilder_Rebuild_m1777 ();
extern "C" void LayoutRebuilder_PerformLayoutControl_m1778 ();
extern "C" void LayoutRebuilder_PerformLayoutCalculation_m1779 ();
extern "C" void LayoutRebuilder_MarkLayoutForRebuild_m1780 ();
extern "C" void LayoutRebuilder_ValidLayoutGroup_m1781 ();
extern "C" void LayoutRebuilder_ValidController_m1782 ();
extern "C" void LayoutRebuilder_MarkLayoutRootForRebuild_m1783 ();
extern "C" void LayoutRebuilder_LayoutComplete_m1784 ();
extern "C" void LayoutRebuilder_GraphicUpdateComplete_m1785 ();
extern "C" void LayoutRebuilder_GetHashCode_m1786 ();
extern "C" void LayoutRebuilder_Equals_m1787 ();
extern "C" void LayoutRebuilder_ToString_m1788 ();
extern "C" void LayoutRebuilder_U3Cs_RebuildersU3Em__6_m1789 ();
extern "C" void LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__7_m1790 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__8_m1791 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__9_m1792 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__A_m1793 ();
extern "C" void LayoutRebuilder_U3CRebuildU3Em__B_m1794 ();
extern "C" void LayoutUtility_GetMinSize_m1795 ();
extern "C" void LayoutUtility_GetPreferredSize_m1796 ();
extern "C" void LayoutUtility_GetFlexibleSize_m1797 ();
extern "C" void LayoutUtility_GetMinWidth_m1798 ();
extern "C" void LayoutUtility_GetPreferredWidth_m1799 ();
extern "C" void LayoutUtility_GetFlexibleWidth_m1800 ();
extern "C" void LayoutUtility_GetMinHeight_m1801 ();
extern "C" void LayoutUtility_GetPreferredHeight_m1802 ();
extern "C" void LayoutUtility_GetFlexibleHeight_m1803 ();
extern "C" void LayoutUtility_GetLayoutProperty_m1804 ();
extern "C" void LayoutUtility_GetLayoutProperty_m1805 ();
extern "C" void LayoutUtility_U3CGetMinWidthU3Em__C_m1806 ();
extern "C" void LayoutUtility_U3CGetPreferredWidthU3Em__D_m1807 ();
extern "C" void LayoutUtility_U3CGetPreferredWidthU3Em__E_m1808 ();
extern "C" void LayoutUtility_U3CGetFlexibleWidthU3Em__F_m1809 ();
extern "C" void LayoutUtility_U3CGetMinHeightU3Em__10_m1810 ();
extern "C" void LayoutUtility_U3CGetPreferredHeightU3Em__11_m1811 ();
extern "C" void LayoutUtility_U3CGetPreferredHeightU3Em__12_m1812 ();
extern "C" void LayoutUtility_U3CGetFlexibleHeightU3Em__13_m1813 ();
extern "C" void VerticalLayoutGroup__ctor_m1814 ();
extern "C" void VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1815 ();
extern "C" void VerticalLayoutGroup_CalculateLayoutInputVertical_m1816 ();
extern "C" void VerticalLayoutGroup_SetLayoutHorizontal_m1817 ();
extern "C" void VerticalLayoutGroup_SetLayoutVertical_m1818 ();
extern "C" void VertexHelper__ctor_m1819 ();
extern "C" void VertexHelper__ctor_m1820 ();
extern "C" void VertexHelper__cctor_m1821 ();
extern "C" void VertexHelper_Clear_m1822 ();
extern "C" void VertexHelper_get_currentVertCount_m1823 ();
extern "C" void VertexHelper_get_currentIndexCount_m1824 ();
extern "C" void VertexHelper_PopulateUIVertex_m1825 ();
extern "C" void VertexHelper_SetUIVertex_m1826 ();
extern "C" void VertexHelper_FillMesh_m1827 ();
extern "C" void VertexHelper_Dispose_m1828 ();
extern "C" void VertexHelper_AddVert_m1829 ();
extern "C" void VertexHelper_AddVert_m1830 ();
extern "C" void VertexHelper_AddVert_m1831 ();
extern "C" void VertexHelper_AddTriangle_m1832 ();
extern "C" void VertexHelper_AddUIVertexQuad_m1833 ();
extern "C" void VertexHelper_AddUIVertexStream_m1834 ();
extern "C" void VertexHelper_AddUIVertexTriangleStream_m1835 ();
extern "C" void VertexHelper_GetUIVertexStream_m1836 ();
extern "C" void BaseVertexEffect__ctor_m1837 ();
extern "C" void BaseMeshEffect__ctor_m1838 ();
extern "C" void BaseMeshEffect_get_graphic_m1839 ();
extern "C" void BaseMeshEffect_OnEnable_m1840 ();
extern "C" void BaseMeshEffect_OnDisable_m1841 ();
extern "C" void BaseMeshEffect_OnDidApplyAnimationProperties_m1842 ();
extern "C" void BaseMeshEffect_ModifyMesh_m1843 ();
extern "C" void Outline__ctor_m1844 ();
extern "C" void Outline_ModifyMesh_m1845 ();
extern "C" void PositionAsUV1__ctor_m1846 ();
extern "C" void PositionAsUV1_ModifyMesh_m1847 ();
extern "C" void Shadow__ctor_m1848 ();
extern "C" void Shadow_get_effectColor_m1849 ();
extern "C" void Shadow_set_effectColor_m1850 ();
extern "C" void Shadow_get_effectDistance_m1851 ();
extern "C" void Shadow_set_effectDistance_m1852 ();
extern "C" void Shadow_get_useGraphicAlpha_m1853 ();
extern "C" void Shadow_set_useGraphicAlpha_m1854 ();
extern "C" void Shadow_ApplyShadowZeroAlloc_m1855 ();
extern "C" void Shadow_ApplyShadow_m1856 ();
extern "C" void Shadow_ModifyMesh_m1857 ();
extern "C" void AssetBundleCreateRequest__ctor_m2447 ();
extern "C" void AssetBundleCreateRequest_get_assetBundle_m2448 ();
extern "C" void AssetBundleCreateRequest_DisableCompatibilityChecks_m2449 ();
extern "C" void AssetBundleRequest__ctor_m2450 ();
extern "C" void AssetBundleRequest_get_asset_m2451 ();
extern "C" void AssetBundleRequest_get_allAssets_m2452 ();
extern "C" void AssetBundle_LoadAsset_m2453 ();
extern "C" void AssetBundle_LoadAsset_Internal_m2454 ();
extern "C" void AssetBundle_LoadAssetWithSubAssets_Internal_m2455 ();
extern "C" void WaitForSeconds__ctor_m451 ();
extern "C" void WaitForFixedUpdate__ctor_m2456 ();
extern "C" void WaitForEndOfFrame__ctor_m2284 ();
extern "C" void Coroutine__ctor_m2457 ();
extern "C" void Coroutine_ReleaseCoroutine_m2458 ();
extern "C" void Coroutine_Finalize_m2459 ();
extern "C" void ScriptableObject__ctor_m2460 ();
extern "C" void ScriptableObject_Internal_CreateScriptableObject_m2461 ();
extern "C" void ScriptableObject_CreateInstance_m2462 ();
extern "C" void ScriptableObject_CreateInstance_m2463 ();
extern "C" void ScriptableObject_CreateInstanceFromType_m2464 ();
extern "C" void UnhandledExceptionHandler__ctor_m2465 ();
extern "C" void UnhandledExceptionHandler_RegisterUECatcher_m2466 ();
extern "C" void UnhandledExceptionHandler_HandleUnhandledException_m2467 ();
extern "C" void UnhandledExceptionHandler_PrintException_m2468 ();
extern "C" void UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m2469 ();
extern "C" void GameCenterPlatform__ctor_m2470 ();
extern "C" void GameCenterPlatform__cctor_m2471 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2472 ();
extern "C" void GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2473 ();
extern "C" void GameCenterPlatform_Internal_Authenticate_m2474 ();
extern "C" void GameCenterPlatform_Internal_Authenticated_m2475 ();
extern "C" void GameCenterPlatform_Internal_UserName_m2476 ();
extern "C" void GameCenterPlatform_Internal_UserID_m2477 ();
extern "C" void GameCenterPlatform_Internal_Underage_m2478 ();
extern "C" void GameCenterPlatform_Internal_UserImage_m2479 ();
extern "C" void GameCenterPlatform_Internal_LoadFriends_m2480 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievementDescriptions_m2481 ();
extern "C" void GameCenterPlatform_Internal_LoadAchievements_m2482 ();
extern "C" void GameCenterPlatform_Internal_ReportProgress_m2483 ();
extern "C" void GameCenterPlatform_Internal_ReportScore_m2484 ();
extern "C" void GameCenterPlatform_Internal_LoadScores_m2485 ();
extern "C" void GameCenterPlatform_Internal_ShowAchievementsUI_m2486 ();
extern "C" void GameCenterPlatform_Internal_ShowLeaderboardUI_m2487 ();
extern "C" void GameCenterPlatform_Internal_LoadUsers_m2488 ();
extern "C" void GameCenterPlatform_Internal_ResetAllAchievements_m2489 ();
extern "C" void GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m2490 ();
extern "C" void GameCenterPlatform_ResetAllAchievements_m2491 ();
extern "C" void GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2492 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m2493 ();
extern "C" void GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m2494 ();
extern "C" void GameCenterPlatform_ClearAchievementDescriptions_m2495 ();
extern "C" void GameCenterPlatform_SetAchievementDescription_m2496 ();
extern "C" void GameCenterPlatform_SetAchievementDescriptionImage_m2497 ();
extern "C" void GameCenterPlatform_TriggerAchievementDescriptionCallback_m2498 ();
extern "C" void GameCenterPlatform_AuthenticateCallbackWrapper_m2499 ();
extern "C" void GameCenterPlatform_ClearFriends_m2500 ();
extern "C" void GameCenterPlatform_SetFriends_m2501 ();
extern "C" void GameCenterPlatform_SetFriendImage_m2502 ();
extern "C" void GameCenterPlatform_TriggerFriendsCallbackWrapper_m2503 ();
extern "C" void GameCenterPlatform_AchievementCallbackWrapper_m2504 ();
extern "C" void GameCenterPlatform_ProgressCallbackWrapper_m2505 ();
extern "C" void GameCenterPlatform_ScoreCallbackWrapper_m2506 ();
extern "C" void GameCenterPlatform_ScoreLoaderCallbackWrapper_m2507 ();
extern "C" void GameCenterPlatform_get_localUser_m2508 ();
extern "C" void GameCenterPlatform_PopulateLocalUser_m2509 ();
extern "C" void GameCenterPlatform_LoadAchievementDescriptions_m2510 ();
extern "C" void GameCenterPlatform_ReportProgress_m2511 ();
extern "C" void GameCenterPlatform_LoadAchievements_m2512 ();
extern "C" void GameCenterPlatform_ReportScore_m2513 ();
extern "C" void GameCenterPlatform_LoadScores_m2514 ();
extern "C" void GameCenterPlatform_LoadScores_m2515 ();
extern "C" void GameCenterPlatform_LeaderboardCallbackWrapper_m2516 ();
extern "C" void GameCenterPlatform_GetLoading_m2517 ();
extern "C" void GameCenterPlatform_VerifyAuthentication_m2518 ();
extern "C" void GameCenterPlatform_ShowAchievementsUI_m2519 ();
extern "C" void GameCenterPlatform_ShowLeaderboardUI_m2520 ();
extern "C" void GameCenterPlatform_ClearUsers_m2521 ();
extern "C" void GameCenterPlatform_SetUser_m2522 ();
extern "C" void GameCenterPlatform_SetUserImage_m2523 ();
extern "C" void GameCenterPlatform_TriggerUsersCallbackWrapper_m2524 ();
extern "C" void GameCenterPlatform_LoadUsers_m2525 ();
extern "C" void GameCenterPlatform_SafeSetUserImage_m2526 ();
extern "C" void GameCenterPlatform_SafeClearArray_m2527 ();
extern "C" void GameCenterPlatform_CreateLeaderboard_m2528 ();
extern "C" void GameCenterPlatform_CreateAchievement_m2529 ();
extern "C" void GameCenterPlatform_TriggerResetAchievementCallback_m2530 ();
extern "C" void GcLeaderboard__ctor_m2531 ();
extern "C" void GcLeaderboard_Finalize_m2532 ();
extern "C" void GcLeaderboard_Contains_m2533 ();
extern "C" void GcLeaderboard_SetScores_m2534 ();
extern "C" void GcLeaderboard_SetLocalScore_m2535 ();
extern "C" void GcLeaderboard_SetMaxRange_m2536 ();
extern "C" void GcLeaderboard_SetTitle_m2537 ();
extern "C" void GcLeaderboard_Internal_LoadScores_m2538 ();
extern "C" void GcLeaderboard_Internal_LoadScoresWithUsers_m2539 ();
extern "C" void GcLeaderboard_Loading_m2540 ();
extern "C" void GcLeaderboard_Dispose_m2541 ();
extern "C" void Mesh__ctor_m2110 ();
extern "C" void Mesh_Internal_Create_m2542 ();
extern "C" void Mesh_Clear_m2543 ();
extern "C" void Mesh_Clear_m2109 ();
extern "C" void Mesh_get_vertices_m2412 ();
extern "C" void Mesh_SetVertices_m2425 ();
extern "C" void Mesh_SetVerticesInternal_m2544 ();
extern "C" void Mesh_get_normals_m2419 ();
extern "C" void Mesh_SetNormals_m2428 ();
extern "C" void Mesh_SetNormalsInternal_m2545 ();
extern "C" void Mesh_get_tangents_m2420 ();
extern "C" void Mesh_SetTangents_m2429 ();
extern "C" void Mesh_SetTangentsInternal_m2546 ();
extern "C" void Mesh_get_uv_m2416 ();
extern "C" void Mesh_get_uv2_m2418 ();
extern "C" void Mesh_SetUVs_m2427 ();
extern "C" void Mesh_SetUVInternal_m2547 ();
extern "C" void Mesh_get_colors32_m2414 ();
extern "C" void Mesh_SetColors_m2426 ();
extern "C" void Mesh_SetColors32Internal_m2548 ();
extern "C" void Mesh_RecalculateBounds_m2431 ();
extern "C" void Mesh_SetTriangles_m2430 ();
extern "C" void Mesh_SetTrianglesInternal_m2549 ();
extern "C" void Mesh_GetIndices_m2422 ();
extern "C" void BoneWeight_get_weight0_m2550 ();
extern "C" void BoneWeight_set_weight0_m2551 ();
extern "C" void BoneWeight_get_weight1_m2552 ();
extern "C" void BoneWeight_set_weight1_m2553 ();
extern "C" void BoneWeight_get_weight2_m2554 ();
extern "C" void BoneWeight_set_weight2_m2555 ();
extern "C" void BoneWeight_get_weight3_m2556 ();
extern "C" void BoneWeight_set_weight3_m2557 ();
extern "C" void BoneWeight_get_boneIndex0_m2558 ();
extern "C" void BoneWeight_set_boneIndex0_m2559 ();
extern "C" void BoneWeight_get_boneIndex1_m2560 ();
extern "C" void BoneWeight_set_boneIndex1_m2561 ();
extern "C" void BoneWeight_get_boneIndex2_m2562 ();
extern "C" void BoneWeight_set_boneIndex2_m2563 ();
extern "C" void BoneWeight_get_boneIndex3_m2564 ();
extern "C" void BoneWeight_set_boneIndex3_m2565 ();
extern "C" void BoneWeight_GetHashCode_m2566 ();
extern "C" void BoneWeight_Equals_m2567 ();
extern "C" void BoneWeight_op_Equality_m2568 ();
extern "C" void BoneWeight_op_Inequality_m2569 ();
extern "C" void Renderer_set_enabled_m265 ();
extern "C" void Renderer_get_sortingLayerID_m1968 ();
extern "C" void Renderer_get_sortingOrder_m1969 ();
extern "C" void Graphics_DrawTexture_m2570 ();
extern "C" void Screen_get_width_m249 ();
extern "C" void Screen_get_height_m2134 ();
extern "C" void Screen_get_dpi_m2372 ();
extern "C" void GUILayer_HitTest_m2571 ();
extern "C" void GUILayer_INTERNAL_CALL_HitTest_m2572 ();
extern "C" void Texture__ctor_m2573 ();
extern "C" void Texture_Internal_GetWidth_m2574 ();
extern "C" void Texture_Internal_GetHeight_m2575 ();
extern "C" void Texture_get_width_m2576 ();
extern "C" void Texture_get_height_m2577 ();
extern "C" void Texture2D__ctor_m2578 ();
extern "C" void Texture2D_Internal_Create_m2579 ();
extern "C" void Texture2D_get_whiteTexture_m2101 ();
extern "C" void Texture2D_GetPixelBilinear_m2178 ();
extern "C" void RenderTexture_Internal_GetWidth_m2580 ();
extern "C" void RenderTexture_Internal_GetHeight_m2581 ();
extern "C" void RenderTexture_get_width_m2582 ();
extern "C" void RenderTexture_get_height_m2583 ();
extern "C" void StateChanged__ctor_m2584 ();
extern "C" void StateChanged_Invoke_m2585 ();
extern "C" void StateChanged_BeginInvoke_m2586 ();
extern "C" void StateChanged_EndInvoke_m2587 ();
extern "C" void CullingGroup_Finalize_m2588 ();
extern "C" void CullingGroup_Dispose_m2589 ();
extern "C" void CullingGroup_SendEvents_m2590 ();
extern "C" void CullingGroup_FinalizerFailure_m2591 ();
extern "C" void GradientColorKey__ctor_m2592 ();
extern "C" void GradientAlphaKey__ctor_m2593 ();
extern "C" void Gradient__ctor_m2594 ();
extern "C" void Gradient_Init_m2595 ();
extern "C" void Gradient_Cleanup_m2596 ();
extern "C" void Gradient_Finalize_m2597 ();
extern "C" void TouchScreenKeyboard__ctor_m2598 ();
extern "C" void TouchScreenKeyboard_Destroy_m2599 ();
extern "C" void TouchScreenKeyboard_Finalize_m2600 ();
extern "C" void TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2601 ();
extern "C" void TouchScreenKeyboard_get_isSupported_m2206 ();
extern "C" void TouchScreenKeyboard_Open_m2262 ();
extern "C" void TouchScreenKeyboard_Open_m2263 ();
extern "C" void TouchScreenKeyboard_Open_m2602 ();
extern "C" void TouchScreenKeyboard_get_text_m2188 ();
extern "C" void TouchScreenKeyboard_set_text_m2189 ();
extern "C" void TouchScreenKeyboard_set_hideInput_m2261 ();
extern "C" void TouchScreenKeyboard_get_active_m2187 ();
extern "C" void TouchScreenKeyboard_set_active_m2260 ();
extern "C" void TouchScreenKeyboard_get_done_m2211 ();
extern "C" void TouchScreenKeyboard_get_wasCanceled_m2207 ();
extern "C" void LayerMask_get_value_m2603 ();
extern "C" void LayerMask_set_value_m2604 ();
extern "C" void LayerMask_LayerToName_m2605 ();
extern "C" void LayerMask_NameToLayer_m2606 ();
extern "C" void LayerMask_GetMask_m2607 ();
extern "C" void LayerMask_op_Implicit_m1972 ();
extern "C" void LayerMask_op_Implicit_m1970 ();
extern "C" void Vector2__ctor_m257 ();
extern "C" void Vector2_get_Item_m2171 ();
extern "C" void Vector2_set_Item_m2179 ();
extern "C" void Vector2_Scale_m2250 ();
extern "C" void Vector2_Normalize_m2608 ();
extern "C" void Vector2_get_normalized_m258 ();
extern "C" void Vector2_ToString_m2609 ();
extern "C" void Vector2_GetHashCode_m2610 ();
extern "C" void Vector2_Equals_m2611 ();
extern "C" void Vector2_Dot_m1940 ();
extern "C" void Vector2_get_magnitude_m2612 ();
extern "C" void Vector2_get_sqrMagnitude_m1904 ();
extern "C" void Vector2_SqrMagnitude_m2613 ();
extern "C" void Vector2_get_zero_m1900 ();
extern "C" void Vector2_get_one_m2009 ();
extern "C" void Vector2_get_up_m2030 ();
extern "C" void Vector2_get_right_m2024 ();
extern "C" void Vector2_op_Addition_m2055 ();
extern "C" void Vector2_op_Subtraction_m1915 ();
extern "C" void Vector2_op_Multiply_m2168 ();
extern "C" void Vector2_op_Division_m2219 ();
extern "C" void Vector2_op_Equality_m2444 ();
extern "C" void Vector2_op_Inequality_m2249 ();
extern "C" void Vector2_op_Implicit_m1918 ();
extern "C" void Vector2_op_Implicit_m253 ();
extern "C" void Vector3__ctor_m230 ();
extern "C" void Vector3__ctor_m2116 ();
extern "C" void Vector3_Lerp_m2297 ();
extern "C" void Vector3_SmoothDamp_m445 ();
extern "C" void Vector3_SmoothDamp_m2614 ();
extern "C" void Vector3_get_Item_m2301 ();
extern "C" void Vector3_set_Item_m2302 ();
extern "C" void Vector3_GetHashCode_m2615 ();
extern "C" void Vector3_Equals_m2616 ();
extern "C" void Vector3_Normalize_m2617 ();
extern "C" void Vector3_get_normalized_m251 ();
extern "C" void Vector3_ToString_m2618 ();
extern "C" void Vector3_ToString_m2619 ();
extern "C" void Vector3_Dot_m2141 ();
extern "C" void Vector3_Angle_m272 ();
extern "C" void Vector3_Distance_m1965 ();
extern "C" void Vector3_ClampMagnitude_m2620 ();
extern "C" void Vector3_Magnitude_m2621 ();
extern "C" void Vector3_get_magnitude_m271 ();
extern "C" void Vector3_SqrMagnitude_m2622 ();
extern "C" void Vector3_get_sqrMagnitude_m289 ();
extern "C" void Vector3_Min_m2309 ();
extern "C" void Vector3_Max_m2310 ();
extern "C" void Vector3_get_zero_m227 ();
extern "C" void Vector3_get_one_m2068 ();
extern "C" void Vector3_get_forward_m2140 ();
extern "C" void Vector3_get_back_m2424 ();
extern "C" void Vector3_get_up_m319 ();
extern "C" void Vector3_get_down_m2328 ();
extern "C" void Vector3_get_left_m2326 ();
extern "C" void Vector3_get_right_m2327 ();
extern "C" void Vector3_op_Addition_m279 ();
extern "C" void Vector3_op_Subtraction_m225 ();
extern "C" void Vector3_op_UnaryNegation_m323 ();
extern "C" void Vector3_op_Multiply_m278 ();
extern "C" void Vector3_op_Multiply_m312 ();
extern "C" void Vector3_op_Division_m226 ();
extern "C" void Vector3_op_Equality_m317 ();
extern "C" void Vector3_op_Inequality_m247 ();
extern "C" void Color__ctor_m1998 ();
extern "C" void Color__ctor_m2001 ();
extern "C" void Color_ToString_m2623 ();
extern "C" void Color_GetHashCode_m2624 ();
extern "C" void Color_Equals_m2123 ();
extern "C" void Color_Lerp_m1980 ();
extern "C" void Color_get_red_m2625 ();
extern "C" void Color_get_white_m421 ();
extern "C" void Color_get_black_m2127 ();
extern "C" void Color_get_clear_m2075 ();
extern "C" void Color_op_Multiply_m2323 ();
extern "C" void Color_op_Implicit_m2626 ();
extern "C" void Color32__ctor_m1996 ();
extern "C" void Color32_ToString_m2627 ();
extern "C" void Color32_op_Implicit_m2115 ();
extern "C" void Color32_op_Implicit_m1997 ();
extern "C" void Quaternion__ctor_m2628 ();
extern "C" void Quaternion_Dot_m2629 ();
extern "C" void Quaternion_AngleAxis_m320 ();
extern "C" void Quaternion_INTERNAL_CALL_AngleAxis_m2630 ();
extern "C" void Quaternion_LookRotation_m327 ();
extern "C" void Quaternion_INTERNAL_CALL_LookRotation_m2631 ();
extern "C" void Quaternion_Inverse_m2324 ();
extern "C" void Quaternion_INTERNAL_CALL_Inverse_m2632 ();
extern "C" void Quaternion_ToString_m2633 ();
extern "C" void Quaternion_get_eulerAngles_m2634 ();
extern "C" void Quaternion_Euler_m328 ();
extern "C" void Quaternion_Internal_ToEulerRad_m2635 ();
extern "C" void Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m2636 ();
extern "C" void Quaternion_Internal_FromEulerRad_m2637 ();
extern "C" void Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2638 ();
extern "C" void Quaternion_GetHashCode_m2639 ();
extern "C" void Quaternion_Equals_m2640 ();
extern "C" void Quaternion_op_Multiply_m329 ();
extern "C" void Quaternion_op_Multiply_m321 ();
extern "C" void Quaternion_op_Inequality_m2247 ();
extern "C" void Rect__ctor_m422 ();
extern "C" void Rect_get_x_m2112 ();
extern "C" void Rect_set_x_m429 ();
extern "C" void Rect_get_y_m2113 ();
extern "C" void Rect_set_y_m430 ();
extern "C" void Rect_get_position_m2169 ();
extern "C" void Rect_get_center_m2290 ();
extern "C" void Rect_get_min_m2054 ();
extern "C" void Rect_get_max_m2056 ();
extern "C" void Rect_get_width_m2107 ();
extern "C" void Rect_set_width_m431 ();
extern "C" void Rect_get_height_m2060 ();
extern "C" void Rect_set_height_m432 ();
extern "C" void Rect_get_size_m2057 ();
extern "C" void Rect_get_xMin_m2183 ();
extern "C" void Rect_get_yMin_m2182 ();
extern "C" void Rect_get_xMax_m2176 ();
extern "C" void Rect_get_yMax_m2177 ();
extern "C" void Rect_ToString_m2641 ();
extern "C" void Rect_Contains_m2062 ();
extern "C" void Rect_Overlaps_m2272 ();
extern "C" void Rect_GetHashCode_m2642 ();
extern "C" void Rect_Equals_m2643 ();
extern "C" void Rect_op_Inequality_m2283 ();
extern "C" void Rect_op_Equality_m2280 ();
extern "C" void Matrix4x4_get_Item_m2644 ();
extern "C" void Matrix4x4_set_Item_m2645 ();
extern "C" void Matrix4x4_get_Item_m2646 ();
extern "C" void Matrix4x4_set_Item_m2647 ();
extern "C" void Matrix4x4_GetHashCode_m2648 ();
extern "C" void Matrix4x4_Equals_m2649 ();
extern "C" void Matrix4x4_Inverse_m2650 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Inverse_m2651 ();
extern "C" void Matrix4x4_Transpose_m2652 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Transpose_m2653 ();
extern "C" void Matrix4x4_Invert_m2654 ();
extern "C" void Matrix4x4_INTERNAL_CALL_Invert_m2655 ();
extern "C" void Matrix4x4_get_inverse_m2656 ();
extern "C" void Matrix4x4_get_transpose_m2657 ();
extern "C" void Matrix4x4_get_isIdentity_m2658 ();
extern "C" void Matrix4x4_GetColumn_m2659 ();
extern "C" void Matrix4x4_GetRow_m2660 ();
extern "C" void Matrix4x4_SetColumn_m2661 ();
extern "C" void Matrix4x4_SetRow_m2662 ();
extern "C" void Matrix4x4_MultiplyPoint_m2663 ();
extern "C" void Matrix4x4_MultiplyPoint3x4_m2308 ();
extern "C" void Matrix4x4_MultiplyVector_m2664 ();
extern "C" void Matrix4x4_Scale_m2665 ();
extern "C" void Matrix4x4_get_zero_m2666 ();
extern "C" void Matrix4x4_get_identity_m2667 ();
extern "C" void Matrix4x4_SetTRS_m2668 ();
extern "C" void Matrix4x4_TRS_m2669 ();
extern "C" void Matrix4x4_INTERNAL_CALL_TRS_m2670 ();
extern "C" void Matrix4x4_ToString_m2671 ();
extern "C" void Matrix4x4_ToString_m2672 ();
extern "C" void Matrix4x4_Ortho_m2673 ();
extern "C" void Matrix4x4_Perspective_m2674 ();
extern "C" void Matrix4x4_op_Multiply_m2675 ();
extern "C" void Matrix4x4_op_Multiply_m2676 ();
extern "C" void Matrix4x4_op_Equality_m2677 ();
extern "C" void Matrix4x4_op_Inequality_m2678 ();
extern "C" void Bounds__ctor_m2303 ();
extern "C" void Bounds_GetHashCode_m2679 ();
extern "C" void Bounds_Equals_m2680 ();
extern "C" void Bounds_get_center_m2304 ();
extern "C" void Bounds_set_center_m2306 ();
extern "C" void Bounds_get_size_m2295 ();
extern "C" void Bounds_set_size_m2305 ();
extern "C" void Bounds_get_extents_m2681 ();
extern "C" void Bounds_set_extents_m2682 ();
extern "C" void Bounds_get_min_m2300 ();
extern "C" void Bounds_set_min_m2683 ();
extern "C" void Bounds_get_max_m2312 ();
extern "C" void Bounds_set_max_m2684 ();
extern "C" void Bounds_SetMinMax_m2685 ();
extern "C" void Bounds_Encapsulate_m2311 ();
extern "C" void Bounds_Encapsulate_m2686 ();
extern "C" void Bounds_Expand_m2687 ();
extern "C" void Bounds_Expand_m2688 ();
extern "C" void Bounds_Intersects_m2689 ();
extern "C" void Bounds_Internal_Contains_m2690 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_Contains_m2691 ();
extern "C" void Bounds_Contains_m2692 ();
extern "C" void Bounds_Internal_SqrDistance_m2693 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_SqrDistance_m2694 ();
extern "C" void Bounds_SqrDistance_m2695 ();
extern "C" void Bounds_Internal_IntersectRay_m2696 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_IntersectRay_m2697 ();
extern "C" void Bounds_IntersectRay_m2698 ();
extern "C" void Bounds_IntersectRay_m2699 ();
extern "C" void Bounds_Internal_GetClosestPoint_m2700 ();
extern "C" void Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m2701 ();
extern "C" void Bounds_ClosestPoint_m2702 ();
extern "C" void Bounds_ToString_m2703 ();
extern "C" void Bounds_ToString_m2704 ();
extern "C" void Bounds_op_Equality_m2705 ();
extern "C" void Bounds_op_Inequality_m2298 ();
extern "C" void Vector4__ctor_m2114 ();
extern "C" void Vector4_get_Item_m2170 ();
extern "C" void Vector4_set_Item_m2172 ();
extern "C" void Vector4_GetHashCode_m2706 ();
extern "C" void Vector4_Equals_m2707 ();
extern "C" void Vector4_ToString_m2708 ();
extern "C" void Vector4_Dot_m2709 ();
extern "C" void Vector4_SqrMagnitude_m2710 ();
extern "C" void Vector4_get_sqrMagnitude_m2158 ();
extern "C" void Vector4_get_zero_m2161 ();
extern "C" void Vector4_op_Subtraction_m2711 ();
extern "C" void Vector4_op_Division_m2167 ();
extern "C" void Vector4_op_Equality_m2712 ();
extern "C" void Ray__ctor_m2713 ();
extern "C" void Ray_get_origin_m1959 ();
extern "C" void Ray_get_direction_m1960 ();
extern "C" void Ray_GetPoint_m2214 ();
extern "C" void Ray_ToString_m2714 ();
extern "C" void Plane__ctor_m2212 ();
extern "C" void Plane_get_normal_m2715 ();
extern "C" void Plane_get_distance_m2716 ();
extern "C" void Plane_Raycast_m2213 ();
extern "C" void MathfInternal__cctor_m2717 ();
extern "C" void Mathf__cctor_m2718 ();
extern "C" void Mathf_Sin_m2719 ();
extern "C" void Mathf_Cos_m2720 ();
extern "C" void Mathf_Acos_m2721 ();
extern "C" void Mathf_Atan2_m2722 ();
extern "C" void Mathf_Sqrt_m2723 ();
extern "C" void Mathf_Abs_m2724 ();
extern "C" void Mathf_Min_m290 ();
extern "C" void Mathf_Min_m2231 ();
extern "C" void Mathf_Max_m274 ();
extern "C" void Mathf_Max_m2229 ();
extern "C" void Mathf_Pow_m2725 ();
extern "C" void Mathf_Log_m2371 ();
extern "C" void Mathf_Floor_m2726 ();
extern "C" void Mathf_Round_m2727 ();
extern "C" void Mathf_CeilToInt_m2381 ();
extern "C" void Mathf_FloorToInt_m2383 ();
extern "C" void Mathf_RoundToInt_m2164 ();
extern "C" void Mathf_Sign_m283 ();
extern "C" void Mathf_Clamp_m281 ();
extern "C" void Mathf_Clamp_m229 ();
extern "C" void Mathf_Clamp01_m2152 ();
extern "C" void Mathf_Lerp_m275 ();
extern "C" void Mathf_MoveTowards_m219 ();
extern "C" void Mathf_Approximately_m1936 ();
extern "C" void Mathf_SmoothDamp_m448 ();
extern "C" void Mathf_SmoothDamp_m2296 ();
extern "C" void Mathf_Repeat_m2180 ();
extern "C" void Mathf_InverseLerp_m248 ();
extern "C" void Mathf_PerlinNoise_m280 ();
extern "C" void DrivenRectTransformTracker_Add_m2289 ();
extern "C" void DrivenRectTransformTracker_Clear_m2288 ();
extern "C" void ReapplyDrivenProperties__ctor_m2397 ();
extern "C" void ReapplyDrivenProperties_Invoke_m2728 ();
extern "C" void ReapplyDrivenProperties_BeginInvoke_m2729 ();
extern "C" void ReapplyDrivenProperties_EndInvoke_m2730 ();
extern "C" void RectTransform_add_reapplyDrivenProperties_m2398 ();
extern "C" void RectTransform_remove_reapplyDrivenProperties_m2731 ();
extern "C" void RectTransform_get_rect_m2053 ();
extern "C" void RectTransform_INTERNAL_get_rect_m2732 ();
extern "C" void RectTransform_get_anchorMin_m2064 ();
extern "C" void RectTransform_set_anchorMin_m2008 ();
extern "C" void RectTransform_INTERNAL_get_anchorMin_m2733 ();
extern "C" void RectTransform_INTERNAL_set_anchorMin_m2734 ();
extern "C" void RectTransform_get_anchorMax_m2065 ();
extern "C" void RectTransform_set_anchorMax_m2010 ();
extern "C" void RectTransform_INTERNAL_get_anchorMax_m2735 ();
extern "C" void RectTransform_INTERNAL_set_anchorMax_m2736 ();
extern "C" void RectTransform_get_anchoredPosition_m2066 ();
extern "C" void RectTransform_set_anchoredPosition_m2011 ();
extern "C" void RectTransform_INTERNAL_get_anchoredPosition_m2737 ();
extern "C" void RectTransform_INTERNAL_set_anchoredPosition_m2738 ();
extern "C" void RectTransform_get_sizeDelta_m2026 ();
extern "C" void RectTransform_set_sizeDelta_m2000 ();
extern "C" void RectTransform_INTERNAL_get_sizeDelta_m2739 ();
extern "C" void RectTransform_INTERNAL_set_sizeDelta_m2740 ();
extern "C" void RectTransform_get_pivot_m2067 ();
extern "C" void RectTransform_set_pivot_m2025 ();
extern "C" void RectTransform_INTERNAL_get_pivot_m2741 ();
extern "C" void RectTransform_INTERNAL_set_pivot_m2742 ();
extern "C" void RectTransform_SendReapplyDrivenProperties_m2743 ();
extern "C" void RectTransform_GetLocalCorners_m2744 ();
extern "C" void RectTransform_GetWorldCorners_m2061 ();
extern "C" void RectTransform_set_offsetMin_m2019 ();
extern "C" void RectTransform_set_offsetMax_m2020 ();
extern "C" void RectTransform_SetInsetAndSizeFromParentEdge_m2393 ();
extern "C" void RectTransform_SetSizeWithCurrentAnchors_m2369 ();
extern "C" void RectTransform_GetParentSize_m2745 ();
extern "C" void ResourceRequest__ctor_m2746 ();
extern "C" void ResourceRequest_get_asset_m2747 ();
extern "C" void Resources_Load_m423 ();
extern "C" void Resources_Load_m2748 ();
extern "C" void SerializePrivateVariables__ctor_m2749 ();
extern "C" void SerializeField__ctor_m2750 ();
extern "C" void Shader_PropertyToID_m2751 ();
extern "C" void Material__ctor_m2348 ();
extern "C" void Material_get_mainTexture_m2155 ();
extern "C" void Material_GetTexture_m2752 ();
extern "C" void Material_GetTexture_m2753 ();
extern "C" void Material_SetFloat_m2754 ();
extern "C" void Material_SetFloat_m2755 ();
extern "C" void Material_SetInt_m2350 ();
extern "C" void Material_HasProperty_m2346 ();
extern "C" void Material_HasProperty_m2756 ();
extern "C" void Material_Internal_CreateWithMaterial_m2757 ();
extern "C" void SortingLayer_GetLayerValueFromID_m1866 ();
extern "C" void SphericalHarmonicsL2_Clear_m2758 ();
extern "C" void SphericalHarmonicsL2_ClearInternal_m2759 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m2760 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLight_m2761 ();
extern "C" void SphericalHarmonicsL2_AddAmbientLightInternal_m2762 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m2763 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLight_m2764 ();
extern "C" void SphericalHarmonicsL2_AddDirectionalLightInternal_m2765 ();
extern "C" void SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m2766 ();
extern "C" void SphericalHarmonicsL2_get_Item_m2767 ();
extern "C" void SphericalHarmonicsL2_set_Item_m2768 ();
extern "C" void SphericalHarmonicsL2_GetHashCode_m2769 ();
extern "C" void SphericalHarmonicsL2_Equals_m2770 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m2771 ();
extern "C" void SphericalHarmonicsL2_op_Multiply_m2772 ();
extern "C" void SphericalHarmonicsL2_op_Addition_m2773 ();
extern "C" void SphericalHarmonicsL2_op_Equality_m2774 ();
extern "C" void SphericalHarmonicsL2_op_Inequality_m2775 ();
extern "C" void Sprite_get_rect_m2163 ();
extern "C" void Sprite_INTERNAL_get_rect_m2776 ();
extern "C" void Sprite_get_pixelsPerUnit_m2159 ();
extern "C" void Sprite_get_texture_m2156 ();
extern "C" void Sprite_get_textureRect_m2175 ();
extern "C" void Sprite_INTERNAL_get_textureRect_m2777 ();
extern "C" void Sprite_get_border_m2157 ();
extern "C" void Sprite_INTERNAL_get_border_m2778 ();
extern "C" void DataUtility_GetInnerUV_m2166 ();
extern "C" void DataUtility_GetOuterUV_m2165 ();
extern "C" void DataUtility_GetPadding_m2162 ();
extern "C" void DataUtility_GetMinSize_m2173 ();
extern "C" void DataUtility_Internal_GetMinSize_m2779 ();
extern "C" void UnityString_Format_m2780 ();
extern "C" void AsyncOperation__ctor_m2781 ();
extern "C" void AsyncOperation_InternalDestroy_m2782 ();
extern "C" void AsyncOperation_Finalize_m2783 ();
extern "C" void LogCallback__ctor_m2784 ();
extern "C" void LogCallback_Invoke_m2785 ();
extern "C" void LogCallback_BeginInvoke_m2786 ();
extern "C" void LogCallback_EndInvoke_m2787 ();
extern "C" void Application_Quit_m454 ();
extern "C" void Application_LoadLevel_m453 ();
extern "C" void Application_LoadLevelAsync_m2788 ();
extern "C" void Application_get_isPlaying_m2035 ();
extern "C" void Application_get_isEditor_m2279 ();
extern "C" void Application_get_platform_m2186 ();
extern "C" void Application_CallLogCallback_m2789 ();
extern "C" void Behaviour__ctor_m2790 ();
extern "C" void Behaviour_get_enabled_m1909 ();
extern "C" void Behaviour_set_enabled_m2039 ();
extern "C" void Behaviour_get_isActiveAndEnabled_m1902 ();
extern "C" void CameraCallback__ctor_m2791 ();
extern "C" void CameraCallback_Invoke_m2792 ();
extern "C" void CameraCallback_BeginInvoke_m2793 ();
extern "C" void CameraCallback_EndInvoke_m2794 ();
extern "C" void Camera_get_nearClipPlane_m1958 ();
extern "C" void Camera_get_farClipPlane_m1957 ();
extern "C" void Camera_get_orthographicSize_m447 ();
extern "C" void Camera_set_orthographicSize_m449 ();
extern "C" void Camera_get_depth_m1864 ();
extern "C" void Camera_get_aspect_m450 ();
extern "C" void Camera_get_cullingMask_m1971 ();
extern "C" void Camera_get_eventMask_m2795 ();
extern "C" void Camera_get_pixelRect_m2796 ();
extern "C" void Camera_INTERNAL_get_pixelRect_m2797 ();
extern "C" void Camera_get_targetTexture_m2798 ();
extern "C" void Camera_get_pixelWidth_m426 ();
extern "C" void Camera_get_pixelHeight_m427 ();
extern "C" void Camera_get_clearFlags_m2799 ();
extern "C" void Camera_ScreenToViewportPoint_m2135 ();
extern "C" void Camera_INTERNAL_CALL_ScreenToViewportPoint_m2800 ();
extern "C" void Camera_ScreenPointToRay_m1956 ();
extern "C" void Camera_INTERNAL_CALL_ScreenPointToRay_m2801 ();
extern "C" void Camera_get_main_m288 ();
extern "C" void Camera_get_current_m428 ();
extern "C" void Camera_get_allCamerasCount_m2802 ();
extern "C" void Camera_GetAllCameras_m2803 ();
extern "C" void Camera_FireOnPreCull_m2804 ();
extern "C" void Camera_FireOnPreRender_m2805 ();
extern "C" void Camera_FireOnPostRender_m2806 ();
extern "C" void Camera_RaycastTry_m2807 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry_m2808 ();
extern "C" void Camera_RaycastTry2D_m2809 ();
extern "C" void Camera_INTERNAL_CALL_RaycastTry2D_m2810 ();
extern "C" void Debug_Internal_Log_m2811 ();
extern "C" void Debug_Internal_LogException_m2812 ();
extern "C" void Debug_Log_m2813 ();
extern "C" void Debug_LogError_m263 ();
extern "C" void Debug_LogError_m2040 ();
extern "C" void Debug_LogException_m2814 ();
extern "C" void Debug_LogException_m1994 ();
extern "C" void Debug_LogWarning_m341 ();
extern "C" void Debug_LogWarning_m2347 ();
extern "C" void DisplaysUpdatedDelegate__ctor_m2815 ();
extern "C" void DisplaysUpdatedDelegate_Invoke_m2816 ();
extern "C" void DisplaysUpdatedDelegate_BeginInvoke_m2817 ();
extern "C" void DisplaysUpdatedDelegate_EndInvoke_m2818 ();
extern "C" void Display__ctor_m2819 ();
extern "C" void Display__ctor_m2820 ();
extern "C" void Display__cctor_m2821 ();
extern "C" void Display_add_onDisplaysUpdated_m2822 ();
extern "C" void Display_remove_onDisplaysUpdated_m2823 ();
extern "C" void Display_get_renderingWidth_m2824 ();
extern "C" void Display_get_renderingHeight_m2825 ();
extern "C" void Display_get_systemWidth_m2826 ();
extern "C" void Display_get_systemHeight_m2827 ();
extern "C" void Display_get_colorBuffer_m2828 ();
extern "C" void Display_get_depthBuffer_m2829 ();
extern "C" void Display_Activate_m2830 ();
extern "C" void Display_Activate_m2831 ();
extern "C" void Display_SetParams_m2832 ();
extern "C" void Display_SetRenderingResolution_m2833 ();
extern "C" void Display_MultiDisplayLicense_m2834 ();
extern "C" void Display_RelativeMouseAt_m2835 ();
extern "C" void Display_get_main_m2836 ();
extern "C" void Display_RecreateDisplayList_m2837 ();
extern "C" void Display_FireDisplaysUpdated_m2838 ();
extern "C" void Display_GetSystemExtImpl_m2839 ();
extern "C" void Display_GetRenderingExtImpl_m2840 ();
extern "C" void Display_GetRenderingBuffersImpl_m2841 ();
extern "C" void Display_SetRenderingResolutionImpl_m2842 ();
extern "C" void Display_ActivateDisplayImpl_m2843 ();
extern "C" void Display_SetParamsImpl_m2844 ();
extern "C" void Display_MultiDisplayLicenseImpl_m2845 ();
extern "C" void Display_RelativeMouseAtImpl_m2846 ();
extern "C" void MonoBehaviour__ctor_m212 ();
extern "C" void MonoBehaviour_StartCoroutine_m346 ();
extern "C" void MonoBehaviour_StartCoroutine_Auto_m2847 ();
extern "C" void MonoBehaviour_StopCoroutine_m2848 ();
extern "C" void MonoBehaviour_StopCoroutine_m2285 ();
extern "C" void MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2849 ();
extern "C" void MonoBehaviour_StopCoroutine_Auto_m2850 ();
extern "C" void Touch_get_fingerId_m1913 ();
extern "C" void Touch_get_position_m256 ();
extern "C" void Touch_get_phase_m1914 ();
extern "C" void Input__cctor_m2851 ();
extern "C" void Input_GetKeyUpInt_m2852 ();
extern "C" void Input_GetKeyUpString_m2853 ();
extern "C" void Input_GetKeyDownInt_m2854 ();
extern "C" void Input_GetAxis_m240 ();
extern "C" void Input_GetAxisRaw_m239 ();
extern "C" void Input_GetButton_m241 ();
extern "C" void Input_GetButtonDown_m242 ();
extern "C" void Input_GetButtonUp_m243 ();
extern "C" void Input_GetKeyDown_m437 ();
extern "C" void Input_GetKeyUp_m436 ();
extern "C" void Input_GetKeyUp_m438 ();
extern "C" void Input_GetMouseButton_m439 ();
extern "C" void Input_GetMouseButtonDown_m1916 ();
extern "C" void Input_GetMouseButtonUp_m1917 ();
extern "C" void Input_get_mousePosition_m245 ();
extern "C" void Input_INTERNAL_get_mousePosition_m2855 ();
extern "C" void Input_get_mouseScrollDelta_m1919 ();
extern "C" void Input_INTERNAL_get_mouseScrollDelta_m2856 ();
extern "C" void Input_get_mousePresent_m1935 ();
extern "C" void Input_get_acceleration_m246 ();
extern "C" void Input_INTERNAL_get_acceleration_m2857 ();
extern "C" void Input_get_touches_m255 ();
extern "C" void Input_GetTouch_m1953 ();
extern "C" void Input_get_touchCount_m254 ();
extern "C" void Input_get_touchSupported_m1952 ();
extern "C" void Input_set_imeCompositionMode_m2264 ();
extern "C" void Input_get_compositionString_m2202 ();
extern "C" void Input_set_compositionCursorPos_m2252 ();
extern "C" void Input_INTERNAL_set_compositionCursorPos_m2858 ();
extern "C" void Object__ctor_m2859 ();
extern "C" void Object_Internal_CloneSingle_m2860 ();
extern "C" void Object_Destroy_m332 ();
extern "C" void Object_Destroy_m287 ();
extern "C" void Object_DestroyImmediate_m2861 ();
extern "C" void Object_DestroyImmediate_m2203 ();
extern "C" void Object_FindObjectsOfType_m214 ();
extern "C" void Object_get_name_m2241 ();
extern "C" void Object_set_name_m2022 ();
extern "C" void Object_set_hideFlags_m2111 ();
extern "C" void Object_ToString_m2862 ();
extern "C" void Object_Equals_m2863 ();
extern "C" void Object_GetHashCode_m2864 ();
extern "C" void Object_CompareBaseObjects_m2865 ();
extern "C" void Object_IsNativeObjectAlive_m2866 ();
extern "C" void Object_GetInstanceID_m2867 ();
extern "C" void Object_GetCachedPtr_m2868 ();
extern "C" void Object_CheckNullArgument_m2869 ();
extern "C" void Object_FindObjectOfType_m2870 ();
extern "C" void Object_op_Implicit_m424 ();
extern "C" void Object_op_Equality_m217 ();
extern "C" void Object_op_Inequality_m216 ();
extern "C" void Component__ctor_m2871 ();
extern "C" void Component_get_transform_m223 ();
extern "C" void Component_get_gameObject_m237 ();
extern "C" void Component_GetComponent_m2394 ();
extern "C" void Component_GetComponentFastPath_m2872 ();
extern "C" void Component_GetComponentInChildren_m2873 ();
extern "C" void Component_GetComponentInParent_m2874 ();
extern "C" void Component_GetComponentsForListInternal_m2875 ();
extern "C" void Component_GetComponents_m2099 ();
extern "C" void GameObject__ctor_m233 ();
extern "C" void GameObject_GetComponent_m2876 ();
extern "C" void GameObject_GetComponentFastPath_m2877 ();
extern "C" void GameObject_GetComponentInChildren_m2878 ();
extern "C" void GameObject_GetComponentInParent_m2879 ();
extern "C" void GameObject_GetComponentsInternal_m2880 ();
extern "C" void GameObject_get_transform_m303 ();
extern "C" void GameObject_get_layer_m2003 ();
extern "C" void GameObject_set_layer_m2004 ();
extern "C" void GameObject_SetActive_m238 ();
extern "C" void GameObject_get_activeSelf_m446 ();
extern "C" void GameObject_get_activeInHierarchy_m1910 ();
extern "C" void GameObject_get_tag_m442 ();
extern "C" void GameObject_SendMessage_m2881 ();
extern "C" void GameObject_Internal_AddComponentWithType_m2882 ();
extern "C" void GameObject_AddComponent_m2883 ();
extern "C" void GameObject_Internal_CreateGameObject_m2884 ();
extern "C" void Enumerator__ctor_m2885 ();
extern "C" void Enumerator_get_Current_m2886 ();
extern "C" void Enumerator_MoveNext_m2887 ();
extern "C" void Enumerator_Reset_m2888 ();
extern "C" void Transform_get_position_m224 ();
extern "C" void Transform_set_position_m231 ();
extern "C" void Transform_INTERNAL_get_position_m2889 ();
extern "C" void Transform_INTERNAL_set_position_m2890 ();
extern "C" void Transform_get_localPosition_m334 ();
extern "C" void Transform_set_localPosition_m335 ();
extern "C" void Transform_INTERNAL_get_localPosition_m2891 ();
extern "C" void Transform_INTERNAL_set_localPosition_m2892 ();
extern "C" void Transform_get_eulerAngles_m318 ();
extern "C" void Transform_get_right_m277 ();
extern "C" void Transform_get_up_m322 ();
extern "C" void Transform_get_forward_m269 ();
extern "C" void Transform_get_rotation_m2139 ();
extern "C" void Transform_set_rotation_m309 ();
extern "C" void Transform_INTERNAL_get_rotation_m2893 ();
extern "C" void Transform_INTERNAL_set_rotation_m2894 ();
extern "C" void Transform_get_localRotation_m305 ();
extern "C" void Transform_set_localRotation_m330 ();
extern "C" void Transform_INTERNAL_get_localRotation_m2895 ();
extern "C" void Transform_INTERNAL_set_localRotation_m2896 ();
extern "C" void Transform_get_localScale_m2248 ();
extern "C" void Transform_set_localScale_m452 ();
extern "C" void Transform_INTERNAL_get_localScale_m2897 ();
extern "C" void Transform_INTERNAL_set_localScale_m2898 ();
extern "C" void Transform_get_parent_m331 ();
extern "C" void Transform_set_parent_m337 ();
extern "C" void Transform_get_parentInternal_m2899 ();
extern "C" void Transform_set_parentInternal_m2900 ();
extern "C" void Transform_SetParent_m2242 ();
extern "C" void Transform_SetParent_m2002 ();
extern "C" void Transform_get_worldToLocalMatrix_m2307 ();
extern "C" void Transform_INTERNAL_get_worldToLocalMatrix_m2901 ();
extern "C" void Transform_Rotate_m2902 ();
extern "C" void Transform_Rotate_m304 ();
extern "C" void Transform_Rotate_m2903 ();
extern "C" void Transform_TransformPoint_m2325 ();
extern "C" void Transform_INTERNAL_CALL_TransformPoint_m2904 ();
extern "C" void Transform_InverseTransformPoint_m282 ();
extern "C" void Transform_INTERNAL_CALL_InverseTransformPoint_m2905 ();
extern "C" void Transform_get_root_m339 ();
extern "C" void Transform_get_childCount_m2006 ();
extern "C" void Transform_SetAsFirstSibling_m2243 ();
extern "C" void Transform_IsChildOf_m2042 ();
extern "C" void Transform_GetEnumerator_m2906 ();
extern "C" void Transform_GetChild_m2005 ();
extern "C" void Time_get_time_m276 ();
extern "C" void Time_get_deltaTime_m218 ();
extern "C" void Time_get_unscaledTime_m1939 ();
extern "C" void Time_get_unscaledDeltaTime_m1987 ();
extern "C" void Time_get_timeScale_m455 ();
extern "C" void Time_set_timeScale_m456 ();
extern "C" void Time_get_frameCount_m221 ();
extern "C" void Random_Range_m298 ();
extern "C" void Random_get_value_m267 ();
extern "C" void YieldInstruction__ctor_m2907 ();
extern "C" void UnityAdsInternal__ctor_m2908 ();
extern "C" void UnityAdsInternal_add_onCampaignsAvailable_m2909 ();
extern "C" void UnityAdsInternal_remove_onCampaignsAvailable_m2910 ();
extern "C" void UnityAdsInternal_add_onCampaignsFetchFailed_m2911 ();
extern "C" void UnityAdsInternal_remove_onCampaignsFetchFailed_m2912 ();
extern "C" void UnityAdsInternal_add_onShow_m2913 ();
extern "C" void UnityAdsInternal_remove_onShow_m2914 ();
extern "C" void UnityAdsInternal_add_onHide_m2915 ();
extern "C" void UnityAdsInternal_remove_onHide_m2916 ();
extern "C" void UnityAdsInternal_add_onVideoCompleted_m2917 ();
extern "C" void UnityAdsInternal_remove_onVideoCompleted_m2918 ();
extern "C" void UnityAdsInternal_add_onVideoStarted_m2919 ();
extern "C" void UnityAdsInternal_remove_onVideoStarted_m2920 ();
extern "C" void UnityAdsInternal_RegisterNative_m2921 ();
extern "C" void UnityAdsInternal_Init_m2922 ();
extern "C" void UnityAdsInternal_Show_m2923 ();
extern "C" void UnityAdsInternal_CanShowAds_m2924 ();
extern "C" void UnityAdsInternal_SetLogLevel_m2925 ();
extern "C" void UnityAdsInternal_SetCampaignDataURL_m2926 ();
extern "C" void UnityAdsInternal_RemoveAllEventHandlers_m2927 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsAvailable_m2928 ();
extern "C" void UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m2929 ();
extern "C" void UnityAdsInternal_CallUnityAdsShow_m2930 ();
extern "C" void UnityAdsInternal_CallUnityAdsHide_m2931 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoCompleted_m2932 ();
extern "C" void UnityAdsInternal_CallUnityAdsVideoStarted_m2933 ();
extern "C" void ParticleSystem_Internal_Stop_m2934 ();
extern "C" void ParticleSystem_Stop_m342 ();
extern "C" void ParticleSystem_Stop_m2935 ();
extern "C" void ParticleSystem_Emit_m345 ();
extern "C" void ParticleSystem_INTERNAL_CALL_Emit_m2936 ();
extern "C" void ParticleSystem_GetParticleSystems_m2937 ();
extern "C" void ParticleSystem_GetDirectParticleSystemChildrenRecursive_m2938 ();
extern "C" void Particle_get_position_m2939 ();
extern "C" void Particle_set_position_m2940 ();
extern "C" void Particle_get_velocity_m2941 ();
extern "C" void Particle_set_velocity_m2942 ();
extern "C" void Particle_get_energy_m2943 ();
extern "C" void Particle_set_energy_m2944 ();
extern "C" void Particle_get_startEnergy_m2945 ();
extern "C" void Particle_set_startEnergy_m2946 ();
extern "C" void Particle_get_size_m2947 ();
extern "C" void Particle_set_size_m2948 ();
extern "C" void Particle_get_rotation_m2949 ();
extern "C" void Particle_set_rotation_m2950 ();
extern "C" void Particle_get_angularVelocity_m2951 ();
extern "C" void Particle_set_angularVelocity_m2952 ();
extern "C" void Particle_get_color_m2953 ();
extern "C" void Particle_set_color_m2954 ();
extern "C" void Collision_get_rigidbody_m284 ();
extern "C" void Physics_Raycast_m2955 ();
extern "C" void Physics_Raycast_m2136 ();
extern "C" void Physics_Raycast_m2956 ();
extern "C" void Physics_RaycastAll_m1973 ();
extern "C" void Physics_RaycastAll_m2957 ();
extern "C" void Physics_RaycastAll_m2958 ();
extern "C" void Physics_INTERNAL_CALL_RaycastAll_m2959 ();
extern "C" void Physics_Linecast_m440 ();
extern "C" void Physics_Linecast_m2960 ();
extern "C" void Physics_Internal_Raycast_m2961 ();
extern "C" void Physics_INTERNAL_CALL_Internal_Raycast_m2962 ();
extern "C" void WheelHit_get_normal_m316 ();
extern "C" void WheelHit_get_forwardSlip_m325 ();
extern "C" void WheelHit_get_sidewaysSlip_m326 ();
extern "C" void Rigidbody_get_velocity_m270 ();
extern "C" void Rigidbody_set_velocity_m313 ();
extern "C" void Rigidbody_INTERNAL_get_velocity_m2963 ();
extern "C" void Rigidbody_INTERNAL_set_velocity_m2964 ();
extern "C" void Rigidbody_get_angularVelocity_m273 ();
extern "C" void Rigidbody_INTERNAL_get_angularVelocity_m2965 ();
extern "C" void Rigidbody_set_freezeRotation_m435 ();
extern "C" void Rigidbody_AddForce_m324 ();
extern "C" void Rigidbody_INTERNAL_CALL_AddForce_m2966 ();
extern "C" void Rigidbody_set_centerOfMass_m307 ();
extern "C" void Rigidbody_INTERNAL_set_centerOfMass_m2967 ();
extern "C" void Collider_get_attachedRigidbody_m306 ();
extern "C" void WheelCollider_get_radius_m338 ();
extern "C" void WheelCollider_set_motorTorque_m314 ();
extern "C" void WheelCollider_set_brakeTorque_m311 ();
extern "C" void WheelCollider_set_steerAngle_m310 ();
extern "C" void WheelCollider_GetGroundHit_m315 ();
extern "C" void WheelCollider_GetWorldPose_m308 ();
extern "C" void RaycastHit_get_point_m1977 ();
extern "C" void RaycastHit_get_normal_m1978 ();
extern "C" void RaycastHit_get_distance_m441 ();
extern "C" void RaycastHit_get_collider_m1976 ();
extern "C" void Physics2D__cctor_m2968 ();
extern "C" void Physics2D_Internal_Raycast_m2969 ();
extern "C" void Physics2D_INTERNAL_CALL_Internal_Raycast_m2970 ();
extern "C" void Physics2D_Raycast_m2137 ();
extern "C" void Physics2D_Raycast_m2971 ();
extern "C" void Physics2D_RaycastAll_m1961 ();
extern "C" void Physics2D_INTERNAL_CALL_RaycastAll_m2972 ();
extern "C" void RaycastHit2D_get_point_m1966 ();
extern "C" void RaycastHit2D_get_normal_m1967 ();
extern "C" void RaycastHit2D_get_fraction_m2138 ();
extern "C" void RaycastHit2D_get_collider_m1962 ();
extern "C" void RaycastHit2D_get_rigidbody_m2973 ();
extern "C" void RaycastHit2D_get_transform_m1964 ();
extern "C" void Collider2D_get_attachedRigidbody_m2974 ();
extern "C" void AudioConfigurationChangeHandler__ctor_m2975 ();
extern "C" void AudioConfigurationChangeHandler_Invoke_m2976 ();
extern "C" void AudioConfigurationChangeHandler_BeginInvoke_m2977 ();
extern "C" void AudioConfigurationChangeHandler_EndInvoke_m2978 ();
extern "C" void AudioSettings_InvokeOnAudioConfigurationChanged_m2979 ();
extern "C" void PCMReaderCallback__ctor_m2980 ();
extern "C" void PCMReaderCallback_Invoke_m2981 ();
extern "C" void PCMReaderCallback_BeginInvoke_m2982 ();
extern "C" void PCMReaderCallback_EndInvoke_m2983 ();
extern "C" void PCMSetPositionCallback__ctor_m2984 ();
extern "C" void PCMSetPositionCallback_Invoke_m2985 ();
extern "C" void PCMSetPositionCallback_BeginInvoke_m2986 ();
extern "C" void PCMSetPositionCallback_EndInvoke_m2987 ();
extern "C" void AudioClip_get_length_m297 ();
extern "C" void AudioClip_InvokePCMReaderCallback_Internal_m2988 ();
extern "C" void AudioClip_InvokePCMSetPositionCallback_Internal_m2989 ();
extern "C" void AudioSource_set_volume_m293 ();
extern "C" void AudioSource_set_pitch_m291 ();
extern "C" void AudioSource_set_time_m299 ();
extern "C" void AudioSource_set_clip_m295 ();
extern "C" void AudioSource_Play_m2990 ();
extern "C" void AudioSource_Play_m300 ();
extern "C" void AudioSource_Stop_m347 ();
extern "C" void AudioSource_set_loop_m296 ();
extern "C" void AudioSource_set_dopplerLevel_m292 ();
extern "C" void AudioSource_set_minDistance_m301 ();
extern "C" void AudioSource_set_maxDistance_m302 ();
extern "C" void WebCamDevice_get_name_m2991 ();
extern "C" void WebCamDevice_get_isFrontFacing_m2992 ();
extern "C" void AnimationEvent__ctor_m2993 ();
extern "C" void AnimationEvent_get_data_m2994 ();
extern "C" void AnimationEvent_set_data_m2995 ();
extern "C" void AnimationEvent_get_stringParameter_m2996 ();
extern "C" void AnimationEvent_set_stringParameter_m2997 ();
extern "C" void AnimationEvent_get_floatParameter_m2998 ();
extern "C" void AnimationEvent_set_floatParameter_m2999 ();
extern "C" void AnimationEvent_get_intParameter_m3000 ();
extern "C" void AnimationEvent_set_intParameter_m3001 ();
extern "C" void AnimationEvent_get_objectReferenceParameter_m3002 ();
extern "C" void AnimationEvent_set_objectReferenceParameter_m3003 ();
extern "C" void AnimationEvent_get_functionName_m3004 ();
extern "C" void AnimationEvent_set_functionName_m3005 ();
extern "C" void AnimationEvent_get_time_m3006 ();
extern "C" void AnimationEvent_set_time_m3007 ();
extern "C" void AnimationEvent_get_messageOptions_m3008 ();
extern "C" void AnimationEvent_set_messageOptions_m3009 ();
extern "C" void AnimationEvent_get_isFiredByLegacy_m3010 ();
extern "C" void AnimationEvent_get_isFiredByAnimator_m3011 ();
extern "C" void AnimationEvent_get_animationState_m3012 ();
extern "C" void AnimationEvent_get_animatorStateInfo_m3013 ();
extern "C" void AnimationEvent_get_animatorClipInfo_m3014 ();
extern "C" void AnimationEvent_GetHash_m3015 ();
extern "C" void AnimationCurve__ctor_m3016 ();
extern "C" void AnimationCurve__ctor_m3017 ();
extern "C" void AnimationCurve_Cleanup_m3018 ();
extern "C" void AnimationCurve_Finalize_m3019 ();
extern "C" void AnimationCurve_Init_m3020 ();
extern "C" void AnimatorStateInfo_IsName_m3021 ();
extern "C" void AnimatorStateInfo_get_fullPathHash_m3022 ();
extern "C" void AnimatorStateInfo_get_nameHash_m3023 ();
extern "C" void AnimatorStateInfo_get_shortNameHash_m3024 ();
extern "C" void AnimatorStateInfo_get_normalizedTime_m3025 ();
extern "C" void AnimatorStateInfo_get_length_m3026 ();
extern "C" void AnimatorStateInfo_get_speed_m3027 ();
extern "C" void AnimatorStateInfo_get_speedMultiplier_m3028 ();
extern "C" void AnimatorStateInfo_get_tagHash_m3029 ();
extern "C" void AnimatorStateInfo_IsTag_m3030 ();
extern "C" void AnimatorStateInfo_get_loop_m3031 ();
extern "C" void AnimatorTransitionInfo_IsName_m3032 ();
extern "C" void AnimatorTransitionInfo_IsUserName_m3033 ();
extern "C" void AnimatorTransitionInfo_get_fullPathHash_m3034 ();
extern "C" void AnimatorTransitionInfo_get_nameHash_m3035 ();
extern "C" void AnimatorTransitionInfo_get_userNameHash_m3036 ();
extern "C" void AnimatorTransitionInfo_get_normalizedTime_m3037 ();
extern "C" void AnimatorTransitionInfo_get_anyState_m3038 ();
extern "C" void AnimatorTransitionInfo_get_entry_m3039 ();
extern "C" void AnimatorTransitionInfo_get_exit_m3040 ();
extern "C" void Animator_SetTrigger_m2331 ();
extern "C" void Animator_ResetTrigger_m2330 ();
extern "C" void Animator_get_runtimeAnimatorController_m2329 ();
extern "C" void Animator_StringToHash_m3041 ();
extern "C" void Animator_SetTriggerString_m3042 ();
extern "C" void Animator_ResetTriggerString_m3043 ();
extern "C" void HumanBone_get_boneName_m3044 ();
extern "C" void HumanBone_set_boneName_m3045 ();
extern "C" void HumanBone_get_humanName_m3046 ();
extern "C" void HumanBone_set_humanName_m3047 ();
extern "C" void CharacterInfo_get_advance_m3048 ();
extern "C" void CharacterInfo_set_advance_m3049 ();
extern "C" void CharacterInfo_get_glyphWidth_m3050 ();
extern "C" void CharacterInfo_set_glyphWidth_m3051 ();
extern "C" void CharacterInfo_get_glyphHeight_m3052 ();
extern "C" void CharacterInfo_set_glyphHeight_m3053 ();
extern "C" void CharacterInfo_get_bearing_m3054 ();
extern "C" void CharacterInfo_set_bearing_m3055 ();
extern "C" void CharacterInfo_get_minY_m3056 ();
extern "C" void CharacterInfo_set_minY_m3057 ();
extern "C" void CharacterInfo_get_maxY_m3058 ();
extern "C" void CharacterInfo_set_maxY_m3059 ();
extern "C" void CharacterInfo_get_minX_m3060 ();
extern "C" void CharacterInfo_set_minX_m3061 ();
extern "C" void CharacterInfo_get_maxX_m3062 ();
extern "C" void CharacterInfo_set_maxX_m3063 ();
extern "C" void CharacterInfo_get_uvBottomLeftUnFlipped_m3064 ();
extern "C" void CharacterInfo_set_uvBottomLeftUnFlipped_m3065 ();
extern "C" void CharacterInfo_get_uvBottomRightUnFlipped_m3066 ();
extern "C" void CharacterInfo_set_uvBottomRightUnFlipped_m3067 ();
extern "C" void CharacterInfo_get_uvTopRightUnFlipped_m3068 ();
extern "C" void CharacterInfo_set_uvTopRightUnFlipped_m3069 ();
extern "C" void CharacterInfo_get_uvTopLeftUnFlipped_m3070 ();
extern "C" void CharacterInfo_set_uvTopLeftUnFlipped_m3071 ();
extern "C" void CharacterInfo_get_uvBottomLeft_m3072 ();
extern "C" void CharacterInfo_set_uvBottomLeft_m3073 ();
extern "C" void CharacterInfo_get_uvBottomRight_m3074 ();
extern "C" void CharacterInfo_set_uvBottomRight_m3075 ();
extern "C" void CharacterInfo_get_uvTopRight_m3076 ();
extern "C" void CharacterInfo_set_uvTopRight_m3077 ();
extern "C" void CharacterInfo_get_uvTopLeft_m3078 ();
extern "C" void CharacterInfo_set_uvTopLeft_m3079 ();
extern "C" void FontTextureRebuildCallback__ctor_m3080 ();
extern "C" void FontTextureRebuildCallback_Invoke_m3081 ();
extern "C" void FontTextureRebuildCallback_BeginInvoke_m3082 ();
extern "C" void FontTextureRebuildCallback_EndInvoke_m3083 ();
extern "C" void Font__ctor_m3084 ();
extern "C" void Font__ctor_m3085 ();
extern "C" void Font__ctor_m3086 ();
extern "C" void Font_add_textureRebuilt_m2090 ();
extern "C" void Font_remove_textureRebuilt_m3087 ();
extern "C" void Font_add_m_FontTextureRebuildCallback_m3088 ();
extern "C" void Font_remove_m_FontTextureRebuildCallback_m3089 ();
extern "C" void Font_GetOSInstalledFontNames_m3090 ();
extern "C" void Font_Internal_CreateFont_m3091 ();
extern "C" void Font_Internal_CreateDynamicFont_m3092 ();
extern "C" void Font_CreateDynamicFontFromOSFont_m3093 ();
extern "C" void Font_CreateDynamicFontFromOSFont_m3094 ();
extern "C" void Font_get_material_m2352 ();
extern "C" void Font_set_material_m3095 ();
extern "C" void Font_HasCharacter_m2223 ();
extern "C" void Font_get_fontNames_m3096 ();
extern "C" void Font_set_fontNames_m3097 ();
extern "C" void Font_get_characterInfo_m3098 ();
extern "C" void Font_set_characterInfo_m3099 ();
extern "C" void Font_RequestCharactersInTexture_m3100 ();
extern "C" void Font_RequestCharactersInTexture_m3101 ();
extern "C" void Font_RequestCharactersInTexture_m3102 ();
extern "C" void Font_InvokeTextureRebuilt_Internal_m3103 ();
extern "C" void Font_get_textureRebuildCallback_m3104 ();
extern "C" void Font_set_textureRebuildCallback_m3105 ();
extern "C" void Font_GetMaxVertsForString_m3106 ();
extern "C" void Font_GetCharacterInfo_m3107 ();
extern "C" void Font_GetCharacterInfo_m3108 ();
extern "C" void Font_GetCharacterInfo_m3109 ();
extern "C" void Font_get_dynamic_m2354 ();
extern "C" void Font_get_ascent_m3110 ();
extern "C" void Font_get_lineHeight_m3111 ();
extern "C" void Font_get_fontSize_m2356 ();
extern "C" void TextGenerator__ctor_m2185 ();
extern "C" void TextGenerator__ctor_m2351 ();
extern "C" void TextGenerator_System_IDisposable_Dispose_m3112 ();
extern "C" void TextGenerator_Init_m3113 ();
extern "C" void TextGenerator_Dispose_cpp_m3114 ();
extern "C" void TextGenerator_Populate_Internal_m3115 ();
extern "C" void TextGenerator_Populate_Internal_cpp_m3116 ();
extern "C" void TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3117 ();
extern "C" void TextGenerator_get_rectExtents_m2240 ();
extern "C" void TextGenerator_INTERNAL_get_rectExtents_m3118 ();
extern "C" void TextGenerator_get_vertexCount_m3119 ();
extern "C" void TextGenerator_GetVerticesInternal_m3120 ();
extern "C" void TextGenerator_GetVerticesArray_m3121 ();
extern "C" void TextGenerator_get_characterCount_m3122 ();
extern "C" void TextGenerator_get_characterCountVisible_m2217 ();
extern "C" void TextGenerator_GetCharactersInternal_m3123 ();
extern "C" void TextGenerator_GetCharactersArray_m3124 ();
extern "C" void TextGenerator_get_lineCount_m2216 ();
extern "C" void TextGenerator_GetLinesInternal_m3125 ();
extern "C" void TextGenerator_GetLinesArray_m3126 ();
extern "C" void TextGenerator_get_fontSizeUsedForBestFit_m2251 ();
extern "C" void TextGenerator_Finalize_m3127 ();
extern "C" void TextGenerator_ValidatedSettings_m3128 ();
extern "C" void TextGenerator_Invalidate_m2353 ();
extern "C" void TextGenerator_GetCharacters_m3129 ();
extern "C" void TextGenerator_GetLines_m3130 ();
extern "C" void TextGenerator_GetVertices_m3131 ();
extern "C" void TextGenerator_GetPreferredWidth_m2358 ();
extern "C" void TextGenerator_GetPreferredHeight_m2359 ();
extern "C" void TextGenerator_Populate_m2239 ();
extern "C" void TextGenerator_PopulateAlways_m3132 ();
extern "C" void TextGenerator_get_verts_m2357 ();
extern "C" void TextGenerator_get_characters_m2218 ();
extern "C" void TextGenerator_get_lines_m2215 ();
extern "C" void WillRenderCanvases__ctor_m1990 ();
extern "C" void WillRenderCanvases_Invoke_m3133 ();
extern "C" void WillRenderCanvases_BeginInvoke_m3134 ();
extern "C" void WillRenderCanvases_EndInvoke_m3135 ();
extern "C" void Canvas_add_willRenderCanvases_m1991 ();
extern "C" void Canvas_remove_willRenderCanvases_m3136 ();
extern "C" void Canvas_get_renderMode_m2131 ();
extern "C" void Canvas_get_isRootCanvas_m2370 ();
extern "C" void Canvas_get_worldCamera_m2142 ();
extern "C" void Canvas_get_scaleFactor_m2355 ();
extern "C" void Canvas_set_scaleFactor_m2373 ();
extern "C" void Canvas_get_referencePixelsPerUnit_m2160 ();
extern "C" void Canvas_set_referencePixelsPerUnit_m2374 ();
extern "C" void Canvas_get_pixelPerfect_m2119 ();
extern "C" void Canvas_get_renderOrder_m2132 ();
extern "C" void Canvas_get_overrideSorting_m2278 ();
extern "C" void Canvas_set_overrideSorting_m2045 ();
extern "C" void Canvas_get_sortingOrder_m2073 ();
extern "C" void Canvas_set_sortingOrder_m2046 ();
extern "C" void Canvas_get_sortingLayerID_m2071 ();
extern "C" void Canvas_set_sortingLayerID_m2072 ();
extern "C" void Canvas_GetDefaultCanvasMaterial_m2093 ();
extern "C" void Canvas_SendWillRenderCanvases_m3137 ();
extern "C" void Canvas_ForceUpdateCanvases_m2294 ();
extern "C" void CanvasGroup_get_alpha_m2082 ();
extern "C" void CanvasGroup_set_alpha_m2086 ();
extern "C" void CanvasGroup_get_interactable_m2322 ();
extern "C" void CanvasGroup_get_blocksRaycasts_m3138 ();
extern "C" void CanvasGroup_get_ignoreParentGroups_m2118 ();
extern "C" void CanvasGroup_IsRaycastLocationValid_m3139 ();
extern "C" void UIVertex__cctor_m3140 ();
extern "C" void CanvasRenderer_SetColor_m2124 ();
extern "C" void CanvasRenderer_INTERNAL_CALL_SetColor_m3141 ();
extern "C" void CanvasRenderer_GetColor_m2122 ();
extern "C" void CanvasRenderer_EnableRectClipping_m2275 ();
extern "C" void CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3142 ();
extern "C" void CanvasRenderer_DisableRectClipping_m2276 ();
extern "C" void CanvasRenderer_set_hasPopInstruction_m2266 ();
extern "C" void CanvasRenderer_get_materialCount_m3143 ();
extern "C" void CanvasRenderer_set_materialCount_m2104 ();
extern "C" void CanvasRenderer_SetMaterial_m2105 ();
extern "C" void CanvasRenderer_SetMaterial_m2245 ();
extern "C" void CanvasRenderer_set_popMaterialCount_m2267 ();
extern "C" void CanvasRenderer_SetPopMaterial_m2268 ();
extern "C" void CanvasRenderer_SetTexture_m2106 ();
extern "C" void CanvasRenderer_SetMesh_m2108 ();
extern "C" void CanvasRenderer_Clear_m2102 ();
extern "C" void CanvasRenderer_SplitUIVertexStreams_m2438 ();
extern "C" void CanvasRenderer_SplitUIVertexStreamsInternal_m3144 ();
extern "C" void CanvasRenderer_SplitIndiciesStreamsInternal_m3145 ();
extern "C" void CanvasRenderer_CreateUIVertexStream_m2439 ();
extern "C" void CanvasRenderer_CreateUIVertexStreamInternal_m3146 ();
extern "C" void CanvasRenderer_AddUIVertexStream_m2437 ();
extern "C" void CanvasRenderer_get_cull_m2103 ();
extern "C" void CanvasRenderer_set_cull_m2273 ();
extern "C" void CanvasRenderer_get_absoluteDepth_m2095 ();
extern "C" void CanvasRenderer_get_hasMoved_m2271 ();
extern "C" void RectTransformUtility__cctor_m3147 ();
extern "C" void RectTransformUtility_RectangleContainsScreenPoint_m2143 ();
extern "C" void RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3148 ();
extern "C" void RectTransformUtility_PixelAdjustPoint_m2120 ();
extern "C" void RectTransformUtility_PixelAdjustPoint_m3149 ();
extern "C" void RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3150 ();
extern "C" void RectTransformUtility_PixelAdjustRect_m2121 ();
extern "C" void RectTransformUtility_ScreenPointToWorldPointInRectangle_m3151 ();
extern "C" void RectTransformUtility_ScreenPointToLocalPointInRectangle_m2174 ();
extern "C" void RectTransformUtility_ScreenPointToRay_m3152 ();
extern "C" void RectTransformUtility_FlipLayoutOnAxis_m2063 ();
extern "C" void RectTransformUtility_FlipLayoutAxes_m2291 ();
extern "C" void RectTransformUtility_GetTransposed_m3153 ();
extern "C" void Event__ctor_m2184 ();
extern "C" void Event__ctor_m3154 ();
extern "C" void Event__ctor_m3155 ();
extern "C" void Event_Finalize_m3156 ();
extern "C" void Event_get_mousePosition_m3157 ();
extern "C" void Event_set_mousePosition_m3158 ();
extern "C" void Event_get_delta_m3159 ();
extern "C" void Event_set_delta_m3160 ();
extern "C" void Event_get_mouseRay_m3161 ();
extern "C" void Event_set_mouseRay_m3162 ();
extern "C" void Event_get_shift_m3163 ();
extern "C" void Event_set_shift_m3164 ();
extern "C" void Event_get_control_m3165 ();
extern "C" void Event_set_control_m3166 ();
extern "C" void Event_get_alt_m3167 ();
extern "C" void Event_set_alt_m3168 ();
extern "C" void Event_get_command_m3169 ();
extern "C" void Event_set_command_m3170 ();
extern "C" void Event_get_capsLock_m3171 ();
extern "C" void Event_set_capsLock_m3172 ();
extern "C" void Event_get_numeric_m3173 ();
extern "C" void Event_set_numeric_m3174 ();
extern "C" void Event_get_functionKey_m3175 ();
extern "C" void Event_get_current_m3176 ();
extern "C" void Event_set_current_m3177 ();
extern "C" void Event_Internal_MakeMasterEventCurrent_m3178 ();
extern "C" void Event_get_isKey_m3179 ();
extern "C" void Event_get_isMouse_m3180 ();
extern "C" void Event_KeyboardEvent_m3181 ();
extern "C" void Event_GetHashCode_m3182 ();
extern "C" void Event_Equals_m3183 ();
extern "C" void Event_ToString_m3184 ();
extern "C" void Event_Init_m3185 ();
extern "C" void Event_Cleanup_m3186 ();
extern "C" void Event_InitCopy_m3187 ();
extern "C" void Event_InitPtr_m3188 ();
extern "C" void Event_get_rawType_m2224 ();
extern "C" void Event_get_type_m2225 ();
extern "C" void Event_set_type_m3189 ();
extern "C" void Event_GetTypeForControl_m3190 ();
extern "C" void Event_Internal_SetMousePosition_m3191 ();
extern "C" void Event_INTERNAL_CALL_Internal_SetMousePosition_m3192 ();
extern "C" void Event_Internal_GetMousePosition_m3193 ();
extern "C" void Event_Internal_SetMouseDelta_m3194 ();
extern "C" void Event_INTERNAL_CALL_Internal_SetMouseDelta_m3195 ();
extern "C" void Event_Internal_GetMouseDelta_m3196 ();
extern "C" void Event_get_button_m3197 ();
extern "C" void Event_set_button_m3198 ();
extern "C" void Event_get_modifiers_m2220 ();
extern "C" void Event_set_modifiers_m3199 ();
extern "C" void Event_get_pressure_m3200 ();
extern "C" void Event_set_pressure_m3201 ();
extern "C" void Event_get_clickCount_m3202 ();
extern "C" void Event_set_clickCount_m3203 ();
extern "C" void Event_get_character_m2222 ();
extern "C" void Event_set_character_m3204 ();
extern "C" void Event_get_commandName_m2226 ();
extern "C" void Event_set_commandName_m3205 ();
extern "C" void Event_get_keyCode_m2221 ();
extern "C" void Event_set_keyCode_m3206 ();
extern "C" void Event_Internal_SetNativeEvent_m3207 ();
extern "C" void Event_Use_m3208 ();
extern "C" void Event_PopEvent_m2227 ();
extern "C" void Event_GetEventCount_m3209 ();
extern "C" void ScrollViewState__ctor_m3210 ();
extern "C" void WindowFunction__ctor_m3211 ();
extern "C" void WindowFunction_Invoke_m3212 ();
extern "C" void WindowFunction_BeginInvoke_m3213 ();
extern "C" void WindowFunction_EndInvoke_m3214 ();
extern "C" void GUI__cctor_m3215 ();
extern "C" void GUI_set_nextScrollStepTime_m3216 ();
extern "C" void GUI_set_skin_m3217 ();
extern "C" void GUI_get_skin_m3218 ();
extern "C" void GUI_DoSetSkin_m3219 ();
extern "C" void GUI_DrawTexture_m434 ();
extern "C" void GUI_DrawTexture_m3220 ();
extern "C" void GUI_DrawTexture_m3221 ();
extern "C" void GUI_DrawTexture_m3222 ();
extern "C" void GUI_CallWindowDelegate_m3223 ();
extern "C" void GUI_get_color_m3224 ();
extern "C" void GUI_set_color_m433 ();
extern "C" void GUI_INTERNAL_get_color_m3225 ();
extern "C" void GUI_INTERNAL_set_color_m3226 ();
extern "C" void GUI_set_changed_m3227 ();
extern "C" void GUI_get_blendMaterial_m3228 ();
extern "C" void GUI_get_blitMaterial_m3229 ();
extern "C" void GUIContent__ctor_m3230 ();
extern "C" void GUIContent__ctor_m3231 ();
extern "C" void GUIContent__cctor_m3232 ();
extern "C" void GUIContent_ClearStaticCache_m3233 ();
extern "C" void GUILayout_Width_m3234 ();
extern "C" void GUILayout_Height_m3235 ();
extern "C" void LayoutCache__ctor_m3236 ();
extern "C" void GUILayoutUtility__cctor_m3237 ();
extern "C" void GUILayoutUtility_SelectIDList_m3238 ();
extern "C" void GUILayoutUtility_Begin_m3239 ();
extern "C" void GUILayoutUtility_BeginWindow_m3240 ();
extern "C" void GUILayoutUtility_Layout_m3241 ();
extern "C" void GUILayoutUtility_LayoutFromEditorWindow_m3242 ();
extern "C" void GUILayoutUtility_LayoutFreeGroup_m3243 ();
extern "C" void GUILayoutUtility_LayoutSingleGroup_m3244 ();
extern "C" void GUILayoutUtility_get_spaceStyle_m3245 ();
extern "C" void GUILayoutUtility_Internal_GetWindowRect_m3246 ();
extern "C" void GUILayoutUtility_Internal_MoveWindow_m3247 ();
extern "C" void GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m3248 ();
extern "C" void GUILayoutEntry__ctor_m3249 ();
extern "C" void GUILayoutEntry__cctor_m3250 ();
extern "C" void GUILayoutEntry_get_style_m3251 ();
extern "C" void GUILayoutEntry_set_style_m3252 ();
extern "C" void GUILayoutEntry_get_margin_m3253 ();
extern "C" void GUILayoutEntry_CalcWidth_m3254 ();
extern "C" void GUILayoutEntry_CalcHeight_m3255 ();
extern "C" void GUILayoutEntry_SetHorizontal_m3256 ();
extern "C" void GUILayoutEntry_SetVertical_m3257 ();
extern "C" void GUILayoutEntry_ApplyStyleSettings_m3258 ();
extern "C" void GUILayoutEntry_ApplyOptions_m3259 ();
extern "C" void GUILayoutEntry_ToString_m3260 ();
extern "C" void GUILayoutGroup__ctor_m3261 ();
extern "C" void GUILayoutGroup_get_margin_m3262 ();
extern "C" void GUILayoutGroup_ApplyOptions_m3263 ();
extern "C" void GUILayoutGroup_ApplyStyleSettings_m3264 ();
extern "C" void GUILayoutGroup_ResetCursor_m3265 ();
extern "C" void GUILayoutGroup_CalcWidth_m3266 ();
extern "C" void GUILayoutGroup_SetHorizontal_m3267 ();
extern "C" void GUILayoutGroup_CalcHeight_m3268 ();
extern "C" void GUILayoutGroup_SetVertical_m3269 ();
extern "C" void GUILayoutGroup_ToString_m3270 ();
extern "C" void GUIScrollGroup__ctor_m3271 ();
extern "C" void GUIScrollGroup_CalcWidth_m3272 ();
extern "C" void GUIScrollGroup_SetHorizontal_m3273 ();
extern "C" void GUIScrollGroup_CalcHeight_m3274 ();
extern "C" void GUIScrollGroup_SetVertical_m3275 ();
extern "C" void GUILayoutOption__ctor_m3276 ();
extern "C" void GUISettings__ctor_m3277 ();
extern "C" void SkinChangedDelegate__ctor_m3278 ();
extern "C" void SkinChangedDelegate_Invoke_m3279 ();
extern "C" void SkinChangedDelegate_BeginInvoke_m3280 ();
extern "C" void SkinChangedDelegate_EndInvoke_m3281 ();
extern "C" void GUISkin__ctor_m3282 ();
extern "C" void GUISkin_OnEnable_m3283 ();
extern "C" void GUISkin_get_font_m3284 ();
extern "C" void GUISkin_set_font_m3285 ();
extern "C" void GUISkin_get_box_m3286 ();
extern "C" void GUISkin_set_box_m3287 ();
extern "C" void GUISkin_get_label_m3288 ();
extern "C" void GUISkin_set_label_m3289 ();
extern "C" void GUISkin_get_textField_m3290 ();
extern "C" void GUISkin_set_textField_m3291 ();
extern "C" void GUISkin_get_textArea_m3292 ();
extern "C" void GUISkin_set_textArea_m3293 ();
extern "C" void GUISkin_get_button_m3294 ();
extern "C" void GUISkin_set_button_m3295 ();
extern "C" void GUISkin_get_toggle_m3296 ();
extern "C" void GUISkin_set_toggle_m3297 ();
extern "C" void GUISkin_get_window_m3298 ();
extern "C" void GUISkin_set_window_m3299 ();
extern "C" void GUISkin_get_horizontalSlider_m3300 ();
extern "C" void GUISkin_set_horizontalSlider_m3301 ();
extern "C" void GUISkin_get_horizontalSliderThumb_m3302 ();
extern "C" void GUISkin_set_horizontalSliderThumb_m3303 ();
extern "C" void GUISkin_get_verticalSlider_m3304 ();
extern "C" void GUISkin_set_verticalSlider_m3305 ();
extern "C" void GUISkin_get_verticalSliderThumb_m3306 ();
extern "C" void GUISkin_set_verticalSliderThumb_m3307 ();
extern "C" void GUISkin_get_horizontalScrollbar_m3308 ();
extern "C" void GUISkin_set_horizontalScrollbar_m3309 ();
extern "C" void GUISkin_get_horizontalScrollbarThumb_m3310 ();
extern "C" void GUISkin_set_horizontalScrollbarThumb_m3311 ();
extern "C" void GUISkin_get_horizontalScrollbarLeftButton_m3312 ();
extern "C" void GUISkin_set_horizontalScrollbarLeftButton_m3313 ();
extern "C" void GUISkin_get_horizontalScrollbarRightButton_m3314 ();
extern "C" void GUISkin_set_horizontalScrollbarRightButton_m3315 ();
extern "C" void GUISkin_get_verticalScrollbar_m3316 ();
extern "C" void GUISkin_set_verticalScrollbar_m3317 ();
extern "C" void GUISkin_get_verticalScrollbarThumb_m3318 ();
extern "C" void GUISkin_set_verticalScrollbarThumb_m3319 ();
extern "C" void GUISkin_get_verticalScrollbarUpButton_m3320 ();
extern "C" void GUISkin_set_verticalScrollbarUpButton_m3321 ();
extern "C" void GUISkin_get_verticalScrollbarDownButton_m3322 ();
extern "C" void GUISkin_set_verticalScrollbarDownButton_m3323 ();
extern "C" void GUISkin_get_scrollView_m3324 ();
extern "C" void GUISkin_set_scrollView_m3325 ();
extern "C" void GUISkin_get_customStyles_m3326 ();
extern "C" void GUISkin_set_customStyles_m3327 ();
extern "C" void GUISkin_get_settings_m3328 ();
extern "C" void GUISkin_get_error_m3329 ();
extern "C" void GUISkin_Apply_m3330 ();
extern "C" void GUISkin_BuildStyleCache_m3331 ();
extern "C" void GUISkin_GetStyle_m3332 ();
extern "C" void GUISkin_FindStyle_m3333 ();
extern "C" void GUISkin_MakeCurrent_m3334 ();
extern "C" void GUISkin_GetEnumerator_m3335 ();
extern "C" void GUIStyleState__ctor_m3336 ();
extern "C" void GUIStyleState__ctor_m3337 ();
extern "C" void GUIStyleState_Finalize_m3338 ();
extern "C" void GUIStyleState_Init_m3339 ();
extern "C" void GUIStyleState_Cleanup_m3340 ();
extern "C" void GUIStyleState_GetBackgroundInternal_m3341 ();
extern "C" void GUIStyleState_set_textColor_m3342 ();
extern "C" void GUIStyleState_INTERNAL_set_textColor_m3343 ();
extern "C" void RectOffset__ctor_m2389 ();
extern "C" void RectOffset__ctor_m3344 ();
extern "C" void RectOffset_Finalize_m3345 ();
extern "C" void RectOffset_ToString_m3346 ();
extern "C" void RectOffset_Init_m3347 ();
extern "C" void RectOffset_Cleanup_m3348 ();
extern "C" void RectOffset_get_left_m2387 ();
extern "C" void RectOffset_set_left_m3349 ();
extern "C" void RectOffset_get_right_m3350 ();
extern "C" void RectOffset_set_right_m3351 ();
extern "C" void RectOffset_get_top_m2388 ();
extern "C" void RectOffset_set_top_m3352 ();
extern "C" void RectOffset_get_bottom_m3353 ();
extern "C" void RectOffset_set_bottom_m3354 ();
extern "C" void RectOffset_get_horizontal_m2382 ();
extern "C" void RectOffset_get_vertical_m2384 ();
extern "C" void GUIStyle__ctor_m3355 ();
extern "C" void GUIStyle__cctor_m3356 ();
extern "C" void GUIStyle_Finalize_m3357 ();
extern "C" void GUIStyle_get_normal_m3358 ();
extern "C" void GUIStyle_get_margin_m3359 ();
extern "C" void GUIStyle_get_padding_m3360 ();
extern "C" void GUIStyle_get_none_m3361 ();
extern "C" void GUIStyle_ToString_m3362 ();
extern "C" void GUIStyle_Init_m3363 ();
extern "C" void GUIStyle_Cleanup_m3364 ();
extern "C" void GUIStyle_get_name_m3365 ();
extern "C" void GUIStyle_set_name_m3366 ();
extern "C" void GUIStyle_GetStyleStatePtr_m3367 ();
extern "C" void GUIStyle_GetRectOffsetPtr_m3368 ();
extern "C" void GUIStyle_get_fixedWidth_m3369 ();
extern "C" void GUIStyle_get_fixedHeight_m3370 ();
extern "C" void GUIStyle_get_stretchWidth_m3371 ();
extern "C" void GUIStyle_set_stretchWidth_m3372 ();
extern "C" void GUIStyle_get_stretchHeight_m3373 ();
extern "C" void GUIStyle_set_stretchHeight_m3374 ();
extern "C" void GUIStyle_SetDefaultFont_m3375 ();
extern "C" void GUIUtility__cctor_m3376 ();
extern "C" void GUIUtility_get_pixelsPerPoint_m3377 ();
extern "C" void GUIUtility_GetDefaultSkin_m3378 ();
extern "C" void GUIUtility_BeginGUI_m3379 ();
extern "C" void GUIUtility_EndGUI_m3380 ();
extern "C" void GUIUtility_EndGUIFromException_m3381 ();
extern "C" void GUIUtility_CheckOnGUI_m3382 ();
extern "C" void GUIUtility_Internal_GetPixelsPerPoint_m3383 ();
extern "C" void GUIUtility_get_systemCopyBuffer_m2204 ();
extern "C" void GUIUtility_set_systemCopyBuffer_m2205 ();
extern "C" void GUIUtility_Internal_GetDefaultSkin_m3384 ();
extern "C" void GUIUtility_Internal_ExitGUI_m3385 ();
extern "C" void GUIUtility_Internal_GetGUIDepth_m3386 ();
extern "C" void WrapperlessIcall__ctor_m3387 ();
extern "C" void IL2CPPStructAlignmentAttribute__ctor_m3388 ();
extern "C" void AttributeHelperEngine__cctor_m3389 ();
extern "C" void AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3390 ();
extern "C" void AttributeHelperEngine_GetRequiredComponents_m3391 ();
extern "C" void AttributeHelperEngine_CheckIsEditorScript_m3392 ();
extern "C" void DisallowMultipleComponent__ctor_m3393 ();
extern "C" void RequireComponent__ctor_m3394 ();
extern "C" void AddComponentMenu__ctor_m3395 ();
extern "C" void AddComponentMenu__ctor_m3396 ();
extern "C" void ExecuteInEditMode__ctor_m3397 ();
extern "C" void SetupCoroutine__ctor_m3398 ();
extern "C" void SetupCoroutine_InvokeMember_m3399 ();
extern "C" void SetupCoroutine_InvokeStatic_m3400 ();
extern "C" void WritableAttribute__ctor_m3401 ();
extern "C" void AssemblyIsEditorAssembly__ctor_m3402 ();
extern "C" void GcUserProfileData_ToUserProfile_m3403 ();
extern "C" void GcUserProfileData_AddToArray_m3404 ();
extern "C" void GcAchievementDescriptionData_ToAchievementDescription_m3405 ();
extern "C" void GcAchievementData_ToAchievement_m3406 ();
extern "C" void GcScoreData_ToScore_m3407 ();
extern "C" void Resolution_get_width_m3408 ();
extern "C" void Resolution_set_width_m3409 ();
extern "C" void Resolution_get_height_m3410 ();
extern "C" void Resolution_set_height_m3411 ();
extern "C" void Resolution_get_refreshRate_m3412 ();
extern "C" void Resolution_set_refreshRate_m3413 ();
extern "C" void Resolution_ToString_m3414 ();
extern "C" void LocalUser__ctor_m3415 ();
extern "C" void LocalUser_SetFriends_m3416 ();
extern "C" void LocalUser_SetAuthenticated_m3417 ();
extern "C" void LocalUser_SetUnderage_m3418 ();
extern "C" void LocalUser_get_authenticated_m3419 ();
extern "C" void UserProfile__ctor_m3420 ();
extern "C" void UserProfile__ctor_m3421 ();
extern "C" void UserProfile_ToString_m3422 ();
extern "C" void UserProfile_SetUserName_m3423 ();
extern "C" void UserProfile_SetUserID_m3424 ();
extern "C" void UserProfile_SetImage_m3425 ();
extern "C" void UserProfile_get_userName_m3426 ();
extern "C" void UserProfile_get_id_m3427 ();
extern "C" void UserProfile_get_isFriend_m3428 ();
extern "C" void UserProfile_get_state_m3429 ();
extern "C" void Achievement__ctor_m3430 ();
extern "C" void Achievement__ctor_m3431 ();
extern "C" void Achievement__ctor_m3432 ();
extern "C" void Achievement_ToString_m3433 ();
extern "C" void Achievement_get_id_m3434 ();
extern "C" void Achievement_set_id_m3435 ();
extern "C" void Achievement_get_percentCompleted_m3436 ();
extern "C" void Achievement_set_percentCompleted_m3437 ();
extern "C" void Achievement_get_completed_m3438 ();
extern "C" void Achievement_get_hidden_m3439 ();
extern "C" void Achievement_get_lastReportedDate_m3440 ();
extern "C" void AchievementDescription__ctor_m3441 ();
extern "C" void AchievementDescription_ToString_m3442 ();
extern "C" void AchievementDescription_SetImage_m3443 ();
extern "C" void AchievementDescription_get_id_m3444 ();
extern "C" void AchievementDescription_set_id_m3445 ();
extern "C" void AchievementDescription_get_title_m3446 ();
extern "C" void AchievementDescription_get_achievedDescription_m3447 ();
extern "C" void AchievementDescription_get_unachievedDescription_m3448 ();
extern "C" void AchievementDescription_get_hidden_m3449 ();
extern "C" void AchievementDescription_get_points_m3450 ();
extern "C" void Score__ctor_m3451 ();
extern "C" void Score__ctor_m3452 ();
extern "C" void Score_ToString_m3453 ();
extern "C" void Score_get_leaderboardID_m3454 ();
extern "C" void Score_set_leaderboardID_m3455 ();
extern "C" void Score_get_value_m3456 ();
extern "C" void Score_set_value_m3457 ();
extern "C" void Leaderboard__ctor_m3458 ();
extern "C" void Leaderboard_ToString_m3459 ();
extern "C" void Leaderboard_SetLocalUserScore_m3460 ();
extern "C" void Leaderboard_SetMaxRange_m3461 ();
extern "C" void Leaderboard_SetScores_m3462 ();
extern "C" void Leaderboard_SetTitle_m3463 ();
extern "C" void Leaderboard_GetUserFilter_m3464 ();
extern "C" void Leaderboard_get_id_m3465 ();
extern "C" void Leaderboard_set_id_m3466 ();
extern "C" void Leaderboard_get_userScope_m3467 ();
extern "C" void Leaderboard_set_userScope_m3468 ();
extern "C" void Leaderboard_get_range_m3469 ();
extern "C" void Leaderboard_set_range_m3470 ();
extern "C" void Leaderboard_get_timeScope_m3471 ();
extern "C" void Leaderboard_set_timeScope_m3472 ();
extern "C" void HitInfo_SendMessage_m3473 ();
extern "C" void HitInfo_Compare_m3474 ();
extern "C" void HitInfo_op_Implicit_m3475 ();
extern "C" void SendMouseEvents__cctor_m3476 ();
extern "C" void SendMouseEvents_SetMouseMoved_m3477 ();
extern "C" void SendMouseEvents_DoSendMouseEvents_m3478 ();
extern "C" void SendMouseEvents_SendEvents_m3479 ();
extern "C" void Range__ctor_m3480 ();
extern "C" void PropertyAttribute__ctor_m3481 ();
extern "C" void TooltipAttribute__ctor_m3482 ();
extern "C" void SpaceAttribute__ctor_m3483 ();
extern "C" void SpaceAttribute__ctor_m3484 ();
extern "C" void RangeAttribute__ctor_m3485 ();
extern "C" void TextAreaAttribute__ctor_m3486 ();
extern "C" void SelectionBaseAttribute__ctor_m3487 ();
extern "C" void SliderState__ctor_m3488 ();
extern "C" void StackTraceUtility__ctor_m3489 ();
extern "C" void StackTraceUtility__cctor_m3490 ();
extern "C" void StackTraceUtility_SetProjectFolder_m3491 ();
extern "C" void StackTraceUtility_ExtractStackTrace_m3492 ();
extern "C" void StackTraceUtility_IsSystemStacktraceType_m3493 ();
extern "C" void StackTraceUtility_ExtractStringFromException_m3494 ();
extern "C" void StackTraceUtility_ExtractStringFromExceptionInternal_m3495 ();
extern "C" void StackTraceUtility_PostprocessStacktrace_m3496 ();
extern "C" void StackTraceUtility_ExtractFormattedStackTrace_m3497 ();
extern "C" void UnityException__ctor_m3498 ();
extern "C" void UnityException__ctor_m3499 ();
extern "C" void UnityException__ctor_m3500 ();
extern "C" void UnityException__ctor_m3501 ();
extern "C" void SharedBetweenAnimatorsAttribute__ctor_m3502 ();
extern "C" void StateMachineBehaviour__ctor_m3503 ();
extern "C" void TextEditor__ctor_m3504 ();
extern "C" void TextGenerationSettings_CompareColors_m3505 ();
extern "C" void TextGenerationSettings_CompareVector2_m3506 ();
extern "C" void TextGenerationSettings_Equals_m3507 ();
extern "C" void TrackedReference_Equals_m3508 ();
extern "C" void TrackedReference_GetHashCode_m3509 ();
extern "C" void TrackedReference_op_Equality_m3510 ();
extern "C" void ArgumentCache__ctor_m3511 ();
extern "C" void ArgumentCache_get_unityObjectArgument_m3512 ();
extern "C" void ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3513 ();
extern "C" void ArgumentCache_get_intArgument_m3514 ();
extern "C" void ArgumentCache_get_floatArgument_m3515 ();
extern "C" void ArgumentCache_get_stringArgument_m3516 ();
extern "C" void ArgumentCache_get_boolArgument_m3517 ();
extern "C" void ArgumentCache_TidyAssemblyTypeName_m3518 ();
extern "C" void ArgumentCache_OnBeforeSerialize_m3519 ();
extern "C" void ArgumentCache_OnAfterDeserialize_m3520 ();
extern "C" void BaseInvokableCall__ctor_m3521 ();
extern "C" void BaseInvokableCall__ctor_m3522 ();
extern "C" void BaseInvokableCall_AllowInvoke_m3523 ();
extern "C" void InvokableCall__ctor_m3524 ();
extern "C" void InvokableCall__ctor_m3525 ();
extern "C" void InvokableCall_Invoke_m3526 ();
extern "C" void InvokableCall_Find_m3527 ();
extern "C" void PersistentCall__ctor_m3528 ();
extern "C" void PersistentCall_get_target_m3529 ();
extern "C" void PersistentCall_get_methodName_m3530 ();
extern "C" void PersistentCall_get_mode_m3531 ();
extern "C" void PersistentCall_get_arguments_m3532 ();
extern "C" void PersistentCall_IsValid_m3533 ();
extern "C" void PersistentCall_GetRuntimeCall_m3534 ();
extern "C" void PersistentCall_GetObjectCall_m3535 ();
extern "C" void PersistentCallGroup__ctor_m3536 ();
extern "C" void PersistentCallGroup_Initialize_m3537 ();
extern "C" void InvokableCallList__ctor_m3538 ();
extern "C" void InvokableCallList_AddPersistentInvokableCall_m3539 ();
extern "C" void InvokableCallList_AddListener_m3540 ();
extern "C" void InvokableCallList_RemoveListener_m3541 ();
extern "C" void InvokableCallList_ClearPersistent_m3542 ();
extern "C" void InvokableCallList_Invoke_m3543 ();
extern "C" void UnityEventBase__ctor_m3544 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3545 ();
extern "C" void UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3546 ();
extern "C" void UnityEventBase_FindMethod_m3547 ();
extern "C" void UnityEventBase_FindMethod_m3548 ();
extern "C" void UnityEventBase_DirtyPersistentCalls_m3549 ();
extern "C" void UnityEventBase_RebuildPersistentCallsIfNeeded_m3550 ();
extern "C" void UnityEventBase_AddCall_m3551 ();
extern "C" void UnityEventBase_RemoveListener_m3552 ();
extern "C" void UnityEventBase_Invoke_m3553 ();
extern "C" void UnityEventBase_ToString_m3554 ();
extern "C" void UnityEventBase_GetValidMethodInfo_m3555 ();
extern "C" void UnityEvent__ctor_m1986 ();
extern "C" void UnityEvent_AddListener_m2077 ();
extern "C" void UnityEvent_FindMethod_Impl_m3556 ();
extern "C" void UnityEvent_GetDelegate_m3557 ();
extern "C" void UnityEvent_GetDelegate_m3558 ();
extern "C" void UnityEvent_Invoke_m1988 ();
extern "C" void DefaultValueAttribute__ctor_m3559 ();
extern "C" void DefaultValueAttribute_get_Value_m3560 ();
extern "C" void DefaultValueAttribute_Equals_m3561 ();
extern "C" void DefaultValueAttribute_GetHashCode_m3562 ();
extern "C" void ExcludeFromDocsAttribute__ctor_m3563 ();
extern "C" void FormerlySerializedAsAttribute__ctor_m3564 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m3565 ();
extern "C" void TypeInferenceRuleAttribute__ctor_m3566 ();
extern "C" void TypeInferenceRuleAttribute_ToString_m3567 ();
extern "C" void GenericStack__ctor_m3568 ();
extern "C" void NetFxCoreExtensions_CreateDelegate_m3569 ();
extern "C" void NetFxCoreExtensions_GetMethodInfo_m3570 ();
extern "C" void UnityAdsDelegate__ctor_m3571 ();
extern "C" void UnityAdsDelegate_Invoke_m3572 ();
extern "C" void UnityAdsDelegate_BeginInvoke_m3573 ();
extern "C" void UnityAdsDelegate_EndInvoke_m3574 ();
extern "C" void UnityAction__ctor_m2076 ();
extern "C" void UnityAction_Invoke_m2094 ();
extern "C" void UnityAction_BeginInvoke_m3575 ();
extern "C" void UnityAction_EndInvoke_m3576 ();
extern "C" void ExtensionAttribute__ctor_m3677 ();
extern "C" void Locale_GetText_m3678 ();
extern "C" void Locale_GetText_m3679 ();
extern "C" void KeyBuilder_get_Rng_m3680 ();
extern "C" void KeyBuilder_Key_m3681 ();
extern "C" void KeyBuilder_IV_m3682 ();
extern "C" void SymmetricTransform__ctor_m3683 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m3684 ();
extern "C" void SymmetricTransform_Finalize_m3685 ();
extern "C" void SymmetricTransform_Dispose_m3686 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m3687 ();
extern "C" void SymmetricTransform_Transform_m3688 ();
extern "C" void SymmetricTransform_CBC_m3689 ();
extern "C" void SymmetricTransform_CFB_m3690 ();
extern "C" void SymmetricTransform_OFB_m3691 ();
extern "C" void SymmetricTransform_CTS_m3692 ();
extern "C" void SymmetricTransform_CheckInput_m3693 ();
extern "C" void SymmetricTransform_TransformBlock_m3694 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m3695 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m3696 ();
extern "C" void SymmetricTransform_Random_m3697 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m3698 ();
extern "C" void SymmetricTransform_FinalEncrypt_m3699 ();
extern "C" void SymmetricTransform_FinalDecrypt_m3700 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m3701 ();
extern "C" void Check_SourceAndPredicate_m3702 ();
extern "C" void Aes__ctor_m3703 ();
extern "C" void AesManaged__ctor_m3704 ();
extern "C" void AesManaged_GenerateIV_m3705 ();
extern "C" void AesManaged_GenerateKey_m3706 ();
extern "C" void AesManaged_CreateDecryptor_m3707 ();
extern "C" void AesManaged_CreateEncryptor_m3708 ();
extern "C" void AesManaged_get_IV_m3709 ();
extern "C" void AesManaged_set_IV_m3710 ();
extern "C" void AesManaged_get_Key_m3711 ();
extern "C" void AesManaged_set_Key_m3712 ();
extern "C" void AesManaged_get_KeySize_m3713 ();
extern "C" void AesManaged_set_KeySize_m3714 ();
extern "C" void AesManaged_CreateDecryptor_m3715 ();
extern "C" void AesManaged_CreateEncryptor_m3716 ();
extern "C" void AesManaged_Dispose_m3717 ();
extern "C" void AesTransform__ctor_m3718 ();
extern "C" void AesTransform__cctor_m3719 ();
extern "C" void AesTransform_ECB_m3720 ();
extern "C" void AesTransform_SubByte_m3721 ();
extern "C" void AesTransform_Encrypt128_m3722 ();
extern "C" void AesTransform_Decrypt128_m3723 ();
extern "C" void Locale_GetText_m3745 ();
extern "C" void ModulusRing__ctor_m3746 ();
extern "C" void ModulusRing_BarrettReduction_m3747 ();
extern "C" void ModulusRing_Multiply_m3748 ();
extern "C" void ModulusRing_Difference_m3749 ();
extern "C" void ModulusRing_Pow_m3750 ();
extern "C" void ModulusRing_Pow_m3751 ();
extern "C" void Kernel_AddSameSign_m3752 ();
extern "C" void Kernel_Subtract_m3753 ();
extern "C" void Kernel_MinusEq_m3754 ();
extern "C" void Kernel_PlusEq_m3755 ();
extern "C" void Kernel_Compare_m3756 ();
extern "C" void Kernel_SingleByteDivideInPlace_m3757 ();
extern "C" void Kernel_DwordMod_m3758 ();
extern "C" void Kernel_DwordDivMod_m3759 ();
extern "C" void Kernel_multiByteDivide_m3760 ();
extern "C" void Kernel_LeftShift_m3761 ();
extern "C" void Kernel_RightShift_m3762 ();
extern "C" void Kernel_Multiply_m3763 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m3764 ();
extern "C" void Kernel_modInverse_m3765 ();
extern "C" void Kernel_modInverse_m3766 ();
extern "C" void BigInteger__ctor_m3767 ();
extern "C" void BigInteger__ctor_m3768 ();
extern "C" void BigInteger__ctor_m3769 ();
extern "C" void BigInteger__ctor_m3770 ();
extern "C" void BigInteger__ctor_m3771 ();
extern "C" void BigInteger__cctor_m3772 ();
extern "C" void BigInteger_get_Rng_m3773 ();
extern "C" void BigInteger_GenerateRandom_m3774 ();
extern "C" void BigInteger_GenerateRandom_m3775 ();
extern "C" void BigInteger_BitCount_m3776 ();
extern "C" void BigInteger_TestBit_m3777 ();
extern "C" void BigInteger_SetBit_m3778 ();
extern "C" void BigInteger_SetBit_m3779 ();
extern "C" void BigInteger_LowestSetBit_m3780 ();
extern "C" void BigInteger_GetBytes_m3781 ();
extern "C" void BigInteger_ToString_m3782 ();
extern "C" void BigInteger_ToString_m3783 ();
extern "C" void BigInteger_Normalize_m3784 ();
extern "C" void BigInteger_Clear_m3785 ();
extern "C" void BigInteger_GetHashCode_m3786 ();
extern "C" void BigInteger_ToString_m3787 ();
extern "C" void BigInteger_Equals_m3788 ();
extern "C" void BigInteger_ModInverse_m3789 ();
extern "C" void BigInteger_ModPow_m3790 ();
extern "C" void BigInteger_GeneratePseudoPrime_m3791 ();
extern "C" void BigInteger_Incr2_m3792 ();
extern "C" void BigInteger_op_Implicit_m3793 ();
extern "C" void BigInteger_op_Implicit_m3794 ();
extern "C" void BigInteger_op_Addition_m3795 ();
extern "C" void BigInteger_op_Subtraction_m3796 ();
extern "C" void BigInteger_op_Modulus_m3797 ();
extern "C" void BigInteger_op_Modulus_m3798 ();
extern "C" void BigInteger_op_Division_m3799 ();
extern "C" void BigInteger_op_Multiply_m3800 ();
extern "C" void BigInteger_op_LeftShift_m3801 ();
extern "C" void BigInteger_op_RightShift_m3802 ();
extern "C" void BigInteger_op_Equality_m3803 ();
extern "C" void BigInteger_op_Inequality_m3804 ();
extern "C" void BigInteger_op_Equality_m3805 ();
extern "C" void BigInteger_op_Inequality_m3806 ();
extern "C" void BigInteger_op_GreaterThan_m3807 ();
extern "C" void BigInteger_op_LessThan_m3808 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m3809 ();
extern "C" void BigInteger_op_LessThanOrEqual_m3810 ();
extern "C" void PrimalityTests_GetSPPRounds_m3811 ();
extern "C" void PrimalityTests_RabinMillerTest_m3812 ();
extern "C" void PrimeGeneratorBase__ctor_m3813 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m3814 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m3815 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m3816 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m3817 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m3818 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3819 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3820 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m3821 ();
extern "C" void ASN1__ctor_m3822 ();
extern "C" void ASN1__ctor_m3823 ();
extern "C" void ASN1__ctor_m3824 ();
extern "C" void ASN1_get_Count_m3825 ();
extern "C" void ASN1_get_Tag_m3826 ();
extern "C" void ASN1_get_Length_m3827 ();
extern "C" void ASN1_get_Value_m3828 ();
extern "C" void ASN1_set_Value_m3829 ();
extern "C" void ASN1_CompareArray_m3830 ();
extern "C" void ASN1_CompareValue_m3831 ();
extern "C" void ASN1_Add_m3832 ();
extern "C" void ASN1_GetBytes_m3833 ();
extern "C" void ASN1_Decode_m3834 ();
extern "C" void ASN1_DecodeTLV_m3835 ();
extern "C" void ASN1_get_Item_m3836 ();
extern "C" void ASN1_Element_m3837 ();
extern "C" void ASN1_ToString_m3838 ();
extern "C" void ASN1Convert_FromInt32_m3839 ();
extern "C" void ASN1Convert_FromOid_m3840 ();
extern "C" void ASN1Convert_ToInt32_m3841 ();
extern "C" void ASN1Convert_ToOid_m3842 ();
extern "C" void ASN1Convert_ToDateTime_m3843 ();
extern "C" void BitConverterLE_GetUIntBytes_m3844 ();
extern "C" void BitConverterLE_GetBytes_m3845 ();
extern "C" void ContentInfo__ctor_m3846 ();
extern "C" void ContentInfo__ctor_m3847 ();
extern "C" void ContentInfo__ctor_m3848 ();
extern "C" void ContentInfo__ctor_m3849 ();
extern "C" void ContentInfo_get_ASN1_m3850 ();
extern "C" void ContentInfo_get_Content_m3851 ();
extern "C" void ContentInfo_set_Content_m3852 ();
extern "C" void ContentInfo_get_ContentType_m3853 ();
extern "C" void ContentInfo_set_ContentType_m3854 ();
extern "C" void ContentInfo_GetASN1_m3855 ();
extern "C" void EncryptedData__ctor_m3856 ();
extern "C" void EncryptedData__ctor_m3857 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m3858 ();
extern "C" void EncryptedData_get_EncryptedContent_m3859 ();
extern "C" void ARC4Managed__ctor_m3860 ();
extern "C" void ARC4Managed_Finalize_m3861 ();
extern "C" void ARC4Managed_Dispose_m3862 ();
extern "C" void ARC4Managed_get_Key_m3863 ();
extern "C" void ARC4Managed_set_Key_m3864 ();
extern "C" void ARC4Managed_get_CanReuseTransform_m3865 ();
extern "C" void ARC4Managed_CreateEncryptor_m3866 ();
extern "C" void ARC4Managed_CreateDecryptor_m3867 ();
extern "C" void ARC4Managed_GenerateIV_m3868 ();
extern "C" void ARC4Managed_GenerateKey_m3869 ();
extern "C" void ARC4Managed_KeySetup_m3870 ();
extern "C" void ARC4Managed_CheckInput_m3871 ();
extern "C" void ARC4Managed_TransformBlock_m3872 ();
extern "C" void ARC4Managed_InternalTransformBlock_m3873 ();
extern "C" void ARC4Managed_TransformFinalBlock_m3874 ();
extern "C" void CryptoConvert_ToHex_m3875 ();
extern "C" void KeyBuilder_get_Rng_m3876 ();
extern "C" void KeyBuilder_Key_m3877 ();
extern "C" void MD2__ctor_m3878 ();
extern "C" void MD2_Create_m3879 ();
extern "C" void MD2_Create_m3880 ();
extern "C" void MD2Managed__ctor_m3881 ();
extern "C" void MD2Managed__cctor_m3882 ();
extern "C" void MD2Managed_Padding_m3883 ();
extern "C" void MD2Managed_Initialize_m3884 ();
extern "C" void MD2Managed_HashCore_m3885 ();
extern "C" void MD2Managed_HashFinal_m3886 ();
extern "C" void MD2Managed_MD2Transform_m3887 ();
extern "C" void PKCS1__cctor_m3888 ();
extern "C" void PKCS1_Compare_m3889 ();
extern "C" void PKCS1_I2OSP_m3890 ();
extern "C" void PKCS1_OS2IP_m3891 ();
extern "C" void PKCS1_RSASP1_m3892 ();
extern "C" void PKCS1_RSAVP1_m3893 ();
extern "C" void PKCS1_Sign_v15_m3894 ();
extern "C" void PKCS1_Verify_v15_m3895 ();
extern "C" void PKCS1_Verify_v15_m3896 ();
extern "C" void PKCS1_Encode_v15_m3897 ();
extern "C" void PrivateKeyInfo__ctor_m3898 ();
extern "C" void PrivateKeyInfo__ctor_m3899 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m3900 ();
extern "C" void PrivateKeyInfo_Decode_m3901 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m3902 ();
extern "C" void PrivateKeyInfo_Normalize_m3903 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m3904 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m3905 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m3906 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m3907 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m3908 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m3909 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m3910 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m3911 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m3912 ();
extern "C" void RC4__ctor_m3913 ();
extern "C" void RC4__cctor_m3914 ();
extern "C" void RC4_get_IV_m3915 ();
extern "C" void RC4_set_IV_m3916 ();
extern "C" void KeyGeneratedEventHandler__ctor_m3917 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m3918 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m3919 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m3920 ();
extern "C" void RSAManaged__ctor_m3921 ();
extern "C" void RSAManaged__ctor_m3922 ();
extern "C" void RSAManaged_Finalize_m3923 ();
extern "C" void RSAManaged_GenerateKeyPair_m3924 ();
extern "C" void RSAManaged_get_KeySize_m3925 ();
extern "C" void RSAManaged_get_PublicOnly_m3926 ();
extern "C" void RSAManaged_DecryptValue_m3927 ();
extern "C" void RSAManaged_EncryptValue_m3928 ();
extern "C" void RSAManaged_ExportParameters_m3929 ();
extern "C" void RSAManaged_ImportParameters_m3930 ();
extern "C" void RSAManaged_Dispose_m3931 ();
extern "C" void RSAManaged_ToXmlString_m3932 ();
extern "C" void RSAManaged_GetPaddedValue_m3933 ();
extern "C" void SafeBag__ctor_m3934 ();
extern "C" void SafeBag_get_BagOID_m3935 ();
extern "C" void SafeBag_get_ASN1_m3936 ();
extern "C" void DeriveBytes__ctor_m3937 ();
extern "C" void DeriveBytes__cctor_m3938 ();
extern "C" void DeriveBytes_set_HashName_m3939 ();
extern "C" void DeriveBytes_set_IterationCount_m3940 ();
extern "C" void DeriveBytes_set_Password_m3941 ();
extern "C" void DeriveBytes_set_Salt_m3942 ();
extern "C" void DeriveBytes_Adjust_m3943 ();
extern "C" void DeriveBytes_Derive_m3944 ();
extern "C" void DeriveBytes_DeriveKey_m3945 ();
extern "C" void DeriveBytes_DeriveIV_m3946 ();
extern "C" void DeriveBytes_DeriveMAC_m3947 ();
extern "C" void PKCS12__ctor_m3948 ();
extern "C" void PKCS12__ctor_m3949 ();
extern "C" void PKCS12__ctor_m3950 ();
extern "C" void PKCS12__cctor_m3951 ();
extern "C" void PKCS12_Decode_m3952 ();
extern "C" void PKCS12_Finalize_m3953 ();
extern "C" void PKCS12_set_Password_m3954 ();
extern "C" void PKCS12_get_IterationCount_m3955 ();
extern "C" void PKCS12_set_IterationCount_m3956 ();
extern "C" void PKCS12_get_Keys_m3957 ();
extern "C" void PKCS12_get_Certificates_m3958 ();
extern "C" void PKCS12_get_RNG_m3959 ();
extern "C" void PKCS12_Compare_m3960 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m3961 ();
extern "C" void PKCS12_Decrypt_m3962 ();
extern "C" void PKCS12_Decrypt_m3963 ();
extern "C" void PKCS12_Encrypt_m3964 ();
extern "C" void PKCS12_GetExistingParameters_m3965 ();
extern "C" void PKCS12_AddPrivateKey_m3966 ();
extern "C" void PKCS12_ReadSafeBag_m3967 ();
extern "C" void PKCS12_CertificateSafeBag_m3968 ();
extern "C" void PKCS12_MAC_m3969 ();
extern "C" void PKCS12_GetBytes_m3970 ();
extern "C" void PKCS12_EncryptedContentInfo_m3971 ();
extern "C" void PKCS12_AddCertificate_m3972 ();
extern "C" void PKCS12_AddCertificate_m3973 ();
extern "C" void PKCS12_RemoveCertificate_m3974 ();
extern "C" void PKCS12_RemoveCertificate_m3975 ();
extern "C" void PKCS12_Clone_m3976 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m3977 ();
extern "C" void X501__cctor_m3978 ();
extern "C" void X501_ToString_m3979 ();
extern "C" void X501_ToString_m3980 ();
extern "C" void X501_AppendEntry_m3981 ();
extern "C" void X509Certificate__ctor_m3982 ();
extern "C" void X509Certificate__cctor_m3983 ();
extern "C" void X509Certificate_Parse_m3984 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m3985 ();
extern "C" void X509Certificate_get_DSA_m3986 ();
extern "C" void X509Certificate_set_DSA_m3987 ();
extern "C" void X509Certificate_get_Extensions_m3988 ();
extern "C" void X509Certificate_get_Hash_m3989 ();
extern "C" void X509Certificate_get_IssuerName_m3990 ();
extern "C" void X509Certificate_get_KeyAlgorithm_m3991 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m3992 ();
extern "C" void X509Certificate_set_KeyAlgorithmParameters_m3993 ();
extern "C" void X509Certificate_get_PublicKey_m3994 ();
extern "C" void X509Certificate_get_RSA_m3995 ();
extern "C" void X509Certificate_set_RSA_m3996 ();
extern "C" void X509Certificate_get_RawData_m3997 ();
extern "C" void X509Certificate_get_SerialNumber_m3998 ();
extern "C" void X509Certificate_get_Signature_m3999 ();
extern "C" void X509Certificate_get_SignatureAlgorithm_m4000 ();
extern "C" void X509Certificate_get_SubjectName_m4001 ();
extern "C" void X509Certificate_get_ValidFrom_m4002 ();
extern "C" void X509Certificate_get_ValidUntil_m4003 ();
extern "C" void X509Certificate_get_Version_m4004 ();
extern "C" void X509Certificate_get_IsCurrent_m4005 ();
extern "C" void X509Certificate_WasCurrent_m4006 ();
extern "C" void X509Certificate_VerifySignature_m4007 ();
extern "C" void X509Certificate_VerifySignature_m4008 ();
extern "C" void X509Certificate_VerifySignature_m4009 ();
extern "C" void X509Certificate_get_IsSelfSigned_m4010 ();
extern "C" void X509Certificate_GetIssuerName_m4011 ();
extern "C" void X509Certificate_GetSubjectName_m4012 ();
extern "C" void X509Certificate_GetObjectData_m4013 ();
extern "C" void X509Certificate_PEM_m4014 ();
extern "C" void X509CertificateEnumerator__ctor_m4015 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4016 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4017 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4018 ();
extern "C" void X509CertificateEnumerator_get_Current_m4019 ();
extern "C" void X509CertificateEnumerator_MoveNext_m4020 ();
extern "C" void X509CertificateEnumerator_Reset_m4021 ();
extern "C" void X509CertificateCollection__ctor_m4022 ();
extern "C" void X509CertificateCollection__ctor_m4023 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4024 ();
extern "C" void X509CertificateCollection_get_Item_m4025 ();
extern "C" void X509CertificateCollection_Add_m4026 ();
extern "C" void X509CertificateCollection_AddRange_m4027 ();
extern "C" void X509CertificateCollection_Contains_m4028 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4029 ();
extern "C" void X509CertificateCollection_GetHashCode_m4030 ();
extern "C" void X509CertificateCollection_IndexOf_m4031 ();
extern "C" void X509CertificateCollection_Remove_m4032 ();
extern "C" void X509CertificateCollection_Compare_m4033 ();
extern "C" void X509Chain__ctor_m4034 ();
extern "C" void X509Chain__ctor_m4035 ();
extern "C" void X509Chain_get_Status_m4036 ();
extern "C" void X509Chain_get_TrustAnchors_m4037 ();
extern "C" void X509Chain_Build_m4038 ();
extern "C" void X509Chain_IsValid_m4039 ();
extern "C" void X509Chain_FindCertificateParent_m4040 ();
extern "C" void X509Chain_FindCertificateRoot_m4041 ();
extern "C" void X509Chain_IsTrusted_m4042 ();
extern "C" void X509Chain_IsParent_m4043 ();
extern "C" void X509CrlEntry__ctor_m4044 ();
extern "C" void X509CrlEntry_get_SerialNumber_m4045 ();
extern "C" void X509CrlEntry_get_RevocationDate_m4046 ();
extern "C" void X509CrlEntry_get_Extensions_m4047 ();
extern "C" void X509Crl__ctor_m4048 ();
extern "C" void X509Crl_Parse_m4049 ();
extern "C" void X509Crl_get_Extensions_m4050 ();
extern "C" void X509Crl_get_Hash_m4051 ();
extern "C" void X509Crl_get_IssuerName_m4052 ();
extern "C" void X509Crl_get_NextUpdate_m4053 ();
extern "C" void X509Crl_Compare_m4054 ();
extern "C" void X509Crl_GetCrlEntry_m4055 ();
extern "C" void X509Crl_GetCrlEntry_m4056 ();
extern "C" void X509Crl_GetHashName_m4057 ();
extern "C" void X509Crl_VerifySignature_m4058 ();
extern "C" void X509Crl_VerifySignature_m4059 ();
extern "C" void X509Crl_VerifySignature_m4060 ();
extern "C" void X509Extension__ctor_m4061 ();
extern "C" void X509Extension__ctor_m4062 ();
extern "C" void X509Extension_Decode_m4063 ();
extern "C" void X509Extension_Encode_m4064 ();
extern "C" void X509Extension_get_Oid_m4065 ();
extern "C" void X509Extension_get_Critical_m4066 ();
extern "C" void X509Extension_get_Value_m4067 ();
extern "C" void X509Extension_Equals_m4068 ();
extern "C" void X509Extension_GetHashCode_m4069 ();
extern "C" void X509Extension_WriteLine_m4070 ();
extern "C" void X509Extension_ToString_m4071 ();
extern "C" void X509ExtensionCollection__ctor_m4072 ();
extern "C" void X509ExtensionCollection__ctor_m4073 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4074 ();
extern "C" void X509ExtensionCollection_IndexOf_m4075 ();
extern "C" void X509ExtensionCollection_get_Item_m4076 ();
extern "C" void X509Store__ctor_m4077 ();
extern "C" void X509Store_get_Certificates_m4078 ();
extern "C" void X509Store_get_Crls_m4079 ();
extern "C" void X509Store_Load_m4080 ();
extern "C" void X509Store_LoadCertificate_m4081 ();
extern "C" void X509Store_LoadCrl_m4082 ();
extern "C" void X509Store_CheckStore_m4083 ();
extern "C" void X509Store_BuildCertificatesCollection_m4084 ();
extern "C" void X509Store_BuildCrlsCollection_m4085 ();
extern "C" void X509StoreManager_get_CurrentUser_m4086 ();
extern "C" void X509StoreManager_get_LocalMachine_m4087 ();
extern "C" void X509StoreManager_get_TrustedRootCertificates_m4088 ();
extern "C" void X509Stores__ctor_m4089 ();
extern "C" void X509Stores_get_TrustedRoot_m4090 ();
extern "C" void X509Stores_Open_m4091 ();
extern "C" void AuthorityKeyIdentifierExtension__ctor_m4092 ();
extern "C" void AuthorityKeyIdentifierExtension_Decode_m4093 ();
extern "C" void AuthorityKeyIdentifierExtension_get_Identifier_m4094 ();
extern "C" void AuthorityKeyIdentifierExtension_ToString_m4095 ();
extern "C" void BasicConstraintsExtension__ctor_m4096 ();
extern "C" void BasicConstraintsExtension_Decode_m4097 ();
extern "C" void BasicConstraintsExtension_Encode_m4098 ();
extern "C" void BasicConstraintsExtension_get_CertificateAuthority_m4099 ();
extern "C" void BasicConstraintsExtension_ToString_m4100 ();
extern "C" void ExtendedKeyUsageExtension__ctor_m4101 ();
extern "C" void ExtendedKeyUsageExtension_Decode_m4102 ();
extern "C" void ExtendedKeyUsageExtension_Encode_m4103 ();
extern "C" void ExtendedKeyUsageExtension_get_KeyPurpose_m4104 ();
extern "C" void ExtendedKeyUsageExtension_ToString_m4105 ();
extern "C" void GeneralNames__ctor_m4106 ();
extern "C" void GeneralNames_get_DNSNames_m4107 ();
extern "C" void GeneralNames_get_IPAddresses_m4108 ();
extern "C" void GeneralNames_ToString_m4109 ();
extern "C" void KeyUsageExtension__ctor_m4110 ();
extern "C" void KeyUsageExtension_Decode_m4111 ();
extern "C" void KeyUsageExtension_Encode_m4112 ();
extern "C" void KeyUsageExtension_Support_m4113 ();
extern "C" void KeyUsageExtension_ToString_m4114 ();
extern "C" void NetscapeCertTypeExtension__ctor_m4115 ();
extern "C" void NetscapeCertTypeExtension_Decode_m4116 ();
extern "C" void NetscapeCertTypeExtension_Support_m4117 ();
extern "C" void NetscapeCertTypeExtension_ToString_m4118 ();
extern "C" void SubjectAltNameExtension__ctor_m4119 ();
extern "C" void SubjectAltNameExtension_Decode_m4120 ();
extern "C" void SubjectAltNameExtension_get_DNSNames_m4121 ();
extern "C" void SubjectAltNameExtension_get_IPAddresses_m4122 ();
extern "C" void SubjectAltNameExtension_ToString_m4123 ();
extern "C" void HMAC__ctor_m4124 ();
extern "C" void HMAC_get_Key_m4125 ();
extern "C" void HMAC_set_Key_m4126 ();
extern "C" void HMAC_Initialize_m4127 ();
extern "C" void HMAC_HashFinal_m4128 ();
extern "C" void HMAC_HashCore_m4129 ();
extern "C" void HMAC_initializePad_m4130 ();
extern "C" void MD5SHA1__ctor_m4131 ();
extern "C" void MD5SHA1_Initialize_m4132 ();
extern "C" void MD5SHA1_HashFinal_m4133 ();
extern "C" void MD5SHA1_HashCore_m4134 ();
extern "C" void MD5SHA1_CreateSignature_m4135 ();
extern "C" void MD5SHA1_VerifySignature_m4136 ();
extern "C" void Alert__ctor_m4137 ();
extern "C" void Alert__ctor_m4138 ();
extern "C" void Alert_get_Level_m4139 ();
extern "C" void Alert_get_Description_m4140 ();
extern "C" void Alert_get_IsWarning_m4141 ();
extern "C" void Alert_get_IsCloseNotify_m4142 ();
extern "C" void Alert_inferAlertLevel_m4143 ();
extern "C" void Alert_GetAlertMessage_m4144 ();
extern "C" void CipherSuite__ctor_m4145 ();
extern "C" void CipherSuite__cctor_m4146 ();
extern "C" void CipherSuite_get_EncryptionCipher_m4147 ();
extern "C" void CipherSuite_get_DecryptionCipher_m4148 ();
extern "C" void CipherSuite_get_ClientHMAC_m4149 ();
extern "C" void CipherSuite_get_ServerHMAC_m4150 ();
extern "C" void CipherSuite_get_CipherAlgorithmType_m4151 ();
extern "C" void CipherSuite_get_HashAlgorithmName_m4152 ();
extern "C" void CipherSuite_get_HashAlgorithmType_m4153 ();
extern "C" void CipherSuite_get_HashSize_m4154 ();
extern "C" void CipherSuite_get_ExchangeAlgorithmType_m4155 ();
extern "C" void CipherSuite_get_CipherMode_m4156 ();
extern "C" void CipherSuite_get_Code_m4157 ();
extern "C" void CipherSuite_get_Name_m4158 ();
extern "C" void CipherSuite_get_IsExportable_m4159 ();
extern "C" void CipherSuite_get_KeyMaterialSize_m4160 ();
extern "C" void CipherSuite_get_KeyBlockSize_m4161 ();
extern "C" void CipherSuite_get_ExpandedKeyMaterialSize_m4162 ();
extern "C" void CipherSuite_get_EffectiveKeyBits_m4163 ();
extern "C" void CipherSuite_get_IvSize_m4164 ();
extern "C" void CipherSuite_get_Context_m4165 ();
extern "C" void CipherSuite_set_Context_m4166 ();
extern "C" void CipherSuite_Write_m4167 ();
extern "C" void CipherSuite_Write_m4168 ();
extern "C" void CipherSuite_InitializeCipher_m4169 ();
extern "C" void CipherSuite_EncryptRecord_m4170 ();
extern "C" void CipherSuite_DecryptRecord_m4171 ();
extern "C" void CipherSuite_CreatePremasterSecret_m4172 ();
extern "C" void CipherSuite_PRF_m4173 ();
extern "C" void CipherSuite_Expand_m4174 ();
extern "C" void CipherSuite_createEncryptionCipher_m4175 ();
extern "C" void CipherSuite_createDecryptionCipher_m4176 ();
extern "C" void CipherSuiteCollection__ctor_m4177 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_get_Item_m4178 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_set_Item_m4179 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_IsSynchronized_m4180 ();
extern "C" void CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m4181 ();
extern "C" void CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m4182 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Contains_m4183 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_IndexOf_m4184 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Insert_m4185 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Remove_m4186 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_RemoveAt_m4187 ();
extern "C" void CipherSuiteCollection_System_Collections_IList_Add_m4188 ();
extern "C" void CipherSuiteCollection_get_Item_m4189 ();
extern "C" void CipherSuiteCollection_get_Item_m4190 ();
extern "C" void CipherSuiteCollection_set_Item_m4191 ();
extern "C" void CipherSuiteCollection_get_Item_m4192 ();
extern "C" void CipherSuiteCollection_get_Count_m4193 ();
extern "C" void CipherSuiteCollection_get_IsFixedSize_m4194 ();
extern "C" void CipherSuiteCollection_get_IsReadOnly_m4195 ();
extern "C" void CipherSuiteCollection_CopyTo_m4196 ();
extern "C" void CipherSuiteCollection_Clear_m4197 ();
extern "C" void CipherSuiteCollection_IndexOf_m4198 ();
extern "C" void CipherSuiteCollection_IndexOf_m4199 ();
extern "C" void CipherSuiteCollection_Add_m4200 ();
extern "C" void CipherSuiteCollection_add_m4201 ();
extern "C" void CipherSuiteCollection_add_m4202 ();
extern "C" void CipherSuiteCollection_cultureAwareCompare_m4203 ();
extern "C" void CipherSuiteFactory_GetSupportedCiphers_m4204 ();
extern "C" void CipherSuiteFactory_GetTls1SupportedCiphers_m4205 ();
extern "C" void CipherSuiteFactory_GetSsl3SupportedCiphers_m4206 ();
extern "C" void ClientContext__ctor_m4207 ();
extern "C" void ClientContext_get_SslStream_m4208 ();
extern "C" void ClientContext_get_ClientHelloProtocol_m4209 ();
extern "C" void ClientContext_set_ClientHelloProtocol_m4210 ();
extern "C" void ClientContext_Clear_m4211 ();
extern "C" void ClientRecordProtocol__ctor_m4212 ();
extern "C" void ClientRecordProtocol_GetMessage_m4213 ();
extern "C" void ClientRecordProtocol_ProcessHandshakeMessage_m4214 ();
extern "C" void ClientRecordProtocol_createClientHandshakeMessage_m4215 ();
extern "C" void ClientRecordProtocol_createServerHandshakeMessage_m4216 ();
extern "C" void ClientSessionInfo__ctor_m4217 ();
extern "C" void ClientSessionInfo__cctor_m4218 ();
extern "C" void ClientSessionInfo_Finalize_m4219 ();
extern "C" void ClientSessionInfo_get_HostName_m4220 ();
extern "C" void ClientSessionInfo_get_Id_m4221 ();
extern "C" void ClientSessionInfo_get_Valid_m4222 ();
extern "C" void ClientSessionInfo_GetContext_m4223 ();
extern "C" void ClientSessionInfo_SetContext_m4224 ();
extern "C" void ClientSessionInfo_KeepAlive_m4225 ();
extern "C" void ClientSessionInfo_Dispose_m4226 ();
extern "C" void ClientSessionInfo_Dispose_m4227 ();
extern "C" void ClientSessionInfo_CheckDisposed_m4228 ();
extern "C" void ClientSessionCache__cctor_m4229 ();
extern "C" void ClientSessionCache_Add_m4230 ();
extern "C" void ClientSessionCache_FromHost_m4231 ();
extern "C" void ClientSessionCache_FromContext_m4232 ();
extern "C" void ClientSessionCache_SetContextInCache_m4233 ();
extern "C" void ClientSessionCache_SetContextFromCache_m4234 ();
extern "C" void Context__ctor_m4235 ();
extern "C" void Context_get_AbbreviatedHandshake_m4236 ();
extern "C" void Context_set_AbbreviatedHandshake_m4237 ();
extern "C" void Context_get_ProtocolNegotiated_m4238 ();
extern "C" void Context_set_ProtocolNegotiated_m4239 ();
extern "C" void Context_get_SecurityProtocol_m4240 ();
extern "C" void Context_set_SecurityProtocol_m4241 ();
extern "C" void Context_get_SecurityProtocolFlags_m4242 ();
extern "C" void Context_get_Protocol_m4243 ();
extern "C" void Context_get_SessionId_m4244 ();
extern "C" void Context_set_SessionId_m4245 ();
extern "C" void Context_get_CompressionMethod_m4246 ();
extern "C" void Context_set_CompressionMethod_m4247 ();
extern "C" void Context_get_ServerSettings_m4248 ();
extern "C" void Context_get_ClientSettings_m4249 ();
extern "C" void Context_get_LastHandshakeMsg_m4250 ();
extern "C" void Context_set_LastHandshakeMsg_m4251 ();
extern "C" void Context_get_HandshakeState_m4252 ();
extern "C" void Context_set_HandshakeState_m4253 ();
extern "C" void Context_get_ReceivedConnectionEnd_m4254 ();
extern "C" void Context_set_ReceivedConnectionEnd_m4255 ();
extern "C" void Context_get_SentConnectionEnd_m4256 ();
extern "C" void Context_set_SentConnectionEnd_m4257 ();
extern "C" void Context_get_SupportedCiphers_m4258 ();
extern "C" void Context_set_SupportedCiphers_m4259 ();
extern "C" void Context_get_HandshakeMessages_m4260 ();
extern "C" void Context_get_WriteSequenceNumber_m4261 ();
extern "C" void Context_set_WriteSequenceNumber_m4262 ();
extern "C" void Context_get_ReadSequenceNumber_m4263 ();
extern "C" void Context_set_ReadSequenceNumber_m4264 ();
extern "C" void Context_get_ClientRandom_m4265 ();
extern "C" void Context_set_ClientRandom_m4266 ();
extern "C" void Context_get_ServerRandom_m4267 ();
extern "C" void Context_set_ServerRandom_m4268 ();
extern "C" void Context_get_RandomCS_m4269 ();
extern "C" void Context_set_RandomCS_m4270 ();
extern "C" void Context_get_RandomSC_m4271 ();
extern "C" void Context_set_RandomSC_m4272 ();
extern "C" void Context_get_MasterSecret_m4273 ();
extern "C" void Context_set_MasterSecret_m4274 ();
extern "C" void Context_get_ClientWriteKey_m4275 ();
extern "C" void Context_set_ClientWriteKey_m4276 ();
extern "C" void Context_get_ServerWriteKey_m4277 ();
extern "C" void Context_set_ServerWriteKey_m4278 ();
extern "C" void Context_get_ClientWriteIV_m4279 ();
extern "C" void Context_set_ClientWriteIV_m4280 ();
extern "C" void Context_get_ServerWriteIV_m4281 ();
extern "C" void Context_set_ServerWriteIV_m4282 ();
extern "C" void Context_get_RecordProtocol_m4283 ();
extern "C" void Context_set_RecordProtocol_m4284 ();
extern "C" void Context_GetUnixTime_m4285 ();
extern "C" void Context_GetSecureRandomBytes_m4286 ();
extern "C" void Context_Clear_m4287 ();
extern "C" void Context_ClearKeyInfo_m4288 ();
extern "C" void Context_DecodeProtocolCode_m4289 ();
extern "C" void Context_ChangeProtocol_m4290 ();
extern "C" void Context_get_Current_m4291 ();
extern "C" void Context_get_Negotiating_m4292 ();
extern "C" void Context_get_Read_m4293 ();
extern "C" void Context_get_Write_m4294 ();
extern "C" void Context_StartSwitchingSecurityParameters_m4295 ();
extern "C" void Context_EndSwitchingSecurityParameters_m4296 ();
extern "C" void HttpsClientStream__ctor_m4297 ();
extern "C" void HttpsClientStream_get_TrustFailure_m4298 ();
extern "C" void HttpsClientStream_RaiseServerCertificateValidation_m4299 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4300 ();
extern "C" void HttpsClientStream_U3CHttpsClientStreamU3Em__1_m4301 ();
extern "C" void ReceiveRecordAsyncResult__ctor_m4302 ();
extern "C" void ReceiveRecordAsyncResult_get_Record_m4303 ();
extern "C" void ReceiveRecordAsyncResult_get_ResultingBuffer_m4304 ();
extern "C" void ReceiveRecordAsyncResult_get_InitialBuffer_m4305 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncState_m4306 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncException_m4307 ();
extern "C" void ReceiveRecordAsyncResult_get_CompletedWithError_m4308 ();
extern "C" void ReceiveRecordAsyncResult_get_AsyncWaitHandle_m4309 ();
extern "C" void ReceiveRecordAsyncResult_get_IsCompleted_m4310 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4311 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4312 ();
extern "C" void ReceiveRecordAsyncResult_SetComplete_m4313 ();
extern "C" void SendRecordAsyncResult__ctor_m4314 ();
extern "C" void SendRecordAsyncResult_get_Message_m4315 ();
extern "C" void SendRecordAsyncResult_get_AsyncState_m4316 ();
extern "C" void SendRecordAsyncResult_get_AsyncException_m4317 ();
extern "C" void SendRecordAsyncResult_get_CompletedWithError_m4318 ();
extern "C" void SendRecordAsyncResult_get_AsyncWaitHandle_m4319 ();
extern "C" void SendRecordAsyncResult_get_IsCompleted_m4320 ();
extern "C" void SendRecordAsyncResult_SetComplete_m4321 ();
extern "C" void SendRecordAsyncResult_SetComplete_m4322 ();
extern "C" void RecordProtocol__ctor_m4323 ();
extern "C" void RecordProtocol__cctor_m4324 ();
extern "C" void RecordProtocol_get_Context_m4325 ();
extern "C" void RecordProtocol_SendRecord_m4326 ();
extern "C" void RecordProtocol_ProcessChangeCipherSpec_m4327 ();
extern "C" void RecordProtocol_GetMessage_m4328 ();
extern "C" void RecordProtocol_BeginReceiveRecord_m4329 ();
extern "C" void RecordProtocol_InternalReceiveRecordCallback_m4330 ();
extern "C" void RecordProtocol_EndReceiveRecord_m4331 ();
extern "C" void RecordProtocol_ReceiveRecord_m4332 ();
extern "C" void RecordProtocol_ReadRecordBuffer_m4333 ();
extern "C" void RecordProtocol_ReadClientHelloV2_m4334 ();
extern "C" void RecordProtocol_ReadStandardRecordBuffer_m4335 ();
extern "C" void RecordProtocol_ProcessAlert_m4336 ();
extern "C" void RecordProtocol_SendAlert_m4337 ();
extern "C" void RecordProtocol_SendAlert_m4338 ();
extern "C" void RecordProtocol_SendAlert_m4339 ();
extern "C" void RecordProtocol_SendChangeCipherSpec_m4340 ();
extern "C" void RecordProtocol_BeginSendRecord_m4341 ();
extern "C" void RecordProtocol_InternalSendRecordCallback_m4342 ();
extern "C" void RecordProtocol_BeginSendRecord_m4343 ();
extern "C" void RecordProtocol_EndSendRecord_m4344 ();
extern "C" void RecordProtocol_SendRecord_m4345 ();
extern "C" void RecordProtocol_EncodeRecord_m4346 ();
extern "C" void RecordProtocol_EncodeRecord_m4347 ();
extern "C" void RecordProtocol_encryptRecordFragment_m4348 ();
extern "C" void RecordProtocol_decryptRecordFragment_m4349 ();
extern "C" void RecordProtocol_Compare_m4350 ();
extern "C" void RecordProtocol_ProcessCipherSpecV2Buffer_m4351 ();
extern "C" void RecordProtocol_MapV2CipherCode_m4352 ();
extern "C" void RSASslSignatureDeformatter__ctor_m4353 ();
extern "C" void RSASslSignatureDeformatter_VerifySignature_m4354 ();
extern "C" void RSASslSignatureDeformatter_SetHashAlgorithm_m4355 ();
extern "C" void RSASslSignatureDeformatter_SetKey_m4356 ();
extern "C" void RSASslSignatureFormatter__ctor_m4357 ();
extern "C" void RSASslSignatureFormatter_CreateSignature_m4358 ();
extern "C" void RSASslSignatureFormatter_SetHashAlgorithm_m4359 ();
extern "C" void RSASslSignatureFormatter_SetKey_m4360 ();
extern "C" void SecurityParameters__ctor_m4361 ();
extern "C" void SecurityParameters_get_Cipher_m4362 ();
extern "C" void SecurityParameters_set_Cipher_m4363 ();
extern "C" void SecurityParameters_get_ClientWriteMAC_m4364 ();
extern "C" void SecurityParameters_set_ClientWriteMAC_m4365 ();
extern "C" void SecurityParameters_get_ServerWriteMAC_m4366 ();
extern "C" void SecurityParameters_set_ServerWriteMAC_m4367 ();
extern "C" void SecurityParameters_Clear_m4368 ();
extern "C" void ValidationResult_get_Trusted_m4369 ();
extern "C" void ValidationResult_get_ErrorCode_m4370 ();
extern "C" void SslClientStream__ctor_m4371 ();
extern "C" void SslClientStream__ctor_m4372 ();
extern "C" void SslClientStream__ctor_m4373 ();
extern "C" void SslClientStream__ctor_m4374 ();
extern "C" void SslClientStream__ctor_m4375 ();
extern "C" void SslClientStream_add_ServerCertValidation_m4376 ();
extern "C" void SslClientStream_remove_ServerCertValidation_m4377 ();
extern "C" void SslClientStream_add_ClientCertSelection_m4378 ();
extern "C" void SslClientStream_remove_ClientCertSelection_m4379 ();
extern "C" void SslClientStream_add_PrivateKeySelection_m4380 ();
extern "C" void SslClientStream_remove_PrivateKeySelection_m4381 ();
extern "C" void SslClientStream_add_ServerCertValidation2_m4382 ();
extern "C" void SslClientStream_remove_ServerCertValidation2_m4383 ();
extern "C" void SslClientStream_get_InputBuffer_m4384 ();
extern "C" void SslClientStream_get_ClientCertificates_m4385 ();
extern "C" void SslClientStream_get_SelectedClientCertificate_m4386 ();
extern "C" void SslClientStream_get_ServerCertValidationDelegate_m4387 ();
extern "C" void SslClientStream_set_ServerCertValidationDelegate_m4388 ();
extern "C" void SslClientStream_get_ClientCertSelectionDelegate_m4389 ();
extern "C" void SslClientStream_set_ClientCertSelectionDelegate_m4390 ();
extern "C" void SslClientStream_get_PrivateKeyCertSelectionDelegate_m4391 ();
extern "C" void SslClientStream_set_PrivateKeyCertSelectionDelegate_m4392 ();
extern "C" void SslClientStream_Finalize_m4393 ();
extern "C" void SslClientStream_Dispose_m4394 ();
extern "C" void SslClientStream_OnBeginNegotiateHandshake_m4395 ();
extern "C" void SslClientStream_SafeReceiveRecord_m4396 ();
extern "C" void SslClientStream_OnNegotiateHandshakeCallback_m4397 ();
extern "C" void SslClientStream_OnLocalCertificateSelection_m4398 ();
extern "C" void SslClientStream_get_HaveRemoteValidation2Callback_m4399 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation2_m4400 ();
extern "C" void SslClientStream_OnRemoteCertificateValidation_m4401 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation_m4402 ();
extern "C" void SslClientStream_RaiseServerCertificateValidation2_m4403 ();
extern "C" void SslClientStream_RaiseClientCertificateSelection_m4404 ();
extern "C" void SslClientStream_OnLocalPrivateKeySelection_m4405 ();
extern "C" void SslClientStream_RaisePrivateKeySelection_m4406 ();
extern "C" void SslCipherSuite__ctor_m4407 ();
extern "C" void SslCipherSuite_ComputeServerRecordMAC_m4408 ();
extern "C" void SslCipherSuite_ComputeClientRecordMAC_m4409 ();
extern "C" void SslCipherSuite_ComputeMasterSecret_m4410 ();
extern "C" void SslCipherSuite_ComputeKeys_m4411 ();
extern "C" void SslCipherSuite_prf_m4412 ();
extern "C" void SslHandshakeHash__ctor_m4413 ();
extern "C" void SslHandshakeHash_Initialize_m4414 ();
extern "C" void SslHandshakeHash_HashFinal_m4415 ();
extern "C" void SslHandshakeHash_HashCore_m4416 ();
extern "C" void SslHandshakeHash_CreateSignature_m4417 ();
extern "C" void SslHandshakeHash_initializePad_m4418 ();
extern "C" void InternalAsyncResult__ctor_m4419 ();
extern "C" void InternalAsyncResult_get_ProceedAfterHandshake_m4420 ();
extern "C" void InternalAsyncResult_get_FromWrite_m4421 ();
extern "C" void InternalAsyncResult_get_Buffer_m4422 ();
extern "C" void InternalAsyncResult_get_Offset_m4423 ();
extern "C" void InternalAsyncResult_get_Count_m4424 ();
extern "C" void InternalAsyncResult_get_BytesRead_m4425 ();
extern "C" void InternalAsyncResult_get_AsyncState_m4426 ();
extern "C" void InternalAsyncResult_get_AsyncException_m4427 ();
extern "C" void InternalAsyncResult_get_CompletedWithError_m4428 ();
extern "C" void InternalAsyncResult_get_AsyncWaitHandle_m4429 ();
extern "C" void InternalAsyncResult_get_IsCompleted_m4430 ();
extern "C" void InternalAsyncResult_SetComplete_m4431 ();
extern "C" void InternalAsyncResult_SetComplete_m4432 ();
extern "C" void InternalAsyncResult_SetComplete_m4433 ();
extern "C" void InternalAsyncResult_SetComplete_m4434 ();
extern "C" void SslStreamBase__ctor_m4435 ();
extern "C" void SslStreamBase__cctor_m4436 ();
extern "C" void SslStreamBase_AsyncHandshakeCallback_m4437 ();
extern "C" void SslStreamBase_get_MightNeedHandshake_m4438 ();
extern "C" void SslStreamBase_NegotiateHandshake_m4439 ();
extern "C" void SslStreamBase_RaiseLocalCertificateSelection_m4440 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation_m4441 ();
extern "C" void SslStreamBase_RaiseRemoteCertificateValidation2_m4442 ();
extern "C" void SslStreamBase_RaiseLocalPrivateKeySelection_m4443 ();
extern "C" void SslStreamBase_get_CheckCertRevocationStatus_m4444 ();
extern "C" void SslStreamBase_set_CheckCertRevocationStatus_m4445 ();
extern "C" void SslStreamBase_get_CipherAlgorithm_m4446 ();
extern "C" void SslStreamBase_get_CipherStrength_m4447 ();
extern "C" void SslStreamBase_get_HashAlgorithm_m4448 ();
extern "C" void SslStreamBase_get_HashStrength_m4449 ();
extern "C" void SslStreamBase_get_KeyExchangeStrength_m4450 ();
extern "C" void SslStreamBase_get_KeyExchangeAlgorithm_m4451 ();
extern "C" void SslStreamBase_get_SecurityProtocol_m4452 ();
extern "C" void SslStreamBase_get_ServerCertificate_m4453 ();
extern "C" void SslStreamBase_get_ServerCertificates_m4454 ();
extern "C" void SslStreamBase_BeginNegotiateHandshake_m4455 ();
extern "C" void SslStreamBase_EndNegotiateHandshake_m4456 ();
extern "C" void SslStreamBase_BeginRead_m4457 ();
extern "C" void SslStreamBase_InternalBeginRead_m4458 ();
extern "C" void SslStreamBase_InternalReadCallback_m4459 ();
extern "C" void SslStreamBase_InternalBeginWrite_m4460 ();
extern "C" void SslStreamBase_InternalWriteCallback_m4461 ();
extern "C" void SslStreamBase_BeginWrite_m4462 ();
extern "C" void SslStreamBase_EndRead_m4463 ();
extern "C" void SslStreamBase_EndWrite_m4464 ();
extern "C" void SslStreamBase_Close_m4465 ();
extern "C" void SslStreamBase_Flush_m4466 ();
extern "C" void SslStreamBase_Read_m4467 ();
extern "C" void SslStreamBase_Read_m4468 ();
extern "C" void SslStreamBase_Seek_m4469 ();
extern "C" void SslStreamBase_SetLength_m4470 ();
extern "C" void SslStreamBase_Write_m4471 ();
extern "C" void SslStreamBase_Write_m4472 ();
extern "C" void SslStreamBase_get_CanRead_m4473 ();
extern "C" void SslStreamBase_get_CanSeek_m4474 ();
extern "C" void SslStreamBase_get_CanWrite_m4475 ();
extern "C" void SslStreamBase_get_Length_m4476 ();
extern "C" void SslStreamBase_get_Position_m4477 ();
extern "C" void SslStreamBase_set_Position_m4478 ();
extern "C" void SslStreamBase_Finalize_m4479 ();
extern "C" void SslStreamBase_Dispose_m4480 ();
extern "C" void SslStreamBase_resetBuffer_m4481 ();
extern "C" void SslStreamBase_checkDisposed_m4482 ();
extern "C" void TlsCipherSuite__ctor_m4483 ();
extern "C" void TlsCipherSuite_ComputeServerRecordMAC_m4484 ();
extern "C" void TlsCipherSuite_ComputeClientRecordMAC_m4485 ();
extern "C" void TlsCipherSuite_ComputeMasterSecret_m4486 ();
extern "C" void TlsCipherSuite_ComputeKeys_m4487 ();
extern "C" void TlsClientSettings__ctor_m4488 ();
extern "C" void TlsClientSettings_get_TargetHost_m4489 ();
extern "C" void TlsClientSettings_set_TargetHost_m4490 ();
extern "C" void TlsClientSettings_get_Certificates_m4491 ();
extern "C" void TlsClientSettings_set_Certificates_m4492 ();
extern "C" void TlsClientSettings_get_ClientCertificate_m4493 ();
extern "C" void TlsClientSettings_set_ClientCertificate_m4494 ();
extern "C" void TlsClientSettings_UpdateCertificateRSA_m4495 ();
extern "C" void TlsException__ctor_m4496 ();
extern "C" void TlsException__ctor_m4497 ();
extern "C" void TlsException__ctor_m4498 ();
extern "C" void TlsException__ctor_m4499 ();
extern "C" void TlsException__ctor_m4500 ();
extern "C" void TlsException__ctor_m4501 ();
extern "C" void TlsException_get_Alert_m4502 ();
extern "C" void TlsServerSettings__ctor_m4503 ();
extern "C" void TlsServerSettings_get_ServerKeyExchange_m4504 ();
extern "C" void TlsServerSettings_set_ServerKeyExchange_m4505 ();
extern "C" void TlsServerSettings_get_Certificates_m4506 ();
extern "C" void TlsServerSettings_set_Certificates_m4507 ();
extern "C" void TlsServerSettings_get_CertificateRSA_m4508 ();
extern "C" void TlsServerSettings_get_RsaParameters_m4509 ();
extern "C" void TlsServerSettings_set_RsaParameters_m4510 ();
extern "C" void TlsServerSettings_set_SignedParams_m4511 ();
extern "C" void TlsServerSettings_get_CertificateRequest_m4512 ();
extern "C" void TlsServerSettings_set_CertificateRequest_m4513 ();
extern "C" void TlsServerSettings_set_CertificateTypes_m4514 ();
extern "C" void TlsServerSettings_set_DistinguisedNames_m4515 ();
extern "C" void TlsServerSettings_UpdateCertificateRSA_m4516 ();
extern "C" void TlsStream__ctor_m4517 ();
extern "C" void TlsStream__ctor_m4518 ();
extern "C" void TlsStream_get_EOF_m4519 ();
extern "C" void TlsStream_get_CanWrite_m4520 ();
extern "C" void TlsStream_get_CanRead_m4521 ();
extern "C" void TlsStream_get_CanSeek_m4522 ();
extern "C" void TlsStream_get_Position_m4523 ();
extern "C" void TlsStream_set_Position_m4524 ();
extern "C" void TlsStream_get_Length_m4525 ();
extern "C" void TlsStream_ReadSmallValue_m4526 ();
extern "C" void TlsStream_ReadByte_m4527 ();
extern "C" void TlsStream_ReadInt16_m4528 ();
extern "C" void TlsStream_ReadInt24_m4529 ();
extern "C" void TlsStream_ReadBytes_m4530 ();
extern "C" void TlsStream_Write_m4531 ();
extern "C" void TlsStream_Write_m4532 ();
extern "C" void TlsStream_WriteInt24_m4533 ();
extern "C" void TlsStream_Write_m4534 ();
extern "C" void TlsStream_Write_m4535 ();
extern "C" void TlsStream_Reset_m4536 ();
extern "C" void TlsStream_ToArray_m4537 ();
extern "C" void TlsStream_Flush_m4538 ();
extern "C" void TlsStream_SetLength_m4539 ();
extern "C" void TlsStream_Seek_m4540 ();
extern "C" void TlsStream_Read_m4541 ();
extern "C" void TlsStream_Write_m4542 ();
extern "C" void HandshakeMessage__ctor_m4543 ();
extern "C" void HandshakeMessage__ctor_m4544 ();
extern "C" void HandshakeMessage__ctor_m4545 ();
extern "C" void HandshakeMessage_get_Context_m4546 ();
extern "C" void HandshakeMessage_get_HandshakeType_m4547 ();
extern "C" void HandshakeMessage_get_ContentType_m4548 ();
extern "C" void HandshakeMessage_Process_m4549 ();
extern "C" void HandshakeMessage_Update_m4550 ();
extern "C" void HandshakeMessage_EncodeMessage_m4551 ();
extern "C" void HandshakeMessage_Compare_m4552 ();
extern "C" void TlsClientCertificate__ctor_m4553 ();
extern "C" void TlsClientCertificate_get_ClientCertificate_m4554 ();
extern "C" void TlsClientCertificate_Update_m4555 ();
extern "C" void TlsClientCertificate_GetClientCertificate_m4556 ();
extern "C" void TlsClientCertificate_SendCertificates_m4557 ();
extern "C" void TlsClientCertificate_ProcessAsSsl3_m4558 ();
extern "C" void TlsClientCertificate_ProcessAsTls1_m4559 ();
extern "C" void TlsClientCertificate_FindParentCertificate_m4560 ();
extern "C" void TlsClientCertificateVerify__ctor_m4561 ();
extern "C" void TlsClientCertificateVerify_Update_m4562 ();
extern "C" void TlsClientCertificateVerify_ProcessAsSsl3_m4563 ();
extern "C" void TlsClientCertificateVerify_ProcessAsTls1_m4564 ();
extern "C" void TlsClientCertificateVerify_getClientCertRSA_m4565 ();
extern "C" void TlsClientCertificateVerify_getUnsignedBigInteger_m4566 ();
extern "C" void TlsClientFinished__ctor_m4567 ();
extern "C" void TlsClientFinished__cctor_m4568 ();
extern "C" void TlsClientFinished_Update_m4569 ();
extern "C" void TlsClientFinished_ProcessAsSsl3_m4570 ();
extern "C" void TlsClientFinished_ProcessAsTls1_m4571 ();
extern "C" void TlsClientHello__ctor_m4572 ();
extern "C" void TlsClientHello_Update_m4573 ();
extern "C" void TlsClientHello_ProcessAsSsl3_m4574 ();
extern "C" void TlsClientHello_ProcessAsTls1_m4575 ();
extern "C" void TlsClientKeyExchange__ctor_m4576 ();
extern "C" void TlsClientKeyExchange_ProcessAsSsl3_m4577 ();
extern "C" void TlsClientKeyExchange_ProcessAsTls1_m4578 ();
extern "C" void TlsClientKeyExchange_ProcessCommon_m4579 ();
extern "C" void TlsServerCertificate__ctor_m4580 ();
extern "C" void TlsServerCertificate_Update_m4581 ();
extern "C" void TlsServerCertificate_ProcessAsSsl3_m4582 ();
extern "C" void TlsServerCertificate_ProcessAsTls1_m4583 ();
extern "C" void TlsServerCertificate_checkCertificateUsage_m4584 ();
extern "C" void TlsServerCertificate_validateCertificates_m4585 ();
extern "C" void TlsServerCertificate_checkServerIdentity_m4586 ();
extern "C" void TlsServerCertificate_checkDomainName_m4587 ();
extern "C" void TlsServerCertificate_Match_m4588 ();
extern "C" void TlsServerCertificateRequest__ctor_m4589 ();
extern "C" void TlsServerCertificateRequest_Update_m4590 ();
extern "C" void TlsServerCertificateRequest_ProcessAsSsl3_m4591 ();
extern "C" void TlsServerCertificateRequest_ProcessAsTls1_m4592 ();
extern "C" void TlsServerFinished__ctor_m4593 ();
extern "C" void TlsServerFinished__cctor_m4594 ();
extern "C" void TlsServerFinished_Update_m4595 ();
extern "C" void TlsServerFinished_ProcessAsSsl3_m4596 ();
extern "C" void TlsServerFinished_ProcessAsTls1_m4597 ();
extern "C" void TlsServerHello__ctor_m4598 ();
extern "C" void TlsServerHello_Update_m4599 ();
extern "C" void TlsServerHello_ProcessAsSsl3_m4600 ();
extern "C" void TlsServerHello_ProcessAsTls1_m4601 ();
extern "C" void TlsServerHello_processProtocol_m4602 ();
extern "C" void TlsServerHelloDone__ctor_m4603 ();
extern "C" void TlsServerHelloDone_ProcessAsSsl3_m4604 ();
extern "C" void TlsServerHelloDone_ProcessAsTls1_m4605 ();
extern "C" void TlsServerKeyExchange__ctor_m4606 ();
extern "C" void TlsServerKeyExchange_Update_m4607 ();
extern "C" void TlsServerKeyExchange_ProcessAsSsl3_m4608 ();
extern "C" void TlsServerKeyExchange_ProcessAsTls1_m4609 ();
extern "C" void TlsServerKeyExchange_verifySignature_m4610 ();
extern "C" void PrimalityTest__ctor_m4611 ();
extern "C" void PrimalityTest_Invoke_m4612 ();
extern "C" void PrimalityTest_BeginInvoke_m4613 ();
extern "C" void PrimalityTest_EndInvoke_m4614 ();
extern "C" void CertificateValidationCallback__ctor_m4615 ();
extern "C" void CertificateValidationCallback_Invoke_m4616 ();
extern "C" void CertificateValidationCallback_BeginInvoke_m4617 ();
extern "C" void CertificateValidationCallback_EndInvoke_m4618 ();
extern "C" void CertificateValidationCallback2__ctor_m4619 ();
extern "C" void CertificateValidationCallback2_Invoke_m4620 ();
extern "C" void CertificateValidationCallback2_BeginInvoke_m4621 ();
extern "C" void CertificateValidationCallback2_EndInvoke_m4622 ();
extern "C" void CertificateSelectionCallback__ctor_m4623 ();
extern "C" void CertificateSelectionCallback_Invoke_m4624 ();
extern "C" void CertificateSelectionCallback_BeginInvoke_m4625 ();
extern "C" void CertificateSelectionCallback_EndInvoke_m4626 ();
extern "C" void PrivateKeySelectionCallback__ctor_m4627 ();
extern "C" void PrivateKeySelectionCallback_Invoke_m4628 ();
extern "C" void PrivateKeySelectionCallback_BeginInvoke_m4629 ();
extern "C" void PrivateKeySelectionCallback_EndInvoke_m4630 ();
extern "C" void Locale_GetText_m4765 ();
extern "C" void Locale_GetText_m4766 ();
extern "C" void MonoTODOAttribute__ctor_m4767 ();
extern "C" void MonoTODOAttribute__ctor_m4768 ();
extern "C" void HybridDictionary__ctor_m4769 ();
extern "C" void HybridDictionary__ctor_m4770 ();
extern "C" void HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m4771 ();
extern "C" void HybridDictionary_get_inner_m4772 ();
extern "C" void HybridDictionary_get_Count_m4773 ();
extern "C" void HybridDictionary_get_IsSynchronized_m4774 ();
extern "C" void HybridDictionary_get_Item_m4775 ();
extern "C" void HybridDictionary_set_Item_m4776 ();
extern "C" void HybridDictionary_get_SyncRoot_m4777 ();
extern "C" void HybridDictionary_Add_m4778 ();
extern "C" void HybridDictionary_Contains_m4779 ();
extern "C" void HybridDictionary_CopyTo_m4780 ();
extern "C" void HybridDictionary_GetEnumerator_m4781 ();
extern "C" void HybridDictionary_Remove_m4782 ();
extern "C" void HybridDictionary_Switch_m4783 ();
extern "C" void DictionaryNode__ctor_m4784 ();
extern "C" void DictionaryNodeEnumerator__ctor_m4785 ();
extern "C" void DictionaryNodeEnumerator_FailFast_m4786 ();
extern "C" void DictionaryNodeEnumerator_MoveNext_m4787 ();
extern "C" void DictionaryNodeEnumerator_Reset_m4788 ();
extern "C" void DictionaryNodeEnumerator_get_Current_m4789 ();
extern "C" void DictionaryNodeEnumerator_get_DictionaryNode_m4790 ();
extern "C" void DictionaryNodeEnumerator_get_Entry_m4791 ();
extern "C" void DictionaryNodeEnumerator_get_Key_m4792 ();
extern "C" void DictionaryNodeEnumerator_get_Value_m4793 ();
extern "C" void ListDictionary__ctor_m4794 ();
extern "C" void ListDictionary__ctor_m4795 ();
extern "C" void ListDictionary_System_Collections_IEnumerable_GetEnumerator_m4796 ();
extern "C" void ListDictionary_FindEntry_m4797 ();
extern "C" void ListDictionary_FindEntry_m4798 ();
extern "C" void ListDictionary_AddImpl_m4799 ();
extern "C" void ListDictionary_get_Count_m4800 ();
extern "C" void ListDictionary_get_IsSynchronized_m4801 ();
extern "C" void ListDictionary_get_SyncRoot_m4802 ();
extern "C" void ListDictionary_CopyTo_m4803 ();
extern "C" void ListDictionary_get_Item_m4804 ();
extern "C" void ListDictionary_set_Item_m4805 ();
extern "C" void ListDictionary_Add_m4806 ();
extern "C" void ListDictionary_Clear_m4807 ();
extern "C" void ListDictionary_Contains_m4808 ();
extern "C" void ListDictionary_GetEnumerator_m4809 ();
extern "C" void ListDictionary_Remove_m4810 ();
extern "C" void _Item__ctor_m4811 ();
extern "C" void _KeysEnumerator__ctor_m4812 ();
extern "C" void _KeysEnumerator_get_Current_m4813 ();
extern "C" void _KeysEnumerator_MoveNext_m4814 ();
extern "C" void _KeysEnumerator_Reset_m4815 ();
extern "C" void KeysCollection__ctor_m4816 ();
extern "C" void KeysCollection_System_Collections_ICollection_CopyTo_m4817 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_IsSynchronized_m4818 ();
extern "C" void KeysCollection_System_Collections_ICollection_get_SyncRoot_m4819 ();
extern "C" void KeysCollection_get_Count_m4820 ();
extern "C" void KeysCollection_GetEnumerator_m4821 ();
extern "C" void NameObjectCollectionBase__ctor_m4822 ();
extern "C" void NameObjectCollectionBase__ctor_m4823 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m4824 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m4825 ();
extern "C" void NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m4826 ();
extern "C" void NameObjectCollectionBase_Init_m4827 ();
extern "C" void NameObjectCollectionBase_get_Keys_m4828 ();
extern "C" void NameObjectCollectionBase_GetEnumerator_m4829 ();
extern "C" void NameObjectCollectionBase_GetObjectData_m4830 ();
extern "C" void NameObjectCollectionBase_get_Count_m4831 ();
extern "C" void NameObjectCollectionBase_OnDeserialization_m4832 ();
extern "C" void NameObjectCollectionBase_get_IsReadOnly_m4833 ();
extern "C" void NameObjectCollectionBase_BaseAdd_m4834 ();
extern "C" void NameObjectCollectionBase_BaseGet_m4835 ();
extern "C" void NameObjectCollectionBase_BaseGet_m4836 ();
extern "C" void NameObjectCollectionBase_BaseGetKey_m4837 ();
extern "C" void NameObjectCollectionBase_FindFirstMatchedItem_m4838 ();
extern "C" void NameValueCollection__ctor_m4839 ();
extern "C" void NameValueCollection__ctor_m4840 ();
extern "C" void NameValueCollection_Add_m4841 ();
extern "C" void NameValueCollection_Get_m4842 ();
extern "C" void NameValueCollection_AsSingleString_m4843 ();
extern "C" void NameValueCollection_GetKey_m4844 ();
extern "C" void NameValueCollection_InvalidateCachedArrays_m4845 ();
extern "C" void EditorBrowsableAttribute__ctor_m4846 ();
extern "C" void EditorBrowsableAttribute_get_State_m4847 ();
extern "C" void EditorBrowsableAttribute_Equals_m4848 ();
extern "C" void EditorBrowsableAttribute_GetHashCode_m4849 ();
extern "C" void TypeConverterAttribute__ctor_m4850 ();
extern "C" void TypeConverterAttribute__ctor_m4851 ();
extern "C" void TypeConverterAttribute__cctor_m4852 ();
extern "C" void TypeConverterAttribute_Equals_m4853 ();
extern "C" void TypeConverterAttribute_GetHashCode_m4854 ();
extern "C" void TypeConverterAttribute_get_ConverterTypeName_m4855 ();
extern "C" void DefaultCertificatePolicy__ctor_m4856 ();
extern "C" void DefaultCertificatePolicy_CheckValidationResult_m4857 ();
extern "C" void FileWebRequest__ctor_m4858 ();
extern "C" void FileWebRequest__ctor_m4859 ();
extern "C" void FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4860 ();
extern "C" void FileWebRequest_GetObjectData_m4861 ();
extern "C" void FileWebRequestCreator__ctor_m4862 ();
extern "C" void FileWebRequestCreator_Create_m4863 ();
extern "C" void FtpRequestCreator__ctor_m4864 ();
extern "C" void FtpRequestCreator_Create_m4865 ();
extern "C" void FtpWebRequest__ctor_m4866 ();
extern "C" void FtpWebRequest__cctor_m4867 ();
extern "C" void FtpWebRequest_U3CcallbackU3Em__B_m4868 ();
extern "C" void GlobalProxySelection_get_Select_m4869 ();
extern "C" void HttpRequestCreator__ctor_m4870 ();
extern "C" void HttpRequestCreator_Create_m4871 ();
extern "C" void HttpVersion__cctor_m4872 ();
extern "C" void HttpWebRequest__ctor_m4873 ();
extern "C" void HttpWebRequest__ctor_m4874 ();
extern "C" void HttpWebRequest__cctor_m4875 ();
extern "C" void HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4876 ();
extern "C" void HttpWebRequest_get_Address_m4723 ();
extern "C" void HttpWebRequest_get_ServicePoint_m4727 ();
extern "C" void HttpWebRequest_GetServicePoint_m4877 ();
extern "C" void HttpWebRequest_GetObjectData_m4878 ();
extern "C" void IPAddress__ctor_m4879 ();
extern "C" void IPAddress__ctor_m4880 ();
extern "C" void IPAddress__cctor_m4881 ();
extern "C" void IPAddress_SwapShort_m4882 ();
extern "C" void IPAddress_HostToNetworkOrder_m4883 ();
extern "C" void IPAddress_NetworkToHostOrder_m4884 ();
extern "C" void IPAddress_Parse_m4885 ();
extern "C" void IPAddress_TryParse_m4886 ();
extern "C" void IPAddress_ParseIPV4_m4887 ();
extern "C" void IPAddress_ParseIPV6_m4888 ();
extern "C" void IPAddress_get_InternalIPv4Address_m4889 ();
extern "C" void IPAddress_get_ScopeId_m4890 ();
extern "C" void IPAddress_get_AddressFamily_m4891 ();
extern "C" void IPAddress_IsLoopback_m4892 ();
extern "C" void IPAddress_ToString_m4893 ();
extern "C" void IPAddress_ToString_m4894 ();
extern "C" void IPAddress_Equals_m4895 ();
extern "C" void IPAddress_GetHashCode_m4896 ();
extern "C" void IPAddress_Hash_m4897 ();
extern "C" void IPv6Address__ctor_m4898 ();
extern "C" void IPv6Address__ctor_m4899 ();
extern "C" void IPv6Address__ctor_m4900 ();
extern "C" void IPv6Address__cctor_m4901 ();
extern "C" void IPv6Address_Parse_m4902 ();
extern "C" void IPv6Address_Fill_m4903 ();
extern "C" void IPv6Address_TryParse_m4904 ();
extern "C" void IPv6Address_TryParse_m4905 ();
extern "C" void IPv6Address_get_Address_m4906 ();
extern "C" void IPv6Address_get_ScopeId_m4907 ();
extern "C" void IPv6Address_set_ScopeId_m4908 ();
extern "C" void IPv6Address_IsLoopback_m4909 ();
extern "C" void IPv6Address_SwapUShort_m4910 ();
extern "C" void IPv6Address_AsIPv4Int_m4911 ();
extern "C" void IPv6Address_IsIPv4Compatible_m4912 ();
extern "C" void IPv6Address_IsIPv4Mapped_m4913 ();
extern "C" void IPv6Address_ToString_m4914 ();
extern "C" void IPv6Address_ToString_m4915 ();
extern "C" void IPv6Address_Equals_m4916 ();
extern "C" void IPv6Address_GetHashCode_m4917 ();
extern "C" void IPv6Address_Hash_m4918 ();
extern "C" void ServicePoint__ctor_m4919 ();
extern "C" void ServicePoint_get_Address_m4920 ();
extern "C" void ServicePoint_get_CurrentConnections_m4921 ();
extern "C" void ServicePoint_get_IdleSince_m4922 ();
extern "C" void ServicePoint_set_IdleSince_m4923 ();
extern "C" void ServicePoint_set_Expect100Continue_m4924 ();
extern "C" void ServicePoint_set_UseNagleAlgorithm_m4925 ();
extern "C" void ServicePoint_set_SendContinue_m4926 ();
extern "C" void ServicePoint_set_UsesProxy_m4927 ();
extern "C" void ServicePoint_set_UseConnect_m4928 ();
extern "C" void ServicePoint_get_AvailableForRecycling_m4929 ();
extern "C" void SPKey__ctor_m4930 ();
extern "C" void SPKey_GetHashCode_m4931 ();
extern "C" void SPKey_Equals_m4932 ();
extern "C" void ServicePointManager__cctor_m4933 ();
extern "C" void ServicePointManager_get_CertificatePolicy_m4726 ();
extern "C" void ServicePointManager_get_CheckCertificateRevocationList_m4692 ();
extern "C" void ServicePointManager_get_SecurityProtocol_m4725 ();
extern "C" void ServicePointManager_get_ServerCertificateValidationCallback_m4728 ();
extern "C" void ServicePointManager_FindServicePoint_m4934 ();
extern "C" void ServicePointManager_RecycleServicePoints_m4935 ();
extern "C" void WebHeaderCollection__ctor_m4936 ();
extern "C" void WebHeaderCollection__ctor_m4937 ();
extern "C" void WebHeaderCollection__ctor_m4938 ();
extern "C" void WebHeaderCollection__cctor_m4939 ();
extern "C" void WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m4940 ();
extern "C" void WebHeaderCollection_Add_m4941 ();
extern "C" void WebHeaderCollection_AddWithoutValidate_m4942 ();
extern "C" void WebHeaderCollection_IsRestricted_m4943 ();
extern "C" void WebHeaderCollection_OnDeserialization_m4944 ();
extern "C" void WebHeaderCollection_ToString_m4945 ();
extern "C" void WebHeaderCollection_GetObjectData_m4946 ();
extern "C" void WebHeaderCollection_get_Count_m4947 ();
extern "C" void WebHeaderCollection_get_Keys_m4948 ();
extern "C" void WebHeaderCollection_Get_m4949 ();
extern "C" void WebHeaderCollection_GetKey_m4950 ();
extern "C" void WebHeaderCollection_GetEnumerator_m4951 ();
extern "C" void WebHeaderCollection_IsHeaderValue_m4952 ();
extern "C" void WebHeaderCollection_IsHeaderName_m4953 ();
extern "C" void WebProxy__ctor_m4954 ();
extern "C" void WebProxy__ctor_m4955 ();
extern "C" void WebProxy__ctor_m4956 ();
extern "C" void WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m4957 ();
extern "C" void WebProxy_get_UseDefaultCredentials_m4958 ();
extern "C" void WebProxy_GetProxy_m4959 ();
extern "C" void WebProxy_IsBypassed_m4960 ();
extern "C" void WebProxy_GetObjectData_m4961 ();
extern "C" void WebProxy_CheckBypassList_m4962 ();
extern "C" void WebRequest__ctor_m4963 ();
extern "C" void WebRequest__ctor_m4964 ();
extern "C" void WebRequest__cctor_m4965 ();
extern "C" void WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4966 ();
extern "C" void WebRequest_AddDynamicPrefix_m4967 ();
extern "C" void WebRequest_GetMustImplement_m4968 ();
extern "C" void WebRequest_get_DefaultWebProxy_m4969 ();
extern "C" void WebRequest_GetDefaultWebProxy_m4970 ();
extern "C" void WebRequest_GetObjectData_m4971 ();
extern "C" void WebRequest_AddPrefix_m4972 ();
extern "C" void PublicKey__ctor_m4973 ();
extern "C" void PublicKey_get_EncodedKeyValue_m4974 ();
extern "C" void PublicKey_get_EncodedParameters_m4975 ();
extern "C" void PublicKey_get_Key_m4976 ();
extern "C" void PublicKey_get_Oid_m4977 ();
extern "C" void PublicKey_GetUnsignedBigInteger_m4978 ();
extern "C" void PublicKey_DecodeDSA_m4979 ();
extern "C" void PublicKey_DecodeRSA_m4980 ();
extern "C" void X500DistinguishedName__ctor_m4981 ();
extern "C" void X500DistinguishedName_Decode_m4982 ();
extern "C" void X500DistinguishedName_GetSeparator_m4983 ();
extern "C" void X500DistinguishedName_DecodeRawData_m4984 ();
extern "C" void X500DistinguishedName_Canonize_m4985 ();
extern "C" void X500DistinguishedName_AreEqual_m4986 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4987 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4988 ();
extern "C" void X509BasicConstraintsExtension__ctor_m4989 ();
extern "C" void X509BasicConstraintsExtension_get_CertificateAuthority_m4990 ();
extern "C" void X509BasicConstraintsExtension_get_HasPathLengthConstraint_m4991 ();
extern "C" void X509BasicConstraintsExtension_get_PathLengthConstraint_m4992 ();
extern "C" void X509BasicConstraintsExtension_CopyFrom_m4993 ();
extern "C" void X509BasicConstraintsExtension_Decode_m4994 ();
extern "C" void X509BasicConstraintsExtension_Encode_m4995 ();
extern "C" void X509BasicConstraintsExtension_ToString_m4996 ();
extern "C" void X509Certificate2__ctor_m4729 ();
extern "C" void X509Certificate2__cctor_m4997 ();
extern "C" void X509Certificate2_get_Extensions_m4998 ();
extern "C" void X509Certificate2_get_IssuerName_m4999 ();
extern "C" void X509Certificate2_get_NotAfter_m5000 ();
extern "C" void X509Certificate2_get_NotBefore_m5001 ();
extern "C" void X509Certificate2_get_PrivateKey_m4734 ();
extern "C" void X509Certificate2_get_PublicKey_m5002 ();
extern "C" void X509Certificate2_get_SerialNumber_m5003 ();
extern "C" void X509Certificate2_get_SignatureAlgorithm_m5004 ();
extern "C" void X509Certificate2_get_SubjectName_m5005 ();
extern "C" void X509Certificate2_get_Thumbprint_m5006 ();
extern "C" void X509Certificate2_get_Version_m5007 ();
extern "C" void X509Certificate2_GetNameInfo_m5008 ();
extern "C" void X509Certificate2_Find_m5009 ();
extern "C" void X509Certificate2_GetValueAsString_m5010 ();
extern "C" void X509Certificate2_ImportPkcs12_m5011 ();
extern "C" void X509Certificate2_Import_m5012 ();
extern "C" void X509Certificate2_Reset_m5013 ();
extern "C" void X509Certificate2_ToString_m5014 ();
extern "C" void X509Certificate2_ToString_m5015 ();
extern "C" void X509Certificate2_AppendBuffer_m5016 ();
extern "C" void X509Certificate2_Verify_m5017 ();
extern "C" void X509Certificate2_get_MonoCertificate_m5018 ();
extern "C" void X509Certificate2Collection__ctor_m5019 ();
extern "C" void X509Certificate2Collection__ctor_m5020 ();
extern "C" void X509Certificate2Collection_get_Item_m5021 ();
extern "C" void X509Certificate2Collection_Add_m5022 ();
extern "C" void X509Certificate2Collection_AddRange_m5023 ();
extern "C" void X509Certificate2Collection_Contains_m5024 ();
extern "C" void X509Certificate2Collection_Find_m5025 ();
extern "C" void X509Certificate2Collection_GetEnumerator_m5026 ();
extern "C" void X509Certificate2Enumerator__ctor_m5027 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m5028 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m5029 ();
extern "C" void X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m5030 ();
extern "C" void X509Certificate2Enumerator_get_Current_m5031 ();
extern "C" void X509Certificate2Enumerator_MoveNext_m5032 ();
extern "C" void X509Certificate2Enumerator_Reset_m5033 ();
extern "C" void X509CertificateEnumerator__ctor_m5034 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m5035 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m5036 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m5037 ();
extern "C" void X509CertificateEnumerator_get_Current_m4754 ();
extern "C" void X509CertificateEnumerator_MoveNext_m5038 ();
extern "C" void X509CertificateEnumerator_Reset_m5039 ();
extern "C" void X509CertificateCollection__ctor_m4744 ();
extern "C" void X509CertificateCollection__ctor_m4743 ();
extern "C" void X509CertificateCollection_get_Item_m4733 ();
extern "C" void X509CertificateCollection_AddRange_m5040 ();
extern "C" void X509CertificateCollection_GetEnumerator_m4753 ();
extern "C" void X509CertificateCollection_GetHashCode_m5041 ();
extern "C" void X509Chain__ctor_m4730 ();
extern "C" void X509Chain__ctor_m5042 ();
extern "C" void X509Chain__cctor_m5043 ();
extern "C" void X509Chain_get_ChainPolicy_m5044 ();
extern "C" void X509Chain_Build_m4731 ();
extern "C" void X509Chain_Reset_m5045 ();
extern "C" void X509Chain_get_Roots_m5046 ();
extern "C" void X509Chain_get_CertificateAuthorities_m5047 ();
extern "C" void X509Chain_get_CertificateCollection_m5048 ();
extern "C" void X509Chain_BuildChainFrom_m5049 ();
extern "C" void X509Chain_SelectBestFromCollection_m5050 ();
extern "C" void X509Chain_FindParent_m5051 ();
extern "C" void X509Chain_IsChainComplete_m5052 ();
extern "C" void X509Chain_IsSelfIssued_m5053 ();
extern "C" void X509Chain_ValidateChain_m5054 ();
extern "C" void X509Chain_Process_m5055 ();
extern "C" void X509Chain_PrepareForNextCertificate_m5056 ();
extern "C" void X509Chain_WrapUp_m5057 ();
extern "C" void X509Chain_ProcessCertificateExtensions_m5058 ();
extern "C" void X509Chain_IsSignedWith_m5059 ();
extern "C" void X509Chain_GetSubjectKeyIdentifier_m5060 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m5061 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m5062 ();
extern "C" void X509Chain_GetAuthorityKeyIdentifier_m5063 ();
extern "C" void X509Chain_CheckRevocationOnChain_m5064 ();
extern "C" void X509Chain_CheckRevocation_m5065 ();
extern "C" void X509Chain_CheckRevocation_m5066 ();
extern "C" void X509Chain_FindCrl_m5067 ();
extern "C" void X509Chain_ProcessCrlExtensions_m5068 ();
extern "C" void X509Chain_ProcessCrlEntryExtensions_m5069 ();
extern "C" void X509ChainElement__ctor_m5070 ();
extern "C" void X509ChainElement_get_Certificate_m5071 ();
extern "C" void X509ChainElement_get_ChainElementStatus_m5072 ();
extern "C" void X509ChainElement_get_StatusFlags_m5073 ();
extern "C" void X509ChainElement_set_StatusFlags_m5074 ();
extern "C" void X509ChainElement_Count_m5075 ();
extern "C" void X509ChainElement_Set_m5076 ();
extern "C" void X509ChainElement_UncompressFlags_m5077 ();
extern "C" void X509ChainElementCollection__ctor_m5078 ();
extern "C" void X509ChainElementCollection_System_Collections_ICollection_CopyTo_m5079 ();
extern "C" void X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m5080 ();
extern "C" void X509ChainElementCollection_get_Count_m5081 ();
extern "C" void X509ChainElementCollection_get_IsSynchronized_m5082 ();
extern "C" void X509ChainElementCollection_get_Item_m5083 ();
extern "C" void X509ChainElementCollection_get_SyncRoot_m5084 ();
extern "C" void X509ChainElementCollection_GetEnumerator_m5085 ();
extern "C" void X509ChainElementCollection_Add_m5086 ();
extern "C" void X509ChainElementCollection_Clear_m5087 ();
extern "C" void X509ChainElementCollection_Contains_m5088 ();
extern "C" void X509ChainElementEnumerator__ctor_m5089 ();
extern "C" void X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m5090 ();
extern "C" void X509ChainElementEnumerator_get_Current_m5091 ();
extern "C" void X509ChainElementEnumerator_MoveNext_m5092 ();
extern "C" void X509ChainElementEnumerator_Reset_m5093 ();
extern "C" void X509ChainPolicy__ctor_m5094 ();
extern "C" void X509ChainPolicy_get_ExtraStore_m5095 ();
extern "C" void X509ChainPolicy_get_RevocationFlag_m5096 ();
extern "C" void X509ChainPolicy_get_RevocationMode_m5097 ();
extern "C" void X509ChainPolicy_get_VerificationFlags_m5098 ();
extern "C" void X509ChainPolicy_get_VerificationTime_m5099 ();
extern "C" void X509ChainPolicy_Reset_m5100 ();
extern "C" void X509ChainStatus__ctor_m5101 ();
extern "C" void X509ChainStatus_get_Status_m5102 ();
extern "C" void X509ChainStatus_set_Status_m5103 ();
extern "C" void X509ChainStatus_set_StatusInformation_m5104 ();
extern "C" void X509ChainStatus_GetInformation_m5105 ();
extern "C" void X509EnhancedKeyUsageExtension__ctor_m5106 ();
extern "C" void X509EnhancedKeyUsageExtension_CopyFrom_m5107 ();
extern "C" void X509EnhancedKeyUsageExtension_Decode_m5108 ();
extern "C" void X509EnhancedKeyUsageExtension_ToString_m5109 ();
extern "C" void X509Extension__ctor_m5110 ();
extern "C" void X509Extension__ctor_m5111 ();
extern "C" void X509Extension_get_Critical_m5112 ();
extern "C" void X509Extension_set_Critical_m5113 ();
extern "C" void X509Extension_CopyFrom_m5114 ();
extern "C" void X509Extension_FormatUnkownData_m5115 ();
extern "C" void X509ExtensionCollection__ctor_m5116 ();
extern "C" void X509ExtensionCollection_System_Collections_ICollection_CopyTo_m5117 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m5118 ();
extern "C" void X509ExtensionCollection_get_Count_m5119 ();
extern "C" void X509ExtensionCollection_get_IsSynchronized_m5120 ();
extern "C" void X509ExtensionCollection_get_SyncRoot_m5121 ();
extern "C" void X509ExtensionCollection_get_Item_m5122 ();
extern "C" void X509ExtensionCollection_GetEnumerator_m5123 ();
extern "C" void X509ExtensionEnumerator__ctor_m5124 ();
extern "C" void X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m5125 ();
extern "C" void X509ExtensionEnumerator_get_Current_m5126 ();
extern "C" void X509ExtensionEnumerator_MoveNext_m5127 ();
extern "C" void X509ExtensionEnumerator_Reset_m5128 ();
extern "C" void X509KeyUsageExtension__ctor_m5129 ();
extern "C" void X509KeyUsageExtension__ctor_m5130 ();
extern "C" void X509KeyUsageExtension__ctor_m5131 ();
extern "C" void X509KeyUsageExtension_get_KeyUsages_m5132 ();
extern "C" void X509KeyUsageExtension_CopyFrom_m5133 ();
extern "C" void X509KeyUsageExtension_GetValidFlags_m5134 ();
extern "C" void X509KeyUsageExtension_Decode_m5135 ();
extern "C" void X509KeyUsageExtension_Encode_m5136 ();
extern "C" void X509KeyUsageExtension_ToString_m5137 ();
extern "C" void X509Store__ctor_m5138 ();
extern "C" void X509Store_get_Certificates_m5139 ();
extern "C" void X509Store_get_Factory_m5140 ();
extern "C" void X509Store_get_Store_m5141 ();
extern "C" void X509Store_Close_m5142 ();
extern "C" void X509Store_Open_m5143 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m5144 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m5145 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m5146 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m5147 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m5148 ();
extern "C" void X509SubjectKeyIdentifierExtension__ctor_m5149 ();
extern "C" void X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m5150 ();
extern "C" void X509SubjectKeyIdentifierExtension_CopyFrom_m5151 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChar_m5152 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHexChars_m5153 ();
extern "C" void X509SubjectKeyIdentifierExtension_FromHex_m5154 ();
extern "C" void X509SubjectKeyIdentifierExtension_Decode_m5155 ();
extern "C" void X509SubjectKeyIdentifierExtension_Encode_m5156 ();
extern "C" void X509SubjectKeyIdentifierExtension_ToString_m5157 ();
extern "C" void AsnEncodedData__ctor_m5158 ();
extern "C" void AsnEncodedData__ctor_m5159 ();
extern "C" void AsnEncodedData__ctor_m5160 ();
extern "C" void AsnEncodedData_get_Oid_m5161 ();
extern "C" void AsnEncodedData_set_Oid_m5162 ();
extern "C" void AsnEncodedData_get_RawData_m5163 ();
extern "C" void AsnEncodedData_set_RawData_m5164 ();
extern "C" void AsnEncodedData_CopyFrom_m5165 ();
extern "C" void AsnEncodedData_ToString_m5166 ();
extern "C" void AsnEncodedData_Default_m5167 ();
extern "C" void AsnEncodedData_BasicConstraintsExtension_m5168 ();
extern "C" void AsnEncodedData_EnhancedKeyUsageExtension_m5169 ();
extern "C" void AsnEncodedData_KeyUsageExtension_m5170 ();
extern "C" void AsnEncodedData_SubjectKeyIdentifierExtension_m5171 ();
extern "C" void AsnEncodedData_SubjectAltName_m5172 ();
extern "C" void AsnEncodedData_NetscapeCertType_m5173 ();
extern "C" void Oid__ctor_m5174 ();
extern "C" void Oid__ctor_m5175 ();
extern "C" void Oid__ctor_m5176 ();
extern "C" void Oid__ctor_m5177 ();
extern "C" void Oid_get_FriendlyName_m5178 ();
extern "C" void Oid_get_Value_m5179 ();
extern "C" void Oid_GetName_m5180 ();
extern "C" void OidCollection__ctor_m5181 ();
extern "C" void OidCollection_System_Collections_ICollection_CopyTo_m5182 ();
extern "C" void OidCollection_System_Collections_IEnumerable_GetEnumerator_m5183 ();
extern "C" void OidCollection_get_Count_m5184 ();
extern "C" void OidCollection_get_IsSynchronized_m5185 ();
extern "C" void OidCollection_get_Item_m5186 ();
extern "C" void OidCollection_get_SyncRoot_m5187 ();
extern "C" void OidCollection_Add_m5188 ();
extern "C" void OidEnumerator__ctor_m5189 ();
extern "C" void OidEnumerator_System_Collections_IEnumerator_get_Current_m5190 ();
extern "C" void OidEnumerator_MoveNext_m5191 ();
extern "C" void OidEnumerator_Reset_m5192 ();
extern "C" void MatchAppendEvaluator__ctor_m5193 ();
extern "C" void MatchAppendEvaluator_Invoke_m5194 ();
extern "C" void MatchAppendEvaluator_BeginInvoke_m5195 ();
extern "C" void MatchAppendEvaluator_EndInvoke_m5196 ();
extern "C" void BaseMachine__ctor_m5197 ();
extern "C" void BaseMachine_Replace_m5198 ();
extern "C" void BaseMachine_Scan_m5199 ();
extern "C" void BaseMachine_LTRReplace_m5200 ();
extern "C" void BaseMachine_RTLReplace_m5201 ();
extern "C" void Capture__ctor_m5202 ();
extern "C" void Capture__ctor_m5203 ();
extern "C" void Capture_get_Index_m5204 ();
extern "C" void Capture_get_Length_m5205 ();
extern "C" void Capture_get_Value_m4761 ();
extern "C" void Capture_ToString_m5206 ();
extern "C" void Capture_get_Text_m5207 ();
extern "C" void CaptureCollection__ctor_m5208 ();
extern "C" void CaptureCollection_get_Count_m5209 ();
extern "C" void CaptureCollection_get_IsSynchronized_m5210 ();
extern "C" void CaptureCollection_SetValue_m5211 ();
extern "C" void CaptureCollection_get_SyncRoot_m5212 ();
extern "C" void CaptureCollection_CopyTo_m5213 ();
extern "C" void CaptureCollection_GetEnumerator_m5214 ();
extern "C" void Group__ctor_m5215 ();
extern "C" void Group__ctor_m5216 ();
extern "C" void Group__ctor_m5217 ();
extern "C" void Group__cctor_m5218 ();
extern "C" void Group_get_Captures_m5219 ();
extern "C" void Group_get_Success_m4759 ();
extern "C" void GroupCollection__ctor_m5220 ();
extern "C" void GroupCollection_get_Count_m5221 ();
extern "C" void GroupCollection_get_IsSynchronized_m5222 ();
extern "C" void GroupCollection_get_Item_m4760 ();
extern "C" void GroupCollection_SetValue_m5223 ();
extern "C" void GroupCollection_get_SyncRoot_m5224 ();
extern "C" void GroupCollection_CopyTo_m5225 ();
extern "C" void GroupCollection_GetEnumerator_m5226 ();
extern "C" void Match__ctor_m5227 ();
extern "C" void Match__ctor_m5228 ();
extern "C" void Match__ctor_m5229 ();
extern "C" void Match__cctor_m5230 ();
extern "C" void Match_get_Empty_m5231 ();
extern "C" void Match_get_Groups_m5232 ();
extern "C" void Match_NextMatch_m5233 ();
extern "C" void Match_get_Regex_m5234 ();
extern "C" void Enumerator__ctor_m5235 ();
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m5236 ();
extern "C" void Enumerator_System_Collections_IEnumerator_get_Current_m5237 ();
extern "C" void Enumerator_System_Collections_IEnumerator_MoveNext_m5238 ();
extern "C" void MatchCollection__ctor_m5239 ();
extern "C" void MatchCollection_get_Count_m5240 ();
extern "C" void MatchCollection_get_IsSynchronized_m5241 ();
extern "C" void MatchCollection_get_Item_m5242 ();
extern "C" void MatchCollection_get_SyncRoot_m5243 ();
extern "C" void MatchCollection_CopyTo_m5244 ();
extern "C" void MatchCollection_GetEnumerator_m5245 ();
extern "C" void MatchCollection_TryToGet_m5246 ();
extern "C" void MatchCollection_get_FullList_m5247 ();
extern "C" void Regex__ctor_m5248 ();
extern "C" void Regex__ctor_m4757 ();
extern "C" void Regex__ctor_m5249 ();
extern "C" void Regex__ctor_m5250 ();
extern "C" void Regex__cctor_m5251 ();
extern "C" void Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m5252 ();
extern "C" void Regex_Replace_m3656 ();
extern "C" void Regex_Replace_m5253 ();
extern "C" void Regex_validate_options_m5254 ();
extern "C" void Regex_Init_m5255 ();
extern "C" void Regex_InitNewRegex_m5256 ();
extern "C" void Regex_CreateMachineFactory_m5257 ();
extern "C" void Regex_get_Options_m5258 ();
extern "C" void Regex_get_RightToLeft_m5259 ();
extern "C" void Regex_GroupNumberFromName_m5260 ();
extern "C" void Regex_GetGroupIndex_m5261 ();
extern "C" void Regex_default_startat_m5262 ();
extern "C" void Regex_IsMatch_m5263 ();
extern "C" void Regex_IsMatch_m5264 ();
extern "C" void Regex_Match_m5265 ();
extern "C" void Regex_Matches_m4758 ();
extern "C" void Regex_Matches_m5266 ();
extern "C" void Regex_Replace_m5267 ();
extern "C" void Regex_Replace_m5268 ();
extern "C" void Regex_ToString_m5269 ();
extern "C" void Regex_get_GroupCount_m5270 ();
extern "C" void Regex_get_Gap_m5271 ();
extern "C" void Regex_CreateMachine_m5272 ();
extern "C" void Regex_GetGroupNamesArray_m5273 ();
extern "C" void Regex_get_GroupNumbers_m5274 ();
extern "C" void Key__ctor_m5275 ();
extern "C" void Key_GetHashCode_m5276 ();
extern "C" void Key_Equals_m5277 ();
extern "C" void Key_ToString_m5278 ();
extern "C" void FactoryCache__ctor_m5279 ();
extern "C" void FactoryCache_Add_m5280 ();
extern "C" void FactoryCache_Cleanup_m5281 ();
extern "C" void FactoryCache_Lookup_m5282 ();
extern "C" void Node__ctor_m5283 ();
extern "C" void MRUList__ctor_m5284 ();
extern "C" void MRUList_Use_m5285 ();
extern "C" void MRUList_Evict_m5286 ();
extern "C" void CategoryUtils_CategoryFromName_m5287 ();
extern "C" void CategoryUtils_IsCategory_m5288 ();
extern "C" void CategoryUtils_IsCategory_m5289 ();
extern "C" void LinkRef__ctor_m5290 ();
extern "C" void InterpreterFactory__ctor_m5291 ();
extern "C" void InterpreterFactory_NewInstance_m5292 ();
extern "C" void InterpreterFactory_get_GroupCount_m5293 ();
extern "C" void InterpreterFactory_get_Gap_m5294 ();
extern "C" void InterpreterFactory_set_Gap_m5295 ();
extern "C" void InterpreterFactory_get_Mapping_m5296 ();
extern "C" void InterpreterFactory_set_Mapping_m5297 ();
extern "C" void InterpreterFactory_get_NamesMapping_m5298 ();
extern "C" void InterpreterFactory_set_NamesMapping_m5299 ();
extern "C" void PatternLinkStack__ctor_m5300 ();
extern "C" void PatternLinkStack_set_BaseAddress_m5301 ();
extern "C" void PatternLinkStack_get_OffsetAddress_m5302 ();
extern "C" void PatternLinkStack_set_OffsetAddress_m5303 ();
extern "C" void PatternLinkStack_GetOffset_m5304 ();
extern "C" void PatternLinkStack_GetCurrent_m5305 ();
extern "C" void PatternLinkStack_SetCurrent_m5306 ();
extern "C" void PatternCompiler__ctor_m5307 ();
extern "C" void PatternCompiler_EncodeOp_m5308 ();
extern "C" void PatternCompiler_GetMachineFactory_m5309 ();
extern "C" void PatternCompiler_EmitFalse_m5310 ();
extern "C" void PatternCompiler_EmitTrue_m5311 ();
extern "C" void PatternCompiler_EmitCount_m5312 ();
extern "C" void PatternCompiler_EmitCharacter_m5313 ();
extern "C" void PatternCompiler_EmitCategory_m5314 ();
extern "C" void PatternCompiler_EmitNotCategory_m5315 ();
extern "C" void PatternCompiler_EmitRange_m5316 ();
extern "C" void PatternCompiler_EmitSet_m5317 ();
extern "C" void PatternCompiler_EmitString_m5318 ();
extern "C" void PatternCompiler_EmitPosition_m5319 ();
extern "C" void PatternCompiler_EmitOpen_m5320 ();
extern "C" void PatternCompiler_EmitClose_m5321 ();
extern "C" void PatternCompiler_EmitBalanceStart_m5322 ();
extern "C" void PatternCompiler_EmitBalance_m5323 ();
extern "C" void PatternCompiler_EmitReference_m5324 ();
extern "C" void PatternCompiler_EmitIfDefined_m5325 ();
extern "C" void PatternCompiler_EmitSub_m5326 ();
extern "C" void PatternCompiler_EmitTest_m5327 ();
extern "C" void PatternCompiler_EmitBranch_m5328 ();
extern "C" void PatternCompiler_EmitJump_m5329 ();
extern "C" void PatternCompiler_EmitRepeat_m5330 ();
extern "C" void PatternCompiler_EmitUntil_m5331 ();
extern "C" void PatternCompiler_EmitFastRepeat_m5332 ();
extern "C" void PatternCompiler_EmitIn_m5333 ();
extern "C" void PatternCompiler_EmitAnchor_m5334 ();
extern "C" void PatternCompiler_EmitInfo_m5335 ();
extern "C" void PatternCompiler_NewLink_m5336 ();
extern "C" void PatternCompiler_ResolveLink_m5337 ();
extern "C" void PatternCompiler_EmitBranchEnd_m5338 ();
extern "C" void PatternCompiler_EmitAlternationEnd_m5339 ();
extern "C" void PatternCompiler_MakeFlags_m5340 ();
extern "C" void PatternCompiler_Emit_m5341 ();
extern "C" void PatternCompiler_Emit_m5342 ();
extern "C" void PatternCompiler_Emit_m5343 ();
extern "C" void PatternCompiler_get_CurrentAddress_m5344 ();
extern "C" void PatternCompiler_BeginLink_m5345 ();
extern "C" void PatternCompiler_EmitLink_m5346 ();
extern "C" void LinkStack__ctor_m5347 ();
extern "C" void LinkStack_Push_m5348 ();
extern "C" void LinkStack_Pop_m5349 ();
extern "C" void Mark_get_IsDefined_m5350 ();
extern "C" void Mark_get_Index_m5351 ();
extern "C" void Mark_get_Length_m5352 ();
extern "C" void IntStack_Pop_m5353 ();
extern "C" void IntStack_Push_m5354 ();
extern "C" void IntStack_get_Count_m5355 ();
extern "C" void IntStack_set_Count_m5356 ();
extern "C" void RepeatContext__ctor_m5357 ();
extern "C" void RepeatContext_get_Count_m5358 ();
extern "C" void RepeatContext_set_Count_m5359 ();
extern "C" void RepeatContext_get_Start_m5360 ();
extern "C" void RepeatContext_set_Start_m5361 ();
extern "C" void RepeatContext_get_IsMinimum_m5362 ();
extern "C" void RepeatContext_get_IsMaximum_m5363 ();
extern "C" void RepeatContext_get_IsLazy_m5364 ();
extern "C" void RepeatContext_get_Expression_m5365 ();
extern "C" void RepeatContext_get_Previous_m5366 ();
extern "C" void Interpreter__ctor_m5367 ();
extern "C" void Interpreter_ReadProgramCount_m5368 ();
extern "C" void Interpreter_Scan_m5369 ();
extern "C" void Interpreter_Reset_m5370 ();
extern "C" void Interpreter_Eval_m5371 ();
extern "C" void Interpreter_EvalChar_m5372 ();
extern "C" void Interpreter_TryMatch_m5373 ();
extern "C" void Interpreter_IsPosition_m5374 ();
extern "C" void Interpreter_IsWordChar_m5375 ();
extern "C" void Interpreter_GetString_m5376 ();
extern "C" void Interpreter_Open_m5377 ();
extern "C" void Interpreter_Close_m5378 ();
extern "C" void Interpreter_Balance_m5379 ();
extern "C" void Interpreter_Checkpoint_m5380 ();
extern "C" void Interpreter_Backtrack_m5381 ();
extern "C" void Interpreter_ResetGroups_m5382 ();
extern "C" void Interpreter_GetLastDefined_m5383 ();
extern "C" void Interpreter_CreateMark_m5384 ();
extern "C" void Interpreter_GetGroupInfo_m5385 ();
extern "C" void Interpreter_PopulateGroup_m5386 ();
extern "C" void Interpreter_GenerateMatch_m5387 ();
extern "C" void Interval__ctor_m5388 ();
extern "C" void Interval_get_Empty_m5389 ();
extern "C" void Interval_get_IsDiscontiguous_m5390 ();
extern "C" void Interval_get_IsSingleton_m5391 ();
extern "C" void Interval_get_IsEmpty_m5392 ();
extern "C" void Interval_get_Size_m5393 ();
extern "C" void Interval_IsDisjoint_m5394 ();
extern "C" void Interval_IsAdjacent_m5395 ();
extern "C" void Interval_Contains_m5396 ();
extern "C" void Interval_Contains_m5397 ();
extern "C" void Interval_Intersects_m5398 ();
extern "C" void Interval_Merge_m5399 ();
extern "C" void Interval_CompareTo_m5400 ();
extern "C" void Enumerator__ctor_m5401 ();
extern "C" void Enumerator_get_Current_m5402 ();
extern "C" void Enumerator_MoveNext_m5403 ();
extern "C" void Enumerator_Reset_m5404 ();
extern "C" void CostDelegate__ctor_m5405 ();
extern "C" void CostDelegate_Invoke_m5406 ();
extern "C" void CostDelegate_BeginInvoke_m5407 ();
extern "C" void CostDelegate_EndInvoke_m5408 ();
extern "C" void IntervalCollection__ctor_m5409 ();
extern "C" void IntervalCollection_get_Item_m5410 ();
extern "C" void IntervalCollection_Add_m5411 ();
extern "C" void IntervalCollection_Normalize_m5412 ();
extern "C" void IntervalCollection_GetMetaCollection_m5413 ();
extern "C" void IntervalCollection_Optimize_m5414 ();
extern "C" void IntervalCollection_get_Count_m5415 ();
extern "C" void IntervalCollection_get_IsSynchronized_m5416 ();
extern "C" void IntervalCollection_get_SyncRoot_m5417 ();
extern "C" void IntervalCollection_CopyTo_m5418 ();
extern "C" void IntervalCollection_GetEnumerator_m5419 ();
extern "C" void Parser__ctor_m5420 ();
extern "C" void Parser_ParseDecimal_m5421 ();
extern "C" void Parser_ParseOctal_m5422 ();
extern "C" void Parser_ParseHex_m5423 ();
extern "C" void Parser_ParseNumber_m5424 ();
extern "C" void Parser_ParseName_m5425 ();
extern "C" void Parser_ParseRegularExpression_m5426 ();
extern "C" void Parser_GetMapping_m5427 ();
extern "C" void Parser_ParseGroup_m5428 ();
extern "C" void Parser_ParseGroupingConstruct_m5429 ();
extern "C" void Parser_ParseAssertionType_m5430 ();
extern "C" void Parser_ParseOptions_m5431 ();
extern "C" void Parser_ParseCharacterClass_m5432 ();
extern "C" void Parser_ParseRepetitionBounds_m5433 ();
extern "C" void Parser_ParseUnicodeCategory_m5434 ();
extern "C" void Parser_ParseSpecial_m5435 ();
extern "C" void Parser_ParseEscape_m5436 ();
extern "C" void Parser_ParseName_m5437 ();
extern "C" void Parser_IsNameChar_m5438 ();
extern "C" void Parser_ParseNumber_m5439 ();
extern "C" void Parser_ParseDigit_m5440 ();
extern "C" void Parser_ConsumeWhitespace_m5441 ();
extern "C" void Parser_ResolveReferences_m5442 ();
extern "C" void Parser_HandleExplicitNumericGroups_m5443 ();
extern "C" void Parser_IsIgnoreCase_m5444 ();
extern "C" void Parser_IsMultiline_m5445 ();
extern "C" void Parser_IsExplicitCapture_m5446 ();
extern "C" void Parser_IsSingleline_m5447 ();
extern "C" void Parser_IsIgnorePatternWhitespace_m5448 ();
extern "C" void Parser_IsECMAScript_m5449 ();
extern "C" void Parser_NewParseException_m5450 ();
extern "C" void QuickSearch__ctor_m5451 ();
extern "C" void QuickSearch__cctor_m5452 ();
extern "C" void QuickSearch_get_Length_m5453 ();
extern "C" void QuickSearch_Search_m5454 ();
extern "C" void QuickSearch_SetupShiftTable_m5455 ();
extern "C" void QuickSearch_GetShiftDistance_m5456 ();
extern "C" void QuickSearch_GetChar_m5457 ();
extern "C" void ReplacementEvaluator__ctor_m5458 ();
extern "C" void ReplacementEvaluator_Evaluate_m5459 ();
extern "C" void ReplacementEvaluator_EvaluateAppend_m5460 ();
extern "C" void ReplacementEvaluator_get_NeedsGroupsOrCaptures_m5461 ();
extern "C" void ReplacementEvaluator_Ensure_m5462 ();
extern "C" void ReplacementEvaluator_AddFromReplacement_m5463 ();
extern "C" void ReplacementEvaluator_AddInt_m5464 ();
extern "C" void ReplacementEvaluator_Compile_m5465 ();
extern "C" void ReplacementEvaluator_CompileTerm_m5466 ();
extern "C" void ExpressionCollection__ctor_m5467 ();
extern "C" void ExpressionCollection_Add_m5468 ();
extern "C" void ExpressionCollection_get_Item_m5469 ();
extern "C" void ExpressionCollection_set_Item_m5470 ();
extern "C" void ExpressionCollection_OnValidate_m5471 ();
extern "C" void Expression__ctor_m5472 ();
extern "C" void Expression_GetFixedWidth_m5473 ();
extern "C" void Expression_GetAnchorInfo_m5474 ();
extern "C" void CompositeExpression__ctor_m5475 ();
extern "C" void CompositeExpression_get_Expressions_m5476 ();
extern "C" void CompositeExpression_GetWidth_m5477 ();
extern "C" void CompositeExpression_IsComplex_m5478 ();
extern "C" void Group__ctor_m5479 ();
extern "C" void Group_AppendExpression_m5480 ();
extern "C" void Group_Compile_m5481 ();
extern "C" void Group_GetWidth_m5482 ();
extern "C" void Group_GetAnchorInfo_m5483 ();
extern "C" void RegularExpression__ctor_m5484 ();
extern "C" void RegularExpression_set_GroupCount_m5485 ();
extern "C" void RegularExpression_Compile_m5486 ();
extern "C" void CapturingGroup__ctor_m5487 ();
extern "C" void CapturingGroup_get_Index_m5488 ();
extern "C" void CapturingGroup_set_Index_m5489 ();
extern "C" void CapturingGroup_get_Name_m5490 ();
extern "C" void CapturingGroup_set_Name_m5491 ();
extern "C" void CapturingGroup_get_IsNamed_m5492 ();
extern "C" void CapturingGroup_Compile_m5493 ();
extern "C" void CapturingGroup_IsComplex_m5494 ();
extern "C" void CapturingGroup_CompareTo_m5495 ();
extern "C" void BalancingGroup__ctor_m5496 ();
extern "C" void BalancingGroup_set_Balance_m5497 ();
extern "C" void BalancingGroup_Compile_m5498 ();
extern "C" void NonBacktrackingGroup__ctor_m5499 ();
extern "C" void NonBacktrackingGroup_Compile_m5500 ();
extern "C" void NonBacktrackingGroup_IsComplex_m5501 ();
extern "C" void Repetition__ctor_m5502 ();
extern "C" void Repetition_get_Expression_m5503 ();
extern "C" void Repetition_set_Expression_m5504 ();
extern "C" void Repetition_get_Minimum_m5505 ();
extern "C" void Repetition_Compile_m5506 ();
extern "C" void Repetition_GetWidth_m5507 ();
extern "C" void Repetition_GetAnchorInfo_m5508 ();
extern "C" void Assertion__ctor_m5509 ();
extern "C" void Assertion_get_TrueExpression_m5510 ();
extern "C" void Assertion_set_TrueExpression_m5511 ();
extern "C" void Assertion_get_FalseExpression_m5512 ();
extern "C" void Assertion_set_FalseExpression_m5513 ();
extern "C" void Assertion_GetWidth_m5514 ();
extern "C" void CaptureAssertion__ctor_m5515 ();
extern "C" void CaptureAssertion_set_CapturingGroup_m5516 ();
extern "C" void CaptureAssertion_Compile_m5517 ();
extern "C" void CaptureAssertion_IsComplex_m5518 ();
extern "C" void CaptureAssertion_get_Alternate_m5519 ();
extern "C" void ExpressionAssertion__ctor_m5520 ();
extern "C" void ExpressionAssertion_set_Reverse_m5521 ();
extern "C" void ExpressionAssertion_set_Negate_m5522 ();
extern "C" void ExpressionAssertion_get_TestExpression_m5523 ();
extern "C" void ExpressionAssertion_set_TestExpression_m5524 ();
extern "C" void ExpressionAssertion_Compile_m5525 ();
extern "C" void ExpressionAssertion_IsComplex_m5526 ();
extern "C" void Alternation__ctor_m5527 ();
extern "C" void Alternation_get_Alternatives_m5528 ();
extern "C" void Alternation_AddAlternative_m5529 ();
extern "C" void Alternation_Compile_m5530 ();
extern "C" void Alternation_GetWidth_m5531 ();
extern "C" void Literal__ctor_m5532 ();
extern "C" void Literal_CompileLiteral_m5533 ();
extern "C" void Literal_Compile_m5534 ();
extern "C" void Literal_GetWidth_m5535 ();
extern "C" void Literal_GetAnchorInfo_m5536 ();
extern "C" void Literal_IsComplex_m5537 ();
extern "C" void PositionAssertion__ctor_m5538 ();
extern "C" void PositionAssertion_Compile_m5539 ();
extern "C" void PositionAssertion_GetWidth_m5540 ();
extern "C" void PositionAssertion_IsComplex_m5541 ();
extern "C" void PositionAssertion_GetAnchorInfo_m5542 ();
extern "C" void Reference__ctor_m5543 ();
extern "C" void Reference_get_CapturingGroup_m5544 ();
extern "C" void Reference_set_CapturingGroup_m5545 ();
extern "C" void Reference_get_IgnoreCase_m5546 ();
extern "C" void Reference_Compile_m5547 ();
extern "C" void Reference_GetWidth_m5548 ();
extern "C" void Reference_IsComplex_m5549 ();
extern "C" void BackslashNumber__ctor_m5550 ();
extern "C" void BackslashNumber_ResolveReference_m5551 ();
extern "C" void BackslashNumber_Compile_m5552 ();
extern "C" void CharacterClass__ctor_m5553 ();
extern "C" void CharacterClass__ctor_m5554 ();
extern "C" void CharacterClass__cctor_m5555 ();
extern "C" void CharacterClass_AddCategory_m5556 ();
extern "C" void CharacterClass_AddCharacter_m5557 ();
extern "C" void CharacterClass_AddRange_m5558 ();
extern "C" void CharacterClass_Compile_m5559 ();
extern "C" void CharacterClass_GetWidth_m5560 ();
extern "C" void CharacterClass_IsComplex_m5561 ();
extern "C" void CharacterClass_GetIntervalCost_m5562 ();
extern "C" void AnchorInfo__ctor_m5563 ();
extern "C" void AnchorInfo__ctor_m5564 ();
extern "C" void AnchorInfo__ctor_m5565 ();
extern "C" void AnchorInfo_get_Offset_m5566 ();
extern "C" void AnchorInfo_get_Width_m5567 ();
extern "C" void AnchorInfo_get_Length_m5568 ();
extern "C" void AnchorInfo_get_IsUnknownWidth_m5569 ();
extern "C" void AnchorInfo_get_IsComplete_m5570 ();
extern "C" void AnchorInfo_get_Substring_m5571 ();
extern "C" void AnchorInfo_get_IgnoreCase_m5572 ();
extern "C" void AnchorInfo_get_Position_m5573 ();
extern "C" void AnchorInfo_get_IsSubstring_m5574 ();
extern "C" void AnchorInfo_get_IsPosition_m5575 ();
extern "C" void AnchorInfo_GetInterval_m5576 ();
extern "C" void DefaultUriParser__ctor_m5577 ();
extern "C" void DefaultUriParser__ctor_m5578 ();
extern "C" void UriScheme__ctor_m5579 ();
extern "C" void Uri__ctor_m5580 ();
extern "C" void Uri__ctor_m5581 ();
extern "C" void Uri__ctor_m5582 ();
extern "C" void Uri__cctor_m5583 ();
extern "C" void Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m5584 ();
extern "C" void Uri_get_AbsoluteUri_m5585 ();
extern "C" void Uri_get_Authority_m5586 ();
extern "C" void Uri_get_Host_m4724 ();
extern "C" void Uri_get_IsFile_m5587 ();
extern "C" void Uri_get_IsLoopback_m5588 ();
extern "C" void Uri_get_IsUnc_m5589 ();
extern "C" void Uri_get_Scheme_m5590 ();
extern "C" void Uri_get_IsAbsoluteUri_m5591 ();
extern "C" void Uri_CheckHostName_m5592 ();
extern "C" void Uri_IsIPv4Address_m5593 ();
extern "C" void Uri_IsDomainAddress_m5594 ();
extern "C" void Uri_CheckSchemeName_m5595 ();
extern "C" void Uri_IsAlpha_m5596 ();
extern "C" void Uri_Equals_m5597 ();
extern "C" void Uri_InternalEquals_m5598 ();
extern "C" void Uri_GetHashCode_m5599 ();
extern "C" void Uri_GetLeftPart_m5600 ();
extern "C" void Uri_FromHex_m5601 ();
extern "C" void Uri_HexEscape_m5602 ();
extern "C" void Uri_IsHexDigit_m5603 ();
extern "C" void Uri_IsHexEncoding_m5604 ();
extern "C" void Uri_AppendQueryAndFragment_m5605 ();
extern "C" void Uri_ToString_m5606 ();
extern "C" void Uri_EscapeString_m5607 ();
extern "C" void Uri_EscapeString_m5608 ();
extern "C" void Uri_ParseUri_m5609 ();
extern "C" void Uri_Unescape_m5610 ();
extern "C" void Uri_Unescape_m5611 ();
extern "C" void Uri_ParseAsWindowsUNC_m5612 ();
extern "C" void Uri_ParseAsWindowsAbsoluteFilePath_m5613 ();
extern "C" void Uri_ParseAsUnixAbsoluteFilePath_m5614 ();
extern "C" void Uri_Parse_m5615 ();
extern "C" void Uri_ParseNoExceptions_m5616 ();
extern "C" void Uri_CompactEscaped_m5617 ();
extern "C" void Uri_Reduce_m5618 ();
extern "C" void Uri_HexUnescapeMultiByte_m5619 ();
extern "C" void Uri_GetSchemeDelimiter_m5620 ();
extern "C" void Uri_GetDefaultPort_m5621 ();
extern "C" void Uri_GetOpaqueWiseSchemeDelimiter_m5622 ();
extern "C" void Uri_IsPredefinedScheme_m5623 ();
extern "C" void Uri_get_Parser_m5624 ();
extern "C" void Uri_EnsureAbsoluteUri_m5625 ();
extern "C" void Uri_op_Equality_m5626 ();
extern "C" void UriFormatException__ctor_m5627 ();
extern "C" void UriFormatException__ctor_m5628 ();
extern "C" void UriFormatException__ctor_m5629 ();
extern "C" void UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m5630 ();
extern "C" void UriParser__ctor_m5631 ();
extern "C" void UriParser__cctor_m5632 ();
extern "C" void UriParser_InitializeAndValidate_m5633 ();
extern "C" void UriParser_OnRegister_m5634 ();
extern "C" void UriParser_set_SchemeName_m5635 ();
extern "C" void UriParser_get_DefaultPort_m5636 ();
extern "C" void UriParser_set_DefaultPort_m5637 ();
extern "C" void UriParser_CreateDefaults_m5638 ();
extern "C" void UriParser_InternalRegister_m5639 ();
extern "C" void UriParser_GetParser_m5640 ();
extern "C" void RemoteCertificateValidationCallback__ctor_m5641 ();
extern "C" void RemoteCertificateValidationCallback_Invoke_m4732 ();
extern "C" void RemoteCertificateValidationCallback_BeginInvoke_m5642 ();
extern "C" void RemoteCertificateValidationCallback_EndInvoke_m5643 ();
extern "C" void MatchEvaluator__ctor_m5644 ();
extern "C" void MatchEvaluator_Invoke_m5645 ();
extern "C" void MatchEvaluator_BeginInvoke_m5646 ();
extern "C" void MatchEvaluator_EndInvoke_m5647 ();
extern "C" void Object__ctor_m220 ();
extern "C" void Object_Equals_m5736 ();
extern "C" void Object_Equals_m5733 ();
extern "C" void Object_Finalize_m3578 ();
extern "C" void Object_GetHashCode_m5737 ();
extern "C" void Object_GetType_m1927 ();
extern "C" void Object_MemberwiseClone_m5738 ();
extern "C" void Object_ToString_m3672 ();
extern "C" void Object_ReferenceEquals_m3617 ();
extern "C" void Object_InternalGetHashCode_m5739 ();
extern "C" void ValueType__ctor_m5740 ();
extern "C" void ValueType_InternalEquals_m5741 ();
extern "C" void ValueType_DefaultEquals_m5742 ();
extern "C" void ValueType_Equals_m5743 ();
extern "C" void ValueType_InternalGetHashCode_m5744 ();
extern "C" void ValueType_GetHashCode_m5745 ();
extern "C" void ValueType_ToString_m5746 ();
extern "C" void Attribute__ctor_m3602 ();
extern "C" void Attribute_CheckParameters_m5747 ();
extern "C" void Attribute_GetCustomAttribute_m5748 ();
extern "C" void Attribute_GetCustomAttribute_m5749 ();
extern "C" void Attribute_GetHashCode_m3673 ();
extern "C" void Attribute_IsDefined_m5750 ();
extern "C" void Attribute_IsDefined_m5751 ();
extern "C" void Attribute_IsDefined_m5752 ();
extern "C" void Attribute_IsDefined_m5753 ();
extern "C" void Attribute_Equals_m5754 ();
extern "C" void Int32_System_IConvertible_ToBoolean_m5755 ();
extern "C" void Int32_System_IConvertible_ToByte_m5756 ();
extern "C" void Int32_System_IConvertible_ToChar_m5757 ();
extern "C" void Int32_System_IConvertible_ToDateTime_m5758 ();
extern "C" void Int32_System_IConvertible_ToDecimal_m5759 ();
extern "C" void Int32_System_IConvertible_ToDouble_m5760 ();
extern "C" void Int32_System_IConvertible_ToInt16_m5761 ();
extern "C" void Int32_System_IConvertible_ToInt32_m5762 ();
extern "C" void Int32_System_IConvertible_ToInt64_m5763 ();
extern "C" void Int32_System_IConvertible_ToSByte_m5764 ();
extern "C" void Int32_System_IConvertible_ToSingle_m5765 ();
extern "C" void Int32_System_IConvertible_ToType_m5766 ();
extern "C" void Int32_System_IConvertible_ToUInt16_m5767 ();
extern "C" void Int32_System_IConvertible_ToUInt32_m5768 ();
extern "C" void Int32_System_IConvertible_ToUInt64_m5769 ();
extern "C" void Int32_CompareTo_m5770 ();
extern "C" void Int32_Equals_m5771 ();
extern "C" void Int32_GetHashCode_m3592 ();
extern "C" void Int32_CompareTo_m1865 ();
extern "C" void Int32_Equals_m3594 ();
extern "C" void Int32_ProcessTrailingWhitespace_m5772 ();
extern "C" void Int32_Parse_m5773 ();
extern "C" void Int32_Parse_m5774 ();
extern "C" void Int32_CheckStyle_m5775 ();
extern "C" void Int32_JumpOverWhite_m5776 ();
extern "C" void Int32_FindSign_m5777 ();
extern "C" void Int32_FindCurrency_m5778 ();
extern "C" void Int32_FindExponent_m5779 ();
extern "C" void Int32_FindOther_m5780 ();
extern "C" void Int32_ValidDigit_m5781 ();
extern "C" void Int32_GetFormatException_m5782 ();
extern "C" void Int32_Parse_m5783 ();
extern "C" void Int32_Parse_m4717 ();
extern "C" void Int32_Parse_m5784 ();
extern "C" void Int32_TryParse_m5785 ();
extern "C" void Int32_TryParse_m5676 ();
extern "C" void Int32_ToString_m3650 ();
extern "C" void Int32_ToString_m4701 ();
extern "C" void Int32_ToString_m5709 ();
extern "C" void Int32_ToString_m4705 ();
extern "C" void Int32_GetTypeCode_m5786 ();
extern "C" void SerializableAttribute__ctor_m5787 ();
extern "C" void AttributeUsageAttribute__ctor_m5788 ();
extern "C" void AttributeUsageAttribute_get_AllowMultiple_m5789 ();
extern "C" void AttributeUsageAttribute_set_AllowMultiple_m5790 ();
extern "C" void AttributeUsageAttribute_get_Inherited_m5791 ();
extern "C" void AttributeUsageAttribute_set_Inherited_m5792 ();
extern "C" void ComVisibleAttribute__ctor_m5793 ();
extern "C" void Int64_System_IConvertible_ToBoolean_m5794 ();
extern "C" void Int64_System_IConvertible_ToByte_m5795 ();
extern "C" void Int64_System_IConvertible_ToChar_m5796 ();
extern "C" void Int64_System_IConvertible_ToDateTime_m5797 ();
extern "C" void Int64_System_IConvertible_ToDecimal_m5798 ();
extern "C" void Int64_System_IConvertible_ToDouble_m5799 ();
extern "C" void Int64_System_IConvertible_ToInt16_m5800 ();
extern "C" void Int64_System_IConvertible_ToInt32_m5801 ();
extern "C" void Int64_System_IConvertible_ToInt64_m5802 ();
extern "C" void Int64_System_IConvertible_ToSByte_m5803 ();
extern "C" void Int64_System_IConvertible_ToSingle_m5804 ();
extern "C" void Int64_System_IConvertible_ToType_m5805 ();
extern "C" void Int64_System_IConvertible_ToUInt16_m5806 ();
extern "C" void Int64_System_IConvertible_ToUInt32_m5807 ();
extern "C" void Int64_System_IConvertible_ToUInt64_m5808 ();
extern "C" void Int64_CompareTo_m5809 ();
extern "C" void Int64_Equals_m5810 ();
extern "C" void Int64_GetHashCode_m5811 ();
extern "C" void Int64_CompareTo_m5812 ();
extern "C" void Int64_Equals_m5813 ();
extern "C" void Int64_Parse_m5814 ();
extern "C" void Int64_Parse_m5815 ();
extern "C" void Int64_Parse_m5816 ();
extern "C" void Int64_Parse_m5817 ();
extern "C" void Int64_Parse_m5818 ();
extern "C" void Int64_TryParse_m5819 ();
extern "C" void Int64_TryParse_m5673 ();
extern "C" void Int64_ToString_m5674 ();
extern "C" void Int64_ToString_m5820 ();
extern "C" void Int64_ToString_m5821 ();
extern "C" void Int64_ToString_m5822 ();
extern "C" void UInt32_System_IConvertible_ToBoolean_m5823 ();
extern "C" void UInt32_System_IConvertible_ToByte_m5824 ();
extern "C" void UInt32_System_IConvertible_ToChar_m5825 ();
extern "C" void UInt32_System_IConvertible_ToDateTime_m5826 ();
extern "C" void UInt32_System_IConvertible_ToDecimal_m5827 ();
extern "C" void UInt32_System_IConvertible_ToDouble_m5828 ();
extern "C" void UInt32_System_IConvertible_ToInt16_m5829 ();
extern "C" void UInt32_System_IConvertible_ToInt32_m5830 ();
extern "C" void UInt32_System_IConvertible_ToInt64_m5831 ();
extern "C" void UInt32_System_IConvertible_ToSByte_m5832 ();
extern "C" void UInt32_System_IConvertible_ToSingle_m5833 ();
extern "C" void UInt32_System_IConvertible_ToType_m5834 ();
extern "C" void UInt32_System_IConvertible_ToUInt16_m5835 ();
extern "C" void UInt32_System_IConvertible_ToUInt32_m5836 ();
extern "C" void UInt32_System_IConvertible_ToUInt64_m5837 ();
extern "C" void UInt32_CompareTo_m5838 ();
extern "C" void UInt32_Equals_m5839 ();
extern "C" void UInt32_GetHashCode_m5840 ();
extern "C" void UInt32_CompareTo_m5841 ();
extern "C" void UInt32_Equals_m5842 ();
extern "C" void UInt32_Parse_m5843 ();
extern "C" void UInt32_Parse_m5844 ();
extern "C" void UInt32_Parse_m5845 ();
extern "C" void UInt32_Parse_m5846 ();
extern "C" void UInt32_TryParse_m5727 ();
extern "C" void UInt32_TryParse_m5847 ();
extern "C" void UInt32_ToString_m5848 ();
extern "C" void UInt32_ToString_m5849 ();
extern "C" void UInt32_ToString_m5850 ();
extern "C" void UInt32_ToString_m5851 ();
extern "C" void CLSCompliantAttribute__ctor_m5852 ();
extern "C" void UInt64_System_IConvertible_ToBoolean_m5853 ();
extern "C" void UInt64_System_IConvertible_ToByte_m5854 ();
extern "C" void UInt64_System_IConvertible_ToChar_m5855 ();
extern "C" void UInt64_System_IConvertible_ToDateTime_m5856 ();
extern "C" void UInt64_System_IConvertible_ToDecimal_m5857 ();
extern "C" void UInt64_System_IConvertible_ToDouble_m5858 ();
extern "C" void UInt64_System_IConvertible_ToInt16_m5859 ();
extern "C" void UInt64_System_IConvertible_ToInt32_m5860 ();
extern "C" void UInt64_System_IConvertible_ToInt64_m5861 ();
extern "C" void UInt64_System_IConvertible_ToSByte_m5862 ();
extern "C" void UInt64_System_IConvertible_ToSingle_m5863 ();
extern "C" void UInt64_System_IConvertible_ToType_m5864 ();
extern "C" void UInt64_System_IConvertible_ToUInt16_m5865 ();
extern "C" void UInt64_System_IConvertible_ToUInt32_m5866 ();
extern "C" void UInt64_System_IConvertible_ToUInt64_m5867 ();
extern "C" void UInt64_CompareTo_m5868 ();
extern "C" void UInt64_Equals_m5869 ();
extern "C" void UInt64_GetHashCode_m5870 ();
extern "C" void UInt64_CompareTo_m5871 ();
extern "C" void UInt64_Equals_m5872 ();
extern "C" void UInt64_Parse_m5873 ();
extern "C" void UInt64_Parse_m5874 ();
extern "C" void UInt64_Parse_m5875 ();
extern "C" void UInt64_TryParse_m5876 ();
extern "C" void UInt64_ToString_m5877 ();
extern "C" void UInt64_ToString_m4646 ();
extern "C" void UInt64_ToString_m5878 ();
extern "C" void UInt64_ToString_m5879 ();
extern "C" void Byte_System_IConvertible_ToType_m5880 ();
extern "C" void Byte_System_IConvertible_ToBoolean_m5881 ();
extern "C" void Byte_System_IConvertible_ToByte_m5882 ();
extern "C" void Byte_System_IConvertible_ToChar_m5883 ();
extern "C" void Byte_System_IConvertible_ToDateTime_m5884 ();
extern "C" void Byte_System_IConvertible_ToDecimal_m5885 ();
extern "C" void Byte_System_IConvertible_ToDouble_m5886 ();
extern "C" void Byte_System_IConvertible_ToInt16_m5887 ();
extern "C" void Byte_System_IConvertible_ToInt32_m5888 ();
extern "C" void Byte_System_IConvertible_ToInt64_m5889 ();
extern "C" void Byte_System_IConvertible_ToSByte_m5890 ();
extern "C" void Byte_System_IConvertible_ToSingle_m5891 ();
extern "C" void Byte_System_IConvertible_ToUInt16_m5892 ();
extern "C" void Byte_System_IConvertible_ToUInt32_m5893 ();
extern "C" void Byte_System_IConvertible_ToUInt64_m5894 ();
extern "C" void Byte_CompareTo_m5895 ();
extern "C" void Byte_Equals_m5896 ();
extern "C" void Byte_GetHashCode_m5897 ();
extern "C" void Byte_CompareTo_m5898 ();
extern "C" void Byte_Equals_m5899 ();
extern "C" void Byte_Parse_m5900 ();
extern "C" void Byte_Parse_m5901 ();
extern "C" void Byte_Parse_m5902 ();
extern "C" void Byte_TryParse_m5903 ();
extern "C" void Byte_TryParse_m5904 ();
extern "C" void Byte_ToString_m4702 ();
extern "C" void Byte_ToString_m4636 ();
extern "C" void Byte_ToString_m4645 ();
extern "C" void Byte_ToString_m4651 ();
extern "C" void SByte_System_IConvertible_ToBoolean_m5905 ();
extern "C" void SByte_System_IConvertible_ToByte_m5906 ();
extern "C" void SByte_System_IConvertible_ToChar_m5907 ();
extern "C" void SByte_System_IConvertible_ToDateTime_m5908 ();
extern "C" void SByte_System_IConvertible_ToDecimal_m5909 ();
extern "C" void SByte_System_IConvertible_ToDouble_m5910 ();
extern "C" void SByte_System_IConvertible_ToInt16_m5911 ();
extern "C" void SByte_System_IConvertible_ToInt32_m5912 ();
extern "C" void SByte_System_IConvertible_ToInt64_m5913 ();
extern "C" void SByte_System_IConvertible_ToSByte_m5914 ();
extern "C" void SByte_System_IConvertible_ToSingle_m5915 ();
extern "C" void SByte_System_IConvertible_ToType_m5916 ();
extern "C" void SByte_System_IConvertible_ToUInt16_m5917 ();
extern "C" void SByte_System_IConvertible_ToUInt32_m5918 ();
extern "C" void SByte_System_IConvertible_ToUInt64_m5919 ();
extern "C" void SByte_CompareTo_m5920 ();
extern "C" void SByte_Equals_m5921 ();
extern "C" void SByte_GetHashCode_m5922 ();
extern "C" void SByte_CompareTo_m5923 ();
extern "C" void SByte_Equals_m5924 ();
extern "C" void SByte_Parse_m5925 ();
extern "C" void SByte_Parse_m5926 ();
extern "C" void SByte_Parse_m5927 ();
extern "C" void SByte_TryParse_m5928 ();
extern "C" void SByte_ToString_m5929 ();
extern "C" void SByte_ToString_m5930 ();
extern "C" void SByte_ToString_m5931 ();
extern "C" void SByte_ToString_m5932 ();
extern "C" void Int16_System_IConvertible_ToBoolean_m5933 ();
extern "C" void Int16_System_IConvertible_ToByte_m5934 ();
extern "C" void Int16_System_IConvertible_ToChar_m5935 ();
extern "C" void Int16_System_IConvertible_ToDateTime_m5936 ();
extern "C" void Int16_System_IConvertible_ToDecimal_m5937 ();
extern "C" void Int16_System_IConvertible_ToDouble_m5938 ();
extern "C" void Int16_System_IConvertible_ToInt16_m5939 ();
extern "C" void Int16_System_IConvertible_ToInt32_m5940 ();
extern "C" void Int16_System_IConvertible_ToInt64_m5941 ();
extern "C" void Int16_System_IConvertible_ToSByte_m5942 ();
extern "C" void Int16_System_IConvertible_ToSingle_m5943 ();
extern "C" void Int16_System_IConvertible_ToType_m5944 ();
extern "C" void Int16_System_IConvertible_ToUInt16_m5945 ();
extern "C" void Int16_System_IConvertible_ToUInt32_m5946 ();
extern "C" void Int16_System_IConvertible_ToUInt64_m5947 ();
extern "C" void Int16_CompareTo_m5948 ();
extern "C" void Int16_Equals_m5949 ();
extern "C" void Int16_GetHashCode_m5950 ();
extern "C" void Int16_CompareTo_m5951 ();
extern "C" void Int16_Equals_m5952 ();
extern "C" void Int16_Parse_m5953 ();
extern "C" void Int16_Parse_m5954 ();
extern "C" void Int16_Parse_m5955 ();
extern "C" void Int16_TryParse_m5956 ();
extern "C" void Int16_ToString_m5957 ();
extern "C" void Int16_ToString_m5958 ();
extern "C" void Int16_ToString_m5959 ();
extern "C" void Int16_ToString_m5960 ();
extern "C" void UInt16_System_IConvertible_ToBoolean_m5961 ();
extern "C" void UInt16_System_IConvertible_ToByte_m5962 ();
extern "C" void UInt16_System_IConvertible_ToChar_m5963 ();
extern "C" void UInt16_System_IConvertible_ToDateTime_m5964 ();
extern "C" void UInt16_System_IConvertible_ToDecimal_m5965 ();
extern "C" void UInt16_System_IConvertible_ToDouble_m5966 ();
extern "C" void UInt16_System_IConvertible_ToInt16_m5967 ();
extern "C" void UInt16_System_IConvertible_ToInt32_m5968 ();
extern "C" void UInt16_System_IConvertible_ToInt64_m5969 ();
extern "C" void UInt16_System_IConvertible_ToSByte_m5970 ();
extern "C" void UInt16_System_IConvertible_ToSingle_m5971 ();
extern "C" void UInt16_System_IConvertible_ToType_m5972 ();
extern "C" void UInt16_System_IConvertible_ToUInt16_m5973 ();
extern "C" void UInt16_System_IConvertible_ToUInt32_m5974 ();
extern "C" void UInt16_System_IConvertible_ToUInt64_m5975 ();
extern "C" void UInt16_CompareTo_m5976 ();
extern "C" void UInt16_Equals_m5977 ();
extern "C" void UInt16_GetHashCode_m5978 ();
extern "C" void UInt16_CompareTo_m5979 ();
extern "C" void UInt16_Equals_m5980 ();
extern "C" void UInt16_Parse_m5981 ();
extern "C" void UInt16_Parse_m5982 ();
extern "C" void UInt16_TryParse_m5983 ();
extern "C" void UInt16_TryParse_m5984 ();
extern "C" void UInt16_ToString_m5985 ();
extern "C" void UInt16_ToString_m5986 ();
extern "C" void UInt16_ToString_m5987 ();
extern "C" void UInt16_ToString_m5988 ();
extern "C" void Char__cctor_m5989 ();
extern "C" void Char_System_IConvertible_ToType_m5990 ();
extern "C" void Char_System_IConvertible_ToBoolean_m5991 ();
extern "C" void Char_System_IConvertible_ToByte_m5992 ();
extern "C" void Char_System_IConvertible_ToChar_m5993 ();
extern "C" void Char_System_IConvertible_ToDateTime_m5994 ();
extern "C" void Char_System_IConvertible_ToDecimal_m5995 ();
extern "C" void Char_System_IConvertible_ToDouble_m5996 ();
extern "C" void Char_System_IConvertible_ToInt16_m5997 ();
extern "C" void Char_System_IConvertible_ToInt32_m5998 ();
extern "C" void Char_System_IConvertible_ToInt64_m5999 ();
extern "C" void Char_System_IConvertible_ToSByte_m6000 ();
extern "C" void Char_System_IConvertible_ToSingle_m6001 ();
extern "C" void Char_System_IConvertible_ToUInt16_m6002 ();
extern "C" void Char_System_IConvertible_ToUInt32_m6003 ();
extern "C" void Char_System_IConvertible_ToUInt64_m6004 ();
extern "C" void Char_GetDataTablePointers_m6005 ();
extern "C" void Char_CompareTo_m6006 ();
extern "C" void Char_Equals_m6007 ();
extern "C" void Char_CompareTo_m6008 ();
extern "C" void Char_Equals_m6009 ();
extern "C" void Char_GetHashCode_m6010 ();
extern "C" void Char_GetUnicodeCategory_m5718 ();
extern "C" void Char_IsDigit_m5716 ();
extern "C" void Char_IsLetter_m2254 ();
extern "C" void Char_IsLetterOrDigit_m5715 ();
extern "C" void Char_IsLower_m2255 ();
extern "C" void Char_IsSurrogate_m6011 ();
extern "C" void Char_IsUpper_m2257 ();
extern "C" void Char_IsWhiteSpace_m5717 ();
extern "C" void Char_IsWhiteSpace_m5690 ();
extern "C" void Char_CheckParameter_m6012 ();
extern "C" void Char_Parse_m6013 ();
extern "C" void Char_ToLower_m2258 ();
extern "C" void Char_ToLowerInvariant_m6014 ();
extern "C" void Char_ToLower_m6015 ();
extern "C" void Char_ToUpper_m2256 ();
extern "C" void Char_ToUpperInvariant_m5692 ();
extern "C" void Char_ToString_m2233 ();
extern "C" void Char_ToString_m6016 ();
extern "C" void Char_GetTypeCode_m6017 ();
extern "C" void String__ctor_m6018 ();
extern "C" void String__ctor_m6019 ();
extern "C" void String__ctor_m6020 ();
extern "C" void String__ctor_m6021 ();
extern "C" void String__cctor_m6022 ();
extern "C" void String_System_IConvertible_ToBoolean_m6023 ();
extern "C" void String_System_IConvertible_ToByte_m6024 ();
extern "C" void String_System_IConvertible_ToChar_m6025 ();
extern "C" void String_System_IConvertible_ToDateTime_m6026 ();
extern "C" void String_System_IConvertible_ToDecimal_m6027 ();
extern "C" void String_System_IConvertible_ToDouble_m6028 ();
extern "C" void String_System_IConvertible_ToInt16_m6029 ();
extern "C" void String_System_IConvertible_ToInt32_m6030 ();
extern "C" void String_System_IConvertible_ToInt64_m6031 ();
extern "C" void String_System_IConvertible_ToSByte_m6032 ();
extern "C" void String_System_IConvertible_ToSingle_m6033 ();
extern "C" void String_System_IConvertible_ToType_m6034 ();
extern "C" void String_System_IConvertible_ToUInt16_m6035 ();
extern "C" void String_System_IConvertible_ToUInt32_m6036 ();
extern "C" void String_System_IConvertible_ToUInt64_m6037 ();
extern "C" void String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m6038 ();
extern "C" void String_System_Collections_IEnumerable_GetEnumerator_m6039 ();
extern "C" void String_Equals_m6040 ();
extern "C" void String_Equals_m6041 ();
extern "C" void String_Equals_m4667 ();
extern "C" void String_get_Chars_m2209 ();
extern "C" void String_Clone_m6042 ();
extern "C" void String_CopyTo_m6043 ();
extern "C" void String_ToCharArray_m5672 ();
extern "C" void String_ToCharArray_m6044 ();
extern "C" void String_Split_m3643 ();
extern "C" void String_Split_m6045 ();
extern "C" void String_Split_m6046 ();
extern "C" void String_Split_m6047 ();
extern "C" void String_Split_m5693 ();
extern "C" void String_Substring_m2236 ();
extern "C" void String_Substring_m2210 ();
extern "C" void String_SubstringUnchecked_m6048 ();
extern "C" void String_Trim_m3640 ();
extern "C" void String_Trim_m6049 ();
extern "C" void String_TrimStart_m5729 ();
extern "C" void String_TrimEnd_m5691 ();
extern "C" void String_FindNotWhiteSpace_m6050 ();
extern "C" void String_FindNotInTable_m6051 ();
extern "C" void String_Compare_m6052 ();
extern "C" void String_Compare_m6053 ();
extern "C" void String_Compare_m4762 ();
extern "C" void String_Compare_m4764 ();
extern "C" void String_CompareTo_m6054 ();
extern "C" void String_CompareTo_m6055 ();
extern "C" void String_CompareOrdinal_m6056 ();
extern "C" void String_CompareOrdinalUnchecked_m6057 ();
extern "C" void String_CompareOrdinalCaseInsensitiveUnchecked_m6058 ();
extern "C" void String_EndsWith_m3645 ();
extern "C" void String_IndexOfAny_m6059 ();
extern "C" void String_IndexOfAny_m2228 ();
extern "C" void String_IndexOfAny_m4675 ();
extern "C" void String_IndexOfAnyUnchecked_m6060 ();
extern "C" void String_IndexOf_m5704 ();
extern "C" void String_IndexOf_m6061 ();
extern "C" void String_IndexOfOrdinal_m6062 ();
extern "C" void String_IndexOfOrdinalUnchecked_m6063 ();
extern "C" void String_IndexOfOrdinalIgnoreCaseUnchecked_m6064 ();
extern "C" void String_IndexOf_m2259 ();
extern "C" void String_IndexOf_m4763 ();
extern "C" void String_IndexOf_m5730 ();
extern "C" void String_IndexOfUnchecked_m6065 ();
extern "C" void String_IndexOf_m3644 ();
extern "C" void String_IndexOf_m3646 ();
extern "C" void String_IndexOf_m6066 ();
extern "C" void String_LastIndexOfAny_m6067 ();
extern "C" void String_LastIndexOfAny_m2230 ();
extern "C" void String_LastIndexOfAnyUnchecked_m6068 ();
extern "C" void String_LastIndexOf_m5677 ();
extern "C" void String_LastIndexOf_m6069 ();
extern "C" void String_LastIndexOf_m5731 ();
extern "C" void String_LastIndexOfUnchecked_m6070 ();
extern "C" void String_LastIndexOf_m3649 ();
extern "C" void String_LastIndexOf_m6071 ();
extern "C" void String_Contains_m2253 ();
extern "C" void String_IsNullOrEmpty_m2238 ();
extern "C" void String_PadRight_m6072 ();
extern "C" void String_StartsWith_m3638 ();
extern "C" void String_Replace_m3648 ();
extern "C" void String_Replace_m3647 ();
extern "C" void String_ReplaceUnchecked_m6073 ();
extern "C" void String_ReplaceFallback_m6074 ();
extern "C" void String_Remove_m2232 ();
extern "C" void String_ToLower_m3615 ();
extern "C" void String_ToLower_m5728 ();
extern "C" void String_ToLowerInvariant_m6075 ();
extern "C" void String_ToString_m3637 ();
extern "C" void String_ToString_m6076 ();
extern "C" void String_Format_m1995 ();
extern "C" void String_Format_m6077 ();
extern "C" void String_Format_m6078 ();
extern "C" void String_Format_m2349 ();
extern "C" void String_Format_m4715 ();
extern "C" void String_FormatHelper_m6079 ();
extern "C" void String_Concat_m1870 ();
extern "C" void String_Concat_m1861 ();
extern "C" void String_Concat_m2080 ();
extern "C" void String_Concat_m262 ();
extern "C" void String_Concat_m3641 ();
extern "C" void String_Concat_m1901 ();
extern "C" void String_Concat_m5675 ();
extern "C" void String_ConcatInternal_m6080 ();
extern "C" void String_Insert_m2234 ();
extern "C" void String_Join_m6081 ();
extern "C" void String_Join_m6082 ();
extern "C" void String_JoinUnchecked_m6083 ();
extern "C" void String_get_Length_m2190 ();
extern "C" void String_ParseFormatSpecifier_m6084 ();
extern "C" void String_ParseDecimal_m6085 ();
extern "C" void String_InternalSetChar_m6086 ();
extern "C" void String_InternalSetLength_m6087 ();
extern "C" void String_GetHashCode_m3609 ();
extern "C" void String_GetCaseInsensitiveHashCode_m6088 ();
extern "C" void String_CreateString_m6089 ();
extern "C" void String_CreateString_m6090 ();
extern "C" void String_CreateString_m6091 ();
extern "C" void String_CreateString_m6092 ();
extern "C" void String_CreateString_m6093 ();
extern "C" void String_CreateString_m6094 ();
extern "C" void String_CreateString_m5721 ();
extern "C" void String_CreateString_m2237 ();
extern "C" void String_memcpy4_m6095 ();
extern "C" void String_memcpy2_m6096 ();
extern "C" void String_memcpy1_m6097 ();
extern "C" void String_memcpy_m6098 ();
extern "C" void String_CharCopy_m6099 ();
extern "C" void String_CharCopyReverse_m6100 ();
extern "C" void String_CharCopy_m6101 ();
extern "C" void String_CharCopy_m6102 ();
extern "C" void String_CharCopyReverse_m6103 ();
extern "C" void String_InternalSplit_m6104 ();
extern "C" void String_InternalAllocateStr_m6105 ();
extern "C" void String_op_Equality_m215 ();
extern "C" void String_op_Inequality_m2208 ();
extern "C" void Single_System_IConvertible_ToBoolean_m6106 ();
extern "C" void Single_System_IConvertible_ToByte_m6107 ();
extern "C" void Single_System_IConvertible_ToChar_m6108 ();
extern "C" void Single_System_IConvertible_ToDateTime_m6109 ();
extern "C" void Single_System_IConvertible_ToDecimal_m6110 ();
extern "C" void Single_System_IConvertible_ToDouble_m6111 ();
extern "C" void Single_System_IConvertible_ToInt16_m6112 ();
extern "C" void Single_System_IConvertible_ToInt32_m6113 ();
extern "C" void Single_System_IConvertible_ToInt64_m6114 ();
extern "C" void Single_System_IConvertible_ToSByte_m6115 ();
extern "C" void Single_System_IConvertible_ToSingle_m6116 ();
extern "C" void Single_System_IConvertible_ToType_m6117 ();
extern "C" void Single_System_IConvertible_ToUInt16_m6118 ();
extern "C" void Single_System_IConvertible_ToUInt32_m6119 ();
extern "C" void Single_System_IConvertible_ToUInt64_m6120 ();
extern "C" void Single_CompareTo_m6121 ();
extern "C" void Single_Equals_m6122 ();
extern "C" void Single_CompareTo_m1867 ();
extern "C" void Single_Equals_m2083 ();
extern "C" void Single_GetHashCode_m3593 ();
extern "C" void Single_IsInfinity_m6123 ();
extern "C" void Single_IsNaN_m6124 ();
extern "C" void Single_IsNegativeInfinity_m6125 ();
extern "C" void Single_IsPositiveInfinity_m6126 ();
extern "C" void Single_Parse_m6127 ();
extern "C" void Single_ToString_m6128 ();
extern "C" void Single_ToString_m6129 ();
extern "C" void Single_ToString_m3600 ();
extern "C" void Single_ToString_m6130 ();
extern "C" void Single_GetTypeCode_m6131 ();
extern "C" void Double_System_IConvertible_ToType_m6132 ();
extern "C" void Double_System_IConvertible_ToBoolean_m6133 ();
extern "C" void Double_System_IConvertible_ToByte_m6134 ();
extern "C" void Double_System_IConvertible_ToChar_m6135 ();
extern "C" void Double_System_IConvertible_ToDateTime_m6136 ();
extern "C" void Double_System_IConvertible_ToDecimal_m6137 ();
extern "C" void Double_System_IConvertible_ToDouble_m6138 ();
extern "C" void Double_System_IConvertible_ToInt16_m6139 ();
extern "C" void Double_System_IConvertible_ToInt32_m6140 ();
extern "C" void Double_System_IConvertible_ToInt64_m6141 ();
extern "C" void Double_System_IConvertible_ToSByte_m6142 ();
extern "C" void Double_System_IConvertible_ToSingle_m6143 ();
extern "C" void Double_System_IConvertible_ToUInt16_m6144 ();
extern "C" void Double_System_IConvertible_ToUInt32_m6145 ();
extern "C" void Double_System_IConvertible_ToUInt64_m6146 ();
extern "C" void Double_CompareTo_m6147 ();
extern "C" void Double_Equals_m6148 ();
extern "C" void Double_CompareTo_m6149 ();
extern "C" void Double_Equals_m6150 ();
extern "C" void Double_GetHashCode_m6151 ();
extern "C" void Double_IsInfinity_m6152 ();
extern "C" void Double_IsNaN_m6153 ();
extern "C" void Double_IsNegativeInfinity_m6154 ();
extern "C" void Double_IsPositiveInfinity_m6155 ();
extern "C" void Double_Parse_m6156 ();
extern "C" void Double_Parse_m6157 ();
extern "C" void Double_Parse_m6158 ();
extern "C" void Double_Parse_m6159 ();
extern "C" void Double_TryParseStringConstant_m6160 ();
extern "C" void Double_ParseImpl_m6161 ();
extern "C" void Double_ToString_m6162 ();
extern "C" void Double_ToString_m6163 ();
extern "C" void Double_ToString_m6164 ();
extern "C" void Decimal__ctor_m6165 ();
extern "C" void Decimal__ctor_m6166 ();
extern "C" void Decimal__ctor_m6167 ();
extern "C" void Decimal__ctor_m6168 ();
extern "C" void Decimal__ctor_m6169 ();
extern "C" void Decimal__ctor_m6170 ();
extern "C" void Decimal__ctor_m6171 ();
extern "C" void Decimal__cctor_m6172 ();
extern "C" void Decimal_System_IConvertible_ToType_m6173 ();
extern "C" void Decimal_System_IConvertible_ToBoolean_m6174 ();
extern "C" void Decimal_System_IConvertible_ToByte_m6175 ();
extern "C" void Decimal_System_IConvertible_ToChar_m6176 ();
extern "C" void Decimal_System_IConvertible_ToDateTime_m6177 ();
extern "C" void Decimal_System_IConvertible_ToDecimal_m6178 ();
extern "C" void Decimal_System_IConvertible_ToDouble_m6179 ();
extern "C" void Decimal_System_IConvertible_ToInt16_m6180 ();
extern "C" void Decimal_System_IConvertible_ToInt32_m6181 ();
extern "C" void Decimal_System_IConvertible_ToInt64_m6182 ();
extern "C" void Decimal_System_IConvertible_ToSByte_m6183 ();
extern "C" void Decimal_System_IConvertible_ToSingle_m6184 ();
extern "C" void Decimal_System_IConvertible_ToUInt16_m6185 ();
extern "C" void Decimal_System_IConvertible_ToUInt32_m6186 ();
extern "C" void Decimal_System_IConvertible_ToUInt64_m6187 ();
extern "C" void Decimal_GetBits_m6188 ();
extern "C" void Decimal_Add_m6189 ();
extern "C" void Decimal_Subtract_m6190 ();
extern "C" void Decimal_GetHashCode_m6191 ();
extern "C" void Decimal_u64_m6192 ();
extern "C" void Decimal_s64_m6193 ();
extern "C" void Decimal_Equals_m6194 ();
extern "C" void Decimal_Equals_m6195 ();
extern "C" void Decimal_IsZero_m6196 ();
extern "C" void Decimal_Floor_m6197 ();
extern "C" void Decimal_Multiply_m6198 ();
extern "C" void Decimal_Divide_m6199 ();
extern "C" void Decimal_Compare_m6200 ();
extern "C" void Decimal_CompareTo_m6201 ();
extern "C" void Decimal_CompareTo_m6202 ();
extern "C" void Decimal_Equals_m6203 ();
extern "C" void Decimal_Parse_m6204 ();
extern "C" void Decimal_ThrowAtPos_m6205 ();
extern "C" void Decimal_ThrowInvalidExp_m6206 ();
extern "C" void Decimal_stripStyles_m6207 ();
extern "C" void Decimal_Parse_m6208 ();
extern "C" void Decimal_PerformParse_m6209 ();
extern "C" void Decimal_ToString_m6210 ();
extern "C" void Decimal_ToString_m6211 ();
extern "C" void Decimal_ToString_m6212 ();
extern "C" void Decimal_decimal2UInt64_m6213 ();
extern "C" void Decimal_decimal2Int64_m6214 ();
extern "C" void Decimal_decimalIncr_m6215 ();
extern "C" void Decimal_string2decimal_m6216 ();
extern "C" void Decimal_decimalSetExponent_m6217 ();
extern "C" void Decimal_decimal2double_m6218 ();
extern "C" void Decimal_decimalFloorAndTrunc_m6219 ();
extern "C" void Decimal_decimalMult_m6220 ();
extern "C" void Decimal_decimalDiv_m6221 ();
extern "C" void Decimal_decimalCompare_m6222 ();
extern "C" void Decimal_op_Increment_m6223 ();
extern "C" void Decimal_op_Subtraction_m6224 ();
extern "C" void Decimal_op_Multiply_m6225 ();
extern "C" void Decimal_op_Division_m6226 ();
extern "C" void Decimal_op_Explicit_m6227 ();
extern "C" void Decimal_op_Explicit_m6228 ();
extern "C" void Decimal_op_Explicit_m6229 ();
extern "C" void Decimal_op_Explicit_m6230 ();
extern "C" void Decimal_op_Explicit_m6231 ();
extern "C" void Decimal_op_Explicit_m6232 ();
extern "C" void Decimal_op_Explicit_m6233 ();
extern "C" void Decimal_op_Explicit_m6234 ();
extern "C" void Decimal_op_Implicit_m6235 ();
extern "C" void Decimal_op_Implicit_m6236 ();
extern "C" void Decimal_op_Implicit_m6237 ();
extern "C" void Decimal_op_Implicit_m6238 ();
extern "C" void Decimal_op_Implicit_m6239 ();
extern "C" void Decimal_op_Implicit_m6240 ();
extern "C" void Decimal_op_Implicit_m6241 ();
extern "C" void Decimal_op_Implicit_m6242 ();
extern "C" void Decimal_op_Explicit_m6243 ();
extern "C" void Decimal_op_Explicit_m6244 ();
extern "C" void Decimal_op_Explicit_m6245 ();
extern "C" void Decimal_op_Explicit_m6246 ();
extern "C" void Decimal_op_Inequality_m6247 ();
extern "C" void Decimal_op_Equality_m6248 ();
extern "C" void Decimal_op_GreaterThan_m6249 ();
extern "C" void Decimal_op_LessThan_m6250 ();
extern "C" void Boolean__cctor_m6251 ();
extern "C" void Boolean_System_IConvertible_ToType_m6252 ();
extern "C" void Boolean_System_IConvertible_ToBoolean_m6253 ();
extern "C" void Boolean_System_IConvertible_ToByte_m6254 ();
extern "C" void Boolean_System_IConvertible_ToChar_m6255 ();
extern "C" void Boolean_System_IConvertible_ToDateTime_m6256 ();
extern "C" void Boolean_System_IConvertible_ToDecimal_m6257 ();
extern "C" void Boolean_System_IConvertible_ToDouble_m6258 ();
extern "C" void Boolean_System_IConvertible_ToInt16_m6259 ();
extern "C" void Boolean_System_IConvertible_ToInt32_m6260 ();
extern "C" void Boolean_System_IConvertible_ToInt64_m6261 ();
extern "C" void Boolean_System_IConvertible_ToSByte_m6262 ();
extern "C" void Boolean_System_IConvertible_ToSingle_m6263 ();
extern "C" void Boolean_System_IConvertible_ToUInt16_m6264 ();
extern "C" void Boolean_System_IConvertible_ToUInt32_m6265 ();
extern "C" void Boolean_System_IConvertible_ToUInt64_m6266 ();
extern "C" void Boolean_CompareTo_m6267 ();
extern "C" void Boolean_Equals_m6268 ();
extern "C" void Boolean_CompareTo_m6269 ();
extern "C" void Boolean_Equals_m6270 ();
extern "C" void Boolean_GetHashCode_m6271 ();
extern "C" void Boolean_Parse_m6272 ();
extern "C" void Boolean_ToString_m6273 ();
extern "C" void Boolean_GetTypeCode_m6274 ();
extern "C" void Boolean_ToString_m6275 ();
extern "C" void IntPtr__ctor_m3603 ();
extern "C" void IntPtr__ctor_m6276 ();
extern "C" void IntPtr__ctor_m6277 ();
extern "C" void IntPtr__ctor_m6278 ();
extern "C" void IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6279 ();
extern "C" void IntPtr_get_Size_m6280 ();
extern "C" void IntPtr_Equals_m6281 ();
extern "C" void IntPtr_GetHashCode_m6282 ();
extern "C" void IntPtr_ToInt64_m6283 ();
extern "C" void IntPtr_ToPointer_m3596 ();
extern "C" void IntPtr_ToString_m6284 ();
extern "C" void IntPtr_ToString_m6285 ();
extern "C" void IntPtr_op_Equality_m3655 ();
extern "C" void IntPtr_op_Inequality_m3595 ();
extern "C" void IntPtr_op_Explicit_m6286 ();
extern "C" void IntPtr_op_Explicit_m6287 ();
extern "C" void IntPtr_op_Explicit_m6288 ();
extern "C" void IntPtr_op_Explicit_m3654 ();
extern "C" void IntPtr_op_Explicit_m6289 ();
extern "C" void UIntPtr__ctor_m6290 ();
extern "C" void UIntPtr__ctor_m6291 ();
extern "C" void UIntPtr__ctor_m6292 ();
extern "C" void UIntPtr__cctor_m6293 ();
extern "C" void UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6294 ();
extern "C" void UIntPtr_Equals_m6295 ();
extern "C" void UIntPtr_GetHashCode_m6296 ();
extern "C" void UIntPtr_ToUInt32_m6297 ();
extern "C" void UIntPtr_ToUInt64_m6298 ();
extern "C" void UIntPtr_ToPointer_m6299 ();
extern "C" void UIntPtr_ToString_m6300 ();
extern "C" void UIntPtr_get_Size_m6301 ();
extern "C" void UIntPtr_op_Equality_m6302 ();
extern "C" void UIntPtr_op_Inequality_m6303 ();
extern "C" void UIntPtr_op_Explicit_m6304 ();
extern "C" void UIntPtr_op_Explicit_m6305 ();
extern "C" void UIntPtr_op_Explicit_m6306 ();
extern "C" void UIntPtr_op_Explicit_m6307 ();
extern "C" void UIntPtr_op_Explicit_m6308 ();
extern "C" void UIntPtr_op_Explicit_m6309 ();
extern "C" void MulticastDelegate_GetObjectData_m6310 ();
extern "C" void MulticastDelegate_Equals_m6311 ();
extern "C" void MulticastDelegate_GetHashCode_m6312 ();
extern "C" void MulticastDelegate_GetInvocationList_m6313 ();
extern "C" void MulticastDelegate_CombineImpl_m6314 ();
extern "C" void MulticastDelegate_BaseEquals_m6315 ();
extern "C" void MulticastDelegate_KPM_m6316 ();
extern "C" void MulticastDelegate_RemoveImpl_m6317 ();
extern "C" void Delegate_get_Method_m3676 ();
extern "C" void Delegate_get_Target_m3657 ();
extern "C" void Delegate_CreateDelegate_internal_m6318 ();
extern "C" void Delegate_SetMulticastInvoke_m6319 ();
extern "C" void Delegate_arg_type_match_m6320 ();
extern "C" void Delegate_return_type_match_m6321 ();
extern "C" void Delegate_CreateDelegate_m6322 ();
extern "C" void Delegate_CreateDelegate_m3675 ();
extern "C" void Delegate_CreateDelegate_m6323 ();
extern "C" void Delegate_CreateDelegate_m6324 ();
extern "C" void Delegate_GetCandidateMethod_m6325 ();
extern "C" void Delegate_CreateDelegate_m6326 ();
extern "C" void Delegate_CreateDelegate_m6327 ();
extern "C" void Delegate_CreateDelegate_m6328 ();
extern "C" void Delegate_CreateDelegate_m6329 ();
extern "C" void Delegate_Clone_m6330 ();
extern "C" void Delegate_Equals_m6331 ();
extern "C" void Delegate_GetHashCode_m6332 ();
extern "C" void Delegate_GetObjectData_m6333 ();
extern "C" void Delegate_GetInvocationList_m6334 ();
extern "C" void Delegate_Combine_m2128 ();
extern "C" void Delegate_Combine_m6335 ();
extern "C" void Delegate_CombineImpl_m6336 ();
extern "C" void Delegate_Remove_m2129 ();
extern "C" void Delegate_RemoveImpl_m6337 ();
extern "C" void Enum__ctor_m6338 ();
extern "C" void Enum__cctor_m6339 ();
extern "C" void Enum_System_IConvertible_ToBoolean_m6340 ();
extern "C" void Enum_System_IConvertible_ToByte_m6341 ();
extern "C" void Enum_System_IConvertible_ToChar_m6342 ();
extern "C" void Enum_System_IConvertible_ToDateTime_m6343 ();
extern "C" void Enum_System_IConvertible_ToDecimal_m6344 ();
extern "C" void Enum_System_IConvertible_ToDouble_m6345 ();
extern "C" void Enum_System_IConvertible_ToInt16_m6346 ();
extern "C" void Enum_System_IConvertible_ToInt32_m6347 ();
extern "C" void Enum_System_IConvertible_ToInt64_m6348 ();
extern "C" void Enum_System_IConvertible_ToSByte_m6349 ();
extern "C" void Enum_System_IConvertible_ToSingle_m6350 ();
extern "C" void Enum_System_IConvertible_ToType_m6351 ();
extern "C" void Enum_System_IConvertible_ToUInt16_m6352 ();
extern "C" void Enum_System_IConvertible_ToUInt32_m6353 ();
extern "C" void Enum_System_IConvertible_ToUInt64_m6354 ();
extern "C" void Enum_GetTypeCode_m6355 ();
extern "C" void Enum_get_value_m6356 ();
extern "C" void Enum_get_Value_m6357 ();
extern "C" void Enum_FindPosition_m6358 ();
extern "C" void Enum_GetName_m6359 ();
extern "C" void Enum_IsDefined_m4740 ();
extern "C" void Enum_get_underlying_type_m6360 ();
extern "C" void Enum_GetUnderlyingType_m6361 ();
extern "C" void Enum_FindName_m6362 ();
extern "C" void Enum_GetValue_m6363 ();
extern "C" void Enum_Parse_m3616 ();
extern "C" void Enum_compare_value_to_m6364 ();
extern "C" void Enum_CompareTo_m6365 ();
extern "C" void Enum_ToString_m6366 ();
extern "C" void Enum_ToString_m6367 ();
extern "C" void Enum_ToString_m6368 ();
extern "C" void Enum_ToString_m6369 ();
extern "C" void Enum_ToObject_m6370 ();
extern "C" void Enum_ToObject_m6371 ();
extern "C" void Enum_ToObject_m6372 ();
extern "C" void Enum_ToObject_m6373 ();
extern "C" void Enum_ToObject_m6374 ();
extern "C" void Enum_ToObject_m6375 ();
extern "C" void Enum_ToObject_m6376 ();
extern "C" void Enum_ToObject_m6377 ();
extern "C" void Enum_ToObject_m6378 ();
extern "C" void Enum_Equals_m6379 ();
extern "C" void Enum_get_hashcode_m6380 ();
extern "C" void Enum_GetHashCode_m6381 ();
extern "C" void Enum_FormatSpecifier_X_m6382 ();
extern "C" void Enum_FormatFlags_m6383 ();
extern "C" void Enum_Format_m6384 ();
extern "C" void SimpleEnumerator__ctor_m6385 ();
extern "C" void SimpleEnumerator_get_Current_m6386 ();
extern "C" void SimpleEnumerator_MoveNext_m6387 ();
extern "C" void SimpleEnumerator_Reset_m6388 ();
extern "C" void SimpleEnumerator_Clone_m6389 ();
extern "C" void Swapper__ctor_m6390 ();
extern "C" void Swapper_Invoke_m6391 ();
extern "C" void Swapper_BeginInvoke_m6392 ();
extern "C" void Swapper_EndInvoke_m6393 ();
extern "C" void Array__ctor_m6394 ();
extern "C" void Array_System_Collections_IList_get_Item_m6395 ();
extern "C" void Array_System_Collections_IList_set_Item_m6396 ();
extern "C" void Array_System_Collections_IList_Add_m6397 ();
extern "C" void Array_System_Collections_IList_Clear_m6398 ();
extern "C" void Array_System_Collections_IList_Contains_m6399 ();
extern "C" void Array_System_Collections_IList_IndexOf_m6400 ();
extern "C" void Array_System_Collections_IList_Insert_m6401 ();
extern "C" void Array_System_Collections_IList_Remove_m6402 ();
extern "C" void Array_System_Collections_IList_RemoveAt_m6403 ();
extern "C" void Array_System_Collections_ICollection_get_Count_m6404 ();
extern "C" void Array_InternalArray__ICollection_get_Count_m6405 ();
extern "C" void Array_InternalArray__ICollection_get_IsReadOnly_m6406 ();
extern "C" void Array_InternalArray__ICollection_Clear_m6407 ();
extern "C" void Array_InternalArray__RemoveAt_m6408 ();
extern "C" void Array_get_Length_m5654 ();
extern "C" void Array_get_LongLength_m6409 ();
extern "C" void Array_get_Rank_m5657 ();
extern "C" void Array_GetRank_m6410 ();
extern "C" void Array_GetLength_m6411 ();
extern "C" void Array_GetLongLength_m6412 ();
extern "C" void Array_GetLowerBound_m6413 ();
extern "C" void Array_GetValue_m6414 ();
extern "C" void Array_SetValue_m6415 ();
extern "C" void Array_GetValueImpl_m6416 ();
extern "C" void Array_SetValueImpl_m6417 ();
extern "C" void Array_FastCopy_m6418 ();
extern "C" void Array_CreateInstanceImpl_m6419 ();
extern "C" void Array_get_IsSynchronized_m6420 ();
extern "C" void Array_get_SyncRoot_m6421 ();
extern "C" void Array_get_IsFixedSize_m6422 ();
extern "C" void Array_get_IsReadOnly_m6423 ();
extern "C" void Array_GetEnumerator_m6424 ();
extern "C" void Array_GetUpperBound_m6425 ();
extern "C" void Array_GetValue_m6426 ();
extern "C" void Array_GetValue_m6427 ();
extern "C" void Array_GetValue_m6428 ();
extern "C" void Array_GetValue_m6429 ();
extern "C" void Array_GetValue_m6430 ();
extern "C" void Array_GetValue_m6431 ();
extern "C" void Array_SetValue_m6432 ();
extern "C" void Array_SetValue_m6433 ();
extern "C" void Array_SetValue_m6434 ();
extern "C" void Array_SetValue_m5655 ();
extern "C" void Array_SetValue_m6435 ();
extern "C" void Array_SetValue_m6436 ();
extern "C" void Array_CreateInstance_m6437 ();
extern "C" void Array_CreateInstance_m6438 ();
extern "C" void Array_CreateInstance_m6439 ();
extern "C" void Array_CreateInstance_m6440 ();
extern "C" void Array_CreateInstance_m6441 ();
extern "C" void Array_GetIntArray_m6442 ();
extern "C" void Array_CreateInstance_m6443 ();
extern "C" void Array_GetValue_m6444 ();
extern "C" void Array_SetValue_m6445 ();
extern "C" void Array_BinarySearch_m6446 ();
extern "C" void Array_BinarySearch_m6447 ();
extern "C" void Array_BinarySearch_m6448 ();
extern "C" void Array_BinarySearch_m6449 ();
extern "C" void Array_DoBinarySearch_m6450 ();
extern "C" void Array_Clear_m3729 ();
extern "C" void Array_ClearInternal_m6451 ();
extern "C" void Array_Clone_m6452 ();
extern "C" void Array_Copy_m5722 ();
extern "C" void Array_Copy_m6453 ();
extern "C" void Array_Copy_m6454 ();
extern "C" void Array_Copy_m6455 ();
extern "C" void Array_IndexOf_m6456 ();
extern "C" void Array_IndexOf_m6457 ();
extern "C" void Array_IndexOf_m6458 ();
extern "C" void Array_Initialize_m6459 ();
extern "C" void Array_LastIndexOf_m6460 ();
extern "C" void Array_LastIndexOf_m6461 ();
extern "C" void Array_LastIndexOf_m6462 ();
extern "C" void Array_get_swapper_m6463 ();
extern "C" void Array_Reverse_m4641 ();
extern "C" void Array_Reverse_m4677 ();
extern "C" void Array_Sort_m6464 ();
extern "C" void Array_Sort_m6465 ();
extern "C" void Array_Sort_m6466 ();
extern "C" void Array_Sort_m6467 ();
extern "C" void Array_Sort_m6468 ();
extern "C" void Array_Sort_m6469 ();
extern "C" void Array_Sort_m6470 ();
extern "C" void Array_Sort_m6471 ();
extern "C" void Array_int_swapper_m6472 ();
extern "C" void Array_obj_swapper_m6473 ();
extern "C" void Array_slow_swapper_m6474 ();
extern "C" void Array_double_swapper_m6475 ();
extern "C" void Array_new_gap_m6476 ();
extern "C" void Array_combsort_m6477 ();
extern "C" void Array_combsort_m6478 ();
extern "C" void Array_combsort_m6479 ();
extern "C" void Array_qsort_m6480 ();
extern "C" void Array_swap_m6481 ();
extern "C" void Array_compare_m6482 ();
extern "C" void Array_CopyTo_m6483 ();
extern "C" void Array_CopyTo_m6484 ();
extern "C" void Array_ConstrainedCopy_m6485 ();
extern "C" void Type__ctor_m6486 ();
extern "C" void Type__cctor_m6487 ();
extern "C" void Type_FilterName_impl_m6488 ();
extern "C" void Type_FilterNameIgnoreCase_impl_m6489 ();
extern "C" void Type_FilterAttribute_impl_m6490 ();
extern "C" void Type_get_Attributes_m6491 ();
extern "C" void Type_get_DeclaringType_m6492 ();
extern "C" void Type_get_HasElementType_m6493 ();
extern "C" void Type_get_IsAbstract_m6494 ();
extern "C" void Type_get_IsArray_m6495 ();
extern "C" void Type_get_IsByRef_m6496 ();
extern "C" void Type_get_IsClass_m6497 ();
extern "C" void Type_get_IsContextful_m6498 ();
extern "C" void Type_get_IsEnum_m6499 ();
extern "C" void Type_get_IsExplicitLayout_m6500 ();
extern "C" void Type_get_IsInterface_m6501 ();
extern "C" void Type_get_IsMarshalByRef_m6502 ();
extern "C" void Type_get_IsPointer_m6503 ();
extern "C" void Type_get_IsPrimitive_m6504 ();
extern "C" void Type_get_IsSealed_m6505 ();
extern "C" void Type_get_IsSerializable_m6506 ();
extern "C" void Type_get_IsValueType_m6507 ();
extern "C" void Type_get_MemberType_m6508 ();
extern "C" void Type_get_ReflectedType_m6509 ();
extern "C" void Type_get_TypeHandle_m6510 ();
extern "C" void Type_Equals_m6511 ();
extern "C" void Type_Equals_m6512 ();
extern "C" void Type_EqualsInternal_m6513 ();
extern "C" void Type_internal_from_handle_m6514 ();
extern "C" void Type_internal_from_name_m6515 ();
extern "C" void Type_GetType_m6516 ();
extern "C" void Type_GetType_m3662 ();
extern "C" void Type_GetTypeCodeInternal_m6517 ();
extern "C" void Type_GetTypeCode_m6518 ();
extern "C" void Type_GetTypeFromHandle_m213 ();
extern "C" void Type_GetTypeHandle_m6519 ();
extern "C" void Type_type_is_subtype_of_m6520 ();
extern "C" void Type_type_is_assignable_from_m6521 ();
extern "C" void Type_IsSubclassOf_m6522 ();
extern "C" void Type_IsAssignableFrom_m6523 ();
extern "C" void Type_IsInstanceOfType_m6524 ();
extern "C" void Type_GetHashCode_m6525 ();
extern "C" void Type_GetMethod_m6526 ();
extern "C" void Type_GetMethod_m6527 ();
extern "C" void Type_GetMethod_m6528 ();
extern "C" void Type_GetMethod_m6529 ();
extern "C" void Type_GetProperty_m6530 ();
extern "C" void Type_GetProperty_m6531 ();
extern "C" void Type_GetProperty_m6532 ();
extern "C" void Type_GetProperty_m6533 ();
extern "C" void Type_IsArrayImpl_m6534 ();
extern "C" void Type_IsValueTypeImpl_m6535 ();
extern "C" void Type_IsContextfulImpl_m6536 ();
extern "C" void Type_IsMarshalByRefImpl_m6537 ();
extern "C" void Type_GetConstructor_m6538 ();
extern "C" void Type_GetConstructor_m6539 ();
extern "C" void Type_GetConstructor_m6540 ();
extern "C" void Type_ToString_m6541 ();
extern "C" void Type_get_IsSystemType_m6542 ();
extern "C" void Type_GetGenericArguments_m6543 ();
extern "C" void Type_get_ContainsGenericParameters_m6544 ();
extern "C" void Type_get_IsGenericTypeDefinition_m6545 ();
extern "C" void Type_GetGenericTypeDefinition_impl_m6546 ();
extern "C" void Type_GetGenericTypeDefinition_m6547 ();
extern "C" void Type_get_IsGenericType_m6548 ();
extern "C" void Type_MakeGenericType_m6549 ();
extern "C" void Type_MakeGenericType_m6550 ();
extern "C" void Type_get_IsGenericParameter_m6551 ();
extern "C" void Type_get_IsNested_m6552 ();
extern "C" void Type_GetPseudoCustomAttributes_m6553 ();
extern "C" void MemberInfo__ctor_m6554 ();
extern "C" void MemberInfo_get_Module_m6555 ();
extern "C" void Exception__ctor_m4631 ();
extern "C" void Exception__ctor_m244 ();
extern "C" void Exception__ctor_m3653 ();
extern "C" void Exception__ctor_m3652 ();
extern "C" void Exception_get_InnerException_m6556 ();
extern "C" void Exception_set_HResult_m3651 ();
extern "C" void Exception_get_ClassName_m6557 ();
extern "C" void Exception_get_Message_m6558 ();
extern "C" void Exception_get_Source_m6559 ();
extern "C" void Exception_get_StackTrace_m6560 ();
extern "C" void Exception_GetObjectData_m5735 ();
extern "C" void Exception_ToString_m6561 ();
extern "C" void Exception_GetFullNameForStackTrace_m6562 ();
extern "C" void Exception_GetType_m6563 ();
extern "C" void RuntimeFieldHandle__ctor_m6564 ();
extern "C" void RuntimeFieldHandle_get_Value_m6565 ();
extern "C" void RuntimeFieldHandle_GetObjectData_m6566 ();
extern "C" void RuntimeFieldHandle_Equals_m6567 ();
extern "C" void RuntimeFieldHandle_GetHashCode_m6568 ();
extern "C" void RuntimeTypeHandle__ctor_m6569 ();
extern "C" void RuntimeTypeHandle_get_Value_m6570 ();
extern "C" void RuntimeTypeHandle_GetObjectData_m6571 ();
extern "C" void RuntimeTypeHandle_Equals_m6572 ();
extern "C" void RuntimeTypeHandle_GetHashCode_m6573 ();
extern "C" void ParamArrayAttribute__ctor_m6574 ();
extern "C" void OutAttribute__ctor_m6575 ();
extern "C" void ObsoleteAttribute__ctor_m6576 ();
extern "C" void ObsoleteAttribute__ctor_m6577 ();
extern "C" void ObsoleteAttribute__ctor_m6578 ();
extern "C" void DllImportAttribute__ctor_m6579 ();
extern "C" void DllImportAttribute_get_Value_m6580 ();
extern "C" void MarshalAsAttribute__ctor_m6581 ();
extern "C" void InAttribute__ctor_m6582 ();
extern "C" void GuidAttribute__ctor_m6583 ();
extern "C" void ComImportAttribute__ctor_m6584 ();
extern "C" void OptionalAttribute__ctor_m6585 ();
extern "C" void CompilerGeneratedAttribute__ctor_m6586 ();
extern "C" void InternalsVisibleToAttribute__ctor_m6587 ();
extern "C" void RuntimeCompatibilityAttribute__ctor_m6588 ();
extern "C" void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m6589 ();
extern "C" void DebuggerHiddenAttribute__ctor_m6590 ();
extern "C" void DefaultMemberAttribute__ctor_m6591 ();
extern "C" void DefaultMemberAttribute_get_MemberName_m6592 ();
extern "C" void DecimalConstantAttribute__ctor_m6593 ();
extern "C" void FieldOffsetAttribute__ctor_m6594 ();
extern "C" void AsyncCallback__ctor_m4739 ();
extern "C" void AsyncCallback_Invoke_m6595 ();
extern "C" void AsyncCallback_BeginInvoke_m4737 ();
extern "C" void AsyncCallback_EndInvoke_m6596 ();
extern "C" void TypedReference_Equals_m6597 ();
extern "C" void TypedReference_GetHashCode_m6598 ();
extern "C" void ArgIterator_Equals_m6599 ();
extern "C" void ArgIterator_GetHashCode_m6600 ();
extern "C" void MarshalByRefObject__ctor_m5686 ();
extern "C" void MarshalByRefObject_get_ObjectIdentity_m6601 ();
extern "C" void RuntimeHelpers_InitializeArray_m6602 ();
extern "C" void RuntimeHelpers_InitializeArray_m3744 ();
extern "C" void RuntimeHelpers_get_OffsetToStringData_m6603 ();
extern "C" void Locale_GetText_m6604 ();
extern "C" void Locale_GetText_m6605 ();
extern "C" void MonoTODOAttribute__ctor_m6606 ();
extern "C" void MonoTODOAttribute__ctor_m6607 ();
extern "C" void MonoDocumentationNoteAttribute__ctor_m6608 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid__ctor_m6609 ();
extern "C" void SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m6610 ();
extern "C" void SafeWaitHandle__ctor_m6611 ();
extern "C" void SafeWaitHandle_ReleaseHandle_m6612 ();
extern "C" void TableRange__ctor_m6613 ();
extern "C" void CodePointIndexer__ctor_m6614 ();
extern "C" void CodePointIndexer_ToIndex_m6615 ();
extern "C" void TailoringInfo__ctor_m6616 ();
extern "C" void Contraction__ctor_m6617 ();
extern "C" void ContractionComparer__ctor_m6618 ();
extern "C" void ContractionComparer__cctor_m6619 ();
extern "C" void ContractionComparer_Compare_m6620 ();
extern "C" void Level2Map__ctor_m6621 ();
extern "C" void Level2MapComparer__ctor_m6622 ();
extern "C" void Level2MapComparer__cctor_m6623 ();
extern "C" void Level2MapComparer_Compare_m6624 ();
extern "C" void MSCompatUnicodeTable__cctor_m6625 ();
extern "C" void MSCompatUnicodeTable_GetTailoringInfo_m6626 ();
extern "C" void MSCompatUnicodeTable_BuildTailoringTables_m6627 ();
extern "C" void MSCompatUnicodeTable_SetCJKReferences_m6628 ();
extern "C" void MSCompatUnicodeTable_Category_m6629 ();
extern "C" void MSCompatUnicodeTable_Level1_m6630 ();
extern "C" void MSCompatUnicodeTable_Level2_m6631 ();
extern "C" void MSCompatUnicodeTable_Level3_m6632 ();
extern "C" void MSCompatUnicodeTable_IsIgnorable_m6633 ();
extern "C" void MSCompatUnicodeTable_IsIgnorableNonSpacing_m6634 ();
extern "C" void MSCompatUnicodeTable_ToKanaTypeInsensitive_m6635 ();
extern "C" void MSCompatUnicodeTable_ToWidthCompat_m6636 ();
extern "C" void MSCompatUnicodeTable_HasSpecialWeight_m6637 ();
extern "C" void MSCompatUnicodeTable_IsHalfWidthKana_m6638 ();
extern "C" void MSCompatUnicodeTable_IsHiragana_m6639 ();
extern "C" void MSCompatUnicodeTable_IsJapaneseSmallLetter_m6640 ();
extern "C" void MSCompatUnicodeTable_get_IsReady_m6641 ();
extern "C" void MSCompatUnicodeTable_GetResource_m6642 ();
extern "C" void MSCompatUnicodeTable_UInt32FromBytePtr_m6643 ();
extern "C" void MSCompatUnicodeTable_FillCJK_m6644 ();
extern "C" void MSCompatUnicodeTable_FillCJKCore_m6645 ();
extern "C" void MSCompatUnicodeTableUtil__cctor_m6646 ();
extern "C" void Context__ctor_m6647 ();
extern "C" void PreviousInfo__ctor_m6648 ();
extern "C" void SimpleCollator__ctor_m6649 ();
extern "C" void SimpleCollator__cctor_m6650 ();
extern "C" void SimpleCollator_SetCJKTable_m6651 ();
extern "C" void SimpleCollator_GetNeutralCulture_m6652 ();
extern "C" void SimpleCollator_Category_m6653 ();
extern "C" void SimpleCollator_Level1_m6654 ();
extern "C" void SimpleCollator_Level2_m6655 ();
extern "C" void SimpleCollator_IsHalfKana_m6656 ();
extern "C" void SimpleCollator_GetContraction_m6657 ();
extern "C" void SimpleCollator_GetContraction_m6658 ();
extern "C" void SimpleCollator_GetTailContraction_m6659 ();
extern "C" void SimpleCollator_GetTailContraction_m6660 ();
extern "C" void SimpleCollator_FilterOptions_m6661 ();
extern "C" void SimpleCollator_GetExtenderType_m6662 ();
extern "C" void SimpleCollator_ToDashTypeValue_m6663 ();
extern "C" void SimpleCollator_FilterExtender_m6664 ();
extern "C" void SimpleCollator_IsIgnorable_m6665 ();
extern "C" void SimpleCollator_IsSafe_m6666 ();
extern "C" void SimpleCollator_GetSortKey_m6667 ();
extern "C" void SimpleCollator_GetSortKey_m6668 ();
extern "C" void SimpleCollator_GetSortKey_m6669 ();
extern "C" void SimpleCollator_FillSortKeyRaw_m6670 ();
extern "C" void SimpleCollator_FillSurrogateSortKeyRaw_m6671 ();
extern "C" void SimpleCollator_CompareOrdinal_m6672 ();
extern "C" void SimpleCollator_CompareQuick_m6673 ();
extern "C" void SimpleCollator_CompareOrdinalIgnoreCase_m6674 ();
extern "C" void SimpleCollator_Compare_m6675 ();
extern "C" void SimpleCollator_ClearBuffer_m6676 ();
extern "C" void SimpleCollator_QuickCheckPossible_m6677 ();
extern "C" void SimpleCollator_CompareInternal_m6678 ();
extern "C" void SimpleCollator_CompareFlagPair_m6679 ();
extern "C" void SimpleCollator_IsPrefix_m6680 ();
extern "C" void SimpleCollator_IsPrefix_m6681 ();
extern "C" void SimpleCollator_IsPrefix_m6682 ();
extern "C" void SimpleCollator_IsSuffix_m6683 ();
extern "C" void SimpleCollator_IsSuffix_m6684 ();
extern "C" void SimpleCollator_QuickIndexOf_m6685 ();
extern "C" void SimpleCollator_IndexOf_m6686 ();
extern "C" void SimpleCollator_IndexOfOrdinal_m6687 ();
extern "C" void SimpleCollator_IndexOfOrdinalIgnoreCase_m6688 ();
extern "C" void SimpleCollator_IndexOfSortKey_m6689 ();
extern "C" void SimpleCollator_IndexOf_m6690 ();
extern "C" void SimpleCollator_LastIndexOf_m6691 ();
extern "C" void SimpleCollator_LastIndexOfOrdinal_m6692 ();
extern "C" void SimpleCollator_LastIndexOfOrdinalIgnoreCase_m6693 ();
extern "C" void SimpleCollator_LastIndexOfSortKey_m6694 ();
extern "C" void SimpleCollator_LastIndexOf_m6695 ();
extern "C" void SimpleCollator_MatchesForward_m6696 ();
extern "C" void SimpleCollator_MatchesForwardCore_m6697 ();
extern "C" void SimpleCollator_MatchesPrimitive_m6698 ();
extern "C" void SimpleCollator_MatchesBackward_m6699 ();
extern "C" void SimpleCollator_MatchesBackwardCore_m6700 ();
extern "C" void SortKey__ctor_m6701 ();
extern "C" void SortKey__ctor_m6702 ();
extern "C" void SortKey_Compare_m6703 ();
extern "C" void SortKey_get_OriginalString_m6704 ();
extern "C" void SortKey_get_KeyData_m6705 ();
extern "C" void SortKey_Equals_m6706 ();
extern "C" void SortKey_GetHashCode_m6707 ();
extern "C" void SortKey_ToString_m6708 ();
extern "C" void SortKeyBuffer__ctor_m6709 ();
extern "C" void SortKeyBuffer_Reset_m6710 ();
extern "C" void SortKeyBuffer_Initialize_m6711 ();
extern "C" void SortKeyBuffer_AppendCJKExtension_m6712 ();
extern "C" void SortKeyBuffer_AppendKana_m6713 ();
extern "C" void SortKeyBuffer_AppendNormal_m6714 ();
extern "C" void SortKeyBuffer_AppendLevel5_m6715 ();
extern "C" void SortKeyBuffer_AppendBufferPrimitive_m6716 ();
extern "C" void SortKeyBuffer_GetResultAndReset_m6717 ();
extern "C" void SortKeyBuffer_GetOptimizedLength_m6718 ();
extern "C" void SortKeyBuffer_GetResult_m6719 ();
extern "C" void PrimeGeneratorBase__ctor_m6720 ();
extern "C" void PrimeGeneratorBase_get_Confidence_m6721 ();
extern "C" void PrimeGeneratorBase_get_PrimalityTest_m6722 ();
extern "C" void PrimeGeneratorBase_get_TrialDivisionBounds_m6723 ();
extern "C" void SequentialSearchPrimeGeneratorBase__ctor_m6724 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m6725 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6726 ();
extern "C" void SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6727 ();
extern "C" void SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m6728 ();
extern "C" void PrimalityTests_GetSPPRounds_m6729 ();
extern "C" void PrimalityTests_Test_m6730 ();
extern "C" void PrimalityTests_RabinMillerTest_m6731 ();
extern "C" void PrimalityTests_SmallPrimeSppTest_m6732 ();
extern "C" void ModulusRing__ctor_m6733 ();
extern "C" void ModulusRing_BarrettReduction_m6734 ();
extern "C" void ModulusRing_Multiply_m6735 ();
extern "C" void ModulusRing_Difference_m6736 ();
extern "C" void ModulusRing_Pow_m6737 ();
extern "C" void ModulusRing_Pow_m6738 ();
extern "C" void Kernel_AddSameSign_m6739 ();
extern "C" void Kernel_Subtract_m6740 ();
extern "C" void Kernel_MinusEq_m6741 ();
extern "C" void Kernel_PlusEq_m6742 ();
extern "C" void Kernel_Compare_m6743 ();
extern "C" void Kernel_SingleByteDivideInPlace_m6744 ();
extern "C" void Kernel_DwordMod_m6745 ();
extern "C" void Kernel_DwordDivMod_m6746 ();
extern "C" void Kernel_multiByteDivide_m6747 ();
extern "C" void Kernel_LeftShift_m6748 ();
extern "C" void Kernel_RightShift_m6749 ();
extern "C" void Kernel_MultiplyByDword_m6750 ();
extern "C" void Kernel_Multiply_m6751 ();
extern "C" void Kernel_MultiplyMod2p32pmod_m6752 ();
extern "C" void Kernel_modInverse_m6753 ();
extern "C" void Kernel_modInverse_m6754 ();
extern "C" void BigInteger__ctor_m6755 ();
extern "C" void BigInteger__ctor_m6756 ();
extern "C" void BigInteger__ctor_m6757 ();
extern "C" void BigInteger__ctor_m6758 ();
extern "C" void BigInteger__ctor_m6759 ();
extern "C" void BigInteger__cctor_m6760 ();
extern "C" void BigInteger_get_Rng_m6761 ();
extern "C" void BigInteger_GenerateRandom_m6762 ();
extern "C" void BigInteger_GenerateRandom_m6763 ();
extern "C" void BigInteger_Randomize_m6764 ();
extern "C" void BigInteger_Randomize_m6765 ();
extern "C" void BigInteger_BitCount_m6766 ();
extern "C" void BigInteger_TestBit_m6767 ();
extern "C" void BigInteger_TestBit_m6768 ();
extern "C" void BigInteger_SetBit_m6769 ();
extern "C" void BigInteger_SetBit_m6770 ();
extern "C" void BigInteger_LowestSetBit_m6771 ();
extern "C" void BigInteger_GetBytes_m6772 ();
extern "C" void BigInteger_ToString_m6773 ();
extern "C" void BigInteger_ToString_m6774 ();
extern "C" void BigInteger_Normalize_m6775 ();
extern "C" void BigInteger_Clear_m6776 ();
extern "C" void BigInteger_GetHashCode_m6777 ();
extern "C" void BigInteger_ToString_m6778 ();
extern "C" void BigInteger_Equals_m6779 ();
extern "C" void BigInteger_ModInverse_m6780 ();
extern "C" void BigInteger_ModPow_m6781 ();
extern "C" void BigInteger_IsProbablePrime_m6782 ();
extern "C" void BigInteger_GeneratePseudoPrime_m6783 ();
extern "C" void BigInteger_Incr2_m6784 ();
extern "C" void BigInteger_op_Implicit_m6785 ();
extern "C" void BigInteger_op_Implicit_m6786 ();
extern "C" void BigInteger_op_Addition_m6787 ();
extern "C" void BigInteger_op_Subtraction_m6788 ();
extern "C" void BigInteger_op_Modulus_m6789 ();
extern "C" void BigInteger_op_Modulus_m6790 ();
extern "C" void BigInteger_op_Division_m6791 ();
extern "C" void BigInteger_op_Multiply_m6792 ();
extern "C" void BigInteger_op_Multiply_m6793 ();
extern "C" void BigInteger_op_LeftShift_m6794 ();
extern "C" void BigInteger_op_RightShift_m6795 ();
extern "C" void BigInteger_op_Equality_m6796 ();
extern "C" void BigInteger_op_Inequality_m6797 ();
extern "C" void BigInteger_op_Equality_m6798 ();
extern "C" void BigInteger_op_Inequality_m6799 ();
extern "C" void BigInteger_op_GreaterThan_m6800 ();
extern "C" void BigInteger_op_LessThan_m6801 ();
extern "C" void BigInteger_op_GreaterThanOrEqual_m6802 ();
extern "C" void BigInteger_op_LessThanOrEqual_m6803 ();
extern "C" void CryptoConvert_ToInt32LE_m6804 ();
extern "C" void CryptoConvert_ToUInt32LE_m6805 ();
extern "C" void CryptoConvert_GetBytesLE_m6806 ();
extern "C" void CryptoConvert_ToCapiPrivateKeyBlob_m6807 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m6808 ();
extern "C" void CryptoConvert_FromCapiPublicKeyBlob_m6809 ();
extern "C" void CryptoConvert_ToCapiPublicKeyBlob_m6810 ();
extern "C" void CryptoConvert_ToCapiKeyBlob_m6811 ();
extern "C" void KeyBuilder_get_Rng_m6812 ();
extern "C" void KeyBuilder_Key_m6813 ();
extern "C" void KeyBuilder_IV_m6814 ();
extern "C" void BlockProcessor__ctor_m6815 ();
extern "C" void BlockProcessor_Finalize_m6816 ();
extern "C" void BlockProcessor_Initialize_m6817 ();
extern "C" void BlockProcessor_Core_m6818 ();
extern "C" void BlockProcessor_Core_m6819 ();
extern "C" void BlockProcessor_Final_m6820 ();
extern "C" void KeyGeneratedEventHandler__ctor_m6821 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m6822 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m6823 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m6824 ();
extern "C" void DSAManaged__ctor_m6825 ();
extern "C" void DSAManaged_add_KeyGenerated_m6826 ();
extern "C" void DSAManaged_remove_KeyGenerated_m6827 ();
extern "C" void DSAManaged_Finalize_m6828 ();
extern "C" void DSAManaged_Generate_m6829 ();
extern "C" void DSAManaged_GenerateKeyPair_m6830 ();
extern "C" void DSAManaged_add_m6831 ();
extern "C" void DSAManaged_GenerateParams_m6832 ();
extern "C" void DSAManaged_get_Random_m6833 ();
extern "C" void DSAManaged_get_KeySize_m6834 ();
extern "C" void DSAManaged_get_PublicOnly_m6835 ();
extern "C" void DSAManaged_NormalizeArray_m6836 ();
extern "C" void DSAManaged_ExportParameters_m6837 ();
extern "C" void DSAManaged_ImportParameters_m6838 ();
extern "C" void DSAManaged_CreateSignature_m6839 ();
extern "C" void DSAManaged_VerifySignature_m6840 ();
extern "C" void DSAManaged_Dispose_m6841 ();
extern "C" void KeyPairPersistence__ctor_m6842 ();
extern "C" void KeyPairPersistence__ctor_m6843 ();
extern "C" void KeyPairPersistence__cctor_m6844 ();
extern "C" void KeyPairPersistence_get_Filename_m6845 ();
extern "C" void KeyPairPersistence_get_KeyValue_m6846 ();
extern "C" void KeyPairPersistence_set_KeyValue_m6847 ();
extern "C" void KeyPairPersistence_Load_m6848 ();
extern "C" void KeyPairPersistence_Save_m6849 ();
extern "C" void KeyPairPersistence_Remove_m6850 ();
extern "C" void KeyPairPersistence_get_UserPath_m6851 ();
extern "C" void KeyPairPersistence_get_MachinePath_m6852 ();
extern "C" void KeyPairPersistence__CanSecure_m6853 ();
extern "C" void KeyPairPersistence__ProtectUser_m6854 ();
extern "C" void KeyPairPersistence__ProtectMachine_m6855 ();
extern "C" void KeyPairPersistence__IsUserProtected_m6856 ();
extern "C" void KeyPairPersistence__IsMachineProtected_m6857 ();
extern "C" void KeyPairPersistence_CanSecure_m6858 ();
extern "C" void KeyPairPersistence_ProtectUser_m6859 ();
extern "C" void KeyPairPersistence_ProtectMachine_m6860 ();
extern "C" void KeyPairPersistence_IsUserProtected_m6861 ();
extern "C" void KeyPairPersistence_IsMachineProtected_m6862 ();
extern "C" void KeyPairPersistence_get_CanChange_m6863 ();
extern "C" void KeyPairPersistence_get_UseDefaultKeyContainer_m6864 ();
extern "C" void KeyPairPersistence_get_UseMachineKeyStore_m6865 ();
extern "C" void KeyPairPersistence_get_ContainerName_m6866 ();
extern "C" void KeyPairPersistence_Copy_m6867 ();
extern "C" void KeyPairPersistence_FromXml_m6868 ();
extern "C" void KeyPairPersistence_ToXml_m6869 ();
extern "C" void MACAlgorithm__ctor_m6870 ();
extern "C" void MACAlgorithm_Initialize_m6871 ();
extern "C" void MACAlgorithm_Core_m6872 ();
extern "C" void MACAlgorithm_Final_m6873 ();
extern "C" void PKCS1__cctor_m6874 ();
extern "C" void PKCS1_Compare_m6875 ();
extern "C" void PKCS1_I2OSP_m6876 ();
extern "C" void PKCS1_OS2IP_m6877 ();
extern "C" void PKCS1_RSAEP_m6878 ();
extern "C" void PKCS1_RSASP1_m6879 ();
extern "C" void PKCS1_RSAVP1_m6880 ();
extern "C" void PKCS1_Encrypt_v15_m6881 ();
extern "C" void PKCS1_Sign_v15_m6882 ();
extern "C" void PKCS1_Verify_v15_m6883 ();
extern "C" void PKCS1_Verify_v15_m6884 ();
extern "C" void PKCS1_Encode_v15_m6885 ();
extern "C" void PrivateKeyInfo__ctor_m6886 ();
extern "C" void PrivateKeyInfo__ctor_m6887 ();
extern "C" void PrivateKeyInfo_get_PrivateKey_m6888 ();
extern "C" void PrivateKeyInfo_Decode_m6889 ();
extern "C" void PrivateKeyInfo_RemoveLeadingZero_m6890 ();
extern "C" void PrivateKeyInfo_Normalize_m6891 ();
extern "C" void PrivateKeyInfo_DecodeRSA_m6892 ();
extern "C" void PrivateKeyInfo_DecodeDSA_m6893 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m6894 ();
extern "C" void EncryptedPrivateKeyInfo__ctor_m6895 ();
extern "C" void EncryptedPrivateKeyInfo_get_Algorithm_m6896 ();
extern "C" void EncryptedPrivateKeyInfo_get_EncryptedData_m6897 ();
extern "C" void EncryptedPrivateKeyInfo_get_Salt_m6898 ();
extern "C" void EncryptedPrivateKeyInfo_get_IterationCount_m6899 ();
extern "C" void EncryptedPrivateKeyInfo_Decode_m6900 ();
extern "C" void KeyGeneratedEventHandler__ctor_m6901 ();
extern "C" void KeyGeneratedEventHandler_Invoke_m6902 ();
extern "C" void KeyGeneratedEventHandler_BeginInvoke_m6903 ();
extern "C" void KeyGeneratedEventHandler_EndInvoke_m6904 ();
extern "C" void RSAManaged__ctor_m6905 ();
extern "C" void RSAManaged_add_KeyGenerated_m6906 ();
extern "C" void RSAManaged_remove_KeyGenerated_m6907 ();
extern "C" void RSAManaged_Finalize_m6908 ();
extern "C" void RSAManaged_GenerateKeyPair_m6909 ();
extern "C" void RSAManaged_get_KeySize_m6910 ();
extern "C" void RSAManaged_get_PublicOnly_m6911 ();
extern "C" void RSAManaged_DecryptValue_m6912 ();
extern "C" void RSAManaged_EncryptValue_m6913 ();
extern "C" void RSAManaged_ExportParameters_m6914 ();
extern "C" void RSAManaged_ImportParameters_m6915 ();
extern "C" void RSAManaged_Dispose_m6916 ();
extern "C" void RSAManaged_ToXmlString_m6917 ();
extern "C" void RSAManaged_get_IsCrtPossible_m6918 ();
extern "C" void RSAManaged_GetPaddedValue_m6919 ();
extern "C" void SymmetricTransform__ctor_m6920 ();
extern "C" void SymmetricTransform_System_IDisposable_Dispose_m6921 ();
extern "C" void SymmetricTransform_Finalize_m6922 ();
extern "C" void SymmetricTransform_Dispose_m6923 ();
extern "C" void SymmetricTransform_get_CanReuseTransform_m6924 ();
extern "C" void SymmetricTransform_Transform_m6925 ();
extern "C" void SymmetricTransform_CBC_m6926 ();
extern "C" void SymmetricTransform_CFB_m6927 ();
extern "C" void SymmetricTransform_OFB_m6928 ();
extern "C" void SymmetricTransform_CTS_m6929 ();
extern "C" void SymmetricTransform_CheckInput_m6930 ();
extern "C" void SymmetricTransform_TransformBlock_m6931 ();
extern "C" void SymmetricTransform_get_KeepLastBlock_m6932 ();
extern "C" void SymmetricTransform_InternalTransformBlock_m6933 ();
extern "C" void SymmetricTransform_Random_m6934 ();
extern "C" void SymmetricTransform_ThrowBadPaddingException_m6935 ();
extern "C" void SymmetricTransform_FinalEncrypt_m6936 ();
extern "C" void SymmetricTransform_FinalDecrypt_m6937 ();
extern "C" void SymmetricTransform_TransformFinalBlock_m6938 ();
extern "C" void SafeBag__ctor_m6939 ();
extern "C" void SafeBag_get_BagOID_m6940 ();
extern "C" void SafeBag_get_ASN1_m6941 ();
extern "C" void DeriveBytes__ctor_m6942 ();
extern "C" void DeriveBytes__cctor_m6943 ();
extern "C" void DeriveBytes_set_HashName_m6944 ();
extern "C" void DeriveBytes_set_IterationCount_m6945 ();
extern "C" void DeriveBytes_set_Password_m6946 ();
extern "C" void DeriveBytes_set_Salt_m6947 ();
extern "C" void DeriveBytes_Adjust_m6948 ();
extern "C" void DeriveBytes_Derive_m6949 ();
extern "C" void DeriveBytes_DeriveKey_m6950 ();
extern "C" void DeriveBytes_DeriveIV_m6951 ();
extern "C" void DeriveBytes_DeriveMAC_m6952 ();
extern "C" void PKCS12__ctor_m6953 ();
extern "C" void PKCS12__ctor_m6954 ();
extern "C" void PKCS12__ctor_m6955 ();
extern "C" void PKCS12__cctor_m6956 ();
extern "C" void PKCS12_Decode_m6957 ();
extern "C" void PKCS12_Finalize_m6958 ();
extern "C" void PKCS12_set_Password_m6959 ();
extern "C" void PKCS12_get_IterationCount_m6960 ();
extern "C" void PKCS12_set_IterationCount_m6961 ();
extern "C" void PKCS12_get_Certificates_m6962 ();
extern "C" void PKCS12_get_RNG_m6963 ();
extern "C" void PKCS12_Compare_m6964 ();
extern "C" void PKCS12_GetSymmetricAlgorithm_m6965 ();
extern "C" void PKCS12_Decrypt_m6966 ();
extern "C" void PKCS12_Decrypt_m6967 ();
extern "C" void PKCS12_Encrypt_m6968 ();
extern "C" void PKCS12_GetExistingParameters_m6969 ();
extern "C" void PKCS12_AddPrivateKey_m6970 ();
extern "C" void PKCS12_ReadSafeBag_m6971 ();
extern "C" void PKCS12_CertificateSafeBag_m6972 ();
extern "C" void PKCS12_MAC_m6973 ();
extern "C" void PKCS12_GetBytes_m6974 ();
extern "C" void PKCS12_EncryptedContentInfo_m6975 ();
extern "C" void PKCS12_AddCertificate_m6976 ();
extern "C" void PKCS12_AddCertificate_m6977 ();
extern "C" void PKCS12_RemoveCertificate_m6978 ();
extern "C" void PKCS12_RemoveCertificate_m6979 ();
extern "C" void PKCS12_Clone_m6980 ();
extern "C" void PKCS12_get_MaximumPasswordLength_m6981 ();
extern "C" void X501__cctor_m6982 ();
extern "C" void X501_ToString_m6983 ();
extern "C" void X501_ToString_m6984 ();
extern "C" void X501_AppendEntry_m6985 ();
extern "C" void X509Certificate__ctor_m6986 ();
extern "C" void X509Certificate__cctor_m6987 ();
extern "C" void X509Certificate_Parse_m6988 ();
extern "C" void X509Certificate_GetUnsignedBigInteger_m6989 ();
extern "C" void X509Certificate_get_DSA_m6990 ();
extern "C" void X509Certificate_get_IssuerName_m6991 ();
extern "C" void X509Certificate_get_KeyAlgorithmParameters_m6992 ();
extern "C" void X509Certificate_get_PublicKey_m6993 ();
extern "C" void X509Certificate_get_RawData_m6994 ();
extern "C" void X509Certificate_get_SubjectName_m6995 ();
extern "C" void X509Certificate_get_ValidFrom_m6996 ();
extern "C" void X509Certificate_get_ValidUntil_m6997 ();
extern "C" void X509Certificate_GetIssuerName_m6998 ();
extern "C" void X509Certificate_GetSubjectName_m6999 ();
extern "C" void X509Certificate_GetObjectData_m7000 ();
extern "C" void X509Certificate_PEM_m7001 ();
extern "C" void X509CertificateEnumerator__ctor_m7002 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m7003 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m7004 ();
extern "C" void X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m7005 ();
extern "C" void X509CertificateEnumerator_get_Current_m7006 ();
extern "C" void X509CertificateEnumerator_MoveNext_m7007 ();
extern "C" void X509CertificateEnumerator_Reset_m7008 ();
extern "C" void X509CertificateCollection__ctor_m7009 ();
extern "C" void X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m7010 ();
extern "C" void X509CertificateCollection_get_Item_m7011 ();
extern "C" void X509CertificateCollection_Add_m7012 ();
extern "C" void X509CertificateCollection_GetEnumerator_m7013 ();
extern "C" void X509CertificateCollection_GetHashCode_m7014 ();
extern "C" void X509Extension__ctor_m7015 ();
extern "C" void X509Extension_Decode_m7016 ();
extern "C" void X509Extension_Equals_m7017 ();
extern "C" void X509Extension_GetHashCode_m7018 ();
extern "C" void X509Extension_WriteLine_m7019 ();
extern "C" void X509Extension_ToString_m7020 ();
extern "C" void X509ExtensionCollection__ctor_m7021 ();
extern "C" void X509ExtensionCollection__ctor_m7022 ();
extern "C" void X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m7023 ();
extern "C" void ASN1__ctor_m7024 ();
extern "C" void ASN1__ctor_m7025 ();
extern "C" void ASN1__ctor_m7026 ();
extern "C" void ASN1_get_Count_m7027 ();
extern "C" void ASN1_get_Tag_m7028 ();
extern "C" void ASN1_get_Length_m7029 ();
extern "C" void ASN1_get_Value_m7030 ();
extern "C" void ASN1_set_Value_m7031 ();
extern "C" void ASN1_CompareArray_m7032 ();
extern "C" void ASN1_CompareValue_m7033 ();
extern "C" void ASN1_Add_m7034 ();
extern "C" void ASN1_GetBytes_m7035 ();
extern "C" void ASN1_Decode_m7036 ();
extern "C" void ASN1_DecodeTLV_m7037 ();
extern "C" void ASN1_get_Item_m7038 ();
extern "C" void ASN1_Element_m7039 ();
extern "C" void ASN1_ToString_m7040 ();
extern "C" void ASN1Convert_FromInt32_m7041 ();
extern "C" void ASN1Convert_FromOid_m7042 ();
extern "C" void ASN1Convert_ToInt32_m7043 ();
extern "C" void ASN1Convert_ToOid_m7044 ();
extern "C" void ASN1Convert_ToDateTime_m7045 ();
extern "C" void BitConverterLE_GetUIntBytes_m7046 ();
extern "C" void BitConverterLE_GetBytes_m7047 ();
extern "C" void BitConverterLE_UShortFromBytes_m7048 ();
extern "C" void BitConverterLE_UIntFromBytes_m7049 ();
extern "C" void BitConverterLE_ULongFromBytes_m7050 ();
extern "C" void BitConverterLE_ToInt16_m7051 ();
extern "C" void BitConverterLE_ToInt32_m7052 ();
extern "C" void BitConverterLE_ToSingle_m7053 ();
extern "C" void BitConverterLE_ToDouble_m7054 ();
extern "C" void ContentInfo__ctor_m7055 ();
extern "C" void ContentInfo__ctor_m7056 ();
extern "C" void ContentInfo__ctor_m7057 ();
extern "C" void ContentInfo__ctor_m7058 ();
extern "C" void ContentInfo_get_ASN1_m7059 ();
extern "C" void ContentInfo_get_Content_m7060 ();
extern "C" void ContentInfo_set_Content_m7061 ();
extern "C" void ContentInfo_get_ContentType_m7062 ();
extern "C" void ContentInfo_set_ContentType_m7063 ();
extern "C" void ContentInfo_GetASN1_m7064 ();
extern "C" void EncryptedData__ctor_m7065 ();
extern "C" void EncryptedData__ctor_m7066 ();
extern "C" void EncryptedData_get_EncryptionAlgorithm_m7067 ();
extern "C" void EncryptedData_get_EncryptedContent_m7068 ();
extern "C" void StrongName__cctor_m7069 ();
extern "C" void StrongName_get_PublicKey_m7070 ();
extern "C" void StrongName_get_PublicKeyToken_m7071 ();
extern "C" void StrongName_get_TokenAlgorithm_m7072 ();
extern "C" void SecurityParser__ctor_m7073 ();
extern "C" void SecurityParser_LoadXml_m7074 ();
extern "C" void SecurityParser_ToXml_m7075 ();
extern "C" void SecurityParser_OnStartParsing_m7076 ();
extern "C" void SecurityParser_OnProcessingInstruction_m7077 ();
extern "C" void SecurityParser_OnIgnorableWhitespace_m7078 ();
extern "C" void SecurityParser_OnStartElement_m7079 ();
extern "C" void SecurityParser_OnEndElement_m7080 ();
extern "C" void SecurityParser_OnChars_m7081 ();
extern "C" void SecurityParser_OnEndParsing_m7082 ();
extern "C" void AttrListImpl__ctor_m7083 ();
extern "C" void AttrListImpl_get_Length_m7084 ();
extern "C" void AttrListImpl_GetName_m7085 ();
extern "C" void AttrListImpl_GetValue_m7086 ();
extern "C" void AttrListImpl_GetValue_m7087 ();
extern "C" void AttrListImpl_get_Names_m7088 ();
extern "C" void AttrListImpl_get_Values_m7089 ();
extern "C" void AttrListImpl_Clear_m7090 ();
extern "C" void AttrListImpl_Add_m7091 ();
extern "C" void SmallXmlParser__ctor_m7092 ();
extern "C" void SmallXmlParser_Error_m7093 ();
extern "C" void SmallXmlParser_UnexpectedEndError_m7094 ();
extern "C" void SmallXmlParser_IsNameChar_m7095 ();
extern "C" void SmallXmlParser_IsWhitespace_m7096 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m7097 ();
extern "C" void SmallXmlParser_HandleWhitespaces_m7098 ();
extern "C" void SmallXmlParser_SkipWhitespaces_m7099 ();
extern "C" void SmallXmlParser_Peek_m7100 ();
extern "C" void SmallXmlParser_Read_m7101 ();
extern "C" void SmallXmlParser_Expect_m7102 ();
extern "C" void SmallXmlParser_ReadUntil_m7103 ();
extern "C" void SmallXmlParser_ReadName_m7104 ();
extern "C" void SmallXmlParser_Parse_m7105 ();
extern "C" void SmallXmlParser_Cleanup_m7106 ();
extern "C" void SmallXmlParser_ReadContent_m7107 ();
extern "C" void SmallXmlParser_HandleBufferedContent_m7108 ();
extern "C" void SmallXmlParser_ReadCharacters_m7109 ();
extern "C" void SmallXmlParser_ReadReference_m7110 ();
extern "C" void SmallXmlParser_ReadCharacterReference_m7111 ();
extern "C" void SmallXmlParser_ReadAttribute_m7112 ();
extern "C" void SmallXmlParser_ReadCDATASection_m7113 ();
extern "C" void SmallXmlParser_ReadComment_m7114 ();
extern "C" void SmallXmlParserException__ctor_m7115 ();
extern "C" void Runtime_GetDisplayName_m7116 ();
extern "C" void KeyNotFoundException__ctor_m7117 ();
extern "C" void KeyNotFoundException__ctor_m7118 ();
extern "C" void SimpleEnumerator__ctor_m7119 ();
extern "C" void SimpleEnumerator__cctor_m7120 ();
extern "C" void SimpleEnumerator_Clone_m7121 ();
extern "C" void SimpleEnumerator_MoveNext_m7122 ();
extern "C" void SimpleEnumerator_get_Current_m7123 ();
extern "C" void SimpleEnumerator_Reset_m7124 ();
extern "C" void ArrayListWrapper__ctor_m7125 ();
extern "C" void ArrayListWrapper_get_Item_m7126 ();
extern "C" void ArrayListWrapper_set_Item_m7127 ();
extern "C" void ArrayListWrapper_get_Count_m7128 ();
extern "C" void ArrayListWrapper_get_Capacity_m7129 ();
extern "C" void ArrayListWrapper_set_Capacity_m7130 ();
extern "C" void ArrayListWrapper_get_IsFixedSize_m7131 ();
extern "C" void ArrayListWrapper_get_IsReadOnly_m7132 ();
extern "C" void ArrayListWrapper_get_IsSynchronized_m7133 ();
extern "C" void ArrayListWrapper_get_SyncRoot_m7134 ();
extern "C" void ArrayListWrapper_Add_m7135 ();
extern "C" void ArrayListWrapper_Clear_m7136 ();
extern "C" void ArrayListWrapper_Contains_m7137 ();
extern "C" void ArrayListWrapper_IndexOf_m7138 ();
extern "C" void ArrayListWrapper_IndexOf_m7139 ();
extern "C" void ArrayListWrapper_IndexOf_m7140 ();
extern "C" void ArrayListWrapper_Insert_m7141 ();
extern "C" void ArrayListWrapper_InsertRange_m7142 ();
extern "C" void ArrayListWrapper_Remove_m7143 ();
extern "C" void ArrayListWrapper_RemoveAt_m7144 ();
extern "C" void ArrayListWrapper_CopyTo_m7145 ();
extern "C" void ArrayListWrapper_CopyTo_m7146 ();
extern "C" void ArrayListWrapper_CopyTo_m7147 ();
extern "C" void ArrayListWrapper_GetEnumerator_m7148 ();
extern "C" void ArrayListWrapper_AddRange_m7149 ();
extern "C" void ArrayListWrapper_Clone_m7150 ();
extern "C" void ArrayListWrapper_Sort_m7151 ();
extern "C" void ArrayListWrapper_Sort_m7152 ();
extern "C" void ArrayListWrapper_ToArray_m7153 ();
extern "C" void ArrayListWrapper_ToArray_m7154 ();
extern "C" void SynchronizedArrayListWrapper__ctor_m7155 ();
extern "C" void SynchronizedArrayListWrapper_get_Item_m7156 ();
extern "C" void SynchronizedArrayListWrapper_set_Item_m7157 ();
extern "C" void SynchronizedArrayListWrapper_get_Count_m7158 ();
extern "C" void SynchronizedArrayListWrapper_get_Capacity_m7159 ();
extern "C" void SynchronizedArrayListWrapper_set_Capacity_m7160 ();
extern "C" void SynchronizedArrayListWrapper_get_IsFixedSize_m7161 ();
extern "C" void SynchronizedArrayListWrapper_get_IsReadOnly_m7162 ();
extern "C" void SynchronizedArrayListWrapper_get_IsSynchronized_m7163 ();
extern "C" void SynchronizedArrayListWrapper_get_SyncRoot_m7164 ();
extern "C" void SynchronizedArrayListWrapper_Add_m7165 ();
extern "C" void SynchronizedArrayListWrapper_Clear_m7166 ();
extern "C" void SynchronizedArrayListWrapper_Contains_m7167 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m7168 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m7169 ();
extern "C" void SynchronizedArrayListWrapper_IndexOf_m7170 ();
extern "C" void SynchronizedArrayListWrapper_Insert_m7171 ();
extern "C" void SynchronizedArrayListWrapper_InsertRange_m7172 ();
extern "C" void SynchronizedArrayListWrapper_Remove_m7173 ();
extern "C" void SynchronizedArrayListWrapper_RemoveAt_m7174 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m7175 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m7176 ();
extern "C" void SynchronizedArrayListWrapper_CopyTo_m7177 ();
extern "C" void SynchronizedArrayListWrapper_GetEnumerator_m7178 ();
extern "C" void SynchronizedArrayListWrapper_AddRange_m7179 ();
extern "C" void SynchronizedArrayListWrapper_Clone_m7180 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m7181 ();
extern "C" void SynchronizedArrayListWrapper_Sort_m7182 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m7183 ();
extern "C" void SynchronizedArrayListWrapper_ToArray_m7184 ();
extern "C" void FixedSizeArrayListWrapper__ctor_m7185 ();
extern "C" void FixedSizeArrayListWrapper_get_ErrorMessage_m7186 ();
extern "C" void FixedSizeArrayListWrapper_get_Capacity_m7187 ();
extern "C" void FixedSizeArrayListWrapper_set_Capacity_m7188 ();
extern "C" void FixedSizeArrayListWrapper_get_IsFixedSize_m7189 ();
extern "C" void FixedSizeArrayListWrapper_Add_m7190 ();
extern "C" void FixedSizeArrayListWrapper_AddRange_m7191 ();
extern "C" void FixedSizeArrayListWrapper_Clear_m7192 ();
extern "C" void FixedSizeArrayListWrapper_Insert_m7193 ();
extern "C" void FixedSizeArrayListWrapper_InsertRange_m7194 ();
extern "C" void FixedSizeArrayListWrapper_Remove_m7195 ();
extern "C" void FixedSizeArrayListWrapper_RemoveAt_m7196 ();
extern "C" void ReadOnlyArrayListWrapper__ctor_m7197 ();
extern "C" void ReadOnlyArrayListWrapper_get_ErrorMessage_m7198 ();
extern "C" void ReadOnlyArrayListWrapper_get_IsReadOnly_m7199 ();
extern "C" void ReadOnlyArrayListWrapper_get_Item_m7200 ();
extern "C" void ReadOnlyArrayListWrapper_set_Item_m7201 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m7202 ();
extern "C" void ReadOnlyArrayListWrapper_Sort_m7203 ();
extern "C" void ArrayList__ctor_m4635 ();
extern "C" void ArrayList__ctor_m5685 ();
extern "C" void ArrayList__ctor_m5707 ();
extern "C" void ArrayList__ctor_m7204 ();
extern "C" void ArrayList__cctor_m7205 ();
extern "C" void ArrayList_get_Item_m7206 ();
extern "C" void ArrayList_set_Item_m7207 ();
extern "C" void ArrayList_get_Count_m7208 ();
extern "C" void ArrayList_get_Capacity_m7209 ();
extern "C" void ArrayList_set_Capacity_m7210 ();
extern "C" void ArrayList_get_IsFixedSize_m7211 ();
extern "C" void ArrayList_get_IsReadOnly_m7212 ();
extern "C" void ArrayList_get_IsSynchronized_m7213 ();
extern "C" void ArrayList_get_SyncRoot_m7214 ();
extern "C" void ArrayList_EnsureCapacity_m7215 ();
extern "C" void ArrayList_Shift_m7216 ();
extern "C" void ArrayList_Add_m7217 ();
extern "C" void ArrayList_Clear_m7218 ();
extern "C" void ArrayList_Contains_m7219 ();
extern "C" void ArrayList_IndexOf_m7220 ();
extern "C" void ArrayList_IndexOf_m7221 ();
extern "C" void ArrayList_IndexOf_m7222 ();
extern "C" void ArrayList_Insert_m7223 ();
extern "C" void ArrayList_InsertRange_m7224 ();
extern "C" void ArrayList_Remove_m7225 ();
extern "C" void ArrayList_RemoveAt_m7226 ();
extern "C" void ArrayList_CopyTo_m7227 ();
extern "C" void ArrayList_CopyTo_m7228 ();
extern "C" void ArrayList_CopyTo_m7229 ();
extern "C" void ArrayList_GetEnumerator_m7230 ();
extern "C" void ArrayList_AddRange_m7231 ();
extern "C" void ArrayList_Sort_m7232 ();
extern "C" void ArrayList_Sort_m7233 ();
extern "C" void ArrayList_ToArray_m7234 ();
extern "C" void ArrayList_ToArray_m7235 ();
extern "C" void ArrayList_Clone_m7236 ();
extern "C" void ArrayList_ThrowNewArgumentOutOfRangeException_m7237 ();
extern "C" void ArrayList_Synchronized_m7238 ();
extern "C" void ArrayList_ReadOnly_m4668 ();
extern "C" void BitArrayEnumerator__ctor_m7239 ();
extern "C" void BitArrayEnumerator_Clone_m7240 ();
extern "C" void BitArrayEnumerator_get_Current_m7241 ();
extern "C" void BitArrayEnumerator_MoveNext_m7242 ();
extern "C" void BitArrayEnumerator_Reset_m7243 ();
extern "C" void BitArrayEnumerator_checkVersion_m7244 ();
extern "C" void BitArray__ctor_m7245 ();
extern "C" void BitArray__ctor_m5725 ();
extern "C" void BitArray_getByte_m7246 ();
extern "C" void BitArray_get_Count_m7247 ();
extern "C" void BitArray_get_IsSynchronized_m7248 ();
extern "C" void BitArray_get_Item_m5720 ();
extern "C" void BitArray_set_Item_m5726 ();
extern "C" void BitArray_get_Length_m5719 ();
extern "C" void BitArray_get_SyncRoot_m7249 ();
extern "C" void BitArray_Clone_m7250 ();
extern "C" void BitArray_CopyTo_m7251 ();
extern "C" void BitArray_Get_m7252 ();
extern "C" void BitArray_Set_m7253 ();
extern "C" void BitArray_GetEnumerator_m7254 ();
extern "C" void CaseInsensitiveComparer__ctor_m7255 ();
extern "C" void CaseInsensitiveComparer__ctor_m7256 ();
extern "C" void CaseInsensitiveComparer__cctor_m7257 ();
extern "C" void CaseInsensitiveComparer_get_DefaultInvariant_m5648 ();
extern "C" void CaseInsensitiveComparer_Compare_m7258 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m7259 ();
extern "C" void CaseInsensitiveHashCodeProvider__ctor_m7260 ();
extern "C" void CaseInsensitiveHashCodeProvider__cctor_m7261 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m7262 ();
extern "C" void CaseInsensitiveHashCodeProvider_AreEqual_m7263 ();
extern "C" void CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m5649 ();
extern "C" void CaseInsensitiveHashCodeProvider_GetHashCode_m7264 ();
extern "C" void CollectionBase__ctor_m4690 ();
extern "C" void CollectionBase_System_Collections_ICollection_CopyTo_m7265 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_SyncRoot_m7266 ();
extern "C" void CollectionBase_System_Collections_ICollection_get_IsSynchronized_m7267 ();
extern "C" void CollectionBase_System_Collections_IList_Add_m7268 ();
extern "C" void CollectionBase_System_Collections_IList_Contains_m7269 ();
extern "C" void CollectionBase_System_Collections_IList_IndexOf_m7270 ();
extern "C" void CollectionBase_System_Collections_IList_Insert_m7271 ();
extern "C" void CollectionBase_System_Collections_IList_Remove_m7272 ();
extern "C" void CollectionBase_System_Collections_IList_get_IsFixedSize_m7273 ();
extern "C" void CollectionBase_System_Collections_IList_get_IsReadOnly_m7274 ();
extern "C" void CollectionBase_System_Collections_IList_get_Item_m7275 ();
extern "C" void CollectionBase_System_Collections_IList_set_Item_m7276 ();
extern "C" void CollectionBase_get_Count_m7277 ();
extern "C" void CollectionBase_GetEnumerator_m7278 ();
extern "C" void CollectionBase_Clear_m7279 ();
extern "C" void CollectionBase_RemoveAt_m7280 ();
extern "C" void CollectionBase_get_InnerList_m4691 ();
extern "C" void CollectionBase_get_List_m5723 ();
extern "C" void CollectionBase_OnClear_m7281 ();
extern "C" void CollectionBase_OnClearComplete_m7282 ();
extern "C" void CollectionBase_OnInsert_m7283 ();
extern "C" void CollectionBase_OnInsertComplete_m7284 ();
extern "C" void CollectionBase_OnRemove_m7285 ();
extern "C" void CollectionBase_OnRemoveComplete_m7286 ();
extern "C" void CollectionBase_OnSet_m7287 ();
extern "C" void CollectionBase_OnSetComplete_m7288 ();
extern "C" void CollectionBase_OnValidate_m7289 ();
extern "C" void Comparer__ctor_m7290 ();
extern "C" void Comparer__ctor_m7291 ();
extern "C" void Comparer__cctor_m7292 ();
extern "C" void Comparer_Compare_m7293 ();
extern "C" void Comparer_GetObjectData_m7294 ();
extern "C" void DictionaryEntry__ctor_m5652 ();
extern "C" void DictionaryEntry_get_Key_m7295 ();
extern "C" void DictionaryEntry_get_Value_m7296 ();
extern "C" void KeyMarker__ctor_m7297 ();
extern "C" void KeyMarker__cctor_m7298 ();
extern "C" void Enumerator__ctor_m7299 ();
extern "C" void Enumerator__cctor_m7300 ();
extern "C" void Enumerator_FailFast_m7301 ();
extern "C" void Enumerator_Reset_m7302 ();
extern "C" void Enumerator_MoveNext_m7303 ();
extern "C" void Enumerator_get_Entry_m7304 ();
extern "C" void Enumerator_get_Key_m7305 ();
extern "C" void Enumerator_get_Value_m7306 ();
extern "C" void Enumerator_get_Current_m7307 ();
extern "C" void HashKeys__ctor_m7308 ();
extern "C" void HashKeys_get_Count_m7309 ();
extern "C" void HashKeys_get_IsSynchronized_m7310 ();
extern "C" void HashKeys_get_SyncRoot_m7311 ();
extern "C" void HashKeys_CopyTo_m7312 ();
extern "C" void HashKeys_GetEnumerator_m7313 ();
extern "C" void HashValues__ctor_m7314 ();
extern "C" void HashValues_get_Count_m7315 ();
extern "C" void HashValues_get_IsSynchronized_m7316 ();
extern "C" void HashValues_get_SyncRoot_m7317 ();
extern "C" void HashValues_CopyTo_m7318 ();
extern "C" void HashValues_GetEnumerator_m7319 ();
extern "C" void SyncHashtable__ctor_m7320 ();
extern "C" void SyncHashtable__ctor_m7321 ();
extern "C" void SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m7322 ();
extern "C" void SyncHashtable_GetObjectData_m7323 ();
extern "C" void SyncHashtable_get_Count_m7324 ();
extern "C" void SyncHashtable_get_IsSynchronized_m7325 ();
extern "C" void SyncHashtable_get_SyncRoot_m7326 ();
extern "C" void SyncHashtable_get_Keys_m7327 ();
extern "C" void SyncHashtable_get_Values_m7328 ();
extern "C" void SyncHashtable_get_Item_m7329 ();
extern "C" void SyncHashtable_set_Item_m7330 ();
extern "C" void SyncHashtable_CopyTo_m7331 ();
extern "C" void SyncHashtable_Add_m7332 ();
extern "C" void SyncHashtable_Clear_m7333 ();
extern "C" void SyncHashtable_Contains_m7334 ();
extern "C" void SyncHashtable_GetEnumerator_m7335 ();
extern "C" void SyncHashtable_Remove_m7336 ();
extern "C" void SyncHashtable_ContainsKey_m7337 ();
extern "C" void SyncHashtable_Clone_m7338 ();
extern "C" void Hashtable__ctor_m4718 ();
extern "C" void Hashtable__ctor_m7339 ();
extern "C" void Hashtable__ctor_m7340 ();
extern "C" void Hashtable__ctor_m5714 ();
extern "C" void Hashtable__ctor_m7341 ();
extern "C" void Hashtable__ctor_m5650 ();
extern "C" void Hashtable__ctor_m7342 ();
extern "C" void Hashtable__ctor_m5651 ();
extern "C" void Hashtable__ctor_m5682 ();
extern "C" void Hashtable__ctor_m7343 ();
extern "C" void Hashtable__ctor_m5658 ();
extern "C" void Hashtable__ctor_m7344 ();
extern "C" void Hashtable__cctor_m7345 ();
extern "C" void Hashtable_System_Collections_IEnumerable_GetEnumerator_m7346 ();
extern "C" void Hashtable_set_comparer_m7347 ();
extern "C" void Hashtable_set_hcp_m7348 ();
extern "C" void Hashtable_get_Count_m7349 ();
extern "C" void Hashtable_get_IsSynchronized_m7350 ();
extern "C" void Hashtable_get_SyncRoot_m7351 ();
extern "C" void Hashtable_get_Keys_m7352 ();
extern "C" void Hashtable_get_Values_m7353 ();
extern "C" void Hashtable_get_Item_m7354 ();
extern "C" void Hashtable_set_Item_m7355 ();
extern "C" void Hashtable_CopyTo_m7356 ();
extern "C" void Hashtable_Add_m7357 ();
extern "C" void Hashtable_Clear_m7358 ();
extern "C" void Hashtable_Contains_m7359 ();
extern "C" void Hashtable_GetEnumerator_m7360 ();
extern "C" void Hashtable_Remove_m7361 ();
extern "C" void Hashtable_ContainsKey_m7362 ();
extern "C" void Hashtable_Clone_m7363 ();
extern "C" void Hashtable_GetObjectData_m7364 ();
extern "C" void Hashtable_OnDeserialization_m7365 ();
extern "C" void Hashtable_Synchronized_m7366 ();
extern "C" void Hashtable_GetHash_m7367 ();
extern "C" void Hashtable_KeyEquals_m7368 ();
extern "C" void Hashtable_AdjustThreshold_m7369 ();
extern "C" void Hashtable_SetTable_m7370 ();
extern "C" void Hashtable_Find_m7371 ();
extern "C" void Hashtable_Rehash_m7372 ();
extern "C" void Hashtable_PutImpl_m7373 ();
extern "C" void Hashtable_CopyToArray_m7374 ();
extern "C" void Hashtable_TestPrime_m7375 ();
extern "C" void Hashtable_CalcPrime_m7376 ();
extern "C" void Hashtable_ToPrime_m7377 ();
extern "C" void Enumerator__ctor_m7378 ();
extern "C" void Enumerator__cctor_m7379 ();
extern "C" void Enumerator_Reset_m7380 ();
extern "C" void Enumerator_MoveNext_m7381 ();
extern "C" void Enumerator_get_Entry_m7382 ();
extern "C" void Enumerator_get_Key_m7383 ();
extern "C" void Enumerator_get_Value_m7384 ();
extern "C" void Enumerator_get_Current_m7385 ();
extern "C" void Enumerator_Clone_m7386 ();
extern "C" void SortedList__ctor_m7387 ();
extern "C" void SortedList__ctor_m5681 ();
extern "C" void SortedList__ctor_m7388 ();
extern "C" void SortedList__ctor_m7389 ();
extern "C" void SortedList__cctor_m7390 ();
extern "C" void SortedList_System_Collections_IEnumerable_GetEnumerator_m7391 ();
extern "C" void SortedList_get_Count_m7392 ();
extern "C" void SortedList_get_IsSynchronized_m7393 ();
extern "C" void SortedList_get_SyncRoot_m7394 ();
extern "C" void SortedList_get_IsFixedSize_m7395 ();
extern "C" void SortedList_get_IsReadOnly_m7396 ();
extern "C" void SortedList_get_Item_m7397 ();
extern "C" void SortedList_set_Item_m7398 ();
extern "C" void SortedList_get_Capacity_m7399 ();
extern "C" void SortedList_set_Capacity_m7400 ();
extern "C" void SortedList_Add_m7401 ();
extern "C" void SortedList_Contains_m7402 ();
extern "C" void SortedList_GetEnumerator_m7403 ();
extern "C" void SortedList_Remove_m7404 ();
extern "C" void SortedList_CopyTo_m7405 ();
extern "C" void SortedList_Clone_m7406 ();
extern "C" void SortedList_RemoveAt_m7407 ();
extern "C" void SortedList_IndexOfKey_m7408 ();
extern "C" void SortedList_ContainsKey_m7409 ();
extern "C" void SortedList_GetByIndex_m7410 ();
extern "C" void SortedList_EnsureCapacity_m7411 ();
extern "C" void SortedList_PutImpl_m7412 ();
extern "C" void SortedList_GetImpl_m7413 ();
extern "C" void SortedList_InitTable_m7414 ();
extern "C" void SortedList_Find_m7415 ();
extern "C" void Enumerator__ctor_m7416 ();
extern "C" void Enumerator_Clone_m7417 ();
extern "C" void Enumerator_get_Current_m7418 ();
extern "C" void Enumerator_MoveNext_m7419 ();
extern "C" void Enumerator_Reset_m7420 ();
extern "C" void Stack__ctor_m3674 ();
extern "C" void Stack__ctor_m7421 ();
extern "C" void Stack__ctor_m7422 ();
extern "C" void Stack_Resize_m7423 ();
extern "C" void Stack_get_Count_m7424 ();
extern "C" void Stack_get_IsSynchronized_m7425 ();
extern "C" void Stack_get_SyncRoot_m7426 ();
extern "C" void Stack_Clear_m7427 ();
extern "C" void Stack_Clone_m7428 ();
extern "C" void Stack_CopyTo_m7429 ();
extern "C" void Stack_GetEnumerator_m7430 ();
extern "C" void Stack_Peek_m7431 ();
extern "C" void Stack_Pop_m7432 ();
extern "C" void Stack_Push_m7433 ();
extern "C" void DebuggableAttribute__ctor_m7434 ();
extern "C" void DebuggerDisplayAttribute__ctor_m7435 ();
extern "C" void DebuggerDisplayAttribute_set_Name_m7436 ();
extern "C" void DebuggerStepThroughAttribute__ctor_m7437 ();
extern "C" void DebuggerTypeProxyAttribute__ctor_m7438 ();
extern "C" void StackFrame__ctor_m7439 ();
extern "C" void StackFrame__ctor_m7440 ();
extern "C" void StackFrame_get_frame_info_m7441 ();
extern "C" void StackFrame_GetFileLineNumber_m7442 ();
extern "C" void StackFrame_GetFileName_m7443 ();
extern "C" void StackFrame_GetSecureFileName_m7444 ();
extern "C" void StackFrame_GetILOffset_m7445 ();
extern "C" void StackFrame_GetMethod_m7446 ();
extern "C" void StackFrame_GetNativeOffset_m7447 ();
extern "C" void StackFrame_GetInternalMethodName_m7448 ();
extern "C" void StackFrame_ToString_m7449 ();
extern "C" void StackTrace__ctor_m7450 ();
extern "C" void StackTrace__ctor_m3636 ();
extern "C" void StackTrace__ctor_m7451 ();
extern "C" void StackTrace__ctor_m7452 ();
extern "C" void StackTrace__ctor_m7453 ();
extern "C" void StackTrace_init_frames_m7454 ();
extern "C" void StackTrace_get_trace_m7455 ();
extern "C" void StackTrace_get_FrameCount_m7456 ();
extern "C" void StackTrace_GetFrame_m7457 ();
extern "C" void StackTrace_ToString_m7458 ();
extern "C" void Calendar__ctor_m7459 ();
extern "C" void Calendar_Clone_m7460 ();
extern "C" void Calendar_CheckReadOnly_m7461 ();
extern "C" void Calendar_get_EraNames_m7462 ();
extern "C" void CCMath_div_m7463 ();
extern "C" void CCMath_mod_m7464 ();
extern "C" void CCMath_div_mod_m7465 ();
extern "C" void CCFixed_FromDateTime_m7466 ();
extern "C" void CCFixed_day_of_week_m7467 ();
extern "C" void CCGregorianCalendar_is_leap_year_m7468 ();
extern "C" void CCGregorianCalendar_fixed_from_dmy_m7469 ();
extern "C" void CCGregorianCalendar_year_from_fixed_m7470 ();
extern "C" void CCGregorianCalendar_my_from_fixed_m7471 ();
extern "C" void CCGregorianCalendar_dmy_from_fixed_m7472 ();
extern "C" void CCGregorianCalendar_month_from_fixed_m7473 ();
extern "C" void CCGregorianCalendar_day_from_fixed_m7474 ();
extern "C" void CCGregorianCalendar_GetDayOfMonth_m7475 ();
extern "C" void CCGregorianCalendar_GetMonth_m7476 ();
extern "C" void CCGregorianCalendar_GetYear_m7477 ();
extern "C" void CompareInfo__ctor_m7478 ();
extern "C" void CompareInfo__ctor_m7479 ();
extern "C" void CompareInfo__cctor_m7480 ();
extern "C" void CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7481 ();
extern "C" void CompareInfo_get_UseManagedCollation_m7482 ();
extern "C" void CompareInfo_construct_compareinfo_m7483 ();
extern "C" void CompareInfo_free_internal_collator_m7484 ();
extern "C" void CompareInfo_internal_compare_m7485 ();
extern "C" void CompareInfo_assign_sortkey_m7486 ();
extern "C" void CompareInfo_internal_index_m7487 ();
extern "C" void CompareInfo_Finalize_m7488 ();
extern "C" void CompareInfo_internal_compare_managed_m7489 ();
extern "C" void CompareInfo_internal_compare_switch_m7490 ();
extern "C" void CompareInfo_Compare_m7491 ();
extern "C" void CompareInfo_Compare_m7492 ();
extern "C" void CompareInfo_Compare_m7493 ();
extern "C" void CompareInfo_Equals_m7494 ();
extern "C" void CompareInfo_GetHashCode_m7495 ();
extern "C" void CompareInfo_GetSortKey_m7496 ();
extern "C" void CompareInfo_IndexOf_m7497 ();
extern "C" void CompareInfo_internal_index_managed_m7498 ();
extern "C" void CompareInfo_internal_index_switch_m7499 ();
extern "C" void CompareInfo_IndexOf_m7500 ();
extern "C" void CompareInfo_IsPrefix_m7501 ();
extern "C" void CompareInfo_IsSuffix_m7502 ();
extern "C" void CompareInfo_LastIndexOf_m7503 ();
extern "C" void CompareInfo_LastIndexOf_m7504 ();
extern "C" void CompareInfo_ToString_m7505 ();
extern "C" void CompareInfo_get_LCID_m7506 ();
extern "C" void CultureInfo__ctor_m7507 ();
extern "C" void CultureInfo__ctor_m7508 ();
extern "C" void CultureInfo__ctor_m7509 ();
extern "C" void CultureInfo__ctor_m7510 ();
extern "C" void CultureInfo__ctor_m7511 ();
extern "C" void CultureInfo__cctor_m7512 ();
extern "C" void CultureInfo_get_InvariantCulture_m4644 ();
extern "C" void CultureInfo_get_CurrentCulture_m4712 ();
extern "C" void CultureInfo_get_CurrentUICulture_m4714 ();
extern "C" void CultureInfo_ConstructCurrentCulture_m7513 ();
extern "C" void CultureInfo_ConstructCurrentUICulture_m7514 ();
extern "C" void CultureInfo_get_LCID_m7515 ();
extern "C" void CultureInfo_get_Name_m7516 ();
extern "C" void CultureInfo_get_Parent_m7517 ();
extern "C" void CultureInfo_get_TextInfo_m7518 ();
extern "C" void CultureInfo_get_IcuName_m7519 ();
extern "C" void CultureInfo_Clone_m7520 ();
extern "C" void CultureInfo_Equals_m7521 ();
extern "C" void CultureInfo_GetHashCode_m7522 ();
extern "C" void CultureInfo_ToString_m7523 ();
extern "C" void CultureInfo_get_CompareInfo_m7524 ();
extern "C" void CultureInfo_get_IsNeutralCulture_m7525 ();
extern "C" void CultureInfo_CheckNeutral_m7526 ();
extern "C" void CultureInfo_get_NumberFormat_m7527 ();
extern "C" void CultureInfo_set_NumberFormat_m7528 ();
extern "C" void CultureInfo_get_DateTimeFormat_m7529 ();
extern "C" void CultureInfo_set_DateTimeFormat_m7530 ();
extern "C" void CultureInfo_get_IsReadOnly_m7531 ();
extern "C" void CultureInfo_GetFormat_m7532 ();
extern "C" void CultureInfo_Construct_m7533 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromName_m7534 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromLcid_m7535 ();
extern "C" void CultureInfo_ConstructInternalLocaleFromCurrentLocale_m7536 ();
extern "C" void CultureInfo_construct_internal_locale_from_lcid_m7537 ();
extern "C" void CultureInfo_construct_internal_locale_from_name_m7538 ();
extern "C" void CultureInfo_construct_internal_locale_from_current_locale_m7539 ();
extern "C" void CultureInfo_construct_datetime_format_m7540 ();
extern "C" void CultureInfo_construct_number_format_m7541 ();
extern "C" void CultureInfo_ConstructInvariant_m7542 ();
extern "C" void CultureInfo_CreateTextInfo_m7543 ();
extern "C" void CultureInfo_CreateCulture_m7544 ();
extern "C" void DateTimeFormatInfo__ctor_m7545 ();
extern "C" void DateTimeFormatInfo__ctor_m7546 ();
extern "C" void DateTimeFormatInfo__cctor_m7547 ();
extern "C" void DateTimeFormatInfo_GetInstance_m7548 ();
extern "C" void DateTimeFormatInfo_get_IsReadOnly_m7549 ();
extern "C" void DateTimeFormatInfo_ReadOnly_m7550 ();
extern "C" void DateTimeFormatInfo_Clone_m7551 ();
extern "C" void DateTimeFormatInfo_GetFormat_m7552 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedMonthName_m7553 ();
extern "C" void DateTimeFormatInfo_GetEraName_m7554 ();
extern "C" void DateTimeFormatInfo_GetMonthName_m7555 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedDayNames_m7556 ();
extern "C" void DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m7557 ();
extern "C" void DateTimeFormatInfo_get_RawDayNames_m7558 ();
extern "C" void DateTimeFormatInfo_get_RawMonthNames_m7559 ();
extern "C" void DateTimeFormatInfo_get_AMDesignator_m7560 ();
extern "C" void DateTimeFormatInfo_get_PMDesignator_m7561 ();
extern "C" void DateTimeFormatInfo_get_DateSeparator_m7562 ();
extern "C" void DateTimeFormatInfo_get_TimeSeparator_m7563 ();
extern "C" void DateTimeFormatInfo_get_LongDatePattern_m7564 ();
extern "C" void DateTimeFormatInfo_get_ShortDatePattern_m7565 ();
extern "C" void DateTimeFormatInfo_get_ShortTimePattern_m7566 ();
extern "C" void DateTimeFormatInfo_get_LongTimePattern_m7567 ();
extern "C" void DateTimeFormatInfo_get_MonthDayPattern_m7568 ();
extern "C" void DateTimeFormatInfo_get_YearMonthPattern_m7569 ();
extern "C" void DateTimeFormatInfo_get_FullDateTimePattern_m7570 ();
extern "C" void DateTimeFormatInfo_get_CurrentInfo_m7571 ();
extern "C" void DateTimeFormatInfo_get_InvariantInfo_m7572 ();
extern "C" void DateTimeFormatInfo_get_Calendar_m7573 ();
extern "C" void DateTimeFormatInfo_set_Calendar_m7574 ();
extern "C" void DateTimeFormatInfo_get_RFC1123Pattern_m7575 ();
extern "C" void DateTimeFormatInfo_get_RoundtripPattern_m7576 ();
extern "C" void DateTimeFormatInfo_get_SortableDateTimePattern_m7577 ();
extern "C" void DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m7578 ();
extern "C" void DateTimeFormatInfo_GetAllDateTimePatternsInternal_m7579 ();
extern "C" void DateTimeFormatInfo_FillAllDateTimePatterns_m7580 ();
extern "C" void DateTimeFormatInfo_GetAllRawDateTimePatterns_m7581 ();
extern "C" void DateTimeFormatInfo_GetDayName_m7582 ();
extern "C" void DateTimeFormatInfo_GetAbbreviatedDayName_m7583 ();
extern "C" void DateTimeFormatInfo_FillInvariantPatterns_m7584 ();
extern "C" void DateTimeFormatInfo_PopulateCombinedList_m7585 ();
extern "C" void DaylightTime__ctor_m7586 ();
extern "C" void DaylightTime_get_Start_m7587 ();
extern "C" void DaylightTime_get_End_m7588 ();
extern "C" void DaylightTime_get_Delta_m7589 ();
extern "C" void GregorianCalendar__ctor_m7590 ();
extern "C" void GregorianCalendar__ctor_m7591 ();
extern "C" void GregorianCalendar_get_Eras_m7592 ();
extern "C" void GregorianCalendar_set_CalendarType_m7593 ();
extern "C" void GregorianCalendar_GetDayOfMonth_m7594 ();
extern "C" void GregorianCalendar_GetDayOfWeek_m7595 ();
extern "C" void GregorianCalendar_GetEra_m7596 ();
extern "C" void GregorianCalendar_GetMonth_m7597 ();
extern "C" void GregorianCalendar_GetYear_m7598 ();
extern "C" void NumberFormatInfo__ctor_m7599 ();
extern "C" void NumberFormatInfo__ctor_m7600 ();
extern "C" void NumberFormatInfo__ctor_m7601 ();
extern "C" void NumberFormatInfo__cctor_m7602 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalDigits_m7603 ();
extern "C" void NumberFormatInfo_get_CurrencyDecimalSeparator_m7604 ();
extern "C" void NumberFormatInfo_get_CurrencyGroupSeparator_m7605 ();
extern "C" void NumberFormatInfo_get_RawCurrencyGroupSizes_m7606 ();
extern "C" void NumberFormatInfo_get_CurrencyNegativePattern_m7607 ();
extern "C" void NumberFormatInfo_get_CurrencyPositivePattern_m7608 ();
extern "C" void NumberFormatInfo_get_CurrencySymbol_m7609 ();
extern "C" void NumberFormatInfo_get_CurrentInfo_m7610 ();
extern "C" void NumberFormatInfo_get_InvariantInfo_m7611 ();
extern "C" void NumberFormatInfo_get_NaNSymbol_m7612 ();
extern "C" void NumberFormatInfo_get_NegativeInfinitySymbol_m7613 ();
extern "C" void NumberFormatInfo_get_NegativeSign_m7614 ();
extern "C" void NumberFormatInfo_get_NumberDecimalDigits_m7615 ();
extern "C" void NumberFormatInfo_get_NumberDecimalSeparator_m7616 ();
extern "C" void NumberFormatInfo_get_NumberGroupSeparator_m7617 ();
extern "C" void NumberFormatInfo_get_RawNumberGroupSizes_m7618 ();
extern "C" void NumberFormatInfo_get_NumberNegativePattern_m7619 ();
extern "C" void NumberFormatInfo_set_NumberNegativePattern_m7620 ();
extern "C" void NumberFormatInfo_get_PercentDecimalDigits_m7621 ();
extern "C" void NumberFormatInfo_get_PercentDecimalSeparator_m7622 ();
extern "C" void NumberFormatInfo_get_PercentGroupSeparator_m7623 ();
extern "C" void NumberFormatInfo_get_RawPercentGroupSizes_m7624 ();
extern "C" void NumberFormatInfo_get_PercentNegativePattern_m7625 ();
extern "C" void NumberFormatInfo_get_PercentPositivePattern_m7626 ();
extern "C" void NumberFormatInfo_get_PercentSymbol_m7627 ();
extern "C" void NumberFormatInfo_get_PerMilleSymbol_m7628 ();
extern "C" void NumberFormatInfo_get_PositiveInfinitySymbol_m7629 ();
extern "C" void NumberFormatInfo_get_PositiveSign_m7630 ();
extern "C" void NumberFormatInfo_GetFormat_m7631 ();
extern "C" void NumberFormatInfo_Clone_m7632 ();
extern "C" void NumberFormatInfo_GetInstance_m7633 ();
extern "C" void TextInfo__ctor_m7634 ();
extern "C" void TextInfo__ctor_m7635 ();
extern "C" void TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7636 ();
extern "C" void TextInfo_get_ListSeparator_m7637 ();
extern "C" void TextInfo_get_CultureName_m7638 ();
extern "C" void TextInfo_Equals_m7639 ();
extern "C" void TextInfo_GetHashCode_m7640 ();
extern "C" void TextInfo_ToString_m7641 ();
extern "C" void TextInfo_ToLower_m7642 ();
extern "C" void TextInfo_ToUpper_m7643 ();
extern "C" void TextInfo_ToLower_m7644 ();
extern "C" void TextInfo_Clone_m7645 ();
extern "C" void IsolatedStorageException__ctor_m7646 ();
extern "C" void IsolatedStorageException__ctor_m7647 ();
extern "C" void IsolatedStorageException__ctor_m7648 ();
extern "C" void BinaryReader__ctor_m7649 ();
extern "C" void BinaryReader__ctor_m7650 ();
extern "C" void BinaryReader_System_IDisposable_Dispose_m7651 ();
extern "C" void BinaryReader_get_BaseStream_m7652 ();
extern "C" void BinaryReader_Close_m7653 ();
extern "C" void BinaryReader_Dispose_m7654 ();
extern "C" void BinaryReader_FillBuffer_m7655 ();
extern "C" void BinaryReader_Read_m7656 ();
extern "C" void BinaryReader_Read_m7657 ();
extern "C" void BinaryReader_Read_m7658 ();
extern "C" void BinaryReader_ReadCharBytes_m7659 ();
extern "C" void BinaryReader_Read7BitEncodedInt_m7660 ();
extern "C" void BinaryReader_ReadBoolean_m7661 ();
extern "C" void BinaryReader_ReadByte_m7662 ();
extern "C" void BinaryReader_ReadBytes_m7663 ();
extern "C" void BinaryReader_ReadChar_m7664 ();
extern "C" void BinaryReader_ReadDecimal_m7665 ();
extern "C" void BinaryReader_ReadDouble_m7666 ();
extern "C" void BinaryReader_ReadInt16_m7667 ();
extern "C" void BinaryReader_ReadInt32_m7668 ();
extern "C" void BinaryReader_ReadInt64_m7669 ();
extern "C" void BinaryReader_ReadSByte_m7670 ();
extern "C" void BinaryReader_ReadString_m7671 ();
extern "C" void BinaryReader_ReadSingle_m7672 ();
extern "C" void BinaryReader_ReadUInt16_m7673 ();
extern "C" void BinaryReader_ReadUInt32_m7674 ();
extern "C" void BinaryReader_ReadUInt64_m7675 ();
extern "C" void BinaryReader_CheckBuffer_m7676 ();
extern "C" void Directory_CreateDirectory_m4697 ();
extern "C" void Directory_CreateDirectoriesInternal_m7677 ();
extern "C" void Directory_Exists_m4696 ();
extern "C" void Directory_GetCurrentDirectory_m7678 ();
extern "C" void Directory_GetFiles_m4699 ();
extern "C" void Directory_GetFileSystemEntries_m7679 ();
extern "C" void DirectoryInfo__ctor_m7680 ();
extern "C" void DirectoryInfo__ctor_m7681 ();
extern "C" void DirectoryInfo__ctor_m7682 ();
extern "C" void DirectoryInfo_Initialize_m7683 ();
extern "C" void DirectoryInfo_get_Exists_m7684 ();
extern "C" void DirectoryInfo_get_Parent_m7685 ();
extern "C" void DirectoryInfo_Create_m7686 ();
extern "C" void DirectoryInfo_ToString_m7687 ();
extern "C" void DirectoryNotFoundException__ctor_m7688 ();
extern "C" void DirectoryNotFoundException__ctor_m7689 ();
extern "C" void DirectoryNotFoundException__ctor_m7690 ();
extern "C" void EndOfStreamException__ctor_m7691 ();
extern "C" void EndOfStreamException__ctor_m7692 ();
extern "C" void File_Delete_m7693 ();
extern "C" void File_Exists_m7694 ();
extern "C" void File_Open_m7695 ();
extern "C" void File_OpenRead_m4695 ();
extern "C" void File_OpenText_m7696 ();
extern "C" void FileNotFoundException__ctor_m7697 ();
extern "C" void FileNotFoundException__ctor_m7698 ();
extern "C" void FileNotFoundException__ctor_m7699 ();
extern "C" void FileNotFoundException_get_Message_m7700 ();
extern "C" void FileNotFoundException_GetObjectData_m7701 ();
extern "C" void FileNotFoundException_ToString_m7702 ();
extern "C" void ReadDelegate__ctor_m7703 ();
extern "C" void ReadDelegate_Invoke_m7704 ();
extern "C" void ReadDelegate_BeginInvoke_m7705 ();
extern "C" void ReadDelegate_EndInvoke_m7706 ();
extern "C" void WriteDelegate__ctor_m7707 ();
extern "C" void WriteDelegate_Invoke_m7708 ();
extern "C" void WriteDelegate_BeginInvoke_m7709 ();
extern "C" void WriteDelegate_EndInvoke_m7710 ();
extern "C" void FileStream__ctor_m7711 ();
extern "C" void FileStream__ctor_m7712 ();
extern "C" void FileStream__ctor_m7713 ();
extern "C" void FileStream__ctor_m7714 ();
extern "C" void FileStream__ctor_m7715 ();
extern "C" void FileStream_get_CanRead_m7716 ();
extern "C" void FileStream_get_CanWrite_m7717 ();
extern "C" void FileStream_get_CanSeek_m7718 ();
extern "C" void FileStream_get_Length_m7719 ();
extern "C" void FileStream_get_Position_m7720 ();
extern "C" void FileStream_set_Position_m7721 ();
extern "C" void FileStream_ReadByte_m7722 ();
extern "C" void FileStream_WriteByte_m7723 ();
extern "C" void FileStream_Read_m7724 ();
extern "C" void FileStream_ReadInternal_m7725 ();
extern "C" void FileStream_BeginRead_m7726 ();
extern "C" void FileStream_EndRead_m7727 ();
extern "C" void FileStream_Write_m7728 ();
extern "C" void FileStream_WriteInternal_m7729 ();
extern "C" void FileStream_BeginWrite_m7730 ();
extern "C" void FileStream_EndWrite_m7731 ();
extern "C" void FileStream_Seek_m7732 ();
extern "C" void FileStream_SetLength_m7733 ();
extern "C" void FileStream_Flush_m7734 ();
extern "C" void FileStream_Finalize_m7735 ();
extern "C" void FileStream_Dispose_m7736 ();
extern "C" void FileStream_ReadSegment_m7737 ();
extern "C" void FileStream_WriteSegment_m7738 ();
extern "C" void FileStream_FlushBuffer_m7739 ();
extern "C" void FileStream_FlushBuffer_m7740 ();
extern "C" void FileStream_FlushBufferIfDirty_m7741 ();
extern "C" void FileStream_RefillBuffer_m7742 ();
extern "C" void FileStream_ReadData_m7743 ();
extern "C" void FileStream_InitBuffer_m7744 ();
extern "C" void FileStream_GetSecureFileName_m7745 ();
extern "C" void FileStream_GetSecureFileName_m7746 ();
extern "C" void FileStreamAsyncResult__ctor_m7747 ();
extern "C" void FileStreamAsyncResult_CBWrapper_m7748 ();
extern "C" void FileStreamAsyncResult_get_AsyncState_m7749 ();
extern "C" void FileStreamAsyncResult_get_AsyncWaitHandle_m7750 ();
extern "C" void FileStreamAsyncResult_get_IsCompleted_m7751 ();
extern "C" void FileSystemInfo__ctor_m7752 ();
extern "C" void FileSystemInfo__ctor_m7753 ();
extern "C" void FileSystemInfo_GetObjectData_m7754 ();
extern "C" void FileSystemInfo_get_FullName_m7755 ();
extern "C" void FileSystemInfo_Refresh_m7756 ();
extern "C" void FileSystemInfo_InternalRefresh_m7757 ();
extern "C" void FileSystemInfo_CheckPath_m7758 ();
extern "C" void IOException__ctor_m7759 ();
extern "C" void IOException__ctor_m7760 ();
extern "C" void IOException__ctor_m4745 ();
extern "C" void IOException__ctor_m7761 ();
extern "C" void IOException__ctor_m7762 ();
extern "C" void MemoryStream__ctor_m4746 ();
extern "C" void MemoryStream__ctor_m4751 ();
extern "C" void MemoryStream__ctor_m4752 ();
extern "C" void MemoryStream_InternalConstructor_m7763 ();
extern "C" void MemoryStream_CheckIfClosedThrowDisposed_m7764 ();
extern "C" void MemoryStream_get_CanRead_m7765 ();
extern "C" void MemoryStream_get_CanSeek_m7766 ();
extern "C" void MemoryStream_get_CanWrite_m7767 ();
extern "C" void MemoryStream_set_Capacity_m7768 ();
extern "C" void MemoryStream_get_Length_m7769 ();
extern "C" void MemoryStream_get_Position_m7770 ();
extern "C" void MemoryStream_set_Position_m7771 ();
extern "C" void MemoryStream_Dispose_m7772 ();
extern "C" void MemoryStream_Flush_m7773 ();
extern "C" void MemoryStream_Read_m7774 ();
extern "C" void MemoryStream_ReadByte_m7775 ();
extern "C" void MemoryStream_Seek_m7776 ();
extern "C" void MemoryStream_CalculateNewCapacity_m7777 ();
extern "C" void MemoryStream_Expand_m7778 ();
extern "C" void MemoryStream_SetLength_m7779 ();
extern "C" void MemoryStream_ToArray_m7780 ();
extern "C" void MemoryStream_Write_m7781 ();
extern "C" void MemoryStream_WriteByte_m7782 ();
extern "C" void MonoIO__cctor_m7783 ();
extern "C" void MonoIO_GetException_m7784 ();
extern "C" void MonoIO_GetException_m7785 ();
extern "C" void MonoIO_CreateDirectory_m7786 ();
extern "C" void MonoIO_GetFileSystemEntries_m7787 ();
extern "C" void MonoIO_GetCurrentDirectory_m7788 ();
extern "C" void MonoIO_DeleteFile_m7789 ();
extern "C" void MonoIO_GetFileAttributes_m7790 ();
extern "C" void MonoIO_GetFileType_m7791 ();
extern "C" void MonoIO_ExistsFile_m7792 ();
extern "C" void MonoIO_ExistsDirectory_m7793 ();
extern "C" void MonoIO_GetFileStat_m7794 ();
extern "C" void MonoIO_Open_m7795 ();
extern "C" void MonoIO_Close_m7796 ();
extern "C" void MonoIO_Read_m7797 ();
extern "C" void MonoIO_Write_m7798 ();
extern "C" void MonoIO_Seek_m7799 ();
extern "C" void MonoIO_GetLength_m7800 ();
extern "C" void MonoIO_SetLength_m7801 ();
extern "C" void MonoIO_get_ConsoleOutput_m7802 ();
extern "C" void MonoIO_get_ConsoleInput_m7803 ();
extern "C" void MonoIO_get_ConsoleError_m7804 ();
extern "C" void MonoIO_get_VolumeSeparatorChar_m7805 ();
extern "C" void MonoIO_get_DirectorySeparatorChar_m7806 ();
extern "C" void MonoIO_get_AltDirectorySeparatorChar_m7807 ();
extern "C" void MonoIO_get_PathSeparator_m7808 ();
extern "C" void Path__cctor_m7809 ();
extern "C" void Path_Combine_m4698 ();
extern "C" void Path_CleanPath_m7810 ();
extern "C" void Path_GetDirectoryName_m7811 ();
extern "C" void Path_GetFileName_m7812 ();
extern "C" void Path_GetFullPath_m7813 ();
extern "C" void Path_WindowsDriveAdjustment_m7814 ();
extern "C" void Path_InsecureGetFullPath_m7815 ();
extern "C" void Path_IsDsc_m7816 ();
extern "C" void Path_GetPathRoot_m7817 ();
extern "C" void Path_IsPathRooted_m7818 ();
extern "C" void Path_GetInvalidPathChars_m7819 ();
extern "C" void Path_GetServerAndShare_m7820 ();
extern "C" void Path_SameRoot_m7821 ();
extern "C" void Path_CanonicalizePath_m7822 ();
extern "C" void PathTooLongException__ctor_m7823 ();
extern "C" void PathTooLongException__ctor_m7824 ();
extern "C" void PathTooLongException__ctor_m7825 ();
extern "C" void SearchPattern__cctor_m7826 ();
extern "C" void Stream__ctor_m4747 ();
extern "C" void Stream__cctor_m7827 ();
extern "C" void Stream_Dispose_m7828 ();
extern "C" void Stream_Dispose_m4750 ();
extern "C" void Stream_Close_m4749 ();
extern "C" void Stream_ReadByte_m7829 ();
extern "C" void Stream_WriteByte_m7830 ();
extern "C" void Stream_BeginRead_m7831 ();
extern "C" void Stream_BeginWrite_m7832 ();
extern "C" void Stream_EndRead_m7833 ();
extern "C" void Stream_EndWrite_m7834 ();
extern "C" void NullStream__ctor_m7835 ();
extern "C" void NullStream_get_CanRead_m7836 ();
extern "C" void NullStream_get_CanSeek_m7837 ();
extern "C" void NullStream_get_CanWrite_m7838 ();
extern "C" void NullStream_get_Length_m7839 ();
extern "C" void NullStream_get_Position_m7840 ();
extern "C" void NullStream_set_Position_m7841 ();
extern "C" void NullStream_Flush_m7842 ();
extern "C" void NullStream_Read_m7843 ();
extern "C" void NullStream_ReadByte_m7844 ();
extern "C" void NullStream_Seek_m7845 ();
extern "C" void NullStream_SetLength_m7846 ();
extern "C" void NullStream_Write_m7847 ();
extern "C" void NullStream_WriteByte_m7848 ();
extern "C" void StreamAsyncResult__ctor_m7849 ();
extern "C" void StreamAsyncResult_SetComplete_m7850 ();
extern "C" void StreamAsyncResult_SetComplete_m7851 ();
extern "C" void StreamAsyncResult_get_AsyncState_m7852 ();
extern "C" void StreamAsyncResult_get_AsyncWaitHandle_m7853 ();
extern "C" void StreamAsyncResult_get_IsCompleted_m7854 ();
extern "C" void StreamAsyncResult_get_Exception_m7855 ();
extern "C" void StreamAsyncResult_get_NBytes_m7856 ();
extern "C" void StreamAsyncResult_get_Done_m7857 ();
extern "C" void StreamAsyncResult_set_Done_m7858 ();
extern "C" void NullStreamReader__ctor_m7859 ();
extern "C" void NullStreamReader_Peek_m7860 ();
extern "C" void NullStreamReader_Read_m7861 ();
extern "C" void NullStreamReader_Read_m7862 ();
extern "C" void NullStreamReader_ReadLine_m7863 ();
extern "C" void NullStreamReader_ReadToEnd_m7864 ();
extern "C" void StreamReader__ctor_m7865 ();
extern "C" void StreamReader__ctor_m7866 ();
extern "C" void StreamReader__ctor_m7867 ();
extern "C" void StreamReader__ctor_m7868 ();
extern "C" void StreamReader__ctor_m7869 ();
extern "C" void StreamReader__cctor_m7870 ();
extern "C" void StreamReader_Initialize_m7871 ();
extern "C" void StreamReader_Dispose_m7872 ();
extern "C" void StreamReader_DoChecks_m7873 ();
extern "C" void StreamReader_ReadBuffer_m7874 ();
extern "C" void StreamReader_Peek_m7875 ();
extern "C" void StreamReader_Read_m7876 ();
extern "C" void StreamReader_Read_m7877 ();
extern "C" void StreamReader_FindNextEOL_m7878 ();
extern "C" void StreamReader_ReadLine_m7879 ();
extern "C" void StreamReader_ReadToEnd_m7880 ();
extern "C" void StreamWriter__ctor_m7881 ();
extern "C" void StreamWriter__ctor_m7882 ();
extern "C" void StreamWriter__cctor_m7883 ();
extern "C" void StreamWriter_Initialize_m7884 ();
extern "C" void StreamWriter_set_AutoFlush_m7885 ();
extern "C" void StreamWriter_Dispose_m7886 ();
extern "C" void StreamWriter_Flush_m7887 ();
extern "C" void StreamWriter_FlushBytes_m7888 ();
extern "C" void StreamWriter_Decode_m7889 ();
extern "C" void StreamWriter_Write_m7890 ();
extern "C" void StreamWriter_LowLevelWrite_m7891 ();
extern "C" void StreamWriter_LowLevelWrite_m7892 ();
extern "C" void StreamWriter_Write_m7893 ();
extern "C" void StreamWriter_Write_m7894 ();
extern "C" void StreamWriter_Write_m7895 ();
extern "C" void StreamWriter_Close_m7896 ();
extern "C" void StreamWriter_Finalize_m7897 ();
extern "C" void StringReader__ctor_m7898 ();
extern "C" void StringReader_Dispose_m7899 ();
extern "C" void StringReader_Peek_m7900 ();
extern "C" void StringReader_Read_m7901 ();
extern "C" void StringReader_Read_m7902 ();
extern "C" void StringReader_ReadLine_m7903 ();
extern "C" void StringReader_ReadToEnd_m7904 ();
extern "C" void StringReader_CheckObjectDisposedException_m7905 ();
extern "C" void NullTextReader__ctor_m7906 ();
extern "C" void NullTextReader_ReadLine_m7907 ();
extern "C" void TextReader__ctor_m7908 ();
extern "C" void TextReader__cctor_m7909 ();
extern "C" void TextReader_Dispose_m7910 ();
extern "C" void TextReader_Dispose_m7911 ();
extern "C" void TextReader_Peek_m7912 ();
extern "C" void TextReader_Read_m7913 ();
extern "C" void TextReader_Read_m7914 ();
extern "C" void TextReader_ReadLine_m7915 ();
extern "C" void TextReader_ReadToEnd_m7916 ();
extern "C" void TextReader_Synchronized_m7917 ();
extern "C" void SynchronizedReader__ctor_m7918 ();
extern "C" void SynchronizedReader_Peek_m7919 ();
extern "C" void SynchronizedReader_ReadLine_m7920 ();
extern "C" void SynchronizedReader_ReadToEnd_m7921 ();
extern "C" void SynchronizedReader_Read_m7922 ();
extern "C" void SynchronizedReader_Read_m7923 ();
extern "C" void NullTextWriter__ctor_m7924 ();
extern "C" void NullTextWriter_Write_m7925 ();
extern "C" void NullTextWriter_Write_m7926 ();
extern "C" void NullTextWriter_Write_m7927 ();
extern "C" void TextWriter__ctor_m7928 ();
extern "C" void TextWriter__cctor_m7929 ();
extern "C" void TextWriter_Close_m7930 ();
extern "C" void TextWriter_Dispose_m7931 ();
extern "C" void TextWriter_Dispose_m7932 ();
extern "C" void TextWriter_Flush_m7933 ();
extern "C" void TextWriter_Synchronized_m7934 ();
extern "C" void TextWriter_Write_m7935 ();
extern "C" void TextWriter_Write_m7936 ();
extern "C" void TextWriter_Write_m7937 ();
extern "C" void TextWriter_Write_m7938 ();
extern "C" void TextWriter_WriteLine_m7939 ();
extern "C" void TextWriter_WriteLine_m7940 ();
extern "C" void SynchronizedWriter__ctor_m7941 ();
extern "C" void SynchronizedWriter_Close_m7942 ();
extern "C" void SynchronizedWriter_Flush_m7943 ();
extern "C" void SynchronizedWriter_Write_m7944 ();
extern "C" void SynchronizedWriter_Write_m7945 ();
extern "C" void SynchronizedWriter_Write_m7946 ();
extern "C" void SynchronizedWriter_Write_m7947 ();
extern "C" void SynchronizedWriter_WriteLine_m7948 ();
extern "C" void SynchronizedWriter_WriteLine_m7949 ();
extern "C" void UnexceptionalStreamReader__ctor_m7950 ();
extern "C" void UnexceptionalStreamReader__cctor_m7951 ();
extern "C" void UnexceptionalStreamReader_Peek_m7952 ();
extern "C" void UnexceptionalStreamReader_Read_m7953 ();
extern "C" void UnexceptionalStreamReader_Read_m7954 ();
extern "C" void UnexceptionalStreamReader_CheckEOL_m7955 ();
extern "C" void UnexceptionalStreamReader_ReadLine_m7956 ();
extern "C" void UnexceptionalStreamReader_ReadToEnd_m7957 ();
extern "C" void UnexceptionalStreamWriter__ctor_m7958 ();
extern "C" void UnexceptionalStreamWriter_Flush_m7959 ();
extern "C" void UnexceptionalStreamWriter_Write_m7960 ();
extern "C" void UnexceptionalStreamWriter_Write_m7961 ();
extern "C" void UnexceptionalStreamWriter_Write_m7962 ();
extern "C" void UnexceptionalStreamWriter_Write_m7963 ();
extern "C" void UnmanagedMemoryStream_get_CanRead_m7964 ();
extern "C" void UnmanagedMemoryStream_get_CanSeek_m7965 ();
extern "C" void UnmanagedMemoryStream_get_CanWrite_m7966 ();
extern "C" void UnmanagedMemoryStream_get_Length_m7967 ();
extern "C" void UnmanagedMemoryStream_get_Position_m7968 ();
extern "C" void UnmanagedMemoryStream_set_Position_m7969 ();
extern "C" void UnmanagedMemoryStream_Read_m7970 ();
extern "C" void UnmanagedMemoryStream_ReadByte_m7971 ();
extern "C" void UnmanagedMemoryStream_Seek_m7972 ();
extern "C" void UnmanagedMemoryStream_SetLength_m7973 ();
extern "C" void UnmanagedMemoryStream_Flush_m7974 ();
extern "C" void UnmanagedMemoryStream_Dispose_m7975 ();
extern "C" void UnmanagedMemoryStream_Write_m7976 ();
extern "C" void UnmanagedMemoryStream_WriteByte_m7977 ();
extern "C" void AssemblyBuilder_get_Location_m7978 ();
extern "C" void AssemblyBuilder_GetModulesInternal_m7979 ();
extern "C" void AssemblyBuilder_GetTypes_m7980 ();
extern "C" void AssemblyBuilder_get_IsCompilerContext_m7981 ();
extern "C" void AssemblyBuilder_not_supported_m7982 ();
extern "C" void AssemblyBuilder_UnprotectedGetName_m7983 ();
extern "C" void ConstructorBuilder__ctor_m7984 ();
extern "C" void ConstructorBuilder_get_CallingConvention_m7985 ();
extern "C" void ConstructorBuilder_get_TypeBuilder_m7986 ();
extern "C" void ConstructorBuilder_GetParameters_m7987 ();
extern "C" void ConstructorBuilder_GetParametersInternal_m7988 ();
extern "C" void ConstructorBuilder_GetParameterCount_m7989 ();
extern "C" void ConstructorBuilder_Invoke_m7990 ();
extern "C" void ConstructorBuilder_Invoke_m7991 ();
extern "C" void ConstructorBuilder_get_MethodHandle_m7992 ();
extern "C" void ConstructorBuilder_get_Attributes_m7993 ();
extern "C" void ConstructorBuilder_get_ReflectedType_m7994 ();
extern "C" void ConstructorBuilder_get_DeclaringType_m7995 ();
extern "C" void ConstructorBuilder_get_Name_m7996 ();
extern "C" void ConstructorBuilder_IsDefined_m7997 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m7998 ();
extern "C" void ConstructorBuilder_GetCustomAttributes_m7999 ();
extern "C" void ConstructorBuilder_GetILGenerator_m8000 ();
extern "C" void ConstructorBuilder_GetILGenerator_m8001 ();
extern "C" void ConstructorBuilder_GetToken_m8002 ();
extern "C" void ConstructorBuilder_get_Module_m8003 ();
extern "C" void ConstructorBuilder_ToString_m8004 ();
extern "C" void ConstructorBuilder_fixup_m8005 ();
extern "C" void ConstructorBuilder_get_next_table_index_m8006 ();
extern "C" void ConstructorBuilder_get_IsCompilerContext_m8007 ();
extern "C" void ConstructorBuilder_not_supported_m8008 ();
extern "C" void ConstructorBuilder_not_created_m8009 ();
extern "C" void EnumBuilder_get_Assembly_m8010 ();
extern "C" void EnumBuilder_get_AssemblyQualifiedName_m8011 ();
extern "C" void EnumBuilder_get_BaseType_m8012 ();
extern "C" void EnumBuilder_get_DeclaringType_m8013 ();
extern "C" void EnumBuilder_get_FullName_m8014 ();
extern "C" void EnumBuilder_get_Module_m8015 ();
extern "C" void EnumBuilder_get_Name_m8016 ();
extern "C" void EnumBuilder_get_Namespace_m8017 ();
extern "C" void EnumBuilder_get_ReflectedType_m8018 ();
extern "C" void EnumBuilder_get_TypeHandle_m8019 ();
extern "C" void EnumBuilder_get_UnderlyingSystemType_m8020 ();
extern "C" void EnumBuilder_GetAttributeFlagsImpl_m8021 ();
extern "C" void EnumBuilder_GetConstructorImpl_m8022 ();
extern "C" void EnumBuilder_GetConstructors_m8023 ();
extern "C" void EnumBuilder_GetCustomAttributes_m8024 ();
extern "C" void EnumBuilder_GetCustomAttributes_m8025 ();
extern "C" void EnumBuilder_GetElementType_m8026 ();
extern "C" void EnumBuilder_GetEvent_m8027 ();
extern "C" void EnumBuilder_GetField_m8028 ();
extern "C" void EnumBuilder_GetFields_m8029 ();
extern "C" void EnumBuilder_GetInterfaces_m8030 ();
extern "C" void EnumBuilder_GetMethodImpl_m8031 ();
extern "C" void EnumBuilder_GetMethods_m8032 ();
extern "C" void EnumBuilder_GetPropertyImpl_m8033 ();
extern "C" void EnumBuilder_HasElementTypeImpl_m8034 ();
extern "C" void EnumBuilder_InvokeMember_m8035 ();
extern "C" void EnumBuilder_IsArrayImpl_m8036 ();
extern "C" void EnumBuilder_IsByRefImpl_m8037 ();
extern "C" void EnumBuilder_IsPointerImpl_m8038 ();
extern "C" void EnumBuilder_IsPrimitiveImpl_m8039 ();
extern "C" void EnumBuilder_IsValueTypeImpl_m8040 ();
extern "C" void EnumBuilder_IsDefined_m8041 ();
extern "C" void EnumBuilder_CreateNotSupportedException_m8042 ();
extern "C" void FieldBuilder_get_Attributes_m8043 ();
extern "C" void FieldBuilder_get_DeclaringType_m8044 ();
extern "C" void FieldBuilder_get_FieldHandle_m8045 ();
extern "C" void FieldBuilder_get_FieldType_m8046 ();
extern "C" void FieldBuilder_get_Name_m8047 ();
extern "C" void FieldBuilder_get_ReflectedType_m8048 ();
extern "C" void FieldBuilder_GetCustomAttributes_m8049 ();
extern "C" void FieldBuilder_GetCustomAttributes_m8050 ();
extern "C" void FieldBuilder_GetValue_m8051 ();
extern "C" void FieldBuilder_IsDefined_m8052 ();
extern "C" void FieldBuilder_GetFieldOffset_m8053 ();
extern "C" void FieldBuilder_SetValue_m8054 ();
extern "C" void FieldBuilder_get_UMarshal_m8055 ();
extern "C" void FieldBuilder_CreateNotSupportedException_m8056 ();
extern "C" void FieldBuilder_get_Module_m8057 ();
extern "C" void GenericTypeParameterBuilder_IsSubclassOf_m8058 ();
extern "C" void GenericTypeParameterBuilder_GetAttributeFlagsImpl_m8059 ();
extern "C" void GenericTypeParameterBuilder_GetConstructorImpl_m8060 ();
extern "C" void GenericTypeParameterBuilder_GetConstructors_m8061 ();
extern "C" void GenericTypeParameterBuilder_GetEvent_m8062 ();
extern "C" void GenericTypeParameterBuilder_GetField_m8063 ();
extern "C" void GenericTypeParameterBuilder_GetFields_m8064 ();
extern "C" void GenericTypeParameterBuilder_GetInterfaces_m8065 ();
extern "C" void GenericTypeParameterBuilder_GetMethods_m8066 ();
extern "C" void GenericTypeParameterBuilder_GetMethodImpl_m8067 ();
extern "C" void GenericTypeParameterBuilder_GetPropertyImpl_m8068 ();
extern "C" void GenericTypeParameterBuilder_HasElementTypeImpl_m8069 ();
extern "C" void GenericTypeParameterBuilder_IsAssignableFrom_m8070 ();
extern "C" void GenericTypeParameterBuilder_IsInstanceOfType_m8071 ();
extern "C" void GenericTypeParameterBuilder_IsArrayImpl_m8072 ();
extern "C" void GenericTypeParameterBuilder_IsByRefImpl_m8073 ();
extern "C" void GenericTypeParameterBuilder_IsPointerImpl_m8074 ();
extern "C" void GenericTypeParameterBuilder_IsPrimitiveImpl_m8075 ();
extern "C" void GenericTypeParameterBuilder_IsValueTypeImpl_m8076 ();
extern "C" void GenericTypeParameterBuilder_InvokeMember_m8077 ();
extern "C" void GenericTypeParameterBuilder_GetElementType_m8078 ();
extern "C" void GenericTypeParameterBuilder_get_UnderlyingSystemType_m8079 ();
extern "C" void GenericTypeParameterBuilder_get_Assembly_m8080 ();
extern "C" void GenericTypeParameterBuilder_get_AssemblyQualifiedName_m8081 ();
extern "C" void GenericTypeParameterBuilder_get_BaseType_m8082 ();
extern "C" void GenericTypeParameterBuilder_get_FullName_m8083 ();
extern "C" void GenericTypeParameterBuilder_IsDefined_m8084 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m8085 ();
extern "C" void GenericTypeParameterBuilder_GetCustomAttributes_m8086 ();
extern "C" void GenericTypeParameterBuilder_get_Name_m8087 ();
extern "C" void GenericTypeParameterBuilder_get_Namespace_m8088 ();
extern "C" void GenericTypeParameterBuilder_get_Module_m8089 ();
extern "C" void GenericTypeParameterBuilder_get_DeclaringType_m8090 ();
extern "C" void GenericTypeParameterBuilder_get_ReflectedType_m8091 ();
extern "C" void GenericTypeParameterBuilder_get_TypeHandle_m8092 ();
extern "C" void GenericTypeParameterBuilder_GetGenericArguments_m8093 ();
extern "C" void GenericTypeParameterBuilder_GetGenericTypeDefinition_m8094 ();
extern "C" void GenericTypeParameterBuilder_get_ContainsGenericParameters_m8095 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericParameter_m8096 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericType_m8097 ();
extern "C" void GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m8098 ();
extern "C" void GenericTypeParameterBuilder_not_supported_m8099 ();
extern "C" void GenericTypeParameterBuilder_ToString_m8100 ();
extern "C" void GenericTypeParameterBuilder_Equals_m8101 ();
extern "C" void GenericTypeParameterBuilder_GetHashCode_m8102 ();
extern "C" void GenericTypeParameterBuilder_MakeGenericType_m8103 ();
extern "C" void ILGenerator__ctor_m8104 ();
extern "C" void ILGenerator__cctor_m8105 ();
extern "C" void ILGenerator_add_token_fixup_m8106 ();
extern "C" void ILGenerator_make_room_m8107 ();
extern "C" void ILGenerator_emit_int_m8108 ();
extern "C" void ILGenerator_ll_emit_m8109 ();
extern "C" void ILGenerator_Emit_m8110 ();
extern "C" void ILGenerator_Emit_m8111 ();
extern "C" void ILGenerator_label_fixup_m8112 ();
extern "C" void ILGenerator_Mono_GetCurrentOffset_m8113 ();
extern "C" void MethodBuilder_get_ContainsGenericParameters_m8114 ();
extern "C" void MethodBuilder_get_MethodHandle_m8115 ();
extern "C" void MethodBuilder_get_ReturnType_m8116 ();
extern "C" void MethodBuilder_get_ReflectedType_m8117 ();
extern "C" void MethodBuilder_get_DeclaringType_m8118 ();
extern "C" void MethodBuilder_get_Name_m8119 ();
extern "C" void MethodBuilder_get_Attributes_m8120 ();
extern "C" void MethodBuilder_get_CallingConvention_m8121 ();
extern "C" void MethodBuilder_GetBaseDefinition_m8122 ();
extern "C" void MethodBuilder_GetParameters_m8123 ();
extern "C" void MethodBuilder_GetParameterCount_m8124 ();
extern "C" void MethodBuilder_Invoke_m8125 ();
extern "C" void MethodBuilder_IsDefined_m8126 ();
extern "C" void MethodBuilder_GetCustomAttributes_m8127 ();
extern "C" void MethodBuilder_GetCustomAttributes_m8128 ();
extern "C" void MethodBuilder_check_override_m8129 ();
extern "C" void MethodBuilder_fixup_m8130 ();
extern "C" void MethodBuilder_ToString_m8131 ();
extern "C" void MethodBuilder_Equals_m8132 ();
extern "C" void MethodBuilder_GetHashCode_m8133 ();
extern "C" void MethodBuilder_get_next_table_index_m8134 ();
extern "C" void MethodBuilder_NotSupported_m8135 ();
extern "C" void MethodBuilder_MakeGenericMethod_m8136 ();
extern "C" void MethodBuilder_get_IsGenericMethodDefinition_m8137 ();
extern "C" void MethodBuilder_get_IsGenericMethod_m8138 ();
extern "C" void MethodBuilder_GetGenericArguments_m8139 ();
extern "C" void MethodBuilder_get_Module_m8140 ();
extern "C" void MethodToken__ctor_m8141 ();
extern "C" void MethodToken__cctor_m8142 ();
extern "C" void MethodToken_Equals_m8143 ();
extern "C" void MethodToken_GetHashCode_m8144 ();
extern "C" void MethodToken_get_Token_m8145 ();
extern "C" void ModuleBuilder__cctor_m8146 ();
extern "C" void ModuleBuilder_get_next_table_index_m8147 ();
extern "C" void ModuleBuilder_GetTypes_m8148 ();
extern "C" void ModuleBuilder_getToken_m8149 ();
extern "C" void ModuleBuilder_GetToken_m8150 ();
extern "C" void ModuleBuilder_RegisterToken_m8151 ();
extern "C" void ModuleBuilder_GetTokenGenerator_m8152 ();
extern "C" void ModuleBuilderTokenGenerator__ctor_m8153 ();
extern "C" void ModuleBuilderTokenGenerator_GetToken_m8154 ();
extern "C" void OpCode__ctor_m8155 ();
extern "C" void OpCode_GetHashCode_m8156 ();
extern "C" void OpCode_Equals_m8157 ();
extern "C" void OpCode_ToString_m8158 ();
extern "C" void OpCode_get_Name_m8159 ();
extern "C" void OpCode_get_Size_m8160 ();
extern "C" void OpCode_get_StackBehaviourPop_m8161 ();
extern "C" void OpCode_get_StackBehaviourPush_m8162 ();
extern "C" void OpCodeNames__cctor_m8163 ();
extern "C" void OpCodes__cctor_m8164 ();
extern "C" void ParameterBuilder_get_Attributes_m8165 ();
extern "C" void ParameterBuilder_get_Name_m8166 ();
extern "C" void ParameterBuilder_get_Position_m8167 ();
extern "C" void TypeBuilder_GetAttributeFlagsImpl_m8168 ();
extern "C" void TypeBuilder_setup_internal_class_m8169 ();
extern "C" void TypeBuilder_create_generic_class_m8170 ();
extern "C" void TypeBuilder_get_Assembly_m8171 ();
extern "C" void TypeBuilder_get_AssemblyQualifiedName_m8172 ();
extern "C" void TypeBuilder_get_BaseType_m8173 ();
extern "C" void TypeBuilder_get_DeclaringType_m8174 ();
extern "C" void TypeBuilder_get_UnderlyingSystemType_m8175 ();
extern "C" void TypeBuilder_get_FullName_m8176 ();
extern "C" void TypeBuilder_get_Module_m8177 ();
extern "C" void TypeBuilder_get_Name_m8178 ();
extern "C" void TypeBuilder_get_Namespace_m8179 ();
extern "C" void TypeBuilder_get_ReflectedType_m8180 ();
extern "C" void TypeBuilder_GetConstructorImpl_m8181 ();
extern "C" void TypeBuilder_IsDefined_m8182 ();
extern "C" void TypeBuilder_GetCustomAttributes_m8183 ();
extern "C" void TypeBuilder_GetCustomAttributes_m8184 ();
extern "C" void TypeBuilder_DefineConstructor_m8185 ();
extern "C" void TypeBuilder_DefineConstructor_m8186 ();
extern "C" void TypeBuilder_DefineDefaultConstructor_m8187 ();
extern "C" void TypeBuilder_create_runtime_class_m8188 ();
extern "C" void TypeBuilder_is_nested_in_m8189 ();
extern "C" void TypeBuilder_has_ctor_method_m8190 ();
extern "C" void TypeBuilder_CreateType_m8191 ();
extern "C" void TypeBuilder_GetConstructors_m8192 ();
extern "C" void TypeBuilder_GetConstructorsInternal_m8193 ();
extern "C" void TypeBuilder_GetElementType_m8194 ();
extern "C" void TypeBuilder_GetEvent_m8195 ();
extern "C" void TypeBuilder_GetField_m8196 ();
extern "C" void TypeBuilder_GetFields_m8197 ();
extern "C" void TypeBuilder_GetInterfaces_m8198 ();
extern "C" void TypeBuilder_GetMethodsByName_m8199 ();
extern "C" void TypeBuilder_GetMethods_m8200 ();
extern "C" void TypeBuilder_GetMethodImpl_m8201 ();
extern "C" void TypeBuilder_GetPropertyImpl_m8202 ();
extern "C" void TypeBuilder_HasElementTypeImpl_m8203 ();
extern "C" void TypeBuilder_InvokeMember_m8204 ();
extern "C" void TypeBuilder_IsArrayImpl_m8205 ();
extern "C" void TypeBuilder_IsByRefImpl_m8206 ();
extern "C" void TypeBuilder_IsPointerImpl_m8207 ();
extern "C" void TypeBuilder_IsPrimitiveImpl_m8208 ();
extern "C" void TypeBuilder_IsValueTypeImpl_m8209 ();
extern "C" void TypeBuilder_MakeGenericType_m8210 ();
extern "C" void TypeBuilder_get_TypeHandle_m8211 ();
extern "C" void TypeBuilder_SetParent_m8212 ();
extern "C" void TypeBuilder_get_next_table_index_m8213 ();
extern "C" void TypeBuilder_get_IsCompilerContext_m8214 ();
extern "C" void TypeBuilder_get_is_created_m8215 ();
extern "C" void TypeBuilder_not_supported_m8216 ();
extern "C" void TypeBuilder_check_not_created_m8217 ();
extern "C" void TypeBuilder_check_created_m8218 ();
extern "C" void TypeBuilder_ToString_m8219 ();
extern "C" void TypeBuilder_IsAssignableFrom_m8220 ();
extern "C" void TypeBuilder_IsSubclassOf_m8221 ();
extern "C" void TypeBuilder_IsAssignableTo_m8222 ();
extern "C" void TypeBuilder_GetGenericArguments_m8223 ();
extern "C" void TypeBuilder_GetGenericTypeDefinition_m8224 ();
extern "C" void TypeBuilder_get_ContainsGenericParameters_m8225 ();
extern "C" void TypeBuilder_get_IsGenericParameter_m8226 ();
extern "C" void TypeBuilder_get_IsGenericTypeDefinition_m8227 ();
extern "C" void TypeBuilder_get_IsGenericType_m8228 ();
extern "C" void UnmanagedMarshal_ToMarshalAsAttribute_m8229 ();
extern "C" void AmbiguousMatchException__ctor_m8230 ();
extern "C" void AmbiguousMatchException__ctor_m8231 ();
extern "C" void AmbiguousMatchException__ctor_m8232 ();
extern "C" void ResolveEventHolder__ctor_m8233 ();
extern "C" void Assembly__ctor_m8234 ();
extern "C" void Assembly_get_code_base_m8235 ();
extern "C" void Assembly_get_fullname_m8236 ();
extern "C" void Assembly_get_location_m8237 ();
extern "C" void Assembly_GetCodeBase_m8238 ();
extern "C" void Assembly_get_FullName_m8239 ();
extern "C" void Assembly_get_Location_m8240 ();
extern "C" void Assembly_IsDefined_m8241 ();
extern "C" void Assembly_GetCustomAttributes_m8242 ();
extern "C" void Assembly_GetManifestResourceInternal_m8243 ();
extern "C" void Assembly_GetTypes_m8244 ();
extern "C" void Assembly_GetTypes_m8245 ();
extern "C" void Assembly_GetType_m8246 ();
extern "C" void Assembly_GetType_m8247 ();
extern "C" void Assembly_InternalGetType_m8248 ();
extern "C" void Assembly_GetType_m8249 ();
extern "C" void Assembly_FillName_m8250 ();
extern "C" void Assembly_GetName_m8251 ();
extern "C" void Assembly_GetName_m8252 ();
extern "C" void Assembly_UnprotectedGetName_m8253 ();
extern "C" void Assembly_ToString_m8254 ();
extern "C" void Assembly_Load_m8255 ();
extern "C" void Assembly_GetModule_m8256 ();
extern "C" void Assembly_GetModulesInternal_m8257 ();
extern "C" void Assembly_GetModules_m8258 ();
extern "C" void Assembly_GetExecutingAssembly_m8259 ();
extern "C" void AssemblyCompanyAttribute__ctor_m8260 ();
extern "C" void AssemblyConfigurationAttribute__ctor_m8261 ();
extern "C" void AssemblyCopyrightAttribute__ctor_m8262 ();
extern "C" void AssemblyDefaultAliasAttribute__ctor_m8263 ();
extern "C" void AssemblyDelaySignAttribute__ctor_m8264 ();
extern "C" void AssemblyDescriptionAttribute__ctor_m8265 ();
extern "C" void AssemblyFileVersionAttribute__ctor_m8266 ();
extern "C" void AssemblyInformationalVersionAttribute__ctor_m8267 ();
extern "C" void AssemblyKeyFileAttribute__ctor_m8268 ();
extern "C" void AssemblyName__ctor_m8269 ();
extern "C" void AssemblyName__ctor_m8270 ();
extern "C" void AssemblyName_get_Name_m8271 ();
extern "C" void AssemblyName_get_Flags_m8272 ();
extern "C" void AssemblyName_get_FullName_m8273 ();
extern "C" void AssemblyName_get_Version_m8274 ();
extern "C" void AssemblyName_set_Version_m8275 ();
extern "C" void AssemblyName_ToString_m8276 ();
extern "C" void AssemblyName_get_IsPublicKeyValid_m8277 ();
extern "C" void AssemblyName_InternalGetPublicKeyToken_m8278 ();
extern "C" void AssemblyName_ComputePublicKeyToken_m8279 ();
extern "C" void AssemblyName_SetPublicKey_m8280 ();
extern "C" void AssemblyName_SetPublicKeyToken_m8281 ();
extern "C" void AssemblyName_GetObjectData_m8282 ();
extern "C" void AssemblyName_Clone_m8283 ();
extern "C" void AssemblyName_OnDeserialization_m8284 ();
extern "C" void AssemblyProductAttribute__ctor_m8285 ();
extern "C" void AssemblyTitleAttribute__ctor_m8286 ();
extern "C" void AssemblyTrademarkAttribute__ctor_m8287 ();
extern "C" void Default__ctor_m8288 ();
extern "C" void Default_BindToMethod_m8289 ();
extern "C" void Default_ReorderParameters_m8290 ();
extern "C" void Default_IsArrayAssignable_m8291 ();
extern "C" void Default_ChangeType_m8292 ();
extern "C" void Default_ReorderArgumentArray_m8293 ();
extern "C" void Default_check_type_m8294 ();
extern "C" void Default_check_arguments_m8295 ();
extern "C" void Default_SelectMethod_m8296 ();
extern "C" void Default_SelectMethod_m8297 ();
extern "C" void Default_GetBetterMethod_m8298 ();
extern "C" void Default_CompareCloserType_m8299 ();
extern "C" void Default_SelectProperty_m8300 ();
extern "C" void Default_check_arguments_with_score_m8301 ();
extern "C" void Default_check_type_with_score_m8302 ();
extern "C" void Binder__ctor_m8303 ();
extern "C" void Binder__cctor_m8304 ();
extern "C" void Binder_get_DefaultBinder_m8305 ();
extern "C" void Binder_ConvertArgs_m8306 ();
extern "C" void Binder_GetDerivedLevel_m8307 ();
extern "C" void Binder_FindMostDerivedMatch_m8308 ();
extern "C" void ConstructorInfo__ctor_m8309 ();
extern "C" void ConstructorInfo__cctor_m8310 ();
extern "C" void ConstructorInfo_get_MemberType_m8311 ();
extern "C" void ConstructorInfo_Invoke_m3663 ();
extern "C" void CustomAttributeData__ctor_m8312 ();
extern "C" void CustomAttributeData_get_Constructor_m8313 ();
extern "C" void CustomAttributeData_get_ConstructorArguments_m8314 ();
extern "C" void CustomAttributeData_get_NamedArguments_m8315 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8316 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8317 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8318 ();
extern "C" void CustomAttributeData_GetCustomAttributes_m8319 ();
extern "C" void CustomAttributeData_ToString_m8320 ();
extern "C" void CustomAttributeData_Equals_m8321 ();
extern "C" void CustomAttributeData_GetHashCode_m8322 ();
extern "C" void CustomAttributeNamedArgument_ToString_m8323 ();
extern "C" void CustomAttributeNamedArgument_Equals_m8324 ();
extern "C" void CustomAttributeNamedArgument_GetHashCode_m8325 ();
extern "C" void CustomAttributeTypedArgument_ToString_m8326 ();
extern "C" void CustomAttributeTypedArgument_Equals_m8327 ();
extern "C" void CustomAttributeTypedArgument_GetHashCode_m8328 ();
extern "C" void AddEventAdapter__ctor_m8329 ();
extern "C" void AddEventAdapter_Invoke_m8330 ();
extern "C" void AddEventAdapter_BeginInvoke_m8331 ();
extern "C" void AddEventAdapter_EndInvoke_m8332 ();
extern "C" void EventInfo__ctor_m8333 ();
extern "C" void EventInfo_get_EventHandlerType_m8334 ();
extern "C" void EventInfo_get_MemberType_m8335 ();
extern "C" void FieldInfo__ctor_m8336 ();
extern "C" void FieldInfo_get_MemberType_m8337 ();
extern "C" void FieldInfo_get_IsLiteral_m8338 ();
extern "C" void FieldInfo_get_IsStatic_m8339 ();
extern "C" void FieldInfo_get_IsNotSerialized_m8340 ();
extern "C" void FieldInfo_SetValue_m8341 ();
extern "C" void FieldInfo_internal_from_handle_type_m8342 ();
extern "C" void FieldInfo_GetFieldFromHandle_m8343 ();
extern "C" void FieldInfo_GetFieldOffset_m8344 ();
extern "C" void FieldInfo_GetUnmanagedMarshal_m8345 ();
extern "C" void FieldInfo_get_UMarshal_m8346 ();
extern "C" void FieldInfo_GetPseudoCustomAttributes_m8347 ();
extern "C" void MemberInfoSerializationHolder__ctor_m8348 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m8349 ();
extern "C" void MemberInfoSerializationHolder_Serialize_m8350 ();
extern "C" void MemberInfoSerializationHolder_GetObjectData_m8351 ();
extern "C" void MemberInfoSerializationHolder_GetRealObject_m8352 ();
extern "C" void MethodBase__ctor_m8353 ();
extern "C" void MethodBase_GetMethodFromHandleNoGenericCheck_m8354 ();
extern "C" void MethodBase_GetMethodFromIntPtr_m8355 ();
extern "C" void MethodBase_GetMethodFromHandle_m8356 ();
extern "C" void MethodBase_GetMethodFromHandleInternalType_m8357 ();
extern "C" void MethodBase_GetParameterCount_m8358 ();
extern "C" void MethodBase_Invoke_m8359 ();
extern "C" void MethodBase_get_CallingConvention_m8360 ();
extern "C" void MethodBase_get_IsPublic_m8361 ();
extern "C" void MethodBase_get_IsStatic_m8362 ();
extern "C" void MethodBase_get_IsVirtual_m8363 ();
extern "C" void MethodBase_get_IsAbstract_m8364 ();
extern "C" void MethodBase_get_next_table_index_m8365 ();
extern "C" void MethodBase_GetGenericArguments_m8366 ();
extern "C" void MethodBase_get_ContainsGenericParameters_m8367 ();
extern "C" void MethodBase_get_IsGenericMethodDefinition_m8368 ();
extern "C" void MethodBase_get_IsGenericMethod_m8369 ();
extern "C" void MethodInfo__ctor_m8370 ();
extern "C" void MethodInfo_get_MemberType_m8371 ();
extern "C" void MethodInfo_get_ReturnType_m8372 ();
extern "C" void MethodInfo_MakeGenericMethod_m8373 ();
extern "C" void MethodInfo_GetGenericArguments_m8374 ();
extern "C" void MethodInfo_get_IsGenericMethod_m8375 ();
extern "C" void MethodInfo_get_IsGenericMethodDefinition_m8376 ();
extern "C" void MethodInfo_get_ContainsGenericParameters_m8377 ();
extern "C" void Missing__ctor_m8378 ();
extern "C" void Missing__cctor_m8379 ();
extern "C" void Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8380 ();
extern "C" void Module__ctor_m8381 ();
extern "C" void Module__cctor_m8382 ();
extern "C" void Module_get_Assembly_m8383 ();
extern "C" void Module_get_ScopeName_m8384 ();
extern "C" void Module_GetCustomAttributes_m8385 ();
extern "C" void Module_GetObjectData_m8386 ();
extern "C" void Module_InternalGetTypes_m8387 ();
extern "C" void Module_GetTypes_m8388 ();
extern "C" void Module_IsDefined_m8389 ();
extern "C" void Module_IsResource_m8390 ();
extern "C" void Module_ToString_m8391 ();
extern "C" void Module_filter_by_type_name_m8392 ();
extern "C" void Module_filter_by_type_name_ignore_case_m8393 ();
extern "C" void MonoEventInfo_get_event_info_m8394 ();
extern "C" void MonoEventInfo_GetEventInfo_m8395 ();
extern "C" void MonoEvent__ctor_m8396 ();
extern "C" void MonoEvent_get_Attributes_m8397 ();
extern "C" void MonoEvent_GetAddMethod_m8398 ();
extern "C" void MonoEvent_get_DeclaringType_m8399 ();
extern "C" void MonoEvent_get_ReflectedType_m8400 ();
extern "C" void MonoEvent_get_Name_m8401 ();
extern "C" void MonoEvent_ToString_m8402 ();
extern "C" void MonoEvent_IsDefined_m8403 ();
extern "C" void MonoEvent_GetCustomAttributes_m8404 ();
extern "C" void MonoEvent_GetCustomAttributes_m8405 ();
extern "C" void MonoEvent_GetObjectData_m8406 ();
extern "C" void MonoField__ctor_m8407 ();
extern "C" void MonoField_get_Attributes_m8408 ();
extern "C" void MonoField_get_FieldHandle_m8409 ();
extern "C" void MonoField_get_FieldType_m8410 ();
extern "C" void MonoField_GetParentType_m8411 ();
extern "C" void MonoField_get_ReflectedType_m8412 ();
extern "C" void MonoField_get_DeclaringType_m8413 ();
extern "C" void MonoField_get_Name_m8414 ();
extern "C" void MonoField_IsDefined_m8415 ();
extern "C" void MonoField_GetCustomAttributes_m8416 ();
extern "C" void MonoField_GetCustomAttributes_m8417 ();
extern "C" void MonoField_GetFieldOffset_m8418 ();
extern "C" void MonoField_GetValueInternal_m8419 ();
extern "C" void MonoField_GetValue_m8420 ();
extern "C" void MonoField_ToString_m8421 ();
extern "C" void MonoField_SetValueInternal_m8422 ();
extern "C" void MonoField_SetValue_m8423 ();
extern "C" void MonoField_GetObjectData_m8424 ();
extern "C" void MonoField_CheckGeneric_m8425 ();
extern "C" void MonoGenericMethod__ctor_m8426 ();
extern "C" void MonoGenericMethod_get_ReflectedType_m8427 ();
extern "C" void MonoGenericCMethod__ctor_m8428 ();
extern "C" void MonoGenericCMethod_get_ReflectedType_m8429 ();
extern "C" void MonoMethodInfo_get_method_info_m8430 ();
extern "C" void MonoMethodInfo_GetMethodInfo_m8431 ();
extern "C" void MonoMethodInfo_GetDeclaringType_m8432 ();
extern "C" void MonoMethodInfo_GetReturnType_m8433 ();
extern "C" void MonoMethodInfo_GetAttributes_m8434 ();
extern "C" void MonoMethodInfo_GetCallingConvention_m8435 ();
extern "C" void MonoMethodInfo_get_parameter_info_m8436 ();
extern "C" void MonoMethodInfo_GetParametersInfo_m8437 ();
extern "C" void MonoMethod__ctor_m8438 ();
extern "C" void MonoMethod_get_name_m8439 ();
extern "C" void MonoMethod_get_base_definition_m8440 ();
extern "C" void MonoMethod_GetBaseDefinition_m8441 ();
extern "C" void MonoMethod_get_ReturnType_m8442 ();
extern "C" void MonoMethod_GetParameters_m8443 ();
extern "C" void MonoMethod_InternalInvoke_m8444 ();
extern "C" void MonoMethod_Invoke_m8445 ();
extern "C" void MonoMethod_get_MethodHandle_m8446 ();
extern "C" void MonoMethod_get_Attributes_m8447 ();
extern "C" void MonoMethod_get_CallingConvention_m8448 ();
extern "C" void MonoMethod_get_ReflectedType_m8449 ();
extern "C" void MonoMethod_get_DeclaringType_m8450 ();
extern "C" void MonoMethod_get_Name_m8451 ();
extern "C" void MonoMethod_IsDefined_m8452 ();
extern "C" void MonoMethod_GetCustomAttributes_m8453 ();
extern "C" void MonoMethod_GetCustomAttributes_m8454 ();
extern "C" void MonoMethod_GetDllImportAttribute_m8455 ();
extern "C" void MonoMethod_GetPseudoCustomAttributes_m8456 ();
extern "C" void MonoMethod_ShouldPrintFullName_m8457 ();
extern "C" void MonoMethod_ToString_m8458 ();
extern "C" void MonoMethod_GetObjectData_m8459 ();
extern "C" void MonoMethod_MakeGenericMethod_m8460 ();
extern "C" void MonoMethod_MakeGenericMethod_impl_m8461 ();
extern "C" void MonoMethod_GetGenericArguments_m8462 ();
extern "C" void MonoMethod_get_IsGenericMethodDefinition_m8463 ();
extern "C" void MonoMethod_get_IsGenericMethod_m8464 ();
extern "C" void MonoMethod_get_ContainsGenericParameters_m8465 ();
extern "C" void MonoCMethod__ctor_m8466 ();
extern "C" void MonoCMethod_GetParameters_m8467 ();
extern "C" void MonoCMethod_InternalInvoke_m8468 ();
extern "C" void MonoCMethod_Invoke_m8469 ();
extern "C" void MonoCMethod_Invoke_m8470 ();
extern "C" void MonoCMethod_get_MethodHandle_m8471 ();
extern "C" void MonoCMethod_get_Attributes_m8472 ();
extern "C" void MonoCMethod_get_CallingConvention_m8473 ();
extern "C" void MonoCMethod_get_ReflectedType_m8474 ();
extern "C" void MonoCMethod_get_DeclaringType_m8475 ();
extern "C" void MonoCMethod_get_Name_m8476 ();
extern "C" void MonoCMethod_IsDefined_m8477 ();
extern "C" void MonoCMethod_GetCustomAttributes_m8478 ();
extern "C" void MonoCMethod_GetCustomAttributes_m8479 ();
extern "C" void MonoCMethod_ToString_m8480 ();
extern "C" void MonoCMethod_GetObjectData_m8481 ();
extern "C" void MonoPropertyInfo_get_property_info_m8482 ();
extern "C" void MonoPropertyInfo_GetTypeModifiers_m8483 ();
extern "C" void GetterAdapter__ctor_m8484 ();
extern "C" void GetterAdapter_Invoke_m8485 ();
extern "C" void GetterAdapter_BeginInvoke_m8486 ();
extern "C" void GetterAdapter_EndInvoke_m8487 ();
extern "C" void MonoProperty__ctor_m8488 ();
extern "C" void MonoProperty_CachePropertyInfo_m8489 ();
extern "C" void MonoProperty_get_Attributes_m8490 ();
extern "C" void MonoProperty_get_CanRead_m8491 ();
extern "C" void MonoProperty_get_CanWrite_m8492 ();
extern "C" void MonoProperty_get_PropertyType_m8493 ();
extern "C" void MonoProperty_get_ReflectedType_m8494 ();
extern "C" void MonoProperty_get_DeclaringType_m8495 ();
extern "C" void MonoProperty_get_Name_m8496 ();
extern "C" void MonoProperty_GetAccessors_m8497 ();
extern "C" void MonoProperty_GetGetMethod_m8498 ();
extern "C" void MonoProperty_GetIndexParameters_m8499 ();
extern "C" void MonoProperty_GetSetMethod_m8500 ();
extern "C" void MonoProperty_IsDefined_m8501 ();
extern "C" void MonoProperty_GetCustomAttributes_m8502 ();
extern "C" void MonoProperty_GetCustomAttributes_m8503 ();
extern "C" void MonoProperty_CreateGetterDelegate_m8504 ();
extern "C" void MonoProperty_GetValue_m8505 ();
extern "C" void MonoProperty_GetValue_m8506 ();
extern "C" void MonoProperty_SetValue_m8507 ();
extern "C" void MonoProperty_ToString_m8508 ();
extern "C" void MonoProperty_GetOptionalCustomModifiers_m8509 ();
extern "C" void MonoProperty_GetRequiredCustomModifiers_m8510 ();
extern "C" void MonoProperty_GetObjectData_m8511 ();
extern "C" void ParameterInfo__ctor_m8512 ();
extern "C" void ParameterInfo__ctor_m8513 ();
extern "C" void ParameterInfo__ctor_m8514 ();
extern "C" void ParameterInfo_ToString_m8515 ();
extern "C" void ParameterInfo_get_ParameterType_m8516 ();
extern "C" void ParameterInfo_get_Attributes_m8517 ();
extern "C" void ParameterInfo_get_IsIn_m8518 ();
extern "C" void ParameterInfo_get_IsOptional_m8519 ();
extern "C" void ParameterInfo_get_IsOut_m8520 ();
extern "C" void ParameterInfo_get_IsRetval_m8521 ();
extern "C" void ParameterInfo_get_Member_m8522 ();
extern "C" void ParameterInfo_get_Name_m8523 ();
extern "C" void ParameterInfo_get_Position_m8524 ();
extern "C" void ParameterInfo_GetCustomAttributes_m8525 ();
extern "C" void ParameterInfo_IsDefined_m8526 ();
extern "C" void ParameterInfo_GetPseudoCustomAttributes_m8527 ();
extern "C" void Pointer__ctor_m8528 ();
extern "C" void Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8529 ();
extern "C" void PropertyInfo__ctor_m8530 ();
extern "C" void PropertyInfo_get_MemberType_m8531 ();
extern "C" void PropertyInfo_GetValue_m8532 ();
extern "C" void PropertyInfo_SetValue_m8533 ();
extern "C" void PropertyInfo_GetOptionalCustomModifiers_m8534 ();
extern "C" void PropertyInfo_GetRequiredCustomModifiers_m8535 ();
extern "C" void StrongNameKeyPair__ctor_m8536 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8537 ();
extern "C" void StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8538 ();
extern "C" void TargetException__ctor_m8539 ();
extern "C" void TargetException__ctor_m8540 ();
extern "C" void TargetException__ctor_m8541 ();
extern "C" void TargetInvocationException__ctor_m8542 ();
extern "C" void TargetInvocationException__ctor_m8543 ();
extern "C" void TargetParameterCountException__ctor_m8544 ();
extern "C" void TargetParameterCountException__ctor_m8545 ();
extern "C" void TargetParameterCountException__ctor_m8546 ();
extern "C" void NeutralResourcesLanguageAttribute__ctor_m8547 ();
extern "C" void ResourceManager__ctor_m8548 ();
extern "C" void ResourceManager__cctor_m8549 ();
extern "C" void ResourceInfo__ctor_m8550 ();
extern "C" void ResourceCacheItem__ctor_m8551 ();
extern "C" void ResourceEnumerator__ctor_m8552 ();
extern "C" void ResourceEnumerator_get_Entry_m8553 ();
extern "C" void ResourceEnumerator_get_Key_m8554 ();
extern "C" void ResourceEnumerator_get_Value_m8555 ();
extern "C" void ResourceEnumerator_get_Current_m8556 ();
extern "C" void ResourceEnumerator_MoveNext_m8557 ();
extern "C" void ResourceEnumerator_Reset_m8558 ();
extern "C" void ResourceEnumerator_FillCache_m8559 ();
extern "C" void ResourceReader__ctor_m8560 ();
extern "C" void ResourceReader__ctor_m8561 ();
extern "C" void ResourceReader_System_Collections_IEnumerable_GetEnumerator_m8562 ();
extern "C" void ResourceReader_System_IDisposable_Dispose_m8563 ();
extern "C" void ResourceReader_ReadHeaders_m8564 ();
extern "C" void ResourceReader_CreateResourceInfo_m8565 ();
extern "C" void ResourceReader_Read7BitEncodedInt_m8566 ();
extern "C" void ResourceReader_ReadValueVer2_m8567 ();
extern "C" void ResourceReader_ReadValueVer1_m8568 ();
extern "C" void ResourceReader_ReadNonPredefinedValue_m8569 ();
extern "C" void ResourceReader_LoadResourceValues_m8570 ();
extern "C" void ResourceReader_Close_m8571 ();
extern "C" void ResourceReader_GetEnumerator_m8572 ();
extern "C" void ResourceReader_Dispose_m8573 ();
extern "C" void ResourceSet__ctor_m8574 ();
extern "C" void ResourceSet__ctor_m8575 ();
extern "C" void ResourceSet__ctor_m8576 ();
extern "C" void ResourceSet__ctor_m8577 ();
extern "C" void ResourceSet_System_Collections_IEnumerable_GetEnumerator_m8578 ();
extern "C" void ResourceSet_Dispose_m8579 ();
extern "C" void ResourceSet_Dispose_m8580 ();
extern "C" void ResourceSet_GetEnumerator_m8581 ();
extern "C" void ResourceSet_GetObjectInternal_m8582 ();
extern "C" void ResourceSet_GetObject_m8583 ();
extern "C" void ResourceSet_GetObject_m8584 ();
extern "C" void ResourceSet_ReadResources_m8585 ();
extern "C" void RuntimeResourceSet__ctor_m8586 ();
extern "C" void RuntimeResourceSet__ctor_m8587 ();
extern "C" void RuntimeResourceSet__ctor_m8588 ();
extern "C" void RuntimeResourceSet_GetObject_m8589 ();
extern "C" void RuntimeResourceSet_GetObject_m8590 ();
extern "C" void RuntimeResourceSet_CloneDisposableObjectIfPossible_m8591 ();
extern "C" void SatelliteContractVersionAttribute__ctor_m8592 ();
extern "C" void CompilationRelaxationsAttribute__ctor_m8593 ();
extern "C" void DefaultDependencyAttribute__ctor_m8594 ();
extern "C" void StringFreezingAttribute__ctor_m8595 ();
extern "C" void CriticalFinalizerObject__ctor_m8596 ();
extern "C" void CriticalFinalizerObject_Finalize_m8597 ();
extern "C" void ReliabilityContractAttribute__ctor_m8598 ();
extern "C" void ClassInterfaceAttribute__ctor_m8599 ();
extern "C" void ComDefaultInterfaceAttribute__ctor_m8600 ();
extern "C" void DispIdAttribute__ctor_m8601 ();
extern "C" void GCHandle__ctor_m8602 ();
extern "C" void GCHandle_get_IsAllocated_m8603 ();
extern "C" void GCHandle_get_Target_m8604 ();
extern "C" void GCHandle_Alloc_m8605 ();
extern "C" void GCHandle_Free_m8606 ();
extern "C" void GCHandle_GetTarget_m8607 ();
extern "C" void GCHandle_GetTargetHandle_m8608 ();
extern "C" void GCHandle_FreeHandle_m8609 ();
extern "C" void GCHandle_Equals_m8610 ();
extern "C" void GCHandle_GetHashCode_m8611 ();
extern "C" void InterfaceTypeAttribute__ctor_m8612 ();
extern "C" void Marshal__cctor_m8613 ();
extern "C" void Marshal_copy_from_unmanaged_m8614 ();
extern "C" void Marshal_Copy_m8615 ();
extern "C" void Marshal_Copy_m8616 ();
extern "C" void Marshal_ReadByte_m8617 ();
extern "C" void Marshal_WriteByte_m8618 ();
extern "C" void MarshalDirectiveException__ctor_m8619 ();
extern "C" void MarshalDirectiveException__ctor_m8620 ();
extern "C" void PreserveSigAttribute__ctor_m8621 ();
extern "C" void SafeHandle__ctor_m8622 ();
extern "C" void SafeHandle_Close_m8623 ();
extern "C" void SafeHandle_DangerousAddRef_m8624 ();
extern "C" void SafeHandle_DangerousGetHandle_m8625 ();
extern "C" void SafeHandle_DangerousRelease_m8626 ();
extern "C" void SafeHandle_Dispose_m8627 ();
extern "C" void SafeHandle_Dispose_m8628 ();
extern "C" void SafeHandle_SetHandle_m8629 ();
extern "C" void SafeHandle_Finalize_m8630 ();
extern "C" void TypeLibImportClassAttribute__ctor_m8631 ();
extern "C" void TypeLibVersionAttribute__ctor_m8632 ();
extern "C" void ActivationServices_get_ConstructionActivator_m8633 ();
extern "C" void ActivationServices_CreateProxyFromAttributes_m8634 ();
extern "C" void ActivationServices_CreateConstructionCall_m8635 ();
extern "C" void ActivationServices_AllocateUninitializedClassInstance_m8636 ();
extern "C" void ActivationServices_EnableProxyActivation_m8637 ();
extern "C" void AppDomainLevelActivator__ctor_m8638 ();
extern "C" void ConstructionLevelActivator__ctor_m8639 ();
extern "C" void ContextLevelActivator__ctor_m8640 ();
extern "C" void UrlAttribute_get_UrlValue_m8641 ();
extern "C" void UrlAttribute_Equals_m8642 ();
extern "C" void UrlAttribute_GetHashCode_m8643 ();
extern "C" void UrlAttribute_GetPropertiesForNewContext_m8644 ();
extern "C" void UrlAttribute_IsContextOK_m8645 ();
extern "C" void ChannelInfo__ctor_m8646 ();
extern "C" void ChannelInfo_get_ChannelData_m8647 ();
extern "C" void ChannelServices__cctor_m8648 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m8649 ();
extern "C" void ChannelServices_CreateClientChannelSinkChain_m8650 ();
extern "C" void ChannelServices_RegisterChannel_m8651 ();
extern "C" void ChannelServices_RegisterChannel_m8652 ();
extern "C" void ChannelServices_RegisterChannelConfig_m8653 ();
extern "C" void ChannelServices_CreateProvider_m8654 ();
extern "C" void ChannelServices_GetCurrentChannelInfo_m8655 ();
extern "C" void CrossAppDomainData__ctor_m8656 ();
extern "C" void CrossAppDomainData_get_DomainID_m8657 ();
extern "C" void CrossAppDomainData_get_ProcessID_m8658 ();
extern "C" void CrossAppDomainChannel__ctor_m8659 ();
extern "C" void CrossAppDomainChannel__cctor_m8660 ();
extern "C" void CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8661 ();
extern "C" void CrossAppDomainChannel_get_ChannelName_m8662 ();
extern "C" void CrossAppDomainChannel_get_ChannelPriority_m8663 ();
extern "C" void CrossAppDomainChannel_get_ChannelData_m8664 ();
extern "C" void CrossAppDomainChannel_StartListening_m8665 ();
extern "C" void CrossAppDomainChannel_CreateMessageSink_m8666 ();
extern "C" void CrossAppDomainSink__ctor_m8667 ();
extern "C" void CrossAppDomainSink__cctor_m8668 ();
extern "C" void CrossAppDomainSink_GetSink_m8669 ();
extern "C" void CrossAppDomainSink_get_TargetDomainId_m8670 ();
extern "C" void SinkProviderData__ctor_m8671 ();
extern "C" void SinkProviderData_get_Children_m8672 ();
extern "C" void SinkProviderData_get_Properties_m8673 ();
extern "C" void Context__ctor_m8674 ();
extern "C" void Context__cctor_m8675 ();
extern "C" void Context_Finalize_m8676 ();
extern "C" void Context_get_DefaultContext_m8677 ();
extern "C" void Context_get_ContextID_m8678 ();
extern "C" void Context_get_ContextProperties_m8679 ();
extern "C" void Context_get_IsDefaultContext_m8680 ();
extern "C" void Context_get_NeedsContextSink_m8681 ();
extern "C" void Context_RegisterDynamicProperty_m8682 ();
extern "C" void Context_UnregisterDynamicProperty_m8683 ();
extern "C" void Context_GetDynamicPropertyCollection_m8684 ();
extern "C" void Context_NotifyGlobalDynamicSinks_m8685 ();
extern "C" void Context_get_HasGlobalDynamicSinks_m8686 ();
extern "C" void Context_NotifyDynamicSinks_m8687 ();
extern "C" void Context_get_HasDynamicSinks_m8688 ();
extern "C" void Context_get_HasExitSinks_m8689 ();
extern "C" void Context_GetProperty_m8690 ();
extern "C" void Context_SetProperty_m8691 ();
extern "C" void Context_Freeze_m8692 ();
extern "C" void Context_ToString_m8693 ();
extern "C" void Context_GetServerContextSinkChain_m8694 ();
extern "C" void Context_GetClientContextSinkChain_m8695 ();
extern "C" void Context_CreateServerObjectSinkChain_m8696 ();
extern "C" void Context_CreateEnvoySink_m8697 ();
extern "C" void Context_SwitchToContext_m8698 ();
extern "C" void Context_CreateNewContext_m8699 ();
extern "C" void Context_DoCallBack_m8700 ();
extern "C" void Context_AllocateDataSlot_m8701 ();
extern "C" void Context_AllocateNamedDataSlot_m8702 ();
extern "C" void Context_FreeNamedDataSlot_m8703 ();
extern "C" void Context_GetData_m8704 ();
extern "C" void Context_GetNamedDataSlot_m8705 ();
extern "C" void Context_SetData_m8706 ();
extern "C" void DynamicPropertyReg__ctor_m8707 ();
extern "C" void DynamicPropertyCollection__ctor_m8708 ();
extern "C" void DynamicPropertyCollection_get_HasProperties_m8709 ();
extern "C" void DynamicPropertyCollection_RegisterDynamicProperty_m8710 ();
extern "C" void DynamicPropertyCollection_UnregisterDynamicProperty_m8711 ();
extern "C" void DynamicPropertyCollection_NotifyMessage_m8712 ();
extern "C" void DynamicPropertyCollection_FindProperty_m8713 ();
extern "C" void ContextCallbackObject__ctor_m8714 ();
extern "C" void ContextCallbackObject_DoCallBack_m8715 ();
extern "C" void ContextAttribute__ctor_m8716 ();
extern "C" void ContextAttribute_get_Name_m8717 ();
extern "C" void ContextAttribute_Equals_m8718 ();
extern "C" void ContextAttribute_Freeze_m8719 ();
extern "C" void ContextAttribute_GetHashCode_m8720 ();
extern "C" void ContextAttribute_GetPropertiesForNewContext_m8721 ();
extern "C" void ContextAttribute_IsContextOK_m8722 ();
extern "C" void ContextAttribute_IsNewContextOK_m8723 ();
extern "C" void CrossContextChannel__ctor_m8724 ();
extern "C" void SynchronizationAttribute__ctor_m8725 ();
extern "C" void SynchronizationAttribute__ctor_m8726 ();
extern "C" void SynchronizationAttribute_set_Locked_m8727 ();
extern "C" void SynchronizationAttribute_ReleaseLock_m8728 ();
extern "C" void SynchronizationAttribute_GetPropertiesForNewContext_m8729 ();
extern "C" void SynchronizationAttribute_GetClientContextSink_m8730 ();
extern "C" void SynchronizationAttribute_GetServerContextSink_m8731 ();
extern "C" void SynchronizationAttribute_IsContextOK_m8732 ();
extern "C" void SynchronizationAttribute_ExitContext_m8733 ();
extern "C" void SynchronizationAttribute_EnterContext_m8734 ();
extern "C" void SynchronizedClientContextSink__ctor_m8735 ();
extern "C" void SynchronizedServerContextSink__ctor_m8736 ();
extern "C" void LeaseManager__ctor_m8737 ();
extern "C" void LeaseManager_SetPollTime_m8738 ();
extern "C" void LeaseSink__ctor_m8739 ();
extern "C" void LifetimeServices__cctor_m8740 ();
extern "C" void LifetimeServices_set_LeaseManagerPollTime_m8741 ();
extern "C" void LifetimeServices_set_LeaseTime_m8742 ();
extern "C" void LifetimeServices_set_RenewOnCallTime_m8743 ();
extern "C" void LifetimeServices_set_SponsorshipTimeout_m8744 ();
extern "C" void ArgInfo__ctor_m8745 ();
extern "C" void ArgInfo_GetInOutArgs_m8746 ();
extern "C" void AsyncResult__ctor_m8747 ();
extern "C" void AsyncResult_get_AsyncState_m8748 ();
extern "C" void AsyncResult_get_AsyncWaitHandle_m8749 ();
extern "C" void AsyncResult_get_CompletedSynchronously_m8750 ();
extern "C" void AsyncResult_get_IsCompleted_m8751 ();
extern "C" void AsyncResult_get_EndInvokeCalled_m8752 ();
extern "C" void AsyncResult_set_EndInvokeCalled_m8753 ();
extern "C" void AsyncResult_get_AsyncDelegate_m8754 ();
extern "C" void AsyncResult_get_NextSink_m8755 ();
extern "C" void AsyncResult_AsyncProcessMessage_m8756 ();
extern "C" void AsyncResult_GetReplyMessage_m8757 ();
extern "C" void AsyncResult_SetMessageCtrl_m8758 ();
extern "C" void AsyncResult_SetCompletedSynchronously_m8759 ();
extern "C" void AsyncResult_EndInvoke_m8760 ();
extern "C" void AsyncResult_SyncProcessMessage_m8761 ();
extern "C" void AsyncResult_get_CallMessage_m8762 ();
extern "C" void AsyncResult_set_CallMessage_m8763 ();
extern "C" void ClientContextTerminatorSink__ctor_m8764 ();
extern "C" void ConstructionCall__ctor_m8765 ();
extern "C" void ConstructionCall__ctor_m8766 ();
extern "C" void ConstructionCall_InitDictionary_m8767 ();
extern "C" void ConstructionCall_set_IsContextOk_m8768 ();
extern "C" void ConstructionCall_get_ActivationType_m8769 ();
extern "C" void ConstructionCall_get_ActivationTypeName_m8770 ();
extern "C" void ConstructionCall_get_Activator_m8771 ();
extern "C" void ConstructionCall_set_Activator_m8772 ();
extern "C" void ConstructionCall_get_CallSiteActivationAttributes_m8773 ();
extern "C" void ConstructionCall_SetActivationAttributes_m8774 ();
extern "C" void ConstructionCall_get_ContextProperties_m8775 ();
extern "C" void ConstructionCall_InitMethodProperty_m8776 ();
extern "C" void ConstructionCall_GetObjectData_m8777 ();
extern "C" void ConstructionCall_get_Properties_m8778 ();
extern "C" void ConstructionCallDictionary__ctor_m8779 ();
extern "C" void ConstructionCallDictionary__cctor_m8780 ();
extern "C" void ConstructionCallDictionary_GetMethodProperty_m8781 ();
extern "C" void ConstructionCallDictionary_SetMethodProperty_m8782 ();
extern "C" void EnvoyTerminatorSink__ctor_m8783 ();
extern "C" void EnvoyTerminatorSink__cctor_m8784 ();
extern "C" void Header__ctor_m8785 ();
extern "C" void Header__ctor_m8786 ();
extern "C" void Header__ctor_m8787 ();
extern "C" void LogicalCallContext__ctor_m8788 ();
extern "C" void LogicalCallContext__ctor_m8789 ();
extern "C" void LogicalCallContext_GetObjectData_m8790 ();
extern "C" void LogicalCallContext_SetData_m8791 ();
extern "C" void LogicalCallContext_Clone_m8792 ();
extern "C" void CallContextRemotingData__ctor_m8793 ();
extern "C" void CallContextRemotingData_Clone_m8794 ();
extern "C" void MethodCall__ctor_m8795 ();
extern "C" void MethodCall__ctor_m8796 ();
extern "C" void MethodCall__ctor_m8797 ();
extern "C" void MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8798 ();
extern "C" void MethodCall_InitMethodProperty_m8799 ();
extern "C" void MethodCall_GetObjectData_m8800 ();
extern "C" void MethodCall_get_Args_m8801 ();
extern "C" void MethodCall_get_LogicalCallContext_m8802 ();
extern "C" void MethodCall_get_MethodBase_m8803 ();
extern "C" void MethodCall_get_MethodName_m8804 ();
extern "C" void MethodCall_get_MethodSignature_m8805 ();
extern "C" void MethodCall_get_Properties_m8806 ();
extern "C" void MethodCall_InitDictionary_m8807 ();
extern "C" void MethodCall_get_TypeName_m8808 ();
extern "C" void MethodCall_get_Uri_m8809 ();
extern "C" void MethodCall_set_Uri_m8810 ();
extern "C" void MethodCall_Init_m8811 ();
extern "C" void MethodCall_ResolveMethod_m8812 ();
extern "C" void MethodCall_CastTo_m8813 ();
extern "C" void MethodCall_GetTypeNameFromAssemblyQualifiedName_m8814 ();
extern "C" void MethodCall_get_GenericArguments_m8815 ();
extern "C" void MethodCallDictionary__ctor_m8816 ();
extern "C" void MethodCallDictionary__cctor_m8817 ();
extern "C" void DictionaryEnumerator__ctor_m8818 ();
extern "C" void DictionaryEnumerator_get_Current_m8819 ();
extern "C" void DictionaryEnumerator_MoveNext_m8820 ();
extern "C" void DictionaryEnumerator_Reset_m8821 ();
extern "C" void DictionaryEnumerator_get_Entry_m8822 ();
extern "C" void DictionaryEnumerator_get_Key_m8823 ();
extern "C" void DictionaryEnumerator_get_Value_m8824 ();
extern "C" void MethodDictionary__ctor_m8825 ();
extern "C" void MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8826 ();
extern "C" void MethodDictionary_set_MethodKeys_m8827 ();
extern "C" void MethodDictionary_AllocInternalProperties_m8828 ();
extern "C" void MethodDictionary_GetInternalProperties_m8829 ();
extern "C" void MethodDictionary_IsOverridenKey_m8830 ();
extern "C" void MethodDictionary_get_Item_m8831 ();
extern "C" void MethodDictionary_set_Item_m8832 ();
extern "C" void MethodDictionary_GetMethodProperty_m8833 ();
extern "C" void MethodDictionary_SetMethodProperty_m8834 ();
extern "C" void MethodDictionary_get_Values_m8835 ();
extern "C" void MethodDictionary_Add_m8836 ();
extern "C" void MethodDictionary_Contains_m8837 ();
extern "C" void MethodDictionary_Remove_m8838 ();
extern "C" void MethodDictionary_get_Count_m8839 ();
extern "C" void MethodDictionary_get_IsSynchronized_m8840 ();
extern "C" void MethodDictionary_get_SyncRoot_m8841 ();
extern "C" void MethodDictionary_CopyTo_m8842 ();
extern "C" void MethodDictionary_GetEnumerator_m8843 ();
extern "C" void MethodReturnDictionary__ctor_m8844 ();
extern "C" void MethodReturnDictionary__cctor_m8845 ();
extern "C" void MonoMethodMessage_get_Args_m8846 ();
extern "C" void MonoMethodMessage_get_LogicalCallContext_m8847 ();
extern "C" void MonoMethodMessage_get_MethodBase_m8848 ();
extern "C" void MonoMethodMessage_get_MethodName_m8849 ();
extern "C" void MonoMethodMessage_get_MethodSignature_m8850 ();
extern "C" void MonoMethodMessage_get_TypeName_m8851 ();
extern "C" void MonoMethodMessage_get_Uri_m8852 ();
extern "C" void MonoMethodMessage_set_Uri_m8853 ();
extern "C" void MonoMethodMessage_get_Exception_m8854 ();
extern "C" void MonoMethodMessage_get_OutArgCount_m8855 ();
extern "C" void MonoMethodMessage_get_OutArgs_m8856 ();
extern "C" void MonoMethodMessage_get_ReturnValue_m8857 ();
extern "C" void RemotingSurrogate__ctor_m8858 ();
extern "C" void RemotingSurrogate_SetObjectData_m8859 ();
extern "C" void ObjRefSurrogate__ctor_m8860 ();
extern "C" void ObjRefSurrogate_SetObjectData_m8861 ();
extern "C" void RemotingSurrogateSelector__ctor_m8862 ();
extern "C" void RemotingSurrogateSelector__cctor_m8863 ();
extern "C" void RemotingSurrogateSelector_GetSurrogate_m8864 ();
extern "C" void ReturnMessage__ctor_m8865 ();
extern "C" void ReturnMessage__ctor_m8866 ();
extern "C" void ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8867 ();
extern "C" void ReturnMessage_get_Args_m8868 ();
extern "C" void ReturnMessage_get_LogicalCallContext_m8869 ();
extern "C" void ReturnMessage_get_MethodBase_m8870 ();
extern "C" void ReturnMessage_get_MethodName_m8871 ();
extern "C" void ReturnMessage_get_MethodSignature_m8872 ();
extern "C" void ReturnMessage_get_Properties_m8873 ();
extern "C" void ReturnMessage_get_TypeName_m8874 ();
extern "C" void ReturnMessage_get_Uri_m8875 ();
extern "C" void ReturnMessage_set_Uri_m8876 ();
extern "C" void ReturnMessage_get_Exception_m8877 ();
extern "C" void ReturnMessage_get_OutArgs_m8878 ();
extern "C" void ReturnMessage_get_ReturnValue_m8879 ();
extern "C" void ServerContextTerminatorSink__ctor_m8880 ();
extern "C" void ServerObjectTerminatorSink__ctor_m8881 ();
extern "C" void StackBuilderSink__ctor_m8882 ();
extern "C" void SoapAttribute__ctor_m8883 ();
extern "C" void SoapAttribute_get_UseAttribute_m8884 ();
extern "C" void SoapAttribute_get_XmlNamespace_m8885 ();
extern "C" void SoapAttribute_SetReflectionObject_m8886 ();
extern "C" void SoapFieldAttribute__ctor_m8887 ();
extern "C" void SoapFieldAttribute_get_XmlElementName_m8888 ();
extern "C" void SoapFieldAttribute_IsInteropXmlElement_m8889 ();
extern "C" void SoapFieldAttribute_SetReflectionObject_m8890 ();
extern "C" void SoapMethodAttribute__ctor_m8891 ();
extern "C" void SoapMethodAttribute_get_UseAttribute_m8892 ();
extern "C" void SoapMethodAttribute_get_XmlNamespace_m8893 ();
extern "C" void SoapMethodAttribute_SetReflectionObject_m8894 ();
extern "C" void SoapParameterAttribute__ctor_m8895 ();
extern "C" void SoapTypeAttribute__ctor_m8896 ();
extern "C" void SoapTypeAttribute_get_UseAttribute_m8897 ();
extern "C" void SoapTypeAttribute_get_XmlElementName_m8898 ();
extern "C" void SoapTypeAttribute_get_XmlNamespace_m8899 ();
extern "C" void SoapTypeAttribute_get_XmlTypeName_m8900 ();
extern "C" void SoapTypeAttribute_get_XmlTypeNamespace_m8901 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlElement_m8902 ();
extern "C" void SoapTypeAttribute_get_IsInteropXmlType_m8903 ();
extern "C" void SoapTypeAttribute_SetReflectionObject_m8904 ();
extern "C" void ProxyAttribute_CreateInstance_m8905 ();
extern "C" void ProxyAttribute_CreateProxy_m8906 ();
extern "C" void ProxyAttribute_GetPropertiesForNewContext_m8907 ();
extern "C" void ProxyAttribute_IsContextOK_m8908 ();
extern "C" void RealProxy__ctor_m8909 ();
extern "C" void RealProxy__ctor_m8910 ();
extern "C" void RealProxy__ctor_m8911 ();
extern "C" void RealProxy_InternalGetProxyType_m8912 ();
extern "C" void RealProxy_GetProxiedType_m8913 ();
extern "C" void RealProxy_get_ObjectIdentity_m8914 ();
extern "C" void RealProxy_InternalGetTransparentProxy_m8915 ();
extern "C" void RealProxy_GetTransparentProxy_m8916 ();
extern "C" void RealProxy_SetTargetDomain_m8917 ();
extern "C" void RemotingProxy__ctor_m8918 ();
extern "C" void RemotingProxy__ctor_m8919 ();
extern "C" void RemotingProxy__cctor_m8920 ();
extern "C" void RemotingProxy_get_TypeName_m8921 ();
extern "C" void RemotingProxy_Finalize_m8922 ();
extern "C" void TrackingServices__cctor_m8923 ();
extern "C" void TrackingServices_NotifyUnmarshaledObject_m8924 ();
extern "C" void ActivatedClientTypeEntry__ctor_m8925 ();
extern "C" void ActivatedClientTypeEntry_get_ApplicationUrl_m8926 ();
extern "C" void ActivatedClientTypeEntry_get_ContextAttributes_m8927 ();
extern "C" void ActivatedClientTypeEntry_get_ObjectType_m8928 ();
extern "C" void ActivatedClientTypeEntry_ToString_m8929 ();
extern "C" void ActivatedServiceTypeEntry__ctor_m8930 ();
extern "C" void ActivatedServiceTypeEntry_get_ObjectType_m8931 ();
extern "C" void ActivatedServiceTypeEntry_ToString_m8932 ();
extern "C" void EnvoyInfo__ctor_m8933 ();
extern "C" void EnvoyInfo_get_EnvoySinks_m8934 ();
extern "C" void Identity__ctor_m8935 ();
extern "C" void Identity_get_ChannelSink_m8936 ();
extern "C" void Identity_set_ChannelSink_m8937 ();
extern "C" void Identity_get_ObjectUri_m8938 ();
extern "C" void Identity_get_Disposed_m8939 ();
extern "C" void Identity_set_Disposed_m8940 ();
extern "C" void Identity_get_ClientDynamicProperties_m8941 ();
extern "C" void Identity_get_ServerDynamicProperties_m8942 ();
extern "C" void ClientIdentity__ctor_m8943 ();
extern "C" void ClientIdentity_get_ClientProxy_m8944 ();
extern "C" void ClientIdentity_set_ClientProxy_m8945 ();
extern "C" void ClientIdentity_CreateObjRef_m8946 ();
extern "C" void ClientIdentity_get_TargetUri_m8947 ();
extern "C" void InternalRemotingServices__cctor_m8948 ();
extern "C" void InternalRemotingServices_GetCachedSoapAttribute_m8949 ();
extern "C" void ObjRef__ctor_m8950 ();
extern "C" void ObjRef__ctor_m8951 ();
extern "C" void ObjRef__cctor_m8952 ();
extern "C" void ObjRef_get_IsReferenceToWellKnow_m8953 ();
extern "C" void ObjRef_get_ChannelInfo_m8954 ();
extern "C" void ObjRef_get_EnvoyInfo_m8955 ();
extern "C" void ObjRef_set_EnvoyInfo_m8956 ();
extern "C" void ObjRef_get_TypeInfo_m8957 ();
extern "C" void ObjRef_set_TypeInfo_m8958 ();
extern "C" void ObjRef_get_URI_m8959 ();
extern "C" void ObjRef_set_URI_m8960 ();
extern "C" void ObjRef_GetObjectData_m8961 ();
extern "C" void ObjRef_GetRealObject_m8962 ();
extern "C" void ObjRef_UpdateChannelInfo_m8963 ();
extern "C" void ObjRef_get_ServerType_m8964 ();
extern "C" void RemotingConfiguration__cctor_m8965 ();
extern "C" void RemotingConfiguration_get_ApplicationName_m8966 ();
extern "C" void RemotingConfiguration_set_ApplicationName_m8967 ();
extern "C" void RemotingConfiguration_get_ProcessId_m8968 ();
extern "C" void RemotingConfiguration_LoadDefaultDelayedChannels_m8969 ();
extern "C" void RemotingConfiguration_IsRemotelyActivatedClientType_m8970 ();
extern "C" void RemotingConfiguration_RegisterActivatedClientType_m8971 ();
extern "C" void RemotingConfiguration_RegisterActivatedServiceType_m8972 ();
extern "C" void RemotingConfiguration_RegisterWellKnownClientType_m8973 ();
extern "C" void RemotingConfiguration_RegisterWellKnownServiceType_m8974 ();
extern "C" void RemotingConfiguration_RegisterChannelTemplate_m8975 ();
extern "C" void RemotingConfiguration_RegisterClientProviderTemplate_m8976 ();
extern "C" void RemotingConfiguration_RegisterServerProviderTemplate_m8977 ();
extern "C" void RemotingConfiguration_RegisterChannels_m8978 ();
extern "C" void RemotingConfiguration_RegisterTypes_m8979 ();
extern "C" void RemotingConfiguration_SetCustomErrorsMode_m8980 ();
extern "C" void ConfigHandler__ctor_m8981 ();
extern "C" void ConfigHandler_ValidatePath_m8982 ();
extern "C" void ConfigHandler_CheckPath_m8983 ();
extern "C" void ConfigHandler_OnStartParsing_m8984 ();
extern "C" void ConfigHandler_OnProcessingInstruction_m8985 ();
extern "C" void ConfigHandler_OnIgnorableWhitespace_m8986 ();
extern "C" void ConfigHandler_OnStartElement_m8987 ();
extern "C" void ConfigHandler_ParseElement_m8988 ();
extern "C" void ConfigHandler_OnEndElement_m8989 ();
extern "C" void ConfigHandler_ReadCustomProviderData_m8990 ();
extern "C" void ConfigHandler_ReadLifetine_m8991 ();
extern "C" void ConfigHandler_ParseTime_m8992 ();
extern "C" void ConfigHandler_ReadChannel_m8993 ();
extern "C" void ConfigHandler_ReadProvider_m8994 ();
extern "C" void ConfigHandler_ReadClientActivated_m8995 ();
extern "C" void ConfigHandler_ReadServiceActivated_m8996 ();
extern "C" void ConfigHandler_ReadClientWellKnown_m8997 ();
extern "C" void ConfigHandler_ReadServiceWellKnown_m8998 ();
extern "C" void ConfigHandler_ReadInteropXml_m8999 ();
extern "C" void ConfigHandler_ReadPreload_m9000 ();
extern "C" void ConfigHandler_GetNotNull_m9001 ();
extern "C" void ConfigHandler_ExtractAssembly_m9002 ();
extern "C" void ConfigHandler_OnChars_m9003 ();
extern "C" void ConfigHandler_OnEndParsing_m9004 ();
extern "C" void ChannelData__ctor_m9005 ();
extern "C" void ChannelData_get_ServerProviders_m9006 ();
extern "C" void ChannelData_get_ClientProviders_m9007 ();
extern "C" void ChannelData_get_CustomProperties_m9008 ();
extern "C" void ChannelData_CopyFrom_m9009 ();
extern "C" void ProviderData__ctor_m9010 ();
extern "C" void ProviderData_CopyFrom_m9011 ();
extern "C" void FormatterData__ctor_m9012 ();
extern "C" void RemotingException__ctor_m9013 ();
extern "C" void RemotingException__ctor_m9014 ();
extern "C" void RemotingException__ctor_m9015 ();
extern "C" void RemotingException__ctor_m9016 ();
extern "C" void RemotingServices__cctor_m9017 ();
extern "C" void RemotingServices_GetVirtualMethod_m9018 ();
extern "C" void RemotingServices_IsTransparentProxy_m9019 ();
extern "C" void RemotingServices_GetServerTypeForUri_m9020 ();
extern "C" void RemotingServices_Unmarshal_m9021 ();
extern "C" void RemotingServices_Unmarshal_m9022 ();
extern "C" void RemotingServices_GetRealProxy_m9023 ();
extern "C" void RemotingServices_GetMethodBaseFromMethodMessage_m9024 ();
extern "C" void RemotingServices_GetMethodBaseFromName_m9025 ();
extern "C" void RemotingServices_FindInterfaceMethod_m9026 ();
extern "C" void RemotingServices_CreateClientProxy_m9027 ();
extern "C" void RemotingServices_CreateClientProxy_m9028 ();
extern "C" void RemotingServices_CreateClientProxyForContextBound_m9029 ();
extern "C" void RemotingServices_GetIdentityForUri_m9030 ();
extern "C" void RemotingServices_RemoveAppNameFromUri_m9031 ();
extern "C" void RemotingServices_GetOrCreateClientIdentity_m9032 ();
extern "C" void RemotingServices_GetClientChannelSinkChain_m9033 ();
extern "C" void RemotingServices_CreateWellKnownServerIdentity_m9034 ();
extern "C" void RemotingServices_RegisterServerIdentity_m9035 ();
extern "C" void RemotingServices_GetProxyForRemoteObject_m9036 ();
extern "C" void RemotingServices_GetRemoteObject_m9037 ();
extern "C" void RemotingServices_RegisterInternalChannels_m9038 ();
extern "C" void RemotingServices_DisposeIdentity_m9039 ();
extern "C" void RemotingServices_GetNormalizedUri_m9040 ();
extern "C" void ServerIdentity__ctor_m9041 ();
extern "C" void ServerIdentity_get_ObjectType_m9042 ();
extern "C" void ServerIdentity_CreateObjRef_m9043 ();
extern "C" void ClientActivatedIdentity_GetServerObject_m9044 ();
extern "C" void SingletonIdentity__ctor_m9045 ();
extern "C" void SingleCallIdentity__ctor_m9046 ();
extern "C" void TypeInfo__ctor_m9047 ();
extern "C" void SoapServices__cctor_m9048 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithAssembly_m9049 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNs_m9050 ();
extern "C" void SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m9051 ();
extern "C" void SoapServices_CodeXmlNamespaceForClrTypeNamespace_m9052 ();
extern "C" void SoapServices_GetNameKey_m9053 ();
extern "C" void SoapServices_GetAssemblyName_m9054 ();
extern "C" void SoapServices_GetXmlElementForInteropType_m9055 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodCall_m9056 ();
extern "C" void SoapServices_GetXmlNamespaceForMethodResponse_m9057 ();
extern "C" void SoapServices_GetXmlTypeForInteropType_m9058 ();
extern "C" void SoapServices_PreLoad_m9059 ();
extern "C" void SoapServices_PreLoad_m9060 ();
extern "C" void SoapServices_RegisterInteropXmlElement_m9061 ();
extern "C" void SoapServices_RegisterInteropXmlType_m9062 ();
extern "C" void SoapServices_EncodeNs_m9063 ();
extern "C" void TypeEntry__ctor_m9064 ();
extern "C" void TypeEntry_get_AssemblyName_m9065 ();
extern "C" void TypeEntry_set_AssemblyName_m9066 ();
extern "C" void TypeEntry_get_TypeName_m9067 ();
extern "C" void TypeEntry_set_TypeName_m9068 ();
extern "C" void TypeInfo__ctor_m9069 ();
extern "C" void TypeInfo_get_TypeName_m9070 ();
extern "C" void WellKnownClientTypeEntry__ctor_m9071 ();
extern "C" void WellKnownClientTypeEntry_get_ApplicationUrl_m9072 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectType_m9073 ();
extern "C" void WellKnownClientTypeEntry_get_ObjectUrl_m9074 ();
extern "C" void WellKnownClientTypeEntry_ToString_m9075 ();
extern "C" void WellKnownServiceTypeEntry__ctor_m9076 ();
extern "C" void WellKnownServiceTypeEntry_get_Mode_m9077 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectType_m9078 ();
extern "C" void WellKnownServiceTypeEntry_get_ObjectUri_m9079 ();
extern "C" void WellKnownServiceTypeEntry_ToString_m9080 ();
extern "C" void BinaryCommon__cctor_m9081 ();
extern "C" void BinaryCommon_IsPrimitive_m9082 ();
extern "C" void BinaryCommon_GetTypeFromCode_m9083 ();
extern "C" void BinaryCommon_SwapBytes_m9084 ();
extern "C" void BinaryFormatter__ctor_m9085 ();
extern "C" void BinaryFormatter__ctor_m9086 ();
extern "C" void BinaryFormatter_get_DefaultSurrogateSelector_m9087 ();
extern "C" void BinaryFormatter_set_AssemblyFormat_m9088 ();
extern "C" void BinaryFormatter_get_Binder_m9089 ();
extern "C" void BinaryFormatter_get_Context_m9090 ();
extern "C" void BinaryFormatter_get_SurrogateSelector_m9091 ();
extern "C" void BinaryFormatter_get_FilterLevel_m9092 ();
extern "C" void BinaryFormatter_Deserialize_m9093 ();
extern "C" void BinaryFormatter_NoCheckDeserialize_m9094 ();
extern "C" void BinaryFormatter_ReadBinaryHeader_m9095 ();
extern "C" void MessageFormatter_ReadMethodCall_m9096 ();
extern "C" void MessageFormatter_ReadMethodResponse_m9097 ();
extern "C" void TypeMetadata__ctor_m9098 ();
extern "C" void ArrayNullFiller__ctor_m9099 ();
extern "C" void ObjectReader__ctor_m9100 ();
extern "C" void ObjectReader_ReadObjectGraph_m9101 ();
extern "C" void ObjectReader_ReadObjectGraph_m9102 ();
extern "C" void ObjectReader_ReadNextObject_m9103 ();
extern "C" void ObjectReader_ReadNextObject_m9104 ();
extern "C" void ObjectReader_get_CurrentObject_m9105 ();
extern "C" void ObjectReader_ReadObject_m9106 ();
extern "C" void ObjectReader_ReadAssembly_m9107 ();
extern "C" void ObjectReader_ReadObjectInstance_m9108 ();
extern "C" void ObjectReader_ReadRefTypeObjectInstance_m9109 ();
extern "C" void ObjectReader_ReadObjectContent_m9110 ();
extern "C" void ObjectReader_RegisterObject_m9111 ();
extern "C" void ObjectReader_ReadStringIntance_m9112 ();
extern "C" void ObjectReader_ReadGenericArray_m9113 ();
extern "C" void ObjectReader_ReadBoxedPrimitiveTypeValue_m9114 ();
extern "C" void ObjectReader_ReadArrayOfPrimitiveType_m9115 ();
extern "C" void ObjectReader_BlockRead_m9116 ();
extern "C" void ObjectReader_ReadArrayOfObject_m9117 ();
extern "C" void ObjectReader_ReadArrayOfString_m9118 ();
extern "C" void ObjectReader_ReadSimpleArray_m9119 ();
extern "C" void ObjectReader_ReadTypeMetadata_m9120 ();
extern "C" void ObjectReader_ReadValue_m9121 ();
extern "C" void ObjectReader_SetObjectValue_m9122 ();
extern "C" void ObjectReader_RecordFixup_m9123 ();
extern "C" void ObjectReader_GetDeserializationType_m9124 ();
extern "C" void ObjectReader_ReadType_m9125 ();
extern "C" void ObjectReader_ReadPrimitiveTypeValue_m9126 ();
extern "C" void FormatterConverter__ctor_m9127 ();
extern "C" void FormatterConverter_Convert_m9128 ();
extern "C" void FormatterConverter_ToBoolean_m9129 ();
extern "C" void FormatterConverter_ToInt16_m9130 ();
extern "C" void FormatterConverter_ToInt32_m9131 ();
extern "C" void FormatterConverter_ToInt64_m9132 ();
extern "C" void FormatterConverter_ToString_m9133 ();
extern "C" void FormatterServices_GetUninitializedObject_m9134 ();
extern "C" void FormatterServices_GetSafeUninitializedObject_m9135 ();
extern "C" void ObjectManager__ctor_m9136 ();
extern "C" void ObjectManager_DoFixups_m9137 ();
extern "C" void ObjectManager_GetObjectRecord_m9138 ();
extern "C" void ObjectManager_GetObject_m9139 ();
extern "C" void ObjectManager_RaiseDeserializationEvent_m9140 ();
extern "C" void ObjectManager_RaiseOnDeserializingEvent_m9141 ();
extern "C" void ObjectManager_RaiseOnDeserializedEvent_m9142 ();
extern "C" void ObjectManager_AddFixup_m9143 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m9144 ();
extern "C" void ObjectManager_RecordArrayElementFixup_m9145 ();
extern "C" void ObjectManager_RecordDelayedFixup_m9146 ();
extern "C" void ObjectManager_RecordFixup_m9147 ();
extern "C" void ObjectManager_RegisterObjectInternal_m9148 ();
extern "C" void ObjectManager_RegisterObject_m9149 ();
extern "C" void BaseFixupRecord__ctor_m9150 ();
extern "C" void BaseFixupRecord_DoFixup_m9151 ();
extern "C" void ArrayFixupRecord__ctor_m9152 ();
extern "C" void ArrayFixupRecord_FixupImpl_m9153 ();
extern "C" void MultiArrayFixupRecord__ctor_m9154 ();
extern "C" void MultiArrayFixupRecord_FixupImpl_m9155 ();
extern "C" void FixupRecord__ctor_m9156 ();
extern "C" void FixupRecord_FixupImpl_m9157 ();
extern "C" void DelayedFixupRecord__ctor_m9158 ();
extern "C" void DelayedFixupRecord_FixupImpl_m9159 ();
extern "C" void ObjectRecord__ctor_m9160 ();
extern "C" void ObjectRecord_SetMemberValue_m9161 ();
extern "C" void ObjectRecord_SetArrayValue_m9162 ();
extern "C" void ObjectRecord_SetMemberValue_m9163 ();
extern "C" void ObjectRecord_get_IsInstanceReady_m9164 ();
extern "C" void ObjectRecord_get_IsUnsolvedObjectReference_m9165 ();
extern "C" void ObjectRecord_get_IsRegistered_m9166 ();
extern "C" void ObjectRecord_DoFixups_m9167 ();
extern "C" void ObjectRecord_RemoveFixup_m9168 ();
extern "C" void ObjectRecord_UnchainFixup_m9169 ();
extern "C" void ObjectRecord_ChainFixup_m9170 ();
extern "C" void ObjectRecord_LoadData_m9171 ();
extern "C" void ObjectRecord_get_HasPendingFixups_m9172 ();
extern "C" void SerializationBinder__ctor_m9173 ();
extern "C" void CallbackHandler__ctor_m9174 ();
extern "C" void CallbackHandler_Invoke_m9175 ();
extern "C" void CallbackHandler_BeginInvoke_m9176 ();
extern "C" void CallbackHandler_EndInvoke_m9177 ();
extern "C" void SerializationCallbacks__ctor_m9178 ();
extern "C" void SerializationCallbacks__cctor_m9179 ();
extern "C" void SerializationCallbacks_get_HasDeserializedCallbacks_m9180 ();
extern "C" void SerializationCallbacks_GetMethodsByAttribute_m9181 ();
extern "C" void SerializationCallbacks_Invoke_m9182 ();
extern "C" void SerializationCallbacks_RaiseOnDeserializing_m9183 ();
extern "C" void SerializationCallbacks_RaiseOnDeserialized_m9184 ();
extern "C" void SerializationCallbacks_GetSerializationCallbacks_m9185 ();
extern "C" void SerializationEntry__ctor_m9186 ();
extern "C" void SerializationEntry_get_Name_m9187 ();
extern "C" void SerializationEntry_get_Value_m9188 ();
extern "C" void SerializationException__ctor_m9189 ();
extern "C" void SerializationException__ctor_m5663 ();
extern "C" void SerializationException__ctor_m9190 ();
extern "C" void SerializationInfo__ctor_m9191 ();
extern "C" void SerializationInfo_AddValue_m5659 ();
extern "C" void SerializationInfo_GetValue_m5662 ();
extern "C" void SerializationInfo_SetType_m9192 ();
extern "C" void SerializationInfo_GetEnumerator_m9193 ();
extern "C" void SerializationInfo_AddValue_m9194 ();
extern "C" void SerializationInfo_AddValue_m5661 ();
extern "C" void SerializationInfo_AddValue_m5660 ();
extern "C" void SerializationInfo_AddValue_m9195 ();
extern "C" void SerializationInfo_AddValue_m9196 ();
extern "C" void SerializationInfo_AddValue_m5670 ();
extern "C" void SerializationInfo_AddValue_m9197 ();
extern "C" void SerializationInfo_AddValue_m4688 ();
extern "C" void SerializationInfo_GetBoolean_m5664 ();
extern "C" void SerializationInfo_GetInt16_m9198 ();
extern "C" void SerializationInfo_GetInt32_m5669 ();
extern "C" void SerializationInfo_GetInt64_m5668 ();
extern "C" void SerializationInfo_GetString_m5667 ();
extern "C" void SerializationInfoEnumerator__ctor_m9199 ();
extern "C" void SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m9200 ();
extern "C" void SerializationInfoEnumerator_get_Current_m9201 ();
extern "C" void SerializationInfoEnumerator_get_Name_m9202 ();
extern "C" void SerializationInfoEnumerator_get_Value_m9203 ();
extern "C" void SerializationInfoEnumerator_MoveNext_m9204 ();
extern "C" void SerializationInfoEnumerator_Reset_m9205 ();
extern "C" void StreamingContext__ctor_m9206 ();
extern "C" void StreamingContext__ctor_m9207 ();
extern "C" void StreamingContext_get_State_m9208 ();
extern "C" void StreamingContext_Equals_m9209 ();
extern "C" void StreamingContext_GetHashCode_m9210 ();
extern "C" void X509Certificate__ctor_m9211 ();
extern "C" void X509Certificate__ctor_m4748 ();
extern "C" void X509Certificate__ctor_m5695 ();
extern "C" void X509Certificate__ctor_m9212 ();
extern "C" void X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9213 ();
extern "C" void X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m9214 ();
extern "C" void X509Certificate_tostr_m9215 ();
extern "C" void X509Certificate_Equals_m9216 ();
extern "C" void X509Certificate_GetCertHash_m9217 ();
extern "C" void X509Certificate_GetCertHashString_m5697 ();
extern "C" void X509Certificate_GetEffectiveDateString_m9218 ();
extern "C" void X509Certificate_GetExpirationDateString_m9219 ();
extern "C" void X509Certificate_GetHashCode_m9220 ();
extern "C" void X509Certificate_GetIssuerName_m9221 ();
extern "C" void X509Certificate_GetName_m9222 ();
extern "C" void X509Certificate_GetPublicKey_m9223 ();
extern "C" void X509Certificate_GetRawCertData_m9224 ();
extern "C" void X509Certificate_ToString_m9225 ();
extern "C" void X509Certificate_ToString_m5700 ();
extern "C" void X509Certificate_get_Issuer_m5702 ();
extern "C" void X509Certificate_get_Subject_m5701 ();
extern "C" void X509Certificate_Equals_m9226 ();
extern "C" void X509Certificate_Import_m5698 ();
extern "C" void X509Certificate_Reset_m5699 ();
extern "C" void AsymmetricAlgorithm__ctor_m9227 ();
extern "C" void AsymmetricAlgorithm_System_IDisposable_Dispose_m9228 ();
extern "C" void AsymmetricAlgorithm_get_KeySize_m4662 ();
extern "C" void AsymmetricAlgorithm_set_KeySize_m4661 ();
extern "C" void AsymmetricAlgorithm_Clear_m4756 ();
extern "C" void AsymmetricAlgorithm_GetNamedParam_m9229 ();
extern "C" void AsymmetricKeyExchangeFormatter__ctor_m9230 ();
extern "C" void AsymmetricSignatureDeformatter__ctor_m4741 ();
extern "C" void AsymmetricSignatureFormatter__ctor_m4742 ();
extern "C" void Base64Constants__cctor_m9231 ();
extern "C" void CryptoConfig__cctor_m9232 ();
extern "C" void CryptoConfig_Initialize_m9233 ();
extern "C" void CryptoConfig_CreateFromName_m4653 ();
extern "C" void CryptoConfig_CreateFromName_m5708 ();
extern "C" void CryptoConfig_MapNameToOID_m4654 ();
extern "C" void CryptoConfig_EncodeOID_m4642 ();
extern "C" void CryptoConfig_EncodeLongNumber_m9234 ();
extern "C" void CryptographicException__ctor_m9235 ();
extern "C" void CryptographicException__ctor_m3725 ();
extern "C" void CryptographicException__ctor_m4676 ();
extern "C" void CryptographicException__ctor_m3734 ();
extern "C" void CryptographicException__ctor_m9236 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m9237 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m4707 ();
extern "C" void CryptographicUnexpectedOperationException__ctor_m9238 ();
extern "C" void CspParameters__ctor_m4656 ();
extern "C" void CspParameters__ctor_m9239 ();
extern "C" void CspParameters__ctor_m9240 ();
extern "C" void CspParameters__ctor_m9241 ();
extern "C" void CspParameters_get_Flags_m9242 ();
extern "C" void CspParameters_set_Flags_m4657 ();
extern "C" void DES__ctor_m9243 ();
extern "C" void DES__cctor_m9244 ();
extern "C" void DES_Create_m4708 ();
extern "C" void DES_Create_m9245 ();
extern "C" void DES_IsWeakKey_m9246 ();
extern "C" void DES_IsSemiWeakKey_m9247 ();
extern "C" void DES_get_Key_m9248 ();
extern "C" void DES_set_Key_m9249 ();
extern "C" void DESTransform__ctor_m9250 ();
extern "C" void DESTransform__cctor_m9251 ();
extern "C" void DESTransform_CipherFunct_m9252 ();
extern "C" void DESTransform_Permutation_m9253 ();
extern "C" void DESTransform_BSwap_m9254 ();
extern "C" void DESTransform_SetKey_m9255 ();
extern "C" void DESTransform_ProcessBlock_m9256 ();
extern "C" void DESTransform_ECB_m9257 ();
extern "C" void DESTransform_GetStrongKey_m9258 ();
extern "C" void DESCryptoServiceProvider__ctor_m9259 ();
extern "C" void DESCryptoServiceProvider_CreateDecryptor_m9260 ();
extern "C" void DESCryptoServiceProvider_CreateEncryptor_m9261 ();
extern "C" void DESCryptoServiceProvider_GenerateIV_m9262 ();
extern "C" void DESCryptoServiceProvider_GenerateKey_m9263 ();
extern "C" void DSA__ctor_m9264 ();
extern "C" void DSA_Create_m4659 ();
extern "C" void DSA_Create_m9265 ();
extern "C" void DSA_ZeroizePrivateKey_m9266 ();
extern "C" void DSA_FromXmlString_m9267 ();
extern "C" void DSA_ToXmlString_m9268 ();
extern "C" void DSACryptoServiceProvider__ctor_m9269 ();
extern "C" void DSACryptoServiceProvider__ctor_m4678 ();
extern "C" void DSACryptoServiceProvider__ctor_m9270 ();
extern "C" void DSACryptoServiceProvider__cctor_m9271 ();
extern "C" void DSACryptoServiceProvider_Finalize_m9272 ();
extern "C" void DSACryptoServiceProvider_get_KeySize_m9273 ();
extern "C" void DSACryptoServiceProvider_get_PublicOnly_m5689 ();
extern "C" void DSACryptoServiceProvider_ExportParameters_m9274 ();
extern "C" void DSACryptoServiceProvider_ImportParameters_m9275 ();
extern "C" void DSACryptoServiceProvider_CreateSignature_m9276 ();
extern "C" void DSACryptoServiceProvider_VerifySignature_m9277 ();
extern "C" void DSACryptoServiceProvider_Dispose_m9278 ();
extern "C" void DSACryptoServiceProvider_OnKeyGenerated_m9279 ();
extern "C" void DSASignatureDeformatter__ctor_m9280 ();
extern "C" void DSASignatureDeformatter__ctor_m4686 ();
extern "C" void DSASignatureDeformatter_SetHashAlgorithm_m9281 ();
extern "C" void DSASignatureDeformatter_SetKey_m9282 ();
extern "C" void DSASignatureDeformatter_VerifySignature_m9283 ();
extern "C" void DSASignatureFormatter__ctor_m9284 ();
extern "C" void DSASignatureFormatter_CreateSignature_m9285 ();
extern "C" void DSASignatureFormatter_SetHashAlgorithm_m9286 ();
extern "C" void DSASignatureFormatter_SetKey_m9287 ();
extern "C" void HMAC__ctor_m9288 ();
extern "C" void HMAC_get_BlockSizeValue_m9289 ();
extern "C" void HMAC_set_BlockSizeValue_m9290 ();
extern "C" void HMAC_set_HashName_m9291 ();
extern "C" void HMAC_get_Key_m9292 ();
extern "C" void HMAC_set_Key_m9293 ();
extern "C" void HMAC_get_Block_m9294 ();
extern "C" void HMAC_KeySetup_m9295 ();
extern "C" void HMAC_Dispose_m9296 ();
extern "C" void HMAC_HashCore_m9297 ();
extern "C" void HMAC_HashFinal_m9298 ();
extern "C" void HMAC_Initialize_m9299 ();
extern "C" void HMAC_Create_m4671 ();
extern "C" void HMAC_Create_m9300 ();
extern "C" void HMACMD5__ctor_m9301 ();
extern "C" void HMACMD5__ctor_m9302 ();
extern "C" void HMACRIPEMD160__ctor_m9303 ();
extern "C" void HMACRIPEMD160__ctor_m9304 ();
extern "C" void HMACSHA1__ctor_m9305 ();
extern "C" void HMACSHA1__ctor_m9306 ();
extern "C" void HMACSHA256__ctor_m9307 ();
extern "C" void HMACSHA256__ctor_m9308 ();
extern "C" void HMACSHA384__ctor_m9309 ();
extern "C" void HMACSHA384__ctor_m9310 ();
extern "C" void HMACSHA384__cctor_m9311 ();
extern "C" void HMACSHA384_set_ProduceLegacyHmacValues_m9312 ();
extern "C" void HMACSHA512__ctor_m9313 ();
extern "C" void HMACSHA512__ctor_m9314 ();
extern "C" void HMACSHA512__cctor_m9315 ();
extern "C" void HMACSHA512_set_ProduceLegacyHmacValues_m9316 ();
extern "C" void HashAlgorithm__ctor_m4652 ();
extern "C" void HashAlgorithm_System_IDisposable_Dispose_m9317 ();
extern "C" void HashAlgorithm_get_CanReuseTransform_m9318 ();
extern "C" void HashAlgorithm_ComputeHash_m4693 ();
extern "C" void HashAlgorithm_ComputeHash_m4665 ();
extern "C" void HashAlgorithm_Create_m4664 ();
extern "C" void HashAlgorithm_get_Hash_m9319 ();
extern "C" void HashAlgorithm_get_HashSize_m9320 ();
extern "C" void HashAlgorithm_Dispose_m9321 ();
extern "C" void HashAlgorithm_TransformBlock_m9322 ();
extern "C" void HashAlgorithm_TransformFinalBlock_m9323 ();
extern "C" void KeySizes__ctor_m3736 ();
extern "C" void KeySizes_get_MaxSize_m9324 ();
extern "C" void KeySizes_get_MinSize_m9325 ();
extern "C" void KeySizes_get_SkipSize_m9326 ();
extern "C" void KeySizes_IsLegal_m9327 ();
extern "C" void KeySizes_IsLegalKeySize_m9328 ();
extern "C" void KeyedHashAlgorithm__ctor_m4706 ();
extern "C" void KeyedHashAlgorithm_Finalize_m9329 ();
extern "C" void KeyedHashAlgorithm_get_Key_m9330 ();
extern "C" void KeyedHashAlgorithm_set_Key_m9331 ();
extern "C" void KeyedHashAlgorithm_Dispose_m9332 ();
extern "C" void KeyedHashAlgorithm_ZeroizeKey_m9333 ();
extern "C" void MACTripleDES__ctor_m9334 ();
extern "C" void MACTripleDES_Setup_m9335 ();
extern "C" void MACTripleDES_Finalize_m9336 ();
extern "C" void MACTripleDES_Dispose_m9337 ();
extern "C" void MACTripleDES_Initialize_m9338 ();
extern "C" void MACTripleDES_HashCore_m9339 ();
extern "C" void MACTripleDES_HashFinal_m9340 ();
extern "C" void MD5__ctor_m9341 ();
extern "C" void MD5_Create_m4679 ();
extern "C" void MD5_Create_m9342 ();
extern "C" void MD5CryptoServiceProvider__ctor_m9343 ();
extern "C" void MD5CryptoServiceProvider__cctor_m9344 ();
extern "C" void MD5CryptoServiceProvider_Finalize_m9345 ();
extern "C" void MD5CryptoServiceProvider_Dispose_m9346 ();
extern "C" void MD5CryptoServiceProvider_HashCore_m9347 ();
extern "C" void MD5CryptoServiceProvider_HashFinal_m9348 ();
extern "C" void MD5CryptoServiceProvider_Initialize_m9349 ();
extern "C" void MD5CryptoServiceProvider_ProcessBlock_m9350 ();
extern "C" void MD5CryptoServiceProvider_ProcessFinalBlock_m9351 ();
extern "C" void MD5CryptoServiceProvider_AddLength_m9352 ();
extern "C" void RC2__ctor_m9353 ();
extern "C" void RC2_Create_m4709 ();
extern "C" void RC2_Create_m9354 ();
extern "C" void RC2_get_EffectiveKeySize_m9355 ();
extern "C" void RC2_get_KeySize_m9356 ();
extern "C" void RC2_set_KeySize_m9357 ();
extern "C" void RC2CryptoServiceProvider__ctor_m9358 ();
extern "C" void RC2CryptoServiceProvider_get_EffectiveKeySize_m9359 ();
extern "C" void RC2CryptoServiceProvider_CreateDecryptor_m9360 ();
extern "C" void RC2CryptoServiceProvider_CreateEncryptor_m9361 ();
extern "C" void RC2CryptoServiceProvider_GenerateIV_m9362 ();
extern "C" void RC2CryptoServiceProvider_GenerateKey_m9363 ();
extern "C" void RC2Transform__ctor_m9364 ();
extern "C" void RC2Transform__cctor_m9365 ();
extern "C" void RC2Transform_ECB_m9366 ();
extern "C" void RIPEMD160__ctor_m9367 ();
extern "C" void RIPEMD160Managed__ctor_m9368 ();
extern "C" void RIPEMD160Managed_Initialize_m9369 ();
extern "C" void RIPEMD160Managed_HashCore_m9370 ();
extern "C" void RIPEMD160Managed_HashFinal_m9371 ();
extern "C" void RIPEMD160Managed_Finalize_m9372 ();
extern "C" void RIPEMD160Managed_ProcessBlock_m9373 ();
extern "C" void RIPEMD160Managed_Compress_m9374 ();
extern "C" void RIPEMD160Managed_CompressFinal_m9375 ();
extern "C" void RIPEMD160Managed_ROL_m9376 ();
extern "C" void RIPEMD160Managed_F_m9377 ();
extern "C" void RIPEMD160Managed_G_m9378 ();
extern "C" void RIPEMD160Managed_H_m9379 ();
extern "C" void RIPEMD160Managed_I_m9380 ();
extern "C" void RIPEMD160Managed_J_m9381 ();
extern "C" void RIPEMD160Managed_FF_m9382 ();
extern "C" void RIPEMD160Managed_GG_m9383 ();
extern "C" void RIPEMD160Managed_HH_m9384 ();
extern "C" void RIPEMD160Managed_II_m9385 ();
extern "C" void RIPEMD160Managed_JJ_m9386 ();
extern "C" void RIPEMD160Managed_FFF_m9387 ();
extern "C" void RIPEMD160Managed_GGG_m9388 ();
extern "C" void RIPEMD160Managed_HHH_m9389 ();
extern "C" void RIPEMD160Managed_III_m9390 ();
extern "C" void RIPEMD160Managed_JJJ_m9391 ();
extern "C" void RNGCryptoServiceProvider__ctor_m9392 ();
extern "C" void RNGCryptoServiceProvider__cctor_m9393 ();
extern "C" void RNGCryptoServiceProvider_Check_m9394 ();
extern "C" void RNGCryptoServiceProvider_RngOpen_m9395 ();
extern "C" void RNGCryptoServiceProvider_RngInitialize_m9396 ();
extern "C" void RNGCryptoServiceProvider_RngGetBytes_m9397 ();
extern "C" void RNGCryptoServiceProvider_RngClose_m9398 ();
extern "C" void RNGCryptoServiceProvider_GetBytes_m9399 ();
extern "C" void RNGCryptoServiceProvider_GetNonZeroBytes_m9400 ();
extern "C" void RNGCryptoServiceProvider_Finalize_m9401 ();
extern "C" void RSA__ctor_m4660 ();
extern "C" void RSA_Create_m4655 ();
extern "C" void RSA_Create_m9402 ();
extern "C" void RSA_ZeroizePrivateKey_m9403 ();
extern "C" void RSA_FromXmlString_m9404 ();
extern "C" void RSA_ToXmlString_m9405 ();
extern "C" void RSACryptoServiceProvider__ctor_m9406 ();
extern "C" void RSACryptoServiceProvider__ctor_m4658 ();
extern "C" void RSACryptoServiceProvider__ctor_m4682 ();
extern "C" void RSACryptoServiceProvider__cctor_m9407 ();
extern "C" void RSACryptoServiceProvider_Common_m9408 ();
extern "C" void RSACryptoServiceProvider_Finalize_m9409 ();
extern "C" void RSACryptoServiceProvider_get_KeySize_m9410 ();
extern "C" void RSACryptoServiceProvider_get_PublicOnly_m5688 ();
extern "C" void RSACryptoServiceProvider_DecryptValue_m9411 ();
extern "C" void RSACryptoServiceProvider_EncryptValue_m9412 ();
extern "C" void RSACryptoServiceProvider_ExportParameters_m9413 ();
extern "C" void RSACryptoServiceProvider_ImportParameters_m9414 ();
extern "C" void RSACryptoServiceProvider_Dispose_m9415 ();
extern "C" void RSACryptoServiceProvider_OnKeyGenerated_m9416 ();
extern "C" void RSAPKCS1KeyExchangeFormatter__ctor_m4755 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m9417 ();
extern "C" void RSAPKCS1KeyExchangeFormatter_SetRSAKey_m9418 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m9419 ();
extern "C" void RSAPKCS1SignatureDeformatter__ctor_m4687 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m9420 ();
extern "C" void RSAPKCS1SignatureDeformatter_SetKey_m9421 ();
extern "C" void RSAPKCS1SignatureDeformatter_VerifySignature_m9422 ();
extern "C" void RSAPKCS1SignatureFormatter__ctor_m9423 ();
extern "C" void RSAPKCS1SignatureFormatter_CreateSignature_m9424 ();
extern "C" void RSAPKCS1SignatureFormatter_SetHashAlgorithm_m9425 ();
extern "C" void RSAPKCS1SignatureFormatter_SetKey_m9426 ();
extern "C" void RandomNumberGenerator__ctor_m9427 ();
extern "C" void RandomNumberGenerator_Create_m3724 ();
extern "C" void RandomNumberGenerator_Create_m9428 ();
extern "C" void Rijndael__ctor_m9429 ();
extern "C" void Rijndael_Create_m4711 ();
extern "C" void Rijndael_Create_m9430 ();
extern "C" void RijndaelManaged__ctor_m9431 ();
extern "C" void RijndaelManaged_GenerateIV_m9432 ();
extern "C" void RijndaelManaged_GenerateKey_m9433 ();
extern "C" void RijndaelManaged_CreateDecryptor_m9434 ();
extern "C" void RijndaelManaged_CreateEncryptor_m9435 ();
extern "C" void RijndaelTransform__ctor_m9436 ();
extern "C" void RijndaelTransform__cctor_m9437 ();
extern "C" void RijndaelTransform_Clear_m9438 ();
extern "C" void RijndaelTransform_ECB_m9439 ();
extern "C" void RijndaelTransform_SubByte_m9440 ();
extern "C" void RijndaelTransform_Encrypt128_m9441 ();
extern "C" void RijndaelTransform_Encrypt192_m9442 ();
extern "C" void RijndaelTransform_Encrypt256_m9443 ();
extern "C" void RijndaelTransform_Decrypt128_m9444 ();
extern "C" void RijndaelTransform_Decrypt192_m9445 ();
extern "C" void RijndaelTransform_Decrypt256_m9446 ();
extern "C" void RijndaelManagedTransform__ctor_m9447 ();
extern "C" void RijndaelManagedTransform_System_IDisposable_Dispose_m9448 ();
extern "C" void RijndaelManagedTransform_get_CanReuseTransform_m9449 ();
extern "C" void RijndaelManagedTransform_TransformBlock_m9450 ();
extern "C" void RijndaelManagedTransform_TransformFinalBlock_m9451 ();
extern "C" void SHA1__ctor_m9452 ();
extern "C" void SHA1_Create_m4680 ();
extern "C" void SHA1_Create_m9453 ();
extern "C" void SHA1Internal__ctor_m9454 ();
extern "C" void SHA1Internal_HashCore_m9455 ();
extern "C" void SHA1Internal_HashFinal_m9456 ();
extern "C" void SHA1Internal_Initialize_m9457 ();
extern "C" void SHA1Internal_ProcessBlock_m9458 ();
extern "C" void SHA1Internal_InitialiseBuff_m9459 ();
extern "C" void SHA1Internal_FillBuff_m9460 ();
extern "C" void SHA1Internal_ProcessFinalBlock_m9461 ();
extern "C" void SHA1Internal_AddLength_m9462 ();
extern "C" void SHA1CryptoServiceProvider__ctor_m9463 ();
extern "C" void SHA1CryptoServiceProvider_Finalize_m9464 ();
extern "C" void SHA1CryptoServiceProvider_Dispose_m9465 ();
extern "C" void SHA1CryptoServiceProvider_HashCore_m9466 ();
extern "C" void SHA1CryptoServiceProvider_HashFinal_m9467 ();
extern "C" void SHA1CryptoServiceProvider_Initialize_m9468 ();
extern "C" void SHA1Managed__ctor_m9469 ();
extern "C" void SHA1Managed_HashCore_m9470 ();
extern "C" void SHA1Managed_HashFinal_m9471 ();
extern "C" void SHA1Managed_Initialize_m9472 ();
extern "C" void SHA256__ctor_m9473 ();
extern "C" void SHA256_Create_m4681 ();
extern "C" void SHA256_Create_m9474 ();
extern "C" void SHA256Managed__ctor_m9475 ();
extern "C" void SHA256Managed_HashCore_m9476 ();
extern "C" void SHA256Managed_HashFinal_m9477 ();
extern "C" void SHA256Managed_Initialize_m9478 ();
extern "C" void SHA256Managed_ProcessBlock_m9479 ();
extern "C" void SHA256Managed_ProcessFinalBlock_m9480 ();
extern "C" void SHA256Managed_AddLength_m9481 ();
extern "C" void SHA384__ctor_m9482 ();
extern "C" void SHA384Managed__ctor_m9483 ();
extern "C" void SHA384Managed_Initialize_m9484 ();
extern "C" void SHA384Managed_Initialize_m9485 ();
extern "C" void SHA384Managed_HashCore_m9486 ();
extern "C" void SHA384Managed_HashFinal_m9487 ();
extern "C" void SHA384Managed_update_m9488 ();
extern "C" void SHA384Managed_processWord_m9489 ();
extern "C" void SHA384Managed_unpackWord_m9490 ();
extern "C" void SHA384Managed_adjustByteCounts_m9491 ();
extern "C" void SHA384Managed_processLength_m9492 ();
extern "C" void SHA384Managed_processBlock_m9493 ();
extern "C" void SHA512__ctor_m9494 ();
extern "C" void SHA512Managed__ctor_m9495 ();
extern "C" void SHA512Managed_Initialize_m9496 ();
extern "C" void SHA512Managed_Initialize_m9497 ();
extern "C" void SHA512Managed_HashCore_m9498 ();
extern "C" void SHA512Managed_HashFinal_m9499 ();
extern "C" void SHA512Managed_update_m9500 ();
extern "C" void SHA512Managed_processWord_m9501 ();
extern "C" void SHA512Managed_unpackWord_m9502 ();
extern "C" void SHA512Managed_adjustByteCounts_m9503 ();
extern "C" void SHA512Managed_processLength_m9504 ();
extern "C" void SHA512Managed_processBlock_m9505 ();
extern "C" void SHA512Managed_rotateRight_m9506 ();
extern "C" void SHA512Managed_Ch_m9507 ();
extern "C" void SHA512Managed_Maj_m9508 ();
extern "C" void SHA512Managed_Sum0_m9509 ();
extern "C" void SHA512Managed_Sum1_m9510 ();
extern "C" void SHA512Managed_Sigma0_m9511 ();
extern "C" void SHA512Managed_Sigma1_m9512 ();
extern "C" void SHAConstants__cctor_m9513 ();
extern "C" void SignatureDescription__ctor_m9514 ();
extern "C" void SignatureDescription_set_DeformatterAlgorithm_m9515 ();
extern "C" void SignatureDescription_set_DigestAlgorithm_m9516 ();
extern "C" void SignatureDescription_set_FormatterAlgorithm_m9517 ();
extern "C" void SignatureDescription_set_KeyAlgorithm_m9518 ();
extern "C" void DSASignatureDescription__ctor_m9519 ();
extern "C" void RSAPKCS1SHA1SignatureDescription__ctor_m9520 ();
extern "C" void SymmetricAlgorithm__ctor_m3735 ();
extern "C" void SymmetricAlgorithm_System_IDisposable_Dispose_m9521 ();
extern "C" void SymmetricAlgorithm_Finalize_m4650 ();
extern "C" void SymmetricAlgorithm_Clear_m4670 ();
extern "C" void SymmetricAlgorithm_Dispose_m3743 ();
extern "C" void SymmetricAlgorithm_get_BlockSize_m9522 ();
extern "C" void SymmetricAlgorithm_set_BlockSize_m9523 ();
extern "C" void SymmetricAlgorithm_get_FeedbackSize_m9524 ();
extern "C" void SymmetricAlgorithm_get_IV_m3737 ();
extern "C" void SymmetricAlgorithm_set_IV_m3738 ();
extern "C" void SymmetricAlgorithm_get_Key_m3739 ();
extern "C" void SymmetricAlgorithm_set_Key_m3740 ();
extern "C" void SymmetricAlgorithm_get_KeySize_m3741 ();
extern "C" void SymmetricAlgorithm_set_KeySize_m3742 ();
extern "C" void SymmetricAlgorithm_get_LegalKeySizes_m9525 ();
extern "C" void SymmetricAlgorithm_get_Mode_m9526 ();
extern "C" void SymmetricAlgorithm_set_Mode_m9527 ();
extern "C" void SymmetricAlgorithm_get_Padding_m9528 ();
extern "C" void SymmetricAlgorithm_set_Padding_m9529 ();
extern "C" void SymmetricAlgorithm_CreateDecryptor_m9530 ();
extern "C" void SymmetricAlgorithm_CreateEncryptor_m9531 ();
extern "C" void SymmetricAlgorithm_Create_m4669 ();
extern "C" void ToBase64Transform_System_IDisposable_Dispose_m9532 ();
extern "C" void ToBase64Transform_Finalize_m9533 ();
extern "C" void ToBase64Transform_get_CanReuseTransform_m9534 ();
extern "C" void ToBase64Transform_get_InputBlockSize_m9535 ();
extern "C" void ToBase64Transform_get_OutputBlockSize_m9536 ();
extern "C" void ToBase64Transform_Dispose_m9537 ();
extern "C" void ToBase64Transform_TransformBlock_m9538 ();
extern "C" void ToBase64Transform_InternalTransformBlock_m9539 ();
extern "C" void ToBase64Transform_TransformFinalBlock_m9540 ();
extern "C" void ToBase64Transform_InternalTransformFinalBlock_m9541 ();
extern "C" void TripleDES__ctor_m9542 ();
extern "C" void TripleDES_get_Key_m9543 ();
extern "C" void TripleDES_set_Key_m9544 ();
extern "C" void TripleDES_IsWeakKey_m9545 ();
extern "C" void TripleDES_Create_m4710 ();
extern "C" void TripleDES_Create_m9546 ();
extern "C" void TripleDESCryptoServiceProvider__ctor_m9547 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateIV_m9548 ();
extern "C" void TripleDESCryptoServiceProvider_GenerateKey_m9549 ();
extern "C" void TripleDESCryptoServiceProvider_CreateDecryptor_m9550 ();
extern "C" void TripleDESCryptoServiceProvider_CreateEncryptor_m9551 ();
extern "C" void TripleDESTransform__ctor_m9552 ();
extern "C" void TripleDESTransform_ECB_m9553 ();
extern "C" void TripleDESTransform_GetStrongKey_m9554 ();
extern "C" void SecurityPermission__ctor_m9555 ();
extern "C" void SecurityPermission_set_Flags_m9556 ();
extern "C" void SecurityPermission_IsUnrestricted_m9557 ();
extern "C" void SecurityPermission_IsSubsetOf_m9558 ();
extern "C" void SecurityPermission_ToXml_m9559 ();
extern "C" void SecurityPermission_IsEmpty_m9560 ();
extern "C" void SecurityPermission_Cast_m9561 ();
extern "C" void StrongNamePublicKeyBlob_Equals_m9562 ();
extern "C" void StrongNamePublicKeyBlob_GetHashCode_m9563 ();
extern "C" void StrongNamePublicKeyBlob_ToString_m9564 ();
extern "C" void ApplicationTrust__ctor_m9565 ();
extern "C" void EvidenceEnumerator__ctor_m9566 ();
extern "C" void EvidenceEnumerator_MoveNext_m9567 ();
extern "C" void EvidenceEnumerator_Reset_m9568 ();
extern "C" void EvidenceEnumerator_get_Current_m9569 ();
extern "C" void Evidence__ctor_m9570 ();
extern "C" void Evidence_get_Count_m9571 ();
extern "C" void Evidence_get_IsSynchronized_m9572 ();
extern "C" void Evidence_get_SyncRoot_m9573 ();
extern "C" void Evidence_get_HostEvidenceList_m9574 ();
extern "C" void Evidence_get_AssemblyEvidenceList_m9575 ();
extern "C" void Evidence_CopyTo_m9576 ();
extern "C" void Evidence_Equals_m9577 ();
extern "C" void Evidence_GetEnumerator_m9578 ();
extern "C" void Evidence_GetHashCode_m9579 ();
extern "C" void Hash__ctor_m9580 ();
extern "C" void Hash__ctor_m9581 ();
extern "C" void Hash_GetObjectData_m9582 ();
extern "C" void Hash_ToString_m9583 ();
extern "C" void Hash_GetData_m9584 ();
extern "C" void StrongName_get_Name_m9585 ();
extern "C" void StrongName_get_PublicKey_m9586 ();
extern "C" void StrongName_get_Version_m9587 ();
extern "C" void StrongName_Equals_m9588 ();
extern "C" void StrongName_GetHashCode_m9589 ();
extern "C" void StrongName_ToString_m9590 ();
extern "C" void WindowsIdentity__ctor_m9591 ();
extern "C" void WindowsIdentity__cctor_m9592 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9593 ();
extern "C" void WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9594 ();
extern "C" void WindowsIdentity_Dispose_m9595 ();
extern "C" void WindowsIdentity_GetCurrentToken_m9596 ();
extern "C" void WindowsIdentity_GetTokenName_m9597 ();
extern "C" void CodeAccessPermission__ctor_m9598 ();
extern "C" void CodeAccessPermission_Equals_m9599 ();
extern "C" void CodeAccessPermission_GetHashCode_m9600 ();
extern "C" void CodeAccessPermission_ToString_m9601 ();
extern "C" void CodeAccessPermission_Element_m9602 ();
extern "C" void CodeAccessPermission_ThrowInvalidPermission_m9603 ();
extern "C" void PermissionSet__ctor_m9604 ();
extern "C" void PermissionSet__ctor_m9605 ();
extern "C" void PermissionSet_set_DeclarativeSecurity_m9606 ();
extern "C" void PermissionSet_CreateFromBinaryFormat_m9607 ();
extern "C" void SecurityContext__ctor_m9608 ();
extern "C" void SecurityContext__ctor_m9609 ();
extern "C" void SecurityContext_Capture_m9610 ();
extern "C" void SecurityContext_get_FlowSuppressed_m9611 ();
extern "C" void SecurityContext_get_CompressedStack_m9612 ();
extern "C" void SecurityAttribute__ctor_m9613 ();
extern "C" void SecurityAttribute_get_Name_m9614 ();
extern "C" void SecurityAttribute_get_Value_m9615 ();
extern "C" void SecurityElement__ctor_m9616 ();
extern "C" void SecurityElement__ctor_m9617 ();
extern "C" void SecurityElement__cctor_m9618 ();
extern "C" void SecurityElement_get_Children_m9619 ();
extern "C" void SecurityElement_get_Tag_m9620 ();
extern "C" void SecurityElement_set_Text_m9621 ();
extern "C" void SecurityElement_AddAttribute_m9622 ();
extern "C" void SecurityElement_AddChild_m9623 ();
extern "C" void SecurityElement_Escape_m9624 ();
extern "C" void SecurityElement_Unescape_m9625 ();
extern "C" void SecurityElement_IsValidAttributeName_m9626 ();
extern "C" void SecurityElement_IsValidAttributeValue_m9627 ();
extern "C" void SecurityElement_IsValidTag_m9628 ();
extern "C" void SecurityElement_IsValidText_m9629 ();
extern "C" void SecurityElement_SearchForChildByTag_m9630 ();
extern "C" void SecurityElement_ToString_m9631 ();
extern "C" void SecurityElement_ToXml_m9632 ();
extern "C" void SecurityElement_GetAttribute_m9633 ();
extern "C" void SecurityException__ctor_m9634 ();
extern "C" void SecurityException__ctor_m9635 ();
extern "C" void SecurityException__ctor_m9636 ();
extern "C" void SecurityException_get_Demanded_m9637 ();
extern "C" void SecurityException_get_FirstPermissionThatFailed_m9638 ();
extern "C" void SecurityException_get_PermissionState_m9639 ();
extern "C" void SecurityException_get_PermissionType_m9640 ();
extern "C" void SecurityException_get_GrantedSet_m9641 ();
extern "C" void SecurityException_get_RefusedSet_m9642 ();
extern "C" void SecurityException_GetObjectData_m9643 ();
extern "C" void SecurityException_ToString_m9644 ();
extern "C" void SecurityFrame__ctor_m9645 ();
extern "C" void SecurityFrame__GetSecurityStack_m9646 ();
extern "C" void SecurityFrame_InitFromRuntimeFrame_m9647 ();
extern "C" void SecurityFrame_get_Assembly_m9648 ();
extern "C" void SecurityFrame_get_Domain_m9649 ();
extern "C" void SecurityFrame_ToString_m9650 ();
extern "C" void SecurityFrame_GetStack_m9651 ();
extern "C" void SecurityManager__cctor_m9652 ();
extern "C" void SecurityManager_get_SecurityEnabled_m9653 ();
extern "C" void SecurityManager_Decode_m9654 ();
extern "C" void SecurityManager_Decode_m9655 ();
extern "C" void SecuritySafeCriticalAttribute__ctor_m9656 ();
extern "C" void SuppressUnmanagedCodeSecurityAttribute__ctor_m9657 ();
extern "C" void UnverifiableCodeAttribute__ctor_m9658 ();
extern "C" void ASCIIEncoding__ctor_m9659 ();
extern "C" void ASCIIEncoding_GetByteCount_m9660 ();
extern "C" void ASCIIEncoding_GetByteCount_m9661 ();
extern "C" void ASCIIEncoding_GetBytes_m9662 ();
extern "C" void ASCIIEncoding_GetBytes_m9663 ();
extern "C" void ASCIIEncoding_GetBytes_m9664 ();
extern "C" void ASCIIEncoding_GetBytes_m9665 ();
extern "C" void ASCIIEncoding_GetCharCount_m9666 ();
extern "C" void ASCIIEncoding_GetChars_m9667 ();
extern "C" void ASCIIEncoding_GetChars_m9668 ();
extern "C" void ASCIIEncoding_GetMaxByteCount_m9669 ();
extern "C" void ASCIIEncoding_GetMaxCharCount_m9670 ();
extern "C" void ASCIIEncoding_GetString_m9671 ();
extern "C" void ASCIIEncoding_GetBytes_m9672 ();
extern "C" void ASCIIEncoding_GetByteCount_m9673 ();
extern "C" void ASCIIEncoding_GetDecoder_m9674 ();
extern "C" void Decoder__ctor_m9675 ();
extern "C" void Decoder_set_Fallback_m9676 ();
extern "C" void Decoder_get_FallbackBuffer_m9677 ();
extern "C" void DecoderExceptionFallback__ctor_m9678 ();
extern "C" void DecoderExceptionFallback_CreateFallbackBuffer_m9679 ();
extern "C" void DecoderExceptionFallback_Equals_m9680 ();
extern "C" void DecoderExceptionFallback_GetHashCode_m9681 ();
extern "C" void DecoderExceptionFallbackBuffer__ctor_m9682 ();
extern "C" void DecoderExceptionFallbackBuffer_get_Remaining_m9683 ();
extern "C" void DecoderExceptionFallbackBuffer_Fallback_m9684 ();
extern "C" void DecoderExceptionFallbackBuffer_GetNextChar_m9685 ();
extern "C" void DecoderFallback__ctor_m9686 ();
extern "C" void DecoderFallback__cctor_m9687 ();
extern "C" void DecoderFallback_get_ExceptionFallback_m9688 ();
extern "C" void DecoderFallback_get_ReplacementFallback_m9689 ();
extern "C" void DecoderFallback_get_StandardSafeFallback_m9690 ();
extern "C" void DecoderFallbackBuffer__ctor_m9691 ();
extern "C" void DecoderFallbackBuffer_Reset_m9692 ();
extern "C" void DecoderFallbackException__ctor_m9693 ();
extern "C" void DecoderFallbackException__ctor_m9694 ();
extern "C" void DecoderFallbackException__ctor_m9695 ();
extern "C" void DecoderReplacementFallback__ctor_m9696 ();
extern "C" void DecoderReplacementFallback__ctor_m9697 ();
extern "C" void DecoderReplacementFallback_get_DefaultString_m9698 ();
extern "C" void DecoderReplacementFallback_CreateFallbackBuffer_m9699 ();
extern "C" void DecoderReplacementFallback_Equals_m9700 ();
extern "C" void DecoderReplacementFallback_GetHashCode_m9701 ();
extern "C" void DecoderReplacementFallbackBuffer__ctor_m9702 ();
extern "C" void DecoderReplacementFallbackBuffer_get_Remaining_m9703 ();
extern "C" void DecoderReplacementFallbackBuffer_Fallback_m9704 ();
extern "C" void DecoderReplacementFallbackBuffer_GetNextChar_m9705 ();
extern "C" void DecoderReplacementFallbackBuffer_Reset_m9706 ();
extern "C" void EncoderExceptionFallback__ctor_m9707 ();
extern "C" void EncoderExceptionFallback_CreateFallbackBuffer_m9708 ();
extern "C" void EncoderExceptionFallback_Equals_m9709 ();
extern "C" void EncoderExceptionFallback_GetHashCode_m9710 ();
extern "C" void EncoderExceptionFallbackBuffer__ctor_m9711 ();
extern "C" void EncoderExceptionFallbackBuffer_get_Remaining_m9712 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m9713 ();
extern "C" void EncoderExceptionFallbackBuffer_Fallback_m9714 ();
extern "C" void EncoderExceptionFallbackBuffer_GetNextChar_m9715 ();
extern "C" void EncoderFallback__ctor_m9716 ();
extern "C" void EncoderFallback__cctor_m9717 ();
extern "C" void EncoderFallback_get_ExceptionFallback_m9718 ();
extern "C" void EncoderFallback_get_ReplacementFallback_m9719 ();
extern "C" void EncoderFallback_get_StandardSafeFallback_m9720 ();
extern "C" void EncoderFallbackBuffer__ctor_m9721 ();
extern "C" void EncoderFallbackException__ctor_m9722 ();
extern "C" void EncoderFallbackException__ctor_m9723 ();
extern "C" void EncoderFallbackException__ctor_m9724 ();
extern "C" void EncoderFallbackException__ctor_m9725 ();
extern "C" void EncoderReplacementFallback__ctor_m9726 ();
extern "C" void EncoderReplacementFallback__ctor_m9727 ();
extern "C" void EncoderReplacementFallback_get_DefaultString_m9728 ();
extern "C" void EncoderReplacementFallback_CreateFallbackBuffer_m9729 ();
extern "C" void EncoderReplacementFallback_Equals_m9730 ();
extern "C" void EncoderReplacementFallback_GetHashCode_m9731 ();
extern "C" void EncoderReplacementFallbackBuffer__ctor_m9732 ();
extern "C" void EncoderReplacementFallbackBuffer_get_Remaining_m9733 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9734 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9735 ();
extern "C" void EncoderReplacementFallbackBuffer_Fallback_m9736 ();
extern "C" void EncoderReplacementFallbackBuffer_GetNextChar_m9737 ();
extern "C" void ForwardingDecoder__ctor_m9738 ();
extern "C" void ForwardingDecoder_GetChars_m9739 ();
extern "C" void Encoding__ctor_m9740 ();
extern "C" void Encoding__ctor_m9741 ();
extern "C" void Encoding__cctor_m9742 ();
extern "C" void Encoding___m9743 ();
extern "C" void Encoding_get_IsReadOnly_m9744 ();
extern "C" void Encoding_get_DecoderFallback_m9745 ();
extern "C" void Encoding_set_DecoderFallback_m9746 ();
extern "C" void Encoding_get_EncoderFallback_m9747 ();
extern "C" void Encoding_SetFallbackInternal_m9748 ();
extern "C" void Encoding_Equals_m9749 ();
extern "C" void Encoding_GetByteCount_m9750 ();
extern "C" void Encoding_GetByteCount_m9751 ();
extern "C" void Encoding_GetBytes_m9752 ();
extern "C" void Encoding_GetBytes_m9753 ();
extern "C" void Encoding_GetBytes_m9754 ();
extern "C" void Encoding_GetBytes_m9755 ();
extern "C" void Encoding_GetChars_m9756 ();
extern "C" void Encoding_GetDecoder_m9757 ();
extern "C" void Encoding_InvokeI18N_m9758 ();
extern "C" void Encoding_GetEncoding_m9759 ();
extern "C" void Encoding_Clone_m9760 ();
extern "C" void Encoding_GetEncoding_m9761 ();
extern "C" void Encoding_GetHashCode_m9762 ();
extern "C" void Encoding_GetPreamble_m9763 ();
extern "C" void Encoding_GetString_m9764 ();
extern "C" void Encoding_GetString_m9765 ();
extern "C" void Encoding_get_ASCII_m4647 ();
extern "C" void Encoding_get_BigEndianUnicode_m4666 ();
extern "C" void Encoding_InternalCodePage_m9766 ();
extern "C" void Encoding_get_Default_m9767 ();
extern "C" void Encoding_get_ISOLatin1_m9768 ();
extern "C" void Encoding_get_UTF7_m4673 ();
extern "C" void Encoding_get_UTF8_m4674 ();
extern "C" void Encoding_get_UTF8Unmarked_m9769 ();
extern "C" void Encoding_get_UTF8UnmarkedUnsafe_m9770 ();
extern "C" void Encoding_get_Unicode_m9771 ();
extern "C" void Encoding_get_UTF32_m9772 ();
extern "C" void Encoding_get_BigEndianUTF32_m9773 ();
extern "C" void Encoding_GetByteCount_m9774 ();
extern "C" void Encoding_GetBytes_m9775 ();
extern "C" void Latin1Encoding__ctor_m9776 ();
extern "C" void Latin1Encoding_GetByteCount_m9777 ();
extern "C" void Latin1Encoding_GetByteCount_m9778 ();
extern "C" void Latin1Encoding_GetBytes_m9779 ();
extern "C" void Latin1Encoding_GetBytes_m9780 ();
extern "C" void Latin1Encoding_GetBytes_m9781 ();
extern "C" void Latin1Encoding_GetBytes_m9782 ();
extern "C" void Latin1Encoding_GetCharCount_m9783 ();
extern "C" void Latin1Encoding_GetChars_m9784 ();
extern "C" void Latin1Encoding_GetMaxByteCount_m9785 ();
extern "C" void Latin1Encoding_GetMaxCharCount_m9786 ();
extern "C" void Latin1Encoding_GetString_m9787 ();
extern "C" void Latin1Encoding_GetString_m9788 ();
extern "C" void StringBuilder__ctor_m9789 ();
extern "C" void StringBuilder__ctor_m9790 ();
extern "C" void StringBuilder__ctor_m1869 ();
extern "C" void StringBuilder__ctor_m3639 ();
extern "C" void StringBuilder__ctor_m1928 ();
extern "C" void StringBuilder__ctor_m5665 ();
extern "C" void StringBuilder__ctor_m9791 ();
extern "C" void StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m9792 ();
extern "C" void StringBuilder_get_Capacity_m9793 ();
extern "C" void StringBuilder_set_Capacity_m9794 ();
extern "C" void StringBuilder_get_Length_m4704 ();
extern "C" void StringBuilder_set_Length_m5732 ();
extern "C" void StringBuilder_get_Chars_m9795 ();
extern "C" void StringBuilder_set_Chars_m9796 ();
extern "C" void StringBuilder_ToString_m1873 ();
extern "C" void StringBuilder_ToString_m9797 ();
extern "C" void StringBuilder_Remove_m9798 ();
extern "C" void StringBuilder_Replace_m9799 ();
extern "C" void StringBuilder_Replace_m9800 ();
extern "C" void StringBuilder_Append_m3642 ();
extern "C" void StringBuilder_Append_m5694 ();
extern "C" void StringBuilder_Append_m5678 ();
extern "C" void StringBuilder_Append_m5666 ();
extern "C" void StringBuilder_Append_m4672 ();
extern "C" void StringBuilder_Append_m9801 ();
extern "C" void StringBuilder_Append_m9802 ();
extern "C" void StringBuilder_Append_m5711 ();
extern "C" void StringBuilder_AppendLine_m1872 ();
extern "C" void StringBuilder_AppendLine_m1871 ();
extern "C" void StringBuilder_AppendFormat_m4640 ();
extern "C" void StringBuilder_AppendFormat_m9803 ();
extern "C" void StringBuilder_AppendFormat_m4639 ();
extern "C" void StringBuilder_AppendFormat_m4638 ();
extern "C" void StringBuilder_AppendFormat_m5703 ();
extern "C" void StringBuilder_Insert_m9804 ();
extern "C" void StringBuilder_Insert_m9805 ();
extern "C" void StringBuilder_Insert_m9806 ();
extern "C" void StringBuilder_InternalEnsureCapacity_m9807 ();
extern "C" void UTF32Decoder__ctor_m9808 ();
extern "C" void UTF32Decoder_GetChars_m9809 ();
extern "C" void UTF32Encoding__ctor_m9810 ();
extern "C" void UTF32Encoding__ctor_m9811 ();
extern "C" void UTF32Encoding__ctor_m9812 ();
extern "C" void UTF32Encoding_GetByteCount_m9813 ();
extern "C" void UTF32Encoding_GetBytes_m9814 ();
extern "C" void UTF32Encoding_GetCharCount_m9815 ();
extern "C" void UTF32Encoding_GetChars_m9816 ();
extern "C" void UTF32Encoding_GetMaxByteCount_m9817 ();
extern "C" void UTF32Encoding_GetMaxCharCount_m9818 ();
extern "C" void UTF32Encoding_GetDecoder_m9819 ();
extern "C" void UTF32Encoding_GetPreamble_m9820 ();
extern "C" void UTF32Encoding_Equals_m9821 ();
extern "C" void UTF32Encoding_GetHashCode_m9822 ();
extern "C" void UTF32Encoding_GetByteCount_m9823 ();
extern "C" void UTF32Encoding_GetByteCount_m9824 ();
extern "C" void UTF32Encoding_GetBytes_m9825 ();
extern "C" void UTF32Encoding_GetBytes_m9826 ();
extern "C" void UTF32Encoding_GetString_m9827 ();
extern "C" void UTF7Decoder__ctor_m9828 ();
extern "C" void UTF7Decoder_GetChars_m9829 ();
extern "C" void UTF7Encoding__ctor_m9830 ();
extern "C" void UTF7Encoding__ctor_m9831 ();
extern "C" void UTF7Encoding__cctor_m9832 ();
extern "C" void UTF7Encoding_GetHashCode_m9833 ();
extern "C" void UTF7Encoding_Equals_m9834 ();
extern "C" void UTF7Encoding_InternalGetByteCount_m9835 ();
extern "C" void UTF7Encoding_GetByteCount_m9836 ();
extern "C" void UTF7Encoding_InternalGetBytes_m9837 ();
extern "C" void UTF7Encoding_GetBytes_m9838 ();
extern "C" void UTF7Encoding_InternalGetCharCount_m9839 ();
extern "C" void UTF7Encoding_GetCharCount_m9840 ();
extern "C" void UTF7Encoding_InternalGetChars_m9841 ();
extern "C" void UTF7Encoding_GetChars_m9842 ();
extern "C" void UTF7Encoding_GetMaxByteCount_m9843 ();
extern "C" void UTF7Encoding_GetMaxCharCount_m9844 ();
extern "C" void UTF7Encoding_GetDecoder_m9845 ();
extern "C" void UTF7Encoding_GetByteCount_m9846 ();
extern "C" void UTF7Encoding_GetByteCount_m9847 ();
extern "C" void UTF7Encoding_GetBytes_m9848 ();
extern "C" void UTF7Encoding_GetBytes_m9849 ();
extern "C" void UTF7Encoding_GetString_m9850 ();
extern "C" void UTF8Decoder__ctor_m9851 ();
extern "C" void UTF8Decoder_GetChars_m9852 ();
extern "C" void UTF8Encoding__ctor_m9853 ();
extern "C" void UTF8Encoding__ctor_m9854 ();
extern "C" void UTF8Encoding__ctor_m9855 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m9856 ();
extern "C" void UTF8Encoding_InternalGetByteCount_m9857 ();
extern "C" void UTF8Encoding_GetByteCount_m9858 ();
extern "C" void UTF8Encoding_GetByteCount_m9859 ();
extern "C" void UTF8Encoding_InternalGetBytes_m9860 ();
extern "C" void UTF8Encoding_InternalGetBytes_m9861 ();
extern "C" void UTF8Encoding_GetBytes_m9862 ();
extern "C" void UTF8Encoding_GetBytes_m9863 ();
extern "C" void UTF8Encoding_GetBytes_m9864 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m9865 ();
extern "C" void UTF8Encoding_InternalGetCharCount_m9866 ();
extern "C" void UTF8Encoding_Fallback_m9867 ();
extern "C" void UTF8Encoding_Fallback_m9868 ();
extern "C" void UTF8Encoding_GetCharCount_m9869 ();
extern "C" void UTF8Encoding_InternalGetChars_m9870 ();
extern "C" void UTF8Encoding_InternalGetChars_m9871 ();
extern "C" void UTF8Encoding_GetChars_m9872 ();
extern "C" void UTF8Encoding_GetMaxByteCount_m9873 ();
extern "C" void UTF8Encoding_GetMaxCharCount_m9874 ();
extern "C" void UTF8Encoding_GetDecoder_m9875 ();
extern "C" void UTF8Encoding_GetPreamble_m9876 ();
extern "C" void UTF8Encoding_Equals_m9877 ();
extern "C" void UTF8Encoding_GetHashCode_m9878 ();
extern "C" void UTF8Encoding_GetByteCount_m9879 ();
extern "C" void UTF8Encoding_GetString_m9880 ();
extern "C" void UnicodeDecoder__ctor_m9881 ();
extern "C" void UnicodeDecoder_GetChars_m9882 ();
extern "C" void UnicodeEncoding__ctor_m9883 ();
extern "C" void UnicodeEncoding__ctor_m9884 ();
extern "C" void UnicodeEncoding__ctor_m9885 ();
extern "C" void UnicodeEncoding_GetByteCount_m9886 ();
extern "C" void UnicodeEncoding_GetByteCount_m9887 ();
extern "C" void UnicodeEncoding_GetByteCount_m9888 ();
extern "C" void UnicodeEncoding_GetBytes_m9889 ();
extern "C" void UnicodeEncoding_GetBytes_m9890 ();
extern "C" void UnicodeEncoding_GetBytes_m9891 ();
extern "C" void UnicodeEncoding_GetBytesInternal_m9892 ();
extern "C" void UnicodeEncoding_GetCharCount_m9893 ();
extern "C" void UnicodeEncoding_GetChars_m9894 ();
extern "C" void UnicodeEncoding_GetString_m9895 ();
extern "C" void UnicodeEncoding_GetCharsInternal_m9896 ();
extern "C" void UnicodeEncoding_GetMaxByteCount_m9897 ();
extern "C" void UnicodeEncoding_GetMaxCharCount_m9898 ();
extern "C" void UnicodeEncoding_GetDecoder_m9899 ();
extern "C" void UnicodeEncoding_GetPreamble_m9900 ();
extern "C" void UnicodeEncoding_Equals_m9901 ();
extern "C" void UnicodeEncoding_GetHashCode_m9902 ();
extern "C" void UnicodeEncoding_CopyChars_m9903 ();
extern "C" void CompressedStack__ctor_m9904 ();
extern "C" void CompressedStack__ctor_m9905 ();
extern "C" void CompressedStack_CreateCopy_m9906 ();
extern "C" void CompressedStack_Capture_m9907 ();
extern "C" void CompressedStack_GetObjectData_m9908 ();
extern "C" void CompressedStack_IsEmpty_m9909 ();
extern "C" void EventWaitHandle__ctor_m9910 ();
extern "C" void EventWaitHandle_IsManualReset_m9911 ();
extern "C" void EventWaitHandle_Reset_m4738 ();
extern "C" void EventWaitHandle_Set_m4736 ();
extern "C" void ExecutionContext__ctor_m9912 ();
extern "C" void ExecutionContext__ctor_m9913 ();
extern "C" void ExecutionContext__ctor_m9914 ();
extern "C" void ExecutionContext_Capture_m9915 ();
extern "C" void ExecutionContext_GetObjectData_m9916 ();
extern "C" void ExecutionContext_get_SecurityContext_m9917 ();
extern "C" void ExecutionContext_set_SecurityContext_m9918 ();
extern "C" void ExecutionContext_get_FlowSuppressed_m9919 ();
extern "C" void ExecutionContext_IsFlowSuppressed_m9920 ();
extern "C" void Interlocked_CompareExchange_m9921 ();
extern "C" void ManualResetEvent__ctor_m4735 ();
extern "C" void Monitor_Enter_m4719 ();
extern "C" void Monitor_Exit_m4721 ();
extern "C" void Monitor_Monitor_pulse_m9922 ();
extern "C" void Monitor_Monitor_test_synchronised_m9923 ();
extern "C" void Monitor_Pulse_m9924 ();
extern "C" void Monitor_Monitor_wait_m9925 ();
extern "C" void Monitor_Wait_m9926 ();
extern "C" void Mutex__ctor_m9927 ();
extern "C" void Mutex_CreateMutex_internal_m9928 ();
extern "C" void Mutex_ReleaseMutex_internal_m9929 ();
extern "C" void Mutex_ReleaseMutex_m9930 ();
extern "C" void NativeEventCalls_CreateEvent_internal_m9931 ();
extern "C" void NativeEventCalls_SetEvent_internal_m9932 ();
extern "C" void NativeEventCalls_ResetEvent_internal_m9933 ();
extern "C" void NativeEventCalls_CloseEvent_internal_m9934 ();
extern "C" void SynchronizationLockException__ctor_m9935 ();
extern "C" void SynchronizationLockException__ctor_m9936 ();
extern "C" void SynchronizationLockException__ctor_m9937 ();
extern "C" void Thread__ctor_m9938 ();
extern "C" void Thread__cctor_m9939 ();
extern "C" void Thread_get_CurrentContext_m9940 ();
extern "C" void Thread_CurrentThread_internal_m9941 ();
extern "C" void Thread_get_CurrentThread_m9942 ();
extern "C" void Thread_FreeLocalSlotValues_m9943 ();
extern "C" void Thread_GetDomainID_m9944 ();
extern "C" void Thread_Thread_internal_m9945 ();
extern "C" void Thread_Thread_init_m9946 ();
extern "C" void Thread_GetCachedCurrentCulture_m9947 ();
extern "C" void Thread_GetSerializedCurrentCulture_m9948 ();
extern "C" void Thread_SetCachedCurrentCulture_m9949 ();
extern "C" void Thread_GetCachedCurrentUICulture_m9950 ();
extern "C" void Thread_GetSerializedCurrentUICulture_m9951 ();
extern "C" void Thread_SetCachedCurrentUICulture_m9952 ();
extern "C" void Thread_get_CurrentCulture_m9953 ();
extern "C" void Thread_get_CurrentUICulture_m9954 ();
extern "C" void Thread_set_IsBackground_m9955 ();
extern "C" void Thread_SetName_internal_m9956 ();
extern "C" void Thread_set_Name_m9957 ();
extern "C" void Thread_Start_m9958 ();
extern "C" void Thread_Thread_free_internal_m9959 ();
extern "C" void Thread_Finalize_m9960 ();
extern "C" void Thread_SetState_m9961 ();
extern "C" void Thread_ClrState_m9962 ();
extern "C" void Thread_GetNewManagedId_m9963 ();
extern "C" void Thread_GetNewManagedId_internal_m9964 ();
extern "C" void Thread_get_ExecutionContext_m9965 ();
extern "C" void Thread_get_ManagedThreadId_m9966 ();
extern "C" void Thread_GetHashCode_m9967 ();
extern "C" void Thread_GetCompressedStack_m9968 ();
extern "C" void ThreadAbortException__ctor_m9969 ();
extern "C" void ThreadAbortException__ctor_m9970 ();
extern "C" void ThreadInterruptedException__ctor_m9971 ();
extern "C" void ThreadInterruptedException__ctor_m9972 ();
extern "C" void ThreadPool_QueueUserWorkItem_m9973 ();
extern "C" void ThreadStateException__ctor_m9974 ();
extern "C" void ThreadStateException__ctor_m9975 ();
extern "C" void TimerComparer__ctor_m9976 ();
extern "C" void TimerComparer_Compare_m9977 ();
extern "C" void Scheduler__ctor_m9978 ();
extern "C" void Scheduler__cctor_m9979 ();
extern "C" void Scheduler_get_Instance_m9980 ();
extern "C" void Scheduler_Remove_m9981 ();
extern "C" void Scheduler_Change_m9982 ();
extern "C" void Scheduler_Add_m9983 ();
extern "C" void Scheduler_InternalRemove_m9984 ();
extern "C" void Scheduler_SchedulerThread_m9985 ();
extern "C" void Scheduler_ShrinkIfNeeded_m9986 ();
extern "C" void Timer__cctor_m9987 ();
extern "C" void Timer_Change_m9988 ();
extern "C" void Timer_Dispose_m9989 ();
extern "C" void Timer_Change_m9990 ();
extern "C" void WaitHandle__ctor_m9991 ();
extern "C" void WaitHandle__cctor_m9992 ();
extern "C" void WaitHandle_System_IDisposable_Dispose_m9993 ();
extern "C" void WaitHandle_get_Handle_m9994 ();
extern "C" void WaitHandle_set_Handle_m9995 ();
extern "C" void WaitHandle_WaitOne_internal_m9996 ();
extern "C" void WaitHandle_Dispose_m9997 ();
extern "C" void WaitHandle_WaitOne_m9998 ();
extern "C" void WaitHandle_WaitOne_m9999 ();
extern "C" void WaitHandle_CheckDisposed_m10000 ();
extern "C" void WaitHandle_Finalize_m10001 ();
extern "C" void AccessViolationException__ctor_m10002 ();
extern "C" void AccessViolationException__ctor_m10003 ();
extern "C" void ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m10004 ();
extern "C" void ActivationContext_Finalize_m10005 ();
extern "C" void ActivationContext_Dispose_m10006 ();
extern "C" void ActivationContext_Dispose_m10007 ();
extern "C" void Activator_CreateInstance_m10008 ();
extern "C" void Activator_CreateInstance_m10009 ();
extern "C" void Activator_CreateInstance_m10010 ();
extern "C" void Activator_CreateInstance_m10011 ();
extern "C" void Activator_CreateInstance_m5687 ();
extern "C" void Activator_CheckType_m10012 ();
extern "C" void Activator_CheckAbstractType_m10013 ();
extern "C" void Activator_CreateInstanceInternal_m10014 ();
extern "C" void AppDomain_add_UnhandledException_m3581 ();
extern "C" void AppDomain_remove_UnhandledException_m10015 ();
extern "C" void AppDomain_getFriendlyName_m10016 ();
extern "C" void AppDomain_getCurDomain_m10017 ();
extern "C" void AppDomain_get_CurrentDomain_m3579 ();
extern "C" void AppDomain_LoadAssembly_m10018 ();
extern "C" void AppDomain_Load_m10019 ();
extern "C" void AppDomain_Load_m10020 ();
extern "C" void AppDomain_InternalSetContext_m10021 ();
extern "C" void AppDomain_InternalGetContext_m10022 ();
extern "C" void AppDomain_InternalGetDefaultContext_m10023 ();
extern "C" void AppDomain_InternalGetProcessGuid_m10024 ();
extern "C" void AppDomain_GetProcessGuid_m10025 ();
extern "C" void AppDomain_ToString_m10026 ();
extern "C" void AppDomain_DoTypeResolve_m10027 ();
extern "C" void AppDomainSetup__ctor_m10028 ();
extern "C" void ApplicationException__ctor_m10029 ();
extern "C" void ApplicationException__ctor_m10030 ();
extern "C" void ApplicationException__ctor_m10031 ();
extern "C" void ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m10032 ();
extern "C" void ApplicationIdentity_ToString_m10033 ();
extern "C" void ArgumentException__ctor_m10034 ();
extern "C" void ArgumentException__ctor_m2361 ();
extern "C" void ArgumentException__ctor_m5706 ();
extern "C" void ArgumentException__ctor_m3732 ();
extern "C" void ArgumentException__ctor_m10035 ();
extern "C" void ArgumentException__ctor_m10036 ();
extern "C" void ArgumentException_get_ParamName_m10037 ();
extern "C" void ArgumentException_get_Message_m10038 ();
extern "C" void ArgumentException_GetObjectData_m10039 ();
extern "C" void ArgumentNullException__ctor_m10040 ();
extern "C" void ArgumentNullException__ctor_m222 ();
extern "C" void ArgumentNullException__ctor_m5653 ();
extern "C" void ArgumentNullException__ctor_m10041 ();
extern "C" void ArgumentOutOfRangeException__ctor_m5710 ();
extern "C" void ArgumentOutOfRangeException__ctor_m4633 ();
extern "C" void ArgumentOutOfRangeException__ctor_m3731 ();
extern "C" void ArgumentOutOfRangeException__ctor_m10042 ();
extern "C" void ArgumentOutOfRangeException__ctor_m10043 ();
extern "C" void ArgumentOutOfRangeException_get_Message_m10044 ();
extern "C" void ArgumentOutOfRangeException_GetObjectData_m10045 ();
extern "C" void ArithmeticException__ctor_m10046 ();
extern "C" void ArithmeticException__ctor_m4632 ();
extern "C" void ArithmeticException__ctor_m10047 ();
extern "C" void ArrayTypeMismatchException__ctor_m10048 ();
extern "C" void ArrayTypeMismatchException__ctor_m10049 ();
extern "C" void ArrayTypeMismatchException__ctor_m10050 ();
extern "C" void BitConverter__cctor_m10051 ();
extern "C" void BitConverter_AmILittleEndian_m10052 ();
extern "C" void BitConverter_DoubleWordsAreSwapped_m10053 ();
extern "C" void BitConverter_DoubleToInt64Bits_m10054 ();
extern "C" void BitConverter_GetBytes_m10055 ();
extern "C" void BitConverter_GetBytes_m10056 ();
extern "C" void BitConverter_PutBytes_m10057 ();
extern "C" void BitConverter_ToInt64_m10058 ();
extern "C" void BitConverter_ToString_m4720 ();
extern "C" void BitConverter_ToString_m10059 ();
extern "C" void Buffer_ByteLength_m10060 ();
extern "C" void Buffer_BlockCopy_m3727 ();
extern "C" void Buffer_ByteLengthInternal_m10061 ();
extern "C" void Buffer_BlockCopyInternal_m10062 ();
extern "C" void CharEnumerator__ctor_m10063 ();
extern "C" void CharEnumerator_System_Collections_IEnumerator_get_Current_m10064 ();
extern "C" void CharEnumerator_System_IDisposable_Dispose_m10065 ();
extern "C" void CharEnumerator_get_Current_m10066 ();
extern "C" void CharEnumerator_Clone_m10067 ();
extern "C" void CharEnumerator_MoveNext_m10068 ();
extern "C" void CharEnumerator_Reset_m10069 ();
extern "C" void Console__cctor_m10070 ();
extern "C" void Console_SetEncodings_m10071 ();
extern "C" void Console_get_Error_m5724 ();
extern "C" void Console_Open_m10072 ();
extern "C" void Console_OpenStandardError_m10073 ();
extern "C" void Console_OpenStandardInput_m10074 ();
extern "C" void Console_OpenStandardOutput_m10075 ();
extern "C" void ContextBoundObject__ctor_m10076 ();
extern "C" void Convert__cctor_m10077 ();
extern "C" void Convert_InternalFromBase64String_m10078 ();
extern "C" void Convert_FromBase64String_m4689 ();
extern "C" void Convert_ToBase64String_m4663 ();
extern "C" void Convert_ToBase64String_m10079 ();
extern "C" void Convert_ToBoolean_m10080 ();
extern "C" void Convert_ToBoolean_m10081 ();
extern "C" void Convert_ToBoolean_m10082 ();
extern "C" void Convert_ToBoolean_m10083 ();
extern "C" void Convert_ToBoolean_m10084 ();
extern "C" void Convert_ToBoolean_m10085 ();
extern "C" void Convert_ToBoolean_m10086 ();
extern "C" void Convert_ToBoolean_m10087 ();
extern "C" void Convert_ToBoolean_m10088 ();
extern "C" void Convert_ToBoolean_m10089 ();
extern "C" void Convert_ToBoolean_m10090 ();
extern "C" void Convert_ToBoolean_m10091 ();
extern "C" void Convert_ToBoolean_m10092 ();
extern "C" void Convert_ToBoolean_m10093 ();
extern "C" void Convert_ToByte_m10094 ();
extern "C" void Convert_ToByte_m10095 ();
extern "C" void Convert_ToByte_m10096 ();
extern "C" void Convert_ToByte_m10097 ();
extern "C" void Convert_ToByte_m10098 ();
extern "C" void Convert_ToByte_m10099 ();
extern "C" void Convert_ToByte_m10100 ();
extern "C" void Convert_ToByte_m10101 ();
extern "C" void Convert_ToByte_m10102 ();
extern "C" void Convert_ToByte_m10103 ();
extern "C" void Convert_ToByte_m10104 ();
extern "C" void Convert_ToByte_m10105 ();
extern "C" void Convert_ToByte_m10106 ();
extern "C" void Convert_ToByte_m10107 ();
extern "C" void Convert_ToByte_m10108 ();
extern "C" void Convert_ToChar_m4694 ();
extern "C" void Convert_ToChar_m10109 ();
extern "C" void Convert_ToChar_m10110 ();
extern "C" void Convert_ToChar_m10111 ();
extern "C" void Convert_ToChar_m10112 ();
extern "C" void Convert_ToChar_m10113 ();
extern "C" void Convert_ToChar_m10114 ();
extern "C" void Convert_ToChar_m10115 ();
extern "C" void Convert_ToChar_m10116 ();
extern "C" void Convert_ToChar_m10117 ();
extern "C" void Convert_ToChar_m10118 ();
extern "C" void Convert_ToDateTime_m10119 ();
extern "C" void Convert_ToDateTime_m10120 ();
extern "C" void Convert_ToDateTime_m10121 ();
extern "C" void Convert_ToDateTime_m10122 ();
extern "C" void Convert_ToDateTime_m10123 ();
extern "C" void Convert_ToDateTime_m10124 ();
extern "C" void Convert_ToDateTime_m10125 ();
extern "C" void Convert_ToDateTime_m10126 ();
extern "C" void Convert_ToDateTime_m10127 ();
extern "C" void Convert_ToDateTime_m10128 ();
extern "C" void Convert_ToDecimal_m10129 ();
extern "C" void Convert_ToDecimal_m10130 ();
extern "C" void Convert_ToDecimal_m10131 ();
extern "C" void Convert_ToDecimal_m10132 ();
extern "C" void Convert_ToDecimal_m10133 ();
extern "C" void Convert_ToDecimal_m10134 ();
extern "C" void Convert_ToDecimal_m10135 ();
extern "C" void Convert_ToDecimal_m10136 ();
extern "C" void Convert_ToDecimal_m10137 ();
extern "C" void Convert_ToDecimal_m10138 ();
extern "C" void Convert_ToDecimal_m10139 ();
extern "C" void Convert_ToDecimal_m10140 ();
extern "C" void Convert_ToDecimal_m10141 ();
extern "C" void Convert_ToDouble_m10142 ();
extern "C" void Convert_ToDouble_m10143 ();
extern "C" void Convert_ToDouble_m10144 ();
extern "C" void Convert_ToDouble_m10145 ();
extern "C" void Convert_ToDouble_m10146 ();
extern "C" void Convert_ToDouble_m10147 ();
extern "C" void Convert_ToDouble_m10148 ();
extern "C" void Convert_ToDouble_m10149 ();
extern "C" void Convert_ToDouble_m10150 ();
extern "C" void Convert_ToDouble_m10151 ();
extern "C" void Convert_ToDouble_m10152 ();
extern "C" void Convert_ToDouble_m10153 ();
extern "C" void Convert_ToDouble_m10154 ();
extern "C" void Convert_ToDouble_m10155 ();
extern "C" void Convert_ToInt16_m10156 ();
extern "C" void Convert_ToInt16_m10157 ();
extern "C" void Convert_ToInt16_m10158 ();
extern "C" void Convert_ToInt16_m10159 ();
extern "C" void Convert_ToInt16_m10160 ();
extern "C" void Convert_ToInt16_m10161 ();
extern "C" void Convert_ToInt16_m10162 ();
extern "C" void Convert_ToInt16_m10163 ();
extern "C" void Convert_ToInt16_m10164 ();
extern "C" void Convert_ToInt16_m10165 ();
extern "C" void Convert_ToInt16_m4648 ();
extern "C" void Convert_ToInt16_m10166 ();
extern "C" void Convert_ToInt16_m10167 ();
extern "C" void Convert_ToInt16_m10168 ();
extern "C" void Convert_ToInt16_m10169 ();
extern "C" void Convert_ToInt16_m10170 ();
extern "C" void Convert_ToInt32_m10171 ();
extern "C" void Convert_ToInt32_m10172 ();
extern "C" void Convert_ToInt32_m10173 ();
extern "C" void Convert_ToInt32_m10174 ();
extern "C" void Convert_ToInt32_m10175 ();
extern "C" void Convert_ToInt32_m10176 ();
extern "C" void Convert_ToInt32_m10177 ();
extern "C" void Convert_ToInt32_m10178 ();
extern "C" void Convert_ToInt32_m10179 ();
extern "C" void Convert_ToInt32_m10180 ();
extern "C" void Convert_ToInt32_m10181 ();
extern "C" void Convert_ToInt32_m10182 ();
extern "C" void Convert_ToInt32_m10183 ();
extern "C" void Convert_ToInt32_m10184 ();
extern "C" void Convert_ToInt32_m4703 ();
extern "C" void Convert_ToInt64_m10185 ();
extern "C" void Convert_ToInt64_m10186 ();
extern "C" void Convert_ToInt64_m10187 ();
extern "C" void Convert_ToInt64_m10188 ();
extern "C" void Convert_ToInt64_m10189 ();
extern "C" void Convert_ToInt64_m10190 ();
extern "C" void Convert_ToInt64_m10191 ();
extern "C" void Convert_ToInt64_m10192 ();
extern "C" void Convert_ToInt64_m10193 ();
extern "C" void Convert_ToInt64_m10194 ();
extern "C" void Convert_ToInt64_m10195 ();
extern "C" void Convert_ToInt64_m10196 ();
extern "C" void Convert_ToInt64_m10197 ();
extern "C" void Convert_ToInt64_m10198 ();
extern "C" void Convert_ToInt64_m10199 ();
extern "C" void Convert_ToInt64_m10200 ();
extern "C" void Convert_ToInt64_m10201 ();
extern "C" void Convert_ToSByte_m10202 ();
extern "C" void Convert_ToSByte_m10203 ();
extern "C" void Convert_ToSByte_m10204 ();
extern "C" void Convert_ToSByte_m10205 ();
extern "C" void Convert_ToSByte_m10206 ();
extern "C" void Convert_ToSByte_m10207 ();
extern "C" void Convert_ToSByte_m10208 ();
extern "C" void Convert_ToSByte_m10209 ();
extern "C" void Convert_ToSByte_m10210 ();
extern "C" void Convert_ToSByte_m10211 ();
extern "C" void Convert_ToSByte_m10212 ();
extern "C" void Convert_ToSByte_m10213 ();
extern "C" void Convert_ToSByte_m10214 ();
extern "C" void Convert_ToSByte_m10215 ();
extern "C" void Convert_ToSingle_m10216 ();
extern "C" void Convert_ToSingle_m10217 ();
extern "C" void Convert_ToSingle_m10218 ();
extern "C" void Convert_ToSingle_m10219 ();
extern "C" void Convert_ToSingle_m10220 ();
extern "C" void Convert_ToSingle_m10221 ();
extern "C" void Convert_ToSingle_m10222 ();
extern "C" void Convert_ToSingle_m10223 ();
extern "C" void Convert_ToSingle_m10224 ();
extern "C" void Convert_ToSingle_m10225 ();
extern "C" void Convert_ToSingle_m10226 ();
extern "C" void Convert_ToSingle_m10227 ();
extern "C" void Convert_ToSingle_m10228 ();
extern "C" void Convert_ToSingle_m10229 ();
extern "C" void Convert_ToString_m10230 ();
extern "C" void Convert_ToString_m10231 ();
extern "C" void Convert_ToUInt16_m10232 ();
extern "C" void Convert_ToUInt16_m10233 ();
extern "C" void Convert_ToUInt16_m10234 ();
extern "C" void Convert_ToUInt16_m10235 ();
extern "C" void Convert_ToUInt16_m10236 ();
extern "C" void Convert_ToUInt16_m10237 ();
extern "C" void Convert_ToUInt16_m10238 ();
extern "C" void Convert_ToUInt16_m10239 ();
extern "C" void Convert_ToUInt16_m10240 ();
extern "C" void Convert_ToUInt16_m10241 ();
extern "C" void Convert_ToUInt16_m10242 ();
extern "C" void Convert_ToUInt16_m10243 ();
extern "C" void Convert_ToUInt16_m10244 ();
extern "C" void Convert_ToUInt16_m10245 ();
extern "C" void Convert_ToUInt32_m3598 ();
extern "C" void Convert_ToUInt32_m10246 ();
extern "C" void Convert_ToUInt32_m10247 ();
extern "C" void Convert_ToUInt32_m10248 ();
extern "C" void Convert_ToUInt32_m10249 ();
extern "C" void Convert_ToUInt32_m10250 ();
extern "C" void Convert_ToUInt32_m10251 ();
extern "C" void Convert_ToUInt32_m10252 ();
extern "C" void Convert_ToUInt32_m10253 ();
extern "C" void Convert_ToUInt32_m10254 ();
extern "C" void Convert_ToUInt32_m10255 ();
extern "C" void Convert_ToUInt32_m10256 ();
extern "C" void Convert_ToUInt32_m10257 ();
extern "C" void Convert_ToUInt32_m3597 ();
extern "C" void Convert_ToUInt32_m10258 ();
extern "C" void Convert_ToUInt64_m10259 ();
extern "C" void Convert_ToUInt64_m10260 ();
extern "C" void Convert_ToUInt64_m10261 ();
extern "C" void Convert_ToUInt64_m10262 ();
extern "C" void Convert_ToUInt64_m10263 ();
extern "C" void Convert_ToUInt64_m10264 ();
extern "C" void Convert_ToUInt64_m10265 ();
extern "C" void Convert_ToUInt64_m10266 ();
extern "C" void Convert_ToUInt64_m10267 ();
extern "C" void Convert_ToUInt64_m10268 ();
extern "C" void Convert_ToUInt64_m10269 ();
extern "C" void Convert_ToUInt64_m10270 ();
extern "C" void Convert_ToUInt64_m10271 ();
extern "C" void Convert_ToUInt64_m10272 ();
extern "C" void Convert_ToUInt64_m10273 ();
extern "C" void Convert_ChangeType_m10274 ();
extern "C" void Convert_ToType_m10275 ();
extern "C" void DBNull__ctor_m10276 ();
extern "C" void DBNull__ctor_m10277 ();
extern "C" void DBNull__cctor_m10278 ();
extern "C" void DBNull_System_IConvertible_ToBoolean_m10279 ();
extern "C" void DBNull_System_IConvertible_ToByte_m10280 ();
extern "C" void DBNull_System_IConvertible_ToChar_m10281 ();
extern "C" void DBNull_System_IConvertible_ToDateTime_m10282 ();
extern "C" void DBNull_System_IConvertible_ToDecimal_m10283 ();
extern "C" void DBNull_System_IConvertible_ToDouble_m10284 ();
extern "C" void DBNull_System_IConvertible_ToInt16_m10285 ();
extern "C" void DBNull_System_IConvertible_ToInt32_m10286 ();
extern "C" void DBNull_System_IConvertible_ToInt64_m10287 ();
extern "C" void DBNull_System_IConvertible_ToSByte_m10288 ();
extern "C" void DBNull_System_IConvertible_ToSingle_m10289 ();
extern "C" void DBNull_System_IConvertible_ToType_m10290 ();
extern "C" void DBNull_System_IConvertible_ToUInt16_m10291 ();
extern "C" void DBNull_System_IConvertible_ToUInt32_m10292 ();
extern "C" void DBNull_System_IConvertible_ToUInt64_m10293 ();
extern "C" void DBNull_GetObjectData_m10294 ();
extern "C" void DBNull_ToString_m10295 ();
extern "C" void DBNull_ToString_m10296 ();
extern "C" void DateTime__ctor_m10297 ();
extern "C" void DateTime__ctor_m10298 ();
extern "C" void DateTime__ctor_m3633 ();
extern "C" void DateTime__ctor_m10299 ();
extern "C" void DateTime__ctor_m10300 ();
extern "C" void DateTime__cctor_m10301 ();
extern "C" void DateTime_System_IConvertible_ToBoolean_m10302 ();
extern "C" void DateTime_System_IConvertible_ToByte_m10303 ();
extern "C" void DateTime_System_IConvertible_ToChar_m10304 ();
extern "C" void DateTime_System_IConvertible_ToDateTime_m10305 ();
extern "C" void DateTime_System_IConvertible_ToDecimal_m10306 ();
extern "C" void DateTime_System_IConvertible_ToDouble_m10307 ();
extern "C" void DateTime_System_IConvertible_ToInt16_m10308 ();
extern "C" void DateTime_System_IConvertible_ToInt32_m10309 ();
extern "C" void DateTime_System_IConvertible_ToInt64_m10310 ();
extern "C" void DateTime_System_IConvertible_ToSByte_m10311 ();
extern "C" void DateTime_System_IConvertible_ToSingle_m10312 ();
extern "C" void DateTime_System_IConvertible_ToType_m10313 ();
extern "C" void DateTime_System_IConvertible_ToUInt16_m10314 ();
extern "C" void DateTime_System_IConvertible_ToUInt32_m10315 ();
extern "C" void DateTime_System_IConvertible_ToUInt64_m10316 ();
extern "C" void DateTime_AbsoluteDays_m10317 ();
extern "C" void DateTime_FromTicks_m10318 ();
extern "C" void DateTime_get_Month_m10319 ();
extern "C" void DateTime_get_Day_m10320 ();
extern "C" void DateTime_get_DayOfWeek_m10321 ();
extern "C" void DateTime_get_Hour_m10322 ();
extern "C" void DateTime_get_Minute_m10323 ();
extern "C" void DateTime_get_Second_m10324 ();
extern "C" void DateTime_GetTimeMonotonic_m10325 ();
extern "C" void DateTime_GetNow_m10326 ();
extern "C" void DateTime_get_Now_m3618 ();
extern "C" void DateTime_get_Ticks_m4722 ();
extern "C" void DateTime_get_Today_m10327 ();
extern "C" void DateTime_get_UtcNow_m4683 ();
extern "C" void DateTime_get_Year_m10328 ();
extern "C" void DateTime_get_Kind_m10329 ();
extern "C" void DateTime_Add_m10330 ();
extern "C" void DateTime_AddTicks_m10331 ();
extern "C" void DateTime_AddMilliseconds_m5679 ();
extern "C" void DateTime_AddSeconds_m3634 ();
extern "C" void DateTime_Compare_m10332 ();
extern "C" void DateTime_CompareTo_m10333 ();
extern "C" void DateTime_CompareTo_m10334 ();
extern "C" void DateTime_Equals_m10335 ();
extern "C" void DateTime_FromBinary_m10336 ();
extern "C" void DateTime_SpecifyKind_m10337 ();
extern "C" void DateTime_DaysInMonth_m10338 ();
extern "C" void DateTime_Equals_m10339 ();
extern "C" void DateTime_CheckDateTimeKind_m10340 ();
extern "C" void DateTime_GetHashCode_m10341 ();
extern "C" void DateTime_IsLeapYear_m10342 ();
extern "C" void DateTime_Parse_m10343 ();
extern "C" void DateTime_Parse_m10344 ();
extern "C" void DateTime_CoreParse_m10345 ();
extern "C" void DateTime_YearMonthDayFormats_m10346 ();
extern "C" void DateTime__ParseNumber_m10347 ();
extern "C" void DateTime__ParseEnum_m10348 ();
extern "C" void DateTime__ParseString_m10349 ();
extern "C" void DateTime__ParseAmPm_m10350 ();
extern "C" void DateTime__ParseTimeSeparator_m10351 ();
extern "C" void DateTime__ParseDateSeparator_m10352 ();
extern "C" void DateTime_IsLetter_m10353 ();
extern "C" void DateTime__DoParse_m10354 ();
extern "C" void DateTime_ParseExact_m4649 ();
extern "C" void DateTime_ParseExact_m10355 ();
extern "C" void DateTime_CheckStyle_m10356 ();
extern "C" void DateTime_ParseExact_m10357 ();
extern "C" void DateTime_Subtract_m10358 ();
extern "C" void DateTime_ToString_m10359 ();
extern "C" void DateTime_ToString_m10360 ();
extern "C" void DateTime_ToString_m10361 ();
extern "C" void DateTime_ToLocalTime_m5696 ();
extern "C" void DateTime_ToUniversalTime_m10362 ();
extern "C" void DateTime_op_Addition_m10363 ();
extern "C" void DateTime_op_Equality_m10364 ();
extern "C" void DateTime_op_GreaterThan_m4684 ();
extern "C" void DateTime_op_GreaterThanOrEqual_m5680 ();
extern "C" void DateTime_op_Inequality_m10365 ();
extern "C" void DateTime_op_LessThan_m5705 ();
extern "C" void DateTime_op_LessThanOrEqual_m4685 ();
extern "C" void DateTime_op_Subtraction_m10366 ();
extern "C" void DateTimeOffset__ctor_m10367 ();
extern "C" void DateTimeOffset__ctor_m10368 ();
extern "C" void DateTimeOffset__ctor_m10369 ();
extern "C" void DateTimeOffset__ctor_m10370 ();
extern "C" void DateTimeOffset__cctor_m10371 ();
extern "C" void DateTimeOffset_System_IComparable_CompareTo_m10372 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m10373 ();
extern "C" void DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10374 ();
extern "C" void DateTimeOffset_CompareTo_m10375 ();
extern "C" void DateTimeOffset_Equals_m10376 ();
extern "C" void DateTimeOffset_Equals_m10377 ();
extern "C" void DateTimeOffset_GetHashCode_m10378 ();
extern "C" void DateTimeOffset_ToString_m10379 ();
extern "C" void DateTimeOffset_ToString_m10380 ();
extern "C" void DateTimeOffset_get_DateTime_m10381 ();
extern "C" void DateTimeOffset_get_Offset_m10382 ();
extern "C" void DateTimeOffset_get_UtcDateTime_m10383 ();
extern "C" void DateTimeUtils_CountRepeat_m10384 ();
extern "C" void DateTimeUtils_ZeroPad_m10385 ();
extern "C" void DateTimeUtils_ParseQuotedString_m10386 ();
extern "C" void DateTimeUtils_GetStandardPattern_m10387 ();
extern "C" void DateTimeUtils_GetStandardPattern_m10388 ();
extern "C" void DateTimeUtils_ToString_m10389 ();
extern "C" void DateTimeUtils_ToString_m10390 ();
extern "C" void DelegateEntry__ctor_m10391 ();
extern "C" void DelegateEntry_DeserializeDelegate_m10392 ();
extern "C" void DelegateSerializationHolder__ctor_m10393 ();
extern "C" void DelegateSerializationHolder_GetDelegateData_m10394 ();
extern "C" void DelegateSerializationHolder_GetObjectData_m10395 ();
extern "C" void DelegateSerializationHolder_GetRealObject_m10396 ();
extern "C" void DivideByZeroException__ctor_m10397 ();
extern "C" void DivideByZeroException__ctor_m10398 ();
extern "C" void DllNotFoundException__ctor_m10399 ();
extern "C" void DllNotFoundException__ctor_m10400 ();
extern "C" void EntryPointNotFoundException__ctor_m10401 ();
extern "C" void EntryPointNotFoundException__ctor_m10402 ();
extern "C" void SByteComparer__ctor_m10403 ();
extern "C" void SByteComparer_Compare_m10404 ();
extern "C" void SByteComparer_Compare_m10405 ();
extern "C" void ShortComparer__ctor_m10406 ();
extern "C" void ShortComparer_Compare_m10407 ();
extern "C" void ShortComparer_Compare_m10408 ();
extern "C" void IntComparer__ctor_m10409 ();
extern "C" void IntComparer_Compare_m10410 ();
extern "C" void IntComparer_Compare_m10411 ();
extern "C" void LongComparer__ctor_m10412 ();
extern "C" void LongComparer_Compare_m10413 ();
extern "C" void LongComparer_Compare_m10414 ();
extern "C" void MonoEnumInfo__ctor_m10415 ();
extern "C" void MonoEnumInfo__cctor_m10416 ();
extern "C" void MonoEnumInfo_get_enum_info_m10417 ();
extern "C" void MonoEnumInfo_get_Cache_m10418 ();
extern "C" void MonoEnumInfo_GetInfo_m10419 ();
extern "C" void Environment_get_SocketSecurityEnabled_m10420 ();
extern "C" void Environment_get_NewLine_m4637 ();
extern "C" void Environment_get_Platform_m10421 ();
extern "C" void Environment_GetOSVersionString_m10422 ();
extern "C" void Environment_get_OSVersion_m10423 ();
extern "C" void Environment_internalGetEnvironmentVariable_m10424 ();
extern "C" void Environment_GetEnvironmentVariable_m4716 ();
extern "C" void Environment_GetWindowsFolderPath_m10425 ();
extern "C" void Environment_GetFolderPath_m4700 ();
extern "C" void Environment_ReadXdgUserDir_m10426 ();
extern "C" void Environment_InternalGetFolderPath_m10427 ();
extern "C" void Environment_get_IsRunningOnWindows_m10428 ();
extern "C" void Environment_GetMachineConfigPath_m10429 ();
extern "C" void Environment_internalGetHome_m10430 ();
extern "C" void EventArgs__ctor_m10431 ();
extern "C" void EventArgs__cctor_m10432 ();
extern "C" void ExecutionEngineException__ctor_m10433 ();
extern "C" void ExecutionEngineException__ctor_m10434 ();
extern "C" void FieldAccessException__ctor_m10435 ();
extern "C" void FieldAccessException__ctor_m10436 ();
extern "C" void FieldAccessException__ctor_m10437 ();
extern "C" void FlagsAttribute__ctor_m10438 ();
extern "C" void FormatException__ctor_m10439 ();
extern "C" void FormatException__ctor_m4643 ();
extern "C" void FormatException__ctor_m5734 ();
extern "C" void GC_SuppressFinalize_m3728 ();
extern "C" void Guid__ctor_m10440 ();
extern "C" void Guid__ctor_m10441 ();
extern "C" void Guid__cctor_m10442 ();
extern "C" void Guid_CheckNull_m10443 ();
extern "C" void Guid_CheckLength_m10444 ();
extern "C" void Guid_CheckArray_m10445 ();
extern "C" void Guid_Compare_m10446 ();
extern "C" void Guid_CompareTo_m10447 ();
extern "C" void Guid_Equals_m10448 ();
extern "C" void Guid_CompareTo_m10449 ();
extern "C" void Guid_Equals_m10450 ();
extern "C" void Guid_GetHashCode_m10451 ();
extern "C" void Guid_ToHex_m10452 ();
extern "C" void Guid_NewGuid_m10453 ();
extern "C" void Guid_AppendInt_m10454 ();
extern "C" void Guid_AppendShort_m10455 ();
extern "C" void Guid_AppendByte_m10456 ();
extern "C" void Guid_BaseToString_m10457 ();
extern "C" void Guid_ToString_m10458 ();
extern "C" void Guid_ToString_m10459 ();
extern "C" void Guid_ToString_m10460 ();
extern "C" void IndexOutOfRangeException__ctor_m10461 ();
extern "C" void IndexOutOfRangeException__ctor_m3599 ();
extern "C" void IndexOutOfRangeException__ctor_m10462 ();
extern "C" void InvalidCastException__ctor_m10463 ();
extern "C" void InvalidCastException__ctor_m10464 ();
extern "C" void InvalidCastException__ctor_m10465 ();
extern "C" void InvalidOperationException__ctor_m5656 ();
extern "C" void InvalidOperationException__ctor_m4713 ();
extern "C" void InvalidOperationException__ctor_m10466 ();
extern "C" void InvalidOperationException__ctor_m10467 ();
extern "C" void LocalDataStoreSlot__ctor_m10468 ();
extern "C" void LocalDataStoreSlot__cctor_m10469 ();
extern "C" void LocalDataStoreSlot_Finalize_m10470 ();
extern "C" void Math_Abs_m10471 ();
extern "C" void Math_Abs_m10472 ();
extern "C" void Math_Abs_m10473 ();
extern "C" void Math_Ceiling_m10474 ();
extern "C" void Math_Floor_m10475 ();
extern "C" void Math_Log_m3601 ();
extern "C" void Math_Max_m3614 ();
extern "C" void Math_Min_m3726 ();
extern "C" void Math_Round_m10476 ();
extern "C" void Math_Round_m10477 ();
extern "C" void Math_Sin_m10478 ();
extern "C" void Math_Cos_m10479 ();
extern "C" void Math_Acos_m10480 ();
extern "C" void Math_Atan2_m10481 ();
extern "C" void Math_Log_m10482 ();
extern "C" void Math_Pow_m10483 ();
extern "C" void Math_Sqrt_m10484 ();
extern "C" void MemberAccessException__ctor_m10485 ();
extern "C" void MemberAccessException__ctor_m10486 ();
extern "C" void MemberAccessException__ctor_m10487 ();
extern "C" void MethodAccessException__ctor_m10488 ();
extern "C" void MethodAccessException__ctor_m10489 ();
extern "C" void MissingFieldException__ctor_m10490 ();
extern "C" void MissingFieldException__ctor_m10491 ();
extern "C" void MissingFieldException__ctor_m10492 ();
extern "C" void MissingFieldException_get_Message_m10493 ();
extern "C" void MissingMemberException__ctor_m10494 ();
extern "C" void MissingMemberException__ctor_m10495 ();
extern "C" void MissingMemberException__ctor_m10496 ();
extern "C" void MissingMemberException__ctor_m10497 ();
extern "C" void MissingMemberException_GetObjectData_m10498 ();
extern "C" void MissingMemberException_get_Message_m10499 ();
extern "C" void MissingMethodException__ctor_m10500 ();
extern "C" void MissingMethodException__ctor_m10501 ();
extern "C" void MissingMethodException__ctor_m10502 ();
extern "C" void MissingMethodException__ctor_m10503 ();
extern "C" void MissingMethodException_get_Message_m10504 ();
extern "C" void MonoAsyncCall__ctor_m10505 ();
extern "C" void AttributeInfo__ctor_m10506 ();
extern "C" void AttributeInfo_get_Usage_m10507 ();
extern "C" void AttributeInfo_get_InheritanceLevel_m10508 ();
extern "C" void MonoCustomAttrs__cctor_m10509 ();
extern "C" void MonoCustomAttrs_IsUserCattrProvider_m10510 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesInternal_m10511 ();
extern "C" void MonoCustomAttrs_GetPseudoCustomAttributes_m10512 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesBase_m10513 ();
extern "C" void MonoCustomAttrs_GetCustomAttribute_m10514 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m10515 ();
extern "C" void MonoCustomAttrs_GetCustomAttributes_m10516 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesDataInternal_m10517 ();
extern "C" void MonoCustomAttrs_GetCustomAttributesData_m10518 ();
extern "C" void MonoCustomAttrs_IsDefined_m10519 ();
extern "C" void MonoCustomAttrs_IsDefinedInternal_m10520 ();
extern "C" void MonoCustomAttrs_GetBasePropertyDefinition_m10521 ();
extern "C" void MonoCustomAttrs_GetBase_m10522 ();
extern "C" void MonoCustomAttrs_RetrieveAttributeUsage_m10523 ();
extern "C" void MonoTouchAOTHelper__cctor_m10524 ();
extern "C" void MonoTypeInfo__ctor_m10525 ();
extern "C" void MonoType_get_attributes_m10526 ();
extern "C" void MonoType_GetDefaultConstructor_m10527 ();
extern "C" void MonoType_GetAttributeFlagsImpl_m10528 ();
extern "C" void MonoType_GetConstructorImpl_m10529 ();
extern "C" void MonoType_GetConstructors_internal_m10530 ();
extern "C" void MonoType_GetConstructors_m10531 ();
extern "C" void MonoType_InternalGetEvent_m10532 ();
extern "C" void MonoType_GetEvent_m10533 ();
extern "C" void MonoType_GetField_m10534 ();
extern "C" void MonoType_GetFields_internal_m10535 ();
extern "C" void MonoType_GetFields_m10536 ();
extern "C" void MonoType_GetInterfaces_m10537 ();
extern "C" void MonoType_GetMethodsByName_m10538 ();
extern "C" void MonoType_GetMethods_m10539 ();
extern "C" void MonoType_GetMethodImpl_m10540 ();
extern "C" void MonoType_GetPropertiesByName_m10541 ();
extern "C" void MonoType_GetPropertyImpl_m10542 ();
extern "C" void MonoType_HasElementTypeImpl_m10543 ();
extern "C" void MonoType_IsArrayImpl_m10544 ();
extern "C" void MonoType_IsByRefImpl_m10545 ();
extern "C" void MonoType_IsPointerImpl_m10546 ();
extern "C" void MonoType_IsPrimitiveImpl_m10547 ();
extern "C" void MonoType_IsSubclassOf_m10548 ();
extern "C" void MonoType_InvokeMember_m10549 ();
extern "C" void MonoType_GetElementType_m10550 ();
extern "C" void MonoType_get_UnderlyingSystemType_m10551 ();
extern "C" void MonoType_get_Assembly_m10552 ();
extern "C" void MonoType_get_AssemblyQualifiedName_m10553 ();
extern "C" void MonoType_getFullName_m10554 ();
extern "C" void MonoType_get_BaseType_m10555 ();
extern "C" void MonoType_get_FullName_m10556 ();
extern "C" void MonoType_IsDefined_m10557 ();
extern "C" void MonoType_GetCustomAttributes_m10558 ();
extern "C" void MonoType_GetCustomAttributes_m10559 ();
extern "C" void MonoType_get_MemberType_m10560 ();
extern "C" void MonoType_get_Name_m10561 ();
extern "C" void MonoType_get_Namespace_m10562 ();
extern "C" void MonoType_get_Module_m10563 ();
extern "C" void MonoType_get_DeclaringType_m10564 ();
extern "C" void MonoType_get_ReflectedType_m10565 ();
extern "C" void MonoType_get_TypeHandle_m10566 ();
extern "C" void MonoType_GetObjectData_m10567 ();
extern "C" void MonoType_ToString_m10568 ();
extern "C" void MonoType_GetGenericArguments_m10569 ();
extern "C" void MonoType_get_ContainsGenericParameters_m10570 ();
extern "C" void MonoType_get_IsGenericParameter_m10571 ();
extern "C" void MonoType_GetGenericTypeDefinition_m10572 ();
extern "C" void MonoType_CheckMethodSecurity_m10573 ();
extern "C" void MonoType_ReorderParamArrayArguments_m10574 ();
extern "C" void MulticastNotSupportedException__ctor_m10575 ();
extern "C" void MulticastNotSupportedException__ctor_m10576 ();
extern "C" void MulticastNotSupportedException__ctor_m10577 ();
extern "C" void NonSerializedAttribute__ctor_m10578 ();
extern "C" void NotImplementedException__ctor_m10579 ();
extern "C" void NotImplementedException__ctor_m3730 ();
extern "C" void NotImplementedException__ctor_m10580 ();
extern "C" void NotSupportedException__ctor_m333 ();
extern "C" void NotSupportedException__ctor_m4634 ();
extern "C" void NotSupportedException__ctor_m10581 ();
extern "C" void NullReferenceException__ctor_m10582 ();
extern "C" void NullReferenceException__ctor_m3577 ();
extern "C" void NullReferenceException__ctor_m10583 ();
extern "C" void CustomInfo__ctor_m10584 ();
extern "C" void CustomInfo_GetActiveSection_m10585 ();
extern "C" void CustomInfo_Parse_m10586 ();
extern "C" void CustomInfo_Format_m10587 ();
extern "C" void NumberFormatter__ctor_m10588 ();
extern "C" void NumberFormatter__cctor_m10589 ();
extern "C" void NumberFormatter_GetFormatterTables_m10590 ();
extern "C" void NumberFormatter_GetTenPowerOf_m10591 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10592 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10593 ();
extern "C" void NumberFormatter_InitDecHexDigits_m10594 ();
extern "C" void NumberFormatter_FastToDecHex_m10595 ();
extern "C" void NumberFormatter_ToDecHex_m10596 ();
extern "C" void NumberFormatter_FastDecHexLen_m10597 ();
extern "C" void NumberFormatter_DecHexLen_m10598 ();
extern "C" void NumberFormatter_DecHexLen_m10599 ();
extern "C" void NumberFormatter_ScaleOrder_m10600 ();
extern "C" void NumberFormatter_InitialFloatingPrecision_m10601 ();
extern "C" void NumberFormatter_ParsePrecision_m10602 ();
extern "C" void NumberFormatter_Init_m10603 ();
extern "C" void NumberFormatter_InitHex_m10604 ();
extern "C" void NumberFormatter_Init_m10605 ();
extern "C" void NumberFormatter_Init_m10606 ();
extern "C" void NumberFormatter_Init_m10607 ();
extern "C" void NumberFormatter_Init_m10608 ();
extern "C" void NumberFormatter_Init_m10609 ();
extern "C" void NumberFormatter_Init_m10610 ();
extern "C" void NumberFormatter_ResetCharBuf_m10611 ();
extern "C" void NumberFormatter_Resize_m10612 ();
extern "C" void NumberFormatter_Append_m10613 ();
extern "C" void NumberFormatter_Append_m10614 ();
extern "C" void NumberFormatter_Append_m10615 ();
extern "C" void NumberFormatter_GetNumberFormatInstance_m10616 ();
extern "C" void NumberFormatter_set_CurrentCulture_m10617 ();
extern "C" void NumberFormatter_get_IntegerDigits_m10618 ();
extern "C" void NumberFormatter_get_DecimalDigits_m10619 ();
extern "C" void NumberFormatter_get_IsFloatingSource_m10620 ();
extern "C" void NumberFormatter_get_IsZero_m10621 ();
extern "C" void NumberFormatter_get_IsZeroInteger_m10622 ();
extern "C" void NumberFormatter_RoundPos_m10623 ();
extern "C" void NumberFormatter_RoundDecimal_m10624 ();
extern "C" void NumberFormatter_RoundBits_m10625 ();
extern "C" void NumberFormatter_RemoveTrailingZeros_m10626 ();
extern "C" void NumberFormatter_AddOneToDecHex_m10627 ();
extern "C" void NumberFormatter_AddOneToDecHex_m10628 ();
extern "C" void NumberFormatter_CountTrailingZeros_m10629 ();
extern "C" void NumberFormatter_CountTrailingZeros_m10630 ();
extern "C" void NumberFormatter_GetInstance_m10631 ();
extern "C" void NumberFormatter_Release_m10632 ();
extern "C" void NumberFormatter_SetThreadCurrentCulture_m10633 ();
extern "C" void NumberFormatter_NumberToString_m10634 ();
extern "C" void NumberFormatter_NumberToString_m10635 ();
extern "C" void NumberFormatter_NumberToString_m10636 ();
extern "C" void NumberFormatter_NumberToString_m10637 ();
extern "C" void NumberFormatter_NumberToString_m10638 ();
extern "C" void NumberFormatter_NumberToString_m10639 ();
extern "C" void NumberFormatter_NumberToString_m10640 ();
extern "C" void NumberFormatter_NumberToString_m10641 ();
extern "C" void NumberFormatter_NumberToString_m10642 ();
extern "C" void NumberFormatter_NumberToString_m10643 ();
extern "C" void NumberFormatter_NumberToString_m10644 ();
extern "C" void NumberFormatter_NumberToString_m10645 ();
extern "C" void NumberFormatter_NumberToString_m10646 ();
extern "C" void NumberFormatter_NumberToString_m10647 ();
extern "C" void NumberFormatter_NumberToString_m10648 ();
extern "C" void NumberFormatter_NumberToString_m10649 ();
extern "C" void NumberFormatter_NumberToString_m10650 ();
extern "C" void NumberFormatter_FastIntegerToString_m10651 ();
extern "C" void NumberFormatter_IntegerToString_m10652 ();
extern "C" void NumberFormatter_NumberToString_m10653 ();
extern "C" void NumberFormatter_FormatCurrency_m10654 ();
extern "C" void NumberFormatter_FormatDecimal_m10655 ();
extern "C" void NumberFormatter_FormatHexadecimal_m10656 ();
extern "C" void NumberFormatter_FormatFixedPoint_m10657 ();
extern "C" void NumberFormatter_FormatRoundtrip_m10658 ();
extern "C" void NumberFormatter_FormatRoundtrip_m10659 ();
extern "C" void NumberFormatter_FormatGeneral_m10660 ();
extern "C" void NumberFormatter_FormatNumber_m10661 ();
extern "C" void NumberFormatter_FormatPercent_m10662 ();
extern "C" void NumberFormatter_FormatExponential_m10663 ();
extern "C" void NumberFormatter_FormatExponential_m10664 ();
extern "C" void NumberFormatter_FormatCustom_m10665 ();
extern "C" void NumberFormatter_ZeroTrimEnd_m10666 ();
extern "C" void NumberFormatter_IsZeroOnly_m10667 ();
extern "C" void NumberFormatter_AppendNonNegativeNumber_m10668 ();
extern "C" void NumberFormatter_AppendIntegerString_m10669 ();
extern "C" void NumberFormatter_AppendIntegerString_m10670 ();
extern "C" void NumberFormatter_AppendDecimalString_m10671 ();
extern "C" void NumberFormatter_AppendDecimalString_m10672 ();
extern "C" void NumberFormatter_AppendIntegerStringWithGroupSeparator_m10673 ();
extern "C" void NumberFormatter_AppendExponent_m10674 ();
extern "C" void NumberFormatter_AppendOneDigit_m10675 ();
extern "C" void NumberFormatter_FastAppendDigits_m10676 ();
extern "C" void NumberFormatter_AppendDigits_m10677 ();
extern "C" void NumberFormatter_AppendDigits_m10678 ();
extern "C" void NumberFormatter_Multiply10_m10679 ();
extern "C" void NumberFormatter_Divide10_m10680 ();
extern "C" void NumberFormatter_GetClone_m10681 ();
extern "C" void ObjectDisposedException__ctor_m3733 ();
extern "C" void ObjectDisposedException__ctor_m10682 ();
extern "C" void ObjectDisposedException__ctor_m10683 ();
extern "C" void ObjectDisposedException_get_Message_m10684 ();
extern "C" void ObjectDisposedException_GetObjectData_m10685 ();
extern "C" void OperatingSystem__ctor_m10686 ();
extern "C" void OperatingSystem_get_Platform_m10687 ();
extern "C" void OperatingSystem_Clone_m10688 ();
extern "C" void OperatingSystem_GetObjectData_m10689 ();
extern "C" void OperatingSystem_ToString_m10690 ();
extern "C" void OutOfMemoryException__ctor_m10691 ();
extern "C" void OutOfMemoryException__ctor_m10692 ();
extern "C" void OverflowException__ctor_m10693 ();
extern "C" void OverflowException__ctor_m10694 ();
extern "C" void OverflowException__ctor_m10695 ();
extern "C" void RankException__ctor_m10696 ();
extern "C" void RankException__ctor_m10697 ();
extern "C" void RankException__ctor_m10698 ();
extern "C" void ResolveEventArgs__ctor_m10699 ();
extern "C" void RuntimeMethodHandle__ctor_m10700 ();
extern "C" void RuntimeMethodHandle__ctor_m10701 ();
extern "C" void RuntimeMethodHandle_get_Value_m10702 ();
extern "C" void RuntimeMethodHandle_GetObjectData_m10703 ();
extern "C" void RuntimeMethodHandle_Equals_m10704 ();
extern "C" void RuntimeMethodHandle_GetHashCode_m10705 ();
extern "C" void StringComparer__ctor_m10706 ();
extern "C" void StringComparer__cctor_m10707 ();
extern "C" void StringComparer_get_InvariantCultureIgnoreCase_m5683 ();
extern "C" void StringComparer_get_OrdinalIgnoreCase_m3624 ();
extern "C" void StringComparer_Compare_m10708 ();
extern "C" void StringComparer_Equals_m10709 ();
extern "C" void StringComparer_GetHashCode_m10710 ();
extern "C" void CultureAwareComparer__ctor_m10711 ();
extern "C" void CultureAwareComparer_Compare_m10712 ();
extern "C" void CultureAwareComparer_Equals_m10713 ();
extern "C" void CultureAwareComparer_GetHashCode_m10714 ();
extern "C" void OrdinalComparer__ctor_m10715 ();
extern "C" void OrdinalComparer_Compare_m10716 ();
extern "C" void OrdinalComparer_Equals_m10717 ();
extern "C" void OrdinalComparer_GetHashCode_m10718 ();
extern "C" void SystemException__ctor_m10719 ();
extern "C" void SystemException__ctor_m5712 ();
extern "C" void SystemException__ctor_m10720 ();
extern "C" void SystemException__ctor_m10721 ();
extern "C" void ThreadStaticAttribute__ctor_m10722 ();
extern "C" void TimeSpan__ctor_m10723 ();
extern "C" void TimeSpan__ctor_m10724 ();
extern "C" void TimeSpan__ctor_m10725 ();
extern "C" void TimeSpan__cctor_m10726 ();
extern "C" void TimeSpan_CalculateTicks_m10727 ();
extern "C" void TimeSpan_get_Days_m10728 ();
extern "C" void TimeSpan_get_Hours_m10729 ();
extern "C" void TimeSpan_get_Milliseconds_m10730 ();
extern "C" void TimeSpan_get_Minutes_m10731 ();
extern "C" void TimeSpan_get_Seconds_m10732 ();
extern "C" void TimeSpan_get_Ticks_m10733 ();
extern "C" void TimeSpan_get_TotalDays_m10734 ();
extern "C" void TimeSpan_get_TotalHours_m10735 ();
extern "C" void TimeSpan_get_TotalMilliseconds_m10736 ();
extern "C" void TimeSpan_get_TotalMinutes_m10737 ();
extern "C" void TimeSpan_get_TotalSeconds_m10738 ();
extern "C" void TimeSpan_Add_m10739 ();
extern "C" void TimeSpan_Compare_m10740 ();
extern "C" void TimeSpan_CompareTo_m10741 ();
extern "C" void TimeSpan_CompareTo_m10742 ();
extern "C" void TimeSpan_Equals_m10743 ();
extern "C" void TimeSpan_Duration_m10744 ();
extern "C" void TimeSpan_Equals_m10745 ();
extern "C" void TimeSpan_FromDays_m10746 ();
extern "C" void TimeSpan_FromHours_m10747 ();
extern "C" void TimeSpan_FromMinutes_m10748 ();
extern "C" void TimeSpan_FromSeconds_m10749 ();
extern "C" void TimeSpan_FromMilliseconds_m10750 ();
extern "C" void TimeSpan_From_m10751 ();
extern "C" void TimeSpan_GetHashCode_m10752 ();
extern "C" void TimeSpan_Negate_m10753 ();
extern "C" void TimeSpan_Subtract_m10754 ();
extern "C" void TimeSpan_ToString_m10755 ();
extern "C" void TimeSpan_op_Addition_m10756 ();
extern "C" void TimeSpan_op_Equality_m10757 ();
extern "C" void TimeSpan_op_GreaterThan_m10758 ();
extern "C" void TimeSpan_op_GreaterThanOrEqual_m10759 ();
extern "C" void TimeSpan_op_Inequality_m10760 ();
extern "C" void TimeSpan_op_LessThan_m10761 ();
extern "C" void TimeSpan_op_LessThanOrEqual_m10762 ();
extern "C" void TimeSpan_op_Subtraction_m10763 ();
extern "C" void TimeZone__ctor_m10764 ();
extern "C" void TimeZone__cctor_m10765 ();
extern "C" void TimeZone_get_CurrentTimeZone_m10766 ();
extern "C" void TimeZone_IsDaylightSavingTime_m10767 ();
extern "C" void TimeZone_IsDaylightSavingTime_m10768 ();
extern "C" void TimeZone_ToLocalTime_m10769 ();
extern "C" void TimeZone_ToUniversalTime_m10770 ();
extern "C" void TimeZone_GetLocalTimeDiff_m10771 ();
extern "C" void TimeZone_GetLocalTimeDiff_m10772 ();
extern "C" void CurrentSystemTimeZone__ctor_m10773 ();
extern "C" void CurrentSystemTimeZone__ctor_m10774 ();
extern "C" void CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10775 ();
extern "C" void CurrentSystemTimeZone_GetTimeZoneData_m10776 ();
extern "C" void CurrentSystemTimeZone_GetDaylightChanges_m10777 ();
extern "C" void CurrentSystemTimeZone_GetUtcOffset_m10778 ();
extern "C" void CurrentSystemTimeZone_OnDeserialization_m10779 ();
extern "C" void CurrentSystemTimeZone_GetDaylightTimeFromData_m10780 ();
extern "C" void TypeInitializationException__ctor_m10781 ();
extern "C" void TypeInitializationException_GetObjectData_m10782 ();
extern "C" void TypeLoadException__ctor_m10783 ();
extern "C" void TypeLoadException__ctor_m10784 ();
extern "C" void TypeLoadException__ctor_m10785 ();
extern "C" void TypeLoadException_get_Message_m10786 ();
extern "C" void TypeLoadException_GetObjectData_m10787 ();
extern "C" void UnauthorizedAccessException__ctor_m10788 ();
extern "C" void UnauthorizedAccessException__ctor_m10789 ();
extern "C" void UnauthorizedAccessException__ctor_m10790 ();
extern "C" void UnhandledExceptionEventArgs__ctor_m10791 ();
extern "C" void UnhandledExceptionEventArgs_get_ExceptionObject_m3582 ();
extern "C" void UnhandledExceptionEventArgs_get_IsTerminating_m10792 ();
extern "C" void UnitySerializationHolder__ctor_m10793 ();
extern "C" void UnitySerializationHolder_GetTypeData_m10794 ();
extern "C" void UnitySerializationHolder_GetDBNullData_m10795 ();
extern "C" void UnitySerializationHolder_GetModuleData_m10796 ();
extern "C" void UnitySerializationHolder_GetObjectData_m10797 ();
extern "C" void UnitySerializationHolder_GetRealObject_m10798 ();
extern "C" void Version__ctor_m10799 ();
extern "C" void Version__ctor_m10800 ();
extern "C" void Version__ctor_m5671 ();
extern "C" void Version__ctor_m10801 ();
extern "C" void Version__ctor_m10802 ();
extern "C" void Version_CheckedSet_m10803 ();
extern "C" void Version_get_Build_m10804 ();
extern "C" void Version_get_Major_m10805 ();
extern "C" void Version_get_Minor_m10806 ();
extern "C" void Version_get_Revision_m10807 ();
extern "C" void Version_Clone_m10808 ();
extern "C" void Version_CompareTo_m10809 ();
extern "C" void Version_Equals_m10810 ();
extern "C" void Version_CompareTo_m10811 ();
extern "C" void Version_Equals_m10812 ();
extern "C" void Version_GetHashCode_m10813 ();
extern "C" void Version_ToString_m10814 ();
extern "C" void Version_CreateFromString_m10815 ();
extern "C" void Version_op_Equality_m10816 ();
extern "C" void Version_op_Inequality_m10817 ();
extern "C" void WeakReference__ctor_m10818 ();
extern "C" void WeakReference__ctor_m10819 ();
extern "C" void WeakReference__ctor_m10820 ();
extern "C" void WeakReference__ctor_m10821 ();
extern "C" void WeakReference_AllocateHandle_m10822 ();
extern "C" void WeakReference_get_Target_m10823 ();
extern "C" void WeakReference_get_TrackResurrection_m10824 ();
extern "C" void WeakReference_Finalize_m10825 ();
extern "C" void WeakReference_GetObjectData_m10826 ();
extern "C" void PrimalityTest__ctor_m10827 ();
extern "C" void PrimalityTest_Invoke_m10828 ();
extern "C" void PrimalityTest_BeginInvoke_m10829 ();
extern "C" void PrimalityTest_EndInvoke_m10830 ();
extern "C" void MemberFilter__ctor_m10831 ();
extern "C" void MemberFilter_Invoke_m10832 ();
extern "C" void MemberFilter_BeginInvoke_m10833 ();
extern "C" void MemberFilter_EndInvoke_m10834 ();
extern "C" void TypeFilter__ctor_m10835 ();
extern "C" void TypeFilter_Invoke_m10836 ();
extern "C" void TypeFilter_BeginInvoke_m10837 ();
extern "C" void TypeFilter_EndInvoke_m10838 ();
extern "C" void CrossContextDelegate__ctor_m10839 ();
extern "C" void CrossContextDelegate_Invoke_m10840 ();
extern "C" void CrossContextDelegate_BeginInvoke_m10841 ();
extern "C" void CrossContextDelegate_EndInvoke_m10842 ();
extern "C" void HeaderHandler__ctor_m10843 ();
extern "C" void HeaderHandler_Invoke_m10844 ();
extern "C" void HeaderHandler_BeginInvoke_m10845 ();
extern "C" void HeaderHandler_EndInvoke_m10846 ();
extern "C" void ThreadStart__ctor_m10847 ();
extern "C" void ThreadStart_Invoke_m10848 ();
extern "C" void ThreadStart_BeginInvoke_m10849 ();
extern "C" void ThreadStart_EndInvoke_m10850 ();
extern "C" void TimerCallback__ctor_m10851 ();
extern "C" void TimerCallback_Invoke_m10852 ();
extern "C" void TimerCallback_BeginInvoke_m10853 ();
extern "C" void TimerCallback_EndInvoke_m10854 ();
extern "C" void WaitCallback__ctor_m10855 ();
extern "C" void WaitCallback_Invoke_m10856 ();
extern "C" void WaitCallback_BeginInvoke_m10857 ();
extern "C" void WaitCallback_EndInvoke_m10858 ();
extern "C" void AppDomainInitializer__ctor_m10859 ();
extern "C" void AppDomainInitializer_Invoke_m10860 ();
extern "C" void AppDomainInitializer_BeginInvoke_m10861 ();
extern "C" void AppDomainInitializer_EndInvoke_m10862 ();
extern "C" void AssemblyLoadEventHandler__ctor_m10863 ();
extern "C" void AssemblyLoadEventHandler_Invoke_m10864 ();
extern "C" void AssemblyLoadEventHandler_BeginInvoke_m10865 ();
extern "C" void AssemblyLoadEventHandler_EndInvoke_m10866 ();
extern "C" void EventHandler__ctor_m10867 ();
extern "C" void EventHandler_Invoke_m10868 ();
extern "C" void EventHandler_BeginInvoke_m10869 ();
extern "C" void EventHandler_EndInvoke_m10870 ();
extern "C" void ResolveEventHandler__ctor_m10871 ();
extern "C" void ResolveEventHandler_Invoke_m10872 ();
extern "C" void ResolveEventHandler_BeginInvoke_m10873 ();
extern "C" void ResolveEventHandler_EndInvoke_m10874 ();
extern "C" void UnhandledExceptionEventHandler__ctor_m3580 ();
extern "C" void UnhandledExceptionEventHandler_Invoke_m10875 ();
extern "C" void UnhandledExceptionEventHandler_BeginInvoke_m10876 ();
extern "C" void UnhandledExceptionEventHandler_EndInvoke_m10877 ();
extern const methodPointerType g_MethodPointers[10562] = 
{
	AxisTouchButton__ctor_m0,
	AxisTouchButton_OnEnable_m1,
	AxisTouchButton_FindPairedButton_m2,
	AxisTouchButton_OnDisable_m3,
	AxisTouchButton_OnPointerDown_m4,
	AxisTouchButton_OnPointerUp_m5,
	ButtonHandler__ctor_m6,
	ButtonHandler_OnEnable_m7,
	ButtonHandler_SetDownState_m8,
	ButtonHandler_SetUpState_m9,
	ButtonHandler_SetAxisPositiveState_m10,
	ButtonHandler_SetAxisNeutralState_m11,
	ButtonHandler_SetAxisNegativeState_m12,
	ButtonHandler_Update_m13,
	VirtualAxis__ctor_m14,
	VirtualAxis__ctor_m15,
	VirtualAxis_get_name_m16,
	VirtualAxis_set_name_m17,
	VirtualAxis_get_matchWithInputManager_m18,
	VirtualAxis_set_matchWithInputManager_m19,
	VirtualAxis_Remove_m20,
	VirtualAxis_Update_m21,
	VirtualAxis_get_GetValue_m22,
	VirtualAxis_get_GetValueRaw_m23,
	VirtualButton__ctor_m24,
	VirtualButton__ctor_m25,
	VirtualButton_get_name_m26,
	VirtualButton_set_name_m27,
	VirtualButton_get_matchWithInputManager_m28,
	VirtualButton_set_matchWithInputManager_m29,
	VirtualButton_Pressed_m30,
	VirtualButton_Released_m31,
	VirtualButton_Remove_m32,
	VirtualButton_get_GetButton_m33,
	VirtualButton_get_GetButtonDown_m34,
	VirtualButton_get_GetButtonUp_m35,
	CrossPlatformInputManager__cctor_m36,
	CrossPlatformInputManager_SwitchActiveInputMethod_m37,
	CrossPlatformInputManager_AxisExists_m38,
	CrossPlatformInputManager_ButtonExists_m39,
	CrossPlatformInputManager_RegisterVirtualAxis_m40,
	CrossPlatformInputManager_RegisterVirtualButton_m41,
	CrossPlatformInputManager_UnRegisterVirtualAxis_m42,
	CrossPlatformInputManager_UnRegisterVirtualButton_m43,
	CrossPlatformInputManager_VirtualAxisReference_m44,
	CrossPlatformInputManager_GetAxis_m45,
	CrossPlatformInputManager_GetAxisRaw_m46,
	CrossPlatformInputManager_GetAxis_m47,
	CrossPlatformInputManager_GetButton_m48,
	CrossPlatformInputManager_GetButtonDown_m49,
	CrossPlatformInputManager_GetButtonUp_m50,
	CrossPlatformInputManager_SetButtonDown_m51,
	CrossPlatformInputManager_SetButtonUp_m52,
	CrossPlatformInputManager_SetAxisPositive_m53,
	CrossPlatformInputManager_SetAxisNegative_m54,
	CrossPlatformInputManager_SetAxisZero_m55,
	CrossPlatformInputManager_SetAxis_m56,
	CrossPlatformInputManager_get_mousePosition_m57,
	CrossPlatformInputManager_SetVirtualMousePositionX_m58,
	CrossPlatformInputManager_SetVirtualMousePositionY_m59,
	CrossPlatformInputManager_SetVirtualMousePositionZ_m60,
	InputAxisScrollbar__ctor_m61,
	InputAxisScrollbar_Update_m62,
	InputAxisScrollbar_HandleInput_m63,
	Joystick__ctor_m64,
	Joystick_OnEnable_m65,
	Joystick_Start_m66,
	Joystick_UpdateVirtualAxes_m67,
	Joystick_CreateVirtualAxes_m68,
	Joystick_OnDrag_m69,
	Joystick_OnPointerUp_m70,
	Joystick_OnPointerDown_m71,
	Joystick_OnDisable_m72,
	MobileControlRig__ctor_m73,
	MobileControlRig_OnEnable_m74,
	MobileControlRig_Start_m75,
	MobileControlRig_CheckEnableControlRig_m76,
	MobileControlRig_EnableControlRig_m77,
	MobileInput__ctor_m78,
	MobileInput_AddButton_m79,
	MobileInput_AddAxes_m80,
	MobileInput_GetAxis_m81,
	MobileInput_SetButtonDown_m82,
	MobileInput_SetButtonUp_m83,
	MobileInput_SetAxisPositive_m84,
	MobileInput_SetAxisNegative_m85,
	MobileInput_SetAxisZero_m86,
	MobileInput_SetAxis_m87,
	MobileInput_GetButtonDown_m88,
	MobileInput_GetButtonUp_m89,
	MobileInput_GetButton_m90,
	MobileInput_MousePosition_m91,
	StandaloneInput__ctor_m92,
	StandaloneInput_GetAxis_m93,
	StandaloneInput_GetButton_m94,
	StandaloneInput_GetButtonDown_m95,
	StandaloneInput_GetButtonUp_m96,
	StandaloneInput_SetButtonDown_m97,
	StandaloneInput_SetButtonUp_m98,
	StandaloneInput_SetAxisPositive_m99,
	StandaloneInput_SetAxisNegative_m100,
	StandaloneInput_SetAxisZero_m101,
	StandaloneInput_SetAxis_m102,
	StandaloneInput_MousePosition_m103,
	AxisMapping__ctor_m104,
	TiltInput__ctor_m105,
	TiltInput_OnEnable_m106,
	TiltInput_Update_m107,
	TiltInput_OnDisable_m108,
	TouchPad__ctor_m109,
	TouchPad_OnEnable_m110,
	TouchPad_Start_m111,
	TouchPad_CreateVirtualAxes_m112,
	TouchPad_UpdateVirtualAxes_m113,
	TouchPad_OnPointerDown_m114,
	TouchPad_Update_m115,
	TouchPad_OnPointerUp_m116,
	TouchPad_OnDisable_m117,
	VirtualInput__ctor_m118,
	VirtualInput_get_virtualMousePosition_m119,
	VirtualInput_set_virtualMousePosition_m120,
	VirtualInput_AxisExists_m121,
	VirtualInput_ButtonExists_m122,
	VirtualInput_RegisterVirtualAxis_m123,
	VirtualInput_RegisterVirtualButton_m124,
	VirtualInput_UnRegisterVirtualAxis_m125,
	VirtualInput_UnRegisterVirtualButton_m126,
	VirtualInput_VirtualAxisReference_m127,
	VirtualInput_SetVirtualMousePositionX_m128,
	VirtualInput_SetVirtualMousePositionY_m129,
	VirtualInput_SetVirtualMousePositionZ_m130,
	BrakeLight__ctor_m131,
	BrakeLight_Start_m132,
	BrakeLight_Update_m133,
	CarAIControl__ctor_m134,
	CarAIControl_Awake_m135,
	CarAIControl_FixedUpdate_m136,
	CarAIControl_OnCollisionStay_m137,
	CarAIControl_SetTarget_m138,
	CarAudio__ctor_m139,
	CarAudio_StartSound_m140,
	CarAudio_StopSound_m141,
	CarAudio_Update_m142,
	CarAudio_SetUpEngineAudioSource_m143,
	CarAudio_ULerp_m144,
	CarController__ctor_m145,
	CarController__cctor_m146,
	CarController_get_Skidding_m147,
	CarController_set_Skidding_m148,
	CarController_get_BrakeInput_m149,
	CarController_set_BrakeInput_m150,
	CarController_get_CurrentSteerAngle_m151,
	CarController_get_CurrentSpeed_m152,
	CarController_get_MaxSpeed_m153,
	CarController_get_Revs_m154,
	CarController_set_Revs_m155,
	CarController_get_AccelInput_m156,
	CarController_set_AccelInput_m157,
	CarController_Start_m158,
	CarController_GearChanging_m159,
	CarController_CurveFactor_m160,
	CarController_ULerp_m161,
	CarController_CalculateGearFactor_m162,
	CarController_CalculateRevs_m163,
	CarController_Move_m164,
	CarController_CapSpeed_m165,
	CarController_ApplyDrive_m166,
	CarController_SteerHelper_m167,
	CarController_AddDownForce_m168,
	CarController_CheckForWheelSpin_m169,
	CarController_TractionControl_m170,
	CarController_AdjustTorque_m171,
	CarController_AnySkidSoundPlaying_m172,
	CarSelfRighting__ctor_m173,
	CarSelfRighting_Start_m174,
	CarSelfRighting_Update_m175,
	CarSelfRighting_RightCar_m176,
	CarUserControl__ctor_m177,
	CarUserControl_Start_m178,
	CarUserControl_Awake_m179,
	CarUserControl_FixedUpdate_m180,
	Mudguard__ctor_m181,
	Mudguard_Start_m182,
	Mudguard_Update_m183,
	U3CStartU3Ec__Iterator0__ctor_m184,
	U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m185,
	U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m186,
	U3CStartU3Ec__Iterator0_MoveNext_m187,
	U3CStartU3Ec__Iterator0_Dispose_m188,
	U3CStartU3Ec__Iterator0_Reset_m189,
	SkidTrail__ctor_m190,
	SkidTrail_Start_m191,
	Suspension__ctor_m192,
	Suspension_Start_m193,
	Suspension_Update_m194,
	U3CStartSkidTrailU3Ec__Iterator1__ctor_m195,
	U3CStartSkidTrailU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m196,
	U3CStartSkidTrailU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m197,
	U3CStartSkidTrailU3Ec__Iterator1_MoveNext_m198,
	U3CStartSkidTrailU3Ec__Iterator1_Dispose_m199,
	U3CStartSkidTrailU3Ec__Iterator1_Reset_m200,
	WheelEffects__ctor_m201,
	WheelEffects_get_skidding_m202,
	WheelEffects_set_skidding_m203,
	WheelEffects_get_PlayingAudio_m204,
	WheelEffects_set_PlayingAudio_m205,
	WheelEffects_Start_m206,
	WheelEffects_EmitTyreSmoke_m207,
	WheelEffects_PlayAudio_m208,
	WheelEffects_StopAudio_m209,
	WheelEffects_StartSkidTrail_m210,
	WheelEffects_EndSkidTrail_m211,
	Logo__ctor_m354,
	Logo_Reset_m355,
	Logo_Start_m356,
	Logo_updateTexRect_m357,
	Logo_OnGUI_m358,
	OrbitCamera__ctor_m359,
	OrbitCamera_Start_m360,
	OrbitCamera_Update_m361,
	OrbitCamera_LateUpdate_m362,
	OrbitCamera_ClampAngle_m363,
	AtualizaVida__ctor_m364,
	AtualizaVida_Start_m365,
	AtualizaVida_Update_m366,
	AtualizaVida_OnCollisionEnter_m367,
	AtualizaVida_OnTriggerStay_m368,
	CameraControl__ctor_m369,
	CameraControl_Awake_m370,
	CameraControl_FixedUpdate_m371,
	CameraControl_Move_m372,
	CameraControl_FindAveragePosition_m373,
	CameraControl_Zoom_m374,
	CameraControl_FindRequiredSize_m375,
	CameraControl_SetStartPositionAndSize_m376,
	U3CArrumarU3Ec__Iterator0__ctor_m377,
	U3CArrumarU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m378,
	U3CArrumarU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m379,
	U3CArrumarU3Ec__Iterator0_MoveNext_m380,
	U3CArrumarU3Ec__Iterator0_Dispose_m381,
	U3CArrumarU3Ec__Iterator0_Reset_m382,
	U3CAbastecerU3Ec__Iterator1__ctor_m383,
	U3CAbastecerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m384,
	U3CAbastecerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m385,
	U3CAbastecerU3Ec__Iterator1_MoveNext_m386,
	U3CAbastecerU3Ec__Iterator1_Dispose_m387,
	U3CAbastecerU3Ec__Iterator1_Reset_m388,
	Car_Health__ctor_m389,
	Car_Health_GetCombustivelAtual_m390,
	Car_Health_GetMecanicaAtual_m391,
	Car_Health_IsMorto_m392,
	Car_Health_IsCombustivelBaixo_m393,
	Car_Health_IsMecanicaBaixa_m394,
	Car_Health_Start_m395,
	Car_Health_OnEnable_m396,
	Car_Health_Update_m397,
	Car_Health_GastaMecanica_m398,
	Car_Health_AddMecanica_m399,
	Car_Health_Arrumar_m400,
	Car_Health_GastaCombustivel_m401,
	Car_Health_AddCombustivel_m402,
	Car_Health_Abastecer_m403,
	Car_Health_SetHealthUI_m404,
	Car_Health_OnDeath_m405,
	Funcoes_Botoes__ctor_m406,
	Funcoes_Botoes_Start_m407,
	Funcoes_Botoes_Update_m408,
	Funcoes_Botoes_Reiniciar_m409,
	Funcoes_Botoes_Sair_m410,
	Funcoes_Botoes_VoltarInicio_m411,
	Funcoes_Botoes_Pausar_m412,
	Principal__ctor_m413,
	Principal_Start_m414,
	Principal_Update_m415,
	Principal_CombustivelBaixo_m416,
	Principal_MecanicaBaixa_m417,
	Principal_ExibirMesagemPadrao_m418,
	Principal_ExibirMesagemAlerta_m419,
	Principal_ExibirMesagemPerigo_m420,
	EventSystem__ctor_m458,
	EventSystem__cctor_m459,
	EventSystem_get_current_m460,
	EventSystem_set_current_m461,
	EventSystem_get_sendNavigationEvents_m462,
	EventSystem_set_sendNavigationEvents_m463,
	EventSystem_get_pixelDragThreshold_m464,
	EventSystem_set_pixelDragThreshold_m465,
	EventSystem_get_currentInputModule_m466,
	EventSystem_get_firstSelectedGameObject_m467,
	EventSystem_set_firstSelectedGameObject_m468,
	EventSystem_get_currentSelectedGameObject_m469,
	EventSystem_get_lastSelectedGameObject_m470,
	EventSystem_UpdateModules_m471,
	EventSystem_get_alreadySelecting_m472,
	EventSystem_SetSelectedGameObject_m473,
	EventSystem_get_baseEventDataCache_m474,
	EventSystem_SetSelectedGameObject_m475,
	EventSystem_RaycastComparer_m476,
	EventSystem_RaycastAll_m477,
	EventSystem_IsPointerOverGameObject_m478,
	EventSystem_IsPointerOverGameObject_m479,
	EventSystem_OnEnable_m480,
	EventSystem_OnDisable_m481,
	EventSystem_TickModules_m482,
	EventSystem_Update_m483,
	EventSystem_ChangeEventModule_m484,
	EventSystem_ToString_m485,
	TriggerEvent__ctor_m486,
	Entry__ctor_m487,
	EventTrigger__ctor_m488,
	EventTrigger_get_triggers_m489,
	EventTrigger_set_triggers_m490,
	EventTrigger_Execute_m491,
	EventTrigger_OnPointerEnter_m492,
	EventTrigger_OnPointerExit_m493,
	EventTrigger_OnDrag_m494,
	EventTrigger_OnDrop_m495,
	EventTrigger_OnPointerDown_m496,
	EventTrigger_OnPointerUp_m497,
	EventTrigger_OnPointerClick_m498,
	EventTrigger_OnSelect_m499,
	EventTrigger_OnDeselect_m500,
	EventTrigger_OnScroll_m501,
	EventTrigger_OnMove_m502,
	EventTrigger_OnUpdateSelected_m503,
	EventTrigger_OnInitializePotentialDrag_m504,
	EventTrigger_OnBeginDrag_m505,
	EventTrigger_OnEndDrag_m506,
	EventTrigger_OnSubmit_m507,
	EventTrigger_OnCancel_m508,
	ExecuteEvents__cctor_m509,
	ExecuteEvents_Execute_m510,
	ExecuteEvents_Execute_m511,
	ExecuteEvents_Execute_m512,
	ExecuteEvents_Execute_m513,
	ExecuteEvents_Execute_m514,
	ExecuteEvents_Execute_m515,
	ExecuteEvents_Execute_m516,
	ExecuteEvents_Execute_m517,
	ExecuteEvents_Execute_m518,
	ExecuteEvents_Execute_m519,
	ExecuteEvents_Execute_m520,
	ExecuteEvents_Execute_m521,
	ExecuteEvents_Execute_m522,
	ExecuteEvents_Execute_m523,
	ExecuteEvents_Execute_m524,
	ExecuteEvents_Execute_m525,
	ExecuteEvents_Execute_m526,
	ExecuteEvents_get_pointerEnterHandler_m527,
	ExecuteEvents_get_pointerExitHandler_m528,
	ExecuteEvents_get_pointerDownHandler_m529,
	ExecuteEvents_get_pointerUpHandler_m530,
	ExecuteEvents_get_pointerClickHandler_m531,
	ExecuteEvents_get_initializePotentialDrag_m532,
	ExecuteEvents_get_beginDragHandler_m533,
	ExecuteEvents_get_dragHandler_m534,
	ExecuteEvents_get_endDragHandler_m535,
	ExecuteEvents_get_dropHandler_m536,
	ExecuteEvents_get_scrollHandler_m537,
	ExecuteEvents_get_updateSelectedHandler_m538,
	ExecuteEvents_get_selectHandler_m539,
	ExecuteEvents_get_deselectHandler_m540,
	ExecuteEvents_get_moveHandler_m541,
	ExecuteEvents_get_submitHandler_m542,
	ExecuteEvents_get_cancelHandler_m543,
	ExecuteEvents_GetEventChain_m544,
	ExecuteEvents_U3Cs_HandlerListPoolU3Em__0_m545,
	RaycasterManager__cctor_m546,
	RaycasterManager_AddRaycaster_m547,
	RaycasterManager_GetRaycasters_m548,
	RaycasterManager_RemoveRaycasters_m549,
	RaycastResult_get_gameObject_m550,
	RaycastResult_set_gameObject_m551,
	RaycastResult_get_isValid_m552,
	RaycastResult_Clear_m553,
	RaycastResult_ToString_m554,
	UIBehaviour__ctor_m555,
	UIBehaviour_Awake_m556,
	UIBehaviour_OnEnable_m557,
	UIBehaviour_Start_m558,
	UIBehaviour_OnDisable_m559,
	UIBehaviour_OnDestroy_m560,
	UIBehaviour_IsActive_m561,
	UIBehaviour_OnRectTransformDimensionsChange_m562,
	UIBehaviour_OnBeforeTransformParentChanged_m563,
	UIBehaviour_OnTransformParentChanged_m564,
	UIBehaviour_OnDidApplyAnimationProperties_m565,
	UIBehaviour_OnCanvasGroupChanged_m566,
	UIBehaviour_OnCanvasHierarchyChanged_m567,
	UIBehaviour_IsDestroyed_m568,
	AxisEventData__ctor_m569,
	AxisEventData_get_moveVector_m570,
	AxisEventData_set_moveVector_m571,
	AxisEventData_get_moveDir_m572,
	AxisEventData_set_moveDir_m573,
	BaseEventData__ctor_m574,
	BaseEventData_Reset_m575,
	BaseEventData_Use_m576,
	BaseEventData_get_used_m577,
	BaseEventData_get_currentInputModule_m578,
	BaseEventData_get_selectedObject_m579,
	BaseEventData_set_selectedObject_m580,
	PointerEventData__ctor_m581,
	PointerEventData_get_pointerEnter_m582,
	PointerEventData_set_pointerEnter_m583,
	PointerEventData_get_lastPress_m584,
	PointerEventData_set_lastPress_m585,
	PointerEventData_get_rawPointerPress_m586,
	PointerEventData_set_rawPointerPress_m587,
	PointerEventData_get_pointerDrag_m588,
	PointerEventData_set_pointerDrag_m589,
	PointerEventData_get_pointerCurrentRaycast_m590,
	PointerEventData_set_pointerCurrentRaycast_m591,
	PointerEventData_get_pointerPressRaycast_m592,
	PointerEventData_set_pointerPressRaycast_m593,
	PointerEventData_get_eligibleForClick_m594,
	PointerEventData_set_eligibleForClick_m595,
	PointerEventData_get_pointerId_m252,
	PointerEventData_set_pointerId_m596,
	PointerEventData_get_position_m228,
	PointerEventData_set_position_m597,
	PointerEventData_get_delta_m598,
	PointerEventData_set_delta_m599,
	PointerEventData_get_pressPosition_m600,
	PointerEventData_set_pressPosition_m601,
	PointerEventData_get_worldPosition_m602,
	PointerEventData_set_worldPosition_m603,
	PointerEventData_get_worldNormal_m604,
	PointerEventData_set_worldNormal_m605,
	PointerEventData_get_clickTime_m606,
	PointerEventData_set_clickTime_m607,
	PointerEventData_get_clickCount_m608,
	PointerEventData_set_clickCount_m609,
	PointerEventData_get_scrollDelta_m610,
	PointerEventData_set_scrollDelta_m611,
	PointerEventData_get_useDragThreshold_m612,
	PointerEventData_set_useDragThreshold_m613,
	PointerEventData_get_dragging_m614,
	PointerEventData_set_dragging_m615,
	PointerEventData_get_button_m616,
	PointerEventData_set_button_m617,
	PointerEventData_IsPointerMoving_m618,
	PointerEventData_IsScrolling_m619,
	PointerEventData_get_enterEventCamera_m620,
	PointerEventData_get_pressEventCamera_m621,
	PointerEventData_get_pointerPress_m622,
	PointerEventData_set_pointerPress_m623,
	PointerEventData_ToString_m624,
	BaseInputModule__ctor_m625,
	BaseInputModule_get_eventSystem_m626,
	BaseInputModule_OnEnable_m627,
	BaseInputModule_OnDisable_m628,
	BaseInputModule_FindFirstRaycast_m629,
	BaseInputModule_DetermineMoveDirection_m630,
	BaseInputModule_DetermineMoveDirection_m631,
	BaseInputModule_FindCommonRoot_m632,
	BaseInputModule_HandlePointerExitAndEnter_m633,
	BaseInputModule_GetAxisEventData_m634,
	BaseInputModule_GetBaseEventData_m635,
	BaseInputModule_IsPointerOverGameObject_m636,
	BaseInputModule_ShouldActivateModule_m637,
	BaseInputModule_DeactivateModule_m638,
	BaseInputModule_ActivateModule_m639,
	BaseInputModule_UpdateModule_m640,
	BaseInputModule_IsModuleSupported_m641,
	ButtonState__ctor_m642,
	ButtonState_get_eventData_m643,
	ButtonState_set_eventData_m644,
	ButtonState_get_button_m645,
	ButtonState_set_button_m646,
	MouseState__ctor_m647,
	MouseState_AnyPressesThisFrame_m648,
	MouseState_AnyReleasesThisFrame_m649,
	MouseState_GetButtonState_m650,
	MouseState_SetButtonState_m651,
	MouseButtonEventData__ctor_m652,
	MouseButtonEventData_PressedThisFrame_m653,
	MouseButtonEventData_ReleasedThisFrame_m654,
	PointerInputModule__ctor_m655,
	PointerInputModule_GetPointerData_m656,
	PointerInputModule_RemovePointerData_m657,
	PointerInputModule_GetTouchPointerEventData_m658,
	PointerInputModule_CopyFromTo_m659,
	PointerInputModule_StateForMouseButton_m660,
	PointerInputModule_GetMousePointerEventData_m661,
	PointerInputModule_GetMousePointerEventData_m662,
	PointerInputModule_GetLastPointerEventData_m663,
	PointerInputModule_ShouldStartDrag_m664,
	PointerInputModule_ProcessMove_m665,
	PointerInputModule_ProcessDrag_m666,
	PointerInputModule_IsPointerOverGameObject_m667,
	PointerInputModule_ClearSelection_m668,
	PointerInputModule_ToString_m669,
	PointerInputModule_DeselectIfSelectionChanged_m670,
	StandaloneInputModule__ctor_m671,
	StandaloneInputModule_get_inputMode_m672,
	StandaloneInputModule_get_allowActivationOnMobileDevice_m673,
	StandaloneInputModule_set_allowActivationOnMobileDevice_m674,
	StandaloneInputModule_get_forceModuleActive_m675,
	StandaloneInputModule_set_forceModuleActive_m676,
	StandaloneInputModule_get_inputActionsPerSecond_m677,
	StandaloneInputModule_set_inputActionsPerSecond_m678,
	StandaloneInputModule_get_repeatDelay_m679,
	StandaloneInputModule_set_repeatDelay_m680,
	StandaloneInputModule_get_horizontalAxis_m681,
	StandaloneInputModule_set_horizontalAxis_m682,
	StandaloneInputModule_get_verticalAxis_m683,
	StandaloneInputModule_set_verticalAxis_m684,
	StandaloneInputModule_get_submitButton_m685,
	StandaloneInputModule_set_submitButton_m686,
	StandaloneInputModule_get_cancelButton_m687,
	StandaloneInputModule_set_cancelButton_m688,
	StandaloneInputModule_UpdateModule_m689,
	StandaloneInputModule_IsModuleSupported_m690,
	StandaloneInputModule_ShouldActivateModule_m691,
	StandaloneInputModule_ActivateModule_m692,
	StandaloneInputModule_DeactivateModule_m693,
	StandaloneInputModule_Process_m694,
	StandaloneInputModule_SendSubmitEventToSelectedObject_m695,
	StandaloneInputModule_GetRawMoveVector_m696,
	StandaloneInputModule_SendMoveEventToSelectedObject_m697,
	StandaloneInputModule_ProcessMouseEvent_m698,
	StandaloneInputModule_ProcessMouseEvent_m699,
	StandaloneInputModule_SendUpdateEventToSelectedObject_m700,
	StandaloneInputModule_ProcessMousePress_m701,
	TouchInputModule__ctor_m702,
	TouchInputModule_get_allowActivationOnStandalone_m703,
	TouchInputModule_set_allowActivationOnStandalone_m704,
	TouchInputModule_get_forceModuleActive_m705,
	TouchInputModule_set_forceModuleActive_m706,
	TouchInputModule_UpdateModule_m707,
	TouchInputModule_IsModuleSupported_m708,
	TouchInputModule_ShouldActivateModule_m709,
	TouchInputModule_UseFakeInput_m710,
	TouchInputModule_Process_m711,
	TouchInputModule_FakeTouches_m712,
	TouchInputModule_ProcessTouchEvents_m713,
	TouchInputModule_ProcessTouchPress_m714,
	TouchInputModule_DeactivateModule_m715,
	TouchInputModule_ToString_m716,
	BaseRaycaster__ctor_m717,
	BaseRaycaster_get_priority_m718,
	BaseRaycaster_get_sortOrderPriority_m719,
	BaseRaycaster_get_renderOrderPriority_m720,
	BaseRaycaster_ToString_m721,
	BaseRaycaster_OnEnable_m722,
	BaseRaycaster_OnDisable_m723,
	Physics2DRaycaster__ctor_m724,
	Physics2DRaycaster_Raycast_m725,
	PhysicsRaycaster__ctor_m726,
	PhysicsRaycaster_get_eventCamera_m727,
	PhysicsRaycaster_get_depth_m728,
	PhysicsRaycaster_get_finalEventMask_m729,
	PhysicsRaycaster_get_eventMask_m730,
	PhysicsRaycaster_set_eventMask_m731,
	PhysicsRaycaster_Raycast_m732,
	PhysicsRaycaster_U3CRaycastU3Em__1_m733,
	ColorTweenCallback__ctor_m734,
	ColorTween_get_startColor_m735,
	ColorTween_set_startColor_m736,
	ColorTween_get_targetColor_m737,
	ColorTween_set_targetColor_m738,
	ColorTween_get_tweenMode_m739,
	ColorTween_set_tweenMode_m740,
	ColorTween_get_duration_m741,
	ColorTween_set_duration_m742,
	ColorTween_get_ignoreTimeScale_m743,
	ColorTween_set_ignoreTimeScale_m744,
	ColorTween_TweenValue_m745,
	ColorTween_AddOnChangedCallback_m746,
	ColorTween_GetIgnoreTimescale_m747,
	ColorTween_GetDuration_m748,
	ColorTween_ValidTarget_m749,
	FloatTweenCallback__ctor_m750,
	FloatTween_get_startValue_m751,
	FloatTween_set_startValue_m752,
	FloatTween_get_targetValue_m753,
	FloatTween_set_targetValue_m754,
	FloatTween_get_duration_m755,
	FloatTween_set_duration_m756,
	FloatTween_get_ignoreTimeScale_m757,
	FloatTween_set_ignoreTimeScale_m758,
	FloatTween_TweenValue_m759,
	FloatTween_AddOnChangedCallback_m760,
	FloatTween_GetIgnoreTimescale_m761,
	FloatTween_GetDuration_m762,
	FloatTween_ValidTarget_m763,
	AnimationTriggers__ctor_m764,
	AnimationTriggers_get_normalTrigger_m765,
	AnimationTriggers_set_normalTrigger_m766,
	AnimationTriggers_get_highlightedTrigger_m767,
	AnimationTriggers_set_highlightedTrigger_m768,
	AnimationTriggers_get_pressedTrigger_m769,
	AnimationTriggers_set_pressedTrigger_m770,
	AnimationTriggers_get_disabledTrigger_m771,
	AnimationTriggers_set_disabledTrigger_m772,
	ButtonClickedEvent__ctor_m773,
	U3COnFinishSubmitU3Ec__Iterator1__ctor_m774,
	U3COnFinishSubmitU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m775,
	U3COnFinishSubmitU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m776,
	U3COnFinishSubmitU3Ec__Iterator1_MoveNext_m777,
	U3COnFinishSubmitU3Ec__Iterator1_Dispose_m778,
	U3COnFinishSubmitU3Ec__Iterator1_Reset_m779,
	Button__ctor_m780,
	Button_get_onClick_m781,
	Button_set_onClick_m782,
	Button_Press_m783,
	Button_OnPointerClick_m784,
	Button_OnSubmit_m785,
	Button_OnFinishSubmit_m786,
	CanvasUpdateRegistry__ctor_m787,
	CanvasUpdateRegistry__cctor_m788,
	CanvasUpdateRegistry_get_instance_m789,
	CanvasUpdateRegistry_ObjectValidForUpdate_m790,
	CanvasUpdateRegistry_CleanInvalidItems_m791,
	CanvasUpdateRegistry_PerformUpdate_m792,
	CanvasUpdateRegistry_ParentCount_m793,
	CanvasUpdateRegistry_SortLayoutList_m794,
	CanvasUpdateRegistry_RegisterCanvasElementForLayoutRebuild_m795,
	CanvasUpdateRegistry_TryRegisterCanvasElementForLayoutRebuild_m796,
	CanvasUpdateRegistry_InternalRegisterCanvasElementForLayoutRebuild_m797,
	CanvasUpdateRegistry_RegisterCanvasElementForGraphicRebuild_m798,
	CanvasUpdateRegistry_TryRegisterCanvasElementForGraphicRebuild_m799,
	CanvasUpdateRegistry_InternalRegisterCanvasElementForGraphicRebuild_m800,
	CanvasUpdateRegistry_UnRegisterCanvasElementForRebuild_m801,
	CanvasUpdateRegistry_InternalUnRegisterCanvasElementForLayoutRebuild_m802,
	CanvasUpdateRegistry_InternalUnRegisterCanvasElementForGraphicRebuild_m803,
	CanvasUpdateRegistry_IsRebuildingLayout_m804,
	CanvasUpdateRegistry_IsRebuildingGraphics_m805,
	ColorBlock_get_normalColor_m806,
	ColorBlock_set_normalColor_m807,
	ColorBlock_get_highlightedColor_m808,
	ColorBlock_set_highlightedColor_m809,
	ColorBlock_get_pressedColor_m810,
	ColorBlock_set_pressedColor_m811,
	ColorBlock_get_disabledColor_m812,
	ColorBlock_set_disabledColor_m813,
	ColorBlock_get_colorMultiplier_m814,
	ColorBlock_set_colorMultiplier_m815,
	ColorBlock_get_fadeDuration_m816,
	ColorBlock_set_fadeDuration_m817,
	ColorBlock_get_defaultColorBlock_m818,
	DefaultControls__cctor_m819,
	DefaultControls_CreateUIElementRoot_m820,
	DefaultControls_CreateUIObject_m821,
	DefaultControls_SetDefaultTextValues_m822,
	DefaultControls_SetDefaultColorTransitionValues_m823,
	DefaultControls_SetParentAndAlign_m824,
	DefaultControls_SetLayerRecursively_m825,
	DefaultControls_CreatePanel_m826,
	DefaultControls_CreateButton_m827,
	DefaultControls_CreateText_m828,
	DefaultControls_CreateImage_m829,
	DefaultControls_CreateRawImage_m830,
	DefaultControls_CreateSlider_m831,
	DefaultControls_CreateScrollbar_m832,
	DefaultControls_CreateToggle_m833,
	DefaultControls_CreateInputField_m834,
	DefaultControls_CreateDropdown_m835,
	DefaultControls_CreateScrollView_m836,
	DropdownItem__ctor_m837,
	DropdownItem_get_text_m838,
	DropdownItem_set_text_m839,
	DropdownItem_get_image_m840,
	DropdownItem_set_image_m841,
	DropdownItem_get_rectTransform_m842,
	DropdownItem_set_rectTransform_m843,
	DropdownItem_get_toggle_m844,
	DropdownItem_set_toggle_m845,
	DropdownItem_OnPointerEnter_m846,
	DropdownItem_OnCancel_m847,
	OptionData__ctor_m848,
	OptionData__ctor_m849,
	OptionData__ctor_m850,
	OptionData__ctor_m851,
	OptionData_get_text_m852,
	OptionData_set_text_m853,
	OptionData_get_image_m854,
	OptionData_set_image_m855,
	OptionDataList__ctor_m856,
	OptionDataList_get_options_m857,
	OptionDataList_set_options_m858,
	DropdownEvent__ctor_m859,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2__ctor_m860,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m861,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_System_Collections_IEnumerator_get_Current_m862,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_MoveNext_m863,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_Dispose_m864,
	U3CDelayedDestroyDropdownListU3Ec__Iterator2_Reset_m865,
	U3CShowU3Ec__AnonStorey6__ctor_m866,
	U3CShowU3Ec__AnonStorey6_U3CU3Em__2_m867,
	Dropdown__ctor_m868,
	Dropdown_get_template_m869,
	Dropdown_set_template_m870,
	Dropdown_get_captionText_m871,
	Dropdown_set_captionText_m872,
	Dropdown_get_captionImage_m873,
	Dropdown_set_captionImage_m874,
	Dropdown_get_itemText_m875,
	Dropdown_set_itemText_m876,
	Dropdown_get_itemImage_m877,
	Dropdown_set_itemImage_m878,
	Dropdown_get_options_m879,
	Dropdown_set_options_m880,
	Dropdown_get_onValueChanged_m881,
	Dropdown_set_onValueChanged_m882,
	Dropdown_get_value_m883,
	Dropdown_set_value_m884,
	Dropdown_Awake_m885,
	Dropdown_Refresh_m886,
	Dropdown_SetupTemplate_m887,
	Dropdown_OnPointerClick_m888,
	Dropdown_OnSubmit_m889,
	Dropdown_OnCancel_m890,
	Dropdown_Show_m891,
	Dropdown_CreateBlocker_m892,
	Dropdown_DestroyBlocker_m893,
	Dropdown_CreateDropdownList_m894,
	Dropdown_DestroyDropdownList_m895,
	Dropdown_CreateItem_m896,
	Dropdown_DestroyItem_m897,
	Dropdown_AddItem_m898,
	Dropdown_AlphaFadeList_m899,
	Dropdown_AlphaFadeList_m900,
	Dropdown_SetAlpha_m901,
	Dropdown_Hide_m902,
	Dropdown_DelayedDestroyDropdownList_m903,
	Dropdown_OnSelectItem_m904,
	FontData__ctor_m905,
	FontData_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m906,
	FontData_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m907,
	FontData_get_defaultFontData_m908,
	FontData_get_font_m909,
	FontData_set_font_m910,
	FontData_get_fontSize_m911,
	FontData_set_fontSize_m912,
	FontData_get_fontStyle_m913,
	FontData_set_fontStyle_m914,
	FontData_get_bestFit_m915,
	FontData_set_bestFit_m916,
	FontData_get_minSize_m917,
	FontData_set_minSize_m918,
	FontData_get_maxSize_m919,
	FontData_set_maxSize_m920,
	FontData_get_alignment_m921,
	FontData_set_alignment_m922,
	FontData_get_richText_m923,
	FontData_set_richText_m924,
	FontData_get_horizontalOverflow_m925,
	FontData_set_horizontalOverflow_m926,
	FontData_get_verticalOverflow_m927,
	FontData_set_verticalOverflow_m928,
	FontData_get_lineSpacing_m929,
	FontData_set_lineSpacing_m930,
	FontUpdateTracker__cctor_m931,
	FontUpdateTracker_TrackText_m932,
	FontUpdateTracker_RebuildForFont_m933,
	FontUpdateTracker_UntrackText_m934,
	Graphic__ctor_m935,
	Graphic__cctor_m936,
	Graphic_get_defaultGraphicMaterial_m937,
	Graphic_get_color_m938,
	Graphic_set_color_m457,
	Graphic_get_raycastTarget_m939,
	Graphic_set_raycastTarget_m940,
	Graphic_get_useLegacyMeshGeneration_m941,
	Graphic_set_useLegacyMeshGeneration_m942,
	Graphic_SetAllDirty_m943,
	Graphic_SetLayoutDirty_m944,
	Graphic_SetVerticesDirty_m945,
	Graphic_SetMaterialDirty_m946,
	Graphic_OnRectTransformDimensionsChange_m947,
	Graphic_OnBeforeTransformParentChanged_m948,
	Graphic_OnTransformParentChanged_m949,
	Graphic_get_depth_m950,
	Graphic_get_rectTransform_m951,
	Graphic_get_canvas_m952,
	Graphic_CacheCanvas_m953,
	Graphic_get_canvasRenderer_m954,
	Graphic_get_defaultMaterial_m955,
	Graphic_get_material_m956,
	Graphic_set_material_m957,
	Graphic_get_materialForRendering_m958,
	Graphic_get_mainTexture_m959,
	Graphic_OnEnable_m960,
	Graphic_OnDisable_m961,
	Graphic_OnCanvasHierarchyChanged_m962,
	Graphic_Rebuild_m963,
	Graphic_LayoutComplete_m964,
	Graphic_GraphicUpdateComplete_m965,
	Graphic_UpdateMaterial_m966,
	Graphic_UpdateGeometry_m967,
	Graphic_DoMeshGeneration_m968,
	Graphic_DoLegacyMeshGeneration_m969,
	Graphic_get_workerMesh_m970,
	Graphic_OnFillVBO_m971,
	Graphic_OnPopulateMesh_m972,
	Graphic_OnPopulateMesh_m973,
	Graphic_OnDidApplyAnimationProperties_m974,
	Graphic_SetNativeSize_m975,
	Graphic_Raycast_m976,
	Graphic_PixelAdjustPoint_m977,
	Graphic_GetPixelAdjustedRect_m978,
	Graphic_CrossFadeColor_m979,
	Graphic_CrossFadeColor_m980,
	Graphic_CreateColorFromAlpha_m981,
	Graphic_CrossFadeAlpha_m982,
	Graphic_RegisterDirtyLayoutCallback_m983,
	Graphic_UnregisterDirtyLayoutCallback_m984,
	Graphic_RegisterDirtyVerticesCallback_m985,
	Graphic_UnregisterDirtyVerticesCallback_m986,
	Graphic_RegisterDirtyMaterialCallback_m987,
	Graphic_UnregisterDirtyMaterialCallback_m988,
	Graphic_UnityEngine_UI_ICanvasElement_IsDestroyed_m989,
	Graphic_UnityEngine_UI_ICanvasElement_get_transform_m990,
	GraphicRaycaster__ctor_m991,
	GraphicRaycaster__cctor_m992,
	GraphicRaycaster_get_sortOrderPriority_m993,
	GraphicRaycaster_get_renderOrderPriority_m994,
	GraphicRaycaster_get_ignoreReversedGraphics_m995,
	GraphicRaycaster_set_ignoreReversedGraphics_m996,
	GraphicRaycaster_get_blockingObjects_m997,
	GraphicRaycaster_set_blockingObjects_m998,
	GraphicRaycaster_get_canvas_m999,
	GraphicRaycaster_Raycast_m1000,
	GraphicRaycaster_get_eventCamera_m1001,
	GraphicRaycaster_Raycast_m1002,
	GraphicRaycaster_U3CRaycastU3Em__3_m1003,
	GraphicRegistry__ctor_m1004,
	GraphicRegistry__cctor_m1005,
	GraphicRegistry_get_instance_m1006,
	GraphicRegistry_RegisterGraphicForCanvas_m1007,
	GraphicRegistry_UnregisterGraphicForCanvas_m1008,
	GraphicRegistry_GetGraphicsForCanvas_m1009,
	Image__ctor_m1010,
	Image__cctor_m1011,
	Image_get_sprite_m1012,
	Image_set_sprite_m1013,
	Image_get_overrideSprite_m1014,
	Image_set_overrideSprite_m1015,
	Image_get_type_m1016,
	Image_set_type_m1017,
	Image_get_preserveAspect_m1018,
	Image_set_preserveAspect_m1019,
	Image_get_fillCenter_m1020,
	Image_set_fillCenter_m1021,
	Image_get_fillMethod_m1022,
	Image_set_fillMethod_m1023,
	Image_get_fillAmount_m1024,
	Image_set_fillAmount_m1025,
	Image_get_fillClockwise_m1026,
	Image_set_fillClockwise_m1027,
	Image_get_fillOrigin_m1028,
	Image_set_fillOrigin_m1029,
	Image_get_eventAlphaThreshold_m1030,
	Image_set_eventAlphaThreshold_m1031,
	Image_get_mainTexture_m1032,
	Image_get_hasBorder_m1033,
	Image_get_pixelsPerUnit_m1034,
	Image_OnBeforeSerialize_m1035,
	Image_OnAfterDeserialize_m1036,
	Image_GetDrawingDimensions_m1037,
	Image_SetNativeSize_m1038,
	Image_OnPopulateMesh_m1039,
	Image_GenerateSimpleSprite_m1040,
	Image_GenerateSlicedSprite_m1041,
	Image_GenerateTiledSprite_m1042,
	Image_AddQuad_m1043,
	Image_AddQuad_m1044,
	Image_GetAdjustedBorders_m1045,
	Image_GenerateFilledSprite_m1046,
	Image_RadialCut_m1047,
	Image_RadialCut_m1048,
	Image_CalculateLayoutInputHorizontal_m1049,
	Image_CalculateLayoutInputVertical_m1050,
	Image_get_minWidth_m1051,
	Image_get_preferredWidth_m1052,
	Image_get_flexibleWidth_m1053,
	Image_get_minHeight_m1054,
	Image_get_preferredHeight_m1055,
	Image_get_flexibleHeight_m1056,
	Image_get_layoutPriority_m1057,
	Image_IsRaycastLocationValid_m1058,
	Image_MapCoordinate_m1059,
	SubmitEvent__ctor_m1060,
	OnChangeEvent__ctor_m1061,
	OnValidateInput__ctor_m1062,
	OnValidateInput_Invoke_m1063,
	OnValidateInput_BeginInvoke_m1064,
	OnValidateInput_EndInvoke_m1065,
	U3CCaretBlinkU3Ec__Iterator3__ctor_m1066,
	U3CCaretBlinkU3Ec__Iterator3_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1067,
	U3CCaretBlinkU3Ec__Iterator3_System_Collections_IEnumerator_get_Current_m1068,
	U3CCaretBlinkU3Ec__Iterator3_MoveNext_m1069,
	U3CCaretBlinkU3Ec__Iterator3_Dispose_m1070,
	U3CCaretBlinkU3Ec__Iterator3_Reset_m1071,
	U3CMouseDragOutsideRectU3Ec__Iterator4__ctor_m1072,
	U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1073,
	U3CMouseDragOutsideRectU3Ec__Iterator4_System_Collections_IEnumerator_get_Current_m1074,
	U3CMouseDragOutsideRectU3Ec__Iterator4_MoveNext_m1075,
	U3CMouseDragOutsideRectU3Ec__Iterator4_Dispose_m1076,
	U3CMouseDragOutsideRectU3Ec__Iterator4_Reset_m1077,
	InputField__ctor_m1078,
	InputField__cctor_m1079,
	InputField_get_mesh_m1080,
	InputField_get_cachedInputTextGenerator_m1081,
	InputField_set_shouldHideMobileInput_m1082,
	InputField_get_shouldHideMobileInput_m1083,
	InputField_get_text_m1084,
	InputField_set_text_m1085,
	InputField_get_isFocused_m1086,
	InputField_get_caretBlinkRate_m1087,
	InputField_set_caretBlinkRate_m1088,
	InputField_get_textComponent_m1089,
	InputField_set_textComponent_m1090,
	InputField_get_placeholder_m1091,
	InputField_set_placeholder_m1092,
	InputField_get_selectionColor_m1093,
	InputField_set_selectionColor_m1094,
	InputField_get_onEndEdit_m1095,
	InputField_set_onEndEdit_m1096,
	InputField_get_onValueChange_m1097,
	InputField_set_onValueChange_m1098,
	InputField_get_onValidateInput_m1099,
	InputField_set_onValidateInput_m1100,
	InputField_get_characterLimit_m1101,
	InputField_set_characterLimit_m1102,
	InputField_get_contentType_m1103,
	InputField_set_contentType_m1104,
	InputField_get_lineType_m1105,
	InputField_set_lineType_m1106,
	InputField_get_inputType_m1107,
	InputField_set_inputType_m1108,
	InputField_get_keyboardType_m1109,
	InputField_set_keyboardType_m1110,
	InputField_get_characterValidation_m1111,
	InputField_set_characterValidation_m1112,
	InputField_get_multiLine_m1113,
	InputField_get_asteriskChar_m1114,
	InputField_set_asteriskChar_m1115,
	InputField_get_wasCanceled_m1116,
	InputField_ClampPos_m1117,
	InputField_get_caretPositionInternal_m1118,
	InputField_set_caretPositionInternal_m1119,
	InputField_get_caretSelectPositionInternal_m1120,
	InputField_set_caretSelectPositionInternal_m1121,
	InputField_get_hasSelection_m1122,
	InputField_get_caretPosition_m1123,
	InputField_set_caretPosition_m1124,
	InputField_get_selectionAnchorPosition_m1125,
	InputField_set_selectionAnchorPosition_m1126,
	InputField_get_selectionFocusPosition_m1127,
	InputField_set_selectionFocusPosition_m1128,
	InputField_OnEnable_m1129,
	InputField_OnDisable_m1130,
	InputField_CaretBlink_m1131,
	InputField_SetCaretVisible_m1132,
	InputField_SetCaretActive_m1133,
	InputField_OnFocus_m1134,
	InputField_SelectAll_m1135,
	InputField_MoveTextEnd_m1136,
	InputField_MoveTextStart_m1137,
	InputField_get_clipboard_m1138,
	InputField_set_clipboard_m1139,
	InputField_InPlaceEditing_m1140,
	InputField_LateUpdate_m1141,
	InputField_ScreenToLocal_m1142,
	InputField_GetUnclampedCharacterLineFromPosition_m1143,
	InputField_GetCharacterIndexFromPosition_m1144,
	InputField_MayDrag_m1145,
	InputField_OnBeginDrag_m1146,
	InputField_OnDrag_m1147,
	InputField_MouseDragOutsideRect_m1148,
	InputField_OnEndDrag_m1149,
	InputField_OnPointerDown_m1150,
	InputField_KeyPressed_m1151,
	InputField_IsValidChar_m1152,
	InputField_ProcessEvent_m1153,
	InputField_OnUpdateSelected_m1154,
	InputField_GetSelectedString_m1155,
	InputField_FindtNextWordBegin_m1156,
	InputField_MoveRight_m1157,
	InputField_FindtPrevWordBegin_m1158,
	InputField_MoveLeft_m1159,
	InputField_DetermineCharacterLine_m1160,
	InputField_LineUpCharacterPosition_m1161,
	InputField_LineDownCharacterPosition_m1162,
	InputField_MoveDown_m1163,
	InputField_MoveDown_m1164,
	InputField_MoveUp_m1165,
	InputField_MoveUp_m1166,
	InputField_Delete_m1167,
	InputField_ForwardSpace_m1168,
	InputField_Backspace_m1169,
	InputField_Insert_m1170,
	InputField_SendOnValueChangedAndUpdateLabel_m1171,
	InputField_SendOnValueChanged_m1172,
	InputField_SendOnSubmit_m1173,
	InputField_Append_m1174,
	InputField_Append_m1175,
	InputField_UpdateLabel_m1176,
	InputField_IsSelectionVisible_m1177,
	InputField_GetLineStartPosition_m1178,
	InputField_GetLineEndPosition_m1179,
	InputField_SetDrawRangeToContainCaretPosition_m1180,
	InputField_MarkGeometryAsDirty_m1181,
	InputField_Rebuild_m1182,
	InputField_LayoutComplete_m1183,
	InputField_GraphicUpdateComplete_m1184,
	InputField_UpdateGeometry_m1185,
	InputField_AssignPositioningIfNeeded_m1186,
	InputField_OnFillVBO_m1187,
	InputField_GenerateCursor_m1188,
	InputField_CreateCursorVerts_m1189,
	InputField_SumLineHeights_m1190,
	InputField_GenerateHightlight_m1191,
	InputField_Validate_m1192,
	InputField_ActivateInputField_m1193,
	InputField_ActivateInputFieldInternal_m1194,
	InputField_OnSelect_m1195,
	InputField_OnPointerClick_m1196,
	InputField_DeactivateInputField_m1197,
	InputField_OnDeselect_m1198,
	InputField_OnSubmit_m1199,
	InputField_EnforceContentType_m1200,
	InputField_SetToCustomIfContentTypeIsNot_m1201,
	InputField_SetToCustom_m1202,
	InputField_DoStateTransition_m1203,
	InputField_UnityEngine_UI_ICanvasElement_IsDestroyed_m1204,
	InputField_UnityEngine_UI_ICanvasElement_get_transform_m1205,
	Mask__ctor_m1206,
	Mask_get_rectTransform_m1207,
	Mask_get_showMaskGraphic_m1208,
	Mask_set_showMaskGraphic_m1209,
	Mask_get_graphic_m1210,
	Mask_MaskEnabled_m1211,
	Mask_OnSiblingGraphicEnabledDisabled_m1212,
	Mask_OnEnable_m1213,
	Mask_OnDisable_m1214,
	Mask_IsRaycastLocationValid_m1215,
	Mask_GetModifiedMaterial_m1216,
	CullStateChangedEvent__ctor_m1217,
	MaskableGraphic__ctor_m1218,
	MaskableGraphic_get_onCullStateChanged_m1219,
	MaskableGraphic_set_onCullStateChanged_m1220,
	MaskableGraphic_get_maskable_m1221,
	MaskableGraphic_set_maskable_m1222,
	MaskableGraphic_GetModifiedMaterial_m1223,
	MaskableGraphic_Cull_m1224,
	MaskableGraphic_SetClipRect_m1225,
	MaskableGraphic_OnEnable_m1226,
	MaskableGraphic_OnDisable_m1227,
	MaskableGraphic_OnTransformParentChanged_m1228,
	MaskableGraphic_ParentMaskStateChanged_m1229,
	MaskableGraphic_OnCanvasHierarchyChanged_m1230,
	MaskableGraphic_get_canvasRect_m1231,
	MaskableGraphic_UpdateClipParent_m1232,
	MaskableGraphic_RecalculateClipping_m1233,
	MaskableGraphic_RecalculateMasking_m1234,
	MaskableGraphic_UnityEngine_UI_IClippable_get_rectTransform_m1235,
	MaskUtilities__ctor_m1236,
	MaskUtilities_Notify2DMaskStateChanged_m1237,
	MaskUtilities_NotifyStencilStateChanged_m1238,
	MaskUtilities_FindRootSortOverrideCanvas_m1239,
	MaskUtilities_GetStencilDepth_m1240,
	MaskUtilities_GetRectMaskForClippable_m1241,
	MaskUtilities_GetRectMasksForClip_m1242,
	Misc_Destroy_m1243,
	Misc_DestroyImmediate_m1244,
	Navigation_get_mode_m1245,
	Navigation_set_mode_m1246,
	Navigation_get_selectOnUp_m1247,
	Navigation_set_selectOnUp_m1248,
	Navigation_get_selectOnDown_m1249,
	Navigation_set_selectOnDown_m1250,
	Navigation_get_selectOnLeft_m1251,
	Navigation_set_selectOnLeft_m1252,
	Navigation_get_selectOnRight_m1253,
	Navigation_set_selectOnRight_m1254,
	Navigation_get_defaultNavigation_m1255,
	RawImage__ctor_m1256,
	RawImage_get_mainTexture_m1257,
	RawImage_get_texture_m1258,
	RawImage_set_texture_m1259,
	RawImage_get_uvRect_m1260,
	RawImage_set_uvRect_m1261,
	RawImage_SetNativeSize_m1262,
	RawImage_OnPopulateMesh_m1263,
	RectMask2D__ctor_m1264,
	RectMask2D_get_canvasRect_m1265,
	RectMask2D_get_rectTransform_m1266,
	RectMask2D_OnEnable_m1267,
	RectMask2D_OnDisable_m1268,
	RectMask2D_IsRaycastLocationValid_m1269,
	RectMask2D_PerformClipping_m1270,
	RectMask2D_AddClippable_m1271,
	RectMask2D_RemoveClippable_m1272,
	RectMask2D_OnTransformParentChanged_m1273,
	RectMask2D_OnCanvasHierarchyChanged_m1274,
	ScrollEvent__ctor_m1275,
	U3CClickRepeatU3Ec__Iterator5__ctor_m1276,
	U3CClickRepeatU3Ec__Iterator5_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m1277,
	U3CClickRepeatU3Ec__Iterator5_System_Collections_IEnumerator_get_Current_m1278,
	U3CClickRepeatU3Ec__Iterator5_MoveNext_m1279,
	U3CClickRepeatU3Ec__Iterator5_Dispose_m1280,
	U3CClickRepeatU3Ec__Iterator5_Reset_m1281,
	Scrollbar__ctor_m1282,
	Scrollbar_get_handleRect_m1283,
	Scrollbar_set_handleRect_m1284,
	Scrollbar_get_direction_m1285,
	Scrollbar_set_direction_m1286,
	Scrollbar_get_value_m1287,
	Scrollbar_set_value_m1288,
	Scrollbar_get_size_m1289,
	Scrollbar_set_size_m1290,
	Scrollbar_get_numberOfSteps_m1291,
	Scrollbar_set_numberOfSteps_m1292,
	Scrollbar_get_onValueChanged_m1293,
	Scrollbar_set_onValueChanged_m1294,
	Scrollbar_get_stepSize_m1295,
	Scrollbar_Rebuild_m1296,
	Scrollbar_LayoutComplete_m1297,
	Scrollbar_GraphicUpdateComplete_m1298,
	Scrollbar_OnEnable_m1299,
	Scrollbar_OnDisable_m1300,
	Scrollbar_UpdateCachedReferences_m1301,
	Scrollbar_Set_m1302,
	Scrollbar_Set_m1303,
	Scrollbar_OnRectTransformDimensionsChange_m1304,
	Scrollbar_get_axis_m1305,
	Scrollbar_get_reverseValue_m1306,
	Scrollbar_UpdateVisuals_m1307,
	Scrollbar_UpdateDrag_m1308,
	Scrollbar_MayDrag_m1309,
	Scrollbar_OnBeginDrag_m1310,
	Scrollbar_OnDrag_m1311,
	Scrollbar_OnPointerDown_m1312,
	Scrollbar_ClickRepeat_m1313,
	Scrollbar_OnPointerUp_m1314,
	Scrollbar_OnMove_m1315,
	Scrollbar_FindSelectableOnLeft_m1316,
	Scrollbar_FindSelectableOnRight_m1317,
	Scrollbar_FindSelectableOnUp_m1318,
	Scrollbar_FindSelectableOnDown_m1319,
	Scrollbar_OnInitializePotentialDrag_m1320,
	Scrollbar_SetDirection_m1321,
	Scrollbar_UnityEngine_UI_ICanvasElement_IsDestroyed_m1322,
	Scrollbar_UnityEngine_UI_ICanvasElement_get_transform_m1323,
	ScrollRectEvent__ctor_m1324,
	ScrollRect__ctor_m1325,
	ScrollRect_get_content_m1326,
	ScrollRect_set_content_m1327,
	ScrollRect_get_horizontal_m1328,
	ScrollRect_set_horizontal_m1329,
	ScrollRect_get_vertical_m1330,
	ScrollRect_set_vertical_m1331,
	ScrollRect_get_movementType_m1332,
	ScrollRect_set_movementType_m1333,
	ScrollRect_get_elasticity_m1334,
	ScrollRect_set_elasticity_m1335,
	ScrollRect_get_inertia_m1336,
	ScrollRect_set_inertia_m1337,
	ScrollRect_get_decelerationRate_m1338,
	ScrollRect_set_decelerationRate_m1339,
	ScrollRect_get_scrollSensitivity_m1340,
	ScrollRect_set_scrollSensitivity_m1341,
	ScrollRect_get_viewport_m1342,
	ScrollRect_set_viewport_m1343,
	ScrollRect_get_horizontalScrollbar_m1344,
	ScrollRect_set_horizontalScrollbar_m1345,
	ScrollRect_get_verticalScrollbar_m1346,
	ScrollRect_set_verticalScrollbar_m1347,
	ScrollRect_get_horizontalScrollbarVisibility_m1348,
	ScrollRect_set_horizontalScrollbarVisibility_m1349,
	ScrollRect_get_verticalScrollbarVisibility_m1350,
	ScrollRect_set_verticalScrollbarVisibility_m1351,
	ScrollRect_get_horizontalScrollbarSpacing_m1352,
	ScrollRect_set_horizontalScrollbarSpacing_m1353,
	ScrollRect_get_verticalScrollbarSpacing_m1354,
	ScrollRect_set_verticalScrollbarSpacing_m1355,
	ScrollRect_get_onValueChanged_m1356,
	ScrollRect_set_onValueChanged_m1357,
	ScrollRect_get_viewRect_m1358,
	ScrollRect_get_velocity_m1359,
	ScrollRect_set_velocity_m1360,
	ScrollRect_get_rectTransform_m1361,
	ScrollRect_Rebuild_m1362,
	ScrollRect_LayoutComplete_m1363,
	ScrollRect_GraphicUpdateComplete_m1364,
	ScrollRect_UpdateCachedData_m1365,
	ScrollRect_OnEnable_m1366,
	ScrollRect_OnDisable_m1367,
	ScrollRect_IsActive_m1368,
	ScrollRect_EnsureLayoutHasRebuilt_m1369,
	ScrollRect_StopMovement_m1370,
	ScrollRect_OnScroll_m1371,
	ScrollRect_OnInitializePotentialDrag_m1372,
	ScrollRect_OnBeginDrag_m1373,
	ScrollRect_OnEndDrag_m1374,
	ScrollRect_OnDrag_m1375,
	ScrollRect_SetContentAnchoredPosition_m1376,
	ScrollRect_LateUpdate_m1377,
	ScrollRect_UpdatePrevData_m1378,
	ScrollRect_UpdateScrollbars_m1379,
	ScrollRect_get_normalizedPosition_m1380,
	ScrollRect_set_normalizedPosition_m1381,
	ScrollRect_get_horizontalNormalizedPosition_m1382,
	ScrollRect_set_horizontalNormalizedPosition_m1383,
	ScrollRect_get_verticalNormalizedPosition_m1384,
	ScrollRect_set_verticalNormalizedPosition_m1385,
	ScrollRect_SetHorizontalNormalizedPosition_m1386,
	ScrollRect_SetVerticalNormalizedPosition_m1387,
	ScrollRect_SetNormalizedPosition_m1388,
	ScrollRect_RubberDelta_m1389,
	ScrollRect_OnRectTransformDimensionsChange_m1390,
	ScrollRect_get_hScrollingNeeded_m1391,
	ScrollRect_get_vScrollingNeeded_m1392,
	ScrollRect_CalculateLayoutInputHorizontal_m1393,
	ScrollRect_CalculateLayoutInputVertical_m1394,
	ScrollRect_get_minWidth_m1395,
	ScrollRect_get_preferredWidth_m1396,
	ScrollRect_get_flexibleWidth_m1397,
	ScrollRect_set_flexibleWidth_m1398,
	ScrollRect_get_minHeight_m1399,
	ScrollRect_get_preferredHeight_m1400,
	ScrollRect_get_flexibleHeight_m1401,
	ScrollRect_get_layoutPriority_m1402,
	ScrollRect_SetLayoutHorizontal_m1403,
	ScrollRect_SetLayoutVertical_m1404,
	ScrollRect_UpdateScrollbarVisibility_m1405,
	ScrollRect_UpdateScrollbarLayout_m1406,
	ScrollRect_UpdateBounds_m1407,
	ScrollRect_GetBounds_m1408,
	ScrollRect_CalculateOffset_m1409,
	ScrollRect_SetDirty_m1410,
	ScrollRect_SetDirtyCaching_m1411,
	ScrollRect_UnityEngine_UI_ICanvasElement_IsDestroyed_m1412,
	ScrollRect_UnityEngine_UI_ICanvasElement_get_transform_m1413,
	Selectable__ctor_m1414,
	Selectable__cctor_m1415,
	Selectable_get_allSelectables_m1416,
	Selectable_get_navigation_m1417,
	Selectable_set_navigation_m1418,
	Selectable_get_transition_m1419,
	Selectable_set_transition_m1420,
	Selectable_get_colors_m1421,
	Selectable_set_colors_m1422,
	Selectable_get_spriteState_m1423,
	Selectable_set_spriteState_m1424,
	Selectable_get_animationTriggers_m1425,
	Selectable_set_animationTriggers_m1426,
	Selectable_get_targetGraphic_m1427,
	Selectable_set_targetGraphic_m1428,
	Selectable_get_interactable_m1429,
	Selectable_set_interactable_m1430,
	Selectable_get_isPointerInside_m1431,
	Selectable_set_isPointerInside_m1432,
	Selectable_get_isPointerDown_m1433,
	Selectable_set_isPointerDown_m1434,
	Selectable_get_hasSelection_m1435,
	Selectable_set_hasSelection_m1436,
	Selectable_get_image_m1437,
	Selectable_set_image_m1438,
	Selectable_get_animator_m1439,
	Selectable_Awake_m1440,
	Selectable_OnCanvasGroupChanged_m1441,
	Selectable_IsInteractable_m1442,
	Selectable_OnDidApplyAnimationProperties_m1443,
	Selectable_OnEnable_m1444,
	Selectable_OnSetProperty_m1445,
	Selectable_OnDisable_m1446,
	Selectable_get_currentSelectionState_m1447,
	Selectable_InstantClearState_m1448,
	Selectable_DoStateTransition_m1449,
	Selectable_FindSelectable_m1450,
	Selectable_GetPointOnRectEdge_m1451,
	Selectable_Navigate_m1452,
	Selectable_FindSelectableOnLeft_m1453,
	Selectable_FindSelectableOnRight_m1454,
	Selectable_FindSelectableOnUp_m1455,
	Selectable_FindSelectableOnDown_m1456,
	Selectable_OnMove_m1457,
	Selectable_StartColorTween_m1458,
	Selectable_DoSpriteSwap_m1459,
	Selectable_TriggerAnimation_m1460,
	Selectable_IsHighlighted_m1461,
	Selectable_IsPressed_m1462,
	Selectable_IsPressed_m1463,
	Selectable_UpdateSelectionState_m1464,
	Selectable_EvaluateAndTransitionToSelectionState_m1465,
	Selectable_InternalEvaluateAndTransitionToSelectionState_m1466,
	Selectable_OnPointerDown_m1467,
	Selectable_OnPointerUp_m1468,
	Selectable_OnPointerEnter_m1469,
	Selectable_OnPointerExit_m1470,
	Selectable_OnSelect_m1471,
	Selectable_OnDeselect_m1472,
	Selectable_Select_m1473,
	SetPropertyUtility_SetColor_m1474,
	SliderEvent__ctor_m1475,
	Slider__ctor_m1476,
	Slider_get_fillRect_m1477,
	Slider_set_fillRect_m1478,
	Slider_get_handleRect_m1479,
	Slider_set_handleRect_m1480,
	Slider_get_direction_m1481,
	Slider_set_direction_m1482,
	Slider_get_minValue_m1483,
	Slider_set_minValue_m1484,
	Slider_get_maxValue_m1485,
	Slider_set_maxValue_m1486,
	Slider_get_wholeNumbers_m1487,
	Slider_set_wholeNumbers_m1488,
	Slider_get_value_m1489,
	Slider_set_value_m1490,
	Slider_get_normalizedValue_m1491,
	Slider_set_normalizedValue_m1492,
	Slider_get_onValueChanged_m1493,
	Slider_set_onValueChanged_m1494,
	Slider_get_stepSize_m1495,
	Slider_Rebuild_m1496,
	Slider_LayoutComplete_m1497,
	Slider_GraphicUpdateComplete_m1498,
	Slider_OnEnable_m1499,
	Slider_OnDisable_m1500,
	Slider_OnDidApplyAnimationProperties_m1501,
	Slider_UpdateCachedReferences_m1502,
	Slider_ClampValue_m1503,
	Slider_Set_m1504,
	Slider_Set_m1505,
	Slider_OnRectTransformDimensionsChange_m1506,
	Slider_get_axis_m1507,
	Slider_get_reverseValue_m1508,
	Slider_UpdateVisuals_m1509,
	Slider_UpdateDrag_m1510,
	Slider_MayDrag_m1511,
	Slider_OnPointerDown_m1512,
	Slider_OnDrag_m1513,
	Slider_OnMove_m1514,
	Slider_FindSelectableOnLeft_m1515,
	Slider_FindSelectableOnRight_m1516,
	Slider_FindSelectableOnUp_m1517,
	Slider_FindSelectableOnDown_m1518,
	Slider_OnInitializePotentialDrag_m1519,
	Slider_SetDirection_m1520,
	Slider_UnityEngine_UI_ICanvasElement_IsDestroyed_m1521,
	Slider_UnityEngine_UI_ICanvasElement_get_transform_m1522,
	SpriteState_get_highlightedSprite_m1523,
	SpriteState_set_highlightedSprite_m1524,
	SpriteState_get_pressedSprite_m1525,
	SpriteState_set_pressedSprite_m1526,
	SpriteState_get_disabledSprite_m1527,
	SpriteState_set_disabledSprite_m1528,
	MatEntry__ctor_m1529,
	StencilMaterial__cctor_m1530,
	StencilMaterial_Add_m1531,
	StencilMaterial_Add_m1532,
	StencilMaterial_Add_m1533,
	StencilMaterial_Remove_m1534,
	StencilMaterial_ClearAll_m1535,
	Text__ctor_m1536,
	Text__cctor_m1537,
	Text_get_cachedTextGenerator_m1538,
	Text_get_cachedTextGeneratorForLayout_m1539,
	Text_get_mainTexture_m1540,
	Text_FontTextureChanged_m1541,
	Text_get_font_m1542,
	Text_set_font_m1543,
	Text_get_text_m1544,
	Text_set_text_m1545,
	Text_get_supportRichText_m1546,
	Text_set_supportRichText_m1547,
	Text_get_resizeTextForBestFit_m1548,
	Text_set_resizeTextForBestFit_m1549,
	Text_get_resizeTextMinSize_m1550,
	Text_set_resizeTextMinSize_m1551,
	Text_get_resizeTextMaxSize_m1552,
	Text_set_resizeTextMaxSize_m1553,
	Text_get_alignment_m1554,
	Text_set_alignment_m1555,
	Text_get_fontSize_m1556,
	Text_set_fontSize_m1557,
	Text_get_horizontalOverflow_m1558,
	Text_set_horizontalOverflow_m1559,
	Text_get_verticalOverflow_m1560,
	Text_set_verticalOverflow_m1561,
	Text_get_lineSpacing_m1562,
	Text_set_lineSpacing_m1563,
	Text_get_fontStyle_m1564,
	Text_set_fontStyle_m1565,
	Text_get_pixelsPerUnit_m1566,
	Text_OnEnable_m1567,
	Text_OnDisable_m1568,
	Text_UpdateGeometry_m1569,
	Text_GetGenerationSettings_m1570,
	Text_GetTextAnchorPivot_m1571,
	Text_OnPopulateMesh_m1572,
	Text_CalculateLayoutInputHorizontal_m1573,
	Text_CalculateLayoutInputVertical_m1574,
	Text_get_minWidth_m1575,
	Text_get_preferredWidth_m1576,
	Text_get_flexibleWidth_m1577,
	Text_get_minHeight_m1578,
	Text_get_preferredHeight_m1579,
	Text_get_flexibleHeight_m1580,
	Text_get_layoutPriority_m1581,
	ToggleEvent__ctor_m1582,
	Toggle__ctor_m1583,
	Toggle_get_group_m1584,
	Toggle_set_group_m1585,
	Toggle_Rebuild_m1586,
	Toggle_LayoutComplete_m1587,
	Toggle_GraphicUpdateComplete_m1588,
	Toggle_OnEnable_m1589,
	Toggle_OnDisable_m1590,
	Toggle_OnDidApplyAnimationProperties_m1591,
	Toggle_SetToggleGroup_m1592,
	Toggle_get_isOn_m1593,
	Toggle_set_isOn_m1594,
	Toggle_Set_m1595,
	Toggle_Set_m1596,
	Toggle_PlayEffect_m1597,
	Toggle_Start_m1598,
	Toggle_InternalToggle_m1599,
	Toggle_OnPointerClick_m1600,
	Toggle_OnSubmit_m1601,
	Toggle_UnityEngine_UI_ICanvasElement_IsDestroyed_m1602,
	Toggle_UnityEngine_UI_ICanvasElement_get_transform_m1603,
	ToggleGroup__ctor_m1604,
	ToggleGroup_get_allowSwitchOff_m1605,
	ToggleGroup_set_allowSwitchOff_m1606,
	ToggleGroup_ValidateToggleIsInGroup_m1607,
	ToggleGroup_NotifyToggleOn_m1608,
	ToggleGroup_UnregisterToggle_m1609,
	ToggleGroup_RegisterToggle_m1610,
	ToggleGroup_AnyTogglesOn_m1611,
	ToggleGroup_ActiveToggles_m1612,
	ToggleGroup_SetAllTogglesOff_m1613,
	ToggleGroup_U3CAnyTogglesOnU3Em__4_m1614,
	ToggleGroup_U3CActiveTogglesU3Em__5_m1615,
	ClipperRegistry__ctor_m1616,
	ClipperRegistry_get_instance_m1617,
	ClipperRegistry_Cull_m1618,
	ClipperRegistry_Register_m1619,
	ClipperRegistry_Unregister_m1620,
	Clipping_FindCullAndClipWorldRect_m1621,
	Clipping_RectIntersect_m1622,
	RectangularVertexClipper__ctor_m1623,
	RectangularVertexClipper_GetCanvasRect_m1624,
	AspectRatioFitter__ctor_m1625,
	AspectRatioFitter_get_aspectMode_m1626,
	AspectRatioFitter_set_aspectMode_m1627,
	AspectRatioFitter_get_aspectRatio_m1628,
	AspectRatioFitter_set_aspectRatio_m1629,
	AspectRatioFitter_get_rectTransform_m1630,
	AspectRatioFitter_OnEnable_m1631,
	AspectRatioFitter_OnDisable_m1632,
	AspectRatioFitter_OnRectTransformDimensionsChange_m1633,
	AspectRatioFitter_UpdateRect_m1634,
	AspectRatioFitter_GetSizeDeltaToProduceSize_m1635,
	AspectRatioFitter_GetParentSize_m1636,
	AspectRatioFitter_SetLayoutHorizontal_m1637,
	AspectRatioFitter_SetLayoutVertical_m1638,
	AspectRatioFitter_SetDirty_m1639,
	CanvasScaler__ctor_m1640,
	CanvasScaler_get_uiScaleMode_m1641,
	CanvasScaler_set_uiScaleMode_m1642,
	CanvasScaler_get_referencePixelsPerUnit_m1643,
	CanvasScaler_set_referencePixelsPerUnit_m1644,
	CanvasScaler_get_scaleFactor_m1645,
	CanvasScaler_set_scaleFactor_m1646,
	CanvasScaler_get_referenceResolution_m1647,
	CanvasScaler_set_referenceResolution_m1648,
	CanvasScaler_get_screenMatchMode_m1649,
	CanvasScaler_set_screenMatchMode_m1650,
	CanvasScaler_get_matchWidthOrHeight_m1651,
	CanvasScaler_set_matchWidthOrHeight_m1652,
	CanvasScaler_get_physicalUnit_m1653,
	CanvasScaler_set_physicalUnit_m1654,
	CanvasScaler_get_fallbackScreenDPI_m1655,
	CanvasScaler_set_fallbackScreenDPI_m1656,
	CanvasScaler_get_defaultSpriteDPI_m1657,
	CanvasScaler_set_defaultSpriteDPI_m1658,
	CanvasScaler_get_dynamicPixelsPerUnit_m1659,
	CanvasScaler_set_dynamicPixelsPerUnit_m1660,
	CanvasScaler_OnEnable_m1661,
	CanvasScaler_OnDisable_m1662,
	CanvasScaler_Update_m1663,
	CanvasScaler_Handle_m1664,
	CanvasScaler_HandleWorldCanvas_m1665,
	CanvasScaler_HandleConstantPixelSize_m1666,
	CanvasScaler_HandleScaleWithScreenSize_m1667,
	CanvasScaler_HandleConstantPhysicalSize_m1668,
	CanvasScaler_SetScaleFactor_m1669,
	CanvasScaler_SetReferencePixelsPerUnit_m1670,
	ContentSizeFitter__ctor_m1671,
	ContentSizeFitter_get_horizontalFit_m1672,
	ContentSizeFitter_set_horizontalFit_m1673,
	ContentSizeFitter_get_verticalFit_m1674,
	ContentSizeFitter_set_verticalFit_m1675,
	ContentSizeFitter_get_rectTransform_m1676,
	ContentSizeFitter_OnEnable_m1677,
	ContentSizeFitter_OnDisable_m1678,
	ContentSizeFitter_OnRectTransformDimensionsChange_m1679,
	ContentSizeFitter_HandleSelfFittingAlongAxis_m1680,
	ContentSizeFitter_SetLayoutHorizontal_m1681,
	ContentSizeFitter_SetLayoutVertical_m1682,
	ContentSizeFitter_SetDirty_m1683,
	GridLayoutGroup__ctor_m1684,
	GridLayoutGroup_get_startCorner_m1685,
	GridLayoutGroup_set_startCorner_m1686,
	GridLayoutGroup_get_startAxis_m1687,
	GridLayoutGroup_set_startAxis_m1688,
	GridLayoutGroup_get_cellSize_m1689,
	GridLayoutGroup_set_cellSize_m1690,
	GridLayoutGroup_get_spacing_m1691,
	GridLayoutGroup_set_spacing_m1692,
	GridLayoutGroup_get_constraint_m1693,
	GridLayoutGroup_set_constraint_m1694,
	GridLayoutGroup_get_constraintCount_m1695,
	GridLayoutGroup_set_constraintCount_m1696,
	GridLayoutGroup_CalculateLayoutInputHorizontal_m1697,
	GridLayoutGroup_CalculateLayoutInputVertical_m1698,
	GridLayoutGroup_SetLayoutHorizontal_m1699,
	GridLayoutGroup_SetLayoutVertical_m1700,
	GridLayoutGroup_SetCellsAlongAxis_m1701,
	HorizontalLayoutGroup__ctor_m1702,
	HorizontalLayoutGroup_CalculateLayoutInputHorizontal_m1703,
	HorizontalLayoutGroup_CalculateLayoutInputVertical_m1704,
	HorizontalLayoutGroup_SetLayoutHorizontal_m1705,
	HorizontalLayoutGroup_SetLayoutVertical_m1706,
	HorizontalOrVerticalLayoutGroup__ctor_m1707,
	HorizontalOrVerticalLayoutGroup_get_spacing_m1708,
	HorizontalOrVerticalLayoutGroup_set_spacing_m1709,
	HorizontalOrVerticalLayoutGroup_get_childForceExpandWidth_m1710,
	HorizontalOrVerticalLayoutGroup_set_childForceExpandWidth_m1711,
	HorizontalOrVerticalLayoutGroup_get_childForceExpandHeight_m1712,
	HorizontalOrVerticalLayoutGroup_set_childForceExpandHeight_m1713,
	HorizontalOrVerticalLayoutGroup_CalcAlongAxis_m1714,
	HorizontalOrVerticalLayoutGroup_SetChildrenAlongAxis_m1715,
	LayoutElement__ctor_m1716,
	LayoutElement_get_ignoreLayout_m1717,
	LayoutElement_set_ignoreLayout_m1718,
	LayoutElement_CalculateLayoutInputHorizontal_m1719,
	LayoutElement_CalculateLayoutInputVertical_m1720,
	LayoutElement_get_minWidth_m1721,
	LayoutElement_set_minWidth_m1722,
	LayoutElement_get_minHeight_m1723,
	LayoutElement_set_minHeight_m1724,
	LayoutElement_get_preferredWidth_m1725,
	LayoutElement_set_preferredWidth_m1726,
	LayoutElement_get_preferredHeight_m1727,
	LayoutElement_set_preferredHeight_m1728,
	LayoutElement_get_flexibleWidth_m1729,
	LayoutElement_set_flexibleWidth_m1730,
	LayoutElement_get_flexibleHeight_m1731,
	LayoutElement_set_flexibleHeight_m1732,
	LayoutElement_get_layoutPriority_m1733,
	LayoutElement_OnEnable_m1734,
	LayoutElement_OnTransformParentChanged_m1735,
	LayoutElement_OnDisable_m1736,
	LayoutElement_OnDidApplyAnimationProperties_m1737,
	LayoutElement_OnBeforeTransformParentChanged_m1738,
	LayoutElement_SetDirty_m1739,
	LayoutGroup__ctor_m1740,
	LayoutGroup_get_padding_m1741,
	LayoutGroup_set_padding_m1742,
	LayoutGroup_get_childAlignment_m1743,
	LayoutGroup_set_childAlignment_m1744,
	LayoutGroup_get_rectTransform_m1745,
	LayoutGroup_get_rectChildren_m1746,
	LayoutGroup_CalculateLayoutInputHorizontal_m1747,
	LayoutGroup_get_minWidth_m1748,
	LayoutGroup_get_preferredWidth_m1749,
	LayoutGroup_get_flexibleWidth_m1750,
	LayoutGroup_get_minHeight_m1751,
	LayoutGroup_get_preferredHeight_m1752,
	LayoutGroup_get_flexibleHeight_m1753,
	LayoutGroup_get_layoutPriority_m1754,
	LayoutGroup_OnEnable_m1755,
	LayoutGroup_OnDisable_m1756,
	LayoutGroup_OnDidApplyAnimationProperties_m1757,
	LayoutGroup_GetTotalMinSize_m1758,
	LayoutGroup_GetTotalPreferredSize_m1759,
	LayoutGroup_GetTotalFlexibleSize_m1760,
	LayoutGroup_GetStartOffset_m1761,
	LayoutGroup_SetLayoutInputForAxis_m1762,
	LayoutGroup_SetChildAlongAxis_m1763,
	LayoutGroup_get_isRootLayoutGroup_m1764,
	LayoutGroup_OnRectTransformDimensionsChange_m1765,
	LayoutGroup_OnTransformChildrenChanged_m1766,
	LayoutGroup_SetDirty_m1767,
	LayoutRebuilder__ctor_m1768,
	LayoutRebuilder__cctor_m1769,
	LayoutRebuilder_Initialize_m1770,
	LayoutRebuilder_Clear_m1771,
	LayoutRebuilder_ReapplyDrivenProperties_m1772,
	LayoutRebuilder_get_transform_m1773,
	LayoutRebuilder_IsDestroyed_m1774,
	LayoutRebuilder_StripDisabledBehavioursFromList_m1775,
	LayoutRebuilder_ForceRebuildLayoutImmediate_m1776,
	LayoutRebuilder_Rebuild_m1777,
	LayoutRebuilder_PerformLayoutControl_m1778,
	LayoutRebuilder_PerformLayoutCalculation_m1779,
	LayoutRebuilder_MarkLayoutForRebuild_m1780,
	LayoutRebuilder_ValidLayoutGroup_m1781,
	LayoutRebuilder_ValidController_m1782,
	LayoutRebuilder_MarkLayoutRootForRebuild_m1783,
	LayoutRebuilder_LayoutComplete_m1784,
	LayoutRebuilder_GraphicUpdateComplete_m1785,
	LayoutRebuilder_GetHashCode_m1786,
	LayoutRebuilder_Equals_m1787,
	LayoutRebuilder_ToString_m1788,
	LayoutRebuilder_U3Cs_RebuildersU3Em__6_m1789,
	LayoutRebuilder_U3CStripDisabledBehavioursFromListU3Em__7_m1790,
	LayoutRebuilder_U3CRebuildU3Em__8_m1791,
	LayoutRebuilder_U3CRebuildU3Em__9_m1792,
	LayoutRebuilder_U3CRebuildU3Em__A_m1793,
	LayoutRebuilder_U3CRebuildU3Em__B_m1794,
	LayoutUtility_GetMinSize_m1795,
	LayoutUtility_GetPreferredSize_m1796,
	LayoutUtility_GetFlexibleSize_m1797,
	LayoutUtility_GetMinWidth_m1798,
	LayoutUtility_GetPreferredWidth_m1799,
	LayoutUtility_GetFlexibleWidth_m1800,
	LayoutUtility_GetMinHeight_m1801,
	LayoutUtility_GetPreferredHeight_m1802,
	LayoutUtility_GetFlexibleHeight_m1803,
	LayoutUtility_GetLayoutProperty_m1804,
	LayoutUtility_GetLayoutProperty_m1805,
	LayoutUtility_U3CGetMinWidthU3Em__C_m1806,
	LayoutUtility_U3CGetPreferredWidthU3Em__D_m1807,
	LayoutUtility_U3CGetPreferredWidthU3Em__E_m1808,
	LayoutUtility_U3CGetFlexibleWidthU3Em__F_m1809,
	LayoutUtility_U3CGetMinHeightU3Em__10_m1810,
	LayoutUtility_U3CGetPreferredHeightU3Em__11_m1811,
	LayoutUtility_U3CGetPreferredHeightU3Em__12_m1812,
	LayoutUtility_U3CGetFlexibleHeightU3Em__13_m1813,
	VerticalLayoutGroup__ctor_m1814,
	VerticalLayoutGroup_CalculateLayoutInputHorizontal_m1815,
	VerticalLayoutGroup_CalculateLayoutInputVertical_m1816,
	VerticalLayoutGroup_SetLayoutHorizontal_m1817,
	VerticalLayoutGroup_SetLayoutVertical_m1818,
	VertexHelper__ctor_m1819,
	VertexHelper__ctor_m1820,
	VertexHelper__cctor_m1821,
	VertexHelper_Clear_m1822,
	VertexHelper_get_currentVertCount_m1823,
	VertexHelper_get_currentIndexCount_m1824,
	VertexHelper_PopulateUIVertex_m1825,
	VertexHelper_SetUIVertex_m1826,
	VertexHelper_FillMesh_m1827,
	VertexHelper_Dispose_m1828,
	VertexHelper_AddVert_m1829,
	VertexHelper_AddVert_m1830,
	VertexHelper_AddVert_m1831,
	VertexHelper_AddTriangle_m1832,
	VertexHelper_AddUIVertexQuad_m1833,
	VertexHelper_AddUIVertexStream_m1834,
	VertexHelper_AddUIVertexTriangleStream_m1835,
	VertexHelper_GetUIVertexStream_m1836,
	BaseVertexEffect__ctor_m1837,
	BaseMeshEffect__ctor_m1838,
	BaseMeshEffect_get_graphic_m1839,
	BaseMeshEffect_OnEnable_m1840,
	BaseMeshEffect_OnDisable_m1841,
	BaseMeshEffect_OnDidApplyAnimationProperties_m1842,
	BaseMeshEffect_ModifyMesh_m1843,
	Outline__ctor_m1844,
	Outline_ModifyMesh_m1845,
	PositionAsUV1__ctor_m1846,
	PositionAsUV1_ModifyMesh_m1847,
	Shadow__ctor_m1848,
	Shadow_get_effectColor_m1849,
	Shadow_set_effectColor_m1850,
	Shadow_get_effectDistance_m1851,
	Shadow_set_effectDistance_m1852,
	Shadow_get_useGraphicAlpha_m1853,
	Shadow_set_useGraphicAlpha_m1854,
	Shadow_ApplyShadowZeroAlloc_m1855,
	Shadow_ApplyShadow_m1856,
	Shadow_ModifyMesh_m1857,
	AssetBundleCreateRequest__ctor_m2447,
	AssetBundleCreateRequest_get_assetBundle_m2448,
	AssetBundleCreateRequest_DisableCompatibilityChecks_m2449,
	AssetBundleRequest__ctor_m2450,
	AssetBundleRequest_get_asset_m2451,
	AssetBundleRequest_get_allAssets_m2452,
	AssetBundle_LoadAsset_m2453,
	AssetBundle_LoadAsset_Internal_m2454,
	AssetBundle_LoadAssetWithSubAssets_Internal_m2455,
	WaitForSeconds__ctor_m451,
	WaitForFixedUpdate__ctor_m2456,
	WaitForEndOfFrame__ctor_m2284,
	Coroutine__ctor_m2457,
	Coroutine_ReleaseCoroutine_m2458,
	Coroutine_Finalize_m2459,
	ScriptableObject__ctor_m2460,
	ScriptableObject_Internal_CreateScriptableObject_m2461,
	ScriptableObject_CreateInstance_m2462,
	ScriptableObject_CreateInstance_m2463,
	ScriptableObject_CreateInstanceFromType_m2464,
	UnhandledExceptionHandler__ctor_m2465,
	UnhandledExceptionHandler_RegisterUECatcher_m2466,
	UnhandledExceptionHandler_HandleUnhandledException_m2467,
	UnhandledExceptionHandler_PrintException_m2468,
	UnhandledExceptionHandler_NativeUnhandledExceptionHandler_m2469,
	GameCenterPlatform__ctor_m2470,
	GameCenterPlatform__cctor_m2471,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_LoadFriends_m2472,
	GameCenterPlatform_UnityEngine_SocialPlatforms_ISocialPlatform_Authenticate_m2473,
	GameCenterPlatform_Internal_Authenticate_m2474,
	GameCenterPlatform_Internal_Authenticated_m2475,
	GameCenterPlatform_Internal_UserName_m2476,
	GameCenterPlatform_Internal_UserID_m2477,
	GameCenterPlatform_Internal_Underage_m2478,
	GameCenterPlatform_Internal_UserImage_m2479,
	GameCenterPlatform_Internal_LoadFriends_m2480,
	GameCenterPlatform_Internal_LoadAchievementDescriptions_m2481,
	GameCenterPlatform_Internal_LoadAchievements_m2482,
	GameCenterPlatform_Internal_ReportProgress_m2483,
	GameCenterPlatform_Internal_ReportScore_m2484,
	GameCenterPlatform_Internal_LoadScores_m2485,
	GameCenterPlatform_Internal_ShowAchievementsUI_m2486,
	GameCenterPlatform_Internal_ShowLeaderboardUI_m2487,
	GameCenterPlatform_Internal_LoadUsers_m2488,
	GameCenterPlatform_Internal_ResetAllAchievements_m2489,
	GameCenterPlatform_Internal_ShowDefaultAchievementBanner_m2490,
	GameCenterPlatform_ResetAllAchievements_m2491,
	GameCenterPlatform_ShowDefaultAchievementCompletionBanner_m2492,
	GameCenterPlatform_ShowLeaderboardUI_m2493,
	GameCenterPlatform_Internal_ShowSpecificLeaderboardUI_m2494,
	GameCenterPlatform_ClearAchievementDescriptions_m2495,
	GameCenterPlatform_SetAchievementDescription_m2496,
	GameCenterPlatform_SetAchievementDescriptionImage_m2497,
	GameCenterPlatform_TriggerAchievementDescriptionCallback_m2498,
	GameCenterPlatform_AuthenticateCallbackWrapper_m2499,
	GameCenterPlatform_ClearFriends_m2500,
	GameCenterPlatform_SetFriends_m2501,
	GameCenterPlatform_SetFriendImage_m2502,
	GameCenterPlatform_TriggerFriendsCallbackWrapper_m2503,
	GameCenterPlatform_AchievementCallbackWrapper_m2504,
	GameCenterPlatform_ProgressCallbackWrapper_m2505,
	GameCenterPlatform_ScoreCallbackWrapper_m2506,
	GameCenterPlatform_ScoreLoaderCallbackWrapper_m2507,
	GameCenterPlatform_get_localUser_m2508,
	GameCenterPlatform_PopulateLocalUser_m2509,
	GameCenterPlatform_LoadAchievementDescriptions_m2510,
	GameCenterPlatform_ReportProgress_m2511,
	GameCenterPlatform_LoadAchievements_m2512,
	GameCenterPlatform_ReportScore_m2513,
	GameCenterPlatform_LoadScores_m2514,
	GameCenterPlatform_LoadScores_m2515,
	GameCenterPlatform_LeaderboardCallbackWrapper_m2516,
	GameCenterPlatform_GetLoading_m2517,
	GameCenterPlatform_VerifyAuthentication_m2518,
	GameCenterPlatform_ShowAchievementsUI_m2519,
	GameCenterPlatform_ShowLeaderboardUI_m2520,
	GameCenterPlatform_ClearUsers_m2521,
	GameCenterPlatform_SetUser_m2522,
	GameCenterPlatform_SetUserImage_m2523,
	GameCenterPlatform_TriggerUsersCallbackWrapper_m2524,
	GameCenterPlatform_LoadUsers_m2525,
	GameCenterPlatform_SafeSetUserImage_m2526,
	GameCenterPlatform_SafeClearArray_m2527,
	GameCenterPlatform_CreateLeaderboard_m2528,
	GameCenterPlatform_CreateAchievement_m2529,
	GameCenterPlatform_TriggerResetAchievementCallback_m2530,
	GcLeaderboard__ctor_m2531,
	GcLeaderboard_Finalize_m2532,
	GcLeaderboard_Contains_m2533,
	GcLeaderboard_SetScores_m2534,
	GcLeaderboard_SetLocalScore_m2535,
	GcLeaderboard_SetMaxRange_m2536,
	GcLeaderboard_SetTitle_m2537,
	GcLeaderboard_Internal_LoadScores_m2538,
	GcLeaderboard_Internal_LoadScoresWithUsers_m2539,
	GcLeaderboard_Loading_m2540,
	GcLeaderboard_Dispose_m2541,
	Mesh__ctor_m2110,
	Mesh_Internal_Create_m2542,
	Mesh_Clear_m2543,
	Mesh_Clear_m2109,
	Mesh_get_vertices_m2412,
	Mesh_SetVertices_m2425,
	Mesh_SetVerticesInternal_m2544,
	Mesh_get_normals_m2419,
	Mesh_SetNormals_m2428,
	Mesh_SetNormalsInternal_m2545,
	Mesh_get_tangents_m2420,
	Mesh_SetTangents_m2429,
	Mesh_SetTangentsInternal_m2546,
	Mesh_get_uv_m2416,
	Mesh_get_uv2_m2418,
	Mesh_SetUVs_m2427,
	Mesh_SetUVInternal_m2547,
	Mesh_get_colors32_m2414,
	Mesh_SetColors_m2426,
	Mesh_SetColors32Internal_m2548,
	Mesh_RecalculateBounds_m2431,
	Mesh_SetTriangles_m2430,
	Mesh_SetTrianglesInternal_m2549,
	Mesh_GetIndices_m2422,
	BoneWeight_get_weight0_m2550,
	BoneWeight_set_weight0_m2551,
	BoneWeight_get_weight1_m2552,
	BoneWeight_set_weight1_m2553,
	BoneWeight_get_weight2_m2554,
	BoneWeight_set_weight2_m2555,
	BoneWeight_get_weight3_m2556,
	BoneWeight_set_weight3_m2557,
	BoneWeight_get_boneIndex0_m2558,
	BoneWeight_set_boneIndex0_m2559,
	BoneWeight_get_boneIndex1_m2560,
	BoneWeight_set_boneIndex1_m2561,
	BoneWeight_get_boneIndex2_m2562,
	BoneWeight_set_boneIndex2_m2563,
	BoneWeight_get_boneIndex3_m2564,
	BoneWeight_set_boneIndex3_m2565,
	BoneWeight_GetHashCode_m2566,
	BoneWeight_Equals_m2567,
	BoneWeight_op_Equality_m2568,
	BoneWeight_op_Inequality_m2569,
	Renderer_set_enabled_m265,
	Renderer_get_sortingLayerID_m1968,
	Renderer_get_sortingOrder_m1969,
	Graphics_DrawTexture_m2570,
	Screen_get_width_m249,
	Screen_get_height_m2134,
	Screen_get_dpi_m2372,
	GUILayer_HitTest_m2571,
	GUILayer_INTERNAL_CALL_HitTest_m2572,
	Texture__ctor_m2573,
	Texture_Internal_GetWidth_m2574,
	Texture_Internal_GetHeight_m2575,
	Texture_get_width_m2576,
	Texture_get_height_m2577,
	Texture2D__ctor_m2578,
	Texture2D_Internal_Create_m2579,
	Texture2D_get_whiteTexture_m2101,
	Texture2D_GetPixelBilinear_m2178,
	RenderTexture_Internal_GetWidth_m2580,
	RenderTexture_Internal_GetHeight_m2581,
	RenderTexture_get_width_m2582,
	RenderTexture_get_height_m2583,
	StateChanged__ctor_m2584,
	StateChanged_Invoke_m2585,
	StateChanged_BeginInvoke_m2586,
	StateChanged_EndInvoke_m2587,
	CullingGroup_Finalize_m2588,
	CullingGroup_Dispose_m2589,
	CullingGroup_SendEvents_m2590,
	CullingGroup_FinalizerFailure_m2591,
	GradientColorKey__ctor_m2592,
	GradientAlphaKey__ctor_m2593,
	Gradient__ctor_m2594,
	Gradient_Init_m2595,
	Gradient_Cleanup_m2596,
	Gradient_Finalize_m2597,
	TouchScreenKeyboard__ctor_m2598,
	TouchScreenKeyboard_Destroy_m2599,
	TouchScreenKeyboard_Finalize_m2600,
	TouchScreenKeyboard_TouchScreenKeyboard_InternalConstructorHelper_m2601,
	TouchScreenKeyboard_get_isSupported_m2206,
	TouchScreenKeyboard_Open_m2262,
	TouchScreenKeyboard_Open_m2263,
	TouchScreenKeyboard_Open_m2602,
	TouchScreenKeyboard_get_text_m2188,
	TouchScreenKeyboard_set_text_m2189,
	TouchScreenKeyboard_set_hideInput_m2261,
	TouchScreenKeyboard_get_active_m2187,
	TouchScreenKeyboard_set_active_m2260,
	TouchScreenKeyboard_get_done_m2211,
	TouchScreenKeyboard_get_wasCanceled_m2207,
	LayerMask_get_value_m2603,
	LayerMask_set_value_m2604,
	LayerMask_LayerToName_m2605,
	LayerMask_NameToLayer_m2606,
	LayerMask_GetMask_m2607,
	LayerMask_op_Implicit_m1972,
	LayerMask_op_Implicit_m1970,
	Vector2__ctor_m257,
	Vector2_get_Item_m2171,
	Vector2_set_Item_m2179,
	Vector2_Scale_m2250,
	Vector2_Normalize_m2608,
	Vector2_get_normalized_m258,
	Vector2_ToString_m2609,
	Vector2_GetHashCode_m2610,
	Vector2_Equals_m2611,
	Vector2_Dot_m1940,
	Vector2_get_magnitude_m2612,
	Vector2_get_sqrMagnitude_m1904,
	Vector2_SqrMagnitude_m2613,
	Vector2_get_zero_m1900,
	Vector2_get_one_m2009,
	Vector2_get_up_m2030,
	Vector2_get_right_m2024,
	Vector2_op_Addition_m2055,
	Vector2_op_Subtraction_m1915,
	Vector2_op_Multiply_m2168,
	Vector2_op_Division_m2219,
	Vector2_op_Equality_m2444,
	Vector2_op_Inequality_m2249,
	Vector2_op_Implicit_m1918,
	Vector2_op_Implicit_m253,
	Vector3__ctor_m230,
	Vector3__ctor_m2116,
	Vector3_Lerp_m2297,
	Vector3_SmoothDamp_m445,
	Vector3_SmoothDamp_m2614,
	Vector3_get_Item_m2301,
	Vector3_set_Item_m2302,
	Vector3_GetHashCode_m2615,
	Vector3_Equals_m2616,
	Vector3_Normalize_m2617,
	Vector3_get_normalized_m251,
	Vector3_ToString_m2618,
	Vector3_ToString_m2619,
	Vector3_Dot_m2141,
	Vector3_Angle_m272,
	Vector3_Distance_m1965,
	Vector3_ClampMagnitude_m2620,
	Vector3_Magnitude_m2621,
	Vector3_get_magnitude_m271,
	Vector3_SqrMagnitude_m2622,
	Vector3_get_sqrMagnitude_m289,
	Vector3_Min_m2309,
	Vector3_Max_m2310,
	Vector3_get_zero_m227,
	Vector3_get_one_m2068,
	Vector3_get_forward_m2140,
	Vector3_get_back_m2424,
	Vector3_get_up_m319,
	Vector3_get_down_m2328,
	Vector3_get_left_m2326,
	Vector3_get_right_m2327,
	Vector3_op_Addition_m279,
	Vector3_op_Subtraction_m225,
	Vector3_op_UnaryNegation_m323,
	Vector3_op_Multiply_m278,
	Vector3_op_Multiply_m312,
	Vector3_op_Division_m226,
	Vector3_op_Equality_m317,
	Vector3_op_Inequality_m247,
	Color__ctor_m1998,
	Color__ctor_m2001,
	Color_ToString_m2623,
	Color_GetHashCode_m2624,
	Color_Equals_m2123,
	Color_Lerp_m1980,
	Color_get_red_m2625,
	Color_get_white_m421,
	Color_get_black_m2127,
	Color_get_clear_m2075,
	Color_op_Multiply_m2323,
	Color_op_Implicit_m2626,
	Color32__ctor_m1996,
	Color32_ToString_m2627,
	Color32_op_Implicit_m2115,
	Color32_op_Implicit_m1997,
	Quaternion__ctor_m2628,
	Quaternion_Dot_m2629,
	Quaternion_AngleAxis_m320,
	Quaternion_INTERNAL_CALL_AngleAxis_m2630,
	Quaternion_LookRotation_m327,
	Quaternion_INTERNAL_CALL_LookRotation_m2631,
	Quaternion_Inverse_m2324,
	Quaternion_INTERNAL_CALL_Inverse_m2632,
	Quaternion_ToString_m2633,
	Quaternion_get_eulerAngles_m2634,
	Quaternion_Euler_m328,
	Quaternion_Internal_ToEulerRad_m2635,
	Quaternion_INTERNAL_CALL_Internal_ToEulerRad_m2636,
	Quaternion_Internal_FromEulerRad_m2637,
	Quaternion_INTERNAL_CALL_Internal_FromEulerRad_m2638,
	Quaternion_GetHashCode_m2639,
	Quaternion_Equals_m2640,
	Quaternion_op_Multiply_m329,
	Quaternion_op_Multiply_m321,
	Quaternion_op_Inequality_m2247,
	Rect__ctor_m422,
	Rect_get_x_m2112,
	Rect_set_x_m429,
	Rect_get_y_m2113,
	Rect_set_y_m430,
	Rect_get_position_m2169,
	Rect_get_center_m2290,
	Rect_get_min_m2054,
	Rect_get_max_m2056,
	Rect_get_width_m2107,
	Rect_set_width_m431,
	Rect_get_height_m2060,
	Rect_set_height_m432,
	Rect_get_size_m2057,
	Rect_get_xMin_m2183,
	Rect_get_yMin_m2182,
	Rect_get_xMax_m2176,
	Rect_get_yMax_m2177,
	Rect_ToString_m2641,
	Rect_Contains_m2062,
	Rect_Overlaps_m2272,
	Rect_GetHashCode_m2642,
	Rect_Equals_m2643,
	Rect_op_Inequality_m2283,
	Rect_op_Equality_m2280,
	Matrix4x4_get_Item_m2644,
	Matrix4x4_set_Item_m2645,
	Matrix4x4_get_Item_m2646,
	Matrix4x4_set_Item_m2647,
	Matrix4x4_GetHashCode_m2648,
	Matrix4x4_Equals_m2649,
	Matrix4x4_Inverse_m2650,
	Matrix4x4_INTERNAL_CALL_Inverse_m2651,
	Matrix4x4_Transpose_m2652,
	Matrix4x4_INTERNAL_CALL_Transpose_m2653,
	Matrix4x4_Invert_m2654,
	Matrix4x4_INTERNAL_CALL_Invert_m2655,
	Matrix4x4_get_inverse_m2656,
	Matrix4x4_get_transpose_m2657,
	Matrix4x4_get_isIdentity_m2658,
	Matrix4x4_GetColumn_m2659,
	Matrix4x4_GetRow_m2660,
	Matrix4x4_SetColumn_m2661,
	Matrix4x4_SetRow_m2662,
	Matrix4x4_MultiplyPoint_m2663,
	Matrix4x4_MultiplyPoint3x4_m2308,
	Matrix4x4_MultiplyVector_m2664,
	Matrix4x4_Scale_m2665,
	Matrix4x4_get_zero_m2666,
	Matrix4x4_get_identity_m2667,
	Matrix4x4_SetTRS_m2668,
	Matrix4x4_TRS_m2669,
	Matrix4x4_INTERNAL_CALL_TRS_m2670,
	Matrix4x4_ToString_m2671,
	Matrix4x4_ToString_m2672,
	Matrix4x4_Ortho_m2673,
	Matrix4x4_Perspective_m2674,
	Matrix4x4_op_Multiply_m2675,
	Matrix4x4_op_Multiply_m2676,
	Matrix4x4_op_Equality_m2677,
	Matrix4x4_op_Inequality_m2678,
	Bounds__ctor_m2303,
	Bounds_GetHashCode_m2679,
	Bounds_Equals_m2680,
	Bounds_get_center_m2304,
	Bounds_set_center_m2306,
	Bounds_get_size_m2295,
	Bounds_set_size_m2305,
	Bounds_get_extents_m2681,
	Bounds_set_extents_m2682,
	Bounds_get_min_m2300,
	Bounds_set_min_m2683,
	Bounds_get_max_m2312,
	Bounds_set_max_m2684,
	Bounds_SetMinMax_m2685,
	Bounds_Encapsulate_m2311,
	Bounds_Encapsulate_m2686,
	Bounds_Expand_m2687,
	Bounds_Expand_m2688,
	Bounds_Intersects_m2689,
	Bounds_Internal_Contains_m2690,
	Bounds_INTERNAL_CALL_Internal_Contains_m2691,
	Bounds_Contains_m2692,
	Bounds_Internal_SqrDistance_m2693,
	Bounds_INTERNAL_CALL_Internal_SqrDistance_m2694,
	Bounds_SqrDistance_m2695,
	Bounds_Internal_IntersectRay_m2696,
	Bounds_INTERNAL_CALL_Internal_IntersectRay_m2697,
	Bounds_IntersectRay_m2698,
	Bounds_IntersectRay_m2699,
	Bounds_Internal_GetClosestPoint_m2700,
	Bounds_INTERNAL_CALL_Internal_GetClosestPoint_m2701,
	Bounds_ClosestPoint_m2702,
	Bounds_ToString_m2703,
	Bounds_ToString_m2704,
	Bounds_op_Equality_m2705,
	Bounds_op_Inequality_m2298,
	Vector4__ctor_m2114,
	Vector4_get_Item_m2170,
	Vector4_set_Item_m2172,
	Vector4_GetHashCode_m2706,
	Vector4_Equals_m2707,
	Vector4_ToString_m2708,
	Vector4_Dot_m2709,
	Vector4_SqrMagnitude_m2710,
	Vector4_get_sqrMagnitude_m2158,
	Vector4_get_zero_m2161,
	Vector4_op_Subtraction_m2711,
	Vector4_op_Division_m2167,
	Vector4_op_Equality_m2712,
	Ray__ctor_m2713,
	Ray_get_origin_m1959,
	Ray_get_direction_m1960,
	Ray_GetPoint_m2214,
	Ray_ToString_m2714,
	Plane__ctor_m2212,
	Plane_get_normal_m2715,
	Plane_get_distance_m2716,
	Plane_Raycast_m2213,
	MathfInternal__cctor_m2717,
	Mathf__cctor_m2718,
	Mathf_Sin_m2719,
	Mathf_Cos_m2720,
	Mathf_Acos_m2721,
	Mathf_Atan2_m2722,
	Mathf_Sqrt_m2723,
	Mathf_Abs_m2724,
	Mathf_Min_m290,
	Mathf_Min_m2231,
	Mathf_Max_m274,
	Mathf_Max_m2229,
	Mathf_Pow_m2725,
	Mathf_Log_m2371,
	Mathf_Floor_m2726,
	Mathf_Round_m2727,
	Mathf_CeilToInt_m2381,
	Mathf_FloorToInt_m2383,
	Mathf_RoundToInt_m2164,
	Mathf_Sign_m283,
	Mathf_Clamp_m281,
	Mathf_Clamp_m229,
	Mathf_Clamp01_m2152,
	Mathf_Lerp_m275,
	Mathf_MoveTowards_m219,
	Mathf_Approximately_m1936,
	Mathf_SmoothDamp_m448,
	Mathf_SmoothDamp_m2296,
	Mathf_Repeat_m2180,
	Mathf_InverseLerp_m248,
	Mathf_PerlinNoise_m280,
	DrivenRectTransformTracker_Add_m2289,
	DrivenRectTransformTracker_Clear_m2288,
	ReapplyDrivenProperties__ctor_m2397,
	ReapplyDrivenProperties_Invoke_m2728,
	ReapplyDrivenProperties_BeginInvoke_m2729,
	ReapplyDrivenProperties_EndInvoke_m2730,
	RectTransform_add_reapplyDrivenProperties_m2398,
	RectTransform_remove_reapplyDrivenProperties_m2731,
	RectTransform_get_rect_m2053,
	RectTransform_INTERNAL_get_rect_m2732,
	RectTransform_get_anchorMin_m2064,
	RectTransform_set_anchorMin_m2008,
	RectTransform_INTERNAL_get_anchorMin_m2733,
	RectTransform_INTERNAL_set_anchorMin_m2734,
	RectTransform_get_anchorMax_m2065,
	RectTransform_set_anchorMax_m2010,
	RectTransform_INTERNAL_get_anchorMax_m2735,
	RectTransform_INTERNAL_set_anchorMax_m2736,
	RectTransform_get_anchoredPosition_m2066,
	RectTransform_set_anchoredPosition_m2011,
	RectTransform_INTERNAL_get_anchoredPosition_m2737,
	RectTransform_INTERNAL_set_anchoredPosition_m2738,
	RectTransform_get_sizeDelta_m2026,
	RectTransform_set_sizeDelta_m2000,
	RectTransform_INTERNAL_get_sizeDelta_m2739,
	RectTransform_INTERNAL_set_sizeDelta_m2740,
	RectTransform_get_pivot_m2067,
	RectTransform_set_pivot_m2025,
	RectTransform_INTERNAL_get_pivot_m2741,
	RectTransform_INTERNAL_set_pivot_m2742,
	RectTransform_SendReapplyDrivenProperties_m2743,
	RectTransform_GetLocalCorners_m2744,
	RectTransform_GetWorldCorners_m2061,
	RectTransform_set_offsetMin_m2019,
	RectTransform_set_offsetMax_m2020,
	RectTransform_SetInsetAndSizeFromParentEdge_m2393,
	RectTransform_SetSizeWithCurrentAnchors_m2369,
	RectTransform_GetParentSize_m2745,
	ResourceRequest__ctor_m2746,
	ResourceRequest_get_asset_m2747,
	Resources_Load_m423,
	Resources_Load_m2748,
	SerializePrivateVariables__ctor_m2749,
	SerializeField__ctor_m2750,
	Shader_PropertyToID_m2751,
	Material__ctor_m2348,
	Material_get_mainTexture_m2155,
	Material_GetTexture_m2752,
	Material_GetTexture_m2753,
	Material_SetFloat_m2754,
	Material_SetFloat_m2755,
	Material_SetInt_m2350,
	Material_HasProperty_m2346,
	Material_HasProperty_m2756,
	Material_Internal_CreateWithMaterial_m2757,
	SortingLayer_GetLayerValueFromID_m1866,
	SphericalHarmonicsL2_Clear_m2758,
	SphericalHarmonicsL2_ClearInternal_m2759,
	SphericalHarmonicsL2_INTERNAL_CALL_ClearInternal_m2760,
	SphericalHarmonicsL2_AddAmbientLight_m2761,
	SphericalHarmonicsL2_AddAmbientLightInternal_m2762,
	SphericalHarmonicsL2_INTERNAL_CALL_AddAmbientLightInternal_m2763,
	SphericalHarmonicsL2_AddDirectionalLight_m2764,
	SphericalHarmonicsL2_AddDirectionalLightInternal_m2765,
	SphericalHarmonicsL2_INTERNAL_CALL_AddDirectionalLightInternal_m2766,
	SphericalHarmonicsL2_get_Item_m2767,
	SphericalHarmonicsL2_set_Item_m2768,
	SphericalHarmonicsL2_GetHashCode_m2769,
	SphericalHarmonicsL2_Equals_m2770,
	SphericalHarmonicsL2_op_Multiply_m2771,
	SphericalHarmonicsL2_op_Multiply_m2772,
	SphericalHarmonicsL2_op_Addition_m2773,
	SphericalHarmonicsL2_op_Equality_m2774,
	SphericalHarmonicsL2_op_Inequality_m2775,
	Sprite_get_rect_m2163,
	Sprite_INTERNAL_get_rect_m2776,
	Sprite_get_pixelsPerUnit_m2159,
	Sprite_get_texture_m2156,
	Sprite_get_textureRect_m2175,
	Sprite_INTERNAL_get_textureRect_m2777,
	Sprite_get_border_m2157,
	Sprite_INTERNAL_get_border_m2778,
	DataUtility_GetInnerUV_m2166,
	DataUtility_GetOuterUV_m2165,
	DataUtility_GetPadding_m2162,
	DataUtility_GetMinSize_m2173,
	DataUtility_Internal_GetMinSize_m2779,
	UnityString_Format_m2780,
	AsyncOperation__ctor_m2781,
	AsyncOperation_InternalDestroy_m2782,
	AsyncOperation_Finalize_m2783,
	LogCallback__ctor_m2784,
	LogCallback_Invoke_m2785,
	LogCallback_BeginInvoke_m2786,
	LogCallback_EndInvoke_m2787,
	Application_Quit_m454,
	Application_LoadLevel_m453,
	Application_LoadLevelAsync_m2788,
	Application_get_isPlaying_m2035,
	Application_get_isEditor_m2279,
	Application_get_platform_m2186,
	Application_CallLogCallback_m2789,
	Behaviour__ctor_m2790,
	Behaviour_get_enabled_m1909,
	Behaviour_set_enabled_m2039,
	Behaviour_get_isActiveAndEnabled_m1902,
	CameraCallback__ctor_m2791,
	CameraCallback_Invoke_m2792,
	CameraCallback_BeginInvoke_m2793,
	CameraCallback_EndInvoke_m2794,
	Camera_get_nearClipPlane_m1958,
	Camera_get_farClipPlane_m1957,
	Camera_get_orthographicSize_m447,
	Camera_set_orthographicSize_m449,
	Camera_get_depth_m1864,
	Camera_get_aspect_m450,
	Camera_get_cullingMask_m1971,
	Camera_get_eventMask_m2795,
	Camera_get_pixelRect_m2796,
	Camera_INTERNAL_get_pixelRect_m2797,
	Camera_get_targetTexture_m2798,
	Camera_get_pixelWidth_m426,
	Camera_get_pixelHeight_m427,
	Camera_get_clearFlags_m2799,
	Camera_ScreenToViewportPoint_m2135,
	Camera_INTERNAL_CALL_ScreenToViewportPoint_m2800,
	Camera_ScreenPointToRay_m1956,
	Camera_INTERNAL_CALL_ScreenPointToRay_m2801,
	Camera_get_main_m288,
	Camera_get_current_m428,
	Camera_get_allCamerasCount_m2802,
	Camera_GetAllCameras_m2803,
	Camera_FireOnPreCull_m2804,
	Camera_FireOnPreRender_m2805,
	Camera_FireOnPostRender_m2806,
	Camera_RaycastTry_m2807,
	Camera_INTERNAL_CALL_RaycastTry_m2808,
	Camera_RaycastTry2D_m2809,
	Camera_INTERNAL_CALL_RaycastTry2D_m2810,
	Debug_Internal_Log_m2811,
	Debug_Internal_LogException_m2812,
	Debug_Log_m2813,
	Debug_LogError_m263,
	Debug_LogError_m2040,
	Debug_LogException_m2814,
	Debug_LogException_m1994,
	Debug_LogWarning_m341,
	Debug_LogWarning_m2347,
	DisplaysUpdatedDelegate__ctor_m2815,
	DisplaysUpdatedDelegate_Invoke_m2816,
	DisplaysUpdatedDelegate_BeginInvoke_m2817,
	DisplaysUpdatedDelegate_EndInvoke_m2818,
	Display__ctor_m2819,
	Display__ctor_m2820,
	Display__cctor_m2821,
	Display_add_onDisplaysUpdated_m2822,
	Display_remove_onDisplaysUpdated_m2823,
	Display_get_renderingWidth_m2824,
	Display_get_renderingHeight_m2825,
	Display_get_systemWidth_m2826,
	Display_get_systemHeight_m2827,
	Display_get_colorBuffer_m2828,
	Display_get_depthBuffer_m2829,
	Display_Activate_m2830,
	Display_Activate_m2831,
	Display_SetParams_m2832,
	Display_SetRenderingResolution_m2833,
	Display_MultiDisplayLicense_m2834,
	Display_RelativeMouseAt_m2835,
	Display_get_main_m2836,
	Display_RecreateDisplayList_m2837,
	Display_FireDisplaysUpdated_m2838,
	Display_GetSystemExtImpl_m2839,
	Display_GetRenderingExtImpl_m2840,
	Display_GetRenderingBuffersImpl_m2841,
	Display_SetRenderingResolutionImpl_m2842,
	Display_ActivateDisplayImpl_m2843,
	Display_SetParamsImpl_m2844,
	Display_MultiDisplayLicenseImpl_m2845,
	Display_RelativeMouseAtImpl_m2846,
	MonoBehaviour__ctor_m212,
	MonoBehaviour_StartCoroutine_m346,
	MonoBehaviour_StartCoroutine_Auto_m2847,
	MonoBehaviour_StopCoroutine_m2848,
	MonoBehaviour_StopCoroutine_m2285,
	MonoBehaviour_StopCoroutineViaEnumerator_Auto_m2849,
	MonoBehaviour_StopCoroutine_Auto_m2850,
	Touch_get_fingerId_m1913,
	Touch_get_position_m256,
	Touch_get_phase_m1914,
	Input__cctor_m2851,
	Input_GetKeyUpInt_m2852,
	Input_GetKeyUpString_m2853,
	Input_GetKeyDownInt_m2854,
	Input_GetAxis_m240,
	Input_GetAxisRaw_m239,
	Input_GetButton_m241,
	Input_GetButtonDown_m242,
	Input_GetButtonUp_m243,
	Input_GetKeyDown_m437,
	Input_GetKeyUp_m436,
	Input_GetKeyUp_m438,
	Input_GetMouseButton_m439,
	Input_GetMouseButtonDown_m1916,
	Input_GetMouseButtonUp_m1917,
	Input_get_mousePosition_m245,
	Input_INTERNAL_get_mousePosition_m2855,
	Input_get_mouseScrollDelta_m1919,
	Input_INTERNAL_get_mouseScrollDelta_m2856,
	Input_get_mousePresent_m1935,
	Input_get_acceleration_m246,
	Input_INTERNAL_get_acceleration_m2857,
	Input_get_touches_m255,
	Input_GetTouch_m1953,
	Input_get_touchCount_m254,
	Input_get_touchSupported_m1952,
	Input_set_imeCompositionMode_m2264,
	Input_get_compositionString_m2202,
	Input_set_compositionCursorPos_m2252,
	Input_INTERNAL_set_compositionCursorPos_m2858,
	Object__ctor_m2859,
	Object_Internal_CloneSingle_m2860,
	Object_Destroy_m332,
	Object_Destroy_m287,
	Object_DestroyImmediate_m2861,
	Object_DestroyImmediate_m2203,
	Object_FindObjectsOfType_m214,
	Object_get_name_m2241,
	Object_set_name_m2022,
	Object_set_hideFlags_m2111,
	Object_ToString_m2862,
	Object_Equals_m2863,
	Object_GetHashCode_m2864,
	Object_CompareBaseObjects_m2865,
	Object_IsNativeObjectAlive_m2866,
	Object_GetInstanceID_m2867,
	Object_GetCachedPtr_m2868,
	Object_CheckNullArgument_m2869,
	Object_FindObjectOfType_m2870,
	Object_op_Implicit_m424,
	Object_op_Equality_m217,
	Object_op_Inequality_m216,
	Component__ctor_m2871,
	Component_get_transform_m223,
	Component_get_gameObject_m237,
	Component_GetComponent_m2394,
	Component_GetComponentFastPath_m2872,
	Component_GetComponentInChildren_m2873,
	Component_GetComponentInParent_m2874,
	Component_GetComponentsForListInternal_m2875,
	Component_GetComponents_m2099,
	GameObject__ctor_m233,
	GameObject_GetComponent_m2876,
	GameObject_GetComponentFastPath_m2877,
	GameObject_GetComponentInChildren_m2878,
	GameObject_GetComponentInParent_m2879,
	GameObject_GetComponentsInternal_m2880,
	GameObject_get_transform_m303,
	GameObject_get_layer_m2003,
	GameObject_set_layer_m2004,
	GameObject_SetActive_m238,
	GameObject_get_activeSelf_m446,
	GameObject_get_activeInHierarchy_m1910,
	GameObject_get_tag_m442,
	GameObject_SendMessage_m2881,
	GameObject_Internal_AddComponentWithType_m2882,
	GameObject_AddComponent_m2883,
	GameObject_Internal_CreateGameObject_m2884,
	Enumerator__ctor_m2885,
	Enumerator_get_Current_m2886,
	Enumerator_MoveNext_m2887,
	Enumerator_Reset_m2888,
	Transform_get_position_m224,
	Transform_set_position_m231,
	Transform_INTERNAL_get_position_m2889,
	Transform_INTERNAL_set_position_m2890,
	Transform_get_localPosition_m334,
	Transform_set_localPosition_m335,
	Transform_INTERNAL_get_localPosition_m2891,
	Transform_INTERNAL_set_localPosition_m2892,
	Transform_get_eulerAngles_m318,
	Transform_get_right_m277,
	Transform_get_up_m322,
	Transform_get_forward_m269,
	Transform_get_rotation_m2139,
	Transform_set_rotation_m309,
	Transform_INTERNAL_get_rotation_m2893,
	Transform_INTERNAL_set_rotation_m2894,
	Transform_get_localRotation_m305,
	Transform_set_localRotation_m330,
	Transform_INTERNAL_get_localRotation_m2895,
	Transform_INTERNAL_set_localRotation_m2896,
	Transform_get_localScale_m2248,
	Transform_set_localScale_m452,
	Transform_INTERNAL_get_localScale_m2897,
	Transform_INTERNAL_set_localScale_m2898,
	Transform_get_parent_m331,
	Transform_set_parent_m337,
	Transform_get_parentInternal_m2899,
	Transform_set_parentInternal_m2900,
	Transform_SetParent_m2242,
	Transform_SetParent_m2002,
	Transform_get_worldToLocalMatrix_m2307,
	Transform_INTERNAL_get_worldToLocalMatrix_m2901,
	Transform_Rotate_m2902,
	Transform_Rotate_m304,
	Transform_Rotate_m2903,
	Transform_TransformPoint_m2325,
	Transform_INTERNAL_CALL_TransformPoint_m2904,
	Transform_InverseTransformPoint_m282,
	Transform_INTERNAL_CALL_InverseTransformPoint_m2905,
	Transform_get_root_m339,
	Transform_get_childCount_m2006,
	Transform_SetAsFirstSibling_m2243,
	Transform_IsChildOf_m2042,
	Transform_GetEnumerator_m2906,
	Transform_GetChild_m2005,
	Time_get_time_m276,
	Time_get_deltaTime_m218,
	Time_get_unscaledTime_m1939,
	Time_get_unscaledDeltaTime_m1987,
	Time_get_timeScale_m455,
	Time_set_timeScale_m456,
	Time_get_frameCount_m221,
	Random_Range_m298,
	Random_get_value_m267,
	YieldInstruction__ctor_m2907,
	UnityAdsInternal__ctor_m2908,
	UnityAdsInternal_add_onCampaignsAvailable_m2909,
	UnityAdsInternal_remove_onCampaignsAvailable_m2910,
	UnityAdsInternal_add_onCampaignsFetchFailed_m2911,
	UnityAdsInternal_remove_onCampaignsFetchFailed_m2912,
	UnityAdsInternal_add_onShow_m2913,
	UnityAdsInternal_remove_onShow_m2914,
	UnityAdsInternal_add_onHide_m2915,
	UnityAdsInternal_remove_onHide_m2916,
	UnityAdsInternal_add_onVideoCompleted_m2917,
	UnityAdsInternal_remove_onVideoCompleted_m2918,
	UnityAdsInternal_add_onVideoStarted_m2919,
	UnityAdsInternal_remove_onVideoStarted_m2920,
	UnityAdsInternal_RegisterNative_m2921,
	UnityAdsInternal_Init_m2922,
	UnityAdsInternal_Show_m2923,
	UnityAdsInternal_CanShowAds_m2924,
	UnityAdsInternal_SetLogLevel_m2925,
	UnityAdsInternal_SetCampaignDataURL_m2926,
	UnityAdsInternal_RemoveAllEventHandlers_m2927,
	UnityAdsInternal_CallUnityAdsCampaignsAvailable_m2928,
	UnityAdsInternal_CallUnityAdsCampaignsFetchFailed_m2929,
	UnityAdsInternal_CallUnityAdsShow_m2930,
	UnityAdsInternal_CallUnityAdsHide_m2931,
	UnityAdsInternal_CallUnityAdsVideoCompleted_m2932,
	UnityAdsInternal_CallUnityAdsVideoStarted_m2933,
	ParticleSystem_Internal_Stop_m2934,
	ParticleSystem_Stop_m342,
	ParticleSystem_Stop_m2935,
	ParticleSystem_Emit_m345,
	ParticleSystem_INTERNAL_CALL_Emit_m2936,
	ParticleSystem_GetParticleSystems_m2937,
	ParticleSystem_GetDirectParticleSystemChildrenRecursive_m2938,
	Particle_get_position_m2939,
	Particle_set_position_m2940,
	Particle_get_velocity_m2941,
	Particle_set_velocity_m2942,
	Particle_get_energy_m2943,
	Particle_set_energy_m2944,
	Particle_get_startEnergy_m2945,
	Particle_set_startEnergy_m2946,
	Particle_get_size_m2947,
	Particle_set_size_m2948,
	Particle_get_rotation_m2949,
	Particle_set_rotation_m2950,
	Particle_get_angularVelocity_m2951,
	Particle_set_angularVelocity_m2952,
	Particle_get_color_m2953,
	Particle_set_color_m2954,
	Collision_get_rigidbody_m284,
	Physics_Raycast_m2955,
	Physics_Raycast_m2136,
	Physics_Raycast_m2956,
	Physics_RaycastAll_m1973,
	Physics_RaycastAll_m2957,
	Physics_RaycastAll_m2958,
	Physics_INTERNAL_CALL_RaycastAll_m2959,
	Physics_Linecast_m440,
	Physics_Linecast_m2960,
	Physics_Internal_Raycast_m2961,
	Physics_INTERNAL_CALL_Internal_Raycast_m2962,
	WheelHit_get_normal_m316,
	WheelHit_get_forwardSlip_m325,
	WheelHit_get_sidewaysSlip_m326,
	Rigidbody_get_velocity_m270,
	Rigidbody_set_velocity_m313,
	Rigidbody_INTERNAL_get_velocity_m2963,
	Rigidbody_INTERNAL_set_velocity_m2964,
	Rigidbody_get_angularVelocity_m273,
	Rigidbody_INTERNAL_get_angularVelocity_m2965,
	Rigidbody_set_freezeRotation_m435,
	Rigidbody_AddForce_m324,
	Rigidbody_INTERNAL_CALL_AddForce_m2966,
	Rigidbody_set_centerOfMass_m307,
	Rigidbody_INTERNAL_set_centerOfMass_m2967,
	Collider_get_attachedRigidbody_m306,
	WheelCollider_get_radius_m338,
	WheelCollider_set_motorTorque_m314,
	WheelCollider_set_brakeTorque_m311,
	WheelCollider_set_steerAngle_m310,
	WheelCollider_GetGroundHit_m315,
	WheelCollider_GetWorldPose_m308,
	RaycastHit_get_point_m1977,
	RaycastHit_get_normal_m1978,
	RaycastHit_get_distance_m441,
	RaycastHit_get_collider_m1976,
	Physics2D__cctor_m2968,
	Physics2D_Internal_Raycast_m2969,
	Physics2D_INTERNAL_CALL_Internal_Raycast_m2970,
	Physics2D_Raycast_m2137,
	Physics2D_Raycast_m2971,
	Physics2D_RaycastAll_m1961,
	Physics2D_INTERNAL_CALL_RaycastAll_m2972,
	RaycastHit2D_get_point_m1966,
	RaycastHit2D_get_normal_m1967,
	RaycastHit2D_get_fraction_m2138,
	RaycastHit2D_get_collider_m1962,
	RaycastHit2D_get_rigidbody_m2973,
	RaycastHit2D_get_transform_m1964,
	Collider2D_get_attachedRigidbody_m2974,
	AudioConfigurationChangeHandler__ctor_m2975,
	AudioConfigurationChangeHandler_Invoke_m2976,
	AudioConfigurationChangeHandler_BeginInvoke_m2977,
	AudioConfigurationChangeHandler_EndInvoke_m2978,
	AudioSettings_InvokeOnAudioConfigurationChanged_m2979,
	PCMReaderCallback__ctor_m2980,
	PCMReaderCallback_Invoke_m2981,
	PCMReaderCallback_BeginInvoke_m2982,
	PCMReaderCallback_EndInvoke_m2983,
	PCMSetPositionCallback__ctor_m2984,
	PCMSetPositionCallback_Invoke_m2985,
	PCMSetPositionCallback_BeginInvoke_m2986,
	PCMSetPositionCallback_EndInvoke_m2987,
	AudioClip_get_length_m297,
	AudioClip_InvokePCMReaderCallback_Internal_m2988,
	AudioClip_InvokePCMSetPositionCallback_Internal_m2989,
	AudioSource_set_volume_m293,
	AudioSource_set_pitch_m291,
	AudioSource_set_time_m299,
	AudioSource_set_clip_m295,
	AudioSource_Play_m2990,
	AudioSource_Play_m300,
	AudioSource_Stop_m347,
	AudioSource_set_loop_m296,
	AudioSource_set_dopplerLevel_m292,
	AudioSource_set_minDistance_m301,
	AudioSource_set_maxDistance_m302,
	WebCamDevice_get_name_m2991,
	WebCamDevice_get_isFrontFacing_m2992,
	AnimationEvent__ctor_m2993,
	AnimationEvent_get_data_m2994,
	AnimationEvent_set_data_m2995,
	AnimationEvent_get_stringParameter_m2996,
	AnimationEvent_set_stringParameter_m2997,
	AnimationEvent_get_floatParameter_m2998,
	AnimationEvent_set_floatParameter_m2999,
	AnimationEvent_get_intParameter_m3000,
	AnimationEvent_set_intParameter_m3001,
	AnimationEvent_get_objectReferenceParameter_m3002,
	AnimationEvent_set_objectReferenceParameter_m3003,
	AnimationEvent_get_functionName_m3004,
	AnimationEvent_set_functionName_m3005,
	AnimationEvent_get_time_m3006,
	AnimationEvent_set_time_m3007,
	AnimationEvent_get_messageOptions_m3008,
	AnimationEvent_set_messageOptions_m3009,
	AnimationEvent_get_isFiredByLegacy_m3010,
	AnimationEvent_get_isFiredByAnimator_m3011,
	AnimationEvent_get_animationState_m3012,
	AnimationEvent_get_animatorStateInfo_m3013,
	AnimationEvent_get_animatorClipInfo_m3014,
	AnimationEvent_GetHash_m3015,
	AnimationCurve__ctor_m3016,
	AnimationCurve__ctor_m3017,
	AnimationCurve_Cleanup_m3018,
	AnimationCurve_Finalize_m3019,
	AnimationCurve_Init_m3020,
	AnimatorStateInfo_IsName_m3021,
	AnimatorStateInfo_get_fullPathHash_m3022,
	AnimatorStateInfo_get_nameHash_m3023,
	AnimatorStateInfo_get_shortNameHash_m3024,
	AnimatorStateInfo_get_normalizedTime_m3025,
	AnimatorStateInfo_get_length_m3026,
	AnimatorStateInfo_get_speed_m3027,
	AnimatorStateInfo_get_speedMultiplier_m3028,
	AnimatorStateInfo_get_tagHash_m3029,
	AnimatorStateInfo_IsTag_m3030,
	AnimatorStateInfo_get_loop_m3031,
	AnimatorTransitionInfo_IsName_m3032,
	AnimatorTransitionInfo_IsUserName_m3033,
	AnimatorTransitionInfo_get_fullPathHash_m3034,
	AnimatorTransitionInfo_get_nameHash_m3035,
	AnimatorTransitionInfo_get_userNameHash_m3036,
	AnimatorTransitionInfo_get_normalizedTime_m3037,
	AnimatorTransitionInfo_get_anyState_m3038,
	AnimatorTransitionInfo_get_entry_m3039,
	AnimatorTransitionInfo_get_exit_m3040,
	Animator_SetTrigger_m2331,
	Animator_ResetTrigger_m2330,
	Animator_get_runtimeAnimatorController_m2329,
	Animator_StringToHash_m3041,
	Animator_SetTriggerString_m3042,
	Animator_ResetTriggerString_m3043,
	HumanBone_get_boneName_m3044,
	HumanBone_set_boneName_m3045,
	HumanBone_get_humanName_m3046,
	HumanBone_set_humanName_m3047,
	CharacterInfo_get_advance_m3048,
	CharacterInfo_set_advance_m3049,
	CharacterInfo_get_glyphWidth_m3050,
	CharacterInfo_set_glyphWidth_m3051,
	CharacterInfo_get_glyphHeight_m3052,
	CharacterInfo_set_glyphHeight_m3053,
	CharacterInfo_get_bearing_m3054,
	CharacterInfo_set_bearing_m3055,
	CharacterInfo_get_minY_m3056,
	CharacterInfo_set_minY_m3057,
	CharacterInfo_get_maxY_m3058,
	CharacterInfo_set_maxY_m3059,
	CharacterInfo_get_minX_m3060,
	CharacterInfo_set_minX_m3061,
	CharacterInfo_get_maxX_m3062,
	CharacterInfo_set_maxX_m3063,
	CharacterInfo_get_uvBottomLeftUnFlipped_m3064,
	CharacterInfo_set_uvBottomLeftUnFlipped_m3065,
	CharacterInfo_get_uvBottomRightUnFlipped_m3066,
	CharacterInfo_set_uvBottomRightUnFlipped_m3067,
	CharacterInfo_get_uvTopRightUnFlipped_m3068,
	CharacterInfo_set_uvTopRightUnFlipped_m3069,
	CharacterInfo_get_uvTopLeftUnFlipped_m3070,
	CharacterInfo_set_uvTopLeftUnFlipped_m3071,
	CharacterInfo_get_uvBottomLeft_m3072,
	CharacterInfo_set_uvBottomLeft_m3073,
	CharacterInfo_get_uvBottomRight_m3074,
	CharacterInfo_set_uvBottomRight_m3075,
	CharacterInfo_get_uvTopRight_m3076,
	CharacterInfo_set_uvTopRight_m3077,
	CharacterInfo_get_uvTopLeft_m3078,
	CharacterInfo_set_uvTopLeft_m3079,
	FontTextureRebuildCallback__ctor_m3080,
	FontTextureRebuildCallback_Invoke_m3081,
	FontTextureRebuildCallback_BeginInvoke_m3082,
	FontTextureRebuildCallback_EndInvoke_m3083,
	Font__ctor_m3084,
	Font__ctor_m3085,
	Font__ctor_m3086,
	Font_add_textureRebuilt_m2090,
	Font_remove_textureRebuilt_m3087,
	Font_add_m_FontTextureRebuildCallback_m3088,
	Font_remove_m_FontTextureRebuildCallback_m3089,
	Font_GetOSInstalledFontNames_m3090,
	Font_Internal_CreateFont_m3091,
	Font_Internal_CreateDynamicFont_m3092,
	Font_CreateDynamicFontFromOSFont_m3093,
	Font_CreateDynamicFontFromOSFont_m3094,
	Font_get_material_m2352,
	Font_set_material_m3095,
	Font_HasCharacter_m2223,
	Font_get_fontNames_m3096,
	Font_set_fontNames_m3097,
	Font_get_characterInfo_m3098,
	Font_set_characterInfo_m3099,
	Font_RequestCharactersInTexture_m3100,
	Font_RequestCharactersInTexture_m3101,
	Font_RequestCharactersInTexture_m3102,
	Font_InvokeTextureRebuilt_Internal_m3103,
	Font_get_textureRebuildCallback_m3104,
	Font_set_textureRebuildCallback_m3105,
	Font_GetMaxVertsForString_m3106,
	Font_GetCharacterInfo_m3107,
	Font_GetCharacterInfo_m3108,
	Font_GetCharacterInfo_m3109,
	Font_get_dynamic_m2354,
	Font_get_ascent_m3110,
	Font_get_lineHeight_m3111,
	Font_get_fontSize_m2356,
	TextGenerator__ctor_m2185,
	TextGenerator__ctor_m2351,
	TextGenerator_System_IDisposable_Dispose_m3112,
	TextGenerator_Init_m3113,
	TextGenerator_Dispose_cpp_m3114,
	TextGenerator_Populate_Internal_m3115,
	TextGenerator_Populate_Internal_cpp_m3116,
	TextGenerator_INTERNAL_CALL_Populate_Internal_cpp_m3117,
	TextGenerator_get_rectExtents_m2240,
	TextGenerator_INTERNAL_get_rectExtents_m3118,
	TextGenerator_get_vertexCount_m3119,
	TextGenerator_GetVerticesInternal_m3120,
	TextGenerator_GetVerticesArray_m3121,
	TextGenerator_get_characterCount_m3122,
	TextGenerator_get_characterCountVisible_m2217,
	TextGenerator_GetCharactersInternal_m3123,
	TextGenerator_GetCharactersArray_m3124,
	TextGenerator_get_lineCount_m2216,
	TextGenerator_GetLinesInternal_m3125,
	TextGenerator_GetLinesArray_m3126,
	TextGenerator_get_fontSizeUsedForBestFit_m2251,
	TextGenerator_Finalize_m3127,
	TextGenerator_ValidatedSettings_m3128,
	TextGenerator_Invalidate_m2353,
	TextGenerator_GetCharacters_m3129,
	TextGenerator_GetLines_m3130,
	TextGenerator_GetVertices_m3131,
	TextGenerator_GetPreferredWidth_m2358,
	TextGenerator_GetPreferredHeight_m2359,
	TextGenerator_Populate_m2239,
	TextGenerator_PopulateAlways_m3132,
	TextGenerator_get_verts_m2357,
	TextGenerator_get_characters_m2218,
	TextGenerator_get_lines_m2215,
	WillRenderCanvases__ctor_m1990,
	WillRenderCanvases_Invoke_m3133,
	WillRenderCanvases_BeginInvoke_m3134,
	WillRenderCanvases_EndInvoke_m3135,
	Canvas_add_willRenderCanvases_m1991,
	Canvas_remove_willRenderCanvases_m3136,
	Canvas_get_renderMode_m2131,
	Canvas_get_isRootCanvas_m2370,
	Canvas_get_worldCamera_m2142,
	Canvas_get_scaleFactor_m2355,
	Canvas_set_scaleFactor_m2373,
	Canvas_get_referencePixelsPerUnit_m2160,
	Canvas_set_referencePixelsPerUnit_m2374,
	Canvas_get_pixelPerfect_m2119,
	Canvas_get_renderOrder_m2132,
	Canvas_get_overrideSorting_m2278,
	Canvas_set_overrideSorting_m2045,
	Canvas_get_sortingOrder_m2073,
	Canvas_set_sortingOrder_m2046,
	Canvas_get_sortingLayerID_m2071,
	Canvas_set_sortingLayerID_m2072,
	Canvas_GetDefaultCanvasMaterial_m2093,
	Canvas_SendWillRenderCanvases_m3137,
	Canvas_ForceUpdateCanvases_m2294,
	CanvasGroup_get_alpha_m2082,
	CanvasGroup_set_alpha_m2086,
	CanvasGroup_get_interactable_m2322,
	CanvasGroup_get_blocksRaycasts_m3138,
	CanvasGroup_get_ignoreParentGroups_m2118,
	CanvasGroup_IsRaycastLocationValid_m3139,
	UIVertex__cctor_m3140,
	CanvasRenderer_SetColor_m2124,
	CanvasRenderer_INTERNAL_CALL_SetColor_m3141,
	CanvasRenderer_GetColor_m2122,
	CanvasRenderer_EnableRectClipping_m2275,
	CanvasRenderer_INTERNAL_CALL_EnableRectClipping_m3142,
	CanvasRenderer_DisableRectClipping_m2276,
	CanvasRenderer_set_hasPopInstruction_m2266,
	CanvasRenderer_get_materialCount_m3143,
	CanvasRenderer_set_materialCount_m2104,
	CanvasRenderer_SetMaterial_m2105,
	CanvasRenderer_SetMaterial_m2245,
	CanvasRenderer_set_popMaterialCount_m2267,
	CanvasRenderer_SetPopMaterial_m2268,
	CanvasRenderer_SetTexture_m2106,
	CanvasRenderer_SetMesh_m2108,
	CanvasRenderer_Clear_m2102,
	CanvasRenderer_SplitUIVertexStreams_m2438,
	CanvasRenderer_SplitUIVertexStreamsInternal_m3144,
	CanvasRenderer_SplitIndiciesStreamsInternal_m3145,
	CanvasRenderer_CreateUIVertexStream_m2439,
	CanvasRenderer_CreateUIVertexStreamInternal_m3146,
	CanvasRenderer_AddUIVertexStream_m2437,
	CanvasRenderer_get_cull_m2103,
	CanvasRenderer_set_cull_m2273,
	CanvasRenderer_get_absoluteDepth_m2095,
	CanvasRenderer_get_hasMoved_m2271,
	RectTransformUtility__cctor_m3147,
	RectTransformUtility_RectangleContainsScreenPoint_m2143,
	RectTransformUtility_INTERNAL_CALL_RectangleContainsScreenPoint_m3148,
	RectTransformUtility_PixelAdjustPoint_m2120,
	RectTransformUtility_PixelAdjustPoint_m3149,
	RectTransformUtility_INTERNAL_CALL_PixelAdjustPoint_m3150,
	RectTransformUtility_PixelAdjustRect_m2121,
	RectTransformUtility_ScreenPointToWorldPointInRectangle_m3151,
	RectTransformUtility_ScreenPointToLocalPointInRectangle_m2174,
	RectTransformUtility_ScreenPointToRay_m3152,
	RectTransformUtility_FlipLayoutOnAxis_m2063,
	RectTransformUtility_FlipLayoutAxes_m2291,
	RectTransformUtility_GetTransposed_m3153,
	Event__ctor_m2184,
	Event__ctor_m3154,
	Event__ctor_m3155,
	Event_Finalize_m3156,
	Event_get_mousePosition_m3157,
	Event_set_mousePosition_m3158,
	Event_get_delta_m3159,
	Event_set_delta_m3160,
	Event_get_mouseRay_m3161,
	Event_set_mouseRay_m3162,
	Event_get_shift_m3163,
	Event_set_shift_m3164,
	Event_get_control_m3165,
	Event_set_control_m3166,
	Event_get_alt_m3167,
	Event_set_alt_m3168,
	Event_get_command_m3169,
	Event_set_command_m3170,
	Event_get_capsLock_m3171,
	Event_set_capsLock_m3172,
	Event_get_numeric_m3173,
	Event_set_numeric_m3174,
	Event_get_functionKey_m3175,
	Event_get_current_m3176,
	Event_set_current_m3177,
	Event_Internal_MakeMasterEventCurrent_m3178,
	Event_get_isKey_m3179,
	Event_get_isMouse_m3180,
	Event_KeyboardEvent_m3181,
	Event_GetHashCode_m3182,
	Event_Equals_m3183,
	Event_ToString_m3184,
	Event_Init_m3185,
	Event_Cleanup_m3186,
	Event_InitCopy_m3187,
	Event_InitPtr_m3188,
	Event_get_rawType_m2224,
	Event_get_type_m2225,
	Event_set_type_m3189,
	Event_GetTypeForControl_m3190,
	Event_Internal_SetMousePosition_m3191,
	Event_INTERNAL_CALL_Internal_SetMousePosition_m3192,
	Event_Internal_GetMousePosition_m3193,
	Event_Internal_SetMouseDelta_m3194,
	Event_INTERNAL_CALL_Internal_SetMouseDelta_m3195,
	Event_Internal_GetMouseDelta_m3196,
	Event_get_button_m3197,
	Event_set_button_m3198,
	Event_get_modifiers_m2220,
	Event_set_modifiers_m3199,
	Event_get_pressure_m3200,
	Event_set_pressure_m3201,
	Event_get_clickCount_m3202,
	Event_set_clickCount_m3203,
	Event_get_character_m2222,
	Event_set_character_m3204,
	Event_get_commandName_m2226,
	Event_set_commandName_m3205,
	Event_get_keyCode_m2221,
	Event_set_keyCode_m3206,
	Event_Internal_SetNativeEvent_m3207,
	Event_Use_m3208,
	Event_PopEvent_m2227,
	Event_GetEventCount_m3209,
	ScrollViewState__ctor_m3210,
	WindowFunction__ctor_m3211,
	WindowFunction_Invoke_m3212,
	WindowFunction_BeginInvoke_m3213,
	WindowFunction_EndInvoke_m3214,
	GUI__cctor_m3215,
	GUI_set_nextScrollStepTime_m3216,
	GUI_set_skin_m3217,
	GUI_get_skin_m3218,
	GUI_DoSetSkin_m3219,
	GUI_DrawTexture_m434,
	GUI_DrawTexture_m3220,
	GUI_DrawTexture_m3221,
	GUI_DrawTexture_m3222,
	GUI_CallWindowDelegate_m3223,
	GUI_get_color_m3224,
	GUI_set_color_m433,
	GUI_INTERNAL_get_color_m3225,
	GUI_INTERNAL_set_color_m3226,
	GUI_set_changed_m3227,
	GUI_get_blendMaterial_m3228,
	GUI_get_blitMaterial_m3229,
	GUIContent__ctor_m3230,
	GUIContent__ctor_m3231,
	GUIContent__cctor_m3232,
	GUIContent_ClearStaticCache_m3233,
	GUILayout_Width_m3234,
	GUILayout_Height_m3235,
	LayoutCache__ctor_m3236,
	GUILayoutUtility__cctor_m3237,
	GUILayoutUtility_SelectIDList_m3238,
	GUILayoutUtility_Begin_m3239,
	GUILayoutUtility_BeginWindow_m3240,
	GUILayoutUtility_Layout_m3241,
	GUILayoutUtility_LayoutFromEditorWindow_m3242,
	GUILayoutUtility_LayoutFreeGroup_m3243,
	GUILayoutUtility_LayoutSingleGroup_m3244,
	GUILayoutUtility_get_spaceStyle_m3245,
	GUILayoutUtility_Internal_GetWindowRect_m3246,
	GUILayoutUtility_Internal_MoveWindow_m3247,
	GUILayoutUtility_INTERNAL_CALL_Internal_MoveWindow_m3248,
	GUILayoutEntry__ctor_m3249,
	GUILayoutEntry__cctor_m3250,
	GUILayoutEntry_get_style_m3251,
	GUILayoutEntry_set_style_m3252,
	GUILayoutEntry_get_margin_m3253,
	GUILayoutEntry_CalcWidth_m3254,
	GUILayoutEntry_CalcHeight_m3255,
	GUILayoutEntry_SetHorizontal_m3256,
	GUILayoutEntry_SetVertical_m3257,
	GUILayoutEntry_ApplyStyleSettings_m3258,
	GUILayoutEntry_ApplyOptions_m3259,
	GUILayoutEntry_ToString_m3260,
	GUILayoutGroup__ctor_m3261,
	GUILayoutGroup_get_margin_m3262,
	GUILayoutGroup_ApplyOptions_m3263,
	GUILayoutGroup_ApplyStyleSettings_m3264,
	GUILayoutGroup_ResetCursor_m3265,
	GUILayoutGroup_CalcWidth_m3266,
	GUILayoutGroup_SetHorizontal_m3267,
	GUILayoutGroup_CalcHeight_m3268,
	GUILayoutGroup_SetVertical_m3269,
	GUILayoutGroup_ToString_m3270,
	GUIScrollGroup__ctor_m3271,
	GUIScrollGroup_CalcWidth_m3272,
	GUIScrollGroup_SetHorizontal_m3273,
	GUIScrollGroup_CalcHeight_m3274,
	GUIScrollGroup_SetVertical_m3275,
	GUILayoutOption__ctor_m3276,
	GUISettings__ctor_m3277,
	SkinChangedDelegate__ctor_m3278,
	SkinChangedDelegate_Invoke_m3279,
	SkinChangedDelegate_BeginInvoke_m3280,
	SkinChangedDelegate_EndInvoke_m3281,
	GUISkin__ctor_m3282,
	GUISkin_OnEnable_m3283,
	GUISkin_get_font_m3284,
	GUISkin_set_font_m3285,
	GUISkin_get_box_m3286,
	GUISkin_set_box_m3287,
	GUISkin_get_label_m3288,
	GUISkin_set_label_m3289,
	GUISkin_get_textField_m3290,
	GUISkin_set_textField_m3291,
	GUISkin_get_textArea_m3292,
	GUISkin_set_textArea_m3293,
	GUISkin_get_button_m3294,
	GUISkin_set_button_m3295,
	GUISkin_get_toggle_m3296,
	GUISkin_set_toggle_m3297,
	GUISkin_get_window_m3298,
	GUISkin_set_window_m3299,
	GUISkin_get_horizontalSlider_m3300,
	GUISkin_set_horizontalSlider_m3301,
	GUISkin_get_horizontalSliderThumb_m3302,
	GUISkin_set_horizontalSliderThumb_m3303,
	GUISkin_get_verticalSlider_m3304,
	GUISkin_set_verticalSlider_m3305,
	GUISkin_get_verticalSliderThumb_m3306,
	GUISkin_set_verticalSliderThumb_m3307,
	GUISkin_get_horizontalScrollbar_m3308,
	GUISkin_set_horizontalScrollbar_m3309,
	GUISkin_get_horizontalScrollbarThumb_m3310,
	GUISkin_set_horizontalScrollbarThumb_m3311,
	GUISkin_get_horizontalScrollbarLeftButton_m3312,
	GUISkin_set_horizontalScrollbarLeftButton_m3313,
	GUISkin_get_horizontalScrollbarRightButton_m3314,
	GUISkin_set_horizontalScrollbarRightButton_m3315,
	GUISkin_get_verticalScrollbar_m3316,
	GUISkin_set_verticalScrollbar_m3317,
	GUISkin_get_verticalScrollbarThumb_m3318,
	GUISkin_set_verticalScrollbarThumb_m3319,
	GUISkin_get_verticalScrollbarUpButton_m3320,
	GUISkin_set_verticalScrollbarUpButton_m3321,
	GUISkin_get_verticalScrollbarDownButton_m3322,
	GUISkin_set_verticalScrollbarDownButton_m3323,
	GUISkin_get_scrollView_m3324,
	GUISkin_set_scrollView_m3325,
	GUISkin_get_customStyles_m3326,
	GUISkin_set_customStyles_m3327,
	GUISkin_get_settings_m3328,
	GUISkin_get_error_m3329,
	GUISkin_Apply_m3330,
	GUISkin_BuildStyleCache_m3331,
	GUISkin_GetStyle_m3332,
	GUISkin_FindStyle_m3333,
	GUISkin_MakeCurrent_m3334,
	GUISkin_GetEnumerator_m3335,
	GUIStyleState__ctor_m3336,
	GUIStyleState__ctor_m3337,
	GUIStyleState_Finalize_m3338,
	GUIStyleState_Init_m3339,
	GUIStyleState_Cleanup_m3340,
	GUIStyleState_GetBackgroundInternal_m3341,
	GUIStyleState_set_textColor_m3342,
	GUIStyleState_INTERNAL_set_textColor_m3343,
	RectOffset__ctor_m2389,
	RectOffset__ctor_m3344,
	RectOffset_Finalize_m3345,
	RectOffset_ToString_m3346,
	RectOffset_Init_m3347,
	RectOffset_Cleanup_m3348,
	RectOffset_get_left_m2387,
	RectOffset_set_left_m3349,
	RectOffset_get_right_m3350,
	RectOffset_set_right_m3351,
	RectOffset_get_top_m2388,
	RectOffset_set_top_m3352,
	RectOffset_get_bottom_m3353,
	RectOffset_set_bottom_m3354,
	RectOffset_get_horizontal_m2382,
	RectOffset_get_vertical_m2384,
	GUIStyle__ctor_m3355,
	GUIStyle__cctor_m3356,
	GUIStyle_Finalize_m3357,
	GUIStyle_get_normal_m3358,
	GUIStyle_get_margin_m3359,
	GUIStyle_get_padding_m3360,
	GUIStyle_get_none_m3361,
	GUIStyle_ToString_m3362,
	GUIStyle_Init_m3363,
	GUIStyle_Cleanup_m3364,
	GUIStyle_get_name_m3365,
	GUIStyle_set_name_m3366,
	GUIStyle_GetStyleStatePtr_m3367,
	GUIStyle_GetRectOffsetPtr_m3368,
	GUIStyle_get_fixedWidth_m3369,
	GUIStyle_get_fixedHeight_m3370,
	GUIStyle_get_stretchWidth_m3371,
	GUIStyle_set_stretchWidth_m3372,
	GUIStyle_get_stretchHeight_m3373,
	GUIStyle_set_stretchHeight_m3374,
	GUIStyle_SetDefaultFont_m3375,
	GUIUtility__cctor_m3376,
	GUIUtility_get_pixelsPerPoint_m3377,
	GUIUtility_GetDefaultSkin_m3378,
	GUIUtility_BeginGUI_m3379,
	GUIUtility_EndGUI_m3380,
	GUIUtility_EndGUIFromException_m3381,
	GUIUtility_CheckOnGUI_m3382,
	GUIUtility_Internal_GetPixelsPerPoint_m3383,
	GUIUtility_get_systemCopyBuffer_m2204,
	GUIUtility_set_systemCopyBuffer_m2205,
	GUIUtility_Internal_GetDefaultSkin_m3384,
	GUIUtility_Internal_ExitGUI_m3385,
	GUIUtility_Internal_GetGUIDepth_m3386,
	WrapperlessIcall__ctor_m3387,
	IL2CPPStructAlignmentAttribute__ctor_m3388,
	AttributeHelperEngine__cctor_m3389,
	AttributeHelperEngine_GetParentTypeDisallowingMultipleInclusion_m3390,
	AttributeHelperEngine_GetRequiredComponents_m3391,
	AttributeHelperEngine_CheckIsEditorScript_m3392,
	DisallowMultipleComponent__ctor_m3393,
	RequireComponent__ctor_m3394,
	AddComponentMenu__ctor_m3395,
	AddComponentMenu__ctor_m3396,
	ExecuteInEditMode__ctor_m3397,
	SetupCoroutine__ctor_m3398,
	SetupCoroutine_InvokeMember_m3399,
	SetupCoroutine_InvokeStatic_m3400,
	WritableAttribute__ctor_m3401,
	AssemblyIsEditorAssembly__ctor_m3402,
	GcUserProfileData_ToUserProfile_m3403,
	GcUserProfileData_AddToArray_m3404,
	GcAchievementDescriptionData_ToAchievementDescription_m3405,
	GcAchievementData_ToAchievement_m3406,
	GcScoreData_ToScore_m3407,
	Resolution_get_width_m3408,
	Resolution_set_width_m3409,
	Resolution_get_height_m3410,
	Resolution_set_height_m3411,
	Resolution_get_refreshRate_m3412,
	Resolution_set_refreshRate_m3413,
	Resolution_ToString_m3414,
	LocalUser__ctor_m3415,
	LocalUser_SetFriends_m3416,
	LocalUser_SetAuthenticated_m3417,
	LocalUser_SetUnderage_m3418,
	LocalUser_get_authenticated_m3419,
	UserProfile__ctor_m3420,
	UserProfile__ctor_m3421,
	UserProfile_ToString_m3422,
	UserProfile_SetUserName_m3423,
	UserProfile_SetUserID_m3424,
	UserProfile_SetImage_m3425,
	UserProfile_get_userName_m3426,
	UserProfile_get_id_m3427,
	UserProfile_get_isFriend_m3428,
	UserProfile_get_state_m3429,
	Achievement__ctor_m3430,
	Achievement__ctor_m3431,
	Achievement__ctor_m3432,
	Achievement_ToString_m3433,
	Achievement_get_id_m3434,
	Achievement_set_id_m3435,
	Achievement_get_percentCompleted_m3436,
	Achievement_set_percentCompleted_m3437,
	Achievement_get_completed_m3438,
	Achievement_get_hidden_m3439,
	Achievement_get_lastReportedDate_m3440,
	AchievementDescription__ctor_m3441,
	AchievementDescription_ToString_m3442,
	AchievementDescription_SetImage_m3443,
	AchievementDescription_get_id_m3444,
	AchievementDescription_set_id_m3445,
	AchievementDescription_get_title_m3446,
	AchievementDescription_get_achievedDescription_m3447,
	AchievementDescription_get_unachievedDescription_m3448,
	AchievementDescription_get_hidden_m3449,
	AchievementDescription_get_points_m3450,
	Score__ctor_m3451,
	Score__ctor_m3452,
	Score_ToString_m3453,
	Score_get_leaderboardID_m3454,
	Score_set_leaderboardID_m3455,
	Score_get_value_m3456,
	Score_set_value_m3457,
	Leaderboard__ctor_m3458,
	Leaderboard_ToString_m3459,
	Leaderboard_SetLocalUserScore_m3460,
	Leaderboard_SetMaxRange_m3461,
	Leaderboard_SetScores_m3462,
	Leaderboard_SetTitle_m3463,
	Leaderboard_GetUserFilter_m3464,
	Leaderboard_get_id_m3465,
	Leaderboard_set_id_m3466,
	Leaderboard_get_userScope_m3467,
	Leaderboard_set_userScope_m3468,
	Leaderboard_get_range_m3469,
	Leaderboard_set_range_m3470,
	Leaderboard_get_timeScope_m3471,
	Leaderboard_set_timeScope_m3472,
	HitInfo_SendMessage_m3473,
	HitInfo_Compare_m3474,
	HitInfo_op_Implicit_m3475,
	SendMouseEvents__cctor_m3476,
	SendMouseEvents_SetMouseMoved_m3477,
	SendMouseEvents_DoSendMouseEvents_m3478,
	SendMouseEvents_SendEvents_m3479,
	Range__ctor_m3480,
	PropertyAttribute__ctor_m3481,
	TooltipAttribute__ctor_m3482,
	SpaceAttribute__ctor_m3483,
	SpaceAttribute__ctor_m3484,
	RangeAttribute__ctor_m3485,
	TextAreaAttribute__ctor_m3486,
	SelectionBaseAttribute__ctor_m3487,
	SliderState__ctor_m3488,
	StackTraceUtility__ctor_m3489,
	StackTraceUtility__cctor_m3490,
	StackTraceUtility_SetProjectFolder_m3491,
	StackTraceUtility_ExtractStackTrace_m3492,
	StackTraceUtility_IsSystemStacktraceType_m3493,
	StackTraceUtility_ExtractStringFromException_m3494,
	StackTraceUtility_ExtractStringFromExceptionInternal_m3495,
	StackTraceUtility_PostprocessStacktrace_m3496,
	StackTraceUtility_ExtractFormattedStackTrace_m3497,
	UnityException__ctor_m3498,
	UnityException__ctor_m3499,
	UnityException__ctor_m3500,
	UnityException__ctor_m3501,
	SharedBetweenAnimatorsAttribute__ctor_m3502,
	StateMachineBehaviour__ctor_m3503,
	TextEditor__ctor_m3504,
	TextGenerationSettings_CompareColors_m3505,
	TextGenerationSettings_CompareVector2_m3506,
	TextGenerationSettings_Equals_m3507,
	TrackedReference_Equals_m3508,
	TrackedReference_GetHashCode_m3509,
	TrackedReference_op_Equality_m3510,
	ArgumentCache__ctor_m3511,
	ArgumentCache_get_unityObjectArgument_m3512,
	ArgumentCache_get_unityObjectArgumentAssemblyTypeName_m3513,
	ArgumentCache_get_intArgument_m3514,
	ArgumentCache_get_floatArgument_m3515,
	ArgumentCache_get_stringArgument_m3516,
	ArgumentCache_get_boolArgument_m3517,
	ArgumentCache_TidyAssemblyTypeName_m3518,
	ArgumentCache_OnBeforeSerialize_m3519,
	ArgumentCache_OnAfterDeserialize_m3520,
	BaseInvokableCall__ctor_m3521,
	BaseInvokableCall__ctor_m3522,
	BaseInvokableCall_AllowInvoke_m3523,
	InvokableCall__ctor_m3524,
	InvokableCall__ctor_m3525,
	InvokableCall_Invoke_m3526,
	InvokableCall_Find_m3527,
	PersistentCall__ctor_m3528,
	PersistentCall_get_target_m3529,
	PersistentCall_get_methodName_m3530,
	PersistentCall_get_mode_m3531,
	PersistentCall_get_arguments_m3532,
	PersistentCall_IsValid_m3533,
	PersistentCall_GetRuntimeCall_m3534,
	PersistentCall_GetObjectCall_m3535,
	PersistentCallGroup__ctor_m3536,
	PersistentCallGroup_Initialize_m3537,
	InvokableCallList__ctor_m3538,
	InvokableCallList_AddPersistentInvokableCall_m3539,
	InvokableCallList_AddListener_m3540,
	InvokableCallList_RemoveListener_m3541,
	InvokableCallList_ClearPersistent_m3542,
	InvokableCallList_Invoke_m3543,
	UnityEventBase__ctor_m3544,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnBeforeSerialize_m3545,
	UnityEventBase_UnityEngine_ISerializationCallbackReceiver_OnAfterDeserialize_m3546,
	UnityEventBase_FindMethod_m3547,
	UnityEventBase_FindMethod_m3548,
	UnityEventBase_DirtyPersistentCalls_m3549,
	UnityEventBase_RebuildPersistentCallsIfNeeded_m3550,
	UnityEventBase_AddCall_m3551,
	UnityEventBase_RemoveListener_m3552,
	UnityEventBase_Invoke_m3553,
	UnityEventBase_ToString_m3554,
	UnityEventBase_GetValidMethodInfo_m3555,
	UnityEvent__ctor_m1986,
	UnityEvent_AddListener_m2077,
	UnityEvent_FindMethod_Impl_m3556,
	UnityEvent_GetDelegate_m3557,
	UnityEvent_GetDelegate_m3558,
	UnityEvent_Invoke_m1988,
	DefaultValueAttribute__ctor_m3559,
	DefaultValueAttribute_get_Value_m3560,
	DefaultValueAttribute_Equals_m3561,
	DefaultValueAttribute_GetHashCode_m3562,
	ExcludeFromDocsAttribute__ctor_m3563,
	FormerlySerializedAsAttribute__ctor_m3564,
	TypeInferenceRuleAttribute__ctor_m3565,
	TypeInferenceRuleAttribute__ctor_m3566,
	TypeInferenceRuleAttribute_ToString_m3567,
	GenericStack__ctor_m3568,
	NetFxCoreExtensions_CreateDelegate_m3569,
	NetFxCoreExtensions_GetMethodInfo_m3570,
	UnityAdsDelegate__ctor_m3571,
	UnityAdsDelegate_Invoke_m3572,
	UnityAdsDelegate_BeginInvoke_m3573,
	UnityAdsDelegate_EndInvoke_m3574,
	UnityAction__ctor_m2076,
	UnityAction_Invoke_m2094,
	UnityAction_BeginInvoke_m3575,
	UnityAction_EndInvoke_m3576,
	ExtensionAttribute__ctor_m3677,
	Locale_GetText_m3678,
	Locale_GetText_m3679,
	KeyBuilder_get_Rng_m3680,
	KeyBuilder_Key_m3681,
	KeyBuilder_IV_m3682,
	SymmetricTransform__ctor_m3683,
	SymmetricTransform_System_IDisposable_Dispose_m3684,
	SymmetricTransform_Finalize_m3685,
	SymmetricTransform_Dispose_m3686,
	SymmetricTransform_get_CanReuseTransform_m3687,
	SymmetricTransform_Transform_m3688,
	SymmetricTransform_CBC_m3689,
	SymmetricTransform_CFB_m3690,
	SymmetricTransform_OFB_m3691,
	SymmetricTransform_CTS_m3692,
	SymmetricTransform_CheckInput_m3693,
	SymmetricTransform_TransformBlock_m3694,
	SymmetricTransform_get_KeepLastBlock_m3695,
	SymmetricTransform_InternalTransformBlock_m3696,
	SymmetricTransform_Random_m3697,
	SymmetricTransform_ThrowBadPaddingException_m3698,
	SymmetricTransform_FinalEncrypt_m3699,
	SymmetricTransform_FinalDecrypt_m3700,
	SymmetricTransform_TransformFinalBlock_m3701,
	Check_SourceAndPredicate_m3702,
	Aes__ctor_m3703,
	AesManaged__ctor_m3704,
	AesManaged_GenerateIV_m3705,
	AesManaged_GenerateKey_m3706,
	AesManaged_CreateDecryptor_m3707,
	AesManaged_CreateEncryptor_m3708,
	AesManaged_get_IV_m3709,
	AesManaged_set_IV_m3710,
	AesManaged_get_Key_m3711,
	AesManaged_set_Key_m3712,
	AesManaged_get_KeySize_m3713,
	AesManaged_set_KeySize_m3714,
	AesManaged_CreateDecryptor_m3715,
	AesManaged_CreateEncryptor_m3716,
	AesManaged_Dispose_m3717,
	AesTransform__ctor_m3718,
	AesTransform__cctor_m3719,
	AesTransform_ECB_m3720,
	AesTransform_SubByte_m3721,
	AesTransform_Encrypt128_m3722,
	AesTransform_Decrypt128_m3723,
	Locale_GetText_m3745,
	ModulusRing__ctor_m3746,
	ModulusRing_BarrettReduction_m3747,
	ModulusRing_Multiply_m3748,
	ModulusRing_Difference_m3749,
	ModulusRing_Pow_m3750,
	ModulusRing_Pow_m3751,
	Kernel_AddSameSign_m3752,
	Kernel_Subtract_m3753,
	Kernel_MinusEq_m3754,
	Kernel_PlusEq_m3755,
	Kernel_Compare_m3756,
	Kernel_SingleByteDivideInPlace_m3757,
	Kernel_DwordMod_m3758,
	Kernel_DwordDivMod_m3759,
	Kernel_multiByteDivide_m3760,
	Kernel_LeftShift_m3761,
	Kernel_RightShift_m3762,
	Kernel_Multiply_m3763,
	Kernel_MultiplyMod2p32pmod_m3764,
	Kernel_modInverse_m3765,
	Kernel_modInverse_m3766,
	BigInteger__ctor_m3767,
	BigInteger__ctor_m3768,
	BigInteger__ctor_m3769,
	BigInteger__ctor_m3770,
	BigInteger__ctor_m3771,
	BigInteger__cctor_m3772,
	BigInteger_get_Rng_m3773,
	BigInteger_GenerateRandom_m3774,
	BigInteger_GenerateRandom_m3775,
	BigInteger_BitCount_m3776,
	BigInteger_TestBit_m3777,
	BigInteger_SetBit_m3778,
	BigInteger_SetBit_m3779,
	BigInteger_LowestSetBit_m3780,
	BigInteger_GetBytes_m3781,
	BigInteger_ToString_m3782,
	BigInteger_ToString_m3783,
	BigInteger_Normalize_m3784,
	BigInteger_Clear_m3785,
	BigInteger_GetHashCode_m3786,
	BigInteger_ToString_m3787,
	BigInteger_Equals_m3788,
	BigInteger_ModInverse_m3789,
	BigInteger_ModPow_m3790,
	BigInteger_GeneratePseudoPrime_m3791,
	BigInteger_Incr2_m3792,
	BigInteger_op_Implicit_m3793,
	BigInteger_op_Implicit_m3794,
	BigInteger_op_Addition_m3795,
	BigInteger_op_Subtraction_m3796,
	BigInteger_op_Modulus_m3797,
	BigInteger_op_Modulus_m3798,
	BigInteger_op_Division_m3799,
	BigInteger_op_Multiply_m3800,
	BigInteger_op_LeftShift_m3801,
	BigInteger_op_RightShift_m3802,
	BigInteger_op_Equality_m3803,
	BigInteger_op_Inequality_m3804,
	BigInteger_op_Equality_m3805,
	BigInteger_op_Inequality_m3806,
	BigInteger_op_GreaterThan_m3807,
	BigInteger_op_LessThan_m3808,
	BigInteger_op_GreaterThanOrEqual_m3809,
	BigInteger_op_LessThanOrEqual_m3810,
	PrimalityTests_GetSPPRounds_m3811,
	PrimalityTests_RabinMillerTest_m3812,
	PrimeGeneratorBase__ctor_m3813,
	PrimeGeneratorBase_get_Confidence_m3814,
	PrimeGeneratorBase_get_PrimalityTest_m3815,
	PrimeGeneratorBase_get_TrialDivisionBounds_m3816,
	SequentialSearchPrimeGeneratorBase__ctor_m3817,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m3818,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3819,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m3820,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m3821,
	ASN1__ctor_m3822,
	ASN1__ctor_m3823,
	ASN1__ctor_m3824,
	ASN1_get_Count_m3825,
	ASN1_get_Tag_m3826,
	ASN1_get_Length_m3827,
	ASN1_get_Value_m3828,
	ASN1_set_Value_m3829,
	ASN1_CompareArray_m3830,
	ASN1_CompareValue_m3831,
	ASN1_Add_m3832,
	ASN1_GetBytes_m3833,
	ASN1_Decode_m3834,
	ASN1_DecodeTLV_m3835,
	ASN1_get_Item_m3836,
	ASN1_Element_m3837,
	ASN1_ToString_m3838,
	ASN1Convert_FromInt32_m3839,
	ASN1Convert_FromOid_m3840,
	ASN1Convert_ToInt32_m3841,
	ASN1Convert_ToOid_m3842,
	ASN1Convert_ToDateTime_m3843,
	BitConverterLE_GetUIntBytes_m3844,
	BitConverterLE_GetBytes_m3845,
	ContentInfo__ctor_m3846,
	ContentInfo__ctor_m3847,
	ContentInfo__ctor_m3848,
	ContentInfo__ctor_m3849,
	ContentInfo_get_ASN1_m3850,
	ContentInfo_get_Content_m3851,
	ContentInfo_set_Content_m3852,
	ContentInfo_get_ContentType_m3853,
	ContentInfo_set_ContentType_m3854,
	ContentInfo_GetASN1_m3855,
	EncryptedData__ctor_m3856,
	EncryptedData__ctor_m3857,
	EncryptedData_get_EncryptionAlgorithm_m3858,
	EncryptedData_get_EncryptedContent_m3859,
	ARC4Managed__ctor_m3860,
	ARC4Managed_Finalize_m3861,
	ARC4Managed_Dispose_m3862,
	ARC4Managed_get_Key_m3863,
	ARC4Managed_set_Key_m3864,
	ARC4Managed_get_CanReuseTransform_m3865,
	ARC4Managed_CreateEncryptor_m3866,
	ARC4Managed_CreateDecryptor_m3867,
	ARC4Managed_GenerateIV_m3868,
	ARC4Managed_GenerateKey_m3869,
	ARC4Managed_KeySetup_m3870,
	ARC4Managed_CheckInput_m3871,
	ARC4Managed_TransformBlock_m3872,
	ARC4Managed_InternalTransformBlock_m3873,
	ARC4Managed_TransformFinalBlock_m3874,
	CryptoConvert_ToHex_m3875,
	KeyBuilder_get_Rng_m3876,
	KeyBuilder_Key_m3877,
	MD2__ctor_m3878,
	MD2_Create_m3879,
	MD2_Create_m3880,
	MD2Managed__ctor_m3881,
	MD2Managed__cctor_m3882,
	MD2Managed_Padding_m3883,
	MD2Managed_Initialize_m3884,
	MD2Managed_HashCore_m3885,
	MD2Managed_HashFinal_m3886,
	MD2Managed_MD2Transform_m3887,
	PKCS1__cctor_m3888,
	PKCS1_Compare_m3889,
	PKCS1_I2OSP_m3890,
	PKCS1_OS2IP_m3891,
	PKCS1_RSASP1_m3892,
	PKCS1_RSAVP1_m3893,
	PKCS1_Sign_v15_m3894,
	PKCS1_Verify_v15_m3895,
	PKCS1_Verify_v15_m3896,
	PKCS1_Encode_v15_m3897,
	PrivateKeyInfo__ctor_m3898,
	PrivateKeyInfo__ctor_m3899,
	PrivateKeyInfo_get_PrivateKey_m3900,
	PrivateKeyInfo_Decode_m3901,
	PrivateKeyInfo_RemoveLeadingZero_m3902,
	PrivateKeyInfo_Normalize_m3903,
	PrivateKeyInfo_DecodeRSA_m3904,
	PrivateKeyInfo_DecodeDSA_m3905,
	EncryptedPrivateKeyInfo__ctor_m3906,
	EncryptedPrivateKeyInfo__ctor_m3907,
	EncryptedPrivateKeyInfo_get_Algorithm_m3908,
	EncryptedPrivateKeyInfo_get_EncryptedData_m3909,
	EncryptedPrivateKeyInfo_get_Salt_m3910,
	EncryptedPrivateKeyInfo_get_IterationCount_m3911,
	EncryptedPrivateKeyInfo_Decode_m3912,
	RC4__ctor_m3913,
	RC4__cctor_m3914,
	RC4_get_IV_m3915,
	RC4_set_IV_m3916,
	KeyGeneratedEventHandler__ctor_m3917,
	KeyGeneratedEventHandler_Invoke_m3918,
	KeyGeneratedEventHandler_BeginInvoke_m3919,
	KeyGeneratedEventHandler_EndInvoke_m3920,
	RSAManaged__ctor_m3921,
	RSAManaged__ctor_m3922,
	RSAManaged_Finalize_m3923,
	RSAManaged_GenerateKeyPair_m3924,
	RSAManaged_get_KeySize_m3925,
	RSAManaged_get_PublicOnly_m3926,
	RSAManaged_DecryptValue_m3927,
	RSAManaged_EncryptValue_m3928,
	RSAManaged_ExportParameters_m3929,
	RSAManaged_ImportParameters_m3930,
	RSAManaged_Dispose_m3931,
	RSAManaged_ToXmlString_m3932,
	RSAManaged_GetPaddedValue_m3933,
	SafeBag__ctor_m3934,
	SafeBag_get_BagOID_m3935,
	SafeBag_get_ASN1_m3936,
	DeriveBytes__ctor_m3937,
	DeriveBytes__cctor_m3938,
	DeriveBytes_set_HashName_m3939,
	DeriveBytes_set_IterationCount_m3940,
	DeriveBytes_set_Password_m3941,
	DeriveBytes_set_Salt_m3942,
	DeriveBytes_Adjust_m3943,
	DeriveBytes_Derive_m3944,
	DeriveBytes_DeriveKey_m3945,
	DeriveBytes_DeriveIV_m3946,
	DeriveBytes_DeriveMAC_m3947,
	PKCS12__ctor_m3948,
	PKCS12__ctor_m3949,
	PKCS12__ctor_m3950,
	PKCS12__cctor_m3951,
	PKCS12_Decode_m3952,
	PKCS12_Finalize_m3953,
	PKCS12_set_Password_m3954,
	PKCS12_get_IterationCount_m3955,
	PKCS12_set_IterationCount_m3956,
	PKCS12_get_Keys_m3957,
	PKCS12_get_Certificates_m3958,
	PKCS12_get_RNG_m3959,
	PKCS12_Compare_m3960,
	PKCS12_GetSymmetricAlgorithm_m3961,
	PKCS12_Decrypt_m3962,
	PKCS12_Decrypt_m3963,
	PKCS12_Encrypt_m3964,
	PKCS12_GetExistingParameters_m3965,
	PKCS12_AddPrivateKey_m3966,
	PKCS12_ReadSafeBag_m3967,
	PKCS12_CertificateSafeBag_m3968,
	PKCS12_MAC_m3969,
	PKCS12_GetBytes_m3970,
	PKCS12_EncryptedContentInfo_m3971,
	PKCS12_AddCertificate_m3972,
	PKCS12_AddCertificate_m3973,
	PKCS12_RemoveCertificate_m3974,
	PKCS12_RemoveCertificate_m3975,
	PKCS12_Clone_m3976,
	PKCS12_get_MaximumPasswordLength_m3977,
	X501__cctor_m3978,
	X501_ToString_m3979,
	X501_ToString_m3980,
	X501_AppendEntry_m3981,
	X509Certificate__ctor_m3982,
	X509Certificate__cctor_m3983,
	X509Certificate_Parse_m3984,
	X509Certificate_GetUnsignedBigInteger_m3985,
	X509Certificate_get_DSA_m3986,
	X509Certificate_set_DSA_m3987,
	X509Certificate_get_Extensions_m3988,
	X509Certificate_get_Hash_m3989,
	X509Certificate_get_IssuerName_m3990,
	X509Certificate_get_KeyAlgorithm_m3991,
	X509Certificate_get_KeyAlgorithmParameters_m3992,
	X509Certificate_set_KeyAlgorithmParameters_m3993,
	X509Certificate_get_PublicKey_m3994,
	X509Certificate_get_RSA_m3995,
	X509Certificate_set_RSA_m3996,
	X509Certificate_get_RawData_m3997,
	X509Certificate_get_SerialNumber_m3998,
	X509Certificate_get_Signature_m3999,
	X509Certificate_get_SignatureAlgorithm_m4000,
	X509Certificate_get_SubjectName_m4001,
	X509Certificate_get_ValidFrom_m4002,
	X509Certificate_get_ValidUntil_m4003,
	X509Certificate_get_Version_m4004,
	X509Certificate_get_IsCurrent_m4005,
	X509Certificate_WasCurrent_m4006,
	X509Certificate_VerifySignature_m4007,
	X509Certificate_VerifySignature_m4008,
	X509Certificate_VerifySignature_m4009,
	X509Certificate_get_IsSelfSigned_m4010,
	X509Certificate_GetIssuerName_m4011,
	X509Certificate_GetSubjectName_m4012,
	X509Certificate_GetObjectData_m4013,
	X509Certificate_PEM_m4014,
	X509CertificateEnumerator__ctor_m4015,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m4016,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m4017,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m4018,
	X509CertificateEnumerator_get_Current_m4019,
	X509CertificateEnumerator_MoveNext_m4020,
	X509CertificateEnumerator_Reset_m4021,
	X509CertificateCollection__ctor_m4022,
	X509CertificateCollection__ctor_m4023,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m4024,
	X509CertificateCollection_get_Item_m4025,
	X509CertificateCollection_Add_m4026,
	X509CertificateCollection_AddRange_m4027,
	X509CertificateCollection_Contains_m4028,
	X509CertificateCollection_GetEnumerator_m4029,
	X509CertificateCollection_GetHashCode_m4030,
	X509CertificateCollection_IndexOf_m4031,
	X509CertificateCollection_Remove_m4032,
	X509CertificateCollection_Compare_m4033,
	X509Chain__ctor_m4034,
	X509Chain__ctor_m4035,
	X509Chain_get_Status_m4036,
	X509Chain_get_TrustAnchors_m4037,
	X509Chain_Build_m4038,
	X509Chain_IsValid_m4039,
	X509Chain_FindCertificateParent_m4040,
	X509Chain_FindCertificateRoot_m4041,
	X509Chain_IsTrusted_m4042,
	X509Chain_IsParent_m4043,
	X509CrlEntry__ctor_m4044,
	X509CrlEntry_get_SerialNumber_m4045,
	X509CrlEntry_get_RevocationDate_m4046,
	X509CrlEntry_get_Extensions_m4047,
	X509Crl__ctor_m4048,
	X509Crl_Parse_m4049,
	X509Crl_get_Extensions_m4050,
	X509Crl_get_Hash_m4051,
	X509Crl_get_IssuerName_m4052,
	X509Crl_get_NextUpdate_m4053,
	X509Crl_Compare_m4054,
	X509Crl_GetCrlEntry_m4055,
	X509Crl_GetCrlEntry_m4056,
	X509Crl_GetHashName_m4057,
	X509Crl_VerifySignature_m4058,
	X509Crl_VerifySignature_m4059,
	X509Crl_VerifySignature_m4060,
	X509Extension__ctor_m4061,
	X509Extension__ctor_m4062,
	X509Extension_Decode_m4063,
	X509Extension_Encode_m4064,
	X509Extension_get_Oid_m4065,
	X509Extension_get_Critical_m4066,
	X509Extension_get_Value_m4067,
	X509Extension_Equals_m4068,
	X509Extension_GetHashCode_m4069,
	X509Extension_WriteLine_m4070,
	X509Extension_ToString_m4071,
	X509ExtensionCollection__ctor_m4072,
	X509ExtensionCollection__ctor_m4073,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m4074,
	X509ExtensionCollection_IndexOf_m4075,
	X509ExtensionCollection_get_Item_m4076,
	X509Store__ctor_m4077,
	X509Store_get_Certificates_m4078,
	X509Store_get_Crls_m4079,
	X509Store_Load_m4080,
	X509Store_LoadCertificate_m4081,
	X509Store_LoadCrl_m4082,
	X509Store_CheckStore_m4083,
	X509Store_BuildCertificatesCollection_m4084,
	X509Store_BuildCrlsCollection_m4085,
	X509StoreManager_get_CurrentUser_m4086,
	X509StoreManager_get_LocalMachine_m4087,
	X509StoreManager_get_TrustedRootCertificates_m4088,
	X509Stores__ctor_m4089,
	X509Stores_get_TrustedRoot_m4090,
	X509Stores_Open_m4091,
	AuthorityKeyIdentifierExtension__ctor_m4092,
	AuthorityKeyIdentifierExtension_Decode_m4093,
	AuthorityKeyIdentifierExtension_get_Identifier_m4094,
	AuthorityKeyIdentifierExtension_ToString_m4095,
	BasicConstraintsExtension__ctor_m4096,
	BasicConstraintsExtension_Decode_m4097,
	BasicConstraintsExtension_Encode_m4098,
	BasicConstraintsExtension_get_CertificateAuthority_m4099,
	BasicConstraintsExtension_ToString_m4100,
	ExtendedKeyUsageExtension__ctor_m4101,
	ExtendedKeyUsageExtension_Decode_m4102,
	ExtendedKeyUsageExtension_Encode_m4103,
	ExtendedKeyUsageExtension_get_KeyPurpose_m4104,
	ExtendedKeyUsageExtension_ToString_m4105,
	GeneralNames__ctor_m4106,
	GeneralNames_get_DNSNames_m4107,
	GeneralNames_get_IPAddresses_m4108,
	GeneralNames_ToString_m4109,
	KeyUsageExtension__ctor_m4110,
	KeyUsageExtension_Decode_m4111,
	KeyUsageExtension_Encode_m4112,
	KeyUsageExtension_Support_m4113,
	KeyUsageExtension_ToString_m4114,
	NetscapeCertTypeExtension__ctor_m4115,
	NetscapeCertTypeExtension_Decode_m4116,
	NetscapeCertTypeExtension_Support_m4117,
	NetscapeCertTypeExtension_ToString_m4118,
	SubjectAltNameExtension__ctor_m4119,
	SubjectAltNameExtension_Decode_m4120,
	SubjectAltNameExtension_get_DNSNames_m4121,
	SubjectAltNameExtension_get_IPAddresses_m4122,
	SubjectAltNameExtension_ToString_m4123,
	HMAC__ctor_m4124,
	HMAC_get_Key_m4125,
	HMAC_set_Key_m4126,
	HMAC_Initialize_m4127,
	HMAC_HashFinal_m4128,
	HMAC_HashCore_m4129,
	HMAC_initializePad_m4130,
	MD5SHA1__ctor_m4131,
	MD5SHA1_Initialize_m4132,
	MD5SHA1_HashFinal_m4133,
	MD5SHA1_HashCore_m4134,
	MD5SHA1_CreateSignature_m4135,
	MD5SHA1_VerifySignature_m4136,
	Alert__ctor_m4137,
	Alert__ctor_m4138,
	Alert_get_Level_m4139,
	Alert_get_Description_m4140,
	Alert_get_IsWarning_m4141,
	Alert_get_IsCloseNotify_m4142,
	Alert_inferAlertLevel_m4143,
	Alert_GetAlertMessage_m4144,
	CipherSuite__ctor_m4145,
	CipherSuite__cctor_m4146,
	CipherSuite_get_EncryptionCipher_m4147,
	CipherSuite_get_DecryptionCipher_m4148,
	CipherSuite_get_ClientHMAC_m4149,
	CipherSuite_get_ServerHMAC_m4150,
	CipherSuite_get_CipherAlgorithmType_m4151,
	CipherSuite_get_HashAlgorithmName_m4152,
	CipherSuite_get_HashAlgorithmType_m4153,
	CipherSuite_get_HashSize_m4154,
	CipherSuite_get_ExchangeAlgorithmType_m4155,
	CipherSuite_get_CipherMode_m4156,
	CipherSuite_get_Code_m4157,
	CipherSuite_get_Name_m4158,
	CipherSuite_get_IsExportable_m4159,
	CipherSuite_get_KeyMaterialSize_m4160,
	CipherSuite_get_KeyBlockSize_m4161,
	CipherSuite_get_ExpandedKeyMaterialSize_m4162,
	CipherSuite_get_EffectiveKeyBits_m4163,
	CipherSuite_get_IvSize_m4164,
	CipherSuite_get_Context_m4165,
	CipherSuite_set_Context_m4166,
	CipherSuite_Write_m4167,
	CipherSuite_Write_m4168,
	CipherSuite_InitializeCipher_m4169,
	CipherSuite_EncryptRecord_m4170,
	CipherSuite_DecryptRecord_m4171,
	CipherSuite_CreatePremasterSecret_m4172,
	CipherSuite_PRF_m4173,
	CipherSuite_Expand_m4174,
	CipherSuite_createEncryptionCipher_m4175,
	CipherSuite_createDecryptionCipher_m4176,
	CipherSuiteCollection__ctor_m4177,
	CipherSuiteCollection_System_Collections_IList_get_Item_m4178,
	CipherSuiteCollection_System_Collections_IList_set_Item_m4179,
	CipherSuiteCollection_System_Collections_ICollection_get_IsSynchronized_m4180,
	CipherSuiteCollection_System_Collections_ICollection_get_SyncRoot_m4181,
	CipherSuiteCollection_System_Collections_IEnumerable_GetEnumerator_m4182,
	CipherSuiteCollection_System_Collections_IList_Contains_m4183,
	CipherSuiteCollection_System_Collections_IList_IndexOf_m4184,
	CipherSuiteCollection_System_Collections_IList_Insert_m4185,
	CipherSuiteCollection_System_Collections_IList_Remove_m4186,
	CipherSuiteCollection_System_Collections_IList_RemoveAt_m4187,
	CipherSuiteCollection_System_Collections_IList_Add_m4188,
	CipherSuiteCollection_get_Item_m4189,
	CipherSuiteCollection_get_Item_m4190,
	CipherSuiteCollection_set_Item_m4191,
	CipherSuiteCollection_get_Item_m4192,
	CipherSuiteCollection_get_Count_m4193,
	CipherSuiteCollection_get_IsFixedSize_m4194,
	CipherSuiteCollection_get_IsReadOnly_m4195,
	CipherSuiteCollection_CopyTo_m4196,
	CipherSuiteCollection_Clear_m4197,
	CipherSuiteCollection_IndexOf_m4198,
	CipherSuiteCollection_IndexOf_m4199,
	CipherSuiteCollection_Add_m4200,
	CipherSuiteCollection_add_m4201,
	CipherSuiteCollection_add_m4202,
	CipherSuiteCollection_cultureAwareCompare_m4203,
	CipherSuiteFactory_GetSupportedCiphers_m4204,
	CipherSuiteFactory_GetTls1SupportedCiphers_m4205,
	CipherSuiteFactory_GetSsl3SupportedCiphers_m4206,
	ClientContext__ctor_m4207,
	ClientContext_get_SslStream_m4208,
	ClientContext_get_ClientHelloProtocol_m4209,
	ClientContext_set_ClientHelloProtocol_m4210,
	ClientContext_Clear_m4211,
	ClientRecordProtocol__ctor_m4212,
	ClientRecordProtocol_GetMessage_m4213,
	ClientRecordProtocol_ProcessHandshakeMessage_m4214,
	ClientRecordProtocol_createClientHandshakeMessage_m4215,
	ClientRecordProtocol_createServerHandshakeMessage_m4216,
	ClientSessionInfo__ctor_m4217,
	ClientSessionInfo__cctor_m4218,
	ClientSessionInfo_Finalize_m4219,
	ClientSessionInfo_get_HostName_m4220,
	ClientSessionInfo_get_Id_m4221,
	ClientSessionInfo_get_Valid_m4222,
	ClientSessionInfo_GetContext_m4223,
	ClientSessionInfo_SetContext_m4224,
	ClientSessionInfo_KeepAlive_m4225,
	ClientSessionInfo_Dispose_m4226,
	ClientSessionInfo_Dispose_m4227,
	ClientSessionInfo_CheckDisposed_m4228,
	ClientSessionCache__cctor_m4229,
	ClientSessionCache_Add_m4230,
	ClientSessionCache_FromHost_m4231,
	ClientSessionCache_FromContext_m4232,
	ClientSessionCache_SetContextInCache_m4233,
	ClientSessionCache_SetContextFromCache_m4234,
	Context__ctor_m4235,
	Context_get_AbbreviatedHandshake_m4236,
	Context_set_AbbreviatedHandshake_m4237,
	Context_get_ProtocolNegotiated_m4238,
	Context_set_ProtocolNegotiated_m4239,
	Context_get_SecurityProtocol_m4240,
	Context_set_SecurityProtocol_m4241,
	Context_get_SecurityProtocolFlags_m4242,
	Context_get_Protocol_m4243,
	Context_get_SessionId_m4244,
	Context_set_SessionId_m4245,
	Context_get_CompressionMethod_m4246,
	Context_set_CompressionMethod_m4247,
	Context_get_ServerSettings_m4248,
	Context_get_ClientSettings_m4249,
	Context_get_LastHandshakeMsg_m4250,
	Context_set_LastHandshakeMsg_m4251,
	Context_get_HandshakeState_m4252,
	Context_set_HandshakeState_m4253,
	Context_get_ReceivedConnectionEnd_m4254,
	Context_set_ReceivedConnectionEnd_m4255,
	Context_get_SentConnectionEnd_m4256,
	Context_set_SentConnectionEnd_m4257,
	Context_get_SupportedCiphers_m4258,
	Context_set_SupportedCiphers_m4259,
	Context_get_HandshakeMessages_m4260,
	Context_get_WriteSequenceNumber_m4261,
	Context_set_WriteSequenceNumber_m4262,
	Context_get_ReadSequenceNumber_m4263,
	Context_set_ReadSequenceNumber_m4264,
	Context_get_ClientRandom_m4265,
	Context_set_ClientRandom_m4266,
	Context_get_ServerRandom_m4267,
	Context_set_ServerRandom_m4268,
	Context_get_RandomCS_m4269,
	Context_set_RandomCS_m4270,
	Context_get_RandomSC_m4271,
	Context_set_RandomSC_m4272,
	Context_get_MasterSecret_m4273,
	Context_set_MasterSecret_m4274,
	Context_get_ClientWriteKey_m4275,
	Context_set_ClientWriteKey_m4276,
	Context_get_ServerWriteKey_m4277,
	Context_set_ServerWriteKey_m4278,
	Context_get_ClientWriteIV_m4279,
	Context_set_ClientWriteIV_m4280,
	Context_get_ServerWriteIV_m4281,
	Context_set_ServerWriteIV_m4282,
	Context_get_RecordProtocol_m4283,
	Context_set_RecordProtocol_m4284,
	Context_GetUnixTime_m4285,
	Context_GetSecureRandomBytes_m4286,
	Context_Clear_m4287,
	Context_ClearKeyInfo_m4288,
	Context_DecodeProtocolCode_m4289,
	Context_ChangeProtocol_m4290,
	Context_get_Current_m4291,
	Context_get_Negotiating_m4292,
	Context_get_Read_m4293,
	Context_get_Write_m4294,
	Context_StartSwitchingSecurityParameters_m4295,
	Context_EndSwitchingSecurityParameters_m4296,
	HttpsClientStream__ctor_m4297,
	HttpsClientStream_get_TrustFailure_m4298,
	HttpsClientStream_RaiseServerCertificateValidation_m4299,
	HttpsClientStream_U3CHttpsClientStreamU3Em__0_m4300,
	HttpsClientStream_U3CHttpsClientStreamU3Em__1_m4301,
	ReceiveRecordAsyncResult__ctor_m4302,
	ReceiveRecordAsyncResult_get_Record_m4303,
	ReceiveRecordAsyncResult_get_ResultingBuffer_m4304,
	ReceiveRecordAsyncResult_get_InitialBuffer_m4305,
	ReceiveRecordAsyncResult_get_AsyncState_m4306,
	ReceiveRecordAsyncResult_get_AsyncException_m4307,
	ReceiveRecordAsyncResult_get_CompletedWithError_m4308,
	ReceiveRecordAsyncResult_get_AsyncWaitHandle_m4309,
	ReceiveRecordAsyncResult_get_IsCompleted_m4310,
	ReceiveRecordAsyncResult_SetComplete_m4311,
	ReceiveRecordAsyncResult_SetComplete_m4312,
	ReceiveRecordAsyncResult_SetComplete_m4313,
	SendRecordAsyncResult__ctor_m4314,
	SendRecordAsyncResult_get_Message_m4315,
	SendRecordAsyncResult_get_AsyncState_m4316,
	SendRecordAsyncResult_get_AsyncException_m4317,
	SendRecordAsyncResult_get_CompletedWithError_m4318,
	SendRecordAsyncResult_get_AsyncWaitHandle_m4319,
	SendRecordAsyncResult_get_IsCompleted_m4320,
	SendRecordAsyncResult_SetComplete_m4321,
	SendRecordAsyncResult_SetComplete_m4322,
	RecordProtocol__ctor_m4323,
	RecordProtocol__cctor_m4324,
	RecordProtocol_get_Context_m4325,
	RecordProtocol_SendRecord_m4326,
	RecordProtocol_ProcessChangeCipherSpec_m4327,
	RecordProtocol_GetMessage_m4328,
	RecordProtocol_BeginReceiveRecord_m4329,
	RecordProtocol_InternalReceiveRecordCallback_m4330,
	RecordProtocol_EndReceiveRecord_m4331,
	RecordProtocol_ReceiveRecord_m4332,
	RecordProtocol_ReadRecordBuffer_m4333,
	RecordProtocol_ReadClientHelloV2_m4334,
	RecordProtocol_ReadStandardRecordBuffer_m4335,
	RecordProtocol_ProcessAlert_m4336,
	RecordProtocol_SendAlert_m4337,
	RecordProtocol_SendAlert_m4338,
	RecordProtocol_SendAlert_m4339,
	RecordProtocol_SendChangeCipherSpec_m4340,
	RecordProtocol_BeginSendRecord_m4341,
	RecordProtocol_InternalSendRecordCallback_m4342,
	RecordProtocol_BeginSendRecord_m4343,
	RecordProtocol_EndSendRecord_m4344,
	RecordProtocol_SendRecord_m4345,
	RecordProtocol_EncodeRecord_m4346,
	RecordProtocol_EncodeRecord_m4347,
	RecordProtocol_encryptRecordFragment_m4348,
	RecordProtocol_decryptRecordFragment_m4349,
	RecordProtocol_Compare_m4350,
	RecordProtocol_ProcessCipherSpecV2Buffer_m4351,
	RecordProtocol_MapV2CipherCode_m4352,
	RSASslSignatureDeformatter__ctor_m4353,
	RSASslSignatureDeformatter_VerifySignature_m4354,
	RSASslSignatureDeformatter_SetHashAlgorithm_m4355,
	RSASslSignatureDeformatter_SetKey_m4356,
	RSASslSignatureFormatter__ctor_m4357,
	RSASslSignatureFormatter_CreateSignature_m4358,
	RSASslSignatureFormatter_SetHashAlgorithm_m4359,
	RSASslSignatureFormatter_SetKey_m4360,
	SecurityParameters__ctor_m4361,
	SecurityParameters_get_Cipher_m4362,
	SecurityParameters_set_Cipher_m4363,
	SecurityParameters_get_ClientWriteMAC_m4364,
	SecurityParameters_set_ClientWriteMAC_m4365,
	SecurityParameters_get_ServerWriteMAC_m4366,
	SecurityParameters_set_ServerWriteMAC_m4367,
	SecurityParameters_Clear_m4368,
	ValidationResult_get_Trusted_m4369,
	ValidationResult_get_ErrorCode_m4370,
	SslClientStream__ctor_m4371,
	SslClientStream__ctor_m4372,
	SslClientStream__ctor_m4373,
	SslClientStream__ctor_m4374,
	SslClientStream__ctor_m4375,
	SslClientStream_add_ServerCertValidation_m4376,
	SslClientStream_remove_ServerCertValidation_m4377,
	SslClientStream_add_ClientCertSelection_m4378,
	SslClientStream_remove_ClientCertSelection_m4379,
	SslClientStream_add_PrivateKeySelection_m4380,
	SslClientStream_remove_PrivateKeySelection_m4381,
	SslClientStream_add_ServerCertValidation2_m4382,
	SslClientStream_remove_ServerCertValidation2_m4383,
	SslClientStream_get_InputBuffer_m4384,
	SslClientStream_get_ClientCertificates_m4385,
	SslClientStream_get_SelectedClientCertificate_m4386,
	SslClientStream_get_ServerCertValidationDelegate_m4387,
	SslClientStream_set_ServerCertValidationDelegate_m4388,
	SslClientStream_get_ClientCertSelectionDelegate_m4389,
	SslClientStream_set_ClientCertSelectionDelegate_m4390,
	SslClientStream_get_PrivateKeyCertSelectionDelegate_m4391,
	SslClientStream_set_PrivateKeyCertSelectionDelegate_m4392,
	SslClientStream_Finalize_m4393,
	SslClientStream_Dispose_m4394,
	SslClientStream_OnBeginNegotiateHandshake_m4395,
	SslClientStream_SafeReceiveRecord_m4396,
	SslClientStream_OnNegotiateHandshakeCallback_m4397,
	SslClientStream_OnLocalCertificateSelection_m4398,
	SslClientStream_get_HaveRemoteValidation2Callback_m4399,
	SslClientStream_OnRemoteCertificateValidation2_m4400,
	SslClientStream_OnRemoteCertificateValidation_m4401,
	SslClientStream_RaiseServerCertificateValidation_m4402,
	SslClientStream_RaiseServerCertificateValidation2_m4403,
	SslClientStream_RaiseClientCertificateSelection_m4404,
	SslClientStream_OnLocalPrivateKeySelection_m4405,
	SslClientStream_RaisePrivateKeySelection_m4406,
	SslCipherSuite__ctor_m4407,
	SslCipherSuite_ComputeServerRecordMAC_m4408,
	SslCipherSuite_ComputeClientRecordMAC_m4409,
	SslCipherSuite_ComputeMasterSecret_m4410,
	SslCipherSuite_ComputeKeys_m4411,
	SslCipherSuite_prf_m4412,
	SslHandshakeHash__ctor_m4413,
	SslHandshakeHash_Initialize_m4414,
	SslHandshakeHash_HashFinal_m4415,
	SslHandshakeHash_HashCore_m4416,
	SslHandshakeHash_CreateSignature_m4417,
	SslHandshakeHash_initializePad_m4418,
	InternalAsyncResult__ctor_m4419,
	InternalAsyncResult_get_ProceedAfterHandshake_m4420,
	InternalAsyncResult_get_FromWrite_m4421,
	InternalAsyncResult_get_Buffer_m4422,
	InternalAsyncResult_get_Offset_m4423,
	InternalAsyncResult_get_Count_m4424,
	InternalAsyncResult_get_BytesRead_m4425,
	InternalAsyncResult_get_AsyncState_m4426,
	InternalAsyncResult_get_AsyncException_m4427,
	InternalAsyncResult_get_CompletedWithError_m4428,
	InternalAsyncResult_get_AsyncWaitHandle_m4429,
	InternalAsyncResult_get_IsCompleted_m4430,
	InternalAsyncResult_SetComplete_m4431,
	InternalAsyncResult_SetComplete_m4432,
	InternalAsyncResult_SetComplete_m4433,
	InternalAsyncResult_SetComplete_m4434,
	SslStreamBase__ctor_m4435,
	SslStreamBase__cctor_m4436,
	SslStreamBase_AsyncHandshakeCallback_m4437,
	SslStreamBase_get_MightNeedHandshake_m4438,
	SslStreamBase_NegotiateHandshake_m4439,
	SslStreamBase_RaiseLocalCertificateSelection_m4440,
	SslStreamBase_RaiseRemoteCertificateValidation_m4441,
	SslStreamBase_RaiseRemoteCertificateValidation2_m4442,
	SslStreamBase_RaiseLocalPrivateKeySelection_m4443,
	SslStreamBase_get_CheckCertRevocationStatus_m4444,
	SslStreamBase_set_CheckCertRevocationStatus_m4445,
	SslStreamBase_get_CipherAlgorithm_m4446,
	SslStreamBase_get_CipherStrength_m4447,
	SslStreamBase_get_HashAlgorithm_m4448,
	SslStreamBase_get_HashStrength_m4449,
	SslStreamBase_get_KeyExchangeStrength_m4450,
	SslStreamBase_get_KeyExchangeAlgorithm_m4451,
	SslStreamBase_get_SecurityProtocol_m4452,
	SslStreamBase_get_ServerCertificate_m4453,
	SslStreamBase_get_ServerCertificates_m4454,
	SslStreamBase_BeginNegotiateHandshake_m4455,
	SslStreamBase_EndNegotiateHandshake_m4456,
	SslStreamBase_BeginRead_m4457,
	SslStreamBase_InternalBeginRead_m4458,
	SslStreamBase_InternalReadCallback_m4459,
	SslStreamBase_InternalBeginWrite_m4460,
	SslStreamBase_InternalWriteCallback_m4461,
	SslStreamBase_BeginWrite_m4462,
	SslStreamBase_EndRead_m4463,
	SslStreamBase_EndWrite_m4464,
	SslStreamBase_Close_m4465,
	SslStreamBase_Flush_m4466,
	SslStreamBase_Read_m4467,
	SslStreamBase_Read_m4468,
	SslStreamBase_Seek_m4469,
	SslStreamBase_SetLength_m4470,
	SslStreamBase_Write_m4471,
	SslStreamBase_Write_m4472,
	SslStreamBase_get_CanRead_m4473,
	SslStreamBase_get_CanSeek_m4474,
	SslStreamBase_get_CanWrite_m4475,
	SslStreamBase_get_Length_m4476,
	SslStreamBase_get_Position_m4477,
	SslStreamBase_set_Position_m4478,
	SslStreamBase_Finalize_m4479,
	SslStreamBase_Dispose_m4480,
	SslStreamBase_resetBuffer_m4481,
	SslStreamBase_checkDisposed_m4482,
	TlsCipherSuite__ctor_m4483,
	TlsCipherSuite_ComputeServerRecordMAC_m4484,
	TlsCipherSuite_ComputeClientRecordMAC_m4485,
	TlsCipherSuite_ComputeMasterSecret_m4486,
	TlsCipherSuite_ComputeKeys_m4487,
	TlsClientSettings__ctor_m4488,
	TlsClientSettings_get_TargetHost_m4489,
	TlsClientSettings_set_TargetHost_m4490,
	TlsClientSettings_get_Certificates_m4491,
	TlsClientSettings_set_Certificates_m4492,
	TlsClientSettings_get_ClientCertificate_m4493,
	TlsClientSettings_set_ClientCertificate_m4494,
	TlsClientSettings_UpdateCertificateRSA_m4495,
	TlsException__ctor_m4496,
	TlsException__ctor_m4497,
	TlsException__ctor_m4498,
	TlsException__ctor_m4499,
	TlsException__ctor_m4500,
	TlsException__ctor_m4501,
	TlsException_get_Alert_m4502,
	TlsServerSettings__ctor_m4503,
	TlsServerSettings_get_ServerKeyExchange_m4504,
	TlsServerSettings_set_ServerKeyExchange_m4505,
	TlsServerSettings_get_Certificates_m4506,
	TlsServerSettings_set_Certificates_m4507,
	TlsServerSettings_get_CertificateRSA_m4508,
	TlsServerSettings_get_RsaParameters_m4509,
	TlsServerSettings_set_RsaParameters_m4510,
	TlsServerSettings_set_SignedParams_m4511,
	TlsServerSettings_get_CertificateRequest_m4512,
	TlsServerSettings_set_CertificateRequest_m4513,
	TlsServerSettings_set_CertificateTypes_m4514,
	TlsServerSettings_set_DistinguisedNames_m4515,
	TlsServerSettings_UpdateCertificateRSA_m4516,
	TlsStream__ctor_m4517,
	TlsStream__ctor_m4518,
	TlsStream_get_EOF_m4519,
	TlsStream_get_CanWrite_m4520,
	TlsStream_get_CanRead_m4521,
	TlsStream_get_CanSeek_m4522,
	TlsStream_get_Position_m4523,
	TlsStream_set_Position_m4524,
	TlsStream_get_Length_m4525,
	TlsStream_ReadSmallValue_m4526,
	TlsStream_ReadByte_m4527,
	TlsStream_ReadInt16_m4528,
	TlsStream_ReadInt24_m4529,
	TlsStream_ReadBytes_m4530,
	TlsStream_Write_m4531,
	TlsStream_Write_m4532,
	TlsStream_WriteInt24_m4533,
	TlsStream_Write_m4534,
	TlsStream_Write_m4535,
	TlsStream_Reset_m4536,
	TlsStream_ToArray_m4537,
	TlsStream_Flush_m4538,
	TlsStream_SetLength_m4539,
	TlsStream_Seek_m4540,
	TlsStream_Read_m4541,
	TlsStream_Write_m4542,
	HandshakeMessage__ctor_m4543,
	HandshakeMessage__ctor_m4544,
	HandshakeMessage__ctor_m4545,
	HandshakeMessage_get_Context_m4546,
	HandshakeMessage_get_HandshakeType_m4547,
	HandshakeMessage_get_ContentType_m4548,
	HandshakeMessage_Process_m4549,
	HandshakeMessage_Update_m4550,
	HandshakeMessage_EncodeMessage_m4551,
	HandshakeMessage_Compare_m4552,
	TlsClientCertificate__ctor_m4553,
	TlsClientCertificate_get_ClientCertificate_m4554,
	TlsClientCertificate_Update_m4555,
	TlsClientCertificate_GetClientCertificate_m4556,
	TlsClientCertificate_SendCertificates_m4557,
	TlsClientCertificate_ProcessAsSsl3_m4558,
	TlsClientCertificate_ProcessAsTls1_m4559,
	TlsClientCertificate_FindParentCertificate_m4560,
	TlsClientCertificateVerify__ctor_m4561,
	TlsClientCertificateVerify_Update_m4562,
	TlsClientCertificateVerify_ProcessAsSsl3_m4563,
	TlsClientCertificateVerify_ProcessAsTls1_m4564,
	TlsClientCertificateVerify_getClientCertRSA_m4565,
	TlsClientCertificateVerify_getUnsignedBigInteger_m4566,
	TlsClientFinished__ctor_m4567,
	TlsClientFinished__cctor_m4568,
	TlsClientFinished_Update_m4569,
	TlsClientFinished_ProcessAsSsl3_m4570,
	TlsClientFinished_ProcessAsTls1_m4571,
	TlsClientHello__ctor_m4572,
	TlsClientHello_Update_m4573,
	TlsClientHello_ProcessAsSsl3_m4574,
	TlsClientHello_ProcessAsTls1_m4575,
	TlsClientKeyExchange__ctor_m4576,
	TlsClientKeyExchange_ProcessAsSsl3_m4577,
	TlsClientKeyExchange_ProcessAsTls1_m4578,
	TlsClientKeyExchange_ProcessCommon_m4579,
	TlsServerCertificate__ctor_m4580,
	TlsServerCertificate_Update_m4581,
	TlsServerCertificate_ProcessAsSsl3_m4582,
	TlsServerCertificate_ProcessAsTls1_m4583,
	TlsServerCertificate_checkCertificateUsage_m4584,
	TlsServerCertificate_validateCertificates_m4585,
	TlsServerCertificate_checkServerIdentity_m4586,
	TlsServerCertificate_checkDomainName_m4587,
	TlsServerCertificate_Match_m4588,
	TlsServerCertificateRequest__ctor_m4589,
	TlsServerCertificateRequest_Update_m4590,
	TlsServerCertificateRequest_ProcessAsSsl3_m4591,
	TlsServerCertificateRequest_ProcessAsTls1_m4592,
	TlsServerFinished__ctor_m4593,
	TlsServerFinished__cctor_m4594,
	TlsServerFinished_Update_m4595,
	TlsServerFinished_ProcessAsSsl3_m4596,
	TlsServerFinished_ProcessAsTls1_m4597,
	TlsServerHello__ctor_m4598,
	TlsServerHello_Update_m4599,
	TlsServerHello_ProcessAsSsl3_m4600,
	TlsServerHello_ProcessAsTls1_m4601,
	TlsServerHello_processProtocol_m4602,
	TlsServerHelloDone__ctor_m4603,
	TlsServerHelloDone_ProcessAsSsl3_m4604,
	TlsServerHelloDone_ProcessAsTls1_m4605,
	TlsServerKeyExchange__ctor_m4606,
	TlsServerKeyExchange_Update_m4607,
	TlsServerKeyExchange_ProcessAsSsl3_m4608,
	TlsServerKeyExchange_ProcessAsTls1_m4609,
	TlsServerKeyExchange_verifySignature_m4610,
	PrimalityTest__ctor_m4611,
	PrimalityTest_Invoke_m4612,
	PrimalityTest_BeginInvoke_m4613,
	PrimalityTest_EndInvoke_m4614,
	CertificateValidationCallback__ctor_m4615,
	CertificateValidationCallback_Invoke_m4616,
	CertificateValidationCallback_BeginInvoke_m4617,
	CertificateValidationCallback_EndInvoke_m4618,
	CertificateValidationCallback2__ctor_m4619,
	CertificateValidationCallback2_Invoke_m4620,
	CertificateValidationCallback2_BeginInvoke_m4621,
	CertificateValidationCallback2_EndInvoke_m4622,
	CertificateSelectionCallback__ctor_m4623,
	CertificateSelectionCallback_Invoke_m4624,
	CertificateSelectionCallback_BeginInvoke_m4625,
	CertificateSelectionCallback_EndInvoke_m4626,
	PrivateKeySelectionCallback__ctor_m4627,
	PrivateKeySelectionCallback_Invoke_m4628,
	PrivateKeySelectionCallback_BeginInvoke_m4629,
	PrivateKeySelectionCallback_EndInvoke_m4630,
	Locale_GetText_m4765,
	Locale_GetText_m4766,
	MonoTODOAttribute__ctor_m4767,
	MonoTODOAttribute__ctor_m4768,
	HybridDictionary__ctor_m4769,
	HybridDictionary__ctor_m4770,
	HybridDictionary_System_Collections_IEnumerable_GetEnumerator_m4771,
	HybridDictionary_get_inner_m4772,
	HybridDictionary_get_Count_m4773,
	HybridDictionary_get_IsSynchronized_m4774,
	HybridDictionary_get_Item_m4775,
	HybridDictionary_set_Item_m4776,
	HybridDictionary_get_SyncRoot_m4777,
	HybridDictionary_Add_m4778,
	HybridDictionary_Contains_m4779,
	HybridDictionary_CopyTo_m4780,
	HybridDictionary_GetEnumerator_m4781,
	HybridDictionary_Remove_m4782,
	HybridDictionary_Switch_m4783,
	DictionaryNode__ctor_m4784,
	DictionaryNodeEnumerator__ctor_m4785,
	DictionaryNodeEnumerator_FailFast_m4786,
	DictionaryNodeEnumerator_MoveNext_m4787,
	DictionaryNodeEnumerator_Reset_m4788,
	DictionaryNodeEnumerator_get_Current_m4789,
	DictionaryNodeEnumerator_get_DictionaryNode_m4790,
	DictionaryNodeEnumerator_get_Entry_m4791,
	DictionaryNodeEnumerator_get_Key_m4792,
	DictionaryNodeEnumerator_get_Value_m4793,
	ListDictionary__ctor_m4794,
	ListDictionary__ctor_m4795,
	ListDictionary_System_Collections_IEnumerable_GetEnumerator_m4796,
	ListDictionary_FindEntry_m4797,
	ListDictionary_FindEntry_m4798,
	ListDictionary_AddImpl_m4799,
	ListDictionary_get_Count_m4800,
	ListDictionary_get_IsSynchronized_m4801,
	ListDictionary_get_SyncRoot_m4802,
	ListDictionary_CopyTo_m4803,
	ListDictionary_get_Item_m4804,
	ListDictionary_set_Item_m4805,
	ListDictionary_Add_m4806,
	ListDictionary_Clear_m4807,
	ListDictionary_Contains_m4808,
	ListDictionary_GetEnumerator_m4809,
	ListDictionary_Remove_m4810,
	_Item__ctor_m4811,
	_KeysEnumerator__ctor_m4812,
	_KeysEnumerator_get_Current_m4813,
	_KeysEnumerator_MoveNext_m4814,
	_KeysEnumerator_Reset_m4815,
	KeysCollection__ctor_m4816,
	KeysCollection_System_Collections_ICollection_CopyTo_m4817,
	KeysCollection_System_Collections_ICollection_get_IsSynchronized_m4818,
	KeysCollection_System_Collections_ICollection_get_SyncRoot_m4819,
	KeysCollection_get_Count_m4820,
	KeysCollection_GetEnumerator_m4821,
	NameObjectCollectionBase__ctor_m4822,
	NameObjectCollectionBase__ctor_m4823,
	NameObjectCollectionBase_System_Collections_ICollection_get_IsSynchronized_m4824,
	NameObjectCollectionBase_System_Collections_ICollection_get_SyncRoot_m4825,
	NameObjectCollectionBase_System_Collections_ICollection_CopyTo_m4826,
	NameObjectCollectionBase_Init_m4827,
	NameObjectCollectionBase_get_Keys_m4828,
	NameObjectCollectionBase_GetEnumerator_m4829,
	NameObjectCollectionBase_GetObjectData_m4830,
	NameObjectCollectionBase_get_Count_m4831,
	NameObjectCollectionBase_OnDeserialization_m4832,
	NameObjectCollectionBase_get_IsReadOnly_m4833,
	NameObjectCollectionBase_BaseAdd_m4834,
	NameObjectCollectionBase_BaseGet_m4835,
	NameObjectCollectionBase_BaseGet_m4836,
	NameObjectCollectionBase_BaseGetKey_m4837,
	NameObjectCollectionBase_FindFirstMatchedItem_m4838,
	NameValueCollection__ctor_m4839,
	NameValueCollection__ctor_m4840,
	NameValueCollection_Add_m4841,
	NameValueCollection_Get_m4842,
	NameValueCollection_AsSingleString_m4843,
	NameValueCollection_GetKey_m4844,
	NameValueCollection_InvalidateCachedArrays_m4845,
	EditorBrowsableAttribute__ctor_m4846,
	EditorBrowsableAttribute_get_State_m4847,
	EditorBrowsableAttribute_Equals_m4848,
	EditorBrowsableAttribute_GetHashCode_m4849,
	TypeConverterAttribute__ctor_m4850,
	TypeConverterAttribute__ctor_m4851,
	TypeConverterAttribute__cctor_m4852,
	TypeConverterAttribute_Equals_m4853,
	TypeConverterAttribute_GetHashCode_m4854,
	TypeConverterAttribute_get_ConverterTypeName_m4855,
	DefaultCertificatePolicy__ctor_m4856,
	DefaultCertificatePolicy_CheckValidationResult_m4857,
	FileWebRequest__ctor_m4858,
	FileWebRequest__ctor_m4859,
	FileWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4860,
	FileWebRequest_GetObjectData_m4861,
	FileWebRequestCreator__ctor_m4862,
	FileWebRequestCreator_Create_m4863,
	FtpRequestCreator__ctor_m4864,
	FtpRequestCreator_Create_m4865,
	FtpWebRequest__ctor_m4866,
	FtpWebRequest__cctor_m4867,
	FtpWebRequest_U3CcallbackU3Em__B_m4868,
	GlobalProxySelection_get_Select_m4869,
	HttpRequestCreator__ctor_m4870,
	HttpRequestCreator_Create_m4871,
	HttpVersion__cctor_m4872,
	HttpWebRequest__ctor_m4873,
	HttpWebRequest__ctor_m4874,
	HttpWebRequest__cctor_m4875,
	HttpWebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4876,
	HttpWebRequest_get_Address_m4723,
	HttpWebRequest_get_ServicePoint_m4727,
	HttpWebRequest_GetServicePoint_m4877,
	HttpWebRequest_GetObjectData_m4878,
	IPAddress__ctor_m4879,
	IPAddress__ctor_m4880,
	IPAddress__cctor_m4881,
	IPAddress_SwapShort_m4882,
	IPAddress_HostToNetworkOrder_m4883,
	IPAddress_NetworkToHostOrder_m4884,
	IPAddress_Parse_m4885,
	IPAddress_TryParse_m4886,
	IPAddress_ParseIPV4_m4887,
	IPAddress_ParseIPV6_m4888,
	IPAddress_get_InternalIPv4Address_m4889,
	IPAddress_get_ScopeId_m4890,
	IPAddress_get_AddressFamily_m4891,
	IPAddress_IsLoopback_m4892,
	IPAddress_ToString_m4893,
	IPAddress_ToString_m4894,
	IPAddress_Equals_m4895,
	IPAddress_GetHashCode_m4896,
	IPAddress_Hash_m4897,
	IPv6Address__ctor_m4898,
	IPv6Address__ctor_m4899,
	IPv6Address__ctor_m4900,
	IPv6Address__cctor_m4901,
	IPv6Address_Parse_m4902,
	IPv6Address_Fill_m4903,
	IPv6Address_TryParse_m4904,
	IPv6Address_TryParse_m4905,
	IPv6Address_get_Address_m4906,
	IPv6Address_get_ScopeId_m4907,
	IPv6Address_set_ScopeId_m4908,
	IPv6Address_IsLoopback_m4909,
	IPv6Address_SwapUShort_m4910,
	IPv6Address_AsIPv4Int_m4911,
	IPv6Address_IsIPv4Compatible_m4912,
	IPv6Address_IsIPv4Mapped_m4913,
	IPv6Address_ToString_m4914,
	IPv6Address_ToString_m4915,
	IPv6Address_Equals_m4916,
	IPv6Address_GetHashCode_m4917,
	IPv6Address_Hash_m4918,
	ServicePoint__ctor_m4919,
	ServicePoint_get_Address_m4920,
	ServicePoint_get_CurrentConnections_m4921,
	ServicePoint_get_IdleSince_m4922,
	ServicePoint_set_IdleSince_m4923,
	ServicePoint_set_Expect100Continue_m4924,
	ServicePoint_set_UseNagleAlgorithm_m4925,
	ServicePoint_set_SendContinue_m4926,
	ServicePoint_set_UsesProxy_m4927,
	ServicePoint_set_UseConnect_m4928,
	ServicePoint_get_AvailableForRecycling_m4929,
	SPKey__ctor_m4930,
	SPKey_GetHashCode_m4931,
	SPKey_Equals_m4932,
	ServicePointManager__cctor_m4933,
	ServicePointManager_get_CertificatePolicy_m4726,
	ServicePointManager_get_CheckCertificateRevocationList_m4692,
	ServicePointManager_get_SecurityProtocol_m4725,
	ServicePointManager_get_ServerCertificateValidationCallback_m4728,
	ServicePointManager_FindServicePoint_m4934,
	ServicePointManager_RecycleServicePoints_m4935,
	WebHeaderCollection__ctor_m4936,
	WebHeaderCollection__ctor_m4937,
	WebHeaderCollection__ctor_m4938,
	WebHeaderCollection__cctor_m4939,
	WebHeaderCollection_System_Runtime_Serialization_ISerializable_GetObjectData_m4940,
	WebHeaderCollection_Add_m4941,
	WebHeaderCollection_AddWithoutValidate_m4942,
	WebHeaderCollection_IsRestricted_m4943,
	WebHeaderCollection_OnDeserialization_m4944,
	WebHeaderCollection_ToString_m4945,
	WebHeaderCollection_GetObjectData_m4946,
	WebHeaderCollection_get_Count_m4947,
	WebHeaderCollection_get_Keys_m4948,
	WebHeaderCollection_Get_m4949,
	WebHeaderCollection_GetKey_m4950,
	WebHeaderCollection_GetEnumerator_m4951,
	WebHeaderCollection_IsHeaderValue_m4952,
	WebHeaderCollection_IsHeaderName_m4953,
	WebProxy__ctor_m4954,
	WebProxy__ctor_m4955,
	WebProxy__ctor_m4956,
	WebProxy_System_Runtime_Serialization_ISerializable_GetObjectData_m4957,
	WebProxy_get_UseDefaultCredentials_m4958,
	WebProxy_GetProxy_m4959,
	WebProxy_IsBypassed_m4960,
	WebProxy_GetObjectData_m4961,
	WebProxy_CheckBypassList_m4962,
	WebRequest__ctor_m4963,
	WebRequest__ctor_m4964,
	WebRequest__cctor_m4965,
	WebRequest_System_Runtime_Serialization_ISerializable_GetObjectData_m4966,
	WebRequest_AddDynamicPrefix_m4967,
	WebRequest_GetMustImplement_m4968,
	WebRequest_get_DefaultWebProxy_m4969,
	WebRequest_GetDefaultWebProxy_m4970,
	WebRequest_GetObjectData_m4971,
	WebRequest_AddPrefix_m4972,
	PublicKey__ctor_m4973,
	PublicKey_get_EncodedKeyValue_m4974,
	PublicKey_get_EncodedParameters_m4975,
	PublicKey_get_Key_m4976,
	PublicKey_get_Oid_m4977,
	PublicKey_GetUnsignedBigInteger_m4978,
	PublicKey_DecodeDSA_m4979,
	PublicKey_DecodeRSA_m4980,
	X500DistinguishedName__ctor_m4981,
	X500DistinguishedName_Decode_m4982,
	X500DistinguishedName_GetSeparator_m4983,
	X500DistinguishedName_DecodeRawData_m4984,
	X500DistinguishedName_Canonize_m4985,
	X500DistinguishedName_AreEqual_m4986,
	X509BasicConstraintsExtension__ctor_m4987,
	X509BasicConstraintsExtension__ctor_m4988,
	X509BasicConstraintsExtension__ctor_m4989,
	X509BasicConstraintsExtension_get_CertificateAuthority_m4990,
	X509BasicConstraintsExtension_get_HasPathLengthConstraint_m4991,
	X509BasicConstraintsExtension_get_PathLengthConstraint_m4992,
	X509BasicConstraintsExtension_CopyFrom_m4993,
	X509BasicConstraintsExtension_Decode_m4994,
	X509BasicConstraintsExtension_Encode_m4995,
	X509BasicConstraintsExtension_ToString_m4996,
	X509Certificate2__ctor_m4729,
	X509Certificate2__cctor_m4997,
	X509Certificate2_get_Extensions_m4998,
	X509Certificate2_get_IssuerName_m4999,
	X509Certificate2_get_NotAfter_m5000,
	X509Certificate2_get_NotBefore_m5001,
	X509Certificate2_get_PrivateKey_m4734,
	X509Certificate2_get_PublicKey_m5002,
	X509Certificate2_get_SerialNumber_m5003,
	X509Certificate2_get_SignatureAlgorithm_m5004,
	X509Certificate2_get_SubjectName_m5005,
	X509Certificate2_get_Thumbprint_m5006,
	X509Certificate2_get_Version_m5007,
	X509Certificate2_GetNameInfo_m5008,
	X509Certificate2_Find_m5009,
	X509Certificate2_GetValueAsString_m5010,
	X509Certificate2_ImportPkcs12_m5011,
	X509Certificate2_Import_m5012,
	X509Certificate2_Reset_m5013,
	X509Certificate2_ToString_m5014,
	X509Certificate2_ToString_m5015,
	X509Certificate2_AppendBuffer_m5016,
	X509Certificate2_Verify_m5017,
	X509Certificate2_get_MonoCertificate_m5018,
	X509Certificate2Collection__ctor_m5019,
	X509Certificate2Collection__ctor_m5020,
	X509Certificate2Collection_get_Item_m5021,
	X509Certificate2Collection_Add_m5022,
	X509Certificate2Collection_AddRange_m5023,
	X509Certificate2Collection_Contains_m5024,
	X509Certificate2Collection_Find_m5025,
	X509Certificate2Collection_GetEnumerator_m5026,
	X509Certificate2Enumerator__ctor_m5027,
	X509Certificate2Enumerator_System_Collections_IEnumerator_get_Current_m5028,
	X509Certificate2Enumerator_System_Collections_IEnumerator_MoveNext_m5029,
	X509Certificate2Enumerator_System_Collections_IEnumerator_Reset_m5030,
	X509Certificate2Enumerator_get_Current_m5031,
	X509Certificate2Enumerator_MoveNext_m5032,
	X509Certificate2Enumerator_Reset_m5033,
	X509CertificateEnumerator__ctor_m5034,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m5035,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m5036,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m5037,
	X509CertificateEnumerator_get_Current_m4754,
	X509CertificateEnumerator_MoveNext_m5038,
	X509CertificateEnumerator_Reset_m5039,
	X509CertificateCollection__ctor_m4744,
	X509CertificateCollection__ctor_m4743,
	X509CertificateCollection_get_Item_m4733,
	X509CertificateCollection_AddRange_m5040,
	X509CertificateCollection_GetEnumerator_m4753,
	X509CertificateCollection_GetHashCode_m5041,
	X509Chain__ctor_m4730,
	X509Chain__ctor_m5042,
	X509Chain__cctor_m5043,
	X509Chain_get_ChainPolicy_m5044,
	X509Chain_Build_m4731,
	X509Chain_Reset_m5045,
	X509Chain_get_Roots_m5046,
	X509Chain_get_CertificateAuthorities_m5047,
	X509Chain_get_CertificateCollection_m5048,
	X509Chain_BuildChainFrom_m5049,
	X509Chain_SelectBestFromCollection_m5050,
	X509Chain_FindParent_m5051,
	X509Chain_IsChainComplete_m5052,
	X509Chain_IsSelfIssued_m5053,
	X509Chain_ValidateChain_m5054,
	X509Chain_Process_m5055,
	X509Chain_PrepareForNextCertificate_m5056,
	X509Chain_WrapUp_m5057,
	X509Chain_ProcessCertificateExtensions_m5058,
	X509Chain_IsSignedWith_m5059,
	X509Chain_GetSubjectKeyIdentifier_m5060,
	X509Chain_GetAuthorityKeyIdentifier_m5061,
	X509Chain_GetAuthorityKeyIdentifier_m5062,
	X509Chain_GetAuthorityKeyIdentifier_m5063,
	X509Chain_CheckRevocationOnChain_m5064,
	X509Chain_CheckRevocation_m5065,
	X509Chain_CheckRevocation_m5066,
	X509Chain_FindCrl_m5067,
	X509Chain_ProcessCrlExtensions_m5068,
	X509Chain_ProcessCrlEntryExtensions_m5069,
	X509ChainElement__ctor_m5070,
	X509ChainElement_get_Certificate_m5071,
	X509ChainElement_get_ChainElementStatus_m5072,
	X509ChainElement_get_StatusFlags_m5073,
	X509ChainElement_set_StatusFlags_m5074,
	X509ChainElement_Count_m5075,
	X509ChainElement_Set_m5076,
	X509ChainElement_UncompressFlags_m5077,
	X509ChainElementCollection__ctor_m5078,
	X509ChainElementCollection_System_Collections_ICollection_CopyTo_m5079,
	X509ChainElementCollection_System_Collections_IEnumerable_GetEnumerator_m5080,
	X509ChainElementCollection_get_Count_m5081,
	X509ChainElementCollection_get_IsSynchronized_m5082,
	X509ChainElementCollection_get_Item_m5083,
	X509ChainElementCollection_get_SyncRoot_m5084,
	X509ChainElementCollection_GetEnumerator_m5085,
	X509ChainElementCollection_Add_m5086,
	X509ChainElementCollection_Clear_m5087,
	X509ChainElementCollection_Contains_m5088,
	X509ChainElementEnumerator__ctor_m5089,
	X509ChainElementEnumerator_System_Collections_IEnumerator_get_Current_m5090,
	X509ChainElementEnumerator_get_Current_m5091,
	X509ChainElementEnumerator_MoveNext_m5092,
	X509ChainElementEnumerator_Reset_m5093,
	X509ChainPolicy__ctor_m5094,
	X509ChainPolicy_get_ExtraStore_m5095,
	X509ChainPolicy_get_RevocationFlag_m5096,
	X509ChainPolicy_get_RevocationMode_m5097,
	X509ChainPolicy_get_VerificationFlags_m5098,
	X509ChainPolicy_get_VerificationTime_m5099,
	X509ChainPolicy_Reset_m5100,
	X509ChainStatus__ctor_m5101,
	X509ChainStatus_get_Status_m5102,
	X509ChainStatus_set_Status_m5103,
	X509ChainStatus_set_StatusInformation_m5104,
	X509ChainStatus_GetInformation_m5105,
	X509EnhancedKeyUsageExtension__ctor_m5106,
	X509EnhancedKeyUsageExtension_CopyFrom_m5107,
	X509EnhancedKeyUsageExtension_Decode_m5108,
	X509EnhancedKeyUsageExtension_ToString_m5109,
	X509Extension__ctor_m5110,
	X509Extension__ctor_m5111,
	X509Extension_get_Critical_m5112,
	X509Extension_set_Critical_m5113,
	X509Extension_CopyFrom_m5114,
	X509Extension_FormatUnkownData_m5115,
	X509ExtensionCollection__ctor_m5116,
	X509ExtensionCollection_System_Collections_ICollection_CopyTo_m5117,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m5118,
	X509ExtensionCollection_get_Count_m5119,
	X509ExtensionCollection_get_IsSynchronized_m5120,
	X509ExtensionCollection_get_SyncRoot_m5121,
	X509ExtensionCollection_get_Item_m5122,
	X509ExtensionCollection_GetEnumerator_m5123,
	X509ExtensionEnumerator__ctor_m5124,
	X509ExtensionEnumerator_System_Collections_IEnumerator_get_Current_m5125,
	X509ExtensionEnumerator_get_Current_m5126,
	X509ExtensionEnumerator_MoveNext_m5127,
	X509ExtensionEnumerator_Reset_m5128,
	X509KeyUsageExtension__ctor_m5129,
	X509KeyUsageExtension__ctor_m5130,
	X509KeyUsageExtension__ctor_m5131,
	X509KeyUsageExtension_get_KeyUsages_m5132,
	X509KeyUsageExtension_CopyFrom_m5133,
	X509KeyUsageExtension_GetValidFlags_m5134,
	X509KeyUsageExtension_Decode_m5135,
	X509KeyUsageExtension_Encode_m5136,
	X509KeyUsageExtension_ToString_m5137,
	X509Store__ctor_m5138,
	X509Store_get_Certificates_m5139,
	X509Store_get_Factory_m5140,
	X509Store_get_Store_m5141,
	X509Store_Close_m5142,
	X509Store_Open_m5143,
	X509SubjectKeyIdentifierExtension__ctor_m5144,
	X509SubjectKeyIdentifierExtension__ctor_m5145,
	X509SubjectKeyIdentifierExtension__ctor_m5146,
	X509SubjectKeyIdentifierExtension__ctor_m5147,
	X509SubjectKeyIdentifierExtension__ctor_m5148,
	X509SubjectKeyIdentifierExtension__ctor_m5149,
	X509SubjectKeyIdentifierExtension_get_SubjectKeyIdentifier_m5150,
	X509SubjectKeyIdentifierExtension_CopyFrom_m5151,
	X509SubjectKeyIdentifierExtension_FromHexChar_m5152,
	X509SubjectKeyIdentifierExtension_FromHexChars_m5153,
	X509SubjectKeyIdentifierExtension_FromHex_m5154,
	X509SubjectKeyIdentifierExtension_Decode_m5155,
	X509SubjectKeyIdentifierExtension_Encode_m5156,
	X509SubjectKeyIdentifierExtension_ToString_m5157,
	AsnEncodedData__ctor_m5158,
	AsnEncodedData__ctor_m5159,
	AsnEncodedData__ctor_m5160,
	AsnEncodedData_get_Oid_m5161,
	AsnEncodedData_set_Oid_m5162,
	AsnEncodedData_get_RawData_m5163,
	AsnEncodedData_set_RawData_m5164,
	AsnEncodedData_CopyFrom_m5165,
	AsnEncodedData_ToString_m5166,
	AsnEncodedData_Default_m5167,
	AsnEncodedData_BasicConstraintsExtension_m5168,
	AsnEncodedData_EnhancedKeyUsageExtension_m5169,
	AsnEncodedData_KeyUsageExtension_m5170,
	AsnEncodedData_SubjectKeyIdentifierExtension_m5171,
	AsnEncodedData_SubjectAltName_m5172,
	AsnEncodedData_NetscapeCertType_m5173,
	Oid__ctor_m5174,
	Oid__ctor_m5175,
	Oid__ctor_m5176,
	Oid__ctor_m5177,
	Oid_get_FriendlyName_m5178,
	Oid_get_Value_m5179,
	Oid_GetName_m5180,
	OidCollection__ctor_m5181,
	OidCollection_System_Collections_ICollection_CopyTo_m5182,
	OidCollection_System_Collections_IEnumerable_GetEnumerator_m5183,
	OidCollection_get_Count_m5184,
	OidCollection_get_IsSynchronized_m5185,
	OidCollection_get_Item_m5186,
	OidCollection_get_SyncRoot_m5187,
	OidCollection_Add_m5188,
	OidEnumerator__ctor_m5189,
	OidEnumerator_System_Collections_IEnumerator_get_Current_m5190,
	OidEnumerator_MoveNext_m5191,
	OidEnumerator_Reset_m5192,
	MatchAppendEvaluator__ctor_m5193,
	MatchAppendEvaluator_Invoke_m5194,
	MatchAppendEvaluator_BeginInvoke_m5195,
	MatchAppendEvaluator_EndInvoke_m5196,
	BaseMachine__ctor_m5197,
	BaseMachine_Replace_m5198,
	BaseMachine_Scan_m5199,
	BaseMachine_LTRReplace_m5200,
	BaseMachine_RTLReplace_m5201,
	Capture__ctor_m5202,
	Capture__ctor_m5203,
	Capture_get_Index_m5204,
	Capture_get_Length_m5205,
	Capture_get_Value_m4761,
	Capture_ToString_m5206,
	Capture_get_Text_m5207,
	CaptureCollection__ctor_m5208,
	CaptureCollection_get_Count_m5209,
	CaptureCollection_get_IsSynchronized_m5210,
	CaptureCollection_SetValue_m5211,
	CaptureCollection_get_SyncRoot_m5212,
	CaptureCollection_CopyTo_m5213,
	CaptureCollection_GetEnumerator_m5214,
	Group__ctor_m5215,
	Group__ctor_m5216,
	Group__ctor_m5217,
	Group__cctor_m5218,
	Group_get_Captures_m5219,
	Group_get_Success_m4759,
	GroupCollection__ctor_m5220,
	GroupCollection_get_Count_m5221,
	GroupCollection_get_IsSynchronized_m5222,
	GroupCollection_get_Item_m4760,
	GroupCollection_SetValue_m5223,
	GroupCollection_get_SyncRoot_m5224,
	GroupCollection_CopyTo_m5225,
	GroupCollection_GetEnumerator_m5226,
	Match__ctor_m5227,
	Match__ctor_m5228,
	Match__ctor_m5229,
	Match__cctor_m5230,
	Match_get_Empty_m5231,
	Match_get_Groups_m5232,
	Match_NextMatch_m5233,
	Match_get_Regex_m5234,
	Enumerator__ctor_m5235,
	Enumerator_System_Collections_IEnumerator_Reset_m5236,
	Enumerator_System_Collections_IEnumerator_get_Current_m5237,
	Enumerator_System_Collections_IEnumerator_MoveNext_m5238,
	MatchCollection__ctor_m5239,
	MatchCollection_get_Count_m5240,
	MatchCollection_get_IsSynchronized_m5241,
	MatchCollection_get_Item_m5242,
	MatchCollection_get_SyncRoot_m5243,
	MatchCollection_CopyTo_m5244,
	MatchCollection_GetEnumerator_m5245,
	MatchCollection_TryToGet_m5246,
	MatchCollection_get_FullList_m5247,
	Regex__ctor_m5248,
	Regex__ctor_m4757,
	Regex__ctor_m5249,
	Regex__ctor_m5250,
	Regex__cctor_m5251,
	Regex_System_Runtime_Serialization_ISerializable_GetObjectData_m5252,
	Regex_Replace_m3656,
	Regex_Replace_m5253,
	Regex_validate_options_m5254,
	Regex_Init_m5255,
	Regex_InitNewRegex_m5256,
	Regex_CreateMachineFactory_m5257,
	Regex_get_Options_m5258,
	Regex_get_RightToLeft_m5259,
	Regex_GroupNumberFromName_m5260,
	Regex_GetGroupIndex_m5261,
	Regex_default_startat_m5262,
	Regex_IsMatch_m5263,
	Regex_IsMatch_m5264,
	Regex_Match_m5265,
	Regex_Matches_m4758,
	Regex_Matches_m5266,
	Regex_Replace_m5267,
	Regex_Replace_m5268,
	Regex_ToString_m5269,
	Regex_get_GroupCount_m5270,
	Regex_get_Gap_m5271,
	Regex_CreateMachine_m5272,
	Regex_GetGroupNamesArray_m5273,
	Regex_get_GroupNumbers_m5274,
	Key__ctor_m5275,
	Key_GetHashCode_m5276,
	Key_Equals_m5277,
	Key_ToString_m5278,
	FactoryCache__ctor_m5279,
	FactoryCache_Add_m5280,
	FactoryCache_Cleanup_m5281,
	FactoryCache_Lookup_m5282,
	Node__ctor_m5283,
	MRUList__ctor_m5284,
	MRUList_Use_m5285,
	MRUList_Evict_m5286,
	CategoryUtils_CategoryFromName_m5287,
	CategoryUtils_IsCategory_m5288,
	CategoryUtils_IsCategory_m5289,
	LinkRef__ctor_m5290,
	InterpreterFactory__ctor_m5291,
	InterpreterFactory_NewInstance_m5292,
	InterpreterFactory_get_GroupCount_m5293,
	InterpreterFactory_get_Gap_m5294,
	InterpreterFactory_set_Gap_m5295,
	InterpreterFactory_get_Mapping_m5296,
	InterpreterFactory_set_Mapping_m5297,
	InterpreterFactory_get_NamesMapping_m5298,
	InterpreterFactory_set_NamesMapping_m5299,
	PatternLinkStack__ctor_m5300,
	PatternLinkStack_set_BaseAddress_m5301,
	PatternLinkStack_get_OffsetAddress_m5302,
	PatternLinkStack_set_OffsetAddress_m5303,
	PatternLinkStack_GetOffset_m5304,
	PatternLinkStack_GetCurrent_m5305,
	PatternLinkStack_SetCurrent_m5306,
	PatternCompiler__ctor_m5307,
	PatternCompiler_EncodeOp_m5308,
	PatternCompiler_GetMachineFactory_m5309,
	PatternCompiler_EmitFalse_m5310,
	PatternCompiler_EmitTrue_m5311,
	PatternCompiler_EmitCount_m5312,
	PatternCompiler_EmitCharacter_m5313,
	PatternCompiler_EmitCategory_m5314,
	PatternCompiler_EmitNotCategory_m5315,
	PatternCompiler_EmitRange_m5316,
	PatternCompiler_EmitSet_m5317,
	PatternCompiler_EmitString_m5318,
	PatternCompiler_EmitPosition_m5319,
	PatternCompiler_EmitOpen_m5320,
	PatternCompiler_EmitClose_m5321,
	PatternCompiler_EmitBalanceStart_m5322,
	PatternCompiler_EmitBalance_m5323,
	PatternCompiler_EmitReference_m5324,
	PatternCompiler_EmitIfDefined_m5325,
	PatternCompiler_EmitSub_m5326,
	PatternCompiler_EmitTest_m5327,
	PatternCompiler_EmitBranch_m5328,
	PatternCompiler_EmitJump_m5329,
	PatternCompiler_EmitRepeat_m5330,
	PatternCompiler_EmitUntil_m5331,
	PatternCompiler_EmitFastRepeat_m5332,
	PatternCompiler_EmitIn_m5333,
	PatternCompiler_EmitAnchor_m5334,
	PatternCompiler_EmitInfo_m5335,
	PatternCompiler_NewLink_m5336,
	PatternCompiler_ResolveLink_m5337,
	PatternCompiler_EmitBranchEnd_m5338,
	PatternCompiler_EmitAlternationEnd_m5339,
	PatternCompiler_MakeFlags_m5340,
	PatternCompiler_Emit_m5341,
	PatternCompiler_Emit_m5342,
	PatternCompiler_Emit_m5343,
	PatternCompiler_get_CurrentAddress_m5344,
	PatternCompiler_BeginLink_m5345,
	PatternCompiler_EmitLink_m5346,
	LinkStack__ctor_m5347,
	LinkStack_Push_m5348,
	LinkStack_Pop_m5349,
	Mark_get_IsDefined_m5350,
	Mark_get_Index_m5351,
	Mark_get_Length_m5352,
	IntStack_Pop_m5353,
	IntStack_Push_m5354,
	IntStack_get_Count_m5355,
	IntStack_set_Count_m5356,
	RepeatContext__ctor_m5357,
	RepeatContext_get_Count_m5358,
	RepeatContext_set_Count_m5359,
	RepeatContext_get_Start_m5360,
	RepeatContext_set_Start_m5361,
	RepeatContext_get_IsMinimum_m5362,
	RepeatContext_get_IsMaximum_m5363,
	RepeatContext_get_IsLazy_m5364,
	RepeatContext_get_Expression_m5365,
	RepeatContext_get_Previous_m5366,
	Interpreter__ctor_m5367,
	Interpreter_ReadProgramCount_m5368,
	Interpreter_Scan_m5369,
	Interpreter_Reset_m5370,
	Interpreter_Eval_m5371,
	Interpreter_EvalChar_m5372,
	Interpreter_TryMatch_m5373,
	Interpreter_IsPosition_m5374,
	Interpreter_IsWordChar_m5375,
	Interpreter_GetString_m5376,
	Interpreter_Open_m5377,
	Interpreter_Close_m5378,
	Interpreter_Balance_m5379,
	Interpreter_Checkpoint_m5380,
	Interpreter_Backtrack_m5381,
	Interpreter_ResetGroups_m5382,
	Interpreter_GetLastDefined_m5383,
	Interpreter_CreateMark_m5384,
	Interpreter_GetGroupInfo_m5385,
	Interpreter_PopulateGroup_m5386,
	Interpreter_GenerateMatch_m5387,
	Interval__ctor_m5388,
	Interval_get_Empty_m5389,
	Interval_get_IsDiscontiguous_m5390,
	Interval_get_IsSingleton_m5391,
	Interval_get_IsEmpty_m5392,
	Interval_get_Size_m5393,
	Interval_IsDisjoint_m5394,
	Interval_IsAdjacent_m5395,
	Interval_Contains_m5396,
	Interval_Contains_m5397,
	Interval_Intersects_m5398,
	Interval_Merge_m5399,
	Interval_CompareTo_m5400,
	Enumerator__ctor_m5401,
	Enumerator_get_Current_m5402,
	Enumerator_MoveNext_m5403,
	Enumerator_Reset_m5404,
	CostDelegate__ctor_m5405,
	CostDelegate_Invoke_m5406,
	CostDelegate_BeginInvoke_m5407,
	CostDelegate_EndInvoke_m5408,
	IntervalCollection__ctor_m5409,
	IntervalCollection_get_Item_m5410,
	IntervalCollection_Add_m5411,
	IntervalCollection_Normalize_m5412,
	IntervalCollection_GetMetaCollection_m5413,
	IntervalCollection_Optimize_m5414,
	IntervalCollection_get_Count_m5415,
	IntervalCollection_get_IsSynchronized_m5416,
	IntervalCollection_get_SyncRoot_m5417,
	IntervalCollection_CopyTo_m5418,
	IntervalCollection_GetEnumerator_m5419,
	Parser__ctor_m5420,
	Parser_ParseDecimal_m5421,
	Parser_ParseOctal_m5422,
	Parser_ParseHex_m5423,
	Parser_ParseNumber_m5424,
	Parser_ParseName_m5425,
	Parser_ParseRegularExpression_m5426,
	Parser_GetMapping_m5427,
	Parser_ParseGroup_m5428,
	Parser_ParseGroupingConstruct_m5429,
	Parser_ParseAssertionType_m5430,
	Parser_ParseOptions_m5431,
	Parser_ParseCharacterClass_m5432,
	Parser_ParseRepetitionBounds_m5433,
	Parser_ParseUnicodeCategory_m5434,
	Parser_ParseSpecial_m5435,
	Parser_ParseEscape_m5436,
	Parser_ParseName_m5437,
	Parser_IsNameChar_m5438,
	Parser_ParseNumber_m5439,
	Parser_ParseDigit_m5440,
	Parser_ConsumeWhitespace_m5441,
	Parser_ResolveReferences_m5442,
	Parser_HandleExplicitNumericGroups_m5443,
	Parser_IsIgnoreCase_m5444,
	Parser_IsMultiline_m5445,
	Parser_IsExplicitCapture_m5446,
	Parser_IsSingleline_m5447,
	Parser_IsIgnorePatternWhitespace_m5448,
	Parser_IsECMAScript_m5449,
	Parser_NewParseException_m5450,
	QuickSearch__ctor_m5451,
	QuickSearch__cctor_m5452,
	QuickSearch_get_Length_m5453,
	QuickSearch_Search_m5454,
	QuickSearch_SetupShiftTable_m5455,
	QuickSearch_GetShiftDistance_m5456,
	QuickSearch_GetChar_m5457,
	ReplacementEvaluator__ctor_m5458,
	ReplacementEvaluator_Evaluate_m5459,
	ReplacementEvaluator_EvaluateAppend_m5460,
	ReplacementEvaluator_get_NeedsGroupsOrCaptures_m5461,
	ReplacementEvaluator_Ensure_m5462,
	ReplacementEvaluator_AddFromReplacement_m5463,
	ReplacementEvaluator_AddInt_m5464,
	ReplacementEvaluator_Compile_m5465,
	ReplacementEvaluator_CompileTerm_m5466,
	ExpressionCollection__ctor_m5467,
	ExpressionCollection_Add_m5468,
	ExpressionCollection_get_Item_m5469,
	ExpressionCollection_set_Item_m5470,
	ExpressionCollection_OnValidate_m5471,
	Expression__ctor_m5472,
	Expression_GetFixedWidth_m5473,
	Expression_GetAnchorInfo_m5474,
	CompositeExpression__ctor_m5475,
	CompositeExpression_get_Expressions_m5476,
	CompositeExpression_GetWidth_m5477,
	CompositeExpression_IsComplex_m5478,
	Group__ctor_m5479,
	Group_AppendExpression_m5480,
	Group_Compile_m5481,
	Group_GetWidth_m5482,
	Group_GetAnchorInfo_m5483,
	RegularExpression__ctor_m5484,
	RegularExpression_set_GroupCount_m5485,
	RegularExpression_Compile_m5486,
	CapturingGroup__ctor_m5487,
	CapturingGroup_get_Index_m5488,
	CapturingGroup_set_Index_m5489,
	CapturingGroup_get_Name_m5490,
	CapturingGroup_set_Name_m5491,
	CapturingGroup_get_IsNamed_m5492,
	CapturingGroup_Compile_m5493,
	CapturingGroup_IsComplex_m5494,
	CapturingGroup_CompareTo_m5495,
	BalancingGroup__ctor_m5496,
	BalancingGroup_set_Balance_m5497,
	BalancingGroup_Compile_m5498,
	NonBacktrackingGroup__ctor_m5499,
	NonBacktrackingGroup_Compile_m5500,
	NonBacktrackingGroup_IsComplex_m5501,
	Repetition__ctor_m5502,
	Repetition_get_Expression_m5503,
	Repetition_set_Expression_m5504,
	Repetition_get_Minimum_m5505,
	Repetition_Compile_m5506,
	Repetition_GetWidth_m5507,
	Repetition_GetAnchorInfo_m5508,
	Assertion__ctor_m5509,
	Assertion_get_TrueExpression_m5510,
	Assertion_set_TrueExpression_m5511,
	Assertion_get_FalseExpression_m5512,
	Assertion_set_FalseExpression_m5513,
	Assertion_GetWidth_m5514,
	CaptureAssertion__ctor_m5515,
	CaptureAssertion_set_CapturingGroup_m5516,
	CaptureAssertion_Compile_m5517,
	CaptureAssertion_IsComplex_m5518,
	CaptureAssertion_get_Alternate_m5519,
	ExpressionAssertion__ctor_m5520,
	ExpressionAssertion_set_Reverse_m5521,
	ExpressionAssertion_set_Negate_m5522,
	ExpressionAssertion_get_TestExpression_m5523,
	ExpressionAssertion_set_TestExpression_m5524,
	ExpressionAssertion_Compile_m5525,
	ExpressionAssertion_IsComplex_m5526,
	Alternation__ctor_m5527,
	Alternation_get_Alternatives_m5528,
	Alternation_AddAlternative_m5529,
	Alternation_Compile_m5530,
	Alternation_GetWidth_m5531,
	Literal__ctor_m5532,
	Literal_CompileLiteral_m5533,
	Literal_Compile_m5534,
	Literal_GetWidth_m5535,
	Literal_GetAnchorInfo_m5536,
	Literal_IsComplex_m5537,
	PositionAssertion__ctor_m5538,
	PositionAssertion_Compile_m5539,
	PositionAssertion_GetWidth_m5540,
	PositionAssertion_IsComplex_m5541,
	PositionAssertion_GetAnchorInfo_m5542,
	Reference__ctor_m5543,
	Reference_get_CapturingGroup_m5544,
	Reference_set_CapturingGroup_m5545,
	Reference_get_IgnoreCase_m5546,
	Reference_Compile_m5547,
	Reference_GetWidth_m5548,
	Reference_IsComplex_m5549,
	BackslashNumber__ctor_m5550,
	BackslashNumber_ResolveReference_m5551,
	BackslashNumber_Compile_m5552,
	CharacterClass__ctor_m5553,
	CharacterClass__ctor_m5554,
	CharacterClass__cctor_m5555,
	CharacterClass_AddCategory_m5556,
	CharacterClass_AddCharacter_m5557,
	CharacterClass_AddRange_m5558,
	CharacterClass_Compile_m5559,
	CharacterClass_GetWidth_m5560,
	CharacterClass_IsComplex_m5561,
	CharacterClass_GetIntervalCost_m5562,
	AnchorInfo__ctor_m5563,
	AnchorInfo__ctor_m5564,
	AnchorInfo__ctor_m5565,
	AnchorInfo_get_Offset_m5566,
	AnchorInfo_get_Width_m5567,
	AnchorInfo_get_Length_m5568,
	AnchorInfo_get_IsUnknownWidth_m5569,
	AnchorInfo_get_IsComplete_m5570,
	AnchorInfo_get_Substring_m5571,
	AnchorInfo_get_IgnoreCase_m5572,
	AnchorInfo_get_Position_m5573,
	AnchorInfo_get_IsSubstring_m5574,
	AnchorInfo_get_IsPosition_m5575,
	AnchorInfo_GetInterval_m5576,
	DefaultUriParser__ctor_m5577,
	DefaultUriParser__ctor_m5578,
	UriScheme__ctor_m5579,
	Uri__ctor_m5580,
	Uri__ctor_m5581,
	Uri__ctor_m5582,
	Uri__cctor_m5583,
	Uri_System_Runtime_Serialization_ISerializable_GetObjectData_m5584,
	Uri_get_AbsoluteUri_m5585,
	Uri_get_Authority_m5586,
	Uri_get_Host_m4724,
	Uri_get_IsFile_m5587,
	Uri_get_IsLoopback_m5588,
	Uri_get_IsUnc_m5589,
	Uri_get_Scheme_m5590,
	Uri_get_IsAbsoluteUri_m5591,
	Uri_CheckHostName_m5592,
	Uri_IsIPv4Address_m5593,
	Uri_IsDomainAddress_m5594,
	Uri_CheckSchemeName_m5595,
	Uri_IsAlpha_m5596,
	Uri_Equals_m5597,
	Uri_InternalEquals_m5598,
	Uri_GetHashCode_m5599,
	Uri_GetLeftPart_m5600,
	Uri_FromHex_m5601,
	Uri_HexEscape_m5602,
	Uri_IsHexDigit_m5603,
	Uri_IsHexEncoding_m5604,
	Uri_AppendQueryAndFragment_m5605,
	Uri_ToString_m5606,
	Uri_EscapeString_m5607,
	Uri_EscapeString_m5608,
	Uri_ParseUri_m5609,
	Uri_Unescape_m5610,
	Uri_Unescape_m5611,
	Uri_ParseAsWindowsUNC_m5612,
	Uri_ParseAsWindowsAbsoluteFilePath_m5613,
	Uri_ParseAsUnixAbsoluteFilePath_m5614,
	Uri_Parse_m5615,
	Uri_ParseNoExceptions_m5616,
	Uri_CompactEscaped_m5617,
	Uri_Reduce_m5618,
	Uri_HexUnescapeMultiByte_m5619,
	Uri_GetSchemeDelimiter_m5620,
	Uri_GetDefaultPort_m5621,
	Uri_GetOpaqueWiseSchemeDelimiter_m5622,
	Uri_IsPredefinedScheme_m5623,
	Uri_get_Parser_m5624,
	Uri_EnsureAbsoluteUri_m5625,
	Uri_op_Equality_m5626,
	UriFormatException__ctor_m5627,
	UriFormatException__ctor_m5628,
	UriFormatException__ctor_m5629,
	UriFormatException_System_Runtime_Serialization_ISerializable_GetObjectData_m5630,
	UriParser__ctor_m5631,
	UriParser__cctor_m5632,
	UriParser_InitializeAndValidate_m5633,
	UriParser_OnRegister_m5634,
	UriParser_set_SchemeName_m5635,
	UriParser_get_DefaultPort_m5636,
	UriParser_set_DefaultPort_m5637,
	UriParser_CreateDefaults_m5638,
	UriParser_InternalRegister_m5639,
	UriParser_GetParser_m5640,
	RemoteCertificateValidationCallback__ctor_m5641,
	RemoteCertificateValidationCallback_Invoke_m4732,
	RemoteCertificateValidationCallback_BeginInvoke_m5642,
	RemoteCertificateValidationCallback_EndInvoke_m5643,
	MatchEvaluator__ctor_m5644,
	MatchEvaluator_Invoke_m5645,
	MatchEvaluator_BeginInvoke_m5646,
	MatchEvaluator_EndInvoke_m5647,
	Object__ctor_m220,
	Object_Equals_m5736,
	Object_Equals_m5733,
	Object_Finalize_m3578,
	Object_GetHashCode_m5737,
	Object_GetType_m1927,
	Object_MemberwiseClone_m5738,
	Object_ToString_m3672,
	Object_ReferenceEquals_m3617,
	Object_InternalGetHashCode_m5739,
	ValueType__ctor_m5740,
	ValueType_InternalEquals_m5741,
	ValueType_DefaultEquals_m5742,
	ValueType_Equals_m5743,
	ValueType_InternalGetHashCode_m5744,
	ValueType_GetHashCode_m5745,
	ValueType_ToString_m5746,
	Attribute__ctor_m3602,
	Attribute_CheckParameters_m5747,
	Attribute_GetCustomAttribute_m5748,
	Attribute_GetCustomAttribute_m5749,
	Attribute_GetHashCode_m3673,
	Attribute_IsDefined_m5750,
	Attribute_IsDefined_m5751,
	Attribute_IsDefined_m5752,
	Attribute_IsDefined_m5753,
	Attribute_Equals_m5754,
	Int32_System_IConvertible_ToBoolean_m5755,
	Int32_System_IConvertible_ToByte_m5756,
	Int32_System_IConvertible_ToChar_m5757,
	Int32_System_IConvertible_ToDateTime_m5758,
	Int32_System_IConvertible_ToDecimal_m5759,
	Int32_System_IConvertible_ToDouble_m5760,
	Int32_System_IConvertible_ToInt16_m5761,
	Int32_System_IConvertible_ToInt32_m5762,
	Int32_System_IConvertible_ToInt64_m5763,
	Int32_System_IConvertible_ToSByte_m5764,
	Int32_System_IConvertible_ToSingle_m5765,
	Int32_System_IConvertible_ToType_m5766,
	Int32_System_IConvertible_ToUInt16_m5767,
	Int32_System_IConvertible_ToUInt32_m5768,
	Int32_System_IConvertible_ToUInt64_m5769,
	Int32_CompareTo_m5770,
	Int32_Equals_m5771,
	Int32_GetHashCode_m3592,
	Int32_CompareTo_m1865,
	Int32_Equals_m3594,
	Int32_ProcessTrailingWhitespace_m5772,
	Int32_Parse_m5773,
	Int32_Parse_m5774,
	Int32_CheckStyle_m5775,
	Int32_JumpOverWhite_m5776,
	Int32_FindSign_m5777,
	Int32_FindCurrency_m5778,
	Int32_FindExponent_m5779,
	Int32_FindOther_m5780,
	Int32_ValidDigit_m5781,
	Int32_GetFormatException_m5782,
	Int32_Parse_m5783,
	Int32_Parse_m4717,
	Int32_Parse_m5784,
	Int32_TryParse_m5785,
	Int32_TryParse_m5676,
	Int32_ToString_m3650,
	Int32_ToString_m4701,
	Int32_ToString_m5709,
	Int32_ToString_m4705,
	Int32_GetTypeCode_m5786,
	SerializableAttribute__ctor_m5787,
	AttributeUsageAttribute__ctor_m5788,
	AttributeUsageAttribute_get_AllowMultiple_m5789,
	AttributeUsageAttribute_set_AllowMultiple_m5790,
	AttributeUsageAttribute_get_Inherited_m5791,
	AttributeUsageAttribute_set_Inherited_m5792,
	ComVisibleAttribute__ctor_m5793,
	Int64_System_IConvertible_ToBoolean_m5794,
	Int64_System_IConvertible_ToByte_m5795,
	Int64_System_IConvertible_ToChar_m5796,
	Int64_System_IConvertible_ToDateTime_m5797,
	Int64_System_IConvertible_ToDecimal_m5798,
	Int64_System_IConvertible_ToDouble_m5799,
	Int64_System_IConvertible_ToInt16_m5800,
	Int64_System_IConvertible_ToInt32_m5801,
	Int64_System_IConvertible_ToInt64_m5802,
	Int64_System_IConvertible_ToSByte_m5803,
	Int64_System_IConvertible_ToSingle_m5804,
	Int64_System_IConvertible_ToType_m5805,
	Int64_System_IConvertible_ToUInt16_m5806,
	Int64_System_IConvertible_ToUInt32_m5807,
	Int64_System_IConvertible_ToUInt64_m5808,
	Int64_CompareTo_m5809,
	Int64_Equals_m5810,
	Int64_GetHashCode_m5811,
	Int64_CompareTo_m5812,
	Int64_Equals_m5813,
	Int64_Parse_m5814,
	Int64_Parse_m5815,
	Int64_Parse_m5816,
	Int64_Parse_m5817,
	Int64_Parse_m5818,
	Int64_TryParse_m5819,
	Int64_TryParse_m5673,
	Int64_ToString_m5674,
	Int64_ToString_m5820,
	Int64_ToString_m5821,
	Int64_ToString_m5822,
	UInt32_System_IConvertible_ToBoolean_m5823,
	UInt32_System_IConvertible_ToByte_m5824,
	UInt32_System_IConvertible_ToChar_m5825,
	UInt32_System_IConvertible_ToDateTime_m5826,
	UInt32_System_IConvertible_ToDecimal_m5827,
	UInt32_System_IConvertible_ToDouble_m5828,
	UInt32_System_IConvertible_ToInt16_m5829,
	UInt32_System_IConvertible_ToInt32_m5830,
	UInt32_System_IConvertible_ToInt64_m5831,
	UInt32_System_IConvertible_ToSByte_m5832,
	UInt32_System_IConvertible_ToSingle_m5833,
	UInt32_System_IConvertible_ToType_m5834,
	UInt32_System_IConvertible_ToUInt16_m5835,
	UInt32_System_IConvertible_ToUInt32_m5836,
	UInt32_System_IConvertible_ToUInt64_m5837,
	UInt32_CompareTo_m5838,
	UInt32_Equals_m5839,
	UInt32_GetHashCode_m5840,
	UInt32_CompareTo_m5841,
	UInt32_Equals_m5842,
	UInt32_Parse_m5843,
	UInt32_Parse_m5844,
	UInt32_Parse_m5845,
	UInt32_Parse_m5846,
	UInt32_TryParse_m5727,
	UInt32_TryParse_m5847,
	UInt32_ToString_m5848,
	UInt32_ToString_m5849,
	UInt32_ToString_m5850,
	UInt32_ToString_m5851,
	CLSCompliantAttribute__ctor_m5852,
	UInt64_System_IConvertible_ToBoolean_m5853,
	UInt64_System_IConvertible_ToByte_m5854,
	UInt64_System_IConvertible_ToChar_m5855,
	UInt64_System_IConvertible_ToDateTime_m5856,
	UInt64_System_IConvertible_ToDecimal_m5857,
	UInt64_System_IConvertible_ToDouble_m5858,
	UInt64_System_IConvertible_ToInt16_m5859,
	UInt64_System_IConvertible_ToInt32_m5860,
	UInt64_System_IConvertible_ToInt64_m5861,
	UInt64_System_IConvertible_ToSByte_m5862,
	UInt64_System_IConvertible_ToSingle_m5863,
	UInt64_System_IConvertible_ToType_m5864,
	UInt64_System_IConvertible_ToUInt16_m5865,
	UInt64_System_IConvertible_ToUInt32_m5866,
	UInt64_System_IConvertible_ToUInt64_m5867,
	UInt64_CompareTo_m5868,
	UInt64_Equals_m5869,
	UInt64_GetHashCode_m5870,
	UInt64_CompareTo_m5871,
	UInt64_Equals_m5872,
	UInt64_Parse_m5873,
	UInt64_Parse_m5874,
	UInt64_Parse_m5875,
	UInt64_TryParse_m5876,
	UInt64_ToString_m5877,
	UInt64_ToString_m4646,
	UInt64_ToString_m5878,
	UInt64_ToString_m5879,
	Byte_System_IConvertible_ToType_m5880,
	Byte_System_IConvertible_ToBoolean_m5881,
	Byte_System_IConvertible_ToByte_m5882,
	Byte_System_IConvertible_ToChar_m5883,
	Byte_System_IConvertible_ToDateTime_m5884,
	Byte_System_IConvertible_ToDecimal_m5885,
	Byte_System_IConvertible_ToDouble_m5886,
	Byte_System_IConvertible_ToInt16_m5887,
	Byte_System_IConvertible_ToInt32_m5888,
	Byte_System_IConvertible_ToInt64_m5889,
	Byte_System_IConvertible_ToSByte_m5890,
	Byte_System_IConvertible_ToSingle_m5891,
	Byte_System_IConvertible_ToUInt16_m5892,
	Byte_System_IConvertible_ToUInt32_m5893,
	Byte_System_IConvertible_ToUInt64_m5894,
	Byte_CompareTo_m5895,
	Byte_Equals_m5896,
	Byte_GetHashCode_m5897,
	Byte_CompareTo_m5898,
	Byte_Equals_m5899,
	Byte_Parse_m5900,
	Byte_Parse_m5901,
	Byte_Parse_m5902,
	Byte_TryParse_m5903,
	Byte_TryParse_m5904,
	Byte_ToString_m4702,
	Byte_ToString_m4636,
	Byte_ToString_m4645,
	Byte_ToString_m4651,
	SByte_System_IConvertible_ToBoolean_m5905,
	SByte_System_IConvertible_ToByte_m5906,
	SByte_System_IConvertible_ToChar_m5907,
	SByte_System_IConvertible_ToDateTime_m5908,
	SByte_System_IConvertible_ToDecimal_m5909,
	SByte_System_IConvertible_ToDouble_m5910,
	SByte_System_IConvertible_ToInt16_m5911,
	SByte_System_IConvertible_ToInt32_m5912,
	SByte_System_IConvertible_ToInt64_m5913,
	SByte_System_IConvertible_ToSByte_m5914,
	SByte_System_IConvertible_ToSingle_m5915,
	SByte_System_IConvertible_ToType_m5916,
	SByte_System_IConvertible_ToUInt16_m5917,
	SByte_System_IConvertible_ToUInt32_m5918,
	SByte_System_IConvertible_ToUInt64_m5919,
	SByte_CompareTo_m5920,
	SByte_Equals_m5921,
	SByte_GetHashCode_m5922,
	SByte_CompareTo_m5923,
	SByte_Equals_m5924,
	SByte_Parse_m5925,
	SByte_Parse_m5926,
	SByte_Parse_m5927,
	SByte_TryParse_m5928,
	SByte_ToString_m5929,
	SByte_ToString_m5930,
	SByte_ToString_m5931,
	SByte_ToString_m5932,
	Int16_System_IConvertible_ToBoolean_m5933,
	Int16_System_IConvertible_ToByte_m5934,
	Int16_System_IConvertible_ToChar_m5935,
	Int16_System_IConvertible_ToDateTime_m5936,
	Int16_System_IConvertible_ToDecimal_m5937,
	Int16_System_IConvertible_ToDouble_m5938,
	Int16_System_IConvertible_ToInt16_m5939,
	Int16_System_IConvertible_ToInt32_m5940,
	Int16_System_IConvertible_ToInt64_m5941,
	Int16_System_IConvertible_ToSByte_m5942,
	Int16_System_IConvertible_ToSingle_m5943,
	Int16_System_IConvertible_ToType_m5944,
	Int16_System_IConvertible_ToUInt16_m5945,
	Int16_System_IConvertible_ToUInt32_m5946,
	Int16_System_IConvertible_ToUInt64_m5947,
	Int16_CompareTo_m5948,
	Int16_Equals_m5949,
	Int16_GetHashCode_m5950,
	Int16_CompareTo_m5951,
	Int16_Equals_m5952,
	Int16_Parse_m5953,
	Int16_Parse_m5954,
	Int16_Parse_m5955,
	Int16_TryParse_m5956,
	Int16_ToString_m5957,
	Int16_ToString_m5958,
	Int16_ToString_m5959,
	Int16_ToString_m5960,
	UInt16_System_IConvertible_ToBoolean_m5961,
	UInt16_System_IConvertible_ToByte_m5962,
	UInt16_System_IConvertible_ToChar_m5963,
	UInt16_System_IConvertible_ToDateTime_m5964,
	UInt16_System_IConvertible_ToDecimal_m5965,
	UInt16_System_IConvertible_ToDouble_m5966,
	UInt16_System_IConvertible_ToInt16_m5967,
	UInt16_System_IConvertible_ToInt32_m5968,
	UInt16_System_IConvertible_ToInt64_m5969,
	UInt16_System_IConvertible_ToSByte_m5970,
	UInt16_System_IConvertible_ToSingle_m5971,
	UInt16_System_IConvertible_ToType_m5972,
	UInt16_System_IConvertible_ToUInt16_m5973,
	UInt16_System_IConvertible_ToUInt32_m5974,
	UInt16_System_IConvertible_ToUInt64_m5975,
	UInt16_CompareTo_m5976,
	UInt16_Equals_m5977,
	UInt16_GetHashCode_m5978,
	UInt16_CompareTo_m5979,
	UInt16_Equals_m5980,
	UInt16_Parse_m5981,
	UInt16_Parse_m5982,
	UInt16_TryParse_m5983,
	UInt16_TryParse_m5984,
	UInt16_ToString_m5985,
	UInt16_ToString_m5986,
	UInt16_ToString_m5987,
	UInt16_ToString_m5988,
	Char__cctor_m5989,
	Char_System_IConvertible_ToType_m5990,
	Char_System_IConvertible_ToBoolean_m5991,
	Char_System_IConvertible_ToByte_m5992,
	Char_System_IConvertible_ToChar_m5993,
	Char_System_IConvertible_ToDateTime_m5994,
	Char_System_IConvertible_ToDecimal_m5995,
	Char_System_IConvertible_ToDouble_m5996,
	Char_System_IConvertible_ToInt16_m5997,
	Char_System_IConvertible_ToInt32_m5998,
	Char_System_IConvertible_ToInt64_m5999,
	Char_System_IConvertible_ToSByte_m6000,
	Char_System_IConvertible_ToSingle_m6001,
	Char_System_IConvertible_ToUInt16_m6002,
	Char_System_IConvertible_ToUInt32_m6003,
	Char_System_IConvertible_ToUInt64_m6004,
	Char_GetDataTablePointers_m6005,
	Char_CompareTo_m6006,
	Char_Equals_m6007,
	Char_CompareTo_m6008,
	Char_Equals_m6009,
	Char_GetHashCode_m6010,
	Char_GetUnicodeCategory_m5718,
	Char_IsDigit_m5716,
	Char_IsLetter_m2254,
	Char_IsLetterOrDigit_m5715,
	Char_IsLower_m2255,
	Char_IsSurrogate_m6011,
	Char_IsUpper_m2257,
	Char_IsWhiteSpace_m5717,
	Char_IsWhiteSpace_m5690,
	Char_CheckParameter_m6012,
	Char_Parse_m6013,
	Char_ToLower_m2258,
	Char_ToLowerInvariant_m6014,
	Char_ToLower_m6015,
	Char_ToUpper_m2256,
	Char_ToUpperInvariant_m5692,
	Char_ToString_m2233,
	Char_ToString_m6016,
	Char_GetTypeCode_m6017,
	String__ctor_m6018,
	String__ctor_m6019,
	String__ctor_m6020,
	String__ctor_m6021,
	String__cctor_m6022,
	String_System_IConvertible_ToBoolean_m6023,
	String_System_IConvertible_ToByte_m6024,
	String_System_IConvertible_ToChar_m6025,
	String_System_IConvertible_ToDateTime_m6026,
	String_System_IConvertible_ToDecimal_m6027,
	String_System_IConvertible_ToDouble_m6028,
	String_System_IConvertible_ToInt16_m6029,
	String_System_IConvertible_ToInt32_m6030,
	String_System_IConvertible_ToInt64_m6031,
	String_System_IConvertible_ToSByte_m6032,
	String_System_IConvertible_ToSingle_m6033,
	String_System_IConvertible_ToType_m6034,
	String_System_IConvertible_ToUInt16_m6035,
	String_System_IConvertible_ToUInt32_m6036,
	String_System_IConvertible_ToUInt64_m6037,
	String_System_Collections_Generic_IEnumerableU3CcharU3E_GetEnumerator_m6038,
	String_System_Collections_IEnumerable_GetEnumerator_m6039,
	String_Equals_m6040,
	String_Equals_m6041,
	String_Equals_m4667,
	String_get_Chars_m2209,
	String_Clone_m6042,
	String_CopyTo_m6043,
	String_ToCharArray_m5672,
	String_ToCharArray_m6044,
	String_Split_m3643,
	String_Split_m6045,
	String_Split_m6046,
	String_Split_m6047,
	String_Split_m5693,
	String_Substring_m2236,
	String_Substring_m2210,
	String_SubstringUnchecked_m6048,
	String_Trim_m3640,
	String_Trim_m6049,
	String_TrimStart_m5729,
	String_TrimEnd_m5691,
	String_FindNotWhiteSpace_m6050,
	String_FindNotInTable_m6051,
	String_Compare_m6052,
	String_Compare_m6053,
	String_Compare_m4762,
	String_Compare_m4764,
	String_CompareTo_m6054,
	String_CompareTo_m6055,
	String_CompareOrdinal_m6056,
	String_CompareOrdinalUnchecked_m6057,
	String_CompareOrdinalCaseInsensitiveUnchecked_m6058,
	String_EndsWith_m3645,
	String_IndexOfAny_m6059,
	String_IndexOfAny_m2228,
	String_IndexOfAny_m4675,
	String_IndexOfAnyUnchecked_m6060,
	String_IndexOf_m5704,
	String_IndexOf_m6061,
	String_IndexOfOrdinal_m6062,
	String_IndexOfOrdinalUnchecked_m6063,
	String_IndexOfOrdinalIgnoreCaseUnchecked_m6064,
	String_IndexOf_m2259,
	String_IndexOf_m4763,
	String_IndexOf_m5730,
	String_IndexOfUnchecked_m6065,
	String_IndexOf_m3644,
	String_IndexOf_m3646,
	String_IndexOf_m6066,
	String_LastIndexOfAny_m6067,
	String_LastIndexOfAny_m2230,
	String_LastIndexOfAnyUnchecked_m6068,
	String_LastIndexOf_m5677,
	String_LastIndexOf_m6069,
	String_LastIndexOf_m5731,
	String_LastIndexOfUnchecked_m6070,
	String_LastIndexOf_m3649,
	String_LastIndexOf_m6071,
	String_Contains_m2253,
	String_IsNullOrEmpty_m2238,
	String_PadRight_m6072,
	String_StartsWith_m3638,
	String_Replace_m3648,
	String_Replace_m3647,
	String_ReplaceUnchecked_m6073,
	String_ReplaceFallback_m6074,
	String_Remove_m2232,
	String_ToLower_m3615,
	String_ToLower_m5728,
	String_ToLowerInvariant_m6075,
	String_ToString_m3637,
	String_ToString_m6076,
	String_Format_m1995,
	String_Format_m6077,
	String_Format_m6078,
	String_Format_m2349,
	String_Format_m4715,
	String_FormatHelper_m6079,
	String_Concat_m1870,
	String_Concat_m1861,
	String_Concat_m2080,
	String_Concat_m262,
	String_Concat_m3641,
	String_Concat_m1901,
	String_Concat_m5675,
	String_ConcatInternal_m6080,
	String_Insert_m2234,
	String_Join_m6081,
	String_Join_m6082,
	String_JoinUnchecked_m6083,
	String_get_Length_m2190,
	String_ParseFormatSpecifier_m6084,
	String_ParseDecimal_m6085,
	String_InternalSetChar_m6086,
	String_InternalSetLength_m6087,
	String_GetHashCode_m3609,
	String_GetCaseInsensitiveHashCode_m6088,
	String_CreateString_m6089,
	String_CreateString_m6090,
	String_CreateString_m6091,
	String_CreateString_m6092,
	String_CreateString_m6093,
	String_CreateString_m6094,
	String_CreateString_m5721,
	String_CreateString_m2237,
	String_memcpy4_m6095,
	String_memcpy2_m6096,
	String_memcpy1_m6097,
	String_memcpy_m6098,
	String_CharCopy_m6099,
	String_CharCopyReverse_m6100,
	String_CharCopy_m6101,
	String_CharCopy_m6102,
	String_CharCopyReverse_m6103,
	String_InternalSplit_m6104,
	String_InternalAllocateStr_m6105,
	String_op_Equality_m215,
	String_op_Inequality_m2208,
	Single_System_IConvertible_ToBoolean_m6106,
	Single_System_IConvertible_ToByte_m6107,
	Single_System_IConvertible_ToChar_m6108,
	Single_System_IConvertible_ToDateTime_m6109,
	Single_System_IConvertible_ToDecimal_m6110,
	Single_System_IConvertible_ToDouble_m6111,
	Single_System_IConvertible_ToInt16_m6112,
	Single_System_IConvertible_ToInt32_m6113,
	Single_System_IConvertible_ToInt64_m6114,
	Single_System_IConvertible_ToSByte_m6115,
	Single_System_IConvertible_ToSingle_m6116,
	Single_System_IConvertible_ToType_m6117,
	Single_System_IConvertible_ToUInt16_m6118,
	Single_System_IConvertible_ToUInt32_m6119,
	Single_System_IConvertible_ToUInt64_m6120,
	Single_CompareTo_m6121,
	Single_Equals_m6122,
	Single_CompareTo_m1867,
	Single_Equals_m2083,
	Single_GetHashCode_m3593,
	Single_IsInfinity_m6123,
	Single_IsNaN_m6124,
	Single_IsNegativeInfinity_m6125,
	Single_IsPositiveInfinity_m6126,
	Single_Parse_m6127,
	Single_ToString_m6128,
	Single_ToString_m6129,
	Single_ToString_m3600,
	Single_ToString_m6130,
	Single_GetTypeCode_m6131,
	Double_System_IConvertible_ToType_m6132,
	Double_System_IConvertible_ToBoolean_m6133,
	Double_System_IConvertible_ToByte_m6134,
	Double_System_IConvertible_ToChar_m6135,
	Double_System_IConvertible_ToDateTime_m6136,
	Double_System_IConvertible_ToDecimal_m6137,
	Double_System_IConvertible_ToDouble_m6138,
	Double_System_IConvertible_ToInt16_m6139,
	Double_System_IConvertible_ToInt32_m6140,
	Double_System_IConvertible_ToInt64_m6141,
	Double_System_IConvertible_ToSByte_m6142,
	Double_System_IConvertible_ToSingle_m6143,
	Double_System_IConvertible_ToUInt16_m6144,
	Double_System_IConvertible_ToUInt32_m6145,
	Double_System_IConvertible_ToUInt64_m6146,
	Double_CompareTo_m6147,
	Double_Equals_m6148,
	Double_CompareTo_m6149,
	Double_Equals_m6150,
	Double_GetHashCode_m6151,
	Double_IsInfinity_m6152,
	Double_IsNaN_m6153,
	Double_IsNegativeInfinity_m6154,
	Double_IsPositiveInfinity_m6155,
	Double_Parse_m6156,
	Double_Parse_m6157,
	Double_Parse_m6158,
	Double_Parse_m6159,
	Double_TryParseStringConstant_m6160,
	Double_ParseImpl_m6161,
	Double_ToString_m6162,
	Double_ToString_m6163,
	Double_ToString_m6164,
	Decimal__ctor_m6165,
	Decimal__ctor_m6166,
	Decimal__ctor_m6167,
	Decimal__ctor_m6168,
	Decimal__ctor_m6169,
	Decimal__ctor_m6170,
	Decimal__ctor_m6171,
	Decimal__cctor_m6172,
	Decimal_System_IConvertible_ToType_m6173,
	Decimal_System_IConvertible_ToBoolean_m6174,
	Decimal_System_IConvertible_ToByte_m6175,
	Decimal_System_IConvertible_ToChar_m6176,
	Decimal_System_IConvertible_ToDateTime_m6177,
	Decimal_System_IConvertible_ToDecimal_m6178,
	Decimal_System_IConvertible_ToDouble_m6179,
	Decimal_System_IConvertible_ToInt16_m6180,
	Decimal_System_IConvertible_ToInt32_m6181,
	Decimal_System_IConvertible_ToInt64_m6182,
	Decimal_System_IConvertible_ToSByte_m6183,
	Decimal_System_IConvertible_ToSingle_m6184,
	Decimal_System_IConvertible_ToUInt16_m6185,
	Decimal_System_IConvertible_ToUInt32_m6186,
	Decimal_System_IConvertible_ToUInt64_m6187,
	Decimal_GetBits_m6188,
	Decimal_Add_m6189,
	Decimal_Subtract_m6190,
	Decimal_GetHashCode_m6191,
	Decimal_u64_m6192,
	Decimal_s64_m6193,
	Decimal_Equals_m6194,
	Decimal_Equals_m6195,
	Decimal_IsZero_m6196,
	Decimal_Floor_m6197,
	Decimal_Multiply_m6198,
	Decimal_Divide_m6199,
	Decimal_Compare_m6200,
	Decimal_CompareTo_m6201,
	Decimal_CompareTo_m6202,
	Decimal_Equals_m6203,
	Decimal_Parse_m6204,
	Decimal_ThrowAtPos_m6205,
	Decimal_ThrowInvalidExp_m6206,
	Decimal_stripStyles_m6207,
	Decimal_Parse_m6208,
	Decimal_PerformParse_m6209,
	Decimal_ToString_m6210,
	Decimal_ToString_m6211,
	Decimal_ToString_m6212,
	Decimal_decimal2UInt64_m6213,
	Decimal_decimal2Int64_m6214,
	Decimal_decimalIncr_m6215,
	Decimal_string2decimal_m6216,
	Decimal_decimalSetExponent_m6217,
	Decimal_decimal2double_m6218,
	Decimal_decimalFloorAndTrunc_m6219,
	Decimal_decimalMult_m6220,
	Decimal_decimalDiv_m6221,
	Decimal_decimalCompare_m6222,
	Decimal_op_Increment_m6223,
	Decimal_op_Subtraction_m6224,
	Decimal_op_Multiply_m6225,
	Decimal_op_Division_m6226,
	Decimal_op_Explicit_m6227,
	Decimal_op_Explicit_m6228,
	Decimal_op_Explicit_m6229,
	Decimal_op_Explicit_m6230,
	Decimal_op_Explicit_m6231,
	Decimal_op_Explicit_m6232,
	Decimal_op_Explicit_m6233,
	Decimal_op_Explicit_m6234,
	Decimal_op_Implicit_m6235,
	Decimal_op_Implicit_m6236,
	Decimal_op_Implicit_m6237,
	Decimal_op_Implicit_m6238,
	Decimal_op_Implicit_m6239,
	Decimal_op_Implicit_m6240,
	Decimal_op_Implicit_m6241,
	Decimal_op_Implicit_m6242,
	Decimal_op_Explicit_m6243,
	Decimal_op_Explicit_m6244,
	Decimal_op_Explicit_m6245,
	Decimal_op_Explicit_m6246,
	Decimal_op_Inequality_m6247,
	Decimal_op_Equality_m6248,
	Decimal_op_GreaterThan_m6249,
	Decimal_op_LessThan_m6250,
	Boolean__cctor_m6251,
	Boolean_System_IConvertible_ToType_m6252,
	Boolean_System_IConvertible_ToBoolean_m6253,
	Boolean_System_IConvertible_ToByte_m6254,
	Boolean_System_IConvertible_ToChar_m6255,
	Boolean_System_IConvertible_ToDateTime_m6256,
	Boolean_System_IConvertible_ToDecimal_m6257,
	Boolean_System_IConvertible_ToDouble_m6258,
	Boolean_System_IConvertible_ToInt16_m6259,
	Boolean_System_IConvertible_ToInt32_m6260,
	Boolean_System_IConvertible_ToInt64_m6261,
	Boolean_System_IConvertible_ToSByte_m6262,
	Boolean_System_IConvertible_ToSingle_m6263,
	Boolean_System_IConvertible_ToUInt16_m6264,
	Boolean_System_IConvertible_ToUInt32_m6265,
	Boolean_System_IConvertible_ToUInt64_m6266,
	Boolean_CompareTo_m6267,
	Boolean_Equals_m6268,
	Boolean_CompareTo_m6269,
	Boolean_Equals_m6270,
	Boolean_GetHashCode_m6271,
	Boolean_Parse_m6272,
	Boolean_ToString_m6273,
	Boolean_GetTypeCode_m6274,
	Boolean_ToString_m6275,
	IntPtr__ctor_m3603,
	IntPtr__ctor_m6276,
	IntPtr__ctor_m6277,
	IntPtr__ctor_m6278,
	IntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6279,
	IntPtr_get_Size_m6280,
	IntPtr_Equals_m6281,
	IntPtr_GetHashCode_m6282,
	IntPtr_ToInt64_m6283,
	IntPtr_ToPointer_m3596,
	IntPtr_ToString_m6284,
	IntPtr_ToString_m6285,
	IntPtr_op_Equality_m3655,
	IntPtr_op_Inequality_m3595,
	IntPtr_op_Explicit_m6286,
	IntPtr_op_Explicit_m6287,
	IntPtr_op_Explicit_m6288,
	IntPtr_op_Explicit_m3654,
	IntPtr_op_Explicit_m6289,
	UIntPtr__ctor_m6290,
	UIntPtr__ctor_m6291,
	UIntPtr__ctor_m6292,
	UIntPtr__cctor_m6293,
	UIntPtr_System_Runtime_Serialization_ISerializable_GetObjectData_m6294,
	UIntPtr_Equals_m6295,
	UIntPtr_GetHashCode_m6296,
	UIntPtr_ToUInt32_m6297,
	UIntPtr_ToUInt64_m6298,
	UIntPtr_ToPointer_m6299,
	UIntPtr_ToString_m6300,
	UIntPtr_get_Size_m6301,
	UIntPtr_op_Equality_m6302,
	UIntPtr_op_Inequality_m6303,
	UIntPtr_op_Explicit_m6304,
	UIntPtr_op_Explicit_m6305,
	UIntPtr_op_Explicit_m6306,
	UIntPtr_op_Explicit_m6307,
	UIntPtr_op_Explicit_m6308,
	UIntPtr_op_Explicit_m6309,
	MulticastDelegate_GetObjectData_m6310,
	MulticastDelegate_Equals_m6311,
	MulticastDelegate_GetHashCode_m6312,
	MulticastDelegate_GetInvocationList_m6313,
	MulticastDelegate_CombineImpl_m6314,
	MulticastDelegate_BaseEquals_m6315,
	MulticastDelegate_KPM_m6316,
	MulticastDelegate_RemoveImpl_m6317,
	Delegate_get_Method_m3676,
	Delegate_get_Target_m3657,
	Delegate_CreateDelegate_internal_m6318,
	Delegate_SetMulticastInvoke_m6319,
	Delegate_arg_type_match_m6320,
	Delegate_return_type_match_m6321,
	Delegate_CreateDelegate_m6322,
	Delegate_CreateDelegate_m3675,
	Delegate_CreateDelegate_m6323,
	Delegate_CreateDelegate_m6324,
	Delegate_GetCandidateMethod_m6325,
	Delegate_CreateDelegate_m6326,
	Delegate_CreateDelegate_m6327,
	Delegate_CreateDelegate_m6328,
	Delegate_CreateDelegate_m6329,
	Delegate_Clone_m6330,
	Delegate_Equals_m6331,
	Delegate_GetHashCode_m6332,
	Delegate_GetObjectData_m6333,
	Delegate_GetInvocationList_m6334,
	Delegate_Combine_m2128,
	Delegate_Combine_m6335,
	Delegate_CombineImpl_m6336,
	Delegate_Remove_m2129,
	Delegate_RemoveImpl_m6337,
	Enum__ctor_m6338,
	Enum__cctor_m6339,
	Enum_System_IConvertible_ToBoolean_m6340,
	Enum_System_IConvertible_ToByte_m6341,
	Enum_System_IConvertible_ToChar_m6342,
	Enum_System_IConvertible_ToDateTime_m6343,
	Enum_System_IConvertible_ToDecimal_m6344,
	Enum_System_IConvertible_ToDouble_m6345,
	Enum_System_IConvertible_ToInt16_m6346,
	Enum_System_IConvertible_ToInt32_m6347,
	Enum_System_IConvertible_ToInt64_m6348,
	Enum_System_IConvertible_ToSByte_m6349,
	Enum_System_IConvertible_ToSingle_m6350,
	Enum_System_IConvertible_ToType_m6351,
	Enum_System_IConvertible_ToUInt16_m6352,
	Enum_System_IConvertible_ToUInt32_m6353,
	Enum_System_IConvertible_ToUInt64_m6354,
	Enum_GetTypeCode_m6355,
	Enum_get_value_m6356,
	Enum_get_Value_m6357,
	Enum_FindPosition_m6358,
	Enum_GetName_m6359,
	Enum_IsDefined_m4740,
	Enum_get_underlying_type_m6360,
	Enum_GetUnderlyingType_m6361,
	Enum_FindName_m6362,
	Enum_GetValue_m6363,
	Enum_Parse_m3616,
	Enum_compare_value_to_m6364,
	Enum_CompareTo_m6365,
	Enum_ToString_m6366,
	Enum_ToString_m6367,
	Enum_ToString_m6368,
	Enum_ToString_m6369,
	Enum_ToObject_m6370,
	Enum_ToObject_m6371,
	Enum_ToObject_m6372,
	Enum_ToObject_m6373,
	Enum_ToObject_m6374,
	Enum_ToObject_m6375,
	Enum_ToObject_m6376,
	Enum_ToObject_m6377,
	Enum_ToObject_m6378,
	Enum_Equals_m6379,
	Enum_get_hashcode_m6380,
	Enum_GetHashCode_m6381,
	Enum_FormatSpecifier_X_m6382,
	Enum_FormatFlags_m6383,
	Enum_Format_m6384,
	SimpleEnumerator__ctor_m6385,
	SimpleEnumerator_get_Current_m6386,
	SimpleEnumerator_MoveNext_m6387,
	SimpleEnumerator_Reset_m6388,
	SimpleEnumerator_Clone_m6389,
	Swapper__ctor_m6390,
	Swapper_Invoke_m6391,
	Swapper_BeginInvoke_m6392,
	Swapper_EndInvoke_m6393,
	Array__ctor_m6394,
	Array_System_Collections_IList_get_Item_m6395,
	Array_System_Collections_IList_set_Item_m6396,
	Array_System_Collections_IList_Add_m6397,
	Array_System_Collections_IList_Clear_m6398,
	Array_System_Collections_IList_Contains_m6399,
	Array_System_Collections_IList_IndexOf_m6400,
	Array_System_Collections_IList_Insert_m6401,
	Array_System_Collections_IList_Remove_m6402,
	Array_System_Collections_IList_RemoveAt_m6403,
	Array_System_Collections_ICollection_get_Count_m6404,
	Array_InternalArray__ICollection_get_Count_m6405,
	Array_InternalArray__ICollection_get_IsReadOnly_m6406,
	Array_InternalArray__ICollection_Clear_m6407,
	Array_InternalArray__RemoveAt_m6408,
	Array_get_Length_m5654,
	Array_get_LongLength_m6409,
	Array_get_Rank_m5657,
	Array_GetRank_m6410,
	Array_GetLength_m6411,
	Array_GetLongLength_m6412,
	Array_GetLowerBound_m6413,
	Array_GetValue_m6414,
	Array_SetValue_m6415,
	Array_GetValueImpl_m6416,
	Array_SetValueImpl_m6417,
	Array_FastCopy_m6418,
	Array_CreateInstanceImpl_m6419,
	Array_get_IsSynchronized_m6420,
	Array_get_SyncRoot_m6421,
	Array_get_IsFixedSize_m6422,
	Array_get_IsReadOnly_m6423,
	Array_GetEnumerator_m6424,
	Array_GetUpperBound_m6425,
	Array_GetValue_m6426,
	Array_GetValue_m6427,
	Array_GetValue_m6428,
	Array_GetValue_m6429,
	Array_GetValue_m6430,
	Array_GetValue_m6431,
	Array_SetValue_m6432,
	Array_SetValue_m6433,
	Array_SetValue_m6434,
	Array_SetValue_m5655,
	Array_SetValue_m6435,
	Array_SetValue_m6436,
	Array_CreateInstance_m6437,
	Array_CreateInstance_m6438,
	Array_CreateInstance_m6439,
	Array_CreateInstance_m6440,
	Array_CreateInstance_m6441,
	Array_GetIntArray_m6442,
	Array_CreateInstance_m6443,
	Array_GetValue_m6444,
	Array_SetValue_m6445,
	Array_BinarySearch_m6446,
	Array_BinarySearch_m6447,
	Array_BinarySearch_m6448,
	Array_BinarySearch_m6449,
	Array_DoBinarySearch_m6450,
	Array_Clear_m3729,
	Array_ClearInternal_m6451,
	Array_Clone_m6452,
	Array_Copy_m5722,
	Array_Copy_m6453,
	Array_Copy_m6454,
	Array_Copy_m6455,
	Array_IndexOf_m6456,
	Array_IndexOf_m6457,
	Array_IndexOf_m6458,
	Array_Initialize_m6459,
	Array_LastIndexOf_m6460,
	Array_LastIndexOf_m6461,
	Array_LastIndexOf_m6462,
	Array_get_swapper_m6463,
	Array_Reverse_m4641,
	Array_Reverse_m4677,
	Array_Sort_m6464,
	Array_Sort_m6465,
	Array_Sort_m6466,
	Array_Sort_m6467,
	Array_Sort_m6468,
	Array_Sort_m6469,
	Array_Sort_m6470,
	Array_Sort_m6471,
	Array_int_swapper_m6472,
	Array_obj_swapper_m6473,
	Array_slow_swapper_m6474,
	Array_double_swapper_m6475,
	Array_new_gap_m6476,
	Array_combsort_m6477,
	Array_combsort_m6478,
	Array_combsort_m6479,
	Array_qsort_m6480,
	Array_swap_m6481,
	Array_compare_m6482,
	Array_CopyTo_m6483,
	Array_CopyTo_m6484,
	Array_ConstrainedCopy_m6485,
	Type__ctor_m6486,
	Type__cctor_m6487,
	Type_FilterName_impl_m6488,
	Type_FilterNameIgnoreCase_impl_m6489,
	Type_FilterAttribute_impl_m6490,
	Type_get_Attributes_m6491,
	Type_get_DeclaringType_m6492,
	Type_get_HasElementType_m6493,
	Type_get_IsAbstract_m6494,
	Type_get_IsArray_m6495,
	Type_get_IsByRef_m6496,
	Type_get_IsClass_m6497,
	Type_get_IsContextful_m6498,
	Type_get_IsEnum_m6499,
	Type_get_IsExplicitLayout_m6500,
	Type_get_IsInterface_m6501,
	Type_get_IsMarshalByRef_m6502,
	Type_get_IsPointer_m6503,
	Type_get_IsPrimitive_m6504,
	Type_get_IsSealed_m6505,
	Type_get_IsSerializable_m6506,
	Type_get_IsValueType_m6507,
	Type_get_MemberType_m6508,
	Type_get_ReflectedType_m6509,
	Type_get_TypeHandle_m6510,
	Type_Equals_m6511,
	Type_Equals_m6512,
	Type_EqualsInternal_m6513,
	Type_internal_from_handle_m6514,
	Type_internal_from_name_m6515,
	Type_GetType_m6516,
	Type_GetType_m3662,
	Type_GetTypeCodeInternal_m6517,
	Type_GetTypeCode_m6518,
	Type_GetTypeFromHandle_m213,
	Type_GetTypeHandle_m6519,
	Type_type_is_subtype_of_m6520,
	Type_type_is_assignable_from_m6521,
	Type_IsSubclassOf_m6522,
	Type_IsAssignableFrom_m6523,
	Type_IsInstanceOfType_m6524,
	Type_GetHashCode_m6525,
	Type_GetMethod_m6526,
	Type_GetMethod_m6527,
	Type_GetMethod_m6528,
	Type_GetMethod_m6529,
	Type_GetProperty_m6530,
	Type_GetProperty_m6531,
	Type_GetProperty_m6532,
	Type_GetProperty_m6533,
	Type_IsArrayImpl_m6534,
	Type_IsValueTypeImpl_m6535,
	Type_IsContextfulImpl_m6536,
	Type_IsMarshalByRefImpl_m6537,
	Type_GetConstructor_m6538,
	Type_GetConstructor_m6539,
	Type_GetConstructor_m6540,
	Type_ToString_m6541,
	Type_get_IsSystemType_m6542,
	Type_GetGenericArguments_m6543,
	Type_get_ContainsGenericParameters_m6544,
	Type_get_IsGenericTypeDefinition_m6545,
	Type_GetGenericTypeDefinition_impl_m6546,
	Type_GetGenericTypeDefinition_m6547,
	Type_get_IsGenericType_m6548,
	Type_MakeGenericType_m6549,
	Type_MakeGenericType_m6550,
	Type_get_IsGenericParameter_m6551,
	Type_get_IsNested_m6552,
	Type_GetPseudoCustomAttributes_m6553,
	MemberInfo__ctor_m6554,
	MemberInfo_get_Module_m6555,
	Exception__ctor_m4631,
	Exception__ctor_m244,
	Exception__ctor_m3653,
	Exception__ctor_m3652,
	Exception_get_InnerException_m6556,
	Exception_set_HResult_m3651,
	Exception_get_ClassName_m6557,
	Exception_get_Message_m6558,
	Exception_get_Source_m6559,
	Exception_get_StackTrace_m6560,
	Exception_GetObjectData_m5735,
	Exception_ToString_m6561,
	Exception_GetFullNameForStackTrace_m6562,
	Exception_GetType_m6563,
	RuntimeFieldHandle__ctor_m6564,
	RuntimeFieldHandle_get_Value_m6565,
	RuntimeFieldHandle_GetObjectData_m6566,
	RuntimeFieldHandle_Equals_m6567,
	RuntimeFieldHandle_GetHashCode_m6568,
	RuntimeTypeHandle__ctor_m6569,
	RuntimeTypeHandle_get_Value_m6570,
	RuntimeTypeHandle_GetObjectData_m6571,
	RuntimeTypeHandle_Equals_m6572,
	RuntimeTypeHandle_GetHashCode_m6573,
	ParamArrayAttribute__ctor_m6574,
	OutAttribute__ctor_m6575,
	ObsoleteAttribute__ctor_m6576,
	ObsoleteAttribute__ctor_m6577,
	ObsoleteAttribute__ctor_m6578,
	DllImportAttribute__ctor_m6579,
	DllImportAttribute_get_Value_m6580,
	MarshalAsAttribute__ctor_m6581,
	InAttribute__ctor_m6582,
	GuidAttribute__ctor_m6583,
	ComImportAttribute__ctor_m6584,
	OptionalAttribute__ctor_m6585,
	CompilerGeneratedAttribute__ctor_m6586,
	InternalsVisibleToAttribute__ctor_m6587,
	RuntimeCompatibilityAttribute__ctor_m6588,
	RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m6589,
	DebuggerHiddenAttribute__ctor_m6590,
	DefaultMemberAttribute__ctor_m6591,
	DefaultMemberAttribute_get_MemberName_m6592,
	DecimalConstantAttribute__ctor_m6593,
	FieldOffsetAttribute__ctor_m6594,
	AsyncCallback__ctor_m4739,
	AsyncCallback_Invoke_m6595,
	AsyncCallback_BeginInvoke_m4737,
	AsyncCallback_EndInvoke_m6596,
	TypedReference_Equals_m6597,
	TypedReference_GetHashCode_m6598,
	ArgIterator_Equals_m6599,
	ArgIterator_GetHashCode_m6600,
	MarshalByRefObject__ctor_m5686,
	MarshalByRefObject_get_ObjectIdentity_m6601,
	RuntimeHelpers_InitializeArray_m6602,
	RuntimeHelpers_InitializeArray_m3744,
	RuntimeHelpers_get_OffsetToStringData_m6603,
	Locale_GetText_m6604,
	Locale_GetText_m6605,
	MonoTODOAttribute__ctor_m6606,
	MonoTODOAttribute__ctor_m6607,
	MonoDocumentationNoteAttribute__ctor_m6608,
	SafeHandleZeroOrMinusOneIsInvalid__ctor_m6609,
	SafeHandleZeroOrMinusOneIsInvalid_get_IsInvalid_m6610,
	SafeWaitHandle__ctor_m6611,
	SafeWaitHandle_ReleaseHandle_m6612,
	TableRange__ctor_m6613,
	CodePointIndexer__ctor_m6614,
	CodePointIndexer_ToIndex_m6615,
	TailoringInfo__ctor_m6616,
	Contraction__ctor_m6617,
	ContractionComparer__ctor_m6618,
	ContractionComparer__cctor_m6619,
	ContractionComparer_Compare_m6620,
	Level2Map__ctor_m6621,
	Level2MapComparer__ctor_m6622,
	Level2MapComparer__cctor_m6623,
	Level2MapComparer_Compare_m6624,
	MSCompatUnicodeTable__cctor_m6625,
	MSCompatUnicodeTable_GetTailoringInfo_m6626,
	MSCompatUnicodeTable_BuildTailoringTables_m6627,
	MSCompatUnicodeTable_SetCJKReferences_m6628,
	MSCompatUnicodeTable_Category_m6629,
	MSCompatUnicodeTable_Level1_m6630,
	MSCompatUnicodeTable_Level2_m6631,
	MSCompatUnicodeTable_Level3_m6632,
	MSCompatUnicodeTable_IsIgnorable_m6633,
	MSCompatUnicodeTable_IsIgnorableNonSpacing_m6634,
	MSCompatUnicodeTable_ToKanaTypeInsensitive_m6635,
	MSCompatUnicodeTable_ToWidthCompat_m6636,
	MSCompatUnicodeTable_HasSpecialWeight_m6637,
	MSCompatUnicodeTable_IsHalfWidthKana_m6638,
	MSCompatUnicodeTable_IsHiragana_m6639,
	MSCompatUnicodeTable_IsJapaneseSmallLetter_m6640,
	MSCompatUnicodeTable_get_IsReady_m6641,
	MSCompatUnicodeTable_GetResource_m6642,
	MSCompatUnicodeTable_UInt32FromBytePtr_m6643,
	MSCompatUnicodeTable_FillCJK_m6644,
	MSCompatUnicodeTable_FillCJKCore_m6645,
	MSCompatUnicodeTableUtil__cctor_m6646,
	Context__ctor_m6647,
	PreviousInfo__ctor_m6648,
	SimpleCollator__ctor_m6649,
	SimpleCollator__cctor_m6650,
	SimpleCollator_SetCJKTable_m6651,
	SimpleCollator_GetNeutralCulture_m6652,
	SimpleCollator_Category_m6653,
	SimpleCollator_Level1_m6654,
	SimpleCollator_Level2_m6655,
	SimpleCollator_IsHalfKana_m6656,
	SimpleCollator_GetContraction_m6657,
	SimpleCollator_GetContraction_m6658,
	SimpleCollator_GetTailContraction_m6659,
	SimpleCollator_GetTailContraction_m6660,
	SimpleCollator_FilterOptions_m6661,
	SimpleCollator_GetExtenderType_m6662,
	SimpleCollator_ToDashTypeValue_m6663,
	SimpleCollator_FilterExtender_m6664,
	SimpleCollator_IsIgnorable_m6665,
	SimpleCollator_IsSafe_m6666,
	SimpleCollator_GetSortKey_m6667,
	SimpleCollator_GetSortKey_m6668,
	SimpleCollator_GetSortKey_m6669,
	SimpleCollator_FillSortKeyRaw_m6670,
	SimpleCollator_FillSurrogateSortKeyRaw_m6671,
	SimpleCollator_CompareOrdinal_m6672,
	SimpleCollator_CompareQuick_m6673,
	SimpleCollator_CompareOrdinalIgnoreCase_m6674,
	SimpleCollator_Compare_m6675,
	SimpleCollator_ClearBuffer_m6676,
	SimpleCollator_QuickCheckPossible_m6677,
	SimpleCollator_CompareInternal_m6678,
	SimpleCollator_CompareFlagPair_m6679,
	SimpleCollator_IsPrefix_m6680,
	SimpleCollator_IsPrefix_m6681,
	SimpleCollator_IsPrefix_m6682,
	SimpleCollator_IsSuffix_m6683,
	SimpleCollator_IsSuffix_m6684,
	SimpleCollator_QuickIndexOf_m6685,
	SimpleCollator_IndexOf_m6686,
	SimpleCollator_IndexOfOrdinal_m6687,
	SimpleCollator_IndexOfOrdinalIgnoreCase_m6688,
	SimpleCollator_IndexOfSortKey_m6689,
	SimpleCollator_IndexOf_m6690,
	SimpleCollator_LastIndexOf_m6691,
	SimpleCollator_LastIndexOfOrdinal_m6692,
	SimpleCollator_LastIndexOfOrdinalIgnoreCase_m6693,
	SimpleCollator_LastIndexOfSortKey_m6694,
	SimpleCollator_LastIndexOf_m6695,
	SimpleCollator_MatchesForward_m6696,
	SimpleCollator_MatchesForwardCore_m6697,
	SimpleCollator_MatchesPrimitive_m6698,
	SimpleCollator_MatchesBackward_m6699,
	SimpleCollator_MatchesBackwardCore_m6700,
	SortKey__ctor_m6701,
	SortKey__ctor_m6702,
	SortKey_Compare_m6703,
	SortKey_get_OriginalString_m6704,
	SortKey_get_KeyData_m6705,
	SortKey_Equals_m6706,
	SortKey_GetHashCode_m6707,
	SortKey_ToString_m6708,
	SortKeyBuffer__ctor_m6709,
	SortKeyBuffer_Reset_m6710,
	SortKeyBuffer_Initialize_m6711,
	SortKeyBuffer_AppendCJKExtension_m6712,
	SortKeyBuffer_AppendKana_m6713,
	SortKeyBuffer_AppendNormal_m6714,
	SortKeyBuffer_AppendLevel5_m6715,
	SortKeyBuffer_AppendBufferPrimitive_m6716,
	SortKeyBuffer_GetResultAndReset_m6717,
	SortKeyBuffer_GetOptimizedLength_m6718,
	SortKeyBuffer_GetResult_m6719,
	PrimeGeneratorBase__ctor_m6720,
	PrimeGeneratorBase_get_Confidence_m6721,
	PrimeGeneratorBase_get_PrimalityTest_m6722,
	PrimeGeneratorBase_get_TrialDivisionBounds_m6723,
	SequentialSearchPrimeGeneratorBase__ctor_m6724,
	SequentialSearchPrimeGeneratorBase_GenerateSearchBase_m6725,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6726,
	SequentialSearchPrimeGeneratorBase_GenerateNewPrime_m6727,
	SequentialSearchPrimeGeneratorBase_IsPrimeAcceptable_m6728,
	PrimalityTests_GetSPPRounds_m6729,
	PrimalityTests_Test_m6730,
	PrimalityTests_RabinMillerTest_m6731,
	PrimalityTests_SmallPrimeSppTest_m6732,
	ModulusRing__ctor_m6733,
	ModulusRing_BarrettReduction_m6734,
	ModulusRing_Multiply_m6735,
	ModulusRing_Difference_m6736,
	ModulusRing_Pow_m6737,
	ModulusRing_Pow_m6738,
	Kernel_AddSameSign_m6739,
	Kernel_Subtract_m6740,
	Kernel_MinusEq_m6741,
	Kernel_PlusEq_m6742,
	Kernel_Compare_m6743,
	Kernel_SingleByteDivideInPlace_m6744,
	Kernel_DwordMod_m6745,
	Kernel_DwordDivMod_m6746,
	Kernel_multiByteDivide_m6747,
	Kernel_LeftShift_m6748,
	Kernel_RightShift_m6749,
	Kernel_MultiplyByDword_m6750,
	Kernel_Multiply_m6751,
	Kernel_MultiplyMod2p32pmod_m6752,
	Kernel_modInverse_m6753,
	Kernel_modInverse_m6754,
	BigInteger__ctor_m6755,
	BigInteger__ctor_m6756,
	BigInteger__ctor_m6757,
	BigInteger__ctor_m6758,
	BigInteger__ctor_m6759,
	BigInteger__cctor_m6760,
	BigInteger_get_Rng_m6761,
	BigInteger_GenerateRandom_m6762,
	BigInteger_GenerateRandom_m6763,
	BigInteger_Randomize_m6764,
	BigInteger_Randomize_m6765,
	BigInteger_BitCount_m6766,
	BigInteger_TestBit_m6767,
	BigInteger_TestBit_m6768,
	BigInteger_SetBit_m6769,
	BigInteger_SetBit_m6770,
	BigInteger_LowestSetBit_m6771,
	BigInteger_GetBytes_m6772,
	BigInteger_ToString_m6773,
	BigInteger_ToString_m6774,
	BigInteger_Normalize_m6775,
	BigInteger_Clear_m6776,
	BigInteger_GetHashCode_m6777,
	BigInteger_ToString_m6778,
	BigInteger_Equals_m6779,
	BigInteger_ModInverse_m6780,
	BigInteger_ModPow_m6781,
	BigInteger_IsProbablePrime_m6782,
	BigInteger_GeneratePseudoPrime_m6783,
	BigInteger_Incr2_m6784,
	BigInteger_op_Implicit_m6785,
	BigInteger_op_Implicit_m6786,
	BigInteger_op_Addition_m6787,
	BigInteger_op_Subtraction_m6788,
	BigInteger_op_Modulus_m6789,
	BigInteger_op_Modulus_m6790,
	BigInteger_op_Division_m6791,
	BigInteger_op_Multiply_m6792,
	BigInteger_op_Multiply_m6793,
	BigInteger_op_LeftShift_m6794,
	BigInteger_op_RightShift_m6795,
	BigInteger_op_Equality_m6796,
	BigInteger_op_Inequality_m6797,
	BigInteger_op_Equality_m6798,
	BigInteger_op_Inequality_m6799,
	BigInteger_op_GreaterThan_m6800,
	BigInteger_op_LessThan_m6801,
	BigInteger_op_GreaterThanOrEqual_m6802,
	BigInteger_op_LessThanOrEqual_m6803,
	CryptoConvert_ToInt32LE_m6804,
	CryptoConvert_ToUInt32LE_m6805,
	CryptoConvert_GetBytesLE_m6806,
	CryptoConvert_ToCapiPrivateKeyBlob_m6807,
	CryptoConvert_FromCapiPublicKeyBlob_m6808,
	CryptoConvert_FromCapiPublicKeyBlob_m6809,
	CryptoConvert_ToCapiPublicKeyBlob_m6810,
	CryptoConvert_ToCapiKeyBlob_m6811,
	KeyBuilder_get_Rng_m6812,
	KeyBuilder_Key_m6813,
	KeyBuilder_IV_m6814,
	BlockProcessor__ctor_m6815,
	BlockProcessor_Finalize_m6816,
	BlockProcessor_Initialize_m6817,
	BlockProcessor_Core_m6818,
	BlockProcessor_Core_m6819,
	BlockProcessor_Final_m6820,
	KeyGeneratedEventHandler__ctor_m6821,
	KeyGeneratedEventHandler_Invoke_m6822,
	KeyGeneratedEventHandler_BeginInvoke_m6823,
	KeyGeneratedEventHandler_EndInvoke_m6824,
	DSAManaged__ctor_m6825,
	DSAManaged_add_KeyGenerated_m6826,
	DSAManaged_remove_KeyGenerated_m6827,
	DSAManaged_Finalize_m6828,
	DSAManaged_Generate_m6829,
	DSAManaged_GenerateKeyPair_m6830,
	DSAManaged_add_m6831,
	DSAManaged_GenerateParams_m6832,
	DSAManaged_get_Random_m6833,
	DSAManaged_get_KeySize_m6834,
	DSAManaged_get_PublicOnly_m6835,
	DSAManaged_NormalizeArray_m6836,
	DSAManaged_ExportParameters_m6837,
	DSAManaged_ImportParameters_m6838,
	DSAManaged_CreateSignature_m6839,
	DSAManaged_VerifySignature_m6840,
	DSAManaged_Dispose_m6841,
	KeyPairPersistence__ctor_m6842,
	KeyPairPersistence__ctor_m6843,
	KeyPairPersistence__cctor_m6844,
	KeyPairPersistence_get_Filename_m6845,
	KeyPairPersistence_get_KeyValue_m6846,
	KeyPairPersistence_set_KeyValue_m6847,
	KeyPairPersistence_Load_m6848,
	KeyPairPersistence_Save_m6849,
	KeyPairPersistence_Remove_m6850,
	KeyPairPersistence_get_UserPath_m6851,
	KeyPairPersistence_get_MachinePath_m6852,
	KeyPairPersistence__CanSecure_m6853,
	KeyPairPersistence__ProtectUser_m6854,
	KeyPairPersistence__ProtectMachine_m6855,
	KeyPairPersistence__IsUserProtected_m6856,
	KeyPairPersistence__IsMachineProtected_m6857,
	KeyPairPersistence_CanSecure_m6858,
	KeyPairPersistence_ProtectUser_m6859,
	KeyPairPersistence_ProtectMachine_m6860,
	KeyPairPersistence_IsUserProtected_m6861,
	KeyPairPersistence_IsMachineProtected_m6862,
	KeyPairPersistence_get_CanChange_m6863,
	KeyPairPersistence_get_UseDefaultKeyContainer_m6864,
	KeyPairPersistence_get_UseMachineKeyStore_m6865,
	KeyPairPersistence_get_ContainerName_m6866,
	KeyPairPersistence_Copy_m6867,
	KeyPairPersistence_FromXml_m6868,
	KeyPairPersistence_ToXml_m6869,
	MACAlgorithm__ctor_m6870,
	MACAlgorithm_Initialize_m6871,
	MACAlgorithm_Core_m6872,
	MACAlgorithm_Final_m6873,
	PKCS1__cctor_m6874,
	PKCS1_Compare_m6875,
	PKCS1_I2OSP_m6876,
	PKCS1_OS2IP_m6877,
	PKCS1_RSAEP_m6878,
	PKCS1_RSASP1_m6879,
	PKCS1_RSAVP1_m6880,
	PKCS1_Encrypt_v15_m6881,
	PKCS1_Sign_v15_m6882,
	PKCS1_Verify_v15_m6883,
	PKCS1_Verify_v15_m6884,
	PKCS1_Encode_v15_m6885,
	PrivateKeyInfo__ctor_m6886,
	PrivateKeyInfo__ctor_m6887,
	PrivateKeyInfo_get_PrivateKey_m6888,
	PrivateKeyInfo_Decode_m6889,
	PrivateKeyInfo_RemoveLeadingZero_m6890,
	PrivateKeyInfo_Normalize_m6891,
	PrivateKeyInfo_DecodeRSA_m6892,
	PrivateKeyInfo_DecodeDSA_m6893,
	EncryptedPrivateKeyInfo__ctor_m6894,
	EncryptedPrivateKeyInfo__ctor_m6895,
	EncryptedPrivateKeyInfo_get_Algorithm_m6896,
	EncryptedPrivateKeyInfo_get_EncryptedData_m6897,
	EncryptedPrivateKeyInfo_get_Salt_m6898,
	EncryptedPrivateKeyInfo_get_IterationCount_m6899,
	EncryptedPrivateKeyInfo_Decode_m6900,
	KeyGeneratedEventHandler__ctor_m6901,
	KeyGeneratedEventHandler_Invoke_m6902,
	KeyGeneratedEventHandler_BeginInvoke_m6903,
	KeyGeneratedEventHandler_EndInvoke_m6904,
	RSAManaged__ctor_m6905,
	RSAManaged_add_KeyGenerated_m6906,
	RSAManaged_remove_KeyGenerated_m6907,
	RSAManaged_Finalize_m6908,
	RSAManaged_GenerateKeyPair_m6909,
	RSAManaged_get_KeySize_m6910,
	RSAManaged_get_PublicOnly_m6911,
	RSAManaged_DecryptValue_m6912,
	RSAManaged_EncryptValue_m6913,
	RSAManaged_ExportParameters_m6914,
	RSAManaged_ImportParameters_m6915,
	RSAManaged_Dispose_m6916,
	RSAManaged_ToXmlString_m6917,
	RSAManaged_get_IsCrtPossible_m6918,
	RSAManaged_GetPaddedValue_m6919,
	SymmetricTransform__ctor_m6920,
	SymmetricTransform_System_IDisposable_Dispose_m6921,
	SymmetricTransform_Finalize_m6922,
	SymmetricTransform_Dispose_m6923,
	SymmetricTransform_get_CanReuseTransform_m6924,
	SymmetricTransform_Transform_m6925,
	SymmetricTransform_CBC_m6926,
	SymmetricTransform_CFB_m6927,
	SymmetricTransform_OFB_m6928,
	SymmetricTransform_CTS_m6929,
	SymmetricTransform_CheckInput_m6930,
	SymmetricTransform_TransformBlock_m6931,
	SymmetricTransform_get_KeepLastBlock_m6932,
	SymmetricTransform_InternalTransformBlock_m6933,
	SymmetricTransform_Random_m6934,
	SymmetricTransform_ThrowBadPaddingException_m6935,
	SymmetricTransform_FinalEncrypt_m6936,
	SymmetricTransform_FinalDecrypt_m6937,
	SymmetricTransform_TransformFinalBlock_m6938,
	SafeBag__ctor_m6939,
	SafeBag_get_BagOID_m6940,
	SafeBag_get_ASN1_m6941,
	DeriveBytes__ctor_m6942,
	DeriveBytes__cctor_m6943,
	DeriveBytes_set_HashName_m6944,
	DeriveBytes_set_IterationCount_m6945,
	DeriveBytes_set_Password_m6946,
	DeriveBytes_set_Salt_m6947,
	DeriveBytes_Adjust_m6948,
	DeriveBytes_Derive_m6949,
	DeriveBytes_DeriveKey_m6950,
	DeriveBytes_DeriveIV_m6951,
	DeriveBytes_DeriveMAC_m6952,
	PKCS12__ctor_m6953,
	PKCS12__ctor_m6954,
	PKCS12__ctor_m6955,
	PKCS12__cctor_m6956,
	PKCS12_Decode_m6957,
	PKCS12_Finalize_m6958,
	PKCS12_set_Password_m6959,
	PKCS12_get_IterationCount_m6960,
	PKCS12_set_IterationCount_m6961,
	PKCS12_get_Certificates_m6962,
	PKCS12_get_RNG_m6963,
	PKCS12_Compare_m6964,
	PKCS12_GetSymmetricAlgorithm_m6965,
	PKCS12_Decrypt_m6966,
	PKCS12_Decrypt_m6967,
	PKCS12_Encrypt_m6968,
	PKCS12_GetExistingParameters_m6969,
	PKCS12_AddPrivateKey_m6970,
	PKCS12_ReadSafeBag_m6971,
	PKCS12_CertificateSafeBag_m6972,
	PKCS12_MAC_m6973,
	PKCS12_GetBytes_m6974,
	PKCS12_EncryptedContentInfo_m6975,
	PKCS12_AddCertificate_m6976,
	PKCS12_AddCertificate_m6977,
	PKCS12_RemoveCertificate_m6978,
	PKCS12_RemoveCertificate_m6979,
	PKCS12_Clone_m6980,
	PKCS12_get_MaximumPasswordLength_m6981,
	X501__cctor_m6982,
	X501_ToString_m6983,
	X501_ToString_m6984,
	X501_AppendEntry_m6985,
	X509Certificate__ctor_m6986,
	X509Certificate__cctor_m6987,
	X509Certificate_Parse_m6988,
	X509Certificate_GetUnsignedBigInteger_m6989,
	X509Certificate_get_DSA_m6990,
	X509Certificate_get_IssuerName_m6991,
	X509Certificate_get_KeyAlgorithmParameters_m6992,
	X509Certificate_get_PublicKey_m6993,
	X509Certificate_get_RawData_m6994,
	X509Certificate_get_SubjectName_m6995,
	X509Certificate_get_ValidFrom_m6996,
	X509Certificate_get_ValidUntil_m6997,
	X509Certificate_GetIssuerName_m6998,
	X509Certificate_GetSubjectName_m6999,
	X509Certificate_GetObjectData_m7000,
	X509Certificate_PEM_m7001,
	X509CertificateEnumerator__ctor_m7002,
	X509CertificateEnumerator_System_Collections_IEnumerator_get_Current_m7003,
	X509CertificateEnumerator_System_Collections_IEnumerator_MoveNext_m7004,
	X509CertificateEnumerator_System_Collections_IEnumerator_Reset_m7005,
	X509CertificateEnumerator_get_Current_m7006,
	X509CertificateEnumerator_MoveNext_m7007,
	X509CertificateEnumerator_Reset_m7008,
	X509CertificateCollection__ctor_m7009,
	X509CertificateCollection_System_Collections_IEnumerable_GetEnumerator_m7010,
	X509CertificateCollection_get_Item_m7011,
	X509CertificateCollection_Add_m7012,
	X509CertificateCollection_GetEnumerator_m7013,
	X509CertificateCollection_GetHashCode_m7014,
	X509Extension__ctor_m7015,
	X509Extension_Decode_m7016,
	X509Extension_Equals_m7017,
	X509Extension_GetHashCode_m7018,
	X509Extension_WriteLine_m7019,
	X509Extension_ToString_m7020,
	X509ExtensionCollection__ctor_m7021,
	X509ExtensionCollection__ctor_m7022,
	X509ExtensionCollection_System_Collections_IEnumerable_GetEnumerator_m7023,
	ASN1__ctor_m7024,
	ASN1__ctor_m7025,
	ASN1__ctor_m7026,
	ASN1_get_Count_m7027,
	ASN1_get_Tag_m7028,
	ASN1_get_Length_m7029,
	ASN1_get_Value_m7030,
	ASN1_set_Value_m7031,
	ASN1_CompareArray_m7032,
	ASN1_CompareValue_m7033,
	ASN1_Add_m7034,
	ASN1_GetBytes_m7035,
	ASN1_Decode_m7036,
	ASN1_DecodeTLV_m7037,
	ASN1_get_Item_m7038,
	ASN1_Element_m7039,
	ASN1_ToString_m7040,
	ASN1Convert_FromInt32_m7041,
	ASN1Convert_FromOid_m7042,
	ASN1Convert_ToInt32_m7043,
	ASN1Convert_ToOid_m7044,
	ASN1Convert_ToDateTime_m7045,
	BitConverterLE_GetUIntBytes_m7046,
	BitConverterLE_GetBytes_m7047,
	BitConverterLE_UShortFromBytes_m7048,
	BitConverterLE_UIntFromBytes_m7049,
	BitConverterLE_ULongFromBytes_m7050,
	BitConverterLE_ToInt16_m7051,
	BitConverterLE_ToInt32_m7052,
	BitConverterLE_ToSingle_m7053,
	BitConverterLE_ToDouble_m7054,
	ContentInfo__ctor_m7055,
	ContentInfo__ctor_m7056,
	ContentInfo__ctor_m7057,
	ContentInfo__ctor_m7058,
	ContentInfo_get_ASN1_m7059,
	ContentInfo_get_Content_m7060,
	ContentInfo_set_Content_m7061,
	ContentInfo_get_ContentType_m7062,
	ContentInfo_set_ContentType_m7063,
	ContentInfo_GetASN1_m7064,
	EncryptedData__ctor_m7065,
	EncryptedData__ctor_m7066,
	EncryptedData_get_EncryptionAlgorithm_m7067,
	EncryptedData_get_EncryptedContent_m7068,
	StrongName__cctor_m7069,
	StrongName_get_PublicKey_m7070,
	StrongName_get_PublicKeyToken_m7071,
	StrongName_get_TokenAlgorithm_m7072,
	SecurityParser__ctor_m7073,
	SecurityParser_LoadXml_m7074,
	SecurityParser_ToXml_m7075,
	SecurityParser_OnStartParsing_m7076,
	SecurityParser_OnProcessingInstruction_m7077,
	SecurityParser_OnIgnorableWhitespace_m7078,
	SecurityParser_OnStartElement_m7079,
	SecurityParser_OnEndElement_m7080,
	SecurityParser_OnChars_m7081,
	SecurityParser_OnEndParsing_m7082,
	AttrListImpl__ctor_m7083,
	AttrListImpl_get_Length_m7084,
	AttrListImpl_GetName_m7085,
	AttrListImpl_GetValue_m7086,
	AttrListImpl_GetValue_m7087,
	AttrListImpl_get_Names_m7088,
	AttrListImpl_get_Values_m7089,
	AttrListImpl_Clear_m7090,
	AttrListImpl_Add_m7091,
	SmallXmlParser__ctor_m7092,
	SmallXmlParser_Error_m7093,
	SmallXmlParser_UnexpectedEndError_m7094,
	SmallXmlParser_IsNameChar_m7095,
	SmallXmlParser_IsWhitespace_m7096,
	SmallXmlParser_SkipWhitespaces_m7097,
	SmallXmlParser_HandleWhitespaces_m7098,
	SmallXmlParser_SkipWhitespaces_m7099,
	SmallXmlParser_Peek_m7100,
	SmallXmlParser_Read_m7101,
	SmallXmlParser_Expect_m7102,
	SmallXmlParser_ReadUntil_m7103,
	SmallXmlParser_ReadName_m7104,
	SmallXmlParser_Parse_m7105,
	SmallXmlParser_Cleanup_m7106,
	SmallXmlParser_ReadContent_m7107,
	SmallXmlParser_HandleBufferedContent_m7108,
	SmallXmlParser_ReadCharacters_m7109,
	SmallXmlParser_ReadReference_m7110,
	SmallXmlParser_ReadCharacterReference_m7111,
	SmallXmlParser_ReadAttribute_m7112,
	SmallXmlParser_ReadCDATASection_m7113,
	SmallXmlParser_ReadComment_m7114,
	SmallXmlParserException__ctor_m7115,
	Runtime_GetDisplayName_m7116,
	KeyNotFoundException__ctor_m7117,
	KeyNotFoundException__ctor_m7118,
	SimpleEnumerator__ctor_m7119,
	SimpleEnumerator__cctor_m7120,
	SimpleEnumerator_Clone_m7121,
	SimpleEnumerator_MoveNext_m7122,
	SimpleEnumerator_get_Current_m7123,
	SimpleEnumerator_Reset_m7124,
	ArrayListWrapper__ctor_m7125,
	ArrayListWrapper_get_Item_m7126,
	ArrayListWrapper_set_Item_m7127,
	ArrayListWrapper_get_Count_m7128,
	ArrayListWrapper_get_Capacity_m7129,
	ArrayListWrapper_set_Capacity_m7130,
	ArrayListWrapper_get_IsFixedSize_m7131,
	ArrayListWrapper_get_IsReadOnly_m7132,
	ArrayListWrapper_get_IsSynchronized_m7133,
	ArrayListWrapper_get_SyncRoot_m7134,
	ArrayListWrapper_Add_m7135,
	ArrayListWrapper_Clear_m7136,
	ArrayListWrapper_Contains_m7137,
	ArrayListWrapper_IndexOf_m7138,
	ArrayListWrapper_IndexOf_m7139,
	ArrayListWrapper_IndexOf_m7140,
	ArrayListWrapper_Insert_m7141,
	ArrayListWrapper_InsertRange_m7142,
	ArrayListWrapper_Remove_m7143,
	ArrayListWrapper_RemoveAt_m7144,
	ArrayListWrapper_CopyTo_m7145,
	ArrayListWrapper_CopyTo_m7146,
	ArrayListWrapper_CopyTo_m7147,
	ArrayListWrapper_GetEnumerator_m7148,
	ArrayListWrapper_AddRange_m7149,
	ArrayListWrapper_Clone_m7150,
	ArrayListWrapper_Sort_m7151,
	ArrayListWrapper_Sort_m7152,
	ArrayListWrapper_ToArray_m7153,
	ArrayListWrapper_ToArray_m7154,
	SynchronizedArrayListWrapper__ctor_m7155,
	SynchronizedArrayListWrapper_get_Item_m7156,
	SynchronizedArrayListWrapper_set_Item_m7157,
	SynchronizedArrayListWrapper_get_Count_m7158,
	SynchronizedArrayListWrapper_get_Capacity_m7159,
	SynchronizedArrayListWrapper_set_Capacity_m7160,
	SynchronizedArrayListWrapper_get_IsFixedSize_m7161,
	SynchronizedArrayListWrapper_get_IsReadOnly_m7162,
	SynchronizedArrayListWrapper_get_IsSynchronized_m7163,
	SynchronizedArrayListWrapper_get_SyncRoot_m7164,
	SynchronizedArrayListWrapper_Add_m7165,
	SynchronizedArrayListWrapper_Clear_m7166,
	SynchronizedArrayListWrapper_Contains_m7167,
	SynchronizedArrayListWrapper_IndexOf_m7168,
	SynchronizedArrayListWrapper_IndexOf_m7169,
	SynchronizedArrayListWrapper_IndexOf_m7170,
	SynchronizedArrayListWrapper_Insert_m7171,
	SynchronizedArrayListWrapper_InsertRange_m7172,
	SynchronizedArrayListWrapper_Remove_m7173,
	SynchronizedArrayListWrapper_RemoveAt_m7174,
	SynchronizedArrayListWrapper_CopyTo_m7175,
	SynchronizedArrayListWrapper_CopyTo_m7176,
	SynchronizedArrayListWrapper_CopyTo_m7177,
	SynchronizedArrayListWrapper_GetEnumerator_m7178,
	SynchronizedArrayListWrapper_AddRange_m7179,
	SynchronizedArrayListWrapper_Clone_m7180,
	SynchronizedArrayListWrapper_Sort_m7181,
	SynchronizedArrayListWrapper_Sort_m7182,
	SynchronizedArrayListWrapper_ToArray_m7183,
	SynchronizedArrayListWrapper_ToArray_m7184,
	FixedSizeArrayListWrapper__ctor_m7185,
	FixedSizeArrayListWrapper_get_ErrorMessage_m7186,
	FixedSizeArrayListWrapper_get_Capacity_m7187,
	FixedSizeArrayListWrapper_set_Capacity_m7188,
	FixedSizeArrayListWrapper_get_IsFixedSize_m7189,
	FixedSizeArrayListWrapper_Add_m7190,
	FixedSizeArrayListWrapper_AddRange_m7191,
	FixedSizeArrayListWrapper_Clear_m7192,
	FixedSizeArrayListWrapper_Insert_m7193,
	FixedSizeArrayListWrapper_InsertRange_m7194,
	FixedSizeArrayListWrapper_Remove_m7195,
	FixedSizeArrayListWrapper_RemoveAt_m7196,
	ReadOnlyArrayListWrapper__ctor_m7197,
	ReadOnlyArrayListWrapper_get_ErrorMessage_m7198,
	ReadOnlyArrayListWrapper_get_IsReadOnly_m7199,
	ReadOnlyArrayListWrapper_get_Item_m7200,
	ReadOnlyArrayListWrapper_set_Item_m7201,
	ReadOnlyArrayListWrapper_Sort_m7202,
	ReadOnlyArrayListWrapper_Sort_m7203,
	ArrayList__ctor_m4635,
	ArrayList__ctor_m5685,
	ArrayList__ctor_m5707,
	ArrayList__ctor_m7204,
	ArrayList__cctor_m7205,
	ArrayList_get_Item_m7206,
	ArrayList_set_Item_m7207,
	ArrayList_get_Count_m7208,
	ArrayList_get_Capacity_m7209,
	ArrayList_set_Capacity_m7210,
	ArrayList_get_IsFixedSize_m7211,
	ArrayList_get_IsReadOnly_m7212,
	ArrayList_get_IsSynchronized_m7213,
	ArrayList_get_SyncRoot_m7214,
	ArrayList_EnsureCapacity_m7215,
	ArrayList_Shift_m7216,
	ArrayList_Add_m7217,
	ArrayList_Clear_m7218,
	ArrayList_Contains_m7219,
	ArrayList_IndexOf_m7220,
	ArrayList_IndexOf_m7221,
	ArrayList_IndexOf_m7222,
	ArrayList_Insert_m7223,
	ArrayList_InsertRange_m7224,
	ArrayList_Remove_m7225,
	ArrayList_RemoveAt_m7226,
	ArrayList_CopyTo_m7227,
	ArrayList_CopyTo_m7228,
	ArrayList_CopyTo_m7229,
	ArrayList_GetEnumerator_m7230,
	ArrayList_AddRange_m7231,
	ArrayList_Sort_m7232,
	ArrayList_Sort_m7233,
	ArrayList_ToArray_m7234,
	ArrayList_ToArray_m7235,
	ArrayList_Clone_m7236,
	ArrayList_ThrowNewArgumentOutOfRangeException_m7237,
	ArrayList_Synchronized_m7238,
	ArrayList_ReadOnly_m4668,
	BitArrayEnumerator__ctor_m7239,
	BitArrayEnumerator_Clone_m7240,
	BitArrayEnumerator_get_Current_m7241,
	BitArrayEnumerator_MoveNext_m7242,
	BitArrayEnumerator_Reset_m7243,
	BitArrayEnumerator_checkVersion_m7244,
	BitArray__ctor_m7245,
	BitArray__ctor_m5725,
	BitArray_getByte_m7246,
	BitArray_get_Count_m7247,
	BitArray_get_IsSynchronized_m7248,
	BitArray_get_Item_m5720,
	BitArray_set_Item_m5726,
	BitArray_get_Length_m5719,
	BitArray_get_SyncRoot_m7249,
	BitArray_Clone_m7250,
	BitArray_CopyTo_m7251,
	BitArray_Get_m7252,
	BitArray_Set_m7253,
	BitArray_GetEnumerator_m7254,
	CaseInsensitiveComparer__ctor_m7255,
	CaseInsensitiveComparer__ctor_m7256,
	CaseInsensitiveComparer__cctor_m7257,
	CaseInsensitiveComparer_get_DefaultInvariant_m5648,
	CaseInsensitiveComparer_Compare_m7258,
	CaseInsensitiveHashCodeProvider__ctor_m7259,
	CaseInsensitiveHashCodeProvider__ctor_m7260,
	CaseInsensitiveHashCodeProvider__cctor_m7261,
	CaseInsensitiveHashCodeProvider_AreEqual_m7262,
	CaseInsensitiveHashCodeProvider_AreEqual_m7263,
	CaseInsensitiveHashCodeProvider_get_DefaultInvariant_m5649,
	CaseInsensitiveHashCodeProvider_GetHashCode_m7264,
	CollectionBase__ctor_m4690,
	CollectionBase_System_Collections_ICollection_CopyTo_m7265,
	CollectionBase_System_Collections_ICollection_get_SyncRoot_m7266,
	CollectionBase_System_Collections_ICollection_get_IsSynchronized_m7267,
	CollectionBase_System_Collections_IList_Add_m7268,
	CollectionBase_System_Collections_IList_Contains_m7269,
	CollectionBase_System_Collections_IList_IndexOf_m7270,
	CollectionBase_System_Collections_IList_Insert_m7271,
	CollectionBase_System_Collections_IList_Remove_m7272,
	CollectionBase_System_Collections_IList_get_IsFixedSize_m7273,
	CollectionBase_System_Collections_IList_get_IsReadOnly_m7274,
	CollectionBase_System_Collections_IList_get_Item_m7275,
	CollectionBase_System_Collections_IList_set_Item_m7276,
	CollectionBase_get_Count_m7277,
	CollectionBase_GetEnumerator_m7278,
	CollectionBase_Clear_m7279,
	CollectionBase_RemoveAt_m7280,
	CollectionBase_get_InnerList_m4691,
	CollectionBase_get_List_m5723,
	CollectionBase_OnClear_m7281,
	CollectionBase_OnClearComplete_m7282,
	CollectionBase_OnInsert_m7283,
	CollectionBase_OnInsertComplete_m7284,
	CollectionBase_OnRemove_m7285,
	CollectionBase_OnRemoveComplete_m7286,
	CollectionBase_OnSet_m7287,
	CollectionBase_OnSetComplete_m7288,
	CollectionBase_OnValidate_m7289,
	Comparer__ctor_m7290,
	Comparer__ctor_m7291,
	Comparer__cctor_m7292,
	Comparer_Compare_m7293,
	Comparer_GetObjectData_m7294,
	DictionaryEntry__ctor_m5652,
	DictionaryEntry_get_Key_m7295,
	DictionaryEntry_get_Value_m7296,
	KeyMarker__ctor_m7297,
	KeyMarker__cctor_m7298,
	Enumerator__ctor_m7299,
	Enumerator__cctor_m7300,
	Enumerator_FailFast_m7301,
	Enumerator_Reset_m7302,
	Enumerator_MoveNext_m7303,
	Enumerator_get_Entry_m7304,
	Enumerator_get_Key_m7305,
	Enumerator_get_Value_m7306,
	Enumerator_get_Current_m7307,
	HashKeys__ctor_m7308,
	HashKeys_get_Count_m7309,
	HashKeys_get_IsSynchronized_m7310,
	HashKeys_get_SyncRoot_m7311,
	HashKeys_CopyTo_m7312,
	HashKeys_GetEnumerator_m7313,
	HashValues__ctor_m7314,
	HashValues_get_Count_m7315,
	HashValues_get_IsSynchronized_m7316,
	HashValues_get_SyncRoot_m7317,
	HashValues_CopyTo_m7318,
	HashValues_GetEnumerator_m7319,
	SyncHashtable__ctor_m7320,
	SyncHashtable__ctor_m7321,
	SyncHashtable_System_Collections_IEnumerable_GetEnumerator_m7322,
	SyncHashtable_GetObjectData_m7323,
	SyncHashtable_get_Count_m7324,
	SyncHashtable_get_IsSynchronized_m7325,
	SyncHashtable_get_SyncRoot_m7326,
	SyncHashtable_get_Keys_m7327,
	SyncHashtable_get_Values_m7328,
	SyncHashtable_get_Item_m7329,
	SyncHashtable_set_Item_m7330,
	SyncHashtable_CopyTo_m7331,
	SyncHashtable_Add_m7332,
	SyncHashtable_Clear_m7333,
	SyncHashtable_Contains_m7334,
	SyncHashtable_GetEnumerator_m7335,
	SyncHashtable_Remove_m7336,
	SyncHashtable_ContainsKey_m7337,
	SyncHashtable_Clone_m7338,
	Hashtable__ctor_m4718,
	Hashtable__ctor_m7339,
	Hashtable__ctor_m7340,
	Hashtable__ctor_m5714,
	Hashtable__ctor_m7341,
	Hashtable__ctor_m5650,
	Hashtable__ctor_m7342,
	Hashtable__ctor_m5651,
	Hashtable__ctor_m5682,
	Hashtable__ctor_m7343,
	Hashtable__ctor_m5658,
	Hashtable__ctor_m7344,
	Hashtable__cctor_m7345,
	Hashtable_System_Collections_IEnumerable_GetEnumerator_m7346,
	Hashtable_set_comparer_m7347,
	Hashtable_set_hcp_m7348,
	Hashtable_get_Count_m7349,
	Hashtable_get_IsSynchronized_m7350,
	Hashtable_get_SyncRoot_m7351,
	Hashtable_get_Keys_m7352,
	Hashtable_get_Values_m7353,
	Hashtable_get_Item_m7354,
	Hashtable_set_Item_m7355,
	Hashtable_CopyTo_m7356,
	Hashtable_Add_m7357,
	Hashtable_Clear_m7358,
	Hashtable_Contains_m7359,
	Hashtable_GetEnumerator_m7360,
	Hashtable_Remove_m7361,
	Hashtable_ContainsKey_m7362,
	Hashtable_Clone_m7363,
	Hashtable_GetObjectData_m7364,
	Hashtable_OnDeserialization_m7365,
	Hashtable_Synchronized_m7366,
	Hashtable_GetHash_m7367,
	Hashtable_KeyEquals_m7368,
	Hashtable_AdjustThreshold_m7369,
	Hashtable_SetTable_m7370,
	Hashtable_Find_m7371,
	Hashtable_Rehash_m7372,
	Hashtable_PutImpl_m7373,
	Hashtable_CopyToArray_m7374,
	Hashtable_TestPrime_m7375,
	Hashtable_CalcPrime_m7376,
	Hashtable_ToPrime_m7377,
	Enumerator__ctor_m7378,
	Enumerator__cctor_m7379,
	Enumerator_Reset_m7380,
	Enumerator_MoveNext_m7381,
	Enumerator_get_Entry_m7382,
	Enumerator_get_Key_m7383,
	Enumerator_get_Value_m7384,
	Enumerator_get_Current_m7385,
	Enumerator_Clone_m7386,
	SortedList__ctor_m7387,
	SortedList__ctor_m5681,
	SortedList__ctor_m7388,
	SortedList__ctor_m7389,
	SortedList__cctor_m7390,
	SortedList_System_Collections_IEnumerable_GetEnumerator_m7391,
	SortedList_get_Count_m7392,
	SortedList_get_IsSynchronized_m7393,
	SortedList_get_SyncRoot_m7394,
	SortedList_get_IsFixedSize_m7395,
	SortedList_get_IsReadOnly_m7396,
	SortedList_get_Item_m7397,
	SortedList_set_Item_m7398,
	SortedList_get_Capacity_m7399,
	SortedList_set_Capacity_m7400,
	SortedList_Add_m7401,
	SortedList_Contains_m7402,
	SortedList_GetEnumerator_m7403,
	SortedList_Remove_m7404,
	SortedList_CopyTo_m7405,
	SortedList_Clone_m7406,
	SortedList_RemoveAt_m7407,
	SortedList_IndexOfKey_m7408,
	SortedList_ContainsKey_m7409,
	SortedList_GetByIndex_m7410,
	SortedList_EnsureCapacity_m7411,
	SortedList_PutImpl_m7412,
	SortedList_GetImpl_m7413,
	SortedList_InitTable_m7414,
	SortedList_Find_m7415,
	Enumerator__ctor_m7416,
	Enumerator_Clone_m7417,
	Enumerator_get_Current_m7418,
	Enumerator_MoveNext_m7419,
	Enumerator_Reset_m7420,
	Stack__ctor_m3674,
	Stack__ctor_m7421,
	Stack__ctor_m7422,
	Stack_Resize_m7423,
	Stack_get_Count_m7424,
	Stack_get_IsSynchronized_m7425,
	Stack_get_SyncRoot_m7426,
	Stack_Clear_m7427,
	Stack_Clone_m7428,
	Stack_CopyTo_m7429,
	Stack_GetEnumerator_m7430,
	Stack_Peek_m7431,
	Stack_Pop_m7432,
	Stack_Push_m7433,
	DebuggableAttribute__ctor_m7434,
	DebuggerDisplayAttribute__ctor_m7435,
	DebuggerDisplayAttribute_set_Name_m7436,
	DebuggerStepThroughAttribute__ctor_m7437,
	DebuggerTypeProxyAttribute__ctor_m7438,
	StackFrame__ctor_m7439,
	StackFrame__ctor_m7440,
	StackFrame_get_frame_info_m7441,
	StackFrame_GetFileLineNumber_m7442,
	StackFrame_GetFileName_m7443,
	StackFrame_GetSecureFileName_m7444,
	StackFrame_GetILOffset_m7445,
	StackFrame_GetMethod_m7446,
	StackFrame_GetNativeOffset_m7447,
	StackFrame_GetInternalMethodName_m7448,
	StackFrame_ToString_m7449,
	StackTrace__ctor_m7450,
	StackTrace__ctor_m3636,
	StackTrace__ctor_m7451,
	StackTrace__ctor_m7452,
	StackTrace__ctor_m7453,
	StackTrace_init_frames_m7454,
	StackTrace_get_trace_m7455,
	StackTrace_get_FrameCount_m7456,
	StackTrace_GetFrame_m7457,
	StackTrace_ToString_m7458,
	Calendar__ctor_m7459,
	Calendar_Clone_m7460,
	Calendar_CheckReadOnly_m7461,
	Calendar_get_EraNames_m7462,
	CCMath_div_m7463,
	CCMath_mod_m7464,
	CCMath_div_mod_m7465,
	CCFixed_FromDateTime_m7466,
	CCFixed_day_of_week_m7467,
	CCGregorianCalendar_is_leap_year_m7468,
	CCGregorianCalendar_fixed_from_dmy_m7469,
	CCGregorianCalendar_year_from_fixed_m7470,
	CCGregorianCalendar_my_from_fixed_m7471,
	CCGregorianCalendar_dmy_from_fixed_m7472,
	CCGregorianCalendar_month_from_fixed_m7473,
	CCGregorianCalendar_day_from_fixed_m7474,
	CCGregorianCalendar_GetDayOfMonth_m7475,
	CCGregorianCalendar_GetMonth_m7476,
	CCGregorianCalendar_GetYear_m7477,
	CompareInfo__ctor_m7478,
	CompareInfo__ctor_m7479,
	CompareInfo__cctor_m7480,
	CompareInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7481,
	CompareInfo_get_UseManagedCollation_m7482,
	CompareInfo_construct_compareinfo_m7483,
	CompareInfo_free_internal_collator_m7484,
	CompareInfo_internal_compare_m7485,
	CompareInfo_assign_sortkey_m7486,
	CompareInfo_internal_index_m7487,
	CompareInfo_Finalize_m7488,
	CompareInfo_internal_compare_managed_m7489,
	CompareInfo_internal_compare_switch_m7490,
	CompareInfo_Compare_m7491,
	CompareInfo_Compare_m7492,
	CompareInfo_Compare_m7493,
	CompareInfo_Equals_m7494,
	CompareInfo_GetHashCode_m7495,
	CompareInfo_GetSortKey_m7496,
	CompareInfo_IndexOf_m7497,
	CompareInfo_internal_index_managed_m7498,
	CompareInfo_internal_index_switch_m7499,
	CompareInfo_IndexOf_m7500,
	CompareInfo_IsPrefix_m7501,
	CompareInfo_IsSuffix_m7502,
	CompareInfo_LastIndexOf_m7503,
	CompareInfo_LastIndexOf_m7504,
	CompareInfo_ToString_m7505,
	CompareInfo_get_LCID_m7506,
	CultureInfo__ctor_m7507,
	CultureInfo__ctor_m7508,
	CultureInfo__ctor_m7509,
	CultureInfo__ctor_m7510,
	CultureInfo__ctor_m7511,
	CultureInfo__cctor_m7512,
	CultureInfo_get_InvariantCulture_m4644,
	CultureInfo_get_CurrentCulture_m4712,
	CultureInfo_get_CurrentUICulture_m4714,
	CultureInfo_ConstructCurrentCulture_m7513,
	CultureInfo_ConstructCurrentUICulture_m7514,
	CultureInfo_get_LCID_m7515,
	CultureInfo_get_Name_m7516,
	CultureInfo_get_Parent_m7517,
	CultureInfo_get_TextInfo_m7518,
	CultureInfo_get_IcuName_m7519,
	CultureInfo_Clone_m7520,
	CultureInfo_Equals_m7521,
	CultureInfo_GetHashCode_m7522,
	CultureInfo_ToString_m7523,
	CultureInfo_get_CompareInfo_m7524,
	CultureInfo_get_IsNeutralCulture_m7525,
	CultureInfo_CheckNeutral_m7526,
	CultureInfo_get_NumberFormat_m7527,
	CultureInfo_set_NumberFormat_m7528,
	CultureInfo_get_DateTimeFormat_m7529,
	CultureInfo_set_DateTimeFormat_m7530,
	CultureInfo_get_IsReadOnly_m7531,
	CultureInfo_GetFormat_m7532,
	CultureInfo_Construct_m7533,
	CultureInfo_ConstructInternalLocaleFromName_m7534,
	CultureInfo_ConstructInternalLocaleFromLcid_m7535,
	CultureInfo_ConstructInternalLocaleFromCurrentLocale_m7536,
	CultureInfo_construct_internal_locale_from_lcid_m7537,
	CultureInfo_construct_internal_locale_from_name_m7538,
	CultureInfo_construct_internal_locale_from_current_locale_m7539,
	CultureInfo_construct_datetime_format_m7540,
	CultureInfo_construct_number_format_m7541,
	CultureInfo_ConstructInvariant_m7542,
	CultureInfo_CreateTextInfo_m7543,
	CultureInfo_CreateCulture_m7544,
	DateTimeFormatInfo__ctor_m7545,
	DateTimeFormatInfo__ctor_m7546,
	DateTimeFormatInfo__cctor_m7547,
	DateTimeFormatInfo_GetInstance_m7548,
	DateTimeFormatInfo_get_IsReadOnly_m7549,
	DateTimeFormatInfo_ReadOnly_m7550,
	DateTimeFormatInfo_Clone_m7551,
	DateTimeFormatInfo_GetFormat_m7552,
	DateTimeFormatInfo_GetAbbreviatedMonthName_m7553,
	DateTimeFormatInfo_GetEraName_m7554,
	DateTimeFormatInfo_GetMonthName_m7555,
	DateTimeFormatInfo_get_RawAbbreviatedDayNames_m7556,
	DateTimeFormatInfo_get_RawAbbreviatedMonthNames_m7557,
	DateTimeFormatInfo_get_RawDayNames_m7558,
	DateTimeFormatInfo_get_RawMonthNames_m7559,
	DateTimeFormatInfo_get_AMDesignator_m7560,
	DateTimeFormatInfo_get_PMDesignator_m7561,
	DateTimeFormatInfo_get_DateSeparator_m7562,
	DateTimeFormatInfo_get_TimeSeparator_m7563,
	DateTimeFormatInfo_get_LongDatePattern_m7564,
	DateTimeFormatInfo_get_ShortDatePattern_m7565,
	DateTimeFormatInfo_get_ShortTimePattern_m7566,
	DateTimeFormatInfo_get_LongTimePattern_m7567,
	DateTimeFormatInfo_get_MonthDayPattern_m7568,
	DateTimeFormatInfo_get_YearMonthPattern_m7569,
	DateTimeFormatInfo_get_FullDateTimePattern_m7570,
	DateTimeFormatInfo_get_CurrentInfo_m7571,
	DateTimeFormatInfo_get_InvariantInfo_m7572,
	DateTimeFormatInfo_get_Calendar_m7573,
	DateTimeFormatInfo_set_Calendar_m7574,
	DateTimeFormatInfo_get_RFC1123Pattern_m7575,
	DateTimeFormatInfo_get_RoundtripPattern_m7576,
	DateTimeFormatInfo_get_SortableDateTimePattern_m7577,
	DateTimeFormatInfo_get_UniversalSortableDateTimePattern_m7578,
	DateTimeFormatInfo_GetAllDateTimePatternsInternal_m7579,
	DateTimeFormatInfo_FillAllDateTimePatterns_m7580,
	DateTimeFormatInfo_GetAllRawDateTimePatterns_m7581,
	DateTimeFormatInfo_GetDayName_m7582,
	DateTimeFormatInfo_GetAbbreviatedDayName_m7583,
	DateTimeFormatInfo_FillInvariantPatterns_m7584,
	DateTimeFormatInfo_PopulateCombinedList_m7585,
	DaylightTime__ctor_m7586,
	DaylightTime_get_Start_m7587,
	DaylightTime_get_End_m7588,
	DaylightTime_get_Delta_m7589,
	GregorianCalendar__ctor_m7590,
	GregorianCalendar__ctor_m7591,
	GregorianCalendar_get_Eras_m7592,
	GregorianCalendar_set_CalendarType_m7593,
	GregorianCalendar_GetDayOfMonth_m7594,
	GregorianCalendar_GetDayOfWeek_m7595,
	GregorianCalendar_GetEra_m7596,
	GregorianCalendar_GetMonth_m7597,
	GregorianCalendar_GetYear_m7598,
	NumberFormatInfo__ctor_m7599,
	NumberFormatInfo__ctor_m7600,
	NumberFormatInfo__ctor_m7601,
	NumberFormatInfo__cctor_m7602,
	NumberFormatInfo_get_CurrencyDecimalDigits_m7603,
	NumberFormatInfo_get_CurrencyDecimalSeparator_m7604,
	NumberFormatInfo_get_CurrencyGroupSeparator_m7605,
	NumberFormatInfo_get_RawCurrencyGroupSizes_m7606,
	NumberFormatInfo_get_CurrencyNegativePattern_m7607,
	NumberFormatInfo_get_CurrencyPositivePattern_m7608,
	NumberFormatInfo_get_CurrencySymbol_m7609,
	NumberFormatInfo_get_CurrentInfo_m7610,
	NumberFormatInfo_get_InvariantInfo_m7611,
	NumberFormatInfo_get_NaNSymbol_m7612,
	NumberFormatInfo_get_NegativeInfinitySymbol_m7613,
	NumberFormatInfo_get_NegativeSign_m7614,
	NumberFormatInfo_get_NumberDecimalDigits_m7615,
	NumberFormatInfo_get_NumberDecimalSeparator_m7616,
	NumberFormatInfo_get_NumberGroupSeparator_m7617,
	NumberFormatInfo_get_RawNumberGroupSizes_m7618,
	NumberFormatInfo_get_NumberNegativePattern_m7619,
	NumberFormatInfo_set_NumberNegativePattern_m7620,
	NumberFormatInfo_get_PercentDecimalDigits_m7621,
	NumberFormatInfo_get_PercentDecimalSeparator_m7622,
	NumberFormatInfo_get_PercentGroupSeparator_m7623,
	NumberFormatInfo_get_RawPercentGroupSizes_m7624,
	NumberFormatInfo_get_PercentNegativePattern_m7625,
	NumberFormatInfo_get_PercentPositivePattern_m7626,
	NumberFormatInfo_get_PercentSymbol_m7627,
	NumberFormatInfo_get_PerMilleSymbol_m7628,
	NumberFormatInfo_get_PositiveInfinitySymbol_m7629,
	NumberFormatInfo_get_PositiveSign_m7630,
	NumberFormatInfo_GetFormat_m7631,
	NumberFormatInfo_Clone_m7632,
	NumberFormatInfo_GetInstance_m7633,
	TextInfo__ctor_m7634,
	TextInfo__ctor_m7635,
	TextInfo_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m7636,
	TextInfo_get_ListSeparator_m7637,
	TextInfo_get_CultureName_m7638,
	TextInfo_Equals_m7639,
	TextInfo_GetHashCode_m7640,
	TextInfo_ToString_m7641,
	TextInfo_ToLower_m7642,
	TextInfo_ToUpper_m7643,
	TextInfo_ToLower_m7644,
	TextInfo_Clone_m7645,
	IsolatedStorageException__ctor_m7646,
	IsolatedStorageException__ctor_m7647,
	IsolatedStorageException__ctor_m7648,
	BinaryReader__ctor_m7649,
	BinaryReader__ctor_m7650,
	BinaryReader_System_IDisposable_Dispose_m7651,
	BinaryReader_get_BaseStream_m7652,
	BinaryReader_Close_m7653,
	BinaryReader_Dispose_m7654,
	BinaryReader_FillBuffer_m7655,
	BinaryReader_Read_m7656,
	BinaryReader_Read_m7657,
	BinaryReader_Read_m7658,
	BinaryReader_ReadCharBytes_m7659,
	BinaryReader_Read7BitEncodedInt_m7660,
	BinaryReader_ReadBoolean_m7661,
	BinaryReader_ReadByte_m7662,
	BinaryReader_ReadBytes_m7663,
	BinaryReader_ReadChar_m7664,
	BinaryReader_ReadDecimal_m7665,
	BinaryReader_ReadDouble_m7666,
	BinaryReader_ReadInt16_m7667,
	BinaryReader_ReadInt32_m7668,
	BinaryReader_ReadInt64_m7669,
	BinaryReader_ReadSByte_m7670,
	BinaryReader_ReadString_m7671,
	BinaryReader_ReadSingle_m7672,
	BinaryReader_ReadUInt16_m7673,
	BinaryReader_ReadUInt32_m7674,
	BinaryReader_ReadUInt64_m7675,
	BinaryReader_CheckBuffer_m7676,
	Directory_CreateDirectory_m4697,
	Directory_CreateDirectoriesInternal_m7677,
	Directory_Exists_m4696,
	Directory_GetCurrentDirectory_m7678,
	Directory_GetFiles_m4699,
	Directory_GetFileSystemEntries_m7679,
	DirectoryInfo__ctor_m7680,
	DirectoryInfo__ctor_m7681,
	DirectoryInfo__ctor_m7682,
	DirectoryInfo_Initialize_m7683,
	DirectoryInfo_get_Exists_m7684,
	DirectoryInfo_get_Parent_m7685,
	DirectoryInfo_Create_m7686,
	DirectoryInfo_ToString_m7687,
	DirectoryNotFoundException__ctor_m7688,
	DirectoryNotFoundException__ctor_m7689,
	DirectoryNotFoundException__ctor_m7690,
	EndOfStreamException__ctor_m7691,
	EndOfStreamException__ctor_m7692,
	File_Delete_m7693,
	File_Exists_m7694,
	File_Open_m7695,
	File_OpenRead_m4695,
	File_OpenText_m7696,
	FileNotFoundException__ctor_m7697,
	FileNotFoundException__ctor_m7698,
	FileNotFoundException__ctor_m7699,
	FileNotFoundException_get_Message_m7700,
	FileNotFoundException_GetObjectData_m7701,
	FileNotFoundException_ToString_m7702,
	ReadDelegate__ctor_m7703,
	ReadDelegate_Invoke_m7704,
	ReadDelegate_BeginInvoke_m7705,
	ReadDelegate_EndInvoke_m7706,
	WriteDelegate__ctor_m7707,
	WriteDelegate_Invoke_m7708,
	WriteDelegate_BeginInvoke_m7709,
	WriteDelegate_EndInvoke_m7710,
	FileStream__ctor_m7711,
	FileStream__ctor_m7712,
	FileStream__ctor_m7713,
	FileStream__ctor_m7714,
	FileStream__ctor_m7715,
	FileStream_get_CanRead_m7716,
	FileStream_get_CanWrite_m7717,
	FileStream_get_CanSeek_m7718,
	FileStream_get_Length_m7719,
	FileStream_get_Position_m7720,
	FileStream_set_Position_m7721,
	FileStream_ReadByte_m7722,
	FileStream_WriteByte_m7723,
	FileStream_Read_m7724,
	FileStream_ReadInternal_m7725,
	FileStream_BeginRead_m7726,
	FileStream_EndRead_m7727,
	FileStream_Write_m7728,
	FileStream_WriteInternal_m7729,
	FileStream_BeginWrite_m7730,
	FileStream_EndWrite_m7731,
	FileStream_Seek_m7732,
	FileStream_SetLength_m7733,
	FileStream_Flush_m7734,
	FileStream_Finalize_m7735,
	FileStream_Dispose_m7736,
	FileStream_ReadSegment_m7737,
	FileStream_WriteSegment_m7738,
	FileStream_FlushBuffer_m7739,
	FileStream_FlushBuffer_m7740,
	FileStream_FlushBufferIfDirty_m7741,
	FileStream_RefillBuffer_m7742,
	FileStream_ReadData_m7743,
	FileStream_InitBuffer_m7744,
	FileStream_GetSecureFileName_m7745,
	FileStream_GetSecureFileName_m7746,
	FileStreamAsyncResult__ctor_m7747,
	FileStreamAsyncResult_CBWrapper_m7748,
	FileStreamAsyncResult_get_AsyncState_m7749,
	FileStreamAsyncResult_get_AsyncWaitHandle_m7750,
	FileStreamAsyncResult_get_IsCompleted_m7751,
	FileSystemInfo__ctor_m7752,
	FileSystemInfo__ctor_m7753,
	FileSystemInfo_GetObjectData_m7754,
	FileSystemInfo_get_FullName_m7755,
	FileSystemInfo_Refresh_m7756,
	FileSystemInfo_InternalRefresh_m7757,
	FileSystemInfo_CheckPath_m7758,
	IOException__ctor_m7759,
	IOException__ctor_m7760,
	IOException__ctor_m4745,
	IOException__ctor_m7761,
	IOException__ctor_m7762,
	MemoryStream__ctor_m4746,
	MemoryStream__ctor_m4751,
	MemoryStream__ctor_m4752,
	MemoryStream_InternalConstructor_m7763,
	MemoryStream_CheckIfClosedThrowDisposed_m7764,
	MemoryStream_get_CanRead_m7765,
	MemoryStream_get_CanSeek_m7766,
	MemoryStream_get_CanWrite_m7767,
	MemoryStream_set_Capacity_m7768,
	MemoryStream_get_Length_m7769,
	MemoryStream_get_Position_m7770,
	MemoryStream_set_Position_m7771,
	MemoryStream_Dispose_m7772,
	MemoryStream_Flush_m7773,
	MemoryStream_Read_m7774,
	MemoryStream_ReadByte_m7775,
	MemoryStream_Seek_m7776,
	MemoryStream_CalculateNewCapacity_m7777,
	MemoryStream_Expand_m7778,
	MemoryStream_SetLength_m7779,
	MemoryStream_ToArray_m7780,
	MemoryStream_Write_m7781,
	MemoryStream_WriteByte_m7782,
	MonoIO__cctor_m7783,
	MonoIO_GetException_m7784,
	MonoIO_GetException_m7785,
	MonoIO_CreateDirectory_m7786,
	MonoIO_GetFileSystemEntries_m7787,
	MonoIO_GetCurrentDirectory_m7788,
	MonoIO_DeleteFile_m7789,
	MonoIO_GetFileAttributes_m7790,
	MonoIO_GetFileType_m7791,
	MonoIO_ExistsFile_m7792,
	MonoIO_ExistsDirectory_m7793,
	MonoIO_GetFileStat_m7794,
	MonoIO_Open_m7795,
	MonoIO_Close_m7796,
	MonoIO_Read_m7797,
	MonoIO_Write_m7798,
	MonoIO_Seek_m7799,
	MonoIO_GetLength_m7800,
	MonoIO_SetLength_m7801,
	MonoIO_get_ConsoleOutput_m7802,
	MonoIO_get_ConsoleInput_m7803,
	MonoIO_get_ConsoleError_m7804,
	MonoIO_get_VolumeSeparatorChar_m7805,
	MonoIO_get_DirectorySeparatorChar_m7806,
	MonoIO_get_AltDirectorySeparatorChar_m7807,
	MonoIO_get_PathSeparator_m7808,
	Path__cctor_m7809,
	Path_Combine_m4698,
	Path_CleanPath_m7810,
	Path_GetDirectoryName_m7811,
	Path_GetFileName_m7812,
	Path_GetFullPath_m7813,
	Path_WindowsDriveAdjustment_m7814,
	Path_InsecureGetFullPath_m7815,
	Path_IsDsc_m7816,
	Path_GetPathRoot_m7817,
	Path_IsPathRooted_m7818,
	Path_GetInvalidPathChars_m7819,
	Path_GetServerAndShare_m7820,
	Path_SameRoot_m7821,
	Path_CanonicalizePath_m7822,
	PathTooLongException__ctor_m7823,
	PathTooLongException__ctor_m7824,
	PathTooLongException__ctor_m7825,
	SearchPattern__cctor_m7826,
	Stream__ctor_m4747,
	Stream__cctor_m7827,
	Stream_Dispose_m7828,
	Stream_Dispose_m4750,
	Stream_Close_m4749,
	Stream_ReadByte_m7829,
	Stream_WriteByte_m7830,
	Stream_BeginRead_m7831,
	Stream_BeginWrite_m7832,
	Stream_EndRead_m7833,
	Stream_EndWrite_m7834,
	NullStream__ctor_m7835,
	NullStream_get_CanRead_m7836,
	NullStream_get_CanSeek_m7837,
	NullStream_get_CanWrite_m7838,
	NullStream_get_Length_m7839,
	NullStream_get_Position_m7840,
	NullStream_set_Position_m7841,
	NullStream_Flush_m7842,
	NullStream_Read_m7843,
	NullStream_ReadByte_m7844,
	NullStream_Seek_m7845,
	NullStream_SetLength_m7846,
	NullStream_Write_m7847,
	NullStream_WriteByte_m7848,
	StreamAsyncResult__ctor_m7849,
	StreamAsyncResult_SetComplete_m7850,
	StreamAsyncResult_SetComplete_m7851,
	StreamAsyncResult_get_AsyncState_m7852,
	StreamAsyncResult_get_AsyncWaitHandle_m7853,
	StreamAsyncResult_get_IsCompleted_m7854,
	StreamAsyncResult_get_Exception_m7855,
	StreamAsyncResult_get_NBytes_m7856,
	StreamAsyncResult_get_Done_m7857,
	StreamAsyncResult_set_Done_m7858,
	NullStreamReader__ctor_m7859,
	NullStreamReader_Peek_m7860,
	NullStreamReader_Read_m7861,
	NullStreamReader_Read_m7862,
	NullStreamReader_ReadLine_m7863,
	NullStreamReader_ReadToEnd_m7864,
	StreamReader__ctor_m7865,
	StreamReader__ctor_m7866,
	StreamReader__ctor_m7867,
	StreamReader__ctor_m7868,
	StreamReader__ctor_m7869,
	StreamReader__cctor_m7870,
	StreamReader_Initialize_m7871,
	StreamReader_Dispose_m7872,
	StreamReader_DoChecks_m7873,
	StreamReader_ReadBuffer_m7874,
	StreamReader_Peek_m7875,
	StreamReader_Read_m7876,
	StreamReader_Read_m7877,
	StreamReader_FindNextEOL_m7878,
	StreamReader_ReadLine_m7879,
	StreamReader_ReadToEnd_m7880,
	StreamWriter__ctor_m7881,
	StreamWriter__ctor_m7882,
	StreamWriter__cctor_m7883,
	StreamWriter_Initialize_m7884,
	StreamWriter_set_AutoFlush_m7885,
	StreamWriter_Dispose_m7886,
	StreamWriter_Flush_m7887,
	StreamWriter_FlushBytes_m7888,
	StreamWriter_Decode_m7889,
	StreamWriter_Write_m7890,
	StreamWriter_LowLevelWrite_m7891,
	StreamWriter_LowLevelWrite_m7892,
	StreamWriter_Write_m7893,
	StreamWriter_Write_m7894,
	StreamWriter_Write_m7895,
	StreamWriter_Close_m7896,
	StreamWriter_Finalize_m7897,
	StringReader__ctor_m7898,
	StringReader_Dispose_m7899,
	StringReader_Peek_m7900,
	StringReader_Read_m7901,
	StringReader_Read_m7902,
	StringReader_ReadLine_m7903,
	StringReader_ReadToEnd_m7904,
	StringReader_CheckObjectDisposedException_m7905,
	NullTextReader__ctor_m7906,
	NullTextReader_ReadLine_m7907,
	TextReader__ctor_m7908,
	TextReader__cctor_m7909,
	TextReader_Dispose_m7910,
	TextReader_Dispose_m7911,
	TextReader_Peek_m7912,
	TextReader_Read_m7913,
	TextReader_Read_m7914,
	TextReader_ReadLine_m7915,
	TextReader_ReadToEnd_m7916,
	TextReader_Synchronized_m7917,
	SynchronizedReader__ctor_m7918,
	SynchronizedReader_Peek_m7919,
	SynchronizedReader_ReadLine_m7920,
	SynchronizedReader_ReadToEnd_m7921,
	SynchronizedReader_Read_m7922,
	SynchronizedReader_Read_m7923,
	NullTextWriter__ctor_m7924,
	NullTextWriter_Write_m7925,
	NullTextWriter_Write_m7926,
	NullTextWriter_Write_m7927,
	TextWriter__ctor_m7928,
	TextWriter__cctor_m7929,
	TextWriter_Close_m7930,
	TextWriter_Dispose_m7931,
	TextWriter_Dispose_m7932,
	TextWriter_Flush_m7933,
	TextWriter_Synchronized_m7934,
	TextWriter_Write_m7935,
	TextWriter_Write_m7936,
	TextWriter_Write_m7937,
	TextWriter_Write_m7938,
	TextWriter_WriteLine_m7939,
	TextWriter_WriteLine_m7940,
	SynchronizedWriter__ctor_m7941,
	SynchronizedWriter_Close_m7942,
	SynchronizedWriter_Flush_m7943,
	SynchronizedWriter_Write_m7944,
	SynchronizedWriter_Write_m7945,
	SynchronizedWriter_Write_m7946,
	SynchronizedWriter_Write_m7947,
	SynchronizedWriter_WriteLine_m7948,
	SynchronizedWriter_WriteLine_m7949,
	UnexceptionalStreamReader__ctor_m7950,
	UnexceptionalStreamReader__cctor_m7951,
	UnexceptionalStreamReader_Peek_m7952,
	UnexceptionalStreamReader_Read_m7953,
	UnexceptionalStreamReader_Read_m7954,
	UnexceptionalStreamReader_CheckEOL_m7955,
	UnexceptionalStreamReader_ReadLine_m7956,
	UnexceptionalStreamReader_ReadToEnd_m7957,
	UnexceptionalStreamWriter__ctor_m7958,
	UnexceptionalStreamWriter_Flush_m7959,
	UnexceptionalStreamWriter_Write_m7960,
	UnexceptionalStreamWriter_Write_m7961,
	UnexceptionalStreamWriter_Write_m7962,
	UnexceptionalStreamWriter_Write_m7963,
	UnmanagedMemoryStream_get_CanRead_m7964,
	UnmanagedMemoryStream_get_CanSeek_m7965,
	UnmanagedMemoryStream_get_CanWrite_m7966,
	UnmanagedMemoryStream_get_Length_m7967,
	UnmanagedMemoryStream_get_Position_m7968,
	UnmanagedMemoryStream_set_Position_m7969,
	UnmanagedMemoryStream_Read_m7970,
	UnmanagedMemoryStream_ReadByte_m7971,
	UnmanagedMemoryStream_Seek_m7972,
	UnmanagedMemoryStream_SetLength_m7973,
	UnmanagedMemoryStream_Flush_m7974,
	UnmanagedMemoryStream_Dispose_m7975,
	UnmanagedMemoryStream_Write_m7976,
	UnmanagedMemoryStream_WriteByte_m7977,
	AssemblyBuilder_get_Location_m7978,
	AssemblyBuilder_GetModulesInternal_m7979,
	AssemblyBuilder_GetTypes_m7980,
	AssemblyBuilder_get_IsCompilerContext_m7981,
	AssemblyBuilder_not_supported_m7982,
	AssemblyBuilder_UnprotectedGetName_m7983,
	ConstructorBuilder__ctor_m7984,
	ConstructorBuilder_get_CallingConvention_m7985,
	ConstructorBuilder_get_TypeBuilder_m7986,
	ConstructorBuilder_GetParameters_m7987,
	ConstructorBuilder_GetParametersInternal_m7988,
	ConstructorBuilder_GetParameterCount_m7989,
	ConstructorBuilder_Invoke_m7990,
	ConstructorBuilder_Invoke_m7991,
	ConstructorBuilder_get_MethodHandle_m7992,
	ConstructorBuilder_get_Attributes_m7993,
	ConstructorBuilder_get_ReflectedType_m7994,
	ConstructorBuilder_get_DeclaringType_m7995,
	ConstructorBuilder_get_Name_m7996,
	ConstructorBuilder_IsDefined_m7997,
	ConstructorBuilder_GetCustomAttributes_m7998,
	ConstructorBuilder_GetCustomAttributes_m7999,
	ConstructorBuilder_GetILGenerator_m8000,
	ConstructorBuilder_GetILGenerator_m8001,
	ConstructorBuilder_GetToken_m8002,
	ConstructorBuilder_get_Module_m8003,
	ConstructorBuilder_ToString_m8004,
	ConstructorBuilder_fixup_m8005,
	ConstructorBuilder_get_next_table_index_m8006,
	ConstructorBuilder_get_IsCompilerContext_m8007,
	ConstructorBuilder_not_supported_m8008,
	ConstructorBuilder_not_created_m8009,
	EnumBuilder_get_Assembly_m8010,
	EnumBuilder_get_AssemblyQualifiedName_m8011,
	EnumBuilder_get_BaseType_m8012,
	EnumBuilder_get_DeclaringType_m8013,
	EnumBuilder_get_FullName_m8014,
	EnumBuilder_get_Module_m8015,
	EnumBuilder_get_Name_m8016,
	EnumBuilder_get_Namespace_m8017,
	EnumBuilder_get_ReflectedType_m8018,
	EnumBuilder_get_TypeHandle_m8019,
	EnumBuilder_get_UnderlyingSystemType_m8020,
	EnumBuilder_GetAttributeFlagsImpl_m8021,
	EnumBuilder_GetConstructorImpl_m8022,
	EnumBuilder_GetConstructors_m8023,
	EnumBuilder_GetCustomAttributes_m8024,
	EnumBuilder_GetCustomAttributes_m8025,
	EnumBuilder_GetElementType_m8026,
	EnumBuilder_GetEvent_m8027,
	EnumBuilder_GetField_m8028,
	EnumBuilder_GetFields_m8029,
	EnumBuilder_GetInterfaces_m8030,
	EnumBuilder_GetMethodImpl_m8031,
	EnumBuilder_GetMethods_m8032,
	EnumBuilder_GetPropertyImpl_m8033,
	EnumBuilder_HasElementTypeImpl_m8034,
	EnumBuilder_InvokeMember_m8035,
	EnumBuilder_IsArrayImpl_m8036,
	EnumBuilder_IsByRefImpl_m8037,
	EnumBuilder_IsPointerImpl_m8038,
	EnumBuilder_IsPrimitiveImpl_m8039,
	EnumBuilder_IsValueTypeImpl_m8040,
	EnumBuilder_IsDefined_m8041,
	EnumBuilder_CreateNotSupportedException_m8042,
	FieldBuilder_get_Attributes_m8043,
	FieldBuilder_get_DeclaringType_m8044,
	FieldBuilder_get_FieldHandle_m8045,
	FieldBuilder_get_FieldType_m8046,
	FieldBuilder_get_Name_m8047,
	FieldBuilder_get_ReflectedType_m8048,
	FieldBuilder_GetCustomAttributes_m8049,
	FieldBuilder_GetCustomAttributes_m8050,
	FieldBuilder_GetValue_m8051,
	FieldBuilder_IsDefined_m8052,
	FieldBuilder_GetFieldOffset_m8053,
	FieldBuilder_SetValue_m8054,
	FieldBuilder_get_UMarshal_m8055,
	FieldBuilder_CreateNotSupportedException_m8056,
	FieldBuilder_get_Module_m8057,
	GenericTypeParameterBuilder_IsSubclassOf_m8058,
	GenericTypeParameterBuilder_GetAttributeFlagsImpl_m8059,
	GenericTypeParameterBuilder_GetConstructorImpl_m8060,
	GenericTypeParameterBuilder_GetConstructors_m8061,
	GenericTypeParameterBuilder_GetEvent_m8062,
	GenericTypeParameterBuilder_GetField_m8063,
	GenericTypeParameterBuilder_GetFields_m8064,
	GenericTypeParameterBuilder_GetInterfaces_m8065,
	GenericTypeParameterBuilder_GetMethods_m8066,
	GenericTypeParameterBuilder_GetMethodImpl_m8067,
	GenericTypeParameterBuilder_GetPropertyImpl_m8068,
	GenericTypeParameterBuilder_HasElementTypeImpl_m8069,
	GenericTypeParameterBuilder_IsAssignableFrom_m8070,
	GenericTypeParameterBuilder_IsInstanceOfType_m8071,
	GenericTypeParameterBuilder_IsArrayImpl_m8072,
	GenericTypeParameterBuilder_IsByRefImpl_m8073,
	GenericTypeParameterBuilder_IsPointerImpl_m8074,
	GenericTypeParameterBuilder_IsPrimitiveImpl_m8075,
	GenericTypeParameterBuilder_IsValueTypeImpl_m8076,
	GenericTypeParameterBuilder_InvokeMember_m8077,
	GenericTypeParameterBuilder_GetElementType_m8078,
	GenericTypeParameterBuilder_get_UnderlyingSystemType_m8079,
	GenericTypeParameterBuilder_get_Assembly_m8080,
	GenericTypeParameterBuilder_get_AssemblyQualifiedName_m8081,
	GenericTypeParameterBuilder_get_BaseType_m8082,
	GenericTypeParameterBuilder_get_FullName_m8083,
	GenericTypeParameterBuilder_IsDefined_m8084,
	GenericTypeParameterBuilder_GetCustomAttributes_m8085,
	GenericTypeParameterBuilder_GetCustomAttributes_m8086,
	GenericTypeParameterBuilder_get_Name_m8087,
	GenericTypeParameterBuilder_get_Namespace_m8088,
	GenericTypeParameterBuilder_get_Module_m8089,
	GenericTypeParameterBuilder_get_DeclaringType_m8090,
	GenericTypeParameterBuilder_get_ReflectedType_m8091,
	GenericTypeParameterBuilder_get_TypeHandle_m8092,
	GenericTypeParameterBuilder_GetGenericArguments_m8093,
	GenericTypeParameterBuilder_GetGenericTypeDefinition_m8094,
	GenericTypeParameterBuilder_get_ContainsGenericParameters_m8095,
	GenericTypeParameterBuilder_get_IsGenericParameter_m8096,
	GenericTypeParameterBuilder_get_IsGenericType_m8097,
	GenericTypeParameterBuilder_get_IsGenericTypeDefinition_m8098,
	GenericTypeParameterBuilder_not_supported_m8099,
	GenericTypeParameterBuilder_ToString_m8100,
	GenericTypeParameterBuilder_Equals_m8101,
	GenericTypeParameterBuilder_GetHashCode_m8102,
	GenericTypeParameterBuilder_MakeGenericType_m8103,
	ILGenerator__ctor_m8104,
	ILGenerator__cctor_m8105,
	ILGenerator_add_token_fixup_m8106,
	ILGenerator_make_room_m8107,
	ILGenerator_emit_int_m8108,
	ILGenerator_ll_emit_m8109,
	ILGenerator_Emit_m8110,
	ILGenerator_Emit_m8111,
	ILGenerator_label_fixup_m8112,
	ILGenerator_Mono_GetCurrentOffset_m8113,
	MethodBuilder_get_ContainsGenericParameters_m8114,
	MethodBuilder_get_MethodHandle_m8115,
	MethodBuilder_get_ReturnType_m8116,
	MethodBuilder_get_ReflectedType_m8117,
	MethodBuilder_get_DeclaringType_m8118,
	MethodBuilder_get_Name_m8119,
	MethodBuilder_get_Attributes_m8120,
	MethodBuilder_get_CallingConvention_m8121,
	MethodBuilder_GetBaseDefinition_m8122,
	MethodBuilder_GetParameters_m8123,
	MethodBuilder_GetParameterCount_m8124,
	MethodBuilder_Invoke_m8125,
	MethodBuilder_IsDefined_m8126,
	MethodBuilder_GetCustomAttributes_m8127,
	MethodBuilder_GetCustomAttributes_m8128,
	MethodBuilder_check_override_m8129,
	MethodBuilder_fixup_m8130,
	MethodBuilder_ToString_m8131,
	MethodBuilder_Equals_m8132,
	MethodBuilder_GetHashCode_m8133,
	MethodBuilder_get_next_table_index_m8134,
	MethodBuilder_NotSupported_m8135,
	MethodBuilder_MakeGenericMethod_m8136,
	MethodBuilder_get_IsGenericMethodDefinition_m8137,
	MethodBuilder_get_IsGenericMethod_m8138,
	MethodBuilder_GetGenericArguments_m8139,
	MethodBuilder_get_Module_m8140,
	MethodToken__ctor_m8141,
	MethodToken__cctor_m8142,
	MethodToken_Equals_m8143,
	MethodToken_GetHashCode_m8144,
	MethodToken_get_Token_m8145,
	ModuleBuilder__cctor_m8146,
	ModuleBuilder_get_next_table_index_m8147,
	ModuleBuilder_GetTypes_m8148,
	ModuleBuilder_getToken_m8149,
	ModuleBuilder_GetToken_m8150,
	ModuleBuilder_RegisterToken_m8151,
	ModuleBuilder_GetTokenGenerator_m8152,
	ModuleBuilderTokenGenerator__ctor_m8153,
	ModuleBuilderTokenGenerator_GetToken_m8154,
	OpCode__ctor_m8155,
	OpCode_GetHashCode_m8156,
	OpCode_Equals_m8157,
	OpCode_ToString_m8158,
	OpCode_get_Name_m8159,
	OpCode_get_Size_m8160,
	OpCode_get_StackBehaviourPop_m8161,
	OpCode_get_StackBehaviourPush_m8162,
	OpCodeNames__cctor_m8163,
	OpCodes__cctor_m8164,
	ParameterBuilder_get_Attributes_m8165,
	ParameterBuilder_get_Name_m8166,
	ParameterBuilder_get_Position_m8167,
	TypeBuilder_GetAttributeFlagsImpl_m8168,
	TypeBuilder_setup_internal_class_m8169,
	TypeBuilder_create_generic_class_m8170,
	TypeBuilder_get_Assembly_m8171,
	TypeBuilder_get_AssemblyQualifiedName_m8172,
	TypeBuilder_get_BaseType_m8173,
	TypeBuilder_get_DeclaringType_m8174,
	TypeBuilder_get_UnderlyingSystemType_m8175,
	TypeBuilder_get_FullName_m8176,
	TypeBuilder_get_Module_m8177,
	TypeBuilder_get_Name_m8178,
	TypeBuilder_get_Namespace_m8179,
	TypeBuilder_get_ReflectedType_m8180,
	TypeBuilder_GetConstructorImpl_m8181,
	TypeBuilder_IsDefined_m8182,
	TypeBuilder_GetCustomAttributes_m8183,
	TypeBuilder_GetCustomAttributes_m8184,
	TypeBuilder_DefineConstructor_m8185,
	TypeBuilder_DefineConstructor_m8186,
	TypeBuilder_DefineDefaultConstructor_m8187,
	TypeBuilder_create_runtime_class_m8188,
	TypeBuilder_is_nested_in_m8189,
	TypeBuilder_has_ctor_method_m8190,
	TypeBuilder_CreateType_m8191,
	TypeBuilder_GetConstructors_m8192,
	TypeBuilder_GetConstructorsInternal_m8193,
	TypeBuilder_GetElementType_m8194,
	TypeBuilder_GetEvent_m8195,
	TypeBuilder_GetField_m8196,
	TypeBuilder_GetFields_m8197,
	TypeBuilder_GetInterfaces_m8198,
	TypeBuilder_GetMethodsByName_m8199,
	TypeBuilder_GetMethods_m8200,
	TypeBuilder_GetMethodImpl_m8201,
	TypeBuilder_GetPropertyImpl_m8202,
	TypeBuilder_HasElementTypeImpl_m8203,
	TypeBuilder_InvokeMember_m8204,
	TypeBuilder_IsArrayImpl_m8205,
	TypeBuilder_IsByRefImpl_m8206,
	TypeBuilder_IsPointerImpl_m8207,
	TypeBuilder_IsPrimitiveImpl_m8208,
	TypeBuilder_IsValueTypeImpl_m8209,
	TypeBuilder_MakeGenericType_m8210,
	TypeBuilder_get_TypeHandle_m8211,
	TypeBuilder_SetParent_m8212,
	TypeBuilder_get_next_table_index_m8213,
	TypeBuilder_get_IsCompilerContext_m8214,
	TypeBuilder_get_is_created_m8215,
	TypeBuilder_not_supported_m8216,
	TypeBuilder_check_not_created_m8217,
	TypeBuilder_check_created_m8218,
	TypeBuilder_ToString_m8219,
	TypeBuilder_IsAssignableFrom_m8220,
	TypeBuilder_IsSubclassOf_m8221,
	TypeBuilder_IsAssignableTo_m8222,
	TypeBuilder_GetGenericArguments_m8223,
	TypeBuilder_GetGenericTypeDefinition_m8224,
	TypeBuilder_get_ContainsGenericParameters_m8225,
	TypeBuilder_get_IsGenericParameter_m8226,
	TypeBuilder_get_IsGenericTypeDefinition_m8227,
	TypeBuilder_get_IsGenericType_m8228,
	UnmanagedMarshal_ToMarshalAsAttribute_m8229,
	AmbiguousMatchException__ctor_m8230,
	AmbiguousMatchException__ctor_m8231,
	AmbiguousMatchException__ctor_m8232,
	ResolveEventHolder__ctor_m8233,
	Assembly__ctor_m8234,
	Assembly_get_code_base_m8235,
	Assembly_get_fullname_m8236,
	Assembly_get_location_m8237,
	Assembly_GetCodeBase_m8238,
	Assembly_get_FullName_m8239,
	Assembly_get_Location_m8240,
	Assembly_IsDefined_m8241,
	Assembly_GetCustomAttributes_m8242,
	Assembly_GetManifestResourceInternal_m8243,
	Assembly_GetTypes_m8244,
	Assembly_GetTypes_m8245,
	Assembly_GetType_m8246,
	Assembly_GetType_m8247,
	Assembly_InternalGetType_m8248,
	Assembly_GetType_m8249,
	Assembly_FillName_m8250,
	Assembly_GetName_m8251,
	Assembly_GetName_m8252,
	Assembly_UnprotectedGetName_m8253,
	Assembly_ToString_m8254,
	Assembly_Load_m8255,
	Assembly_GetModule_m8256,
	Assembly_GetModulesInternal_m8257,
	Assembly_GetModules_m8258,
	Assembly_GetExecutingAssembly_m8259,
	AssemblyCompanyAttribute__ctor_m8260,
	AssemblyConfigurationAttribute__ctor_m8261,
	AssemblyCopyrightAttribute__ctor_m8262,
	AssemblyDefaultAliasAttribute__ctor_m8263,
	AssemblyDelaySignAttribute__ctor_m8264,
	AssemblyDescriptionAttribute__ctor_m8265,
	AssemblyFileVersionAttribute__ctor_m8266,
	AssemblyInformationalVersionAttribute__ctor_m8267,
	AssemblyKeyFileAttribute__ctor_m8268,
	AssemblyName__ctor_m8269,
	AssemblyName__ctor_m8270,
	AssemblyName_get_Name_m8271,
	AssemblyName_get_Flags_m8272,
	AssemblyName_get_FullName_m8273,
	AssemblyName_get_Version_m8274,
	AssemblyName_set_Version_m8275,
	AssemblyName_ToString_m8276,
	AssemblyName_get_IsPublicKeyValid_m8277,
	AssemblyName_InternalGetPublicKeyToken_m8278,
	AssemblyName_ComputePublicKeyToken_m8279,
	AssemblyName_SetPublicKey_m8280,
	AssemblyName_SetPublicKeyToken_m8281,
	AssemblyName_GetObjectData_m8282,
	AssemblyName_Clone_m8283,
	AssemblyName_OnDeserialization_m8284,
	AssemblyProductAttribute__ctor_m8285,
	AssemblyTitleAttribute__ctor_m8286,
	AssemblyTrademarkAttribute__ctor_m8287,
	Default__ctor_m8288,
	Default_BindToMethod_m8289,
	Default_ReorderParameters_m8290,
	Default_IsArrayAssignable_m8291,
	Default_ChangeType_m8292,
	Default_ReorderArgumentArray_m8293,
	Default_check_type_m8294,
	Default_check_arguments_m8295,
	Default_SelectMethod_m8296,
	Default_SelectMethod_m8297,
	Default_GetBetterMethod_m8298,
	Default_CompareCloserType_m8299,
	Default_SelectProperty_m8300,
	Default_check_arguments_with_score_m8301,
	Default_check_type_with_score_m8302,
	Binder__ctor_m8303,
	Binder__cctor_m8304,
	Binder_get_DefaultBinder_m8305,
	Binder_ConvertArgs_m8306,
	Binder_GetDerivedLevel_m8307,
	Binder_FindMostDerivedMatch_m8308,
	ConstructorInfo__ctor_m8309,
	ConstructorInfo__cctor_m8310,
	ConstructorInfo_get_MemberType_m8311,
	ConstructorInfo_Invoke_m3663,
	CustomAttributeData__ctor_m8312,
	CustomAttributeData_get_Constructor_m8313,
	CustomAttributeData_get_ConstructorArguments_m8314,
	CustomAttributeData_get_NamedArguments_m8315,
	CustomAttributeData_GetCustomAttributes_m8316,
	CustomAttributeData_GetCustomAttributes_m8317,
	CustomAttributeData_GetCustomAttributes_m8318,
	CustomAttributeData_GetCustomAttributes_m8319,
	CustomAttributeData_ToString_m8320,
	CustomAttributeData_Equals_m8321,
	CustomAttributeData_GetHashCode_m8322,
	CustomAttributeNamedArgument_ToString_m8323,
	CustomAttributeNamedArgument_Equals_m8324,
	CustomAttributeNamedArgument_GetHashCode_m8325,
	CustomAttributeTypedArgument_ToString_m8326,
	CustomAttributeTypedArgument_Equals_m8327,
	CustomAttributeTypedArgument_GetHashCode_m8328,
	AddEventAdapter__ctor_m8329,
	AddEventAdapter_Invoke_m8330,
	AddEventAdapter_BeginInvoke_m8331,
	AddEventAdapter_EndInvoke_m8332,
	EventInfo__ctor_m8333,
	EventInfo_get_EventHandlerType_m8334,
	EventInfo_get_MemberType_m8335,
	FieldInfo__ctor_m8336,
	FieldInfo_get_MemberType_m8337,
	FieldInfo_get_IsLiteral_m8338,
	FieldInfo_get_IsStatic_m8339,
	FieldInfo_get_IsNotSerialized_m8340,
	FieldInfo_SetValue_m8341,
	FieldInfo_internal_from_handle_type_m8342,
	FieldInfo_GetFieldFromHandle_m8343,
	FieldInfo_GetFieldOffset_m8344,
	FieldInfo_GetUnmanagedMarshal_m8345,
	FieldInfo_get_UMarshal_m8346,
	FieldInfo_GetPseudoCustomAttributes_m8347,
	MemberInfoSerializationHolder__ctor_m8348,
	MemberInfoSerializationHolder_Serialize_m8349,
	MemberInfoSerializationHolder_Serialize_m8350,
	MemberInfoSerializationHolder_GetObjectData_m8351,
	MemberInfoSerializationHolder_GetRealObject_m8352,
	MethodBase__ctor_m8353,
	MethodBase_GetMethodFromHandleNoGenericCheck_m8354,
	MethodBase_GetMethodFromIntPtr_m8355,
	MethodBase_GetMethodFromHandle_m8356,
	MethodBase_GetMethodFromHandleInternalType_m8357,
	MethodBase_GetParameterCount_m8358,
	MethodBase_Invoke_m8359,
	MethodBase_get_CallingConvention_m8360,
	MethodBase_get_IsPublic_m8361,
	MethodBase_get_IsStatic_m8362,
	MethodBase_get_IsVirtual_m8363,
	MethodBase_get_IsAbstract_m8364,
	MethodBase_get_next_table_index_m8365,
	MethodBase_GetGenericArguments_m8366,
	MethodBase_get_ContainsGenericParameters_m8367,
	MethodBase_get_IsGenericMethodDefinition_m8368,
	MethodBase_get_IsGenericMethod_m8369,
	MethodInfo__ctor_m8370,
	MethodInfo_get_MemberType_m8371,
	MethodInfo_get_ReturnType_m8372,
	MethodInfo_MakeGenericMethod_m8373,
	MethodInfo_GetGenericArguments_m8374,
	MethodInfo_get_IsGenericMethod_m8375,
	MethodInfo_get_IsGenericMethodDefinition_m8376,
	MethodInfo_get_ContainsGenericParameters_m8377,
	Missing__ctor_m8378,
	Missing__cctor_m8379,
	Missing_System_Runtime_Serialization_ISerializable_GetObjectData_m8380,
	Module__ctor_m8381,
	Module__cctor_m8382,
	Module_get_Assembly_m8383,
	Module_get_ScopeName_m8384,
	Module_GetCustomAttributes_m8385,
	Module_GetObjectData_m8386,
	Module_InternalGetTypes_m8387,
	Module_GetTypes_m8388,
	Module_IsDefined_m8389,
	Module_IsResource_m8390,
	Module_ToString_m8391,
	Module_filter_by_type_name_m8392,
	Module_filter_by_type_name_ignore_case_m8393,
	MonoEventInfo_get_event_info_m8394,
	MonoEventInfo_GetEventInfo_m8395,
	MonoEvent__ctor_m8396,
	MonoEvent_get_Attributes_m8397,
	MonoEvent_GetAddMethod_m8398,
	MonoEvent_get_DeclaringType_m8399,
	MonoEvent_get_ReflectedType_m8400,
	MonoEvent_get_Name_m8401,
	MonoEvent_ToString_m8402,
	MonoEvent_IsDefined_m8403,
	MonoEvent_GetCustomAttributes_m8404,
	MonoEvent_GetCustomAttributes_m8405,
	MonoEvent_GetObjectData_m8406,
	MonoField__ctor_m8407,
	MonoField_get_Attributes_m8408,
	MonoField_get_FieldHandle_m8409,
	MonoField_get_FieldType_m8410,
	MonoField_GetParentType_m8411,
	MonoField_get_ReflectedType_m8412,
	MonoField_get_DeclaringType_m8413,
	MonoField_get_Name_m8414,
	MonoField_IsDefined_m8415,
	MonoField_GetCustomAttributes_m8416,
	MonoField_GetCustomAttributes_m8417,
	MonoField_GetFieldOffset_m8418,
	MonoField_GetValueInternal_m8419,
	MonoField_GetValue_m8420,
	MonoField_ToString_m8421,
	MonoField_SetValueInternal_m8422,
	MonoField_SetValue_m8423,
	MonoField_GetObjectData_m8424,
	MonoField_CheckGeneric_m8425,
	MonoGenericMethod__ctor_m8426,
	MonoGenericMethod_get_ReflectedType_m8427,
	MonoGenericCMethod__ctor_m8428,
	MonoGenericCMethod_get_ReflectedType_m8429,
	MonoMethodInfo_get_method_info_m8430,
	MonoMethodInfo_GetMethodInfo_m8431,
	MonoMethodInfo_GetDeclaringType_m8432,
	MonoMethodInfo_GetReturnType_m8433,
	MonoMethodInfo_GetAttributes_m8434,
	MonoMethodInfo_GetCallingConvention_m8435,
	MonoMethodInfo_get_parameter_info_m8436,
	MonoMethodInfo_GetParametersInfo_m8437,
	MonoMethod__ctor_m8438,
	MonoMethod_get_name_m8439,
	MonoMethod_get_base_definition_m8440,
	MonoMethod_GetBaseDefinition_m8441,
	MonoMethod_get_ReturnType_m8442,
	MonoMethod_GetParameters_m8443,
	MonoMethod_InternalInvoke_m8444,
	MonoMethod_Invoke_m8445,
	MonoMethod_get_MethodHandle_m8446,
	MonoMethod_get_Attributes_m8447,
	MonoMethod_get_CallingConvention_m8448,
	MonoMethod_get_ReflectedType_m8449,
	MonoMethod_get_DeclaringType_m8450,
	MonoMethod_get_Name_m8451,
	MonoMethod_IsDefined_m8452,
	MonoMethod_GetCustomAttributes_m8453,
	MonoMethod_GetCustomAttributes_m8454,
	MonoMethod_GetDllImportAttribute_m8455,
	MonoMethod_GetPseudoCustomAttributes_m8456,
	MonoMethod_ShouldPrintFullName_m8457,
	MonoMethod_ToString_m8458,
	MonoMethod_GetObjectData_m8459,
	MonoMethod_MakeGenericMethod_m8460,
	MonoMethod_MakeGenericMethod_impl_m8461,
	MonoMethod_GetGenericArguments_m8462,
	MonoMethod_get_IsGenericMethodDefinition_m8463,
	MonoMethod_get_IsGenericMethod_m8464,
	MonoMethod_get_ContainsGenericParameters_m8465,
	MonoCMethod__ctor_m8466,
	MonoCMethod_GetParameters_m8467,
	MonoCMethod_InternalInvoke_m8468,
	MonoCMethod_Invoke_m8469,
	MonoCMethod_Invoke_m8470,
	MonoCMethod_get_MethodHandle_m8471,
	MonoCMethod_get_Attributes_m8472,
	MonoCMethod_get_CallingConvention_m8473,
	MonoCMethod_get_ReflectedType_m8474,
	MonoCMethod_get_DeclaringType_m8475,
	MonoCMethod_get_Name_m8476,
	MonoCMethod_IsDefined_m8477,
	MonoCMethod_GetCustomAttributes_m8478,
	MonoCMethod_GetCustomAttributes_m8479,
	MonoCMethod_ToString_m8480,
	MonoCMethod_GetObjectData_m8481,
	MonoPropertyInfo_get_property_info_m8482,
	MonoPropertyInfo_GetTypeModifiers_m8483,
	GetterAdapter__ctor_m8484,
	GetterAdapter_Invoke_m8485,
	GetterAdapter_BeginInvoke_m8486,
	GetterAdapter_EndInvoke_m8487,
	MonoProperty__ctor_m8488,
	MonoProperty_CachePropertyInfo_m8489,
	MonoProperty_get_Attributes_m8490,
	MonoProperty_get_CanRead_m8491,
	MonoProperty_get_CanWrite_m8492,
	MonoProperty_get_PropertyType_m8493,
	MonoProperty_get_ReflectedType_m8494,
	MonoProperty_get_DeclaringType_m8495,
	MonoProperty_get_Name_m8496,
	MonoProperty_GetAccessors_m8497,
	MonoProperty_GetGetMethod_m8498,
	MonoProperty_GetIndexParameters_m8499,
	MonoProperty_GetSetMethod_m8500,
	MonoProperty_IsDefined_m8501,
	MonoProperty_GetCustomAttributes_m8502,
	MonoProperty_GetCustomAttributes_m8503,
	MonoProperty_CreateGetterDelegate_m8504,
	MonoProperty_GetValue_m8505,
	MonoProperty_GetValue_m8506,
	MonoProperty_SetValue_m8507,
	MonoProperty_ToString_m8508,
	MonoProperty_GetOptionalCustomModifiers_m8509,
	MonoProperty_GetRequiredCustomModifiers_m8510,
	MonoProperty_GetObjectData_m8511,
	ParameterInfo__ctor_m8512,
	ParameterInfo__ctor_m8513,
	ParameterInfo__ctor_m8514,
	ParameterInfo_ToString_m8515,
	ParameterInfo_get_ParameterType_m8516,
	ParameterInfo_get_Attributes_m8517,
	ParameterInfo_get_IsIn_m8518,
	ParameterInfo_get_IsOptional_m8519,
	ParameterInfo_get_IsOut_m8520,
	ParameterInfo_get_IsRetval_m8521,
	ParameterInfo_get_Member_m8522,
	ParameterInfo_get_Name_m8523,
	ParameterInfo_get_Position_m8524,
	ParameterInfo_GetCustomAttributes_m8525,
	ParameterInfo_IsDefined_m8526,
	ParameterInfo_GetPseudoCustomAttributes_m8527,
	Pointer__ctor_m8528,
	Pointer_System_Runtime_Serialization_ISerializable_GetObjectData_m8529,
	PropertyInfo__ctor_m8530,
	PropertyInfo_get_MemberType_m8531,
	PropertyInfo_GetValue_m8532,
	PropertyInfo_SetValue_m8533,
	PropertyInfo_GetOptionalCustomModifiers_m8534,
	PropertyInfo_GetRequiredCustomModifiers_m8535,
	StrongNameKeyPair__ctor_m8536,
	StrongNameKeyPair_System_Runtime_Serialization_ISerializable_GetObjectData_m8537,
	StrongNameKeyPair_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m8538,
	TargetException__ctor_m8539,
	TargetException__ctor_m8540,
	TargetException__ctor_m8541,
	TargetInvocationException__ctor_m8542,
	TargetInvocationException__ctor_m8543,
	TargetParameterCountException__ctor_m8544,
	TargetParameterCountException__ctor_m8545,
	TargetParameterCountException__ctor_m8546,
	NeutralResourcesLanguageAttribute__ctor_m8547,
	ResourceManager__ctor_m8548,
	ResourceManager__cctor_m8549,
	ResourceInfo__ctor_m8550,
	ResourceCacheItem__ctor_m8551,
	ResourceEnumerator__ctor_m8552,
	ResourceEnumerator_get_Entry_m8553,
	ResourceEnumerator_get_Key_m8554,
	ResourceEnumerator_get_Value_m8555,
	ResourceEnumerator_get_Current_m8556,
	ResourceEnumerator_MoveNext_m8557,
	ResourceEnumerator_Reset_m8558,
	ResourceEnumerator_FillCache_m8559,
	ResourceReader__ctor_m8560,
	ResourceReader__ctor_m8561,
	ResourceReader_System_Collections_IEnumerable_GetEnumerator_m8562,
	ResourceReader_System_IDisposable_Dispose_m8563,
	ResourceReader_ReadHeaders_m8564,
	ResourceReader_CreateResourceInfo_m8565,
	ResourceReader_Read7BitEncodedInt_m8566,
	ResourceReader_ReadValueVer2_m8567,
	ResourceReader_ReadValueVer1_m8568,
	ResourceReader_ReadNonPredefinedValue_m8569,
	ResourceReader_LoadResourceValues_m8570,
	ResourceReader_Close_m8571,
	ResourceReader_GetEnumerator_m8572,
	ResourceReader_Dispose_m8573,
	ResourceSet__ctor_m8574,
	ResourceSet__ctor_m8575,
	ResourceSet__ctor_m8576,
	ResourceSet__ctor_m8577,
	ResourceSet_System_Collections_IEnumerable_GetEnumerator_m8578,
	ResourceSet_Dispose_m8579,
	ResourceSet_Dispose_m8580,
	ResourceSet_GetEnumerator_m8581,
	ResourceSet_GetObjectInternal_m8582,
	ResourceSet_GetObject_m8583,
	ResourceSet_GetObject_m8584,
	ResourceSet_ReadResources_m8585,
	RuntimeResourceSet__ctor_m8586,
	RuntimeResourceSet__ctor_m8587,
	RuntimeResourceSet__ctor_m8588,
	RuntimeResourceSet_GetObject_m8589,
	RuntimeResourceSet_GetObject_m8590,
	RuntimeResourceSet_CloneDisposableObjectIfPossible_m8591,
	SatelliteContractVersionAttribute__ctor_m8592,
	CompilationRelaxationsAttribute__ctor_m8593,
	DefaultDependencyAttribute__ctor_m8594,
	StringFreezingAttribute__ctor_m8595,
	CriticalFinalizerObject__ctor_m8596,
	CriticalFinalizerObject_Finalize_m8597,
	ReliabilityContractAttribute__ctor_m8598,
	ClassInterfaceAttribute__ctor_m8599,
	ComDefaultInterfaceAttribute__ctor_m8600,
	DispIdAttribute__ctor_m8601,
	GCHandle__ctor_m8602,
	GCHandle_get_IsAllocated_m8603,
	GCHandle_get_Target_m8604,
	GCHandle_Alloc_m8605,
	GCHandle_Free_m8606,
	GCHandle_GetTarget_m8607,
	GCHandle_GetTargetHandle_m8608,
	GCHandle_FreeHandle_m8609,
	GCHandle_Equals_m8610,
	GCHandle_GetHashCode_m8611,
	InterfaceTypeAttribute__ctor_m8612,
	Marshal__cctor_m8613,
	Marshal_copy_from_unmanaged_m8614,
	Marshal_Copy_m8615,
	Marshal_Copy_m8616,
	Marshal_ReadByte_m8617,
	Marshal_WriteByte_m8618,
	MarshalDirectiveException__ctor_m8619,
	MarshalDirectiveException__ctor_m8620,
	PreserveSigAttribute__ctor_m8621,
	SafeHandle__ctor_m8622,
	SafeHandle_Close_m8623,
	SafeHandle_DangerousAddRef_m8624,
	SafeHandle_DangerousGetHandle_m8625,
	SafeHandle_DangerousRelease_m8626,
	SafeHandle_Dispose_m8627,
	SafeHandle_Dispose_m8628,
	SafeHandle_SetHandle_m8629,
	SafeHandle_Finalize_m8630,
	TypeLibImportClassAttribute__ctor_m8631,
	TypeLibVersionAttribute__ctor_m8632,
	ActivationServices_get_ConstructionActivator_m8633,
	ActivationServices_CreateProxyFromAttributes_m8634,
	ActivationServices_CreateConstructionCall_m8635,
	ActivationServices_AllocateUninitializedClassInstance_m8636,
	ActivationServices_EnableProxyActivation_m8637,
	AppDomainLevelActivator__ctor_m8638,
	ConstructionLevelActivator__ctor_m8639,
	ContextLevelActivator__ctor_m8640,
	UrlAttribute_get_UrlValue_m8641,
	UrlAttribute_Equals_m8642,
	UrlAttribute_GetHashCode_m8643,
	UrlAttribute_GetPropertiesForNewContext_m8644,
	UrlAttribute_IsContextOK_m8645,
	ChannelInfo__ctor_m8646,
	ChannelInfo_get_ChannelData_m8647,
	ChannelServices__cctor_m8648,
	ChannelServices_CreateClientChannelSinkChain_m8649,
	ChannelServices_CreateClientChannelSinkChain_m8650,
	ChannelServices_RegisterChannel_m8651,
	ChannelServices_RegisterChannel_m8652,
	ChannelServices_RegisterChannelConfig_m8653,
	ChannelServices_CreateProvider_m8654,
	ChannelServices_GetCurrentChannelInfo_m8655,
	CrossAppDomainData__ctor_m8656,
	CrossAppDomainData_get_DomainID_m8657,
	CrossAppDomainData_get_ProcessID_m8658,
	CrossAppDomainChannel__ctor_m8659,
	CrossAppDomainChannel__cctor_m8660,
	CrossAppDomainChannel_RegisterCrossAppDomainChannel_m8661,
	CrossAppDomainChannel_get_ChannelName_m8662,
	CrossAppDomainChannel_get_ChannelPriority_m8663,
	CrossAppDomainChannel_get_ChannelData_m8664,
	CrossAppDomainChannel_StartListening_m8665,
	CrossAppDomainChannel_CreateMessageSink_m8666,
	CrossAppDomainSink__ctor_m8667,
	CrossAppDomainSink__cctor_m8668,
	CrossAppDomainSink_GetSink_m8669,
	CrossAppDomainSink_get_TargetDomainId_m8670,
	SinkProviderData__ctor_m8671,
	SinkProviderData_get_Children_m8672,
	SinkProviderData_get_Properties_m8673,
	Context__ctor_m8674,
	Context__cctor_m8675,
	Context_Finalize_m8676,
	Context_get_DefaultContext_m8677,
	Context_get_ContextID_m8678,
	Context_get_ContextProperties_m8679,
	Context_get_IsDefaultContext_m8680,
	Context_get_NeedsContextSink_m8681,
	Context_RegisterDynamicProperty_m8682,
	Context_UnregisterDynamicProperty_m8683,
	Context_GetDynamicPropertyCollection_m8684,
	Context_NotifyGlobalDynamicSinks_m8685,
	Context_get_HasGlobalDynamicSinks_m8686,
	Context_NotifyDynamicSinks_m8687,
	Context_get_HasDynamicSinks_m8688,
	Context_get_HasExitSinks_m8689,
	Context_GetProperty_m8690,
	Context_SetProperty_m8691,
	Context_Freeze_m8692,
	Context_ToString_m8693,
	Context_GetServerContextSinkChain_m8694,
	Context_GetClientContextSinkChain_m8695,
	Context_CreateServerObjectSinkChain_m8696,
	Context_CreateEnvoySink_m8697,
	Context_SwitchToContext_m8698,
	Context_CreateNewContext_m8699,
	Context_DoCallBack_m8700,
	Context_AllocateDataSlot_m8701,
	Context_AllocateNamedDataSlot_m8702,
	Context_FreeNamedDataSlot_m8703,
	Context_GetData_m8704,
	Context_GetNamedDataSlot_m8705,
	Context_SetData_m8706,
	DynamicPropertyReg__ctor_m8707,
	DynamicPropertyCollection__ctor_m8708,
	DynamicPropertyCollection_get_HasProperties_m8709,
	DynamicPropertyCollection_RegisterDynamicProperty_m8710,
	DynamicPropertyCollection_UnregisterDynamicProperty_m8711,
	DynamicPropertyCollection_NotifyMessage_m8712,
	DynamicPropertyCollection_FindProperty_m8713,
	ContextCallbackObject__ctor_m8714,
	ContextCallbackObject_DoCallBack_m8715,
	ContextAttribute__ctor_m8716,
	ContextAttribute_get_Name_m8717,
	ContextAttribute_Equals_m8718,
	ContextAttribute_Freeze_m8719,
	ContextAttribute_GetHashCode_m8720,
	ContextAttribute_GetPropertiesForNewContext_m8721,
	ContextAttribute_IsContextOK_m8722,
	ContextAttribute_IsNewContextOK_m8723,
	CrossContextChannel__ctor_m8724,
	SynchronizationAttribute__ctor_m8725,
	SynchronizationAttribute__ctor_m8726,
	SynchronizationAttribute_set_Locked_m8727,
	SynchronizationAttribute_ReleaseLock_m8728,
	SynchronizationAttribute_GetPropertiesForNewContext_m8729,
	SynchronizationAttribute_GetClientContextSink_m8730,
	SynchronizationAttribute_GetServerContextSink_m8731,
	SynchronizationAttribute_IsContextOK_m8732,
	SynchronizationAttribute_ExitContext_m8733,
	SynchronizationAttribute_EnterContext_m8734,
	SynchronizedClientContextSink__ctor_m8735,
	SynchronizedServerContextSink__ctor_m8736,
	LeaseManager__ctor_m8737,
	LeaseManager_SetPollTime_m8738,
	LeaseSink__ctor_m8739,
	LifetimeServices__cctor_m8740,
	LifetimeServices_set_LeaseManagerPollTime_m8741,
	LifetimeServices_set_LeaseTime_m8742,
	LifetimeServices_set_RenewOnCallTime_m8743,
	LifetimeServices_set_SponsorshipTimeout_m8744,
	ArgInfo__ctor_m8745,
	ArgInfo_GetInOutArgs_m8746,
	AsyncResult__ctor_m8747,
	AsyncResult_get_AsyncState_m8748,
	AsyncResult_get_AsyncWaitHandle_m8749,
	AsyncResult_get_CompletedSynchronously_m8750,
	AsyncResult_get_IsCompleted_m8751,
	AsyncResult_get_EndInvokeCalled_m8752,
	AsyncResult_set_EndInvokeCalled_m8753,
	AsyncResult_get_AsyncDelegate_m8754,
	AsyncResult_get_NextSink_m8755,
	AsyncResult_AsyncProcessMessage_m8756,
	AsyncResult_GetReplyMessage_m8757,
	AsyncResult_SetMessageCtrl_m8758,
	AsyncResult_SetCompletedSynchronously_m8759,
	AsyncResult_EndInvoke_m8760,
	AsyncResult_SyncProcessMessage_m8761,
	AsyncResult_get_CallMessage_m8762,
	AsyncResult_set_CallMessage_m8763,
	ClientContextTerminatorSink__ctor_m8764,
	ConstructionCall__ctor_m8765,
	ConstructionCall__ctor_m8766,
	ConstructionCall_InitDictionary_m8767,
	ConstructionCall_set_IsContextOk_m8768,
	ConstructionCall_get_ActivationType_m8769,
	ConstructionCall_get_ActivationTypeName_m8770,
	ConstructionCall_get_Activator_m8771,
	ConstructionCall_set_Activator_m8772,
	ConstructionCall_get_CallSiteActivationAttributes_m8773,
	ConstructionCall_SetActivationAttributes_m8774,
	ConstructionCall_get_ContextProperties_m8775,
	ConstructionCall_InitMethodProperty_m8776,
	ConstructionCall_GetObjectData_m8777,
	ConstructionCall_get_Properties_m8778,
	ConstructionCallDictionary__ctor_m8779,
	ConstructionCallDictionary__cctor_m8780,
	ConstructionCallDictionary_GetMethodProperty_m8781,
	ConstructionCallDictionary_SetMethodProperty_m8782,
	EnvoyTerminatorSink__ctor_m8783,
	EnvoyTerminatorSink__cctor_m8784,
	Header__ctor_m8785,
	Header__ctor_m8786,
	Header__ctor_m8787,
	LogicalCallContext__ctor_m8788,
	LogicalCallContext__ctor_m8789,
	LogicalCallContext_GetObjectData_m8790,
	LogicalCallContext_SetData_m8791,
	LogicalCallContext_Clone_m8792,
	CallContextRemotingData__ctor_m8793,
	CallContextRemotingData_Clone_m8794,
	MethodCall__ctor_m8795,
	MethodCall__ctor_m8796,
	MethodCall__ctor_m8797,
	MethodCall_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8798,
	MethodCall_InitMethodProperty_m8799,
	MethodCall_GetObjectData_m8800,
	MethodCall_get_Args_m8801,
	MethodCall_get_LogicalCallContext_m8802,
	MethodCall_get_MethodBase_m8803,
	MethodCall_get_MethodName_m8804,
	MethodCall_get_MethodSignature_m8805,
	MethodCall_get_Properties_m8806,
	MethodCall_InitDictionary_m8807,
	MethodCall_get_TypeName_m8808,
	MethodCall_get_Uri_m8809,
	MethodCall_set_Uri_m8810,
	MethodCall_Init_m8811,
	MethodCall_ResolveMethod_m8812,
	MethodCall_CastTo_m8813,
	MethodCall_GetTypeNameFromAssemblyQualifiedName_m8814,
	MethodCall_get_GenericArguments_m8815,
	MethodCallDictionary__ctor_m8816,
	MethodCallDictionary__cctor_m8817,
	DictionaryEnumerator__ctor_m8818,
	DictionaryEnumerator_get_Current_m8819,
	DictionaryEnumerator_MoveNext_m8820,
	DictionaryEnumerator_Reset_m8821,
	DictionaryEnumerator_get_Entry_m8822,
	DictionaryEnumerator_get_Key_m8823,
	DictionaryEnumerator_get_Value_m8824,
	MethodDictionary__ctor_m8825,
	MethodDictionary_System_Collections_IEnumerable_GetEnumerator_m8826,
	MethodDictionary_set_MethodKeys_m8827,
	MethodDictionary_AllocInternalProperties_m8828,
	MethodDictionary_GetInternalProperties_m8829,
	MethodDictionary_IsOverridenKey_m8830,
	MethodDictionary_get_Item_m8831,
	MethodDictionary_set_Item_m8832,
	MethodDictionary_GetMethodProperty_m8833,
	MethodDictionary_SetMethodProperty_m8834,
	MethodDictionary_get_Values_m8835,
	MethodDictionary_Add_m8836,
	MethodDictionary_Contains_m8837,
	MethodDictionary_Remove_m8838,
	MethodDictionary_get_Count_m8839,
	MethodDictionary_get_IsSynchronized_m8840,
	MethodDictionary_get_SyncRoot_m8841,
	MethodDictionary_CopyTo_m8842,
	MethodDictionary_GetEnumerator_m8843,
	MethodReturnDictionary__ctor_m8844,
	MethodReturnDictionary__cctor_m8845,
	MonoMethodMessage_get_Args_m8846,
	MonoMethodMessage_get_LogicalCallContext_m8847,
	MonoMethodMessage_get_MethodBase_m8848,
	MonoMethodMessage_get_MethodName_m8849,
	MonoMethodMessage_get_MethodSignature_m8850,
	MonoMethodMessage_get_TypeName_m8851,
	MonoMethodMessage_get_Uri_m8852,
	MonoMethodMessage_set_Uri_m8853,
	MonoMethodMessage_get_Exception_m8854,
	MonoMethodMessage_get_OutArgCount_m8855,
	MonoMethodMessage_get_OutArgs_m8856,
	MonoMethodMessage_get_ReturnValue_m8857,
	RemotingSurrogate__ctor_m8858,
	RemotingSurrogate_SetObjectData_m8859,
	ObjRefSurrogate__ctor_m8860,
	ObjRefSurrogate_SetObjectData_m8861,
	RemotingSurrogateSelector__ctor_m8862,
	RemotingSurrogateSelector__cctor_m8863,
	RemotingSurrogateSelector_GetSurrogate_m8864,
	ReturnMessage__ctor_m8865,
	ReturnMessage__ctor_m8866,
	ReturnMessage_System_Runtime_Remoting_Messaging_IInternalMessage_set_Uri_m8867,
	ReturnMessage_get_Args_m8868,
	ReturnMessage_get_LogicalCallContext_m8869,
	ReturnMessage_get_MethodBase_m8870,
	ReturnMessage_get_MethodName_m8871,
	ReturnMessage_get_MethodSignature_m8872,
	ReturnMessage_get_Properties_m8873,
	ReturnMessage_get_TypeName_m8874,
	ReturnMessage_get_Uri_m8875,
	ReturnMessage_set_Uri_m8876,
	ReturnMessage_get_Exception_m8877,
	ReturnMessage_get_OutArgs_m8878,
	ReturnMessage_get_ReturnValue_m8879,
	ServerContextTerminatorSink__ctor_m8880,
	ServerObjectTerminatorSink__ctor_m8881,
	StackBuilderSink__ctor_m8882,
	SoapAttribute__ctor_m8883,
	SoapAttribute_get_UseAttribute_m8884,
	SoapAttribute_get_XmlNamespace_m8885,
	SoapAttribute_SetReflectionObject_m8886,
	SoapFieldAttribute__ctor_m8887,
	SoapFieldAttribute_get_XmlElementName_m8888,
	SoapFieldAttribute_IsInteropXmlElement_m8889,
	SoapFieldAttribute_SetReflectionObject_m8890,
	SoapMethodAttribute__ctor_m8891,
	SoapMethodAttribute_get_UseAttribute_m8892,
	SoapMethodAttribute_get_XmlNamespace_m8893,
	SoapMethodAttribute_SetReflectionObject_m8894,
	SoapParameterAttribute__ctor_m8895,
	SoapTypeAttribute__ctor_m8896,
	SoapTypeAttribute_get_UseAttribute_m8897,
	SoapTypeAttribute_get_XmlElementName_m8898,
	SoapTypeAttribute_get_XmlNamespace_m8899,
	SoapTypeAttribute_get_XmlTypeName_m8900,
	SoapTypeAttribute_get_XmlTypeNamespace_m8901,
	SoapTypeAttribute_get_IsInteropXmlElement_m8902,
	SoapTypeAttribute_get_IsInteropXmlType_m8903,
	SoapTypeAttribute_SetReflectionObject_m8904,
	ProxyAttribute_CreateInstance_m8905,
	ProxyAttribute_CreateProxy_m8906,
	ProxyAttribute_GetPropertiesForNewContext_m8907,
	ProxyAttribute_IsContextOK_m8908,
	RealProxy__ctor_m8909,
	RealProxy__ctor_m8910,
	RealProxy__ctor_m8911,
	RealProxy_InternalGetProxyType_m8912,
	RealProxy_GetProxiedType_m8913,
	RealProxy_get_ObjectIdentity_m8914,
	RealProxy_InternalGetTransparentProxy_m8915,
	RealProxy_GetTransparentProxy_m8916,
	RealProxy_SetTargetDomain_m8917,
	RemotingProxy__ctor_m8918,
	RemotingProxy__ctor_m8919,
	RemotingProxy__cctor_m8920,
	RemotingProxy_get_TypeName_m8921,
	RemotingProxy_Finalize_m8922,
	TrackingServices__cctor_m8923,
	TrackingServices_NotifyUnmarshaledObject_m8924,
	ActivatedClientTypeEntry__ctor_m8925,
	ActivatedClientTypeEntry_get_ApplicationUrl_m8926,
	ActivatedClientTypeEntry_get_ContextAttributes_m8927,
	ActivatedClientTypeEntry_get_ObjectType_m8928,
	ActivatedClientTypeEntry_ToString_m8929,
	ActivatedServiceTypeEntry__ctor_m8930,
	ActivatedServiceTypeEntry_get_ObjectType_m8931,
	ActivatedServiceTypeEntry_ToString_m8932,
	EnvoyInfo__ctor_m8933,
	EnvoyInfo_get_EnvoySinks_m8934,
	Identity__ctor_m8935,
	Identity_get_ChannelSink_m8936,
	Identity_set_ChannelSink_m8937,
	Identity_get_ObjectUri_m8938,
	Identity_get_Disposed_m8939,
	Identity_set_Disposed_m8940,
	Identity_get_ClientDynamicProperties_m8941,
	Identity_get_ServerDynamicProperties_m8942,
	ClientIdentity__ctor_m8943,
	ClientIdentity_get_ClientProxy_m8944,
	ClientIdentity_set_ClientProxy_m8945,
	ClientIdentity_CreateObjRef_m8946,
	ClientIdentity_get_TargetUri_m8947,
	InternalRemotingServices__cctor_m8948,
	InternalRemotingServices_GetCachedSoapAttribute_m8949,
	ObjRef__ctor_m8950,
	ObjRef__ctor_m8951,
	ObjRef__cctor_m8952,
	ObjRef_get_IsReferenceToWellKnow_m8953,
	ObjRef_get_ChannelInfo_m8954,
	ObjRef_get_EnvoyInfo_m8955,
	ObjRef_set_EnvoyInfo_m8956,
	ObjRef_get_TypeInfo_m8957,
	ObjRef_set_TypeInfo_m8958,
	ObjRef_get_URI_m8959,
	ObjRef_set_URI_m8960,
	ObjRef_GetObjectData_m8961,
	ObjRef_GetRealObject_m8962,
	ObjRef_UpdateChannelInfo_m8963,
	ObjRef_get_ServerType_m8964,
	RemotingConfiguration__cctor_m8965,
	RemotingConfiguration_get_ApplicationName_m8966,
	RemotingConfiguration_set_ApplicationName_m8967,
	RemotingConfiguration_get_ProcessId_m8968,
	RemotingConfiguration_LoadDefaultDelayedChannels_m8969,
	RemotingConfiguration_IsRemotelyActivatedClientType_m8970,
	RemotingConfiguration_RegisterActivatedClientType_m8971,
	RemotingConfiguration_RegisterActivatedServiceType_m8972,
	RemotingConfiguration_RegisterWellKnownClientType_m8973,
	RemotingConfiguration_RegisterWellKnownServiceType_m8974,
	RemotingConfiguration_RegisterChannelTemplate_m8975,
	RemotingConfiguration_RegisterClientProviderTemplate_m8976,
	RemotingConfiguration_RegisterServerProviderTemplate_m8977,
	RemotingConfiguration_RegisterChannels_m8978,
	RemotingConfiguration_RegisterTypes_m8979,
	RemotingConfiguration_SetCustomErrorsMode_m8980,
	ConfigHandler__ctor_m8981,
	ConfigHandler_ValidatePath_m8982,
	ConfigHandler_CheckPath_m8983,
	ConfigHandler_OnStartParsing_m8984,
	ConfigHandler_OnProcessingInstruction_m8985,
	ConfigHandler_OnIgnorableWhitespace_m8986,
	ConfigHandler_OnStartElement_m8987,
	ConfigHandler_ParseElement_m8988,
	ConfigHandler_OnEndElement_m8989,
	ConfigHandler_ReadCustomProviderData_m8990,
	ConfigHandler_ReadLifetine_m8991,
	ConfigHandler_ParseTime_m8992,
	ConfigHandler_ReadChannel_m8993,
	ConfigHandler_ReadProvider_m8994,
	ConfigHandler_ReadClientActivated_m8995,
	ConfigHandler_ReadServiceActivated_m8996,
	ConfigHandler_ReadClientWellKnown_m8997,
	ConfigHandler_ReadServiceWellKnown_m8998,
	ConfigHandler_ReadInteropXml_m8999,
	ConfigHandler_ReadPreload_m9000,
	ConfigHandler_GetNotNull_m9001,
	ConfigHandler_ExtractAssembly_m9002,
	ConfigHandler_OnChars_m9003,
	ConfigHandler_OnEndParsing_m9004,
	ChannelData__ctor_m9005,
	ChannelData_get_ServerProviders_m9006,
	ChannelData_get_ClientProviders_m9007,
	ChannelData_get_CustomProperties_m9008,
	ChannelData_CopyFrom_m9009,
	ProviderData__ctor_m9010,
	ProviderData_CopyFrom_m9011,
	FormatterData__ctor_m9012,
	RemotingException__ctor_m9013,
	RemotingException__ctor_m9014,
	RemotingException__ctor_m9015,
	RemotingException__ctor_m9016,
	RemotingServices__cctor_m9017,
	RemotingServices_GetVirtualMethod_m9018,
	RemotingServices_IsTransparentProxy_m9019,
	RemotingServices_GetServerTypeForUri_m9020,
	RemotingServices_Unmarshal_m9021,
	RemotingServices_Unmarshal_m9022,
	RemotingServices_GetRealProxy_m9023,
	RemotingServices_GetMethodBaseFromMethodMessage_m9024,
	RemotingServices_GetMethodBaseFromName_m9025,
	RemotingServices_FindInterfaceMethod_m9026,
	RemotingServices_CreateClientProxy_m9027,
	RemotingServices_CreateClientProxy_m9028,
	RemotingServices_CreateClientProxyForContextBound_m9029,
	RemotingServices_GetIdentityForUri_m9030,
	RemotingServices_RemoveAppNameFromUri_m9031,
	RemotingServices_GetOrCreateClientIdentity_m9032,
	RemotingServices_GetClientChannelSinkChain_m9033,
	RemotingServices_CreateWellKnownServerIdentity_m9034,
	RemotingServices_RegisterServerIdentity_m9035,
	RemotingServices_GetProxyForRemoteObject_m9036,
	RemotingServices_GetRemoteObject_m9037,
	RemotingServices_RegisterInternalChannels_m9038,
	RemotingServices_DisposeIdentity_m9039,
	RemotingServices_GetNormalizedUri_m9040,
	ServerIdentity__ctor_m9041,
	ServerIdentity_get_ObjectType_m9042,
	ServerIdentity_CreateObjRef_m9043,
	ClientActivatedIdentity_GetServerObject_m9044,
	SingletonIdentity__ctor_m9045,
	SingleCallIdentity__ctor_m9046,
	TypeInfo__ctor_m9047,
	SoapServices__cctor_m9048,
	SoapServices_get_XmlNsForClrTypeWithAssembly_m9049,
	SoapServices_get_XmlNsForClrTypeWithNs_m9050,
	SoapServices_get_XmlNsForClrTypeWithNsAndAssembly_m9051,
	SoapServices_CodeXmlNamespaceForClrTypeNamespace_m9052,
	SoapServices_GetNameKey_m9053,
	SoapServices_GetAssemblyName_m9054,
	SoapServices_GetXmlElementForInteropType_m9055,
	SoapServices_GetXmlNamespaceForMethodCall_m9056,
	SoapServices_GetXmlNamespaceForMethodResponse_m9057,
	SoapServices_GetXmlTypeForInteropType_m9058,
	SoapServices_PreLoad_m9059,
	SoapServices_PreLoad_m9060,
	SoapServices_RegisterInteropXmlElement_m9061,
	SoapServices_RegisterInteropXmlType_m9062,
	SoapServices_EncodeNs_m9063,
	TypeEntry__ctor_m9064,
	TypeEntry_get_AssemblyName_m9065,
	TypeEntry_set_AssemblyName_m9066,
	TypeEntry_get_TypeName_m9067,
	TypeEntry_set_TypeName_m9068,
	TypeInfo__ctor_m9069,
	TypeInfo_get_TypeName_m9070,
	WellKnownClientTypeEntry__ctor_m9071,
	WellKnownClientTypeEntry_get_ApplicationUrl_m9072,
	WellKnownClientTypeEntry_get_ObjectType_m9073,
	WellKnownClientTypeEntry_get_ObjectUrl_m9074,
	WellKnownClientTypeEntry_ToString_m9075,
	WellKnownServiceTypeEntry__ctor_m9076,
	WellKnownServiceTypeEntry_get_Mode_m9077,
	WellKnownServiceTypeEntry_get_ObjectType_m9078,
	WellKnownServiceTypeEntry_get_ObjectUri_m9079,
	WellKnownServiceTypeEntry_ToString_m9080,
	BinaryCommon__cctor_m9081,
	BinaryCommon_IsPrimitive_m9082,
	BinaryCommon_GetTypeFromCode_m9083,
	BinaryCommon_SwapBytes_m9084,
	BinaryFormatter__ctor_m9085,
	BinaryFormatter__ctor_m9086,
	BinaryFormatter_get_DefaultSurrogateSelector_m9087,
	BinaryFormatter_set_AssemblyFormat_m9088,
	BinaryFormatter_get_Binder_m9089,
	BinaryFormatter_get_Context_m9090,
	BinaryFormatter_get_SurrogateSelector_m9091,
	BinaryFormatter_get_FilterLevel_m9092,
	BinaryFormatter_Deserialize_m9093,
	BinaryFormatter_NoCheckDeserialize_m9094,
	BinaryFormatter_ReadBinaryHeader_m9095,
	MessageFormatter_ReadMethodCall_m9096,
	MessageFormatter_ReadMethodResponse_m9097,
	TypeMetadata__ctor_m9098,
	ArrayNullFiller__ctor_m9099,
	ObjectReader__ctor_m9100,
	ObjectReader_ReadObjectGraph_m9101,
	ObjectReader_ReadObjectGraph_m9102,
	ObjectReader_ReadNextObject_m9103,
	ObjectReader_ReadNextObject_m9104,
	ObjectReader_get_CurrentObject_m9105,
	ObjectReader_ReadObject_m9106,
	ObjectReader_ReadAssembly_m9107,
	ObjectReader_ReadObjectInstance_m9108,
	ObjectReader_ReadRefTypeObjectInstance_m9109,
	ObjectReader_ReadObjectContent_m9110,
	ObjectReader_RegisterObject_m9111,
	ObjectReader_ReadStringIntance_m9112,
	ObjectReader_ReadGenericArray_m9113,
	ObjectReader_ReadBoxedPrimitiveTypeValue_m9114,
	ObjectReader_ReadArrayOfPrimitiveType_m9115,
	ObjectReader_BlockRead_m9116,
	ObjectReader_ReadArrayOfObject_m9117,
	ObjectReader_ReadArrayOfString_m9118,
	ObjectReader_ReadSimpleArray_m9119,
	ObjectReader_ReadTypeMetadata_m9120,
	ObjectReader_ReadValue_m9121,
	ObjectReader_SetObjectValue_m9122,
	ObjectReader_RecordFixup_m9123,
	ObjectReader_GetDeserializationType_m9124,
	ObjectReader_ReadType_m9125,
	ObjectReader_ReadPrimitiveTypeValue_m9126,
	FormatterConverter__ctor_m9127,
	FormatterConverter_Convert_m9128,
	FormatterConverter_ToBoolean_m9129,
	FormatterConverter_ToInt16_m9130,
	FormatterConverter_ToInt32_m9131,
	FormatterConverter_ToInt64_m9132,
	FormatterConverter_ToString_m9133,
	FormatterServices_GetUninitializedObject_m9134,
	FormatterServices_GetSafeUninitializedObject_m9135,
	ObjectManager__ctor_m9136,
	ObjectManager_DoFixups_m9137,
	ObjectManager_GetObjectRecord_m9138,
	ObjectManager_GetObject_m9139,
	ObjectManager_RaiseDeserializationEvent_m9140,
	ObjectManager_RaiseOnDeserializingEvent_m9141,
	ObjectManager_RaiseOnDeserializedEvent_m9142,
	ObjectManager_AddFixup_m9143,
	ObjectManager_RecordArrayElementFixup_m9144,
	ObjectManager_RecordArrayElementFixup_m9145,
	ObjectManager_RecordDelayedFixup_m9146,
	ObjectManager_RecordFixup_m9147,
	ObjectManager_RegisterObjectInternal_m9148,
	ObjectManager_RegisterObject_m9149,
	BaseFixupRecord__ctor_m9150,
	BaseFixupRecord_DoFixup_m9151,
	ArrayFixupRecord__ctor_m9152,
	ArrayFixupRecord_FixupImpl_m9153,
	MultiArrayFixupRecord__ctor_m9154,
	MultiArrayFixupRecord_FixupImpl_m9155,
	FixupRecord__ctor_m9156,
	FixupRecord_FixupImpl_m9157,
	DelayedFixupRecord__ctor_m9158,
	DelayedFixupRecord_FixupImpl_m9159,
	ObjectRecord__ctor_m9160,
	ObjectRecord_SetMemberValue_m9161,
	ObjectRecord_SetArrayValue_m9162,
	ObjectRecord_SetMemberValue_m9163,
	ObjectRecord_get_IsInstanceReady_m9164,
	ObjectRecord_get_IsUnsolvedObjectReference_m9165,
	ObjectRecord_get_IsRegistered_m9166,
	ObjectRecord_DoFixups_m9167,
	ObjectRecord_RemoveFixup_m9168,
	ObjectRecord_UnchainFixup_m9169,
	ObjectRecord_ChainFixup_m9170,
	ObjectRecord_LoadData_m9171,
	ObjectRecord_get_HasPendingFixups_m9172,
	SerializationBinder__ctor_m9173,
	CallbackHandler__ctor_m9174,
	CallbackHandler_Invoke_m9175,
	CallbackHandler_BeginInvoke_m9176,
	CallbackHandler_EndInvoke_m9177,
	SerializationCallbacks__ctor_m9178,
	SerializationCallbacks__cctor_m9179,
	SerializationCallbacks_get_HasDeserializedCallbacks_m9180,
	SerializationCallbacks_GetMethodsByAttribute_m9181,
	SerializationCallbacks_Invoke_m9182,
	SerializationCallbacks_RaiseOnDeserializing_m9183,
	SerializationCallbacks_RaiseOnDeserialized_m9184,
	SerializationCallbacks_GetSerializationCallbacks_m9185,
	SerializationEntry__ctor_m9186,
	SerializationEntry_get_Name_m9187,
	SerializationEntry_get_Value_m9188,
	SerializationException__ctor_m9189,
	SerializationException__ctor_m5663,
	SerializationException__ctor_m9190,
	SerializationInfo__ctor_m9191,
	SerializationInfo_AddValue_m5659,
	SerializationInfo_GetValue_m5662,
	SerializationInfo_SetType_m9192,
	SerializationInfo_GetEnumerator_m9193,
	SerializationInfo_AddValue_m9194,
	SerializationInfo_AddValue_m5661,
	SerializationInfo_AddValue_m5660,
	SerializationInfo_AddValue_m9195,
	SerializationInfo_AddValue_m9196,
	SerializationInfo_AddValue_m5670,
	SerializationInfo_AddValue_m9197,
	SerializationInfo_AddValue_m4688,
	SerializationInfo_GetBoolean_m5664,
	SerializationInfo_GetInt16_m9198,
	SerializationInfo_GetInt32_m5669,
	SerializationInfo_GetInt64_m5668,
	SerializationInfo_GetString_m5667,
	SerializationInfoEnumerator__ctor_m9199,
	SerializationInfoEnumerator_System_Collections_IEnumerator_get_Current_m9200,
	SerializationInfoEnumerator_get_Current_m9201,
	SerializationInfoEnumerator_get_Name_m9202,
	SerializationInfoEnumerator_get_Value_m9203,
	SerializationInfoEnumerator_MoveNext_m9204,
	SerializationInfoEnumerator_Reset_m9205,
	StreamingContext__ctor_m9206,
	StreamingContext__ctor_m9207,
	StreamingContext_get_State_m9208,
	StreamingContext_Equals_m9209,
	StreamingContext_GetHashCode_m9210,
	X509Certificate__ctor_m9211,
	X509Certificate__ctor_m4748,
	X509Certificate__ctor_m5695,
	X509Certificate__ctor_m9212,
	X509Certificate_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9213,
	X509Certificate_System_Runtime_Serialization_ISerializable_GetObjectData_m9214,
	X509Certificate_tostr_m9215,
	X509Certificate_Equals_m9216,
	X509Certificate_GetCertHash_m9217,
	X509Certificate_GetCertHashString_m5697,
	X509Certificate_GetEffectiveDateString_m9218,
	X509Certificate_GetExpirationDateString_m9219,
	X509Certificate_GetHashCode_m9220,
	X509Certificate_GetIssuerName_m9221,
	X509Certificate_GetName_m9222,
	X509Certificate_GetPublicKey_m9223,
	X509Certificate_GetRawCertData_m9224,
	X509Certificate_ToString_m9225,
	X509Certificate_ToString_m5700,
	X509Certificate_get_Issuer_m5702,
	X509Certificate_get_Subject_m5701,
	X509Certificate_Equals_m9226,
	X509Certificate_Import_m5698,
	X509Certificate_Reset_m5699,
	AsymmetricAlgorithm__ctor_m9227,
	AsymmetricAlgorithm_System_IDisposable_Dispose_m9228,
	AsymmetricAlgorithm_get_KeySize_m4662,
	AsymmetricAlgorithm_set_KeySize_m4661,
	AsymmetricAlgorithm_Clear_m4756,
	AsymmetricAlgorithm_GetNamedParam_m9229,
	AsymmetricKeyExchangeFormatter__ctor_m9230,
	AsymmetricSignatureDeformatter__ctor_m4741,
	AsymmetricSignatureFormatter__ctor_m4742,
	Base64Constants__cctor_m9231,
	CryptoConfig__cctor_m9232,
	CryptoConfig_Initialize_m9233,
	CryptoConfig_CreateFromName_m4653,
	CryptoConfig_CreateFromName_m5708,
	CryptoConfig_MapNameToOID_m4654,
	CryptoConfig_EncodeOID_m4642,
	CryptoConfig_EncodeLongNumber_m9234,
	CryptographicException__ctor_m9235,
	CryptographicException__ctor_m3725,
	CryptographicException__ctor_m4676,
	CryptographicException__ctor_m3734,
	CryptographicException__ctor_m9236,
	CryptographicUnexpectedOperationException__ctor_m9237,
	CryptographicUnexpectedOperationException__ctor_m4707,
	CryptographicUnexpectedOperationException__ctor_m9238,
	CspParameters__ctor_m4656,
	CspParameters__ctor_m9239,
	CspParameters__ctor_m9240,
	CspParameters__ctor_m9241,
	CspParameters_get_Flags_m9242,
	CspParameters_set_Flags_m4657,
	DES__ctor_m9243,
	DES__cctor_m9244,
	DES_Create_m4708,
	DES_Create_m9245,
	DES_IsWeakKey_m9246,
	DES_IsSemiWeakKey_m9247,
	DES_get_Key_m9248,
	DES_set_Key_m9249,
	DESTransform__ctor_m9250,
	DESTransform__cctor_m9251,
	DESTransform_CipherFunct_m9252,
	DESTransform_Permutation_m9253,
	DESTransform_BSwap_m9254,
	DESTransform_SetKey_m9255,
	DESTransform_ProcessBlock_m9256,
	DESTransform_ECB_m9257,
	DESTransform_GetStrongKey_m9258,
	DESCryptoServiceProvider__ctor_m9259,
	DESCryptoServiceProvider_CreateDecryptor_m9260,
	DESCryptoServiceProvider_CreateEncryptor_m9261,
	DESCryptoServiceProvider_GenerateIV_m9262,
	DESCryptoServiceProvider_GenerateKey_m9263,
	DSA__ctor_m9264,
	DSA_Create_m4659,
	DSA_Create_m9265,
	DSA_ZeroizePrivateKey_m9266,
	DSA_FromXmlString_m9267,
	DSA_ToXmlString_m9268,
	DSACryptoServiceProvider__ctor_m9269,
	DSACryptoServiceProvider__ctor_m4678,
	DSACryptoServiceProvider__ctor_m9270,
	DSACryptoServiceProvider__cctor_m9271,
	DSACryptoServiceProvider_Finalize_m9272,
	DSACryptoServiceProvider_get_KeySize_m9273,
	DSACryptoServiceProvider_get_PublicOnly_m5689,
	DSACryptoServiceProvider_ExportParameters_m9274,
	DSACryptoServiceProvider_ImportParameters_m9275,
	DSACryptoServiceProvider_CreateSignature_m9276,
	DSACryptoServiceProvider_VerifySignature_m9277,
	DSACryptoServiceProvider_Dispose_m9278,
	DSACryptoServiceProvider_OnKeyGenerated_m9279,
	DSASignatureDeformatter__ctor_m9280,
	DSASignatureDeformatter__ctor_m4686,
	DSASignatureDeformatter_SetHashAlgorithm_m9281,
	DSASignatureDeformatter_SetKey_m9282,
	DSASignatureDeformatter_VerifySignature_m9283,
	DSASignatureFormatter__ctor_m9284,
	DSASignatureFormatter_CreateSignature_m9285,
	DSASignatureFormatter_SetHashAlgorithm_m9286,
	DSASignatureFormatter_SetKey_m9287,
	HMAC__ctor_m9288,
	HMAC_get_BlockSizeValue_m9289,
	HMAC_set_BlockSizeValue_m9290,
	HMAC_set_HashName_m9291,
	HMAC_get_Key_m9292,
	HMAC_set_Key_m9293,
	HMAC_get_Block_m9294,
	HMAC_KeySetup_m9295,
	HMAC_Dispose_m9296,
	HMAC_HashCore_m9297,
	HMAC_HashFinal_m9298,
	HMAC_Initialize_m9299,
	HMAC_Create_m4671,
	HMAC_Create_m9300,
	HMACMD5__ctor_m9301,
	HMACMD5__ctor_m9302,
	HMACRIPEMD160__ctor_m9303,
	HMACRIPEMD160__ctor_m9304,
	HMACSHA1__ctor_m9305,
	HMACSHA1__ctor_m9306,
	HMACSHA256__ctor_m9307,
	HMACSHA256__ctor_m9308,
	HMACSHA384__ctor_m9309,
	HMACSHA384__ctor_m9310,
	HMACSHA384__cctor_m9311,
	HMACSHA384_set_ProduceLegacyHmacValues_m9312,
	HMACSHA512__ctor_m9313,
	HMACSHA512__ctor_m9314,
	HMACSHA512__cctor_m9315,
	HMACSHA512_set_ProduceLegacyHmacValues_m9316,
	HashAlgorithm__ctor_m4652,
	HashAlgorithm_System_IDisposable_Dispose_m9317,
	HashAlgorithm_get_CanReuseTransform_m9318,
	HashAlgorithm_ComputeHash_m4693,
	HashAlgorithm_ComputeHash_m4665,
	HashAlgorithm_Create_m4664,
	HashAlgorithm_get_Hash_m9319,
	HashAlgorithm_get_HashSize_m9320,
	HashAlgorithm_Dispose_m9321,
	HashAlgorithm_TransformBlock_m9322,
	HashAlgorithm_TransformFinalBlock_m9323,
	KeySizes__ctor_m3736,
	KeySizes_get_MaxSize_m9324,
	KeySizes_get_MinSize_m9325,
	KeySizes_get_SkipSize_m9326,
	KeySizes_IsLegal_m9327,
	KeySizes_IsLegalKeySize_m9328,
	KeyedHashAlgorithm__ctor_m4706,
	KeyedHashAlgorithm_Finalize_m9329,
	KeyedHashAlgorithm_get_Key_m9330,
	KeyedHashAlgorithm_set_Key_m9331,
	KeyedHashAlgorithm_Dispose_m9332,
	KeyedHashAlgorithm_ZeroizeKey_m9333,
	MACTripleDES__ctor_m9334,
	MACTripleDES_Setup_m9335,
	MACTripleDES_Finalize_m9336,
	MACTripleDES_Dispose_m9337,
	MACTripleDES_Initialize_m9338,
	MACTripleDES_HashCore_m9339,
	MACTripleDES_HashFinal_m9340,
	MD5__ctor_m9341,
	MD5_Create_m4679,
	MD5_Create_m9342,
	MD5CryptoServiceProvider__ctor_m9343,
	MD5CryptoServiceProvider__cctor_m9344,
	MD5CryptoServiceProvider_Finalize_m9345,
	MD5CryptoServiceProvider_Dispose_m9346,
	MD5CryptoServiceProvider_HashCore_m9347,
	MD5CryptoServiceProvider_HashFinal_m9348,
	MD5CryptoServiceProvider_Initialize_m9349,
	MD5CryptoServiceProvider_ProcessBlock_m9350,
	MD5CryptoServiceProvider_ProcessFinalBlock_m9351,
	MD5CryptoServiceProvider_AddLength_m9352,
	RC2__ctor_m9353,
	RC2_Create_m4709,
	RC2_Create_m9354,
	RC2_get_EffectiveKeySize_m9355,
	RC2_get_KeySize_m9356,
	RC2_set_KeySize_m9357,
	RC2CryptoServiceProvider__ctor_m9358,
	RC2CryptoServiceProvider_get_EffectiveKeySize_m9359,
	RC2CryptoServiceProvider_CreateDecryptor_m9360,
	RC2CryptoServiceProvider_CreateEncryptor_m9361,
	RC2CryptoServiceProvider_GenerateIV_m9362,
	RC2CryptoServiceProvider_GenerateKey_m9363,
	RC2Transform__ctor_m9364,
	RC2Transform__cctor_m9365,
	RC2Transform_ECB_m9366,
	RIPEMD160__ctor_m9367,
	RIPEMD160Managed__ctor_m9368,
	RIPEMD160Managed_Initialize_m9369,
	RIPEMD160Managed_HashCore_m9370,
	RIPEMD160Managed_HashFinal_m9371,
	RIPEMD160Managed_Finalize_m9372,
	RIPEMD160Managed_ProcessBlock_m9373,
	RIPEMD160Managed_Compress_m9374,
	RIPEMD160Managed_CompressFinal_m9375,
	RIPEMD160Managed_ROL_m9376,
	RIPEMD160Managed_F_m9377,
	RIPEMD160Managed_G_m9378,
	RIPEMD160Managed_H_m9379,
	RIPEMD160Managed_I_m9380,
	RIPEMD160Managed_J_m9381,
	RIPEMD160Managed_FF_m9382,
	RIPEMD160Managed_GG_m9383,
	RIPEMD160Managed_HH_m9384,
	RIPEMD160Managed_II_m9385,
	RIPEMD160Managed_JJ_m9386,
	RIPEMD160Managed_FFF_m9387,
	RIPEMD160Managed_GGG_m9388,
	RIPEMD160Managed_HHH_m9389,
	RIPEMD160Managed_III_m9390,
	RIPEMD160Managed_JJJ_m9391,
	RNGCryptoServiceProvider__ctor_m9392,
	RNGCryptoServiceProvider__cctor_m9393,
	RNGCryptoServiceProvider_Check_m9394,
	RNGCryptoServiceProvider_RngOpen_m9395,
	RNGCryptoServiceProvider_RngInitialize_m9396,
	RNGCryptoServiceProvider_RngGetBytes_m9397,
	RNGCryptoServiceProvider_RngClose_m9398,
	RNGCryptoServiceProvider_GetBytes_m9399,
	RNGCryptoServiceProvider_GetNonZeroBytes_m9400,
	RNGCryptoServiceProvider_Finalize_m9401,
	RSA__ctor_m4660,
	RSA_Create_m4655,
	RSA_Create_m9402,
	RSA_ZeroizePrivateKey_m9403,
	RSA_FromXmlString_m9404,
	RSA_ToXmlString_m9405,
	RSACryptoServiceProvider__ctor_m9406,
	RSACryptoServiceProvider__ctor_m4658,
	RSACryptoServiceProvider__ctor_m4682,
	RSACryptoServiceProvider__cctor_m9407,
	RSACryptoServiceProvider_Common_m9408,
	RSACryptoServiceProvider_Finalize_m9409,
	RSACryptoServiceProvider_get_KeySize_m9410,
	RSACryptoServiceProvider_get_PublicOnly_m5688,
	RSACryptoServiceProvider_DecryptValue_m9411,
	RSACryptoServiceProvider_EncryptValue_m9412,
	RSACryptoServiceProvider_ExportParameters_m9413,
	RSACryptoServiceProvider_ImportParameters_m9414,
	RSACryptoServiceProvider_Dispose_m9415,
	RSACryptoServiceProvider_OnKeyGenerated_m9416,
	RSAPKCS1KeyExchangeFormatter__ctor_m4755,
	RSAPKCS1KeyExchangeFormatter_CreateKeyExchange_m9417,
	RSAPKCS1KeyExchangeFormatter_SetRSAKey_m9418,
	RSAPKCS1SignatureDeformatter__ctor_m9419,
	RSAPKCS1SignatureDeformatter__ctor_m4687,
	RSAPKCS1SignatureDeformatter_SetHashAlgorithm_m9420,
	RSAPKCS1SignatureDeformatter_SetKey_m9421,
	RSAPKCS1SignatureDeformatter_VerifySignature_m9422,
	RSAPKCS1SignatureFormatter__ctor_m9423,
	RSAPKCS1SignatureFormatter_CreateSignature_m9424,
	RSAPKCS1SignatureFormatter_SetHashAlgorithm_m9425,
	RSAPKCS1SignatureFormatter_SetKey_m9426,
	RandomNumberGenerator__ctor_m9427,
	RandomNumberGenerator_Create_m3724,
	RandomNumberGenerator_Create_m9428,
	Rijndael__ctor_m9429,
	Rijndael_Create_m4711,
	Rijndael_Create_m9430,
	RijndaelManaged__ctor_m9431,
	RijndaelManaged_GenerateIV_m9432,
	RijndaelManaged_GenerateKey_m9433,
	RijndaelManaged_CreateDecryptor_m9434,
	RijndaelManaged_CreateEncryptor_m9435,
	RijndaelTransform__ctor_m9436,
	RijndaelTransform__cctor_m9437,
	RijndaelTransform_Clear_m9438,
	RijndaelTransform_ECB_m9439,
	RijndaelTransform_SubByte_m9440,
	RijndaelTransform_Encrypt128_m9441,
	RijndaelTransform_Encrypt192_m9442,
	RijndaelTransform_Encrypt256_m9443,
	RijndaelTransform_Decrypt128_m9444,
	RijndaelTransform_Decrypt192_m9445,
	RijndaelTransform_Decrypt256_m9446,
	RijndaelManagedTransform__ctor_m9447,
	RijndaelManagedTransform_System_IDisposable_Dispose_m9448,
	RijndaelManagedTransform_get_CanReuseTransform_m9449,
	RijndaelManagedTransform_TransformBlock_m9450,
	RijndaelManagedTransform_TransformFinalBlock_m9451,
	SHA1__ctor_m9452,
	SHA1_Create_m4680,
	SHA1_Create_m9453,
	SHA1Internal__ctor_m9454,
	SHA1Internal_HashCore_m9455,
	SHA1Internal_HashFinal_m9456,
	SHA1Internal_Initialize_m9457,
	SHA1Internal_ProcessBlock_m9458,
	SHA1Internal_InitialiseBuff_m9459,
	SHA1Internal_FillBuff_m9460,
	SHA1Internal_ProcessFinalBlock_m9461,
	SHA1Internal_AddLength_m9462,
	SHA1CryptoServiceProvider__ctor_m9463,
	SHA1CryptoServiceProvider_Finalize_m9464,
	SHA1CryptoServiceProvider_Dispose_m9465,
	SHA1CryptoServiceProvider_HashCore_m9466,
	SHA1CryptoServiceProvider_HashFinal_m9467,
	SHA1CryptoServiceProvider_Initialize_m9468,
	SHA1Managed__ctor_m9469,
	SHA1Managed_HashCore_m9470,
	SHA1Managed_HashFinal_m9471,
	SHA1Managed_Initialize_m9472,
	SHA256__ctor_m9473,
	SHA256_Create_m4681,
	SHA256_Create_m9474,
	SHA256Managed__ctor_m9475,
	SHA256Managed_HashCore_m9476,
	SHA256Managed_HashFinal_m9477,
	SHA256Managed_Initialize_m9478,
	SHA256Managed_ProcessBlock_m9479,
	SHA256Managed_ProcessFinalBlock_m9480,
	SHA256Managed_AddLength_m9481,
	SHA384__ctor_m9482,
	SHA384Managed__ctor_m9483,
	SHA384Managed_Initialize_m9484,
	SHA384Managed_Initialize_m9485,
	SHA384Managed_HashCore_m9486,
	SHA384Managed_HashFinal_m9487,
	SHA384Managed_update_m9488,
	SHA384Managed_processWord_m9489,
	SHA384Managed_unpackWord_m9490,
	SHA384Managed_adjustByteCounts_m9491,
	SHA384Managed_processLength_m9492,
	SHA384Managed_processBlock_m9493,
	SHA512__ctor_m9494,
	SHA512Managed__ctor_m9495,
	SHA512Managed_Initialize_m9496,
	SHA512Managed_Initialize_m9497,
	SHA512Managed_HashCore_m9498,
	SHA512Managed_HashFinal_m9499,
	SHA512Managed_update_m9500,
	SHA512Managed_processWord_m9501,
	SHA512Managed_unpackWord_m9502,
	SHA512Managed_adjustByteCounts_m9503,
	SHA512Managed_processLength_m9504,
	SHA512Managed_processBlock_m9505,
	SHA512Managed_rotateRight_m9506,
	SHA512Managed_Ch_m9507,
	SHA512Managed_Maj_m9508,
	SHA512Managed_Sum0_m9509,
	SHA512Managed_Sum1_m9510,
	SHA512Managed_Sigma0_m9511,
	SHA512Managed_Sigma1_m9512,
	SHAConstants__cctor_m9513,
	SignatureDescription__ctor_m9514,
	SignatureDescription_set_DeformatterAlgorithm_m9515,
	SignatureDescription_set_DigestAlgorithm_m9516,
	SignatureDescription_set_FormatterAlgorithm_m9517,
	SignatureDescription_set_KeyAlgorithm_m9518,
	DSASignatureDescription__ctor_m9519,
	RSAPKCS1SHA1SignatureDescription__ctor_m9520,
	SymmetricAlgorithm__ctor_m3735,
	SymmetricAlgorithm_System_IDisposable_Dispose_m9521,
	SymmetricAlgorithm_Finalize_m4650,
	SymmetricAlgorithm_Clear_m4670,
	SymmetricAlgorithm_Dispose_m3743,
	SymmetricAlgorithm_get_BlockSize_m9522,
	SymmetricAlgorithm_set_BlockSize_m9523,
	SymmetricAlgorithm_get_FeedbackSize_m9524,
	SymmetricAlgorithm_get_IV_m3737,
	SymmetricAlgorithm_set_IV_m3738,
	SymmetricAlgorithm_get_Key_m3739,
	SymmetricAlgorithm_set_Key_m3740,
	SymmetricAlgorithm_get_KeySize_m3741,
	SymmetricAlgorithm_set_KeySize_m3742,
	SymmetricAlgorithm_get_LegalKeySizes_m9525,
	SymmetricAlgorithm_get_Mode_m9526,
	SymmetricAlgorithm_set_Mode_m9527,
	SymmetricAlgorithm_get_Padding_m9528,
	SymmetricAlgorithm_set_Padding_m9529,
	SymmetricAlgorithm_CreateDecryptor_m9530,
	SymmetricAlgorithm_CreateEncryptor_m9531,
	SymmetricAlgorithm_Create_m4669,
	ToBase64Transform_System_IDisposable_Dispose_m9532,
	ToBase64Transform_Finalize_m9533,
	ToBase64Transform_get_CanReuseTransform_m9534,
	ToBase64Transform_get_InputBlockSize_m9535,
	ToBase64Transform_get_OutputBlockSize_m9536,
	ToBase64Transform_Dispose_m9537,
	ToBase64Transform_TransformBlock_m9538,
	ToBase64Transform_InternalTransformBlock_m9539,
	ToBase64Transform_TransformFinalBlock_m9540,
	ToBase64Transform_InternalTransformFinalBlock_m9541,
	TripleDES__ctor_m9542,
	TripleDES_get_Key_m9543,
	TripleDES_set_Key_m9544,
	TripleDES_IsWeakKey_m9545,
	TripleDES_Create_m4710,
	TripleDES_Create_m9546,
	TripleDESCryptoServiceProvider__ctor_m9547,
	TripleDESCryptoServiceProvider_GenerateIV_m9548,
	TripleDESCryptoServiceProvider_GenerateKey_m9549,
	TripleDESCryptoServiceProvider_CreateDecryptor_m9550,
	TripleDESCryptoServiceProvider_CreateEncryptor_m9551,
	TripleDESTransform__ctor_m9552,
	TripleDESTransform_ECB_m9553,
	TripleDESTransform_GetStrongKey_m9554,
	SecurityPermission__ctor_m9555,
	SecurityPermission_set_Flags_m9556,
	SecurityPermission_IsUnrestricted_m9557,
	SecurityPermission_IsSubsetOf_m9558,
	SecurityPermission_ToXml_m9559,
	SecurityPermission_IsEmpty_m9560,
	SecurityPermission_Cast_m9561,
	StrongNamePublicKeyBlob_Equals_m9562,
	StrongNamePublicKeyBlob_GetHashCode_m9563,
	StrongNamePublicKeyBlob_ToString_m9564,
	ApplicationTrust__ctor_m9565,
	EvidenceEnumerator__ctor_m9566,
	EvidenceEnumerator_MoveNext_m9567,
	EvidenceEnumerator_Reset_m9568,
	EvidenceEnumerator_get_Current_m9569,
	Evidence__ctor_m9570,
	Evidence_get_Count_m9571,
	Evidence_get_IsSynchronized_m9572,
	Evidence_get_SyncRoot_m9573,
	Evidence_get_HostEvidenceList_m9574,
	Evidence_get_AssemblyEvidenceList_m9575,
	Evidence_CopyTo_m9576,
	Evidence_Equals_m9577,
	Evidence_GetEnumerator_m9578,
	Evidence_GetHashCode_m9579,
	Hash__ctor_m9580,
	Hash__ctor_m9581,
	Hash_GetObjectData_m9582,
	Hash_ToString_m9583,
	Hash_GetData_m9584,
	StrongName_get_Name_m9585,
	StrongName_get_PublicKey_m9586,
	StrongName_get_Version_m9587,
	StrongName_Equals_m9588,
	StrongName_GetHashCode_m9589,
	StrongName_ToString_m9590,
	WindowsIdentity__ctor_m9591,
	WindowsIdentity__cctor_m9592,
	WindowsIdentity_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m9593,
	WindowsIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m9594,
	WindowsIdentity_Dispose_m9595,
	WindowsIdentity_GetCurrentToken_m9596,
	WindowsIdentity_GetTokenName_m9597,
	CodeAccessPermission__ctor_m9598,
	CodeAccessPermission_Equals_m9599,
	CodeAccessPermission_GetHashCode_m9600,
	CodeAccessPermission_ToString_m9601,
	CodeAccessPermission_Element_m9602,
	CodeAccessPermission_ThrowInvalidPermission_m9603,
	PermissionSet__ctor_m9604,
	PermissionSet__ctor_m9605,
	PermissionSet_set_DeclarativeSecurity_m9606,
	PermissionSet_CreateFromBinaryFormat_m9607,
	SecurityContext__ctor_m9608,
	SecurityContext__ctor_m9609,
	SecurityContext_Capture_m9610,
	SecurityContext_get_FlowSuppressed_m9611,
	SecurityContext_get_CompressedStack_m9612,
	SecurityAttribute__ctor_m9613,
	SecurityAttribute_get_Name_m9614,
	SecurityAttribute_get_Value_m9615,
	SecurityElement__ctor_m9616,
	SecurityElement__ctor_m9617,
	SecurityElement__cctor_m9618,
	SecurityElement_get_Children_m9619,
	SecurityElement_get_Tag_m9620,
	SecurityElement_set_Text_m9621,
	SecurityElement_AddAttribute_m9622,
	SecurityElement_AddChild_m9623,
	SecurityElement_Escape_m9624,
	SecurityElement_Unescape_m9625,
	SecurityElement_IsValidAttributeName_m9626,
	SecurityElement_IsValidAttributeValue_m9627,
	SecurityElement_IsValidTag_m9628,
	SecurityElement_IsValidText_m9629,
	SecurityElement_SearchForChildByTag_m9630,
	SecurityElement_ToString_m9631,
	SecurityElement_ToXml_m9632,
	SecurityElement_GetAttribute_m9633,
	SecurityException__ctor_m9634,
	SecurityException__ctor_m9635,
	SecurityException__ctor_m9636,
	SecurityException_get_Demanded_m9637,
	SecurityException_get_FirstPermissionThatFailed_m9638,
	SecurityException_get_PermissionState_m9639,
	SecurityException_get_PermissionType_m9640,
	SecurityException_get_GrantedSet_m9641,
	SecurityException_get_RefusedSet_m9642,
	SecurityException_GetObjectData_m9643,
	SecurityException_ToString_m9644,
	SecurityFrame__ctor_m9645,
	SecurityFrame__GetSecurityStack_m9646,
	SecurityFrame_InitFromRuntimeFrame_m9647,
	SecurityFrame_get_Assembly_m9648,
	SecurityFrame_get_Domain_m9649,
	SecurityFrame_ToString_m9650,
	SecurityFrame_GetStack_m9651,
	SecurityManager__cctor_m9652,
	SecurityManager_get_SecurityEnabled_m9653,
	SecurityManager_Decode_m9654,
	SecurityManager_Decode_m9655,
	SecuritySafeCriticalAttribute__ctor_m9656,
	SuppressUnmanagedCodeSecurityAttribute__ctor_m9657,
	UnverifiableCodeAttribute__ctor_m9658,
	ASCIIEncoding__ctor_m9659,
	ASCIIEncoding_GetByteCount_m9660,
	ASCIIEncoding_GetByteCount_m9661,
	ASCIIEncoding_GetBytes_m9662,
	ASCIIEncoding_GetBytes_m9663,
	ASCIIEncoding_GetBytes_m9664,
	ASCIIEncoding_GetBytes_m9665,
	ASCIIEncoding_GetCharCount_m9666,
	ASCIIEncoding_GetChars_m9667,
	ASCIIEncoding_GetChars_m9668,
	ASCIIEncoding_GetMaxByteCount_m9669,
	ASCIIEncoding_GetMaxCharCount_m9670,
	ASCIIEncoding_GetString_m9671,
	ASCIIEncoding_GetBytes_m9672,
	ASCIIEncoding_GetByteCount_m9673,
	ASCIIEncoding_GetDecoder_m9674,
	Decoder__ctor_m9675,
	Decoder_set_Fallback_m9676,
	Decoder_get_FallbackBuffer_m9677,
	DecoderExceptionFallback__ctor_m9678,
	DecoderExceptionFallback_CreateFallbackBuffer_m9679,
	DecoderExceptionFallback_Equals_m9680,
	DecoderExceptionFallback_GetHashCode_m9681,
	DecoderExceptionFallbackBuffer__ctor_m9682,
	DecoderExceptionFallbackBuffer_get_Remaining_m9683,
	DecoderExceptionFallbackBuffer_Fallback_m9684,
	DecoderExceptionFallbackBuffer_GetNextChar_m9685,
	DecoderFallback__ctor_m9686,
	DecoderFallback__cctor_m9687,
	DecoderFallback_get_ExceptionFallback_m9688,
	DecoderFallback_get_ReplacementFallback_m9689,
	DecoderFallback_get_StandardSafeFallback_m9690,
	DecoderFallbackBuffer__ctor_m9691,
	DecoderFallbackBuffer_Reset_m9692,
	DecoderFallbackException__ctor_m9693,
	DecoderFallbackException__ctor_m9694,
	DecoderFallbackException__ctor_m9695,
	DecoderReplacementFallback__ctor_m9696,
	DecoderReplacementFallback__ctor_m9697,
	DecoderReplacementFallback_get_DefaultString_m9698,
	DecoderReplacementFallback_CreateFallbackBuffer_m9699,
	DecoderReplacementFallback_Equals_m9700,
	DecoderReplacementFallback_GetHashCode_m9701,
	DecoderReplacementFallbackBuffer__ctor_m9702,
	DecoderReplacementFallbackBuffer_get_Remaining_m9703,
	DecoderReplacementFallbackBuffer_Fallback_m9704,
	DecoderReplacementFallbackBuffer_GetNextChar_m9705,
	DecoderReplacementFallbackBuffer_Reset_m9706,
	EncoderExceptionFallback__ctor_m9707,
	EncoderExceptionFallback_CreateFallbackBuffer_m9708,
	EncoderExceptionFallback_Equals_m9709,
	EncoderExceptionFallback_GetHashCode_m9710,
	EncoderExceptionFallbackBuffer__ctor_m9711,
	EncoderExceptionFallbackBuffer_get_Remaining_m9712,
	EncoderExceptionFallbackBuffer_Fallback_m9713,
	EncoderExceptionFallbackBuffer_Fallback_m9714,
	EncoderExceptionFallbackBuffer_GetNextChar_m9715,
	EncoderFallback__ctor_m9716,
	EncoderFallback__cctor_m9717,
	EncoderFallback_get_ExceptionFallback_m9718,
	EncoderFallback_get_ReplacementFallback_m9719,
	EncoderFallback_get_StandardSafeFallback_m9720,
	EncoderFallbackBuffer__ctor_m9721,
	EncoderFallbackException__ctor_m9722,
	EncoderFallbackException__ctor_m9723,
	EncoderFallbackException__ctor_m9724,
	EncoderFallbackException__ctor_m9725,
	EncoderReplacementFallback__ctor_m9726,
	EncoderReplacementFallback__ctor_m9727,
	EncoderReplacementFallback_get_DefaultString_m9728,
	EncoderReplacementFallback_CreateFallbackBuffer_m9729,
	EncoderReplacementFallback_Equals_m9730,
	EncoderReplacementFallback_GetHashCode_m9731,
	EncoderReplacementFallbackBuffer__ctor_m9732,
	EncoderReplacementFallbackBuffer_get_Remaining_m9733,
	EncoderReplacementFallbackBuffer_Fallback_m9734,
	EncoderReplacementFallbackBuffer_Fallback_m9735,
	EncoderReplacementFallbackBuffer_Fallback_m9736,
	EncoderReplacementFallbackBuffer_GetNextChar_m9737,
	ForwardingDecoder__ctor_m9738,
	ForwardingDecoder_GetChars_m9739,
	Encoding__ctor_m9740,
	Encoding__ctor_m9741,
	Encoding__cctor_m9742,
	Encoding___m9743,
	Encoding_get_IsReadOnly_m9744,
	Encoding_get_DecoderFallback_m9745,
	Encoding_set_DecoderFallback_m9746,
	Encoding_get_EncoderFallback_m9747,
	Encoding_SetFallbackInternal_m9748,
	Encoding_Equals_m9749,
	Encoding_GetByteCount_m9750,
	Encoding_GetByteCount_m9751,
	Encoding_GetBytes_m9752,
	Encoding_GetBytes_m9753,
	Encoding_GetBytes_m9754,
	Encoding_GetBytes_m9755,
	Encoding_GetChars_m9756,
	Encoding_GetDecoder_m9757,
	Encoding_InvokeI18N_m9758,
	Encoding_GetEncoding_m9759,
	Encoding_Clone_m9760,
	Encoding_GetEncoding_m9761,
	Encoding_GetHashCode_m9762,
	Encoding_GetPreamble_m9763,
	Encoding_GetString_m9764,
	Encoding_GetString_m9765,
	Encoding_get_ASCII_m4647,
	Encoding_get_BigEndianUnicode_m4666,
	Encoding_InternalCodePage_m9766,
	Encoding_get_Default_m9767,
	Encoding_get_ISOLatin1_m9768,
	Encoding_get_UTF7_m4673,
	Encoding_get_UTF8_m4674,
	Encoding_get_UTF8Unmarked_m9769,
	Encoding_get_UTF8UnmarkedUnsafe_m9770,
	Encoding_get_Unicode_m9771,
	Encoding_get_UTF32_m9772,
	Encoding_get_BigEndianUTF32_m9773,
	Encoding_GetByteCount_m9774,
	Encoding_GetBytes_m9775,
	Latin1Encoding__ctor_m9776,
	Latin1Encoding_GetByteCount_m9777,
	Latin1Encoding_GetByteCount_m9778,
	Latin1Encoding_GetBytes_m9779,
	Latin1Encoding_GetBytes_m9780,
	Latin1Encoding_GetBytes_m9781,
	Latin1Encoding_GetBytes_m9782,
	Latin1Encoding_GetCharCount_m9783,
	Latin1Encoding_GetChars_m9784,
	Latin1Encoding_GetMaxByteCount_m9785,
	Latin1Encoding_GetMaxCharCount_m9786,
	Latin1Encoding_GetString_m9787,
	Latin1Encoding_GetString_m9788,
	StringBuilder__ctor_m9789,
	StringBuilder__ctor_m9790,
	StringBuilder__ctor_m1869,
	StringBuilder__ctor_m3639,
	StringBuilder__ctor_m1928,
	StringBuilder__ctor_m5665,
	StringBuilder__ctor_m9791,
	StringBuilder_System_Runtime_Serialization_ISerializable_GetObjectData_m9792,
	StringBuilder_get_Capacity_m9793,
	StringBuilder_set_Capacity_m9794,
	StringBuilder_get_Length_m4704,
	StringBuilder_set_Length_m5732,
	StringBuilder_get_Chars_m9795,
	StringBuilder_set_Chars_m9796,
	StringBuilder_ToString_m1873,
	StringBuilder_ToString_m9797,
	StringBuilder_Remove_m9798,
	StringBuilder_Replace_m9799,
	StringBuilder_Replace_m9800,
	StringBuilder_Append_m3642,
	StringBuilder_Append_m5694,
	StringBuilder_Append_m5678,
	StringBuilder_Append_m5666,
	StringBuilder_Append_m4672,
	StringBuilder_Append_m9801,
	StringBuilder_Append_m9802,
	StringBuilder_Append_m5711,
	StringBuilder_AppendLine_m1872,
	StringBuilder_AppendLine_m1871,
	StringBuilder_AppendFormat_m4640,
	StringBuilder_AppendFormat_m9803,
	StringBuilder_AppendFormat_m4639,
	StringBuilder_AppendFormat_m4638,
	StringBuilder_AppendFormat_m5703,
	StringBuilder_Insert_m9804,
	StringBuilder_Insert_m9805,
	StringBuilder_Insert_m9806,
	StringBuilder_InternalEnsureCapacity_m9807,
	UTF32Decoder__ctor_m9808,
	UTF32Decoder_GetChars_m9809,
	UTF32Encoding__ctor_m9810,
	UTF32Encoding__ctor_m9811,
	UTF32Encoding__ctor_m9812,
	UTF32Encoding_GetByteCount_m9813,
	UTF32Encoding_GetBytes_m9814,
	UTF32Encoding_GetCharCount_m9815,
	UTF32Encoding_GetChars_m9816,
	UTF32Encoding_GetMaxByteCount_m9817,
	UTF32Encoding_GetMaxCharCount_m9818,
	UTF32Encoding_GetDecoder_m9819,
	UTF32Encoding_GetPreamble_m9820,
	UTF32Encoding_Equals_m9821,
	UTF32Encoding_GetHashCode_m9822,
	UTF32Encoding_GetByteCount_m9823,
	UTF32Encoding_GetByteCount_m9824,
	UTF32Encoding_GetBytes_m9825,
	UTF32Encoding_GetBytes_m9826,
	UTF32Encoding_GetString_m9827,
	UTF7Decoder__ctor_m9828,
	UTF7Decoder_GetChars_m9829,
	UTF7Encoding__ctor_m9830,
	UTF7Encoding__ctor_m9831,
	UTF7Encoding__cctor_m9832,
	UTF7Encoding_GetHashCode_m9833,
	UTF7Encoding_Equals_m9834,
	UTF7Encoding_InternalGetByteCount_m9835,
	UTF7Encoding_GetByteCount_m9836,
	UTF7Encoding_InternalGetBytes_m9837,
	UTF7Encoding_GetBytes_m9838,
	UTF7Encoding_InternalGetCharCount_m9839,
	UTF7Encoding_GetCharCount_m9840,
	UTF7Encoding_InternalGetChars_m9841,
	UTF7Encoding_GetChars_m9842,
	UTF7Encoding_GetMaxByteCount_m9843,
	UTF7Encoding_GetMaxCharCount_m9844,
	UTF7Encoding_GetDecoder_m9845,
	UTF7Encoding_GetByteCount_m9846,
	UTF7Encoding_GetByteCount_m9847,
	UTF7Encoding_GetBytes_m9848,
	UTF7Encoding_GetBytes_m9849,
	UTF7Encoding_GetString_m9850,
	UTF8Decoder__ctor_m9851,
	UTF8Decoder_GetChars_m9852,
	UTF8Encoding__ctor_m9853,
	UTF8Encoding__ctor_m9854,
	UTF8Encoding__ctor_m9855,
	UTF8Encoding_InternalGetByteCount_m9856,
	UTF8Encoding_InternalGetByteCount_m9857,
	UTF8Encoding_GetByteCount_m9858,
	UTF8Encoding_GetByteCount_m9859,
	UTF8Encoding_InternalGetBytes_m9860,
	UTF8Encoding_InternalGetBytes_m9861,
	UTF8Encoding_GetBytes_m9862,
	UTF8Encoding_GetBytes_m9863,
	UTF8Encoding_GetBytes_m9864,
	UTF8Encoding_InternalGetCharCount_m9865,
	UTF8Encoding_InternalGetCharCount_m9866,
	UTF8Encoding_Fallback_m9867,
	UTF8Encoding_Fallback_m9868,
	UTF8Encoding_GetCharCount_m9869,
	UTF8Encoding_InternalGetChars_m9870,
	UTF8Encoding_InternalGetChars_m9871,
	UTF8Encoding_GetChars_m9872,
	UTF8Encoding_GetMaxByteCount_m9873,
	UTF8Encoding_GetMaxCharCount_m9874,
	UTF8Encoding_GetDecoder_m9875,
	UTF8Encoding_GetPreamble_m9876,
	UTF8Encoding_Equals_m9877,
	UTF8Encoding_GetHashCode_m9878,
	UTF8Encoding_GetByteCount_m9879,
	UTF8Encoding_GetString_m9880,
	UnicodeDecoder__ctor_m9881,
	UnicodeDecoder_GetChars_m9882,
	UnicodeEncoding__ctor_m9883,
	UnicodeEncoding__ctor_m9884,
	UnicodeEncoding__ctor_m9885,
	UnicodeEncoding_GetByteCount_m9886,
	UnicodeEncoding_GetByteCount_m9887,
	UnicodeEncoding_GetByteCount_m9888,
	UnicodeEncoding_GetBytes_m9889,
	UnicodeEncoding_GetBytes_m9890,
	UnicodeEncoding_GetBytes_m9891,
	UnicodeEncoding_GetBytesInternal_m9892,
	UnicodeEncoding_GetCharCount_m9893,
	UnicodeEncoding_GetChars_m9894,
	UnicodeEncoding_GetString_m9895,
	UnicodeEncoding_GetCharsInternal_m9896,
	UnicodeEncoding_GetMaxByteCount_m9897,
	UnicodeEncoding_GetMaxCharCount_m9898,
	UnicodeEncoding_GetDecoder_m9899,
	UnicodeEncoding_GetPreamble_m9900,
	UnicodeEncoding_Equals_m9901,
	UnicodeEncoding_GetHashCode_m9902,
	UnicodeEncoding_CopyChars_m9903,
	CompressedStack__ctor_m9904,
	CompressedStack__ctor_m9905,
	CompressedStack_CreateCopy_m9906,
	CompressedStack_Capture_m9907,
	CompressedStack_GetObjectData_m9908,
	CompressedStack_IsEmpty_m9909,
	EventWaitHandle__ctor_m9910,
	EventWaitHandle_IsManualReset_m9911,
	EventWaitHandle_Reset_m4738,
	EventWaitHandle_Set_m4736,
	ExecutionContext__ctor_m9912,
	ExecutionContext__ctor_m9913,
	ExecutionContext__ctor_m9914,
	ExecutionContext_Capture_m9915,
	ExecutionContext_GetObjectData_m9916,
	ExecutionContext_get_SecurityContext_m9917,
	ExecutionContext_set_SecurityContext_m9918,
	ExecutionContext_get_FlowSuppressed_m9919,
	ExecutionContext_IsFlowSuppressed_m9920,
	Interlocked_CompareExchange_m9921,
	ManualResetEvent__ctor_m4735,
	Monitor_Enter_m4719,
	Monitor_Exit_m4721,
	Monitor_Monitor_pulse_m9922,
	Monitor_Monitor_test_synchronised_m9923,
	Monitor_Pulse_m9924,
	Monitor_Monitor_wait_m9925,
	Monitor_Wait_m9926,
	Mutex__ctor_m9927,
	Mutex_CreateMutex_internal_m9928,
	Mutex_ReleaseMutex_internal_m9929,
	Mutex_ReleaseMutex_m9930,
	NativeEventCalls_CreateEvent_internal_m9931,
	NativeEventCalls_SetEvent_internal_m9932,
	NativeEventCalls_ResetEvent_internal_m9933,
	NativeEventCalls_CloseEvent_internal_m9934,
	SynchronizationLockException__ctor_m9935,
	SynchronizationLockException__ctor_m9936,
	SynchronizationLockException__ctor_m9937,
	Thread__ctor_m9938,
	Thread__cctor_m9939,
	Thread_get_CurrentContext_m9940,
	Thread_CurrentThread_internal_m9941,
	Thread_get_CurrentThread_m9942,
	Thread_FreeLocalSlotValues_m9943,
	Thread_GetDomainID_m9944,
	Thread_Thread_internal_m9945,
	Thread_Thread_init_m9946,
	Thread_GetCachedCurrentCulture_m9947,
	Thread_GetSerializedCurrentCulture_m9948,
	Thread_SetCachedCurrentCulture_m9949,
	Thread_GetCachedCurrentUICulture_m9950,
	Thread_GetSerializedCurrentUICulture_m9951,
	Thread_SetCachedCurrentUICulture_m9952,
	Thread_get_CurrentCulture_m9953,
	Thread_get_CurrentUICulture_m9954,
	Thread_set_IsBackground_m9955,
	Thread_SetName_internal_m9956,
	Thread_set_Name_m9957,
	Thread_Start_m9958,
	Thread_Thread_free_internal_m9959,
	Thread_Finalize_m9960,
	Thread_SetState_m9961,
	Thread_ClrState_m9962,
	Thread_GetNewManagedId_m9963,
	Thread_GetNewManagedId_internal_m9964,
	Thread_get_ExecutionContext_m9965,
	Thread_get_ManagedThreadId_m9966,
	Thread_GetHashCode_m9967,
	Thread_GetCompressedStack_m9968,
	ThreadAbortException__ctor_m9969,
	ThreadAbortException__ctor_m9970,
	ThreadInterruptedException__ctor_m9971,
	ThreadInterruptedException__ctor_m9972,
	ThreadPool_QueueUserWorkItem_m9973,
	ThreadStateException__ctor_m9974,
	ThreadStateException__ctor_m9975,
	TimerComparer__ctor_m9976,
	TimerComparer_Compare_m9977,
	Scheduler__ctor_m9978,
	Scheduler__cctor_m9979,
	Scheduler_get_Instance_m9980,
	Scheduler_Remove_m9981,
	Scheduler_Change_m9982,
	Scheduler_Add_m9983,
	Scheduler_InternalRemove_m9984,
	Scheduler_SchedulerThread_m9985,
	Scheduler_ShrinkIfNeeded_m9986,
	Timer__cctor_m9987,
	Timer_Change_m9988,
	Timer_Dispose_m9989,
	Timer_Change_m9990,
	WaitHandle__ctor_m9991,
	WaitHandle__cctor_m9992,
	WaitHandle_System_IDisposable_Dispose_m9993,
	WaitHandle_get_Handle_m9994,
	WaitHandle_set_Handle_m9995,
	WaitHandle_WaitOne_internal_m9996,
	WaitHandle_Dispose_m9997,
	WaitHandle_WaitOne_m9998,
	WaitHandle_WaitOne_m9999,
	WaitHandle_CheckDisposed_m10000,
	WaitHandle_Finalize_m10001,
	AccessViolationException__ctor_m10002,
	AccessViolationException__ctor_m10003,
	ActivationContext_System_Runtime_Serialization_ISerializable_GetObjectData_m10004,
	ActivationContext_Finalize_m10005,
	ActivationContext_Dispose_m10006,
	ActivationContext_Dispose_m10007,
	Activator_CreateInstance_m10008,
	Activator_CreateInstance_m10009,
	Activator_CreateInstance_m10010,
	Activator_CreateInstance_m10011,
	Activator_CreateInstance_m5687,
	Activator_CheckType_m10012,
	Activator_CheckAbstractType_m10013,
	Activator_CreateInstanceInternal_m10014,
	AppDomain_add_UnhandledException_m3581,
	AppDomain_remove_UnhandledException_m10015,
	AppDomain_getFriendlyName_m10016,
	AppDomain_getCurDomain_m10017,
	AppDomain_get_CurrentDomain_m3579,
	AppDomain_LoadAssembly_m10018,
	AppDomain_Load_m10019,
	AppDomain_Load_m10020,
	AppDomain_InternalSetContext_m10021,
	AppDomain_InternalGetContext_m10022,
	AppDomain_InternalGetDefaultContext_m10023,
	AppDomain_InternalGetProcessGuid_m10024,
	AppDomain_GetProcessGuid_m10025,
	AppDomain_ToString_m10026,
	AppDomain_DoTypeResolve_m10027,
	AppDomainSetup__ctor_m10028,
	ApplicationException__ctor_m10029,
	ApplicationException__ctor_m10030,
	ApplicationException__ctor_m10031,
	ApplicationIdentity_System_Runtime_Serialization_ISerializable_GetObjectData_m10032,
	ApplicationIdentity_ToString_m10033,
	ArgumentException__ctor_m10034,
	ArgumentException__ctor_m2361,
	ArgumentException__ctor_m5706,
	ArgumentException__ctor_m3732,
	ArgumentException__ctor_m10035,
	ArgumentException__ctor_m10036,
	ArgumentException_get_ParamName_m10037,
	ArgumentException_get_Message_m10038,
	ArgumentException_GetObjectData_m10039,
	ArgumentNullException__ctor_m10040,
	ArgumentNullException__ctor_m222,
	ArgumentNullException__ctor_m5653,
	ArgumentNullException__ctor_m10041,
	ArgumentOutOfRangeException__ctor_m5710,
	ArgumentOutOfRangeException__ctor_m4633,
	ArgumentOutOfRangeException__ctor_m3731,
	ArgumentOutOfRangeException__ctor_m10042,
	ArgumentOutOfRangeException__ctor_m10043,
	ArgumentOutOfRangeException_get_Message_m10044,
	ArgumentOutOfRangeException_GetObjectData_m10045,
	ArithmeticException__ctor_m10046,
	ArithmeticException__ctor_m4632,
	ArithmeticException__ctor_m10047,
	ArrayTypeMismatchException__ctor_m10048,
	ArrayTypeMismatchException__ctor_m10049,
	ArrayTypeMismatchException__ctor_m10050,
	BitConverter__cctor_m10051,
	BitConverter_AmILittleEndian_m10052,
	BitConverter_DoubleWordsAreSwapped_m10053,
	BitConverter_DoubleToInt64Bits_m10054,
	BitConverter_GetBytes_m10055,
	BitConverter_GetBytes_m10056,
	BitConverter_PutBytes_m10057,
	BitConverter_ToInt64_m10058,
	BitConverter_ToString_m4720,
	BitConverter_ToString_m10059,
	Buffer_ByteLength_m10060,
	Buffer_BlockCopy_m3727,
	Buffer_ByteLengthInternal_m10061,
	Buffer_BlockCopyInternal_m10062,
	CharEnumerator__ctor_m10063,
	CharEnumerator_System_Collections_IEnumerator_get_Current_m10064,
	CharEnumerator_System_IDisposable_Dispose_m10065,
	CharEnumerator_get_Current_m10066,
	CharEnumerator_Clone_m10067,
	CharEnumerator_MoveNext_m10068,
	CharEnumerator_Reset_m10069,
	Console__cctor_m10070,
	Console_SetEncodings_m10071,
	Console_get_Error_m5724,
	Console_Open_m10072,
	Console_OpenStandardError_m10073,
	Console_OpenStandardInput_m10074,
	Console_OpenStandardOutput_m10075,
	ContextBoundObject__ctor_m10076,
	Convert__cctor_m10077,
	Convert_InternalFromBase64String_m10078,
	Convert_FromBase64String_m4689,
	Convert_ToBase64String_m4663,
	Convert_ToBase64String_m10079,
	Convert_ToBoolean_m10080,
	Convert_ToBoolean_m10081,
	Convert_ToBoolean_m10082,
	Convert_ToBoolean_m10083,
	Convert_ToBoolean_m10084,
	Convert_ToBoolean_m10085,
	Convert_ToBoolean_m10086,
	Convert_ToBoolean_m10087,
	Convert_ToBoolean_m10088,
	Convert_ToBoolean_m10089,
	Convert_ToBoolean_m10090,
	Convert_ToBoolean_m10091,
	Convert_ToBoolean_m10092,
	Convert_ToBoolean_m10093,
	Convert_ToByte_m10094,
	Convert_ToByte_m10095,
	Convert_ToByte_m10096,
	Convert_ToByte_m10097,
	Convert_ToByte_m10098,
	Convert_ToByte_m10099,
	Convert_ToByte_m10100,
	Convert_ToByte_m10101,
	Convert_ToByte_m10102,
	Convert_ToByte_m10103,
	Convert_ToByte_m10104,
	Convert_ToByte_m10105,
	Convert_ToByte_m10106,
	Convert_ToByte_m10107,
	Convert_ToByte_m10108,
	Convert_ToChar_m4694,
	Convert_ToChar_m10109,
	Convert_ToChar_m10110,
	Convert_ToChar_m10111,
	Convert_ToChar_m10112,
	Convert_ToChar_m10113,
	Convert_ToChar_m10114,
	Convert_ToChar_m10115,
	Convert_ToChar_m10116,
	Convert_ToChar_m10117,
	Convert_ToChar_m10118,
	Convert_ToDateTime_m10119,
	Convert_ToDateTime_m10120,
	Convert_ToDateTime_m10121,
	Convert_ToDateTime_m10122,
	Convert_ToDateTime_m10123,
	Convert_ToDateTime_m10124,
	Convert_ToDateTime_m10125,
	Convert_ToDateTime_m10126,
	Convert_ToDateTime_m10127,
	Convert_ToDateTime_m10128,
	Convert_ToDecimal_m10129,
	Convert_ToDecimal_m10130,
	Convert_ToDecimal_m10131,
	Convert_ToDecimal_m10132,
	Convert_ToDecimal_m10133,
	Convert_ToDecimal_m10134,
	Convert_ToDecimal_m10135,
	Convert_ToDecimal_m10136,
	Convert_ToDecimal_m10137,
	Convert_ToDecimal_m10138,
	Convert_ToDecimal_m10139,
	Convert_ToDecimal_m10140,
	Convert_ToDecimal_m10141,
	Convert_ToDouble_m10142,
	Convert_ToDouble_m10143,
	Convert_ToDouble_m10144,
	Convert_ToDouble_m10145,
	Convert_ToDouble_m10146,
	Convert_ToDouble_m10147,
	Convert_ToDouble_m10148,
	Convert_ToDouble_m10149,
	Convert_ToDouble_m10150,
	Convert_ToDouble_m10151,
	Convert_ToDouble_m10152,
	Convert_ToDouble_m10153,
	Convert_ToDouble_m10154,
	Convert_ToDouble_m10155,
	Convert_ToInt16_m10156,
	Convert_ToInt16_m10157,
	Convert_ToInt16_m10158,
	Convert_ToInt16_m10159,
	Convert_ToInt16_m10160,
	Convert_ToInt16_m10161,
	Convert_ToInt16_m10162,
	Convert_ToInt16_m10163,
	Convert_ToInt16_m10164,
	Convert_ToInt16_m10165,
	Convert_ToInt16_m4648,
	Convert_ToInt16_m10166,
	Convert_ToInt16_m10167,
	Convert_ToInt16_m10168,
	Convert_ToInt16_m10169,
	Convert_ToInt16_m10170,
	Convert_ToInt32_m10171,
	Convert_ToInt32_m10172,
	Convert_ToInt32_m10173,
	Convert_ToInt32_m10174,
	Convert_ToInt32_m10175,
	Convert_ToInt32_m10176,
	Convert_ToInt32_m10177,
	Convert_ToInt32_m10178,
	Convert_ToInt32_m10179,
	Convert_ToInt32_m10180,
	Convert_ToInt32_m10181,
	Convert_ToInt32_m10182,
	Convert_ToInt32_m10183,
	Convert_ToInt32_m10184,
	Convert_ToInt32_m4703,
	Convert_ToInt64_m10185,
	Convert_ToInt64_m10186,
	Convert_ToInt64_m10187,
	Convert_ToInt64_m10188,
	Convert_ToInt64_m10189,
	Convert_ToInt64_m10190,
	Convert_ToInt64_m10191,
	Convert_ToInt64_m10192,
	Convert_ToInt64_m10193,
	Convert_ToInt64_m10194,
	Convert_ToInt64_m10195,
	Convert_ToInt64_m10196,
	Convert_ToInt64_m10197,
	Convert_ToInt64_m10198,
	Convert_ToInt64_m10199,
	Convert_ToInt64_m10200,
	Convert_ToInt64_m10201,
	Convert_ToSByte_m10202,
	Convert_ToSByte_m10203,
	Convert_ToSByte_m10204,
	Convert_ToSByte_m10205,
	Convert_ToSByte_m10206,
	Convert_ToSByte_m10207,
	Convert_ToSByte_m10208,
	Convert_ToSByte_m10209,
	Convert_ToSByte_m10210,
	Convert_ToSByte_m10211,
	Convert_ToSByte_m10212,
	Convert_ToSByte_m10213,
	Convert_ToSByte_m10214,
	Convert_ToSByte_m10215,
	Convert_ToSingle_m10216,
	Convert_ToSingle_m10217,
	Convert_ToSingle_m10218,
	Convert_ToSingle_m10219,
	Convert_ToSingle_m10220,
	Convert_ToSingle_m10221,
	Convert_ToSingle_m10222,
	Convert_ToSingle_m10223,
	Convert_ToSingle_m10224,
	Convert_ToSingle_m10225,
	Convert_ToSingle_m10226,
	Convert_ToSingle_m10227,
	Convert_ToSingle_m10228,
	Convert_ToSingle_m10229,
	Convert_ToString_m10230,
	Convert_ToString_m10231,
	Convert_ToUInt16_m10232,
	Convert_ToUInt16_m10233,
	Convert_ToUInt16_m10234,
	Convert_ToUInt16_m10235,
	Convert_ToUInt16_m10236,
	Convert_ToUInt16_m10237,
	Convert_ToUInt16_m10238,
	Convert_ToUInt16_m10239,
	Convert_ToUInt16_m10240,
	Convert_ToUInt16_m10241,
	Convert_ToUInt16_m10242,
	Convert_ToUInt16_m10243,
	Convert_ToUInt16_m10244,
	Convert_ToUInt16_m10245,
	Convert_ToUInt32_m3598,
	Convert_ToUInt32_m10246,
	Convert_ToUInt32_m10247,
	Convert_ToUInt32_m10248,
	Convert_ToUInt32_m10249,
	Convert_ToUInt32_m10250,
	Convert_ToUInt32_m10251,
	Convert_ToUInt32_m10252,
	Convert_ToUInt32_m10253,
	Convert_ToUInt32_m10254,
	Convert_ToUInt32_m10255,
	Convert_ToUInt32_m10256,
	Convert_ToUInt32_m10257,
	Convert_ToUInt32_m3597,
	Convert_ToUInt32_m10258,
	Convert_ToUInt64_m10259,
	Convert_ToUInt64_m10260,
	Convert_ToUInt64_m10261,
	Convert_ToUInt64_m10262,
	Convert_ToUInt64_m10263,
	Convert_ToUInt64_m10264,
	Convert_ToUInt64_m10265,
	Convert_ToUInt64_m10266,
	Convert_ToUInt64_m10267,
	Convert_ToUInt64_m10268,
	Convert_ToUInt64_m10269,
	Convert_ToUInt64_m10270,
	Convert_ToUInt64_m10271,
	Convert_ToUInt64_m10272,
	Convert_ToUInt64_m10273,
	Convert_ChangeType_m10274,
	Convert_ToType_m10275,
	DBNull__ctor_m10276,
	DBNull__ctor_m10277,
	DBNull__cctor_m10278,
	DBNull_System_IConvertible_ToBoolean_m10279,
	DBNull_System_IConvertible_ToByte_m10280,
	DBNull_System_IConvertible_ToChar_m10281,
	DBNull_System_IConvertible_ToDateTime_m10282,
	DBNull_System_IConvertible_ToDecimal_m10283,
	DBNull_System_IConvertible_ToDouble_m10284,
	DBNull_System_IConvertible_ToInt16_m10285,
	DBNull_System_IConvertible_ToInt32_m10286,
	DBNull_System_IConvertible_ToInt64_m10287,
	DBNull_System_IConvertible_ToSByte_m10288,
	DBNull_System_IConvertible_ToSingle_m10289,
	DBNull_System_IConvertible_ToType_m10290,
	DBNull_System_IConvertible_ToUInt16_m10291,
	DBNull_System_IConvertible_ToUInt32_m10292,
	DBNull_System_IConvertible_ToUInt64_m10293,
	DBNull_GetObjectData_m10294,
	DBNull_ToString_m10295,
	DBNull_ToString_m10296,
	DateTime__ctor_m10297,
	DateTime__ctor_m10298,
	DateTime__ctor_m3633,
	DateTime__ctor_m10299,
	DateTime__ctor_m10300,
	DateTime__cctor_m10301,
	DateTime_System_IConvertible_ToBoolean_m10302,
	DateTime_System_IConvertible_ToByte_m10303,
	DateTime_System_IConvertible_ToChar_m10304,
	DateTime_System_IConvertible_ToDateTime_m10305,
	DateTime_System_IConvertible_ToDecimal_m10306,
	DateTime_System_IConvertible_ToDouble_m10307,
	DateTime_System_IConvertible_ToInt16_m10308,
	DateTime_System_IConvertible_ToInt32_m10309,
	DateTime_System_IConvertible_ToInt64_m10310,
	DateTime_System_IConvertible_ToSByte_m10311,
	DateTime_System_IConvertible_ToSingle_m10312,
	DateTime_System_IConvertible_ToType_m10313,
	DateTime_System_IConvertible_ToUInt16_m10314,
	DateTime_System_IConvertible_ToUInt32_m10315,
	DateTime_System_IConvertible_ToUInt64_m10316,
	DateTime_AbsoluteDays_m10317,
	DateTime_FromTicks_m10318,
	DateTime_get_Month_m10319,
	DateTime_get_Day_m10320,
	DateTime_get_DayOfWeek_m10321,
	DateTime_get_Hour_m10322,
	DateTime_get_Minute_m10323,
	DateTime_get_Second_m10324,
	DateTime_GetTimeMonotonic_m10325,
	DateTime_GetNow_m10326,
	DateTime_get_Now_m3618,
	DateTime_get_Ticks_m4722,
	DateTime_get_Today_m10327,
	DateTime_get_UtcNow_m4683,
	DateTime_get_Year_m10328,
	DateTime_get_Kind_m10329,
	DateTime_Add_m10330,
	DateTime_AddTicks_m10331,
	DateTime_AddMilliseconds_m5679,
	DateTime_AddSeconds_m3634,
	DateTime_Compare_m10332,
	DateTime_CompareTo_m10333,
	DateTime_CompareTo_m10334,
	DateTime_Equals_m10335,
	DateTime_FromBinary_m10336,
	DateTime_SpecifyKind_m10337,
	DateTime_DaysInMonth_m10338,
	DateTime_Equals_m10339,
	DateTime_CheckDateTimeKind_m10340,
	DateTime_GetHashCode_m10341,
	DateTime_IsLeapYear_m10342,
	DateTime_Parse_m10343,
	DateTime_Parse_m10344,
	DateTime_CoreParse_m10345,
	DateTime_YearMonthDayFormats_m10346,
	DateTime__ParseNumber_m10347,
	DateTime__ParseEnum_m10348,
	DateTime__ParseString_m10349,
	DateTime__ParseAmPm_m10350,
	DateTime__ParseTimeSeparator_m10351,
	DateTime__ParseDateSeparator_m10352,
	DateTime_IsLetter_m10353,
	DateTime__DoParse_m10354,
	DateTime_ParseExact_m4649,
	DateTime_ParseExact_m10355,
	DateTime_CheckStyle_m10356,
	DateTime_ParseExact_m10357,
	DateTime_Subtract_m10358,
	DateTime_ToString_m10359,
	DateTime_ToString_m10360,
	DateTime_ToString_m10361,
	DateTime_ToLocalTime_m5696,
	DateTime_ToUniversalTime_m10362,
	DateTime_op_Addition_m10363,
	DateTime_op_Equality_m10364,
	DateTime_op_GreaterThan_m4684,
	DateTime_op_GreaterThanOrEqual_m5680,
	DateTime_op_Inequality_m10365,
	DateTime_op_LessThan_m5705,
	DateTime_op_LessThanOrEqual_m4685,
	DateTime_op_Subtraction_m10366,
	DateTimeOffset__ctor_m10367,
	DateTimeOffset__ctor_m10368,
	DateTimeOffset__ctor_m10369,
	DateTimeOffset__ctor_m10370,
	DateTimeOffset__cctor_m10371,
	DateTimeOffset_System_IComparable_CompareTo_m10372,
	DateTimeOffset_System_Runtime_Serialization_ISerializable_GetObjectData_m10373,
	DateTimeOffset_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10374,
	DateTimeOffset_CompareTo_m10375,
	DateTimeOffset_Equals_m10376,
	DateTimeOffset_Equals_m10377,
	DateTimeOffset_GetHashCode_m10378,
	DateTimeOffset_ToString_m10379,
	DateTimeOffset_ToString_m10380,
	DateTimeOffset_get_DateTime_m10381,
	DateTimeOffset_get_Offset_m10382,
	DateTimeOffset_get_UtcDateTime_m10383,
	DateTimeUtils_CountRepeat_m10384,
	DateTimeUtils_ZeroPad_m10385,
	DateTimeUtils_ParseQuotedString_m10386,
	DateTimeUtils_GetStandardPattern_m10387,
	DateTimeUtils_GetStandardPattern_m10388,
	DateTimeUtils_ToString_m10389,
	DateTimeUtils_ToString_m10390,
	DelegateEntry__ctor_m10391,
	DelegateEntry_DeserializeDelegate_m10392,
	DelegateSerializationHolder__ctor_m10393,
	DelegateSerializationHolder_GetDelegateData_m10394,
	DelegateSerializationHolder_GetObjectData_m10395,
	DelegateSerializationHolder_GetRealObject_m10396,
	DivideByZeroException__ctor_m10397,
	DivideByZeroException__ctor_m10398,
	DllNotFoundException__ctor_m10399,
	DllNotFoundException__ctor_m10400,
	EntryPointNotFoundException__ctor_m10401,
	EntryPointNotFoundException__ctor_m10402,
	SByteComparer__ctor_m10403,
	SByteComparer_Compare_m10404,
	SByteComparer_Compare_m10405,
	ShortComparer__ctor_m10406,
	ShortComparer_Compare_m10407,
	ShortComparer_Compare_m10408,
	IntComparer__ctor_m10409,
	IntComparer_Compare_m10410,
	IntComparer_Compare_m10411,
	LongComparer__ctor_m10412,
	LongComparer_Compare_m10413,
	LongComparer_Compare_m10414,
	MonoEnumInfo__ctor_m10415,
	MonoEnumInfo__cctor_m10416,
	MonoEnumInfo_get_enum_info_m10417,
	MonoEnumInfo_get_Cache_m10418,
	MonoEnumInfo_GetInfo_m10419,
	Environment_get_SocketSecurityEnabled_m10420,
	Environment_get_NewLine_m4637,
	Environment_get_Platform_m10421,
	Environment_GetOSVersionString_m10422,
	Environment_get_OSVersion_m10423,
	Environment_internalGetEnvironmentVariable_m10424,
	Environment_GetEnvironmentVariable_m4716,
	Environment_GetWindowsFolderPath_m10425,
	Environment_GetFolderPath_m4700,
	Environment_ReadXdgUserDir_m10426,
	Environment_InternalGetFolderPath_m10427,
	Environment_get_IsRunningOnWindows_m10428,
	Environment_GetMachineConfigPath_m10429,
	Environment_internalGetHome_m10430,
	EventArgs__ctor_m10431,
	EventArgs__cctor_m10432,
	ExecutionEngineException__ctor_m10433,
	ExecutionEngineException__ctor_m10434,
	FieldAccessException__ctor_m10435,
	FieldAccessException__ctor_m10436,
	FieldAccessException__ctor_m10437,
	FlagsAttribute__ctor_m10438,
	FormatException__ctor_m10439,
	FormatException__ctor_m4643,
	FormatException__ctor_m5734,
	GC_SuppressFinalize_m3728,
	Guid__ctor_m10440,
	Guid__ctor_m10441,
	Guid__cctor_m10442,
	Guid_CheckNull_m10443,
	Guid_CheckLength_m10444,
	Guid_CheckArray_m10445,
	Guid_Compare_m10446,
	Guid_CompareTo_m10447,
	Guid_Equals_m10448,
	Guid_CompareTo_m10449,
	Guid_Equals_m10450,
	Guid_GetHashCode_m10451,
	Guid_ToHex_m10452,
	Guid_NewGuid_m10453,
	Guid_AppendInt_m10454,
	Guid_AppendShort_m10455,
	Guid_AppendByte_m10456,
	Guid_BaseToString_m10457,
	Guid_ToString_m10458,
	Guid_ToString_m10459,
	Guid_ToString_m10460,
	IndexOutOfRangeException__ctor_m10461,
	IndexOutOfRangeException__ctor_m3599,
	IndexOutOfRangeException__ctor_m10462,
	InvalidCastException__ctor_m10463,
	InvalidCastException__ctor_m10464,
	InvalidCastException__ctor_m10465,
	InvalidOperationException__ctor_m5656,
	InvalidOperationException__ctor_m4713,
	InvalidOperationException__ctor_m10466,
	InvalidOperationException__ctor_m10467,
	LocalDataStoreSlot__ctor_m10468,
	LocalDataStoreSlot__cctor_m10469,
	LocalDataStoreSlot_Finalize_m10470,
	Math_Abs_m10471,
	Math_Abs_m10472,
	Math_Abs_m10473,
	Math_Ceiling_m10474,
	Math_Floor_m10475,
	Math_Log_m3601,
	Math_Max_m3614,
	Math_Min_m3726,
	Math_Round_m10476,
	Math_Round_m10477,
	Math_Sin_m10478,
	Math_Cos_m10479,
	Math_Acos_m10480,
	Math_Atan2_m10481,
	Math_Log_m10482,
	Math_Pow_m10483,
	Math_Sqrt_m10484,
	MemberAccessException__ctor_m10485,
	MemberAccessException__ctor_m10486,
	MemberAccessException__ctor_m10487,
	MethodAccessException__ctor_m10488,
	MethodAccessException__ctor_m10489,
	MissingFieldException__ctor_m10490,
	MissingFieldException__ctor_m10491,
	MissingFieldException__ctor_m10492,
	MissingFieldException_get_Message_m10493,
	MissingMemberException__ctor_m10494,
	MissingMemberException__ctor_m10495,
	MissingMemberException__ctor_m10496,
	MissingMemberException__ctor_m10497,
	MissingMemberException_GetObjectData_m10498,
	MissingMemberException_get_Message_m10499,
	MissingMethodException__ctor_m10500,
	MissingMethodException__ctor_m10501,
	MissingMethodException__ctor_m10502,
	MissingMethodException__ctor_m10503,
	MissingMethodException_get_Message_m10504,
	MonoAsyncCall__ctor_m10505,
	AttributeInfo__ctor_m10506,
	AttributeInfo_get_Usage_m10507,
	AttributeInfo_get_InheritanceLevel_m10508,
	MonoCustomAttrs__cctor_m10509,
	MonoCustomAttrs_IsUserCattrProvider_m10510,
	MonoCustomAttrs_GetCustomAttributesInternal_m10511,
	MonoCustomAttrs_GetPseudoCustomAttributes_m10512,
	MonoCustomAttrs_GetCustomAttributesBase_m10513,
	MonoCustomAttrs_GetCustomAttribute_m10514,
	MonoCustomAttrs_GetCustomAttributes_m10515,
	MonoCustomAttrs_GetCustomAttributes_m10516,
	MonoCustomAttrs_GetCustomAttributesDataInternal_m10517,
	MonoCustomAttrs_GetCustomAttributesData_m10518,
	MonoCustomAttrs_IsDefined_m10519,
	MonoCustomAttrs_IsDefinedInternal_m10520,
	MonoCustomAttrs_GetBasePropertyDefinition_m10521,
	MonoCustomAttrs_GetBase_m10522,
	MonoCustomAttrs_RetrieveAttributeUsage_m10523,
	MonoTouchAOTHelper__cctor_m10524,
	MonoTypeInfo__ctor_m10525,
	MonoType_get_attributes_m10526,
	MonoType_GetDefaultConstructor_m10527,
	MonoType_GetAttributeFlagsImpl_m10528,
	MonoType_GetConstructorImpl_m10529,
	MonoType_GetConstructors_internal_m10530,
	MonoType_GetConstructors_m10531,
	MonoType_InternalGetEvent_m10532,
	MonoType_GetEvent_m10533,
	MonoType_GetField_m10534,
	MonoType_GetFields_internal_m10535,
	MonoType_GetFields_m10536,
	MonoType_GetInterfaces_m10537,
	MonoType_GetMethodsByName_m10538,
	MonoType_GetMethods_m10539,
	MonoType_GetMethodImpl_m10540,
	MonoType_GetPropertiesByName_m10541,
	MonoType_GetPropertyImpl_m10542,
	MonoType_HasElementTypeImpl_m10543,
	MonoType_IsArrayImpl_m10544,
	MonoType_IsByRefImpl_m10545,
	MonoType_IsPointerImpl_m10546,
	MonoType_IsPrimitiveImpl_m10547,
	MonoType_IsSubclassOf_m10548,
	MonoType_InvokeMember_m10549,
	MonoType_GetElementType_m10550,
	MonoType_get_UnderlyingSystemType_m10551,
	MonoType_get_Assembly_m10552,
	MonoType_get_AssemblyQualifiedName_m10553,
	MonoType_getFullName_m10554,
	MonoType_get_BaseType_m10555,
	MonoType_get_FullName_m10556,
	MonoType_IsDefined_m10557,
	MonoType_GetCustomAttributes_m10558,
	MonoType_GetCustomAttributes_m10559,
	MonoType_get_MemberType_m10560,
	MonoType_get_Name_m10561,
	MonoType_get_Namespace_m10562,
	MonoType_get_Module_m10563,
	MonoType_get_DeclaringType_m10564,
	MonoType_get_ReflectedType_m10565,
	MonoType_get_TypeHandle_m10566,
	MonoType_GetObjectData_m10567,
	MonoType_ToString_m10568,
	MonoType_GetGenericArguments_m10569,
	MonoType_get_ContainsGenericParameters_m10570,
	MonoType_get_IsGenericParameter_m10571,
	MonoType_GetGenericTypeDefinition_m10572,
	MonoType_CheckMethodSecurity_m10573,
	MonoType_ReorderParamArrayArguments_m10574,
	MulticastNotSupportedException__ctor_m10575,
	MulticastNotSupportedException__ctor_m10576,
	MulticastNotSupportedException__ctor_m10577,
	NonSerializedAttribute__ctor_m10578,
	NotImplementedException__ctor_m10579,
	NotImplementedException__ctor_m3730,
	NotImplementedException__ctor_m10580,
	NotSupportedException__ctor_m333,
	NotSupportedException__ctor_m4634,
	NotSupportedException__ctor_m10581,
	NullReferenceException__ctor_m10582,
	NullReferenceException__ctor_m3577,
	NullReferenceException__ctor_m10583,
	CustomInfo__ctor_m10584,
	CustomInfo_GetActiveSection_m10585,
	CustomInfo_Parse_m10586,
	CustomInfo_Format_m10587,
	NumberFormatter__ctor_m10588,
	NumberFormatter__cctor_m10589,
	NumberFormatter_GetFormatterTables_m10590,
	NumberFormatter_GetTenPowerOf_m10591,
	NumberFormatter_InitDecHexDigits_m10592,
	NumberFormatter_InitDecHexDigits_m10593,
	NumberFormatter_InitDecHexDigits_m10594,
	NumberFormatter_FastToDecHex_m10595,
	NumberFormatter_ToDecHex_m10596,
	NumberFormatter_FastDecHexLen_m10597,
	NumberFormatter_DecHexLen_m10598,
	NumberFormatter_DecHexLen_m10599,
	NumberFormatter_ScaleOrder_m10600,
	NumberFormatter_InitialFloatingPrecision_m10601,
	NumberFormatter_ParsePrecision_m10602,
	NumberFormatter_Init_m10603,
	NumberFormatter_InitHex_m10604,
	NumberFormatter_Init_m10605,
	NumberFormatter_Init_m10606,
	NumberFormatter_Init_m10607,
	NumberFormatter_Init_m10608,
	NumberFormatter_Init_m10609,
	NumberFormatter_Init_m10610,
	NumberFormatter_ResetCharBuf_m10611,
	NumberFormatter_Resize_m10612,
	NumberFormatter_Append_m10613,
	NumberFormatter_Append_m10614,
	NumberFormatter_Append_m10615,
	NumberFormatter_GetNumberFormatInstance_m10616,
	NumberFormatter_set_CurrentCulture_m10617,
	NumberFormatter_get_IntegerDigits_m10618,
	NumberFormatter_get_DecimalDigits_m10619,
	NumberFormatter_get_IsFloatingSource_m10620,
	NumberFormatter_get_IsZero_m10621,
	NumberFormatter_get_IsZeroInteger_m10622,
	NumberFormatter_RoundPos_m10623,
	NumberFormatter_RoundDecimal_m10624,
	NumberFormatter_RoundBits_m10625,
	NumberFormatter_RemoveTrailingZeros_m10626,
	NumberFormatter_AddOneToDecHex_m10627,
	NumberFormatter_AddOneToDecHex_m10628,
	NumberFormatter_CountTrailingZeros_m10629,
	NumberFormatter_CountTrailingZeros_m10630,
	NumberFormatter_GetInstance_m10631,
	NumberFormatter_Release_m10632,
	NumberFormatter_SetThreadCurrentCulture_m10633,
	NumberFormatter_NumberToString_m10634,
	NumberFormatter_NumberToString_m10635,
	NumberFormatter_NumberToString_m10636,
	NumberFormatter_NumberToString_m10637,
	NumberFormatter_NumberToString_m10638,
	NumberFormatter_NumberToString_m10639,
	NumberFormatter_NumberToString_m10640,
	NumberFormatter_NumberToString_m10641,
	NumberFormatter_NumberToString_m10642,
	NumberFormatter_NumberToString_m10643,
	NumberFormatter_NumberToString_m10644,
	NumberFormatter_NumberToString_m10645,
	NumberFormatter_NumberToString_m10646,
	NumberFormatter_NumberToString_m10647,
	NumberFormatter_NumberToString_m10648,
	NumberFormatter_NumberToString_m10649,
	NumberFormatter_NumberToString_m10650,
	NumberFormatter_FastIntegerToString_m10651,
	NumberFormatter_IntegerToString_m10652,
	NumberFormatter_NumberToString_m10653,
	NumberFormatter_FormatCurrency_m10654,
	NumberFormatter_FormatDecimal_m10655,
	NumberFormatter_FormatHexadecimal_m10656,
	NumberFormatter_FormatFixedPoint_m10657,
	NumberFormatter_FormatRoundtrip_m10658,
	NumberFormatter_FormatRoundtrip_m10659,
	NumberFormatter_FormatGeneral_m10660,
	NumberFormatter_FormatNumber_m10661,
	NumberFormatter_FormatPercent_m10662,
	NumberFormatter_FormatExponential_m10663,
	NumberFormatter_FormatExponential_m10664,
	NumberFormatter_FormatCustom_m10665,
	NumberFormatter_ZeroTrimEnd_m10666,
	NumberFormatter_IsZeroOnly_m10667,
	NumberFormatter_AppendNonNegativeNumber_m10668,
	NumberFormatter_AppendIntegerString_m10669,
	NumberFormatter_AppendIntegerString_m10670,
	NumberFormatter_AppendDecimalString_m10671,
	NumberFormatter_AppendDecimalString_m10672,
	NumberFormatter_AppendIntegerStringWithGroupSeparator_m10673,
	NumberFormatter_AppendExponent_m10674,
	NumberFormatter_AppendOneDigit_m10675,
	NumberFormatter_FastAppendDigits_m10676,
	NumberFormatter_AppendDigits_m10677,
	NumberFormatter_AppendDigits_m10678,
	NumberFormatter_Multiply10_m10679,
	NumberFormatter_Divide10_m10680,
	NumberFormatter_GetClone_m10681,
	ObjectDisposedException__ctor_m3733,
	ObjectDisposedException__ctor_m10682,
	ObjectDisposedException__ctor_m10683,
	ObjectDisposedException_get_Message_m10684,
	ObjectDisposedException_GetObjectData_m10685,
	OperatingSystem__ctor_m10686,
	OperatingSystem_get_Platform_m10687,
	OperatingSystem_Clone_m10688,
	OperatingSystem_GetObjectData_m10689,
	OperatingSystem_ToString_m10690,
	OutOfMemoryException__ctor_m10691,
	OutOfMemoryException__ctor_m10692,
	OverflowException__ctor_m10693,
	OverflowException__ctor_m10694,
	OverflowException__ctor_m10695,
	RankException__ctor_m10696,
	RankException__ctor_m10697,
	RankException__ctor_m10698,
	ResolveEventArgs__ctor_m10699,
	RuntimeMethodHandle__ctor_m10700,
	RuntimeMethodHandle__ctor_m10701,
	RuntimeMethodHandle_get_Value_m10702,
	RuntimeMethodHandle_GetObjectData_m10703,
	RuntimeMethodHandle_Equals_m10704,
	RuntimeMethodHandle_GetHashCode_m10705,
	StringComparer__ctor_m10706,
	StringComparer__cctor_m10707,
	StringComparer_get_InvariantCultureIgnoreCase_m5683,
	StringComparer_get_OrdinalIgnoreCase_m3624,
	StringComparer_Compare_m10708,
	StringComparer_Equals_m10709,
	StringComparer_GetHashCode_m10710,
	CultureAwareComparer__ctor_m10711,
	CultureAwareComparer_Compare_m10712,
	CultureAwareComparer_Equals_m10713,
	CultureAwareComparer_GetHashCode_m10714,
	OrdinalComparer__ctor_m10715,
	OrdinalComparer_Compare_m10716,
	OrdinalComparer_Equals_m10717,
	OrdinalComparer_GetHashCode_m10718,
	SystemException__ctor_m10719,
	SystemException__ctor_m5712,
	SystemException__ctor_m10720,
	SystemException__ctor_m10721,
	ThreadStaticAttribute__ctor_m10722,
	TimeSpan__ctor_m10723,
	TimeSpan__ctor_m10724,
	TimeSpan__ctor_m10725,
	TimeSpan__cctor_m10726,
	TimeSpan_CalculateTicks_m10727,
	TimeSpan_get_Days_m10728,
	TimeSpan_get_Hours_m10729,
	TimeSpan_get_Milliseconds_m10730,
	TimeSpan_get_Minutes_m10731,
	TimeSpan_get_Seconds_m10732,
	TimeSpan_get_Ticks_m10733,
	TimeSpan_get_TotalDays_m10734,
	TimeSpan_get_TotalHours_m10735,
	TimeSpan_get_TotalMilliseconds_m10736,
	TimeSpan_get_TotalMinutes_m10737,
	TimeSpan_get_TotalSeconds_m10738,
	TimeSpan_Add_m10739,
	TimeSpan_Compare_m10740,
	TimeSpan_CompareTo_m10741,
	TimeSpan_CompareTo_m10742,
	TimeSpan_Equals_m10743,
	TimeSpan_Duration_m10744,
	TimeSpan_Equals_m10745,
	TimeSpan_FromDays_m10746,
	TimeSpan_FromHours_m10747,
	TimeSpan_FromMinutes_m10748,
	TimeSpan_FromSeconds_m10749,
	TimeSpan_FromMilliseconds_m10750,
	TimeSpan_From_m10751,
	TimeSpan_GetHashCode_m10752,
	TimeSpan_Negate_m10753,
	TimeSpan_Subtract_m10754,
	TimeSpan_ToString_m10755,
	TimeSpan_op_Addition_m10756,
	TimeSpan_op_Equality_m10757,
	TimeSpan_op_GreaterThan_m10758,
	TimeSpan_op_GreaterThanOrEqual_m10759,
	TimeSpan_op_Inequality_m10760,
	TimeSpan_op_LessThan_m10761,
	TimeSpan_op_LessThanOrEqual_m10762,
	TimeSpan_op_Subtraction_m10763,
	TimeZone__ctor_m10764,
	TimeZone__cctor_m10765,
	TimeZone_get_CurrentTimeZone_m10766,
	TimeZone_IsDaylightSavingTime_m10767,
	TimeZone_IsDaylightSavingTime_m10768,
	TimeZone_ToLocalTime_m10769,
	TimeZone_ToUniversalTime_m10770,
	TimeZone_GetLocalTimeDiff_m10771,
	TimeZone_GetLocalTimeDiff_m10772,
	CurrentSystemTimeZone__ctor_m10773,
	CurrentSystemTimeZone__ctor_m10774,
	CurrentSystemTimeZone_System_Runtime_Serialization_IDeserializationCallback_OnDeserialization_m10775,
	CurrentSystemTimeZone_GetTimeZoneData_m10776,
	CurrentSystemTimeZone_GetDaylightChanges_m10777,
	CurrentSystemTimeZone_GetUtcOffset_m10778,
	CurrentSystemTimeZone_OnDeserialization_m10779,
	CurrentSystemTimeZone_GetDaylightTimeFromData_m10780,
	TypeInitializationException__ctor_m10781,
	TypeInitializationException_GetObjectData_m10782,
	TypeLoadException__ctor_m10783,
	TypeLoadException__ctor_m10784,
	TypeLoadException__ctor_m10785,
	TypeLoadException_get_Message_m10786,
	TypeLoadException_GetObjectData_m10787,
	UnauthorizedAccessException__ctor_m10788,
	UnauthorizedAccessException__ctor_m10789,
	UnauthorizedAccessException__ctor_m10790,
	UnhandledExceptionEventArgs__ctor_m10791,
	UnhandledExceptionEventArgs_get_ExceptionObject_m3582,
	UnhandledExceptionEventArgs_get_IsTerminating_m10792,
	UnitySerializationHolder__ctor_m10793,
	UnitySerializationHolder_GetTypeData_m10794,
	UnitySerializationHolder_GetDBNullData_m10795,
	UnitySerializationHolder_GetModuleData_m10796,
	UnitySerializationHolder_GetObjectData_m10797,
	UnitySerializationHolder_GetRealObject_m10798,
	Version__ctor_m10799,
	Version__ctor_m10800,
	Version__ctor_m5671,
	Version__ctor_m10801,
	Version__ctor_m10802,
	Version_CheckedSet_m10803,
	Version_get_Build_m10804,
	Version_get_Major_m10805,
	Version_get_Minor_m10806,
	Version_get_Revision_m10807,
	Version_Clone_m10808,
	Version_CompareTo_m10809,
	Version_Equals_m10810,
	Version_CompareTo_m10811,
	Version_Equals_m10812,
	Version_GetHashCode_m10813,
	Version_ToString_m10814,
	Version_CreateFromString_m10815,
	Version_op_Equality_m10816,
	Version_op_Inequality_m10817,
	WeakReference__ctor_m10818,
	WeakReference__ctor_m10819,
	WeakReference__ctor_m10820,
	WeakReference__ctor_m10821,
	WeakReference_AllocateHandle_m10822,
	WeakReference_get_Target_m10823,
	WeakReference_get_TrackResurrection_m10824,
	WeakReference_Finalize_m10825,
	WeakReference_GetObjectData_m10826,
	PrimalityTest__ctor_m10827,
	PrimalityTest_Invoke_m10828,
	PrimalityTest_BeginInvoke_m10829,
	PrimalityTest_EndInvoke_m10830,
	MemberFilter__ctor_m10831,
	MemberFilter_Invoke_m10832,
	MemberFilter_BeginInvoke_m10833,
	MemberFilter_EndInvoke_m10834,
	TypeFilter__ctor_m10835,
	TypeFilter_Invoke_m10836,
	TypeFilter_BeginInvoke_m10837,
	TypeFilter_EndInvoke_m10838,
	CrossContextDelegate__ctor_m10839,
	CrossContextDelegate_Invoke_m10840,
	CrossContextDelegate_BeginInvoke_m10841,
	CrossContextDelegate_EndInvoke_m10842,
	HeaderHandler__ctor_m10843,
	HeaderHandler_Invoke_m10844,
	HeaderHandler_BeginInvoke_m10845,
	HeaderHandler_EndInvoke_m10846,
	ThreadStart__ctor_m10847,
	ThreadStart_Invoke_m10848,
	ThreadStart_BeginInvoke_m10849,
	ThreadStart_EndInvoke_m10850,
	TimerCallback__ctor_m10851,
	TimerCallback_Invoke_m10852,
	TimerCallback_BeginInvoke_m10853,
	TimerCallback_EndInvoke_m10854,
	WaitCallback__ctor_m10855,
	WaitCallback_Invoke_m10856,
	WaitCallback_BeginInvoke_m10857,
	WaitCallback_EndInvoke_m10858,
	AppDomainInitializer__ctor_m10859,
	AppDomainInitializer_Invoke_m10860,
	AppDomainInitializer_BeginInvoke_m10861,
	AppDomainInitializer_EndInvoke_m10862,
	AssemblyLoadEventHandler__ctor_m10863,
	AssemblyLoadEventHandler_Invoke_m10864,
	AssemblyLoadEventHandler_BeginInvoke_m10865,
	AssemblyLoadEventHandler_EndInvoke_m10866,
	EventHandler__ctor_m10867,
	EventHandler_Invoke_m10868,
	EventHandler_BeginInvoke_m10869,
	EventHandler_EndInvoke_m10870,
	ResolveEventHandler__ctor_m10871,
	ResolveEventHandler_Invoke_m10872,
	ResolveEventHandler_BeginInvoke_m10873,
	ResolveEventHandler_EndInvoke_m10874,
	UnhandledExceptionEventHandler__ctor_m3580,
	UnhandledExceptionEventHandler_Invoke_m10875,
	UnhandledExceptionEventHandler_BeginInvoke_m10876,
	UnhandledExceptionEventHandler_EndInvoke_m10877,
};
