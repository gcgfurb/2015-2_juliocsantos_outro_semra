﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,UnityEngine.TextEditor/TextEditOp>
struct Dictionary_2_t2268;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_ValueCollec_19.h"
#include "UnityEngine_UnityEngine_TextEditor_TextEditOp.h"

// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17067_gshared (Enumerator_t2274 * __this, Dictionary_2_t2268 * ___host, const MethodInfo* method);
#define Enumerator__ctor_m17067(__this, ___host, method) (( void (*) (Enumerator_t2274 *, Dictionary_2_t2268 *, const MethodInfo*))Enumerator__ctor_m17067_gshared)(__this, ___host, method)
// System.Object System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17068_gshared (Enumerator_t2274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17068(__this, method) (( Object_t * (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17068_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17069_gshared (Enumerator_t2274 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17069(__this, method) (( void (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17069_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::Dispose()
extern "C" void Enumerator_Dispose_m17070_gshared (Enumerator_t2274 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17070(__this, method) (( void (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_Dispose_m17070_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17071_gshared (Enumerator_t2274 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17071(__this, method) (( bool (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_MoveNext_m17071_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/ValueCollection/Enumerator<System.Object,UnityEngine.TextEditor/TextEditOp>::get_Current()
extern "C" int32_t Enumerator_get_Current_m17072_gshared (Enumerator_t2274 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17072(__this, method) (( int32_t (*) (Enumerator_t2274 *, const MethodInfo*))Enumerator_get_Current_m17072_gshared)(__this, method)
