﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16590(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2240 *, int32_t, LayoutCache_t549 *, const MethodInfo*))KeyValuePair_2__ctor_m12756_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Key()
#define KeyValuePair_2_get_Key_m16591(__this, method) (( int32_t (*) (KeyValuePair_2_t2240 *, const MethodInfo*))KeyValuePair_2_get_Key_m12757_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16592(__this, ___value, method) (( void (*) (KeyValuePair_2_t2240 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Key_m12758_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::get_Value()
#define KeyValuePair_2_get_Value_m16593(__this, method) (( LayoutCache_t549 * (*) (KeyValuePair_2_t2240 *, const MethodInfo*))KeyValuePair_2_get_Value_m12759_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16594(__this, ___value, method) (( void (*) (KeyValuePair_2_t2240 *, LayoutCache_t549 *, const MethodInfo*))KeyValuePair_2_set_Value_m12760_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.Int32,UnityEngine.GUILayoutUtility/LayoutCache>::ToString()
#define KeyValuePair_2_ToString_m16595(__this, method) (( String_t* (*) (KeyValuePair_2_t2240 *, const MethodInfo*))KeyValuePair_2_ToString_m12761_gshared)(__this, method)
