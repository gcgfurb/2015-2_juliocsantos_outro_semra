﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void WaitForSeconds_t101_marshal ();
extern "C" void WaitForSeconds_t101_marshal_back ();
extern "C" void WaitForSeconds_t101_marshal_cleanup ();
extern "C" void Coroutine_t236_marshal ();
extern "C" void Coroutine_t236_marshal_back ();
extern "C" void Coroutine_t236_marshal_cleanup ();
extern "C" void ScriptableObject_t441_marshal ();
extern "C" void ScriptableObject_t441_marshal_back ();
extern "C" void ScriptableObject_t441_marshal_cleanup ();
extern "C" void Gradient_t468_marshal ();
extern "C" void Gradient_t468_marshal_back ();
extern "C" void Gradient_t468_marshal_cleanup ();
extern "C" void CacheIndex_t483_marshal ();
extern "C" void CacheIndex_t483_marshal_back ();
extern "C" void CacheIndex_t483_marshal_cleanup ();
extern "C" void AsyncOperation_t432_marshal ();
extern "C" void AsyncOperation_t432_marshal_back ();
extern "C" void AsyncOperation_t432_marshal_cleanup ();
extern "C" void Touch_t72_marshal ();
extern "C" void Touch_t72_marshal_back ();
extern "C" void Touch_t72_marshal_cleanup ();
extern "C" void Object_t62_marshal ();
extern "C" void Object_t62_marshal_back ();
extern "C" void Object_t62_marshal_cleanup ();
extern "C" void YieldInstruction_t439_marshal ();
extern "C" void YieldInstruction_t439_marshal_back ();
extern "C" void YieldInstruction_t439_marshal_cleanup ();
extern "C" void WebCamDevice_t518_marshal ();
extern "C" void WebCamDevice_t518_marshal_back ();
extern "C" void WebCamDevice_t518_marshal_cleanup ();
extern "C" void AnimationCurve_t525_marshal ();
extern "C" void AnimationCurve_t525_marshal_back ();
extern "C" void AnimationCurve_t525_marshal_cleanup ();
extern "C" void AnimatorTransitionInfo_t527_marshal ();
extern "C" void AnimatorTransitionInfo_t527_marshal_back ();
extern "C" void AnimatorTransitionInfo_t527_marshal_cleanup ();
extern "C" void SkeletonBone_t528_marshal ();
extern "C" void SkeletonBone_t528_marshal_back ();
extern "C" void SkeletonBone_t528_marshal_cleanup ();
extern "C" void HumanBone_t530_marshal ();
extern "C" void HumanBone_t530_marshal_back ();
extern "C" void HumanBone_t530_marshal_cleanup ();
extern "C" void CharacterInfo_t533_marshal ();
extern "C" void CharacterInfo_t533_marshal_back ();
extern "C" void CharacterInfo_t533_marshal_cleanup ();
extern "C" void Event_t237_marshal ();
extern "C" void Event_t237_marshal_back ();
extern "C" void Event_t237_marshal_cleanup ();
extern "C" void GcAchievementData_t581_marshal ();
extern "C" void GcAchievementData_t581_marshal_back ();
extern "C" void GcAchievementData_t581_marshal_cleanup ();
extern "C" void GcScoreData_t582_marshal ();
extern "C" void GcScoreData_t582_marshal_back ();
extern "C" void GcScoreData_t582_marshal_cleanup ();
extern "C" void TrackedReference_t526_marshal ();
extern "C" void TrackedReference_t526_marshal_back ();
extern "C" void TrackedReference_t526_marshal_cleanup ();
extern "C" void X509ChainStatus_t966_marshal ();
extern "C" void X509ChainStatus_t966_marshal_back ();
extern "C" void X509ChainStatus_t966_marshal_cleanup ();
extern "C" void IntStack_t1010_marshal ();
extern "C" void IntStack_t1010_marshal_back ();
extern "C" void IntStack_t1010_marshal_cleanup ();
extern "C" void Interval_t1016_marshal ();
extern "C" void Interval_t1016_marshal_back ();
extern "C" void Interval_t1016_marshal_cleanup ();
extern "C" void UriScheme_t1046_marshal ();
extern "C" void UriScheme_t1046_marshal_back ();
extern "C" void UriScheme_t1046_marshal_cleanup ();
extern "C" void Context_t1125_marshal ();
extern "C" void Context_t1125_marshal_back ();
extern "C" void Context_t1125_marshal_cleanup ();
extern "C" void PreviousInfo_t1126_marshal ();
extern "C" void PreviousInfo_t1126_marshal_back ();
extern "C" void PreviousInfo_t1126_marshal_cleanup ();
extern "C" void Escape_t1127_marshal ();
extern "C" void Escape_t1127_marshal_back ();
extern "C" void Escape_t1127_marshal_cleanup ();
extern "C" void MonoIOStat_t1245_marshal ();
extern "C" void MonoIOStat_t1245_marshal_back ();
extern "C" void MonoIOStat_t1245_marshal_cleanup ();
extern "C" void ParameterModifier_t1345_marshal ();
extern "C" void ParameterModifier_t1345_marshal_back ();
extern "C" void ParameterModifier_t1345_marshal_cleanup ();
extern "C" void ResourceInfo_t1356_marshal ();
extern "C" void ResourceInfo_t1356_marshal_back ();
extern "C" void ResourceInfo_t1356_marshal_cleanup ();
extern "C" void DSAParameters_t859_marshal ();
extern "C" void DSAParameters_t859_marshal_back ();
extern "C" void DSAParameters_t859_marshal_cleanup ();
extern "C" void RSAParameters_t831_marshal ();
extern "C" void RSAParameters_t831_marshal_back ();
extern "C" void RSAParameters_t831_marshal_cleanup ();
extern const Il2CppMarshalingFunctions g_MarshalingFunctions[32] = 
{
	{ WaitForSeconds_t101_marshal, WaitForSeconds_t101_marshal_back, WaitForSeconds_t101_marshal_cleanup },
	{ Coroutine_t236_marshal, Coroutine_t236_marshal_back, Coroutine_t236_marshal_cleanup },
	{ ScriptableObject_t441_marshal, ScriptableObject_t441_marshal_back, ScriptableObject_t441_marshal_cleanup },
	{ Gradient_t468_marshal, Gradient_t468_marshal_back, Gradient_t468_marshal_cleanup },
	{ CacheIndex_t483_marshal, CacheIndex_t483_marshal_back, CacheIndex_t483_marshal_cleanup },
	{ AsyncOperation_t432_marshal, AsyncOperation_t432_marshal_back, AsyncOperation_t432_marshal_cleanup },
	{ Touch_t72_marshal, Touch_t72_marshal_back, Touch_t72_marshal_cleanup },
	{ Object_t62_marshal, Object_t62_marshal_back, Object_t62_marshal_cleanup },
	{ YieldInstruction_t439_marshal, YieldInstruction_t439_marshal_back, YieldInstruction_t439_marshal_cleanup },
	{ WebCamDevice_t518_marshal, WebCamDevice_t518_marshal_back, WebCamDevice_t518_marshal_cleanup },
	{ AnimationCurve_t525_marshal, AnimationCurve_t525_marshal_back, AnimationCurve_t525_marshal_cleanup },
	{ AnimatorTransitionInfo_t527_marshal, AnimatorTransitionInfo_t527_marshal_back, AnimatorTransitionInfo_t527_marshal_cleanup },
	{ SkeletonBone_t528_marshal, SkeletonBone_t528_marshal_back, SkeletonBone_t528_marshal_cleanup },
	{ HumanBone_t530_marshal, HumanBone_t530_marshal_back, HumanBone_t530_marshal_cleanup },
	{ CharacterInfo_t533_marshal, CharacterInfo_t533_marshal_back, CharacterInfo_t533_marshal_cleanup },
	{ Event_t237_marshal, Event_t237_marshal_back, Event_t237_marshal_cleanup },
	{ GcAchievementData_t581_marshal, GcAchievementData_t581_marshal_back, GcAchievementData_t581_marshal_cleanup },
	{ GcScoreData_t582_marshal, GcScoreData_t582_marshal_back, GcScoreData_t582_marshal_cleanup },
	{ TrackedReference_t526_marshal, TrackedReference_t526_marshal_back, TrackedReference_t526_marshal_cleanup },
	{ X509ChainStatus_t966_marshal, X509ChainStatus_t966_marshal_back, X509ChainStatus_t966_marshal_cleanup },
	{ IntStack_t1010_marshal, IntStack_t1010_marshal_back, IntStack_t1010_marshal_cleanup },
	{ Interval_t1016_marshal, Interval_t1016_marshal_back, Interval_t1016_marshal_cleanup },
	{ UriScheme_t1046_marshal, UriScheme_t1046_marshal_back, UriScheme_t1046_marshal_cleanup },
	{ Context_t1125_marshal, Context_t1125_marshal_back, Context_t1125_marshal_cleanup },
	{ PreviousInfo_t1126_marshal, PreviousInfo_t1126_marshal_back, PreviousInfo_t1126_marshal_cleanup },
	{ Escape_t1127_marshal, Escape_t1127_marshal_back, Escape_t1127_marshal_cleanup },
	{ MonoIOStat_t1245_marshal, MonoIOStat_t1245_marshal_back, MonoIOStat_t1245_marshal_cleanup },
	{ ParameterModifier_t1345_marshal, ParameterModifier_t1345_marshal_back, ParameterModifier_t1345_marshal_cleanup },
	{ ResourceInfo_t1356_marshal, ResourceInfo_t1356_marshal_back, ResourceInfo_t1356_marshal_cleanup },
	{ DSAParameters_t859_marshal, DSAParameters_t859_marshal_back, DSAParameters_t859_marshal_cleanup },
	{ RSAParameters_t831_marshal, RSAParameters_t831_marshal_back, RSAParameters_t831_marshal_cleanup },
	NULL,
};
