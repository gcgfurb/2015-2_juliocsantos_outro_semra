﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UICharInfo>
struct List_1_t535;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t2490;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// System.Collections.Generic.ICollection`1<UnityEngine.UICharInfo>
struct ICollection_1_t2491;
// System.Collections.Generic.IEnumerable`1<UnityEngine.UICharInfo>
struct IEnumerable_1_t2492;
// System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UICharInfo>
struct ReadOnlyCollection_1_t2220;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t647;
// System.Predicate`1<UnityEngine.UICharInfo>
struct Predicate_1_t2224;
// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t2227;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_33.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void List_1__ctor_m16251_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1__ctor_m16251(__this, method) (( void (*) (List_1_t535 *, const MethodInfo*))List_1__ctor_m16251_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.ctor(System.Int32)
extern "C" void List_1__ctor_m3612_gshared (List_1_t535 * __this, int32_t ___capacity, const MethodInfo* method);
#define List_1__ctor_m3612(__this, ___capacity, method) (( void (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1__ctor_m3612_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::.cctor()
extern "C" void List_1__cctor_m16252_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define List_1__cctor_m16252(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m16252_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
extern "C" Object_t* List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16253_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16253(__this, method) (( Object_t* (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16253_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void List_1_System_Collections_ICollection_CopyTo_m16254_gshared (List_1_t535 * __this, Array_t * ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_System_Collections_ICollection_CopyTo_m16254(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t535 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m16254_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * List_1_System_Collections_IEnumerable_GetEnumerator_m16255_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_IEnumerable_GetEnumerator_m16255(__this, method) (( Object_t * (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m16255_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t List_1_System_Collections_IList_Add_m16256_gshared (List_1_t535 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Add_m16256(__this, ___item, method) (( int32_t (*) (List_1_t535 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m16256_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool List_1_System_Collections_IList_Contains_m16257_gshared (List_1_t535 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Contains_m16257(__this, ___item, method) (( bool (*) (List_1_t535 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m16257_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t List_1_System_Collections_IList_IndexOf_m16258_gshared (List_1_t535 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_IndexOf_m16258(__this, ___item, method) (( int32_t (*) (List_1_t535 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m16258_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_Insert_m16259_gshared (List_1_t535 * __this, int32_t ___index, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Insert_m16259(__this, ___index, ___item, method) (( void (*) (List_1_t535 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m16259_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void List_1_System_Collections_IList_Remove_m16260_gshared (List_1_t535 * __this, Object_t * ___item, const MethodInfo* method);
#define List_1_System_Collections_IList_Remove_m16260(__this, ___item, method) (( void (*) (List_1_t535 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m16260_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16261_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16261(__this, method) (( bool (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16261_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool List_1_System_Collections_ICollection_get_IsSynchronized_m16262_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_IsSynchronized_m16262(__this, method) (( bool (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m16262_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * List_1_System_Collections_ICollection_get_SyncRoot_m16263_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_ICollection_get_SyncRoot_m16263(__this, method) (( Object_t * (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m16263_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool List_1_System_Collections_IList_get_IsFixedSize_m16264_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsFixedSize_m16264(__this, method) (( bool (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m16264_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool List_1_System_Collections_IList_get_IsReadOnly_m16265_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_System_Collections_IList_get_IsReadOnly_m16265(__this, method) (( bool (*) (List_1_t535 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m16265_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * List_1_System_Collections_IList_get_Item_m16266_gshared (List_1_t535 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_System_Collections_IList_get_Item_m16266(__this, ___index, method) (( Object_t * (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m16266_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void List_1_System_Collections_IList_set_Item_m16267_gshared (List_1_t535 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define List_1_System_Collections_IList_set_Item_m16267(__this, ___index, ___value, method) (( void (*) (List_1_t535 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m16267_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void List_1_Add_m16268_gshared (List_1_t535 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define List_1_Add_m16268(__this, ___item, method) (( void (*) (List_1_t535 *, UICharInfo_t396 , const MethodInfo*))List_1_Add_m16268_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GrowIfNeeded(System.Int32)
extern "C" void List_1_GrowIfNeeded_m16269_gshared (List_1_t535 * __this, int32_t ___newCount, const MethodInfo* method);
#define List_1_GrowIfNeeded_m16269(__this, ___newCount, method) (( void (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m16269_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddCollection(System.Collections.Generic.ICollection`1<T>)
extern "C" void List_1_AddCollection_m16270_gshared (List_1_t535 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddCollection_m16270(__this, ___collection, method) (( void (*) (List_1_t535 *, Object_t*, const MethodInfo*))List_1_AddCollection_m16270_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddEnumerable_m16271_gshared (List_1_t535 * __this, Object_t* ___enumerable, const MethodInfo* method);
#define List_1_AddEnumerable_m16271(__this, ___enumerable, method) (( void (*) (List_1_t535 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m16271_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_AddRange_m16272_gshared (List_1_t535 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_AddRange_m16272(__this, ___collection, method) (( void (*) (List_1_t535 *, Object_t*, const MethodInfo*))List_1_AddRange_m16272_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::AsReadOnly()
extern "C" ReadOnlyCollection_1_t2220 * List_1_AsReadOnly_m16273_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_AsReadOnly_m16273(__this, method) (( ReadOnlyCollection_1_t2220 * (*) (List_1_t535 *, const MethodInfo*))List_1_AsReadOnly_m16273_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Clear()
extern "C" void List_1_Clear_m16274_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_Clear_m16274(__this, method) (( void (*) (List_1_t535 *, const MethodInfo*))List_1_Clear_m16274_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool List_1_Contains_m16275_gshared (List_1_t535 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define List_1_Contains_m16275(__this, ___item, method) (( bool (*) (List_1_t535 *, UICharInfo_t396 , const MethodInfo*))List_1_Contains_m16275_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void List_1_CopyTo_m16276_gshared (List_1_t535 * __this, UICharInfoU5BU5D_t647* ___array, int32_t ___arrayIndex, const MethodInfo* method);
#define List_1_CopyTo_m16276(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t535 *, UICharInfoU5BU5D_t647*, int32_t, const MethodInfo*))List_1_CopyTo_m16276_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Find(System.Predicate`1<T>)
extern "C" UICharInfo_t396  List_1_Find_m16277_gshared (List_1_t535 * __this, Predicate_1_t2224 * ___match, const MethodInfo* method);
#define List_1_Find_m16277(__this, ___match, method) (( UICharInfo_t396  (*) (List_1_t535 *, Predicate_1_t2224 *, const MethodInfo*))List_1_Find_m16277_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckMatch(System.Predicate`1<T>)
extern "C" void List_1_CheckMatch_m16278_gshared (Object_t * __this /* static, unused */, Predicate_1_t2224 * ___match, const MethodInfo* method);
#define List_1_CheckMatch_m16278(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t2224 *, const MethodInfo*))List_1_CheckMatch_m16278_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
extern "C" int32_t List_1_GetIndex_m16279_gshared (List_1_t535 * __this, int32_t ___startIndex, int32_t ___count, Predicate_1_t2224 * ___match, const MethodInfo* method);
#define List_1_GetIndex_m16279(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t535 *, int32_t, int32_t, Predicate_1_t2224 *, const MethodInfo*))List_1_GetIndex_m16279_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Enumerator_t2219  List_1_GetEnumerator_m16280_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_GetEnumerator_m16280(__this, method) (( Enumerator_t2219  (*) (List_1_t535 *, const MethodInfo*))List_1_GetEnumerator_m16280_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t List_1_IndexOf_m16281_gshared (List_1_t535 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define List_1_IndexOf_m16281(__this, ___item, method) (( int32_t (*) (List_1_t535 *, UICharInfo_t396 , const MethodInfo*))List_1_IndexOf_m16281_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Shift(System.Int32,System.Int32)
extern "C" void List_1_Shift_m16282_gshared (List_1_t535 * __this, int32_t ___start, int32_t ___delta, const MethodInfo* method);
#define List_1_Shift_m16282(__this, ___start, ___delta, method) (( void (*) (List_1_t535 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m16282_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckIndex(System.Int32)
extern "C" void List_1_CheckIndex_m16283_gshared (List_1_t535 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_CheckIndex_m16283(__this, ___index, method) (( void (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1_CheckIndex_m16283_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void List_1_Insert_m16284_gshared (List_1_t535 * __this, int32_t ___index, UICharInfo_t396  ___item, const MethodInfo* method);
#define List_1_Insert_m16284(__this, ___index, ___item, method) (( void (*) (List_1_t535 *, int32_t, UICharInfo_t396 , const MethodInfo*))List_1_Insert_m16284_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
extern "C" void List_1_CheckCollection_m16285_gshared (List_1_t535 * __this, Object_t* ___collection, const MethodInfo* method);
#define List_1_CheckCollection_m16285(__this, ___collection, method) (( void (*) (List_1_t535 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m16285_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool List_1_Remove_m16286_gshared (List_1_t535 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define List_1_Remove_m16286(__this, ___item, method) (( bool (*) (List_1_t535 *, UICharInfo_t396 , const MethodInfo*))List_1_Remove_m16286_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAll(System.Predicate`1<T>)
extern "C" int32_t List_1_RemoveAll_m16287_gshared (List_1_t535 * __this, Predicate_1_t2224 * ___match, const MethodInfo* method);
#define List_1_RemoveAll_m16287(__this, ___match, method) (( int32_t (*) (List_1_t535 *, Predicate_1_t2224 *, const MethodInfo*))List_1_RemoveAll_m16287_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void List_1_RemoveAt_m16288_gshared (List_1_t535 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_RemoveAt_m16288(__this, ___index, method) (( void (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1_RemoveAt_m16288_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Reverse()
extern "C" void List_1_Reverse_m16289_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_Reverse_m16289(__this, method) (( void (*) (List_1_t535 *, const MethodInfo*))List_1_Reverse_m16289_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort()
extern "C" void List_1_Sort_m16290_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_Sort_m16290(__this, method) (( void (*) (List_1_t535 *, const MethodInfo*))List_1_Sort_m16290_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::Sort(System.Comparison`1<T>)
extern "C" void List_1_Sort_m16291_gshared (List_1_t535 * __this, Comparison_1_t2227 * ___comparison, const MethodInfo* method);
#define List_1_Sort_m16291(__this, ___comparison, method) (( void (*) (List_1_t535 *, Comparison_1_t2227 *, const MethodInfo*))List_1_Sort_m16291_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.UICharInfo>::ToArray()
extern "C" UICharInfoU5BU5D_t647* List_1_ToArray_m16292_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_ToArray_m16292(__this, method) (( UICharInfoU5BU5D_t647* (*) (List_1_t535 *, const MethodInfo*))List_1_ToArray_m16292_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::TrimExcess()
extern "C" void List_1_TrimExcess_m16293_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_TrimExcess_m16293(__this, method) (( void (*) (List_1_t535 *, const MethodInfo*))List_1_TrimExcess_m16293_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Capacity()
extern "C" int32_t List_1_get_Capacity_m16294_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_get_Capacity_m16294(__this, method) (( int32_t (*) (List_1_t535 *, const MethodInfo*))List_1_get_Capacity_m16294_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Capacity(System.Int32)
extern "C" void List_1_set_Capacity_m16295_gshared (List_1_t535 * __this, int32_t ___value, const MethodInfo* method);
#define List_1_set_Capacity_m16295(__this, ___value, method) (( void (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1_set_Capacity_m16295_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t List_1_get_Count_m16296_gshared (List_1_t535 * __this, const MethodInfo* method);
#define List_1_get_Count_m16296(__this, method) (( int32_t (*) (List_1_t535 *, const MethodInfo*))List_1_get_Count_m16296_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t396  List_1_get_Item_m16297_gshared (List_1_t535 * __this, int32_t ___index, const MethodInfo* method);
#define List_1_get_Item_m16297(__this, ___index, method) (( UICharInfo_t396  (*) (List_1_t535 *, int32_t, const MethodInfo*))List_1_get_Item_m16297_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void List_1_set_Item_m16298_gshared (List_1_t535 * __this, int32_t ___index, UICharInfo_t396  ___value, const MethodInfo* method);
#define List_1_set_Item_m16298(__this, ___index, ___value, method) (( void (*) (List_1_t535 *, int32_t, UICharInfo_t396 , const MethodInfo*))List_1_set_Item_m16298_gshared)(__this, ___index, ___value, method)
