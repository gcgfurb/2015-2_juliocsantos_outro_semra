﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.CarController
struct CarController_t29;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.CarController::.ctor()
extern "C" void CarController__ctor_m145 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::.cctor()
extern "C" void CarController__cctor_m146 (Object_t * __this /* static, unused */, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Vehicles.Car.CarController::get_Skidding()
extern "C" bool CarController_get_Skidding_m147 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Skidding(System.Boolean)
extern "C" void CarController_set_Skidding_m148 (CarController_t29 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_BrakeInput()
extern "C" float CarController_get_BrakeInput_m149 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_BrakeInput(System.Single)
extern "C" void CarController_set_BrakeInput_m150 (CarController_t29 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSteerAngle()
extern "C" float CarController_get_CurrentSteerAngle_m151 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_CurrentSpeed()
extern "C" float CarController_get_CurrentSpeed_m152 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_MaxSpeed()
extern "C" float CarController_get_MaxSpeed_m153 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_Revs()
extern "C" float CarController_get_Revs_m154 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_Revs(System.Single)
extern "C" void CarController_set_Revs_m155 (CarController_t29 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::get_AccelInput()
extern "C" float CarController_get_AccelInput_m156 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::set_AccelInput(System.Single)
extern "C" void CarController_set_AccelInput_m157 (CarController_t29 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::Start()
extern "C" void CarController_Start_m158 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::GearChanging()
extern "C" void CarController_GearChanging_m159 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::CurveFactor(System.Single)
extern "C" float CarController_CurveFactor_m160 (Object_t * __this /* static, unused */, float ___factor, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityStandardAssets.Vehicles.Car.CarController::ULerp(System.Single,System.Single,System.Single)
extern "C" float CarController_ULerp_m161 (Object_t * __this /* static, unused */, float ___from, float ___to, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateGearFactor()
extern "C" void CarController_CalculateGearFactor_m162 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CalculateRevs()
extern "C" void CarController_CalculateRevs_m163 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::Move(System.Single,System.Single,System.Single,System.Single)
extern "C" void CarController_Move_m164 (CarController_t29 * __this, float ___steering, float ___accel, float ___footbrake, float ___handbrake, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CapSpeed()
extern "C" void CarController_CapSpeed_m165 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::ApplyDrive(System.Single,System.Single)
extern "C" void CarController_ApplyDrive_m166 (CarController_t29 * __this, float ___accel, float ___footbrake, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::SteerHelper()
extern "C" void CarController_SteerHelper_m167 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::AddDownForce()
extern "C" void CarController_AddDownForce_m168 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::CheckForWheelSpin()
extern "C" void CarController_CheckForWheelSpin_m169 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::TractionControl()
extern "C" void CarController_TractionControl_m170 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.CarController::AdjustTorque(System.Single)
extern "C" void CarController_AdjustTorque_m171 (CarController_t29 * __this, float ___forwardSlip, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Vehicles.Car.CarController::AnySkidSoundPlaying()
extern "C" bool CarController_AnySkidSoundPlaying_m172 (CarController_t29 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
