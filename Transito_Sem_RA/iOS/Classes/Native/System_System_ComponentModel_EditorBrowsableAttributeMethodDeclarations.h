﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.ComponentModel.EditorBrowsableAttribute
struct EditorBrowsableAttribute_t920;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "System_System_ComponentModel_EditorBrowsableState.h"

// System.Void System.ComponentModel.EditorBrowsableAttribute::.ctor(System.ComponentModel.EditorBrowsableState)
extern "C" void EditorBrowsableAttribute__ctor_m4846 (EditorBrowsableAttribute_t920 * __this, int32_t ___state, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.ComponentModel.EditorBrowsableState System.ComponentModel.EditorBrowsableAttribute::get_State()
extern "C" int32_t EditorBrowsableAttribute_get_State_m4847 (EditorBrowsableAttribute_t920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.ComponentModel.EditorBrowsableAttribute::Equals(System.Object)
extern "C" bool EditorBrowsableAttribute_Equals_m4848 (EditorBrowsableAttribute_t920 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.ComponentModel.EditorBrowsableAttribute::GetHashCode()
extern "C" int32_t EditorBrowsableAttribute_GetHashCode_m4849 (EditorBrowsableAttribute_t920 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
