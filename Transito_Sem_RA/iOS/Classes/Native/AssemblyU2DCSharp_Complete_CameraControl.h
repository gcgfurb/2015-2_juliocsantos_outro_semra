﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Transform[]
struct TransformU5BU5D_t90;
// UnityEngine.Camera
struct Camera_t74;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// Complete.CameraControl
struct  CameraControl_t89  : public MonoBehaviour_t2
{
	// System.Single Complete.CameraControl::m_DampTime
	float ___m_DampTime_2;
	// System.Single Complete.CameraControl::m_ScreenEdgeBuffer
	float ___m_ScreenEdgeBuffer_3;
	// System.Single Complete.CameraControl::m_MinSize
	float ___m_MinSize_4;
	// UnityEngine.Transform[] Complete.CameraControl::m_Targets
	TransformU5BU5D_t90* ___m_Targets_5;
	// UnityEngine.Camera Complete.CameraControl::m_Camera
	Camera_t74 * ___m_Camera_6;
	// System.Single Complete.CameraControl::m_ZoomSpeed
	float ___m_ZoomSpeed_7;
	// UnityEngine.Vector3 Complete.CameraControl::m_MoveVelocity
	Vector3_t12  ___m_MoveVelocity_8;
	// UnityEngine.Vector3 Complete.CameraControl::m_DesiredPosition
	Vector3_t12  ___m_DesiredPosition_9;
};
