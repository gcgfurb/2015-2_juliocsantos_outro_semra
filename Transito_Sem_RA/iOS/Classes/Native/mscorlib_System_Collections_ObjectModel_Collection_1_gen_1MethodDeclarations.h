﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>
struct Collection_1_t2021;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t234;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UIVertex>
struct IEnumerator_1_t2458;
// System.Collections.Generic.IList`1<UnityEngine.UIVertex>
struct IList_1_t409;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UIVertex.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::.ctor()
extern "C" void Collection_1__ctor_m13718_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1__ctor_m13718(__this, method) (( void (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1__ctor_m13718_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13719_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13719(__this, method) (( bool (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m13719_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m13720_gshared (Collection_1_t2021 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m13720(__this, ___array, ___index, method) (( void (*) (Collection_1_t2021 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m13720_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m13721_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m13721(__this, method) (( Object_t * (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m13721_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m13722_gshared (Collection_1_t2021 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m13722(__this, ___value, method) (( int32_t (*) (Collection_1_t2021 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m13722_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m13723_gshared (Collection_1_t2021 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m13723(__this, ___value, method) (( bool (*) (Collection_1_t2021 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m13723_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m13724_gshared (Collection_1_t2021 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m13724(__this, ___value, method) (( int32_t (*) (Collection_1_t2021 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m13724_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m13725_gshared (Collection_1_t2021 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m13725(__this, ___index, ___value, method) (( void (*) (Collection_1_t2021 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m13725_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m13726_gshared (Collection_1_t2021 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m13726(__this, ___value, method) (( void (*) (Collection_1_t2021 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m13726_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m13727_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m13727(__this, method) (( bool (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m13727_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m13728_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m13728(__this, method) (( Object_t * (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m13728_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m13729_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m13729(__this, method) (( bool (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m13729_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m13730_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m13730(__this, method) (( bool (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m13730_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m13731_gshared (Collection_1_t2021 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m13731(__this, ___index, method) (( Object_t * (*) (Collection_1_t2021 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m13731_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m13732_gshared (Collection_1_t2021 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m13732(__this, ___index, ___value, method) (( void (*) (Collection_1_t2021 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m13732_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Add(T)
extern "C" void Collection_1_Add_m13733_gshared (Collection_1_t2021 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_Add_m13733(__this, ___item, method) (( void (*) (Collection_1_t2021 *, UIVertex_t239 , const MethodInfo*))Collection_1_Add_m13733_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Clear()
extern "C" void Collection_1_Clear_m13734_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_Clear_m13734(__this, method) (( void (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_Clear_m13734_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ClearItems()
extern "C" void Collection_1_ClearItems_m13735_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m13735(__this, method) (( void (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_ClearItems_m13735_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Contains(T)
extern "C" bool Collection_1_Contains_m13736_gshared (Collection_1_t2021 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_Contains_m13736(__this, ___item, method) (( bool (*) (Collection_1_t2021 *, UIVertex_t239 , const MethodInfo*))Collection_1_Contains_m13736_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m13737_gshared (Collection_1_t2021 * __this, UIVertexU5BU5D_t234* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m13737(__this, ___array, ___index, method) (( void (*) (Collection_1_t2021 *, UIVertexU5BU5D_t234*, int32_t, const MethodInfo*))Collection_1_CopyTo_m13737_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m13738_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m13738(__this, method) (( Object_t* (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_GetEnumerator_m13738_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m13739_gshared (Collection_1_t2021 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m13739(__this, ___item, method) (( int32_t (*) (Collection_1_t2021 *, UIVertex_t239 , const MethodInfo*))Collection_1_IndexOf_m13739_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m13740_gshared (Collection_1_t2021 * __this, int32_t ___index, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_Insert_m13740(__this, ___index, ___item, method) (( void (*) (Collection_1_t2021 *, int32_t, UIVertex_t239 , const MethodInfo*))Collection_1_Insert_m13740_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m13741_gshared (Collection_1_t2021 * __this, int32_t ___index, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m13741(__this, ___index, ___item, method) (( void (*) (Collection_1_t2021 *, int32_t, UIVertex_t239 , const MethodInfo*))Collection_1_InsertItem_m13741_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::Remove(T)
extern "C" bool Collection_1_Remove_m13742_gshared (Collection_1_t2021 * __this, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_Remove_m13742(__this, ___item, method) (( bool (*) (Collection_1_t2021 *, UIVertex_t239 , const MethodInfo*))Collection_1_Remove_m13742_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m13743_gshared (Collection_1_t2021 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m13743(__this, ___index, method) (( void (*) (Collection_1_t2021 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m13743_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m13744_gshared (Collection_1_t2021 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m13744(__this, ___index, method) (( void (*) (Collection_1_t2021 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m13744_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Count()
extern "C" int32_t Collection_1_get_Count_m13745_gshared (Collection_1_t2021 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m13745(__this, method) (( int32_t (*) (Collection_1_t2021 *, const MethodInfo*))Collection_1_get_Count_m13745_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::get_Item(System.Int32)
extern "C" UIVertex_t239  Collection_1_get_Item_m13746_gshared (Collection_1_t2021 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m13746(__this, ___index, method) (( UIVertex_t239  (*) (Collection_1_t2021 *, int32_t, const MethodInfo*))Collection_1_get_Item_m13746_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m13747_gshared (Collection_1_t2021 * __this, int32_t ___index, UIVertex_t239  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m13747(__this, ___index, ___value, method) (( void (*) (Collection_1_t2021 *, int32_t, UIVertex_t239 , const MethodInfo*))Collection_1_set_Item_m13747_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m13748_gshared (Collection_1_t2021 * __this, int32_t ___index, UIVertex_t239  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m13748(__this, ___index, ___item, method) (( void (*) (Collection_1_t2021 *, int32_t, UIVertex_t239 , const MethodInfo*))Collection_1_SetItem_m13748_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m13749_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m13749(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m13749_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::ConvertItem(System.Object)
extern "C" UIVertex_t239  Collection_1_ConvertItem_m13750_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m13750(__this /* static, unused */, ___item, method) (( UIVertex_t239  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m13750_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m13751_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m13751(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m13751_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m13752_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m13752(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m13752_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UIVertex>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m13753_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m13753(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m13753_gshared)(__this /* static, unused */, ___list, method)
