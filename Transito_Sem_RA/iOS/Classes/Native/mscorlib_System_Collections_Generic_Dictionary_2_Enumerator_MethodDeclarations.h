﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m12835(__this, ___dictionary, method) (( void (*) (Enumerator_t364 *, Dictionary_2_t151 *, const MethodInfo*))Enumerator__ctor_m12782_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m12836(__this, method) (( Object_t * (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m12783_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m12837(__this, method) (( void (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m12784_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12838(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m12785_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12839(__this, method) (( Object_t * (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m12786_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12840(__this, method) (( Object_t * (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m12787_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::MoveNext()
#define Enumerator_MoveNext_m1933(__this, method) (( bool (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_MoveNext_m12788_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_Current()
#define Enumerator_get_Current_m1930(__this, method) (( KeyValuePair_2_t363  (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_get_Current_m12789_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m12841(__this, method) (( int32_t (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_get_CurrentKey_m12790_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m12842(__this, method) (( PointerEventData_t57 * (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_get_CurrentValue_m12791_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Reset()
#define Enumerator_Reset_m12843(__this, method) (( void (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_Reset_m12792_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyState()
#define Enumerator_VerifyState_m12844(__this, method) (( void (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_VerifyState_m12793_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m12845(__this, method) (( void (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_VerifyCurrent_m12794_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Int32,UnityEngine.EventSystems.PointerEventData>::Dispose()
#define Enumerator_Dispose_m12846(__this, method) (( void (*) (Enumerator_t364 *, const MethodInfo*))Enumerator_Dispose_m12795_gshared)(__this, method)
