﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m13974(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2037 *, Canvas_t197 *, IndexedSet_1_t388 *, const MethodInfo*))KeyValuePair_2__ctor_m11006_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Key()
#define KeyValuePair_2_get_Key_m13975(__this, method) (( Canvas_t197 * (*) (KeyValuePair_2_t2037 *, const MethodInfo*))KeyValuePair_2_get_Key_m11007_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m13976(__this, ___value, method) (( void (*) (KeyValuePair_2_t2037 *, Canvas_t197 *, const MethodInfo*))KeyValuePair_2_set_Key_m11008_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::get_Value()
#define KeyValuePair_2_get_Value_m13977(__this, method) (( IndexedSet_1_t388 * (*) (KeyValuePair_2_t2037 *, const MethodInfo*))KeyValuePair_2_get_Value_m11009_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m13978(__this, ___value, method) (( void (*) (KeyValuePair_2_t2037 *, IndexedSet_1_t388 *, const MethodInfo*))KeyValuePair_2_set_Value_m11010_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Canvas,UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>>::ToString()
#define KeyValuePair_2_ToString_m13979(__this, method) (( String_t* (*) (KeyValuePair_2_t2037 *, const MethodInfo*))KeyValuePair_2_ToString_m11011_gshared)(__this, method)
