﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_5.h"

// UnityStandardAssets.Vehicles.Car.SpeedType
struct  SpeedType_t40 
{
	// System.Int32 UnityStandardAssets.Vehicles.Car.SpeedType::value__
	int32_t ___value___1;
};
