﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// UnityStandardAssets.Vehicles.Car.SkidTrail
struct  SkidTrail_t50  : public MonoBehaviour_t2
{
	// System.Single UnityStandardAssets.Vehicles.Car.SkidTrail::m_PersistTime
	float ___m_PersistTime_2;
};
