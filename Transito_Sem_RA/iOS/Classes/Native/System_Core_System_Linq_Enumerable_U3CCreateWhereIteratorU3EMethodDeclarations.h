﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>
struct U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088;
// System.Object
struct Object_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Collections.Generic.IEnumerator`1<System.Object>
struct IEnumerator_1_t2089;

#include "codegen/il2cpp-codegen.h"

// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::.ctor()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m14693_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m14693(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1__ctor_m14693_gshared)(__this, method)
// TSource System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerator<TSource>.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14694_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14694(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumeratorU3CTSourceU3E_get_Current_m14694_gshared)(__this, method)
// System.Object System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m14695_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m14695(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerator_get_Current_m14695_gshared)(__this, method)
// System.Collections.IEnumerator System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m14696_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m14696(__this, method) (( Object_t * (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_IEnumerable_GetEnumerator_m14696_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<TSource> System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::System.Collections.Generic.IEnumerable<TSource>.GetEnumerator()
extern "C" Object_t* U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14697_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14697(__this, method) (( Object_t* (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_System_Collections_Generic_IEnumerableU3CTSourceU3E_GetEnumerator_m14697_gshared)(__this, method)
// System.Boolean System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::MoveNext()
extern "C" bool U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m14698_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m14698(__this, method) (( bool (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_MoveNext_m14698_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Dispose()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m14699_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m14699(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Dispose_m14699_gshared)(__this, method)
// System.Void System.Linq.Enumerable/<CreateWhereIterator>c__Iterator1D`1<System.Object>::Reset()
extern "C" void U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m14700_gshared (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 * __this, const MethodInfo* method);
#define U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m14700(__this, method) (( void (*) (U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2088 *, const MethodInfo*))U3CCreateWhereIteratorU3Ec__Iterator1D_1_Reset_m14700_gshared)(__this, method)
