﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "mscorlib_System_Enum.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_0.h"

// UnityStandardAssets.Vehicles.Car.CarAIControl/BrakeCondition
struct  BrakeCondition_t31 
{
	// System.Int32 UnityStandardAssets.Vehicles.Car.CarAIControl/BrakeCondition::value__
	int32_t ___value___1;
};
