﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.UI.Text
struct Text_t97;
// AtualizaVida
struct AtualizaVida_t86;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_Color.h"

// Principal
struct  Principal_t96  : public MonoBehaviour_t2
{
	// UnityEngine.Color Principal::m_CorMesagemPadraso
	Color_t83  ___m_CorMesagemPadraso_2;
	// UnityEngine.Color Principal::m_CorMesagemAlerta
	Color_t83  ___m_CorMesagemAlerta_3;
	// UnityEngine.Color Principal::m_CorMesagemPerigo
	Color_t83  ___m_CorMesagemPerigo_4;
	// UnityEngine.UI.Text Principal::m_MesagemTexto
	Text_t97 * ___m_MesagemTexto_5;
	// AtualizaVida Principal::m_Vidas
	AtualizaVida_t86 * ___m_Vidas_6;
};
