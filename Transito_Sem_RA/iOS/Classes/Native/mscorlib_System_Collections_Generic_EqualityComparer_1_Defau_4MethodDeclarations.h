﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>
struct DefaultComparer_t2133;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::.ctor()
extern "C" void DefaultComparer__ctor_m15387_gshared (DefaultComparer_t2133 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m15387(__this, method) (( void (*) (DefaultComparer_t2133 *, const MethodInfo*))DefaultComparer__ctor_m15387_gshared)(__this, method)
// System.Int32 System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::GetHashCode(T)
extern "C" int32_t DefaultComparer_GetHashCode_m15388_gshared (DefaultComparer_t2133 * __this, Color32_t347  ___obj, const MethodInfo* method);
#define DefaultComparer_GetHashCode_m15388(__this, ___obj, method) (( int32_t (*) (DefaultComparer_t2133 *, Color32_t347 , const MethodInfo*))DefaultComparer_GetHashCode_m15388_gshared)(__this, ___obj, method)
// System.Boolean System.Collections.Generic.EqualityComparer`1/DefaultComparer<UnityEngine.Color32>::Equals(T,T)
extern "C" bool DefaultComparer_Equals_m15389_gshared (DefaultComparer_t2133 * __this, Color32_t347  ___x, Color32_t347  ___y, const MethodInfo* method);
#define DefaultComparer_Equals_m15389(__this, ___x, ___y, method) (( bool (*) (DefaultComparer_t2133 *, Color32_t347 , Color32_t347 , const MethodInfo*))DefaultComparer_Equals_m15389_gshared)(__this, ___x, ___y, method)
