﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_68.h"
#include "System_System_Security_Cryptography_X509Certificates_X509Cha_5.h"

// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17635_gshared (InternalEnumerator_1_t2333 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17635(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2333 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17635_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17636_gshared (InternalEnumerator_1_t2333 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17636(__this, method) (( void (*) (InternalEnumerator_1_t2333 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17636_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17637_gshared (InternalEnumerator_1_t2333 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17637(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2333 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17637_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17638_gshared (InternalEnumerator_1_t2333 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17638(__this, method) (( void (*) (InternalEnumerator_1_t2333 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17638_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17639_gshared (InternalEnumerator_1_t2333 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17639(__this, method) (( bool (*) (InternalEnumerator_1_t2333 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17639_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Security.Cryptography.X509Certificates.X509ChainStatus>::get_Current()
extern "C" X509ChainStatus_t966  InternalEnumerator_1_get_Current_m17640_gshared (InternalEnumerator_1_t2333 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17640(__this, method) (( X509ChainStatus_t966  (*) (InternalEnumerator_1_t2333 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17640_gshared)(__this, method)
