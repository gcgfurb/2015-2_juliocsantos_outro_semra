﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.Dropdown/DropdownItem
struct DropdownItem_t178;
// UnityEngine.UI.Text
struct Text_t97;
// UnityEngine.UI.Image
struct Image_t24;
// UnityEngine.RectTransform
struct RectTransform_t95;
// UnityEngine.UI.Toggle
struct Toggle_t179;
// UnityEngine.EventSystems.PointerEventData
struct PointerEventData_t57;
// UnityEngine.EventSystems.BaseEventData
struct BaseEventData_t107;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.Dropdown/DropdownItem::.ctor()
extern "C" void DropdownItem__ctor_m837 (DropdownItem_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Text UnityEngine.UI.Dropdown/DropdownItem::get_text()
extern "C" Text_t97 * DropdownItem_get_text_m838 (DropdownItem_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_text(UnityEngine.UI.Text)
extern "C" void DropdownItem_set_text_m839 (DropdownItem_t178 * __this, Text_t97 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Image UnityEngine.UI.Dropdown/DropdownItem::get_image()
extern "C" Image_t24 * DropdownItem_get_image_m840 (DropdownItem_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_image(UnityEngine.UI.Image)
extern "C" void DropdownItem_set_image_m841 (DropdownItem_t178 * __this, Image_t24 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.RectTransform UnityEngine.UI.Dropdown/DropdownItem::get_rectTransform()
extern "C" RectTransform_t95 * DropdownItem_get_rectTransform_m842 (DropdownItem_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_rectTransform(UnityEngine.RectTransform)
extern "C" void DropdownItem_set_rectTransform_m843 (DropdownItem_t178 * __this, RectTransform_t95 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Toggle UnityEngine.UI.Dropdown/DropdownItem::get_toggle()
extern "C" Toggle_t179 * DropdownItem_get_toggle_m844 (DropdownItem_t178 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::set_toggle(UnityEngine.UI.Toggle)
extern "C" void DropdownItem_set_toggle_m845 (DropdownItem_t178 * __this, Toggle_t179 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern "C" void DropdownItem_OnPointerEnter_m846 (DropdownItem_t178 * __this, PointerEventData_t57 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.Dropdown/DropdownItem::OnCancel(UnityEngine.EventSystems.BaseEventData)
extern "C" void DropdownItem_OnCancel_m847 (DropdownItem_t178 * __this, BaseEventData_t107 * ___eventData, const MethodInfo* method) IL2CPP_METHOD_ATTR;
