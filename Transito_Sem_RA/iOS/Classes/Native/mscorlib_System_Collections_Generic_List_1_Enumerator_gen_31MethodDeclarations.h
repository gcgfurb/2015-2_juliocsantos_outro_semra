﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_2MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::.ctor(System.Collections.Generic.List`1<T>)
#define Enumerator__ctor_m16117(__this, ___l, method) (( void (*) (Enumerator_t2206 *, List_1_t644 *, const MethodInfo*))Enumerator__ctor_m11324_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m16118(__this, method) (( void (*) (Enumerator_t2206 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m11325_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m16119(__this, method) (( Object_t * (*) (Enumerator_t2206 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m11326_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::Dispose()
#define Enumerator_Dispose_m16120(__this, method) (( void (*) (Enumerator_t2206 *, const MethodInfo*))Enumerator_Dispose_m11327_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::VerifyState()
#define Enumerator_VerifyState_m16121(__this, method) (( void (*) (Enumerator_t2206 *, const MethodInfo*))Enumerator_VerifyState_m11328_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::MoveNext()
#define Enumerator_MoveNext_m16122(__this, method) (( bool (*) (Enumerator_t2206 *, const MethodInfo*))Enumerator_MoveNext_m11329_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.ParticleSystem>::get_Current()
#define Enumerator_get_Current_m16123(__this, method) (( ParticleSystem_t55 * (*) (Enumerator_t2206 *, const MethodInfo*))Enumerator_get_Current_m11330_gshared)(__this, method)
