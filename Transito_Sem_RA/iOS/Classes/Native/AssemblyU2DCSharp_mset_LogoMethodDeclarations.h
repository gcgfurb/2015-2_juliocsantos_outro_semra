﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// mset.Logo
struct Logo_t81;

#include "codegen/il2cpp-codegen.h"

// System.Void mset.Logo::.ctor()
extern "C" void Logo__ctor_m354 (Logo_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mset.Logo::Reset()
extern "C" void Logo_Reset_m355 (Logo_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mset.Logo::Start()
extern "C" void Logo_Start_m356 (Logo_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mset.Logo::updateTexRect()
extern "C" void Logo_updateTexRect_m357 (Logo_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void mset.Logo::OnGUI()
extern "C" void Logo_OnGUI_m358 (Logo_t81 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
