﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"

extern "C" void SkeletonBone_t528_marshal(const SkeletonBone_t528& unmarshaled, SkeletonBone_t528_marshaled& marshaled);
extern "C" void SkeletonBone_t528_marshal_back(const SkeletonBone_t528_marshaled& marshaled, SkeletonBone_t528& unmarshaled);
extern "C" void SkeletonBone_t528_marshal_cleanup(SkeletonBone_t528_marshaled& marshaled);
