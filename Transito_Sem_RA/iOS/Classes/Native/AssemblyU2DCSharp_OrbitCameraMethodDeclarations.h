﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// OrbitCamera
struct OrbitCamera_t85;

#include "codegen/il2cpp-codegen.h"

// System.Void OrbitCamera::.ctor()
extern "C" void OrbitCamera__ctor_m359 (OrbitCamera_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Start()
extern "C" void OrbitCamera_Start_m360 (OrbitCamera_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::Update()
extern "C" void OrbitCamera_Update_m361 (OrbitCamera_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void OrbitCamera::LateUpdate()
extern "C" void OrbitCamera_LateUpdate_m362 (OrbitCamera_t85 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern "C" float OrbitCamera_ClampAngle_m363 (Object_t * __this /* static, unused */, float ___angle, float ___min, float ___max, const MethodInfo* method) IL2CPP_METHOD_ATTR;
