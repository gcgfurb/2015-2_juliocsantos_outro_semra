﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityEngine.MonoBehaviour[]
// UnityEngine.MonoBehaviour[]
struct MonoBehaviourU5BU5D_t2579  : public Array_t { };
// UnityEngine.Behaviour[]
// UnityEngine.Behaviour[]
struct BehaviourU5BU5D_t2580  : public Array_t { };
// UnityEngine.Component[]
// UnityEngine.Component[]
struct ComponentU5BU5D_t1898  : public Array_t { };
// UnityEngine.Object[]
// UnityEngine.Object[]
struct ObjectU5BU5D_t61  : public Array_t { };
// UnityEngine.Touch[]
// UnityEngine.Touch[]
struct TouchU5BU5D_t71  : public Array_t { };
// UnityEngine.AudioSource[]
// UnityEngine.AudioSource[]
struct AudioSourceU5BU5D_t73  : public Array_t { };
// UnityEngine.WheelCollider[]
// UnityEngine.WheelCollider[]
struct WheelColliderU5BU5D_t41  : public Array_t { };
// UnityEngine.Collider[]
// UnityEngine.Collider[]
struct ColliderU5BU5D_t2581  : public Array_t { };
// UnityEngine.GameObject[]
// UnityEngine.GameObject[]
struct GameObjectU5BU5D_t42  : public Array_t { };
// UnityEngine.Quaternion[]
// UnityEngine.Quaternion[]
struct QuaternionU5BU5D_t44  : public Array_t { };
// UnityEngine.Transform[]
// UnityEngine.Transform[]
struct TransformU5BU5D_t90  : public Array_t { };
// UnityEngine.RaycastHit2D[]
// UnityEngine.RaycastHit2D[]
struct RaycastHit2DU5BU5D_t366  : public Array_t { };
// UnityEngine.RaycastHit[]
// UnityEngine.RaycastHit[]
struct RaycastHitU5BU5D_t371  : public Array_t { };
// UnityEngine.Canvas[]
// UnityEngine.Canvas[]
struct CanvasU5BU5D_t1991  : public Array_t { };
// UnityEngine.Vector3[]
// UnityEngine.Vector3[]
struct Vector3U5BU5D_t217  : public Array_t { };
// UnityEngine.Font[]
// UnityEngine.Font[]
struct FontU5BU5D_t2003  : public Array_t { };
// UnityEngine.UIVertex[]
// UnityEngine.UIVertex[]
struct UIVertexU5BU5D_t234  : public Array_t { };
// UnityEngine.Vector2[]
// UnityEngine.Vector2[]
struct Vector2U5BU5D_t216  : public Array_t { };
// UnityEngine.UILineInfo[]
// UnityEngine.UILineInfo[]
struct UILineInfoU5BU5D_t648  : public Array_t { };
// UnityEngine.UICharInfo[]
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t647  : public Array_t { };
// UnityEngine.CanvasGroup[]
// UnityEngine.CanvasGroup[]
struct CanvasGroupU5BU5D_t2071  : public Array_t { };
// UnityEngine.RectTransform[]
// UnityEngine.RectTransform[]
struct RectTransformU5BU5D_t2099  : public Array_t { };
// UnityEngine.Color32[]
// UnityEngine.Color32[]
struct Color32U5BU5D_t424  : public Array_t { };
// UnityEngine.Vector4[]
// UnityEngine.Vector4[]
struct Vector4U5BU5D_t425  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievementDescription[]
// UnityEngine.SocialPlatforms.IAchievementDescription[]
struct IAchievementDescriptionU5BU5D_t657  : public Array_t { };
// UnityEngine.SocialPlatforms.IAchievement[]
// UnityEngine.SocialPlatforms.IAchievement[]
struct IAchievementU5BU5D_t659  : public Array_t { };
// UnityEngine.SocialPlatforms.IScore[]
// UnityEngine.SocialPlatforms.IScore[]
struct IScoreU5BU5D_t593  : public Array_t { };
// UnityEngine.SocialPlatforms.IUserProfile[]
// UnityEngine.SocialPlatforms.IUserProfile[]
struct IUserProfileU5BU5D_t589  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
// UnityEngine.SocialPlatforms.Impl.AchievementDescription[]
struct AchievementDescriptionU5BU5D_t449  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
// UnityEngine.SocialPlatforms.Impl.UserProfile[]
struct UserProfileU5BU5D_t450  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
// UnityEngine.SocialPlatforms.GameCenter.GcLeaderboard[]
struct GcLeaderboardU5BU5D_t2189  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
// UnityEngine.SocialPlatforms.GameCenter.GcAchievementData[]
struct GcAchievementDataU5BU5D_t637  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Achievement[]
// UnityEngine.SocialPlatforms.Impl.Achievement[]
struct AchievementU5BU5D_t658  : public Array_t { };
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
// UnityEngine.SocialPlatforms.GameCenter.GcScoreData[]
struct GcScoreDataU5BU5D_t638  : public Array_t { };
// UnityEngine.SocialPlatforms.Impl.Score[]
// UnityEngine.SocialPlatforms.Impl.Score[]
struct ScoreU5BU5D_t660  : public Array_t { };
// UnityEngine.Camera[]
// UnityEngine.Camera[]
struct CameraU5BU5D_t600  : public Array_t { };
// UnityEngine.Display[]
// UnityEngine.Display[]
struct DisplayU5BU5D_t491  : public Array_t { };
// UnityEngine.ParticleSystem[]
// UnityEngine.ParticleSystem[]
struct ParticleSystemU5BU5D_t643  : public Array_t { };
// UnityEngine.ContactPoint[]
// UnityEngine.ContactPoint[]
struct ContactPointU5BU5D_t504  : public Array_t { };
// UnityEngine.Rigidbody2D[]
// UnityEngine.Rigidbody2D[]
struct Rigidbody2DU5BU5D_t2209  : public Array_t { };
// UnityEngine.ContactPoint2D[]
// UnityEngine.ContactPoint2D[]
struct ContactPoint2DU5BU5D_t512  : public Array_t { };
// UnityEngine.Keyframe[]
// UnityEngine.Keyframe[]
struct KeyframeU5BU5D_t645  : public Array_t { };
// UnityEngine.CharacterInfo[]
// UnityEngine.CharacterInfo[]
struct CharacterInfoU5BU5D_t646  : public Array_t { };
// UnityEngine.GUILayoutOption[]
// UnityEngine.GUILayoutOption[]
struct GUILayoutOptionU5BU5D_t649  : public Array_t { };
// UnityEngine.GUILayoutUtility/LayoutCache[]
// UnityEngine.GUILayoutUtility/LayoutCache[]
struct LayoutCacheU5BU5D_t2238  : public Array_t { };
// UnityEngine.GUILayoutEntry[]
// UnityEngine.GUILayoutEntry[]
struct GUILayoutEntryU5BU5D_t2243  : public Array_t { };
// UnityEngine.GUIStyle[]
// UnityEngine.GUIStyle[]
struct GUIStyleU5BU5D_t561  : public Array_t { };
// UnityEngine.DisallowMultipleComponent[]
// UnityEngine.DisallowMultipleComponent[]
struct DisallowMultipleComponentU5BU5D_t569  : public Array_t { };
// UnityEngine.ExecuteInEditMode[]
// UnityEngine.ExecuteInEditMode[]
struct ExecuteInEditModeU5BU5D_t570  : public Array_t { };
// UnityEngine.RequireComponent[]
// UnityEngine.RequireComponent[]
struct RequireComponentU5BU5D_t571  : public Array_t { };
// UnityEngine.SendMouseEvents/HitInfo[]
// UnityEngine.SendMouseEvents/HitInfo[]
struct HitInfoU5BU5D_t599  : public Array_t { };
// UnityEngine.TextEditor/TextEditOp[]
// UnityEngine.TextEditor/TextEditOp[]
struct TextEditOpU5BU5D_t2265  : public Array_t { };
// UnityEngine.Event[]
// UnityEngine.Event[]
struct EventU5BU5D_t2264  : public Array_t { };
// UnityEngine.Events.PersistentCall[]
// UnityEngine.Events.PersistentCall[]
struct PersistentCallU5BU5D_t2292  : public Array_t { };
// UnityEngine.Events.BaseInvokableCall[]
// UnityEngine.Events.BaseInvokableCall[]
struct BaseInvokableCallU5BU5D_t2297  : public Array_t { };
