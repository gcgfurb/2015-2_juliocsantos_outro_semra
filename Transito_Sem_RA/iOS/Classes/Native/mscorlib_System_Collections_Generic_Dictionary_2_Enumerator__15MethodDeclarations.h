﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Object,System.Boolean>
struct Dictionary_2_t2316;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__15.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_15.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
extern "C" void Enumerator__ctor_m17553_gshared (Enumerator_t2323 * __this, Dictionary_2_t2316 * ___dictionary, const MethodInfo* method);
#define Enumerator__ctor_m17553(__this, ___dictionary, method) (( void (*) (Enumerator_t2323 *, Dictionary_2_t2316 *, const MethodInfo*))Enumerator__ctor_m17553_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m17554_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m17554(__this, method) (( Object_t * (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17554_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m17555_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m17555(__this, method) (( void (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17555_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Entry()
extern "C" DictionaryEntry_t1057  Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17556_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17556(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17556_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Key()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17557_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17557(__this, method) (( Object_t * (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17557_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::System.Collections.IDictionaryEnumerator.get_Value()
extern "C" Object_t * Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17558_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17558(__this, method) (( Object_t * (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17558_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::MoveNext()
extern "C" bool Enumerator_MoveNext_m17559_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m17559(__this, method) (( bool (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_MoveNext_m17559_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_Current()
extern "C" KeyValuePair_2_t2318  Enumerator_get_Current_m17560_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m17560(__this, method) (( KeyValuePair_2_t2318  (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_get_Current_m17560_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentKey()
extern "C" Object_t * Enumerator_get_CurrentKey_m17561_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentKey_m17561(__this, method) (( Object_t * (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_get_CurrentKey_m17561_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::get_CurrentValue()
extern "C" bool Enumerator_get_CurrentValue_m17562_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_get_CurrentValue_m17562(__this, method) (( bool (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_get_CurrentValue_m17562_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Reset()
extern "C" void Enumerator_Reset_m17563_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_Reset_m17563(__this, method) (( void (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_Reset_m17563_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyState()
extern "C" void Enumerator_VerifyState_m17564_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m17564(__this, method) (( void (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_VerifyState_m17564_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::VerifyCurrent()
extern "C" void Enumerator_VerifyCurrent_m17565_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_VerifyCurrent_m17565(__this, method) (( void (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_VerifyCurrent_m17565_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<System.Object,System.Boolean>::Dispose()
extern "C" void Enumerator_Dispose_m17566_gshared (Enumerator_t2323 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m17566(__this, method) (( void (*) (Enumerator_t2323 *, const MethodInfo*))Enumerator_Dispose_m17566_gshared)(__this, method)
