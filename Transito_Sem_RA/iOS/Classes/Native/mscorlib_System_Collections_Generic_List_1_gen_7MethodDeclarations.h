﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_gen_37MethodDeclarations.h"

// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.ctor()
#define List_1__ctor_m1911(__this, method) (( void (*) (List_1_t149 *, const MethodInfo*))List_1__ctor_m11228_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.ctor(System.Int32)
#define List_1__ctor_m12851(__this, ___capacity, method) (( void (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1__ctor_m11230_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::.cctor()
#define List_1__cctor_m12852(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))List_1__cctor_m11232_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m12853(__this, method) (( Object_t* (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11234_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define List_1_System_Collections_ICollection_CopyTo_m12854(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t149 *, Array_t *, int32_t, const MethodInfo*))List_1_System_Collections_ICollection_CopyTo_m11236_gshared)(__this, ___array, ___arrayIndex, method)
// System.Collections.IEnumerator System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IEnumerable.GetEnumerator()
#define List_1_System_Collections_IEnumerable_GetEnumerator_m12855(__this, method) (( Object_t * (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_IEnumerable_GetEnumerator_m11238_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Add(System.Object)
#define List_1_System_Collections_IList_Add_m12856(__this, ___item, method) (( int32_t (*) (List_1_t149 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Add_m11240_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Contains(System.Object)
#define List_1_System_Collections_IList_Contains_m12857(__this, ___item, method) (( bool (*) (List_1_t149 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Contains_m11242_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.IndexOf(System.Object)
#define List_1_System_Collections_IList_IndexOf_m12858(__this, ___item, method) (( int32_t (*) (List_1_t149 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_IndexOf_m11244_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Insert(System.Int32,System.Object)
#define List_1_System_Collections_IList_Insert_m12859(__this, ___index, ___item, method) (( void (*) (List_1_t149 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Insert_m11246_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.Remove(System.Object)
#define List_1_System_Collections_IList_Remove_m12860(__this, ___item, method) (( void (*) (List_1_t149 *, Object_t *, const MethodInfo*))List_1_System_Collections_IList_Remove_m11248_gshared)(__this, ___item, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12861(__this, method) (( bool (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11250_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.ICollection.get_IsSynchronized()
#define List_1_System_Collections_ICollection_get_IsSynchronized_m12862(__this, method) (( bool (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_ICollection_get_IsSynchronized_m11252_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.ICollection.get_SyncRoot()
#define List_1_System_Collections_ICollection_get_SyncRoot_m12863(__this, method) (( Object_t * (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_ICollection_get_SyncRoot_m11254_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.get_IsFixedSize()
#define List_1_System_Collections_IList_get_IsFixedSize_m12864(__this, method) (( bool (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_IList_get_IsFixedSize_m11256_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.get_IsReadOnly()
#define List_1_System_Collections_IList_get_IsReadOnly_m12865(__this, method) (( bool (*) (List_1_t149 *, const MethodInfo*))List_1_System_Collections_IList_get_IsReadOnly_m11258_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.get_Item(System.Int32)
#define List_1_System_Collections_IList_get_Item_m12866(__this, ___index, method) (( Object_t * (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1_System_Collections_IList_get_Item_m11260_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define List_1_System_Collections_IList_set_Item_m12867(__this, ___index, ___value, method) (( void (*) (List_1_t149 *, int32_t, Object_t *, const MethodInfo*))List_1_System_Collections_IList_set_Item_m11262_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Add(T)
#define List_1_Add_m12868(__this, ___item, method) (( void (*) (List_1_t149 *, ButtonState_t146 *, const MethodInfo*))List_1_Add_m11264_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::GrowIfNeeded(System.Int32)
#define List_1_GrowIfNeeded_m12869(__this, ___newCount, method) (( void (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1_GrowIfNeeded_m11266_gshared)(__this, ___newCount, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AddCollection(System.Collections.Generic.ICollection`1<T>)
#define List_1_AddCollection_m12870(__this, ___collection, method) (( void (*) (List_1_t149 *, Object_t*, const MethodInfo*))List_1_AddCollection_m11268_gshared)(__this, ___collection, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AddEnumerable(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddEnumerable_m12871(__this, ___enumerable, method) (( void (*) (List_1_t149 *, Object_t*, const MethodInfo*))List_1_AddEnumerable_m11270_gshared)(__this, ___enumerable, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AddRange(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_AddRange_m12872(__this, ___collection, method) (( void (*) (List_1_t149 *, Object_t*, const MethodInfo*))List_1_AddRange_m11272_gshared)(__this, ___collection, method)
// System.Collections.ObjectModel.ReadOnlyCollection`1<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::AsReadOnly()
#define List_1_AsReadOnly_m12873(__this, method) (( ReadOnlyCollection_1_t1961 * (*) (List_1_t149 *, const MethodInfo*))List_1_AsReadOnly_m11274_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Clear()
#define List_1_Clear_m12874(__this, method) (( void (*) (List_1_t149 *, const MethodInfo*))List_1_Clear_m11276_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Contains(T)
#define List_1_Contains_m12875(__this, ___item, method) (( bool (*) (List_1_t149 *, ButtonState_t146 *, const MethodInfo*))List_1_Contains_m11278_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CopyTo(T[],System.Int32)
#define List_1_CopyTo_m12876(__this, ___array, ___arrayIndex, method) (( void (*) (List_1_t149 *, ButtonStateU5BU5D_t1960*, int32_t, const MethodInfo*))List_1_CopyTo_m11280_gshared)(__this, ___array, ___arrayIndex, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Find(System.Predicate`1<T>)
#define List_1_Find_m12877(__this, ___match, method) (( ButtonState_t146 * (*) (List_1_t149 *, Predicate_1_t1963 *, const MethodInfo*))List_1_Find_m11282_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CheckMatch(System.Predicate`1<T>)
#define List_1_CheckMatch_m12878(__this /* static, unused */, ___match, method) (( void (*) (Object_t * /* static, unused */, Predicate_1_t1963 *, const MethodInfo*))List_1_CheckMatch_m11284_gshared)(__this /* static, unused */, ___match, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::GetIndex(System.Int32,System.Int32,System.Predicate`1<T>)
#define List_1_GetIndex_m12879(__this, ___startIndex, ___count, ___match, method) (( int32_t (*) (List_1_t149 *, int32_t, int32_t, Predicate_1_t1963 *, const MethodInfo*))List_1_GetIndex_m11286_gshared)(__this, ___startIndex, ___count, ___match, method)
// System.Collections.Generic.List`1/Enumerator<T> System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::GetEnumerator()
#define List_1_GetEnumerator_m12880(__this, method) (( Enumerator_t1964  (*) (List_1_t149 *, const MethodInfo*))List_1_GetEnumerator_m11288_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::IndexOf(T)
#define List_1_IndexOf_m12881(__this, ___item, method) (( int32_t (*) (List_1_t149 *, ButtonState_t146 *, const MethodInfo*))List_1_IndexOf_m11290_gshared)(__this, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Shift(System.Int32,System.Int32)
#define List_1_Shift_m12882(__this, ___start, ___delta, method) (( void (*) (List_1_t149 *, int32_t, int32_t, const MethodInfo*))List_1_Shift_m11292_gshared)(__this, ___start, ___delta, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CheckIndex(System.Int32)
#define List_1_CheckIndex_m12883(__this, ___index, method) (( void (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1_CheckIndex_m11294_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Insert(System.Int32,T)
#define List_1_Insert_m12884(__this, ___index, ___item, method) (( void (*) (List_1_t149 *, int32_t, ButtonState_t146 *, const MethodInfo*))List_1_Insert_m11296_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::CheckCollection(System.Collections.Generic.IEnumerable`1<T>)
#define List_1_CheckCollection_m12885(__this, ___collection, method) (( void (*) (List_1_t149 *, Object_t*, const MethodInfo*))List_1_CheckCollection_m11298_gshared)(__this, ___collection, method)
// System.Boolean System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Remove(T)
#define List_1_Remove_m12886(__this, ___item, method) (( bool (*) (List_1_t149 *, ButtonState_t146 *, const MethodInfo*))List_1_Remove_m11300_gshared)(__this, ___item, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::RemoveAll(System.Predicate`1<T>)
#define List_1_RemoveAll_m12887(__this, ___match, method) (( int32_t (*) (List_1_t149 *, Predicate_1_t1963 *, const MethodInfo*))List_1_RemoveAll_m11302_gshared)(__this, ___match, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::RemoveAt(System.Int32)
#define List_1_RemoveAt_m12888(__this, ___index, method) (( void (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1_RemoveAt_m11304_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Reverse()
#define List_1_Reverse_m12889(__this, method) (( void (*) (List_1_t149 *, const MethodInfo*))List_1_Reverse_m11306_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Sort()
#define List_1_Sort_m12890(__this, method) (( void (*) (List_1_t149 *, const MethodInfo*))List_1_Sort_m11308_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::Sort(System.Comparison`1<T>)
#define List_1_Sort_m12891(__this, ___comparison, method) (( void (*) (List_1_t149 *, Comparison_1_t1965 *, const MethodInfo*))List_1_Sort_m11310_gshared)(__this, ___comparison, method)
// T[] System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::ToArray()
#define List_1_ToArray_m12892(__this, method) (( ButtonStateU5BU5D_t1960* (*) (List_1_t149 *, const MethodInfo*))List_1_ToArray_m11311_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::TrimExcess()
#define List_1_TrimExcess_m12893(__this, method) (( void (*) (List_1_t149 *, const MethodInfo*))List_1_TrimExcess_m11313_gshared)(__this, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::get_Capacity()
#define List_1_get_Capacity_m12894(__this, method) (( int32_t (*) (List_1_t149 *, const MethodInfo*))List_1_get_Capacity_m11315_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::set_Capacity(System.Int32)
#define List_1_set_Capacity_m12895(__this, ___value, method) (( void (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1_set_Capacity_m11317_gshared)(__this, ___value, method)
// System.Int32 System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::get_Count()
#define List_1_get_Count_m12896(__this, method) (( int32_t (*) (List_1_t149 *, const MethodInfo*))List_1_get_Count_m11319_gshared)(__this, method)
// T System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::get_Item(System.Int32)
#define List_1_get_Item_m12897(__this, ___index, method) (( ButtonState_t146 * (*) (List_1_t149 *, int32_t, const MethodInfo*))List_1_get_Item_m11321_gshared)(__this, ___index, method)
// System.Void System.Collections.Generic.List`1<UnityEngine.EventSystems.PointerInputModule/ButtonState>::set_Item(System.Int32,T)
#define List_1_set_Item_m12898(__this, ___index, ___value, method) (( void (*) (List_1_t149 *, int32_t, ButtonState_t146 *, const MethodInfo*))List_1_set_Item_m11323_gshared)(__this, ___index, ___value, method)
