﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>
struct U3CStartU3Ec__Iterator0_t2017;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m13623_gshared (U3CStartU3Ec__Iterator0_t2017 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0__ctor_m13623(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2017 *, const MethodInfo*))U3CStartU3Ec__Iterator0__ctor_m13623_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13624_gshared (U3CStartU3Ec__Iterator0_t2017 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13624(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2017 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m13624_gshared)(__this, method)
// System.Object UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13625_gshared (U3CStartU3Ec__Iterator0_t2017 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13625(__this, method) (( Object_t * (*) (U3CStartU3Ec__Iterator0_t2017 *, const MethodInfo*))U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m13625_gshared)(__this, method)
// System.Boolean UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m13626_gshared (U3CStartU3Ec__Iterator0_t2017 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_MoveNext_m13626(__this, method) (( bool (*) (U3CStartU3Ec__Iterator0_t2017 *, const MethodInfo*))U3CStartU3Ec__Iterator0_MoveNext_m13626_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m13627_gshared (U3CStartU3Ec__Iterator0_t2017 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Dispose_m13627(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2017 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Dispose_m13627_gshared)(__this, method)
// System.Void UnityEngine.UI.CoroutineTween.TweenRunner`1/<Start>c__Iterator0<UnityEngine.UI.CoroutineTween.ColorTween>::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m13628_gshared (U3CStartU3Ec__Iterator0_t2017 * __this, const MethodInfo* method);
#define U3CStartU3Ec__Iterator0_Reset_m13628(__this, method) (( void (*) (U3CStartU3Ec__Iterator0_t2017 *, const MethodInfo*))U3CStartU3Ec__Iterator0_Reset_m13628_gshared)(__this, method)
