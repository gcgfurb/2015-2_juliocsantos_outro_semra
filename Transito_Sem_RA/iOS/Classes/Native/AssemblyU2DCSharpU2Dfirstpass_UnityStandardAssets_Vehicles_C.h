﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityStandardAssets.Vehicles.Car.CarController
struct CarController_t29;
// UnityEngine.Renderer
struct Renderer_t30;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// UnityStandardAssets.Vehicles.Car.BrakeLight
struct  BrakeLight_t28  : public MonoBehaviour_t2
{
	// UnityStandardAssets.Vehicles.Car.CarController UnityStandardAssets.Vehicles.Car.BrakeLight::car
	CarController_t29 * ___car_2;
	// UnityEngine.Renderer UnityStandardAssets.Vehicles.Car.BrakeLight::m_Renderer
	Renderer_t30 * ___m_Renderer_3;
};
