﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Text.RegularExpressions.Match
struct Match_t902;
// System.Collections.ArrayList
struct ArrayList_t729;

#include "mscorlib_System_Object.h"

// System.Text.RegularExpressions.MatchCollection
struct  MatchCollection_t901  : public Object_t
{
	// System.Text.RegularExpressions.Match System.Text.RegularExpressions.MatchCollection::current
	Match_t902 * ___current_0;
	// System.Collections.ArrayList System.Text.RegularExpressions.MatchCollection::list
	ArrayList_t729 * ___list_1;
};
