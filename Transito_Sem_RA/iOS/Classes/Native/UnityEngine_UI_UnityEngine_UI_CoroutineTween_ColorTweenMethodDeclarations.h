﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.Events.UnityAction`1<UnityEngine.Color>
struct UnityAction_1_t342;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UI_UnityEngine_UI_CoroutineTween_ColorTween_Colo.h"

// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_startColor()
extern "C" Color_t83  ColorTween_get_startColor_m735 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_startColor(UnityEngine.Color)
extern "C" void ColorTween_set_startColor_m736 (ColorTween_t160 * __this, Color_t83  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.Color UnityEngine.UI.CoroutineTween.ColorTween::get_targetColor()
extern "C" Color_t83  ColorTween_get_targetColor_m737 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_targetColor(UnityEngine.Color)
extern "C" void ColorTween_set_targetColor_m738 (ColorTween_t160 * __this, Color_t83  ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode UnityEngine.UI.CoroutineTween.ColorTween::get_tweenMode()
extern "C" int32_t ColorTween_get_tweenMode_m739 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_tweenMode(UnityEngine.UI.CoroutineTween.ColorTween/ColorTweenMode)
extern "C" void ColorTween_set_tweenMode_m740 (ColorTween_t160 * __this, int32_t ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::get_duration()
extern "C" float ColorTween_get_duration_m741 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_duration(System.Single)
extern "C" void ColorTween_set_duration_m742 (ColorTween_t160 * __this, float ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::get_ignoreTimeScale()
extern "C" bool ColorTween_get_ignoreTimeScale_m743 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::set_ignoreTimeScale(System.Boolean)
extern "C" void ColorTween_set_ignoreTimeScale_m744 (ColorTween_t160 * __this, bool ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::TweenValue(System.Single)
extern "C" void ColorTween_TweenValue_m745 (ColorTween_t160 * __this, float ___floatPercentage, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.CoroutineTween.ColorTween::AddOnChangedCallback(UnityEngine.Events.UnityAction`1<UnityEngine.Color>)
extern "C" void ColorTween_AddOnChangedCallback_m746 (ColorTween_t160 * __this, UnityAction_1_t342 * ___callback, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::GetIgnoreTimescale()
extern "C" bool ColorTween_GetIgnoreTimescale_m747 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single UnityEngine.UI.CoroutineTween.ColorTween::GetDuration()
extern "C" float ColorTween_GetDuration_m748 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityEngine.UI.CoroutineTween.ColorTween::ValidTarget()
extern "C" bool ColorTween_ValidTarget_m749 (ColorTween_t160 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
