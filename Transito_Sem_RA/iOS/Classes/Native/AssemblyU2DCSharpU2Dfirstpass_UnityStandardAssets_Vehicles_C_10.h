﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Object
struct Object_t;
// UnityStandardAssets.Vehicles.Car.SkidTrail
struct SkidTrail_t50;

#include "mscorlib_System_Object.h"

// UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0
struct  U3CStartU3Ec__Iterator0_t49  : public Object_t
{
	// System.Int32 UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::$PC
	int32_t ___U24PC_0;
	// System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::$current
	Object_t * ___U24current_1;
	// UnityStandardAssets.Vehicles.Car.SkidTrail UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::<>f__this
	SkidTrail_t50 * ___U3CU3Ef__this_2;
};
