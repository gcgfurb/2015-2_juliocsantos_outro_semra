﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>
struct DefaultComparer_t2226;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::.ctor()
extern "C" void DefaultComparer__ctor_m16388_gshared (DefaultComparer_t2226 * __this, const MethodInfo* method);
#define DefaultComparer__ctor_m16388(__this, method) (( void (*) (DefaultComparer_t2226 *, const MethodInfo*))DefaultComparer__ctor_m16388_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Comparer`1/DefaultComparer<UnityEngine.UICharInfo>::Compare(T,T)
extern "C" int32_t DefaultComparer_Compare_m16389_gshared (DefaultComparer_t2226 * __this, UICharInfo_t396  ___x, UICharInfo_t396  ___y, const MethodInfo* method);
#define DefaultComparer_Compare_m16389(__this, ___x, ___y, method) (( int32_t (*) (DefaultComparer_t2226 *, UICharInfo_t396 , UICharInfo_t396 , const MethodInfo*))DefaultComparer_Compare_m16389_gshared)(__this, ___x, ___y, method)
