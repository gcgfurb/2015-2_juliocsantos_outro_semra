﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<System.Reflection.CustomAttributeNamedArgument>
struct List_1_t2388;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_38.h"
#include "mscorlib_System_Reflection_CustomAttributeNamedArgument.h"

// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m18163_gshared (Enumerator_t2389 * __this, List_1_t2388 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m18163(__this, ___l, method) (( void (*) (Enumerator_t2389 *, List_1_t2388 *, const MethodInfo*))Enumerator__ctor_m18163_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m18164_gshared (Enumerator_t2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m18164(__this, method) (( void (*) (Enumerator_t2389 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m18164_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m18165_gshared (Enumerator_t2389 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m18165(__this, method) (( Object_t * (*) (Enumerator_t2389 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m18165_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::Dispose()
extern "C" void Enumerator_Dispose_m18166_gshared (Enumerator_t2389 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m18166(__this, method) (( void (*) (Enumerator_t2389 *, const MethodInfo*))Enumerator_Dispose_m18166_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::VerifyState()
extern "C" void Enumerator_VerifyState_m18167_gshared (Enumerator_t2389 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m18167(__this, method) (( void (*) (Enumerator_t2389 *, const MethodInfo*))Enumerator_VerifyState_m18167_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::MoveNext()
extern "C" bool Enumerator_MoveNext_m18168_gshared (Enumerator_t2389 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m18168(__this, method) (( bool (*) (Enumerator_t2389 *, const MethodInfo*))Enumerator_MoveNext_m18168_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<System.Reflection.CustomAttributeNamedArgument>::get_Current()
extern "C" CustomAttributeNamedArgument_t1325  Enumerator_get_Current_m18169_gshared (Enumerator_t2389 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m18169(__this, method) (( CustomAttributeNamedArgument_t1325  (*) (Enumerator_t2389 *, const MethodInfo*))Enumerator_get_Current_m18169_gshared)(__this, method)
