﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_13MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m17118(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2281 *, Event_t237 *, int32_t, const MethodInfo*))KeyValuePair_2__ctor_m17041_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Key()
#define KeyValuePair_2_get_Key_m17119(__this, method) (( Event_t237 * (*) (KeyValuePair_2_t2281 *, const MethodInfo*))KeyValuePair_2_get_Key_m17042_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m17120(__this, ___value, method) (( void (*) (KeyValuePair_2_t2281 *, Event_t237 *, const MethodInfo*))KeyValuePair_2_set_Key_m17043_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Value()
#define KeyValuePair_2_get_Value_m17121(__this, method) (( int32_t (*) (KeyValuePair_2_t2281 *, const MethodInfo*))KeyValuePair_2_get_Value_m17044_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m17122(__this, ___value, method) (( void (*) (KeyValuePair_2_t2281 *, int32_t, const MethodInfo*))KeyValuePair_2_set_Value_m17045_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::ToString()
#define KeyValuePair_2_ToString_m17123(__this, method) (( String_t* (*) (KeyValuePair_2_t2281 *, const MethodInfo*))KeyValuePair_2_ToString_m17046_gshared)(__this, method)
