﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>


#include "UnityEngine_UnityEngine_YieldInstruction.h"

// UnityEngine.WaitForSeconds
struct  WaitForSeconds_t101  : public YieldInstruction_t439
{
	// System.Single UnityEngine.WaitForSeconds::m_Seconds
	float ___m_Seconds_0;
};
// Native definition for marshalling of: UnityEngine.WaitForSeconds
struct WaitForSeconds_t101_marshaled
{
	float ___m_Seconds_0;
};
