﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.SkidTrail
struct SkidTrail_t50;
// System.Collections.IEnumerator
struct IEnumerator_t59;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail::.ctor()
extern "C" void SkidTrail__ctor_m190 (SkidTrail_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Collections.IEnumerator UnityStandardAssets.Vehicles.Car.SkidTrail::Start()
extern "C" Object_t * SkidTrail_Start_m191 (SkidTrail_t50 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
