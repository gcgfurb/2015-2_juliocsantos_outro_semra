﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_14.h"
#include "UnityEngine_UnityEngine_Quaternion.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m11492_gshared (InternalEnumerator_1_t1860 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m11492(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1860 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m11492_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11493_gshared (InternalEnumerator_1_t1860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11493(__this, method) (( void (*) (InternalEnumerator_1_t1860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11493_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11494_gshared (InternalEnumerator_1_t1860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11494(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1860 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11494_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m11495_gshared (InternalEnumerator_1_t1860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m11495(__this, method) (( void (*) (InternalEnumerator_1_t1860 *, const MethodInfo*))InternalEnumerator_1_Dispose_m11495_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m11496_gshared (InternalEnumerator_1_t1860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m11496(__this, method) (( bool (*) (InternalEnumerator_1_t1860 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m11496_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.Quaternion>::get_Current()
extern "C" Quaternion_t45  InternalEnumerator_1_get_Current_m11497_gshared (InternalEnumerator_1_t1860 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m11497(__this, method) (( Quaternion_t45  (*) (InternalEnumerator_1_t1860 *, const MethodInfo*))InternalEnumerator_1_get_Current_m11497_gshared)(__this, method)
