﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector2>
struct List_1_t314;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_29.h"
#include "UnityEngine_UnityEngine_Vector2.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15452_gshared (Enumerator_t2138 * __this, List_1_t314 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15452(__this, ___l, method) (( void (*) (Enumerator_t2138 *, List_1_t314 *, const MethodInfo*))Enumerator__ctor_m15452_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15453_gshared (Enumerator_t2138 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15453(__this, method) (( void (*) (Enumerator_t2138 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15453_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15454_gshared (Enumerator_t2138 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15454(__this, method) (( Object_t * (*) (Enumerator_t2138 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15454_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::Dispose()
extern "C" void Enumerator_Dispose_m15455_gshared (Enumerator_t2138 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15455(__this, method) (( void (*) (Enumerator_t2138 *, const MethodInfo*))Enumerator_Dispose_m15455_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::VerifyState()
extern "C" void Enumerator_VerifyState_m15456_gshared (Enumerator_t2138 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15456(__this, method) (( void (*) (Enumerator_t2138 *, const MethodInfo*))Enumerator_VerifyState_m15456_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15457_gshared (Enumerator_t2138 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15457(__this, method) (( bool (*) (Enumerator_t2138 *, const MethodInfo*))Enumerator_MoveNext_m15457_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector2>::get_Current()
extern "C" Vector2_t23  Enumerator_get_Current_m15458_gshared (Enumerator_t2138 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15458(__this, method) (( Vector2_t23  (*) (Enumerator_t2138 *, const MethodInfo*))Enumerator_get_Current_m15458_gshared)(__this, method)
