﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__13MethodDeclarations.h"

// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::.ctor(System.Collections.Generic.Dictionary`2<TKey,TValue>)
#define Enumerator__ctor_m17138(__this, ___dictionary, method) (( void (*) (Enumerator_t2283 *, Dictionary_2_t617 *, const MethodInfo*))Enumerator__ctor_m17073_gshared)(__this, ___dictionary, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.get_Current()
#define Enumerator_System_Collections_IEnumerator_get_Current_m17139(__this, method) (( Object_t * (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m17074_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IEnumerator.Reset()
#define Enumerator_System_Collections_IEnumerator_Reset_m17140(__this, method) (( void (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m17075_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Entry()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17141(__this, method) (( DictionaryEntry_t1057  (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Entry_m17076_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Key()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17142(__this, method) (( Object_t * (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Key_m17077_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::System.Collections.IDictionaryEnumerator.get_Value()
#define Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17143(__this, method) (( Object_t * (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_System_Collections_IDictionaryEnumerator_get_Value_m17078_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::MoveNext()
#define Enumerator_MoveNext_m17144(__this, method) (( bool (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_MoveNext_m17079_gshared)(__this, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_Current()
#define Enumerator_get_Current_m17145(__this, method) (( KeyValuePair_2_t2281  (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_get_Current_m17080_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentKey()
#define Enumerator_get_CurrentKey_m17146(__this, method) (( Event_t237 * (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_get_CurrentKey_m17081_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::get_CurrentValue()
#define Enumerator_get_CurrentValue_m17147(__this, method) (( int32_t (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_get_CurrentValue_m17082_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Reset()
#define Enumerator_Reset_m17148(__this, method) (( void (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_Reset_m17083_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyState()
#define Enumerator_VerifyState_m17149(__this, method) (( void (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_VerifyState_m17084_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::VerifyCurrent()
#define Enumerator_VerifyCurrent_m17150(__this, method) (( void (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_VerifyCurrent_m17085_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2/Enumerator<UnityEngine.Event,UnityEngine.TextEditor/TextEditOp>::Dispose()
#define Enumerator_Dispose_m17151(__this, method) (( void (*) (Enumerator_t2283 *, const MethodInfo*))Enumerator_Dispose_m17086_gshared)(__this, method)
