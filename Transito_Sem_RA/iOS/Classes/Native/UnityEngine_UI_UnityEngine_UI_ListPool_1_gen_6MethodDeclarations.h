﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UIVertex>
struct List_1_t345;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::.cctor()
extern "C" void ListPool_1__cctor_m15816_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1__cctor_m15816(__this /* static, unused */, method) (( void (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1__cctor_m15816_gshared)(__this /* static, unused */, method)
// System.Collections.Generic.List`1<T> UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Get()
extern "C" List_1_t345 * ListPool_1_Get_m2440_gshared (Object_t * __this /* static, unused */, const MethodInfo* method);
#define ListPool_1_Get_m2440(__this /* static, unused */, method) (( List_1_t345 * (*) (Object_t * /* static, unused */, const MethodInfo*))ListPool_1_Get_m2440_gshared)(__this /* static, unused */, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::Release(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_Release_m2443_gshared (Object_t * __this /* static, unused */, List_1_t345 * ___toRelease, const MethodInfo* method);
#define ListPool_1_Release_m2443(__this /* static, unused */, ___toRelease, method) (( void (*) (Object_t * /* static, unused */, List_1_t345 *, const MethodInfo*))ListPool_1_Release_m2443_gshared)(__this /* static, unused */, ___toRelease, method)
// System.Void UnityEngine.UI.ListPool`1<UnityEngine.UIVertex>::<s_ListPool>m__14(System.Collections.Generic.List`1<T>)
extern "C" void ListPool_1_U3Cs_ListPoolU3Em__14_m15817_gshared (Object_t * __this /* static, unused */, List_1_t345 * ___l, const MethodInfo* method);
#define ListPool_1_U3Cs_ListPoolU3Em__14_m15817(__this /* static, unused */, ___l, method) (( void (*) (Object_t * /* static, unused */, List_1_t345 *, const MethodInfo*))ListPool_1_U3Cs_ListPoolU3Em__14_m15817_gshared)(__this /* static, unused */, ___l, method)
