﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.UILineInfo>
struct List_1_t536;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_34.h"
#include "UnityEngine_UnityEngine_UILineInfo.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m16442_gshared (Enumerator_t2228 * __this, List_1_t536 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m16442(__this, ___l, method) (( void (*) (Enumerator_t2228 *, List_1_t536 *, const MethodInfo*))Enumerator__ctor_m16442_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m16443_gshared (Enumerator_t2228 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m16443(__this, method) (( void (*) (Enumerator_t2228 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m16443_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m16444_gshared (Enumerator_t2228 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m16444(__this, method) (( Object_t * (*) (Enumerator_t2228 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m16444_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::Dispose()
extern "C" void Enumerator_Dispose_m16445_gshared (Enumerator_t2228 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m16445(__this, method) (( void (*) (Enumerator_t2228 *, const MethodInfo*))Enumerator_Dispose_m16445_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::VerifyState()
extern "C" void Enumerator_VerifyState_m16446_gshared (Enumerator_t2228 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m16446(__this, method) (( void (*) (Enumerator_t2228 *, const MethodInfo*))Enumerator_VerifyState_m16446_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::MoveNext()
extern "C" bool Enumerator_MoveNext_m16447_gshared (Enumerator_t2228 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m16447(__this, method) (( bool (*) (Enumerator_t2228 *, const MethodInfo*))Enumerator_MoveNext_m16447_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.UILineInfo>::get_Current()
extern "C" UILineInfo_t394  Enumerator_get_Current_m16448_gshared (Enumerator_t2228 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m16448(__this, method) (( UILineInfo_t394  (*) (Enumerator_t2228 *, const MethodInfo*))Enumerator_get_Current_m16448_gshared)(__this, method)
