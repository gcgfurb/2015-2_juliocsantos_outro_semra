﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "mscorlib_System_Array.h"

#pragma once
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton[]
// UnityStandardAssets.CrossPlatformInput.AxisTouchButton[]
struct AxisTouchButtonU5BU5D_t60  : public Array_t { };
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis[]
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis[]
struct VirtualAxisU5BU5D_t1811  : public Array_t { };
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton[]
// UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualButton[]
struct VirtualButtonU5BU5D_t1835  : public Array_t { };
// UnityStandardAssets.Vehicles.Car.WheelEffects[]
// UnityStandardAssets.Vehicles.Car.WheelEffects[]
struct WheelEffectsU5BU5D_t43  : public Array_t { };
