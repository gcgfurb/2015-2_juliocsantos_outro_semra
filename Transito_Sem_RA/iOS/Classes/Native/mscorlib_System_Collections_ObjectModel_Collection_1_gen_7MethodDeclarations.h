﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>
struct Collection_1_t2221;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// UnityEngine.UICharInfo[]
struct UICharInfoU5BU5D_t647;
// System.Collections.Generic.IEnumerator`1<UnityEngine.UICharInfo>
struct IEnumerator_1_t2490;
// System.Collections.Generic.IList`1<UnityEngine.UICharInfo>
struct IList_1_t397;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::.ctor()
extern "C" void Collection_1__ctor_m16336_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1__ctor_m16336(__this, method) (( void (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1__ctor_m16336_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16337_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16337(__this, method) (( bool (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m16337_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m16338_gshared (Collection_1_t2221 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m16338(__this, ___array, ___index, method) (( void (*) (Collection_1_t2221 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m16338_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m16339_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m16339(__this, method) (( Object_t * (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m16339_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m16340_gshared (Collection_1_t2221 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m16340(__this, ___value, method) (( int32_t (*) (Collection_1_t2221 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m16340_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m16341_gshared (Collection_1_t2221 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m16341(__this, ___value, method) (( bool (*) (Collection_1_t2221 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m16341_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m16342_gshared (Collection_1_t2221 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m16342(__this, ___value, method) (( int32_t (*) (Collection_1_t2221 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m16342_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m16343_gshared (Collection_1_t2221 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m16343(__this, ___index, ___value, method) (( void (*) (Collection_1_t2221 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m16343_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m16344_gshared (Collection_1_t2221 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m16344(__this, ___value, method) (( void (*) (Collection_1_t2221 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m16344_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m16345_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m16345(__this, method) (( bool (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m16345_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m16346_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m16346(__this, method) (( Object_t * (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m16346_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m16347_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m16347(__this, method) (( bool (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m16347_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m16348_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m16348(__this, method) (( bool (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m16348_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m16349_gshared (Collection_1_t2221 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m16349(__this, ___index, method) (( Object_t * (*) (Collection_1_t2221 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m16349_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m16350_gshared (Collection_1_t2221 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m16350(__this, ___index, ___value, method) (( void (*) (Collection_1_t2221 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m16350_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Add(T)
extern "C" void Collection_1_Add_m16351_gshared (Collection_1_t2221 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_Add_m16351(__this, ___item, method) (( void (*) (Collection_1_t2221 *, UICharInfo_t396 , const MethodInfo*))Collection_1_Add_m16351_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Clear()
extern "C" void Collection_1_Clear_m16352_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_Clear_m16352(__this, method) (( void (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_Clear_m16352_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ClearItems()
extern "C" void Collection_1_ClearItems_m16353_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m16353(__this, method) (( void (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_ClearItems_m16353_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Contains(T)
extern "C" bool Collection_1_Contains_m16354_gshared (Collection_1_t2221 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_Contains_m16354(__this, ___item, method) (( bool (*) (Collection_1_t2221 *, UICharInfo_t396 , const MethodInfo*))Collection_1_Contains_m16354_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m16355_gshared (Collection_1_t2221 * __this, UICharInfoU5BU5D_t647* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m16355(__this, ___array, ___index, method) (( void (*) (Collection_1_t2221 *, UICharInfoU5BU5D_t647*, int32_t, const MethodInfo*))Collection_1_CopyTo_m16355_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m16356_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m16356(__this, method) (( Object_t* (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_GetEnumerator_m16356_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m16357_gshared (Collection_1_t2221 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m16357(__this, ___item, method) (( int32_t (*) (Collection_1_t2221 *, UICharInfo_t396 , const MethodInfo*))Collection_1_IndexOf_m16357_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m16358_gshared (Collection_1_t2221 * __this, int32_t ___index, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_Insert_m16358(__this, ___index, ___item, method) (( void (*) (Collection_1_t2221 *, int32_t, UICharInfo_t396 , const MethodInfo*))Collection_1_Insert_m16358_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m16359_gshared (Collection_1_t2221 * __this, int32_t ___index, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m16359(__this, ___index, ___item, method) (( void (*) (Collection_1_t2221 *, int32_t, UICharInfo_t396 , const MethodInfo*))Collection_1_InsertItem_m16359_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::Remove(T)
extern "C" bool Collection_1_Remove_m16360_gshared (Collection_1_t2221 * __this, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_Remove_m16360(__this, ___item, method) (( bool (*) (Collection_1_t2221 *, UICharInfo_t396 , const MethodInfo*))Collection_1_Remove_m16360_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m16361_gshared (Collection_1_t2221 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m16361(__this, ___index, method) (( void (*) (Collection_1_t2221 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m16361_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m16362_gshared (Collection_1_t2221 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m16362(__this, ___index, method) (( void (*) (Collection_1_t2221 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m16362_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Count()
extern "C" int32_t Collection_1_get_Count_m16363_gshared (Collection_1_t2221 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m16363(__this, method) (( int32_t (*) (Collection_1_t2221 *, const MethodInfo*))Collection_1_get_Count_m16363_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::get_Item(System.Int32)
extern "C" UICharInfo_t396  Collection_1_get_Item_m16364_gshared (Collection_1_t2221 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m16364(__this, ___index, method) (( UICharInfo_t396  (*) (Collection_1_t2221 *, int32_t, const MethodInfo*))Collection_1_get_Item_m16364_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m16365_gshared (Collection_1_t2221 * __this, int32_t ___index, UICharInfo_t396  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m16365(__this, ___index, ___value, method) (( void (*) (Collection_1_t2221 *, int32_t, UICharInfo_t396 , const MethodInfo*))Collection_1_set_Item_m16365_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m16366_gshared (Collection_1_t2221 * __this, int32_t ___index, UICharInfo_t396  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m16366(__this, ___index, ___item, method) (( void (*) (Collection_1_t2221 *, int32_t, UICharInfo_t396 , const MethodInfo*))Collection_1_SetItem_m16366_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m16367_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m16367(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m16367_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::ConvertItem(System.Object)
extern "C" UICharInfo_t396  Collection_1_ConvertItem_m16368_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m16368(__this /* static, unused */, ___item, method) (( UICharInfo_t396  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m16368_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m16369_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m16369(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m16369_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m16370_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m16370(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m16370_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.UICharInfo>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m16371_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m16371(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m16371_gshared)(__this /* static, unused */, ___list, method)
