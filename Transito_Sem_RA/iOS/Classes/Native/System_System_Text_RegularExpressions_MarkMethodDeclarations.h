﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Text_RegularExpressions_Mark.h"

// System.Boolean System.Text.RegularExpressions.Mark::get_IsDefined()
extern "C" bool Mark_get_IsDefined_m5350 (Mark_t1009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Mark::get_Index()
extern "C" int32_t Mark_get_Index_m5351 (Mark_t1009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.Text.RegularExpressions.Mark::get_Length()
extern "C" int32_t Mark_get_Length_m5352 (Mark_t1009 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
