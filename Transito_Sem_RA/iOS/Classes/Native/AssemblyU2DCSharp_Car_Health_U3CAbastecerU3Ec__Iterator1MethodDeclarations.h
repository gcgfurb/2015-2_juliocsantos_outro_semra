﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Car_Health/<Abastecer>c__Iterator1
struct U3CAbastecerU3Ec__Iterator1_t92;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void Car_Health/<Abastecer>c__Iterator1::.ctor()
extern "C" void U3CAbastecerU3Ec__Iterator1__ctor_m383 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Car_Health/<Abastecer>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAbastecerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m384 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object Car_Health/<Abastecer>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAbastecerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m385 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean Car_Health/<Abastecer>c__Iterator1::MoveNext()
extern "C" bool U3CAbastecerU3Ec__Iterator1_MoveNext_m386 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health/<Abastecer>c__Iterator1::Dispose()
extern "C" void U3CAbastecerU3Ec__Iterator1_Dispose_m387 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Car_Health/<Abastecer>c__Iterator1::Reset()
extern "C" void U3CAbastecerU3Ec__Iterator1_Reset_m388 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
