﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m11107(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t1832 *, String_t*, VirtualAxis_t3 *, const MethodInfo*))KeyValuePair_2__ctor_m11006_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Key()
#define KeyValuePair_2_get_Key_m11108(__this, method) (( String_t* (*) (KeyValuePair_2_t1832 *, const MethodInfo*))KeyValuePair_2_get_Key_m11007_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m11109(__this, ___value, method) (( void (*) (KeyValuePair_2_t1832 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m11008_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::get_Value()
#define KeyValuePair_2_get_Value_m11110(__this, method) (( VirtualAxis_t3 * (*) (KeyValuePair_2_t1832 *, const MethodInfo*))KeyValuePair_2_get_Value_m11009_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m11111(__this, ___value, method) (( void (*) (KeyValuePair_2_t1832 *, VirtualAxis_t3 *, const MethodInfo*))KeyValuePair_2_set_Value_m11010_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityStandardAssets.CrossPlatformInput.CrossPlatformInputManager/VirtualAxis>::ToString()
#define KeyValuePair_2_ToString_m11112(__this, method) (( String_t* (*) (KeyValuePair_2_t1832 *, const MethodInfo*))KeyValuePair_2_ToString_m11011_gshared)(__this, method)
