﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_26.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m14102_gshared (InternalEnumerator_1_t2049 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m14102(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2049 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m14102_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14103_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14103(__this, method) (( void (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m14103_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14104_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14104(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m14104_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m14105_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m14105(__this, method) (( void (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_Dispose_m14105_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m14106_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m14106(__this, method) (( bool (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m14106_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.UICharInfo>::get_Current()
extern "C" UICharInfo_t396  InternalEnumerator_1_get_Current_m14107_gshared (InternalEnumerator_1_t2049 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m14107(__this, method) (( UICharInfo_t396  (*) (InternalEnumerator_1_t2049 *, const MethodInfo*))InternalEnumerator_1_get_Current_m14107_gshared)(__this, method)
