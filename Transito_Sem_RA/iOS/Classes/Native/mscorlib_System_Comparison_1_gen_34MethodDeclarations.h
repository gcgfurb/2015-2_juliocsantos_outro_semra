﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Comparison`1<UnityEngine.UICharInfo>
struct Comparison_1_t2227;
// System.Object
struct Object_t;
// System.IAsyncResult
struct IAsyncResult_t228;
// System.AsyncCallback
struct AsyncCallback_t229;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_IntPtr.h"
#include "UnityEngine_UnityEngine_UICharInfo.h"

// System.Void System.Comparison`1<UnityEngine.UICharInfo>::.ctor(System.Object,System.IntPtr)
extern "C" void Comparison_1__ctor_m16390_gshared (Comparison_1_t2227 * __this, Object_t * ___object, IntPtr_t ___method, const MethodInfo* method);
#define Comparison_1__ctor_m16390(__this, ___object, ___method, method) (( void (*) (Comparison_1_t2227 *, Object_t *, IntPtr_t, const MethodInfo*))Comparison_1__ctor_m16390_gshared)(__this, ___object, ___method, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::Invoke(T,T)
extern "C" int32_t Comparison_1_Invoke_m16391_gshared (Comparison_1_t2227 * __this, UICharInfo_t396  ___x, UICharInfo_t396  ___y, const MethodInfo* method);
#define Comparison_1_Invoke_m16391(__this, ___x, ___y, method) (( int32_t (*) (Comparison_1_t2227 *, UICharInfo_t396 , UICharInfo_t396 , const MethodInfo*))Comparison_1_Invoke_m16391_gshared)(__this, ___x, ___y, method)
// System.IAsyncResult System.Comparison`1<UnityEngine.UICharInfo>::BeginInvoke(T,T,System.AsyncCallback,System.Object)
extern "C" Object_t * Comparison_1_BeginInvoke_m16392_gshared (Comparison_1_t2227 * __this, UICharInfo_t396  ___x, UICharInfo_t396  ___y, AsyncCallback_t229 * ___callback, Object_t * ___object, const MethodInfo* method);
#define Comparison_1_BeginInvoke_m16392(__this, ___x, ___y, ___callback, ___object, method) (( Object_t * (*) (Comparison_1_t2227 *, UICharInfo_t396 , UICharInfo_t396 , AsyncCallback_t229 *, Object_t *, const MethodInfo*))Comparison_1_BeginInvoke_m16392_gshared)(__this, ___x, ___y, ___callback, ___object, method)
// System.Int32 System.Comparison`1<UnityEngine.UICharInfo>::EndInvoke(System.IAsyncResult)
extern "C" int32_t Comparison_1_EndInvoke_m16393_gshared (Comparison_1_t2227 * __this, Object_t * ___result, const MethodInfo* method);
#define Comparison_1_EndInvoke_m16393(__this, ___result, method) (( int32_t (*) (Comparison_1_t2227 *, Object_t *, const MethodInfo*))Comparison_1_EndInvoke_m16393_gshared)(__this, ___result, method)
