﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern const Il2CppType Object_t_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Types[] = { &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0 = { 1, GenInst_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0 = { 2, GenInst_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 4, GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType EventSystem_t65_0_0_0;
static const Il2CppType* GenInst_EventSystem_t65_0_0_0_Types[] = { &EventSystem_t65_0_0_0 };
extern const Il2CppGenericInst GenInst_EventSystem_t65_0_0_0 = { 1, GenInst_EventSystem_t65_0_0_0_Types };
extern const Il2CppType StandaloneInputModule_t66_0_0_0;
static const Il2CppType* GenInst_StandaloneInputModule_t66_0_0_0_Types[] = { &StandaloneInputModule_t66_0_0_0 };
extern const Il2CppGenericInst GenInst_StandaloneInputModule_t66_0_0_0 = { 1, GenInst_StandaloneInputModule_t66_0_0_0_Types };
extern const Il2CppType TouchInputModule_t67_0_0_0;
static const Il2CppType* GenInst_TouchInputModule_t67_0_0_0_Types[] = { &TouchInputModule_t67_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchInputModule_t67_0_0_0 = { 1, GenInst_TouchInputModule_t67_0_0_0_Types };
extern const Il2CppType Image_t24_0_0_0;
static const Il2CppType* GenInst_Image_t24_0_0_0_Types[] = { &Image_t24_0_0_0 };
extern const Il2CppGenericInst GenInst_Image_t24_0_0_0 = { 1, GenInst_Image_t24_0_0_0_Types };
extern const Il2CppType String_t_0_0_0;
extern const Il2CppType VirtualAxis_t3_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t3_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0 = { 2, GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0_Types };
extern const Il2CppType VirtualButton_t6_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t6_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0 = { 2, GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Types[] = { &String_t_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0 = { 1, GenInst_String_t_0_0_0_Types };
extern const Il2CppType Renderer_t30_0_0_0;
static const Il2CppType* GenInst_Renderer_t30_0_0_0_Types[] = { &Renderer_t30_0_0_0 };
extern const Il2CppGenericInst GenInst_Renderer_t30_0_0_0 = { 1, GenInst_Renderer_t30_0_0_0_Types };
extern const Il2CppType CarController_t29_0_0_0;
static const Il2CppType* GenInst_CarController_t29_0_0_0_Types[] = { &CarController_t29_0_0_0 };
extern const Il2CppGenericInst GenInst_CarController_t29_0_0_0 = { 1, GenInst_CarController_t29_0_0_0_Types };
extern const Il2CppType Rigidbody_t34_0_0_0;
static const Il2CppType* GenInst_Rigidbody_t34_0_0_0_Types[] = { &Rigidbody_t34_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody_t34_0_0_0 = { 1, GenInst_Rigidbody_t34_0_0_0_Types };
extern const Il2CppType CarAIControl_t32_0_0_0;
static const Il2CppType* GenInst_CarAIControl_t32_0_0_0_Types[] = { &CarAIControl_t32_0_0_0 };
extern const Il2CppGenericInst GenInst_CarAIControl_t32_0_0_0 = { 1, GenInst_CarAIControl_t32_0_0_0_Types };
extern const Il2CppType AudioSource_t38_0_0_0;
static const Il2CppType* GenInst_AudioSource_t38_0_0_0_Types[] = { &AudioSource_t38_0_0_0 };
extern const Il2CppGenericInst GenInst_AudioSource_t38_0_0_0 = { 1, GenInst_AudioSource_t38_0_0_0_Types };
extern const Il2CppType Transform_t33_0_0_0;
static const Il2CppType* GenInst_Transform_t33_0_0_0_Types[] = { &Transform_t33_0_0_0 };
extern const Il2CppGenericInst GenInst_Transform_t33_0_0_0 = { 1, GenInst_Transform_t33_0_0_0_Types };
extern const Il2CppType ParticleSystem_t55_0_0_0;
static const Il2CppType* GenInst_ParticleSystem_t55_0_0_0_Types[] = { &ParticleSystem_t55_0_0_0 };
extern const Il2CppGenericInst GenInst_ParticleSystem_t55_0_0_0 = { 1, GenInst_ParticleSystem_t55_0_0_0_Types };
extern const Il2CppType WheelCollider_t56_0_0_0;
static const Il2CppType* GenInst_WheelCollider_t56_0_0_0_Types[] = { &WheelCollider_t56_0_0_0 };
extern const Il2CppGenericInst GenInst_WheelCollider_t56_0_0_0 = { 1, GenInst_WheelCollider_t56_0_0_0_Types };
extern const Il2CppType Camera_t74_0_0_0;
static const Il2CppType* GenInst_Camera_t74_0_0_0_Types[] = { &Camera_t74_0_0_0 };
extern const Il2CppGenericInst GenInst_Camera_t74_0_0_0 = { 1, GenInst_Camera_t74_0_0_0_Types };
extern const Il2CppType Int32_t359_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t359_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_String_t_0_0_0_Int32_t359_0_0_0_Types };
extern const Il2CppType BaseInputModule_t106_0_0_0;
static const Il2CppType* GenInst_BaseInputModule_t106_0_0_0_Types[] = { &BaseInputModule_t106_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInputModule_t106_0_0_0 = { 1, GenInst_BaseInputModule_t106_0_0_0_Types };
extern const Il2CppType RaycastResult_t139_0_0_0;
static const Il2CppType* GenInst_RaycastResult_t139_0_0_0_Types[] = { &RaycastResult_t139_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t139_0_0_0 = { 1, GenInst_RaycastResult_t139_0_0_0_Types };
extern const Il2CppType IDeselectHandler_t337_0_0_0;
static const Il2CppType* GenInst_IDeselectHandler_t337_0_0_0_Types[] = { &IDeselectHandler_t337_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeselectHandler_t337_0_0_0 = { 1, GenInst_IDeselectHandler_t337_0_0_0_Types };
extern const Il2CppType ISelectHandler_t336_0_0_0;
static const Il2CppType* GenInst_ISelectHandler_t336_0_0_0_Types[] = { &ISelectHandler_t336_0_0_0 };
extern const Il2CppGenericInst GenInst_ISelectHandler_t336_0_0_0 = { 1, GenInst_ISelectHandler_t336_0_0_0_Types };
extern const Il2CppType BaseEventData_t107_0_0_0;
static const Il2CppType* GenInst_BaseEventData_t107_0_0_0_Types[] = { &BaseEventData_t107_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseEventData_t107_0_0_0 = { 1, GenInst_BaseEventData_t107_0_0_0_Types };
extern const Il2CppType Entry_t111_0_0_0;
static const Il2CppType* GenInst_Entry_t111_0_0_0_Types[] = { &Entry_t111_0_0_0 };
extern const Il2CppGenericInst GenInst_Entry_t111_0_0_0 = { 1, GenInst_Entry_t111_0_0_0_Types };
extern const Il2CppType IPointerEnterHandler_t324_0_0_0;
static const Il2CppType* GenInst_IPointerEnterHandler_t324_0_0_0_Types[] = { &IPointerEnterHandler_t324_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerEnterHandler_t324_0_0_0 = { 1, GenInst_IPointerEnterHandler_t324_0_0_0_Types };
extern const Il2CppType IPointerExitHandler_t325_0_0_0;
static const Il2CppType* GenInst_IPointerExitHandler_t325_0_0_0_Types[] = { &IPointerExitHandler_t325_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerExitHandler_t325_0_0_0 = { 1, GenInst_IPointerExitHandler_t325_0_0_0_Types };
extern const Il2CppType IPointerDownHandler_t326_0_0_0;
static const Il2CppType* GenInst_IPointerDownHandler_t326_0_0_0_Types[] = { &IPointerDownHandler_t326_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerDownHandler_t326_0_0_0 = { 1, GenInst_IPointerDownHandler_t326_0_0_0_Types };
extern const Il2CppType IPointerUpHandler_t327_0_0_0;
static const Il2CppType* GenInst_IPointerUpHandler_t327_0_0_0_Types[] = { &IPointerUpHandler_t327_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerUpHandler_t327_0_0_0 = { 1, GenInst_IPointerUpHandler_t327_0_0_0_Types };
extern const Il2CppType IPointerClickHandler_t328_0_0_0;
static const Il2CppType* GenInst_IPointerClickHandler_t328_0_0_0_Types[] = { &IPointerClickHandler_t328_0_0_0 };
extern const Il2CppGenericInst GenInst_IPointerClickHandler_t328_0_0_0 = { 1, GenInst_IPointerClickHandler_t328_0_0_0_Types };
extern const Il2CppType IInitializePotentialDragHandler_t329_0_0_0;
static const Il2CppType* GenInst_IInitializePotentialDragHandler_t329_0_0_0_Types[] = { &IInitializePotentialDragHandler_t329_0_0_0 };
extern const Il2CppGenericInst GenInst_IInitializePotentialDragHandler_t329_0_0_0 = { 1, GenInst_IInitializePotentialDragHandler_t329_0_0_0_Types };
extern const Il2CppType IBeginDragHandler_t330_0_0_0;
static const Il2CppType* GenInst_IBeginDragHandler_t330_0_0_0_Types[] = { &IBeginDragHandler_t330_0_0_0 };
extern const Il2CppGenericInst GenInst_IBeginDragHandler_t330_0_0_0 = { 1, GenInst_IBeginDragHandler_t330_0_0_0_Types };
extern const Il2CppType IDragHandler_t331_0_0_0;
static const Il2CppType* GenInst_IDragHandler_t331_0_0_0_Types[] = { &IDragHandler_t331_0_0_0 };
extern const Il2CppGenericInst GenInst_IDragHandler_t331_0_0_0 = { 1, GenInst_IDragHandler_t331_0_0_0_Types };
extern const Il2CppType IEndDragHandler_t332_0_0_0;
static const Il2CppType* GenInst_IEndDragHandler_t332_0_0_0_Types[] = { &IEndDragHandler_t332_0_0_0 };
extern const Il2CppGenericInst GenInst_IEndDragHandler_t332_0_0_0 = { 1, GenInst_IEndDragHandler_t332_0_0_0_Types };
extern const Il2CppType IDropHandler_t333_0_0_0;
static const Il2CppType* GenInst_IDropHandler_t333_0_0_0_Types[] = { &IDropHandler_t333_0_0_0 };
extern const Il2CppGenericInst GenInst_IDropHandler_t333_0_0_0 = { 1, GenInst_IDropHandler_t333_0_0_0_Types };
extern const Il2CppType IScrollHandler_t334_0_0_0;
static const Il2CppType* GenInst_IScrollHandler_t334_0_0_0_Types[] = { &IScrollHandler_t334_0_0_0 };
extern const Il2CppGenericInst GenInst_IScrollHandler_t334_0_0_0 = { 1, GenInst_IScrollHandler_t334_0_0_0_Types };
extern const Il2CppType IUpdateSelectedHandler_t335_0_0_0;
static const Il2CppType* GenInst_IUpdateSelectedHandler_t335_0_0_0_Types[] = { &IUpdateSelectedHandler_t335_0_0_0 };
extern const Il2CppGenericInst GenInst_IUpdateSelectedHandler_t335_0_0_0 = { 1, GenInst_IUpdateSelectedHandler_t335_0_0_0_Types };
extern const Il2CppType IMoveHandler_t338_0_0_0;
static const Il2CppType* GenInst_IMoveHandler_t338_0_0_0_Types[] = { &IMoveHandler_t338_0_0_0 };
extern const Il2CppGenericInst GenInst_IMoveHandler_t338_0_0_0 = { 1, GenInst_IMoveHandler_t338_0_0_0_Types };
extern const Il2CppType ISubmitHandler_t339_0_0_0;
static const Il2CppType* GenInst_ISubmitHandler_t339_0_0_0_Types[] = { &ISubmitHandler_t339_0_0_0 };
extern const Il2CppGenericInst GenInst_ISubmitHandler_t339_0_0_0 = { 1, GenInst_ISubmitHandler_t339_0_0_0_Types };
extern const Il2CppType ICancelHandler_t340_0_0_0;
static const Il2CppType* GenInst_ICancelHandler_t340_0_0_0_Types[] = { &ICancelHandler_t340_0_0_0 };
extern const Il2CppGenericInst GenInst_ICancelHandler_t340_0_0_0 = { 1, GenInst_ICancelHandler_t340_0_0_0_Types };
extern const Il2CppType List_1_t323_0_0_0;
static const Il2CppType* GenInst_List_1_t323_0_0_0_Types[] = { &List_1_t323_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t323_0_0_0 = { 1, GenInst_List_1_t323_0_0_0_Types };
extern const Il2CppType IEventSystemHandler_t1889_0_0_0;
static const Il2CppType* GenInst_IEventSystemHandler_t1889_0_0_0_Types[] = { &IEventSystemHandler_t1889_0_0_0 };
extern const Il2CppGenericInst GenInst_IEventSystemHandler_t1889_0_0_0 = { 1, GenInst_IEventSystemHandler_t1889_0_0_0_Types };
extern const Il2CppType PointerEventData_t57_0_0_0;
static const Il2CppType* GenInst_PointerEventData_t57_0_0_0_Types[] = { &PointerEventData_t57_0_0_0 };
extern const Il2CppGenericInst GenInst_PointerEventData_t57_0_0_0 = { 1, GenInst_PointerEventData_t57_0_0_0_Types };
extern const Il2CppType AxisEventData_t141_0_0_0;
static const Il2CppType* GenInst_AxisEventData_t141_0_0_0_Types[] = { &AxisEventData_t141_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisEventData_t141_0_0_0 = { 1, GenInst_AxisEventData_t141_0_0_0_Types };
extern const Il2CppType BaseRaycaster_t140_0_0_0;
static const Il2CppType* GenInst_BaseRaycaster_t140_0_0_0_Types[] = { &BaseRaycaster_t140_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseRaycaster_t140_0_0_0 = { 1, GenInst_BaseRaycaster_t140_0_0_0_Types };
extern const Il2CppType GameObject_t52_0_0_0;
static const Il2CppType* GenInst_GameObject_t52_0_0_0_Types[] = { &GameObject_t52_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_t52_0_0_0 = { 1, GenInst_GameObject_t52_0_0_0_Types };
extern const Il2CppType ButtonState_t146_0_0_0;
static const Il2CppType* GenInst_ButtonState_t146_0_0_0_Types[] = { &ButtonState_t146_0_0_0 };
extern const Il2CppGenericInst GenInst_ButtonState_t146_0_0_0 = { 1, GenInst_ButtonState_t146_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0_Types[] = { &Int32_t359_0_0_0, &PointerEventData_t57_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0 = { 2, GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0_Types };
extern const Il2CppType SpriteRenderer_t367_0_0_0;
static const Il2CppType* GenInst_SpriteRenderer_t367_0_0_0_Types[] = { &SpriteRenderer_t367_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteRenderer_t367_0_0_0 = { 1, GenInst_SpriteRenderer_t367_0_0_0_Types };
extern const Il2CppType RaycastHit_t100_0_0_0;
static const Il2CppType* GenInst_RaycastHit_t100_0_0_0_Types[] = { &RaycastHit_t100_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit_t100_0_0_0 = { 1, GenInst_RaycastHit_t100_0_0_0_Types };
extern const Il2CppType Color_t83_0_0_0;
static const Il2CppType* GenInst_Color_t83_0_0_0_Types[] = { &Color_t83_0_0_0 };
extern const Il2CppGenericInst GenInst_Color_t83_0_0_0 = { 1, GenInst_Color_t83_0_0_0_Types };
extern const Il2CppType Single_t358_0_0_0;
static const Il2CppType* GenInst_Single_t358_0_0_0_Types[] = { &Single_t358_0_0_0 };
extern const Il2CppGenericInst GenInst_Single_t358_0_0_0 = { 1, GenInst_Single_t358_0_0_0_Types };
extern const Il2CppType ICanvasElement_t344_0_0_0;
static const Il2CppType* GenInst_ICanvasElement_t344_0_0_0_Types[] = { &ICanvasElement_t344_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t344_0_0_0 = { 1, GenInst_ICanvasElement_t344_0_0_0_Types };
extern const Il2CppType RectTransform_t95_0_0_0;
static const Il2CppType* GenInst_RectTransform_t95_0_0_0_Types[] = { &RectTransform_t95_0_0_0 };
extern const Il2CppGenericInst GenInst_RectTransform_t95_0_0_0 = { 1, GenInst_RectTransform_t95_0_0_0_Types };
extern const Il2CppType Button_t168_0_0_0;
static const Il2CppType* GenInst_Button_t168_0_0_0_Types[] = { &Button_t168_0_0_0 };
extern const Il2CppGenericInst GenInst_Button_t168_0_0_0 = { 1, GenInst_Button_t168_0_0_0_Types };
extern const Il2CppType Text_t97_0_0_0;
static const Il2CppType* GenInst_Text_t97_0_0_0_Types[] = { &Text_t97_0_0_0 };
extern const Il2CppGenericInst GenInst_Text_t97_0_0_0 = { 1, GenInst_Text_t97_0_0_0_Types };
extern const Il2CppType RawImage_t248_0_0_0;
static const Il2CppType* GenInst_RawImage_t248_0_0_0_Types[] = { &RawImage_t248_0_0_0 };
extern const Il2CppGenericInst GenInst_RawImage_t248_0_0_0 = { 1, GenInst_RawImage_t248_0_0_0_Types };
extern const Il2CppType Slider_t93_0_0_0;
static const Il2CppType* GenInst_Slider_t93_0_0_0_Types[] = { &Slider_t93_0_0_0 };
extern const Il2CppGenericInst GenInst_Slider_t93_0_0_0 = { 1, GenInst_Slider_t93_0_0_0_Types };
extern const Il2CppType Scrollbar_t257_0_0_0;
static const Il2CppType* GenInst_Scrollbar_t257_0_0_0_Types[] = { &Scrollbar_t257_0_0_0 };
extern const Il2CppGenericInst GenInst_Scrollbar_t257_0_0_0 = { 1, GenInst_Scrollbar_t257_0_0_0_Types };
extern const Il2CppType Toggle_t179_0_0_0;
static const Il2CppType* GenInst_Toggle_t179_0_0_0_Types[] = { &Toggle_t179_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t179_0_0_0 = { 1, GenInst_Toggle_t179_0_0_0_Types };
extern const Il2CppType InputField_t231_0_0_0;
static const Il2CppType* GenInst_InputField_t231_0_0_0_Types[] = { &InputField_t231_0_0_0 };
extern const Il2CppGenericInst GenInst_InputField_t231_0_0_0 = { 1, GenInst_InputField_t231_0_0_0_Types };
extern const Il2CppType ScrollRect_t263_0_0_0;
static const Il2CppType* GenInst_ScrollRect_t263_0_0_0_Types[] = { &ScrollRect_t263_0_0_0 };
extern const Il2CppGenericInst GenInst_ScrollRect_t263_0_0_0 = { 1, GenInst_ScrollRect_t263_0_0_0_Types };
extern const Il2CppType Mask_t240_0_0_0;
static const Il2CppType* GenInst_Mask_t240_0_0_0_Types[] = { &Mask_t240_0_0_0 };
extern const Il2CppGenericInst GenInst_Mask_t240_0_0_0 = { 1, GenInst_Mask_t240_0_0_0_Types };
extern const Il2CppType Dropdown_t186_0_0_0;
static const Il2CppType* GenInst_Dropdown_t186_0_0_0_Types[] = { &Dropdown_t186_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_t186_0_0_0 = { 1, GenInst_Dropdown_t186_0_0_0_Types };
extern const Il2CppType OptionData_t180_0_0_0;
static const Il2CppType* GenInst_OptionData_t180_0_0_0_Types[] = { &OptionData_t180_0_0_0 };
extern const Il2CppGenericInst GenInst_OptionData_t180_0_0_0 = { 1, GenInst_OptionData_t180_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_Types[] = { &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0 = { 1, GenInst_Int32_t359_0_0_0_Types };
extern const Il2CppType DropdownItem_t178_0_0_0;
static const Il2CppType* GenInst_DropdownItem_t178_0_0_0_Types[] = { &DropdownItem_t178_0_0_0 };
extern const Il2CppGenericInst GenInst_DropdownItem_t178_0_0_0 = { 1, GenInst_DropdownItem_t178_0_0_0_Types };
extern const Il2CppType FloatTween_t163_0_0_0;
static const Il2CppType* GenInst_FloatTween_t163_0_0_0_Types[] = { &FloatTween_t163_0_0_0 };
extern const Il2CppGenericInst GenInst_FloatTween_t163_0_0_0 = { 1, GenInst_FloatTween_t163_0_0_0_Types };
extern const Il2CppType Canvas_t197_0_0_0;
static const Il2CppType* GenInst_Canvas_t197_0_0_0_Types[] = { &Canvas_t197_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t197_0_0_0 = { 1, GenInst_Canvas_t197_0_0_0_Types };
extern const Il2CppType GraphicRaycaster_t203_0_0_0;
static const Il2CppType* GenInst_GraphicRaycaster_t203_0_0_0_Types[] = { &GraphicRaycaster_t203_0_0_0 };
extern const Il2CppGenericInst GenInst_GraphicRaycaster_t203_0_0_0 = { 1, GenInst_GraphicRaycaster_t203_0_0_0_Types };
extern const Il2CppType CanvasGroup_t373_0_0_0;
static const Il2CppType* GenInst_CanvasGroup_t373_0_0_0_Types[] = { &CanvasGroup_t373_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasGroup_t373_0_0_0 = { 1, GenInst_CanvasGroup_t373_0_0_0_Types };
extern const Il2CppType Boolean_t360_0_0_0;
static const Il2CppType* GenInst_Boolean_t360_0_0_0_Types[] = { &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t360_0_0_0 = { 1, GenInst_Boolean_t360_0_0_0_Types };
extern const Il2CppType Font_t191_0_0_0;
extern const Il2CppType List_1_t378_0_0_0;
static const Il2CppType* GenInst_Font_t191_0_0_0_List_1_t378_0_0_0_Types[] = { &Font_t191_0_0_0, &List_1_t378_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t191_0_0_0_List_1_t378_0_0_0 = { 2, GenInst_Font_t191_0_0_0_List_1_t378_0_0_0_Types };
static const Il2CppType* GenInst_Font_t191_0_0_0_Types[] = { &Font_t191_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t191_0_0_0 = { 1, GenInst_Font_t191_0_0_0_Types };
extern const Il2CppType ColorTween_t160_0_0_0;
static const Il2CppType* GenInst_ColorTween_t160_0_0_0_Types[] = { &ColorTween_t160_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorTween_t160_0_0_0 = { 1, GenInst_ColorTween_t160_0_0_0_Types };
extern const Il2CppType CanvasRenderer_t196_0_0_0;
static const Il2CppType* GenInst_CanvasRenderer_t196_0_0_0_Types[] = { &CanvasRenderer_t196_0_0_0 };
extern const Il2CppGenericInst GenInst_CanvasRenderer_t196_0_0_0 = { 1, GenInst_CanvasRenderer_t196_0_0_0_Types };
extern const Il2CppType Component_t78_0_0_0;
static const Il2CppType* GenInst_Component_t78_0_0_0_Types[] = { &Component_t78_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_t78_0_0_0 = { 1, GenInst_Component_t78_0_0_0_Types };
extern const Il2CppType Graphic_t194_0_0_0;
static const Il2CppType* GenInst_Graphic_t194_0_0_0_Types[] = { &Graphic_t194_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t194_0_0_0 = { 1, GenInst_Graphic_t194_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t388_0_0_0;
static const Il2CppType* GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0_Types[] = { &Canvas_t197_0_0_0, &IndexedSet_1_t388_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0 = { 2, GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0_Types };
extern const Il2CppType Sprite_t176_0_0_0;
static const Il2CppType* GenInst_Sprite_t176_0_0_0_Types[] = { &Sprite_t176_0_0_0 };
extern const Il2CppGenericInst GenInst_Sprite_t176_0_0_0 = { 1, GenInst_Sprite_t176_0_0_0_Types };
extern const Il2CppType Type_t208_0_0_0;
static const Il2CppType* GenInst_Type_t208_0_0_0_Types[] = { &Type_t208_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t208_0_0_0 = { 1, GenInst_Type_t208_0_0_0_Types };
extern const Il2CppType FillMethod_t209_0_0_0;
static const Il2CppType* GenInst_FillMethod_t209_0_0_0_Types[] = { &FillMethod_t209_0_0_0 };
extern const Il2CppGenericInst GenInst_FillMethod_t209_0_0_0 = { 1, GenInst_FillMethod_t209_0_0_0_Types };
extern const Il2CppType SubmitEvent_t222_0_0_0;
static const Il2CppType* GenInst_SubmitEvent_t222_0_0_0_Types[] = { &SubmitEvent_t222_0_0_0 };
extern const Il2CppGenericInst GenInst_SubmitEvent_t222_0_0_0 = { 1, GenInst_SubmitEvent_t222_0_0_0_Types };
extern const Il2CppType OnChangeEvent_t224_0_0_0;
static const Il2CppType* GenInst_OnChangeEvent_t224_0_0_0_Types[] = { &OnChangeEvent_t224_0_0_0 };
extern const Il2CppGenericInst GenInst_OnChangeEvent_t224_0_0_0 = { 1, GenInst_OnChangeEvent_t224_0_0_0_Types };
extern const Il2CppType OnValidateInput_t226_0_0_0;
static const Il2CppType* GenInst_OnValidateInput_t226_0_0_0_Types[] = { &OnValidateInput_t226_0_0_0 };
extern const Il2CppGenericInst GenInst_OnValidateInput_t226_0_0_0 = { 1, GenInst_OnValidateInput_t226_0_0_0_Types };
extern const Il2CppType ContentType_t218_0_0_0;
static const Il2CppType* GenInst_ContentType_t218_0_0_0_Types[] = { &ContentType_t218_0_0_0 };
extern const Il2CppGenericInst GenInst_ContentType_t218_0_0_0 = { 1, GenInst_ContentType_t218_0_0_0_Types };
extern const Il2CppType LineType_t221_0_0_0;
static const Il2CppType* GenInst_LineType_t221_0_0_0_Types[] = { &LineType_t221_0_0_0 };
extern const Il2CppGenericInst GenInst_LineType_t221_0_0_0 = { 1, GenInst_LineType_t221_0_0_0_Types };
extern const Il2CppType InputType_t219_0_0_0;
static const Il2CppType* GenInst_InputType_t219_0_0_0_Types[] = { &InputType_t219_0_0_0 };
extern const Il2CppGenericInst GenInst_InputType_t219_0_0_0 = { 1, GenInst_InputType_t219_0_0_0_Types };
extern const Il2CppType TouchScreenKeyboardType_t391_0_0_0;
static const Il2CppType* GenInst_TouchScreenKeyboardType_t391_0_0_0_Types[] = { &TouchScreenKeyboardType_t391_0_0_0 };
extern const Il2CppGenericInst GenInst_TouchScreenKeyboardType_t391_0_0_0 = { 1, GenInst_TouchScreenKeyboardType_t391_0_0_0_Types };
extern const Il2CppType CharacterValidation_t220_0_0_0;
static const Il2CppType* GenInst_CharacterValidation_t220_0_0_0_Types[] = { &CharacterValidation_t220_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterValidation_t220_0_0_0 = { 1, GenInst_CharacterValidation_t220_0_0_0_Types };
extern const Il2CppType Char_t390_0_0_0;
static const Il2CppType* GenInst_Char_t390_0_0_0_Types[] = { &Char_t390_0_0_0 };
extern const Il2CppGenericInst GenInst_Char_t390_0_0_0 = { 1, GenInst_Char_t390_0_0_0_Types };
extern const Il2CppType UILineInfo_t394_0_0_0;
static const Il2CppType* GenInst_UILineInfo_t394_0_0_0_Types[] = { &UILineInfo_t394_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t394_0_0_0 = { 1, GenInst_UILineInfo_t394_0_0_0_Types };
extern const Il2CppType UICharInfo_t396_0_0_0;
static const Il2CppType* GenInst_UICharInfo_t396_0_0_0_Types[] = { &UICharInfo_t396_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t396_0_0_0 = { 1, GenInst_UICharInfo_t396_0_0_0_Types };
extern const Il2CppType LayoutElement_t301_0_0_0;
static const Il2CppType* GenInst_LayoutElement_t301_0_0_0_Types[] = { &LayoutElement_t301_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutElement_t301_0_0_0 = { 1, GenInst_LayoutElement_t301_0_0_0_Types };
extern const Il2CppType IClippable_t349_0_0_0;
static const Il2CppType* GenInst_IClippable_t349_0_0_0_Types[] = { &IClippable_t349_0_0_0 };
extern const Il2CppGenericInst GenInst_IClippable_t349_0_0_0 = { 1, GenInst_IClippable_t349_0_0_0_Types };
extern const Il2CppType RectMask2D_t243_0_0_0;
static const Il2CppType* GenInst_RectMask2D_t243_0_0_0_Types[] = { &RectMask2D_t243_0_0_0 };
extern const Il2CppGenericInst GenInst_RectMask2D_t243_0_0_0 = { 1, GenInst_RectMask2D_t243_0_0_0_Types };
extern const Il2CppType Direction_t253_0_0_0;
static const Il2CppType* GenInst_Direction_t253_0_0_0_Types[] = { &Direction_t253_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t253_0_0_0 = { 1, GenInst_Direction_t253_0_0_0_Types };
extern const Il2CppType Vector2_t23_0_0_0;
static const Il2CppType* GenInst_Vector2_t23_0_0_0_Types[] = { &Vector2_t23_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t23_0_0_0 = { 1, GenInst_Vector2_t23_0_0_0_Types };
extern const Il2CppType Selectable_t169_0_0_0;
static const Il2CppType* GenInst_Selectable_t169_0_0_0_Types[] = { &Selectable_t169_0_0_0 };
extern const Il2CppGenericInst GenInst_Selectable_t169_0_0_0 = { 1, GenInst_Selectable_t169_0_0_0_Types };
extern const Il2CppType Navigation_t247_0_0_0;
static const Il2CppType* GenInst_Navigation_t247_0_0_0_Types[] = { &Navigation_t247_0_0_0 };
extern const Il2CppGenericInst GenInst_Navigation_t247_0_0_0 = { 1, GenInst_Navigation_t247_0_0_0_Types };
extern const Il2CppType Transition_t265_0_0_0;
static const Il2CppType* GenInst_Transition_t265_0_0_0_Types[] = { &Transition_t265_0_0_0 };
extern const Il2CppGenericInst GenInst_Transition_t265_0_0_0 = { 1, GenInst_Transition_t265_0_0_0_Types };
extern const Il2CppType ColorBlock_t174_0_0_0;
static const Il2CppType* GenInst_ColorBlock_t174_0_0_0_Types[] = { &ColorBlock_t174_0_0_0 };
extern const Il2CppGenericInst GenInst_ColorBlock_t174_0_0_0 = { 1, GenInst_ColorBlock_t174_0_0_0_Types };
extern const Il2CppType SpriteState_t267_0_0_0;
static const Il2CppType* GenInst_SpriteState_t267_0_0_0_Types[] = { &SpriteState_t267_0_0_0 };
extern const Il2CppGenericInst GenInst_SpriteState_t267_0_0_0 = { 1, GenInst_SpriteState_t267_0_0_0_Types };
extern const Il2CppType AnimationTriggers_t164_0_0_0;
static const Il2CppType* GenInst_AnimationTriggers_t164_0_0_0_Types[] = { &AnimationTriggers_t164_0_0_0 };
extern const Il2CppGenericInst GenInst_AnimationTriggers_t164_0_0_0 = { 1, GenInst_AnimationTriggers_t164_0_0_0_Types };
extern const Il2CppType Animator_t350_0_0_0;
static const Il2CppType* GenInst_Animator_t350_0_0_0_Types[] = { &Animator_t350_0_0_0 };
extern const Il2CppGenericInst GenInst_Animator_t350_0_0_0 = { 1, GenInst_Animator_t350_0_0_0_Types };
extern const Il2CppType Direction_t271_0_0_0;
static const Il2CppType* GenInst_Direction_t271_0_0_0_Types[] = { &Direction_t271_0_0_0 };
extern const Il2CppGenericInst GenInst_Direction_t271_0_0_0 = { 1, GenInst_Direction_t271_0_0_0_Types };
extern const Il2CppType MatEntry_t274_0_0_0;
static const Il2CppType* GenInst_MatEntry_t274_0_0_0_Types[] = { &MatEntry_t274_0_0_0 };
extern const Il2CppGenericInst GenInst_MatEntry_t274_0_0_0 = { 1, GenInst_MatEntry_t274_0_0_0_Types };
extern const Il2CppType UIVertex_t239_0_0_0;
static const Il2CppType* GenInst_UIVertex_t239_0_0_0_Types[] = { &UIVertex_t239_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t239_0_0_0 = { 1, GenInst_UIVertex_t239_0_0_0_Types };
static const Il2CppType* GenInst_Toggle_t179_0_0_0_Boolean_t360_0_0_0_Types[] = { &Toggle_t179_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Toggle_t179_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_Toggle_t179_0_0_0_Boolean_t360_0_0_0_Types };
extern const Il2CppType IClipper_t353_0_0_0;
static const Il2CppType* GenInst_IClipper_t353_0_0_0_Types[] = { &IClipper_t353_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t353_0_0_0 = { 1, GenInst_IClipper_t353_0_0_0_Types };
extern const Il2CppType AspectMode_t286_0_0_0;
static const Il2CppType* GenInst_AspectMode_t286_0_0_0_Types[] = { &AspectMode_t286_0_0_0 };
extern const Il2CppGenericInst GenInst_AspectMode_t286_0_0_0 = { 1, GenInst_AspectMode_t286_0_0_0_Types };
extern const Il2CppType FitMode_t292_0_0_0;
static const Il2CppType* GenInst_FitMode_t292_0_0_0_Types[] = { &FitMode_t292_0_0_0 };
extern const Il2CppGenericInst GenInst_FitMode_t292_0_0_0 = { 1, GenInst_FitMode_t292_0_0_0_Types };
extern const Il2CppType Corner_t294_0_0_0;
static const Il2CppType* GenInst_Corner_t294_0_0_0_Types[] = { &Corner_t294_0_0_0 };
extern const Il2CppGenericInst GenInst_Corner_t294_0_0_0 = { 1, GenInst_Corner_t294_0_0_0_Types };
extern const Il2CppType Axis_t295_0_0_0;
static const Il2CppType* GenInst_Axis_t295_0_0_0_Types[] = { &Axis_t295_0_0_0 };
extern const Il2CppGenericInst GenInst_Axis_t295_0_0_0 = { 1, GenInst_Axis_t295_0_0_0_Types };
extern const Il2CppType Constraint_t296_0_0_0;
static const Il2CppType* GenInst_Constraint_t296_0_0_0_Types[] = { &Constraint_t296_0_0_0 };
extern const Il2CppGenericInst GenInst_Constraint_t296_0_0_0 = { 1, GenInst_Constraint_t296_0_0_0_Types };
extern const Il2CppType RectOffset_t302_0_0_0;
static const Il2CppType* GenInst_RectOffset_t302_0_0_0_Types[] = { &RectOffset_t302_0_0_0 };
extern const Il2CppGenericInst GenInst_RectOffset_t302_0_0_0 = { 1, GenInst_RectOffset_t302_0_0_0_Types };
extern const Il2CppType TextAnchor_t412_0_0_0;
static const Il2CppType* GenInst_TextAnchor_t412_0_0_0_Types[] = { &TextAnchor_t412_0_0_0 };
extern const Il2CppGenericInst GenInst_TextAnchor_t412_0_0_0 = { 1, GenInst_TextAnchor_t412_0_0_0_Types };
extern const Il2CppType LayoutRebuilder_t304_0_0_0;
static const Il2CppType* GenInst_LayoutRebuilder_t304_0_0_0_Types[] = { &LayoutRebuilder_t304_0_0_0 };
extern const Il2CppGenericInst GenInst_LayoutRebuilder_t304_0_0_0 = { 1, GenInst_LayoutRebuilder_t304_0_0_0_Types };
extern const Il2CppType ILayoutElement_t355_0_0_0;
static const Il2CppType* GenInst_ILayoutElement_t355_0_0_0_Single_t358_0_0_0_Types[] = { &ILayoutElement_t355_0_0_0, &Single_t358_0_0_0 };
extern const Il2CppGenericInst GenInst_ILayoutElement_t355_0_0_0_Single_t358_0_0_0 = { 2, GenInst_ILayoutElement_t355_0_0_0_Single_t358_0_0_0_Types };
extern const Il2CppType Vector3_t12_0_0_0;
static const Il2CppType* GenInst_Vector3_t12_0_0_0_Types[] = { &Vector3_t12_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t12_0_0_0 = { 1, GenInst_Vector3_t12_0_0_0_Types };
extern const Il2CppType Color32_t347_0_0_0;
static const Il2CppType* GenInst_Color32_t347_0_0_0_Types[] = { &Color32_t347_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t347_0_0_0 = { 1, GenInst_Color32_t347_0_0_0_Types };
extern const Il2CppType Vector4_t317_0_0_0;
static const Il2CppType* GenInst_Vector4_t317_0_0_0_Types[] = { &Vector4_t317_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t317_0_0_0 = { 1, GenInst_Vector4_t317_0_0_0_Types };
extern const Il2CppType GcLeaderboard_t453_0_0_0;
static const Il2CppType* GenInst_GcLeaderboard_t453_0_0_0_Types[] = { &GcLeaderboard_t453_0_0_0 };
extern const Il2CppGenericInst GenInst_GcLeaderboard_t453_0_0_0 = { 1, GenInst_GcLeaderboard_t453_0_0_0_Types };
extern const Il2CppType IAchievementDescriptionU5BU5D_t657_0_0_0;
static const Il2CppType* GenInst_IAchievementDescriptionU5BU5D_t657_0_0_0_Types[] = { &IAchievementDescriptionU5BU5D_t657_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescriptionU5BU5D_t657_0_0_0 = { 1, GenInst_IAchievementDescriptionU5BU5D_t657_0_0_0_Types };
extern const Il2CppType IAchievementU5BU5D_t659_0_0_0;
static const Il2CppType* GenInst_IAchievementU5BU5D_t659_0_0_0_Types[] = { &IAchievementU5BU5D_t659_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementU5BU5D_t659_0_0_0 = { 1, GenInst_IAchievementU5BU5D_t659_0_0_0_Types };
extern const Il2CppType IScoreU5BU5D_t593_0_0_0;
static const Il2CppType* GenInst_IScoreU5BU5D_t593_0_0_0_Types[] = { &IScoreU5BU5D_t593_0_0_0 };
extern const Il2CppGenericInst GenInst_IScoreU5BU5D_t593_0_0_0 = { 1, GenInst_IScoreU5BU5D_t593_0_0_0_Types };
extern const Il2CppType IUserProfileU5BU5D_t589_0_0_0;
static const Il2CppType* GenInst_IUserProfileU5BU5D_t589_0_0_0_Types[] = { &IUserProfileU5BU5D_t589_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfileU5BU5D_t589_0_0_0 = { 1, GenInst_IUserProfileU5BU5D_t589_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t360_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_String_t_0_0_0_Boolean_t360_0_0_0_Types };
extern const Il2CppType Rigidbody2D_t509_0_0_0;
static const Il2CppType* GenInst_Rigidbody2D_t509_0_0_0_Types[] = { &Rigidbody2D_t509_0_0_0 };
extern const Il2CppGenericInst GenInst_Rigidbody2D_t509_0_0_0 = { 1, GenInst_Rigidbody2D_t509_0_0_0_Types };
extern const Il2CppType LayoutCache_t549_0_0_0;
static const Il2CppType* GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0_Types[] = { &Int32_t359_0_0_0, &LayoutCache_t549_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0 = { 2, GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0_Types };
extern const Il2CppType GUILayoutEntry_t554_0_0_0;
static const Il2CppType* GenInst_GUILayoutEntry_t554_0_0_0_Types[] = { &GUILayoutEntry_t554_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutEntry_t554_0_0_0 = { 1, GenInst_GUILayoutEntry_t554_0_0_0_Types };
extern const Il2CppType GUIStyle_t553_0_0_0;
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t553_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0 = { 2, GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0_Types };
extern const Il2CppType Type_t_0_0_0;
static const Il2CppType* GenInst_Type_t_0_0_0_Types[] = { &Type_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Type_t_0_0_0 = { 1, GenInst_Type_t_0_0_0_Types };
extern const Il2CppType GUILayer_t460_0_0_0;
static const Il2CppType* GenInst_GUILayer_t460_0_0_0_Types[] = { &GUILayer_t460_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayer_t460_0_0_0 = { 1, GenInst_GUILayer_t460_0_0_0_Types };
extern const Il2CppType PersistentCall_t623_0_0_0;
static const Il2CppType* GenInst_PersistentCall_t623_0_0_0_Types[] = { &PersistentCall_t623_0_0_0 };
extern const Il2CppGenericInst GenInst_PersistentCall_t623_0_0_0 = { 1, GenInst_PersistentCall_t623_0_0_0_Types };
extern const Il2CppType BaseInvokableCall_t620_0_0_0;
static const Il2CppType* GenInst_BaseInvokableCall_t620_0_0_0_Types[] = { &BaseInvokableCall_t620_0_0_0 };
extern const Il2CppGenericInst GenInst_BaseInvokableCall_t620_0_0_0 = { 1, GenInst_BaseInvokableCall_t620_0_0_0_Types };
extern const Il2CppType CustomAttributeTypedArgument_t1326_0_0_0;
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1326_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1326_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1326_0_0_0 = { 1, GenInst_CustomAttributeTypedArgument_t1326_0_0_0_Types };
extern const Il2CppType CustomAttributeNamedArgument_t1325_0_0_0;
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1325_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1325_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1325_0_0_0 = { 1, GenInst_CustomAttributeNamedArgument_t1325_0_0_0_Types };
extern const Il2CppType StrongName_t1576_0_0_0;
static const Il2CppType* GenInst_StrongName_t1576_0_0_0_Types[] = { &StrongName_t1576_0_0_0 };
extern const Il2CppGenericInst GenInst_StrongName_t1576_0_0_0 = { 1, GenInst_StrongName_t1576_0_0_0_Types };
extern const Il2CppType DateTime_t546_0_0_0;
static const Il2CppType* GenInst_DateTime_t546_0_0_0_Types[] = { &DateTime_t546_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTime_t546_0_0_0 = { 1, GenInst_DateTime_t546_0_0_0_Types };
extern const Il2CppType DateTimeOffset_t1651_0_0_0;
static const Il2CppType* GenInst_DateTimeOffset_t1651_0_0_0_Types[] = { &DateTimeOffset_t1651_0_0_0 };
extern const Il2CppGenericInst GenInst_DateTimeOffset_t1651_0_0_0 = { 1, GenInst_DateTimeOffset_t1651_0_0_0_Types };
extern const Il2CppType TimeSpan_t969_0_0_0;
static const Il2CppType* GenInst_TimeSpan_t969_0_0_0_Types[] = { &TimeSpan_t969_0_0_0 };
extern const Il2CppGenericInst GenInst_TimeSpan_t969_0_0_0 = { 1, GenInst_TimeSpan_t969_0_0_0_Types };
extern const Il2CppType Guid_t1673_0_0_0;
static const Il2CppType* GenInst_Guid_t1673_0_0_0_Types[] = { &Guid_t1673_0_0_0 };
extern const Il2CppGenericInst GenInst_Guid_t1673_0_0_0 = { 1, GenInst_Guid_t1673_0_0_0_Types };
extern const Il2CppType CustomAttributeData_t1322_0_0_0;
static const Il2CppType* GenInst_CustomAttributeData_t1322_0_0_0_Types[] = { &CustomAttributeData_t1322_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeData_t1322_0_0_0 = { 1, GenInst_CustomAttributeData_t1322_0_0_0_Types };
extern const Il2CppType AxisTouchButton_t1_0_0_0;
static const Il2CppType* GenInst_AxisTouchButton_t1_0_0_0_Types[] = { &AxisTouchButton_t1_0_0_0 };
extern const Il2CppGenericInst GenInst_AxisTouchButton_t1_0_0_0 = { 1, GenInst_AxisTouchButton_t1_0_0_0_Types };
extern const Il2CppType MonoBehaviour_t2_0_0_0;
static const Il2CppType* GenInst_MonoBehaviour_t2_0_0_0_Types[] = { &MonoBehaviour_t2_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoBehaviour_t2_0_0_0 = { 1, GenInst_MonoBehaviour_t2_0_0_0_Types };
extern const Il2CppType Behaviour_t418_0_0_0;
static const Il2CppType* GenInst_Behaviour_t418_0_0_0_Types[] = { &Behaviour_t418_0_0_0 };
extern const Il2CppGenericInst GenInst_Behaviour_t418_0_0_0 = { 1, GenInst_Behaviour_t418_0_0_0_Types };
extern const Il2CppType Object_t62_0_0_0;
static const Il2CppType* GenInst_Object_t62_0_0_0_Types[] = { &Object_t62_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t62_0_0_0 = { 1, GenInst_Object_t62_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1817_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1817_0_0_0_Types[] = { &KeyValuePair_2_t1817_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1817_0_0_0 = { 1, GenInst_KeyValuePair_2_t1817_0_0_0_Types };
extern const Il2CppType ValueType_t1071_0_0_0;
static const Il2CppType* GenInst_ValueType_t1071_0_0_0_Types[] = { &ValueType_t1071_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueType_t1071_0_0_0 = { 1, GenInst_ValueType_t1071_0_0_0_Types };
extern const Il2CppType IConvertible_t1764_0_0_0;
static const Il2CppType* GenInst_IConvertible_t1764_0_0_0_Types[] = { &IConvertible_t1764_0_0_0 };
extern const Il2CppGenericInst GenInst_IConvertible_t1764_0_0_0 = { 1, GenInst_IConvertible_t1764_0_0_0_Types };
extern const Il2CppType IComparable_t1763_0_0_0;
static const Il2CppType* GenInst_IComparable_t1763_0_0_0_Types[] = { &IComparable_t1763_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_t1763_0_0_0 = { 1, GenInst_IComparable_t1763_0_0_0_Types };
extern const Il2CppType IEnumerable_t878_0_0_0;
static const Il2CppType* GenInst_IEnumerable_t878_0_0_0_Types[] = { &IEnumerable_t878_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_t878_0_0_0 = { 1, GenInst_IEnumerable_t878_0_0_0_Types };
extern const Il2CppType ICloneable_t1774_0_0_0;
static const Il2CppType* GenInst_ICloneable_t1774_0_0_0_Types[] = { &ICloneable_t1774_0_0_0 };
extern const Il2CppGenericInst GenInst_ICloneable_t1774_0_0_0 = { 1, GenInst_ICloneable_t1774_0_0_0_Types };
extern const Il2CppType IComparable_1_t2905_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2905_0_0_0_Types[] = { &IComparable_1_t2905_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2905_0_0_0 = { 1, GenInst_IComparable_1_t2905_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2910_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2910_0_0_0_Types[] = { &IEquatable_1_t2910_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2910_0_0_0 = { 1, GenInst_IEquatable_1_t2910_0_0_0_Types };
extern const Il2CppType IFormattable_t1761_0_0_0;
static const Il2CppType* GenInst_IFormattable_t1761_0_0_0_Types[] = { &IFormattable_t1761_0_0_0 };
extern const Il2CppGenericInst GenInst_IFormattable_t1761_0_0_0 = { 1, GenInst_IFormattable_t1761_0_0_0_Types };
extern const Il2CppType IComparable_1_t2920_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2920_0_0_0_Types[] = { &IComparable_1_t2920_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2920_0_0_0 = { 1, GenInst_IComparable_1_t2920_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2925_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2925_0_0_0_Types[] = { &IEquatable_1_t2925_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2925_0_0_0 = { 1, GenInst_IEquatable_1_t2925_0_0_0_Types };
extern const Il2CppType Link_t1181_0_0_0;
static const Il2CppType* GenInst_Link_t1181_0_0_0_Types[] = { &Link_t1181_0_0_0 };
extern const Il2CppGenericInst GenInst_Link_t1181_0_0_0 = { 1, GenInst_Link_t1181_0_0_0_Types };
extern const Il2CppType DictionaryEntry_t1057_0_0_0;
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1057_0_0_0_Types[] = { &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1057_0_0_0 = { 1, GenInst_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1817_0_0_0_Types[] = { &Object_t_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1817_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1817_0_0_0 = { 3, GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1817_0_0_0_Types };
extern const Il2CppType IReflect_t2684_0_0_0;
static const Il2CppType* GenInst_IReflect_t2684_0_0_0_Types[] = { &IReflect_t2684_0_0_0 };
extern const Il2CppGenericInst GenInst_IReflect_t2684_0_0_0 = { 1, GenInst_IReflect_t2684_0_0_0_Types };
extern const Il2CppType _Type_t2682_0_0_0;
static const Il2CppType* GenInst__Type_t2682_0_0_0_Types[] = { &_Type_t2682_0_0_0 };
extern const Il2CppGenericInst GenInst__Type_t2682_0_0_0 = { 1, GenInst__Type_t2682_0_0_0_Types };
extern const Il2CppType MemberInfo_t_0_0_0;
static const Il2CppType* GenInst_MemberInfo_t_0_0_0_Types[] = { &MemberInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MemberInfo_t_0_0_0 = { 1, GenInst_MemberInfo_t_0_0_0_Types };
extern const Il2CppType ICustomAttributeProvider_t1759_0_0_0;
static const Il2CppType* GenInst_ICustomAttributeProvider_t1759_0_0_0_Types[] = { &ICustomAttributeProvider_t1759_0_0_0 };
extern const Il2CppGenericInst GenInst_ICustomAttributeProvider_t1759_0_0_0 = { 1, GenInst_ICustomAttributeProvider_t1759_0_0_0_Types };
extern const Il2CppType _MemberInfo_t2683_0_0_0;
static const Il2CppType* GenInst__MemberInfo_t2683_0_0_0_Types[] = { &_MemberInfo_t2683_0_0_0 };
extern const Il2CppGenericInst GenInst__MemberInfo_t2683_0_0_0 = { 1, GenInst__MemberInfo_t2683_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &String_t_0_0_0, &VirtualAxis_t3_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1832_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1832_0_0_0_Types[] = { &KeyValuePair_2_t1832_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1832_0_0_0 = { 1, GenInst_KeyValuePair_2_t1832_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &String_t_0_0_0, &VirtualButton_t6_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1837_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1837_0_0_0_Types[] = { &KeyValuePair_2_t1837_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1837_0_0_0 = { 1, GenInst_KeyValuePair_2_t1837_0_0_0_Types };
extern const Il2CppType Touch_t72_0_0_0;
static const Il2CppType* GenInst_Touch_t72_0_0_0_Types[] = { &Touch_t72_0_0_0 };
extern const Il2CppGenericInst GenInst_Touch_t72_0_0_0 = { 1, GenInst_Touch_t72_0_0_0_Types };
extern const Il2CppType Double_t675_0_0_0;
static const Il2CppType* GenInst_Double_t675_0_0_0_Types[] = { &Double_t675_0_0_0 };
extern const Il2CppGenericInst GenInst_Double_t675_0_0_0 = { 1, GenInst_Double_t675_0_0_0_Types };
extern const Il2CppType IComparable_1_t2969_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2969_0_0_0_Types[] = { &IComparable_1_t2969_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2969_0_0_0 = { 1, GenInst_IComparable_1_t2969_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2974_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2974_0_0_0_Types[] = { &IEquatable_1_t2974_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2974_0_0_0 = { 1, GenInst_IEquatable_1_t2974_0_0_0_Types };
extern const Il2CppType IComparable_1_t2982_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t2982_0_0_0_Types[] = { &IComparable_1_t2982_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t2982_0_0_0 = { 1, GenInst_IComparable_1_t2982_0_0_0_Types };
extern const Il2CppType IEquatable_1_t2987_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t2987_0_0_0_Types[] = { &IEquatable_1_t2987_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t2987_0_0_0 = { 1, GenInst_IEquatable_1_t2987_0_0_0_Types };
extern const Il2CppType Collider_t98_0_0_0;
static const Il2CppType* GenInst_Collider_t98_0_0_0_Types[] = { &Collider_t98_0_0_0 };
extern const Il2CppGenericInst GenInst_Collider_t98_0_0_0 = { 1, GenInst_Collider_t98_0_0_0_Types };
extern const Il2CppType WheelEffects_t54_0_0_0;
static const Il2CppType* GenInst_WheelEffects_t54_0_0_0_Types[] = { &WheelEffects_t54_0_0_0 };
extern const Il2CppGenericInst GenInst_WheelEffects_t54_0_0_0 = { 1, GenInst_WheelEffects_t54_0_0_0_Types };
extern const Il2CppType Quaternion_t45_0_0_0;
static const Il2CppType* GenInst_Quaternion_t45_0_0_0_Types[] = { &Quaternion_t45_0_0_0 };
extern const Il2CppGenericInst GenInst_Quaternion_t45_0_0_0 = { 1, GenInst_Quaternion_t45_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t359_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_Object_t_0_0_0_Int32_t359_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1864_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1864_0_0_0_Types[] = { &KeyValuePair_2_t1864_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1864_0_0_0 = { 1, GenInst_KeyValuePair_2_t1864_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t359_0_0_0_Int32_t359_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t359_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t359_0_0_0_Int32_t359_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t359_0_0_0_Int32_t359_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t359_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Int32_t359_0_0_0_KeyValuePair_2_t1864_0_0_0_Types[] = { &Object_t_0_0_0, &Int32_t359_0_0_0, &KeyValuePair_2_t1864_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Int32_t359_0_0_0_KeyValuePair_2_t1864_0_0_0 = { 3, GenInst_Object_t_0_0_0_Int32_t359_0_0_0_KeyValuePair_2_t1864_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &String_t_0_0_0, &Int32_t359_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_String_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1875_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1875_0_0_0_Types[] = { &KeyValuePair_2_t1875_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1875_0_0_0 = { 1, GenInst_KeyValuePair_2_t1875_0_0_0_Types };
extern const Il2CppType List_1_t403_0_0_0;
static const Il2CppType* GenInst_List_1_t403_0_0_0_Types[] = { &List_1_t403_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t403_0_0_0 = { 1, GenInst_List_1_t403_0_0_0_Types };
extern const Il2CppType List_1_t354_0_0_0;
static const Il2CppType* GenInst_List_1_t354_0_0_0_Types[] = { &List_1_t354_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t354_0_0_0 = { 1, GenInst_List_1_t354_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t359_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_Object_t_0_0_0 = { 2, GenInst_Int32_t359_0_0_0_Object_t_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t1952_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t1952_0_0_0_Types[] = { &KeyValuePair_2_t1952_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1952_0_0_0 = { 1, GenInst_KeyValuePair_2_t1952_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types[] = { &Int32_t359_0_0_0, &Object_t_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_Object_t_0_0_0_Object_t_0_0_0 = { 3, GenInst_Int32_t359_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Int32_t359_0_0_0, &Object_t_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Int32_t359_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1952_0_0_0_Types[] = { &Int32_t359_0_0_0, &Object_t_0_0_0, &KeyValuePair_2_t1952_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1952_0_0_0 = { 3, GenInst_Int32_t359_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1952_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Int32_t359_0_0_0, &PointerEventData_t57_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t363_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t363_0_0_0_Types[] = { &KeyValuePair_2_t363_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t363_0_0_0 = { 1, GenInst_KeyValuePair_2_t363_0_0_0_Types };
extern const Il2CppType RaycastHit2D_t369_0_0_0;
static const Il2CppType* GenInst_RaycastHit2D_t369_0_0_0_Types[] = { &RaycastHit2D_t369_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastHit2D_t369_0_0_0 = { 1, GenInst_RaycastHit2D_t369_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0_Types[] = { &ICanvasElement_t344_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0_Types };
static const Il2CppType* GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &ICanvasElement_t344_0_0_0, &Int32_t359_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType List_1_t374_0_0_0;
static const Il2CppType* GenInst_List_1_t374_0_0_0_Types[] = { &List_1_t374_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t374_0_0_0 = { 1, GenInst_List_1_t374_0_0_0_Types };
static const Il2CppType* GenInst_Font_t191_0_0_0_List_1_t378_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Font_t191_0_0_0, &List_1_t378_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Font_t191_0_0_0_List_1_t378_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Font_t191_0_0_0_List_1_t378_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2013_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2013_0_0_0_Types[] = { &KeyValuePair_2_t2013_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2013_0_0_0 = { 1, GenInst_KeyValuePair_2_t2013_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0_Types[] = { &Graphic_t194_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0_Types };
static const Il2CppType* GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Graphic_t194_0_0_0, &Int32_t359_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Canvas_t197_0_0_0, &IndexedSet_1_t388_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2037_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2037_0_0_0_Types[] = { &KeyValuePair_2_t2037_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2037_0_0_0 = { 1, GenInst_KeyValuePair_2_t2037_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2040_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2040_0_0_0_Types[] = { &KeyValuePair_2_t2040_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2040_0_0_0 = { 1, GenInst_KeyValuePair_2_t2040_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2043_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2043_0_0_0_Types[] = { &KeyValuePair_2_t2043_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2043_0_0_0 = { 1, GenInst_KeyValuePair_2_t2043_0_0_0_Types };
extern const Il2CppType Enum_t665_0_0_0;
static const Il2CppType* GenInst_Enum_t665_0_0_0_Types[] = { &Enum_t665_0_0_0 };
extern const Il2CppGenericInst GenInst_Enum_t665_0_0_0 = { 1, GenInst_Enum_t665_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0_Types[] = { &IClipper_t353_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0_Types };
static const Il2CppType* GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &IClipper_t353_0_0_0, &Int32_t359_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2096_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2096_0_0_0_Types[] = { &KeyValuePair_2_t2096_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2096_0_0_0 = { 1, GenInst_KeyValuePair_2_t2096_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Single_t358_0_0_0_Types[] = { &Object_t_0_0_0, &Single_t358_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Single_t358_0_0_0 = { 2, GenInst_Object_t_0_0_0_Single_t358_0_0_0_Types };
extern const Il2CppType List_1_t312_0_0_0;
static const Il2CppType* GenInst_List_1_t312_0_0_0_Types[] = { &List_1_t312_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t312_0_0_0 = { 1, GenInst_List_1_t312_0_0_0_Types };
extern const Il2CppType List_1_t313_0_0_0;
static const Il2CppType* GenInst_List_1_t313_0_0_0_Types[] = { &List_1_t313_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t313_0_0_0 = { 1, GenInst_List_1_t313_0_0_0_Types };
extern const Il2CppType List_1_t314_0_0_0;
static const Il2CppType* GenInst_List_1_t314_0_0_0_Types[] = { &List_1_t314_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t314_0_0_0 = { 1, GenInst_List_1_t314_0_0_0_Types };
extern const Il2CppType List_1_t315_0_0_0;
static const Il2CppType* GenInst_List_1_t315_0_0_0_Types[] = { &List_1_t315_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t315_0_0_0 = { 1, GenInst_List_1_t315_0_0_0_Types };
extern const Il2CppType List_1_t316_0_0_0;
static const Il2CppType* GenInst_List_1_t316_0_0_0_Types[] = { &List_1_t316_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t316_0_0_0 = { 1, GenInst_List_1_t316_0_0_0_Types };
extern const Il2CppType List_1_t345_0_0_0;
static const Il2CppType* GenInst_List_1_t345_0_0_0_Types[] = { &List_1_t345_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t345_0_0_0 = { 1, GenInst_List_1_t345_0_0_0_Types };
extern const Il2CppType IAchievementDescription_t2653_0_0_0;
static const Il2CppType* GenInst_IAchievementDescription_t2653_0_0_0_Types[] = { &IAchievementDescription_t2653_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievementDescription_t2653_0_0_0 = { 1, GenInst_IAchievementDescription_t2653_0_0_0_Types };
extern const Il2CppType IAchievement_t641_0_0_0;
static const Il2CppType* GenInst_IAchievement_t641_0_0_0_Types[] = { &IAchievement_t641_0_0_0 };
extern const Il2CppGenericInst GenInst_IAchievement_t641_0_0_0 = { 1, GenInst_IAchievement_t641_0_0_0_Types };
extern const Il2CppType IScore_t596_0_0_0;
static const Il2CppType* GenInst_IScore_t596_0_0_0_Types[] = { &IScore_t596_0_0_0 };
extern const Il2CppGenericInst GenInst_IScore_t596_0_0_0 = { 1, GenInst_IScore_t596_0_0_0_Types };
extern const Il2CppType IUserProfile_t2652_0_0_0;
static const Il2CppType* GenInst_IUserProfile_t2652_0_0_0_Types[] = { &IUserProfile_t2652_0_0_0 };
extern const Il2CppGenericInst GenInst_IUserProfile_t2652_0_0_0 = { 1, GenInst_IUserProfile_t2652_0_0_0_Types };
extern const Il2CppType AchievementDescription_t591_0_0_0;
static const Il2CppType* GenInst_AchievementDescription_t591_0_0_0_Types[] = { &AchievementDescription_t591_0_0_0 };
extern const Il2CppGenericInst GenInst_AchievementDescription_t591_0_0_0 = { 1, GenInst_AchievementDescription_t591_0_0_0_Types };
extern const Il2CppType UserProfile_t588_0_0_0;
static const Il2CppType* GenInst_UserProfile_t588_0_0_0_Types[] = { &UserProfile_t588_0_0_0 };
extern const Il2CppGenericInst GenInst_UserProfile_t588_0_0_0 = { 1, GenInst_UserProfile_t588_0_0_0_Types };
extern const Il2CppType GcAchievementData_t581_0_0_0;
static const Il2CppType* GenInst_GcAchievementData_t581_0_0_0_Types[] = { &GcAchievementData_t581_0_0_0 };
extern const Il2CppGenericInst GenInst_GcAchievementData_t581_0_0_0 = { 1, GenInst_GcAchievementData_t581_0_0_0_Types };
extern const Il2CppType Achievement_t590_0_0_0;
static const Il2CppType* GenInst_Achievement_t590_0_0_0_Types[] = { &Achievement_t590_0_0_0 };
extern const Il2CppGenericInst GenInst_Achievement_t590_0_0_0 = { 1, GenInst_Achievement_t590_0_0_0_Types };
extern const Il2CppType GcScoreData_t582_0_0_0;
static const Il2CppType* GenInst_GcScoreData_t582_0_0_0_Types[] = { &GcScoreData_t582_0_0_0 };
extern const Il2CppGenericInst GenInst_GcScoreData_t582_0_0_0 = { 1, GenInst_GcScoreData_t582_0_0_0_Types };
extern const Il2CppType Score_t592_0_0_0;
static const Il2CppType* GenInst_Score_t592_0_0_0_Types[] = { &Score_t592_0_0_0 };
extern const Il2CppGenericInst GenInst_Score_t592_0_0_0 = { 1, GenInst_Score_t592_0_0_0_Types };
extern const Il2CppType Display_t490_0_0_0;
static const Il2CppType* GenInst_Display_t490_0_0_0_Types[] = { &Display_t490_0_0_0 };
extern const Il2CppGenericInst GenInst_Display_t490_0_0_0 = { 1, GenInst_Display_t490_0_0_0_Types };
extern const Il2CppType IntPtr_t_0_0_0;
static const Il2CppType* GenInst_IntPtr_t_0_0_0_Types[] = { &IntPtr_t_0_0_0 };
extern const Il2CppGenericInst GenInst_IntPtr_t_0_0_0 = { 1, GenInst_IntPtr_t_0_0_0_Types };
extern const Il2CppType ISerializable_t1793_0_0_0;
static const Il2CppType* GenInst_ISerializable_t1793_0_0_0_Types[] = { &ISerializable_t1793_0_0_0 };
extern const Il2CppGenericInst GenInst_ISerializable_t1793_0_0_0 = { 1, GenInst_ISerializable_t1793_0_0_0_Types };
extern const Il2CppType ContactPoint_t505_0_0_0;
static const Il2CppType* GenInst_ContactPoint_t505_0_0_0_Types[] = { &ContactPoint_t505_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint_t505_0_0_0 = { 1, GenInst_ContactPoint_t505_0_0_0_Types };
extern const Il2CppType ContactPoint2D_t510_0_0_0;
static const Il2CppType* GenInst_ContactPoint2D_t510_0_0_0_Types[] = { &ContactPoint2D_t510_0_0_0 };
extern const Il2CppGenericInst GenInst_ContactPoint2D_t510_0_0_0 = { 1, GenInst_ContactPoint2D_t510_0_0_0_Types };
extern const Il2CppType IComparable_1_t3182_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3182_0_0_0_Types[] = { &IComparable_1_t3182_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3182_0_0_0 = { 1, GenInst_IComparable_1_t3182_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3187_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3187_0_0_0_Types[] = { &IEquatable_1_t3187_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3187_0_0_0 = { 1, GenInst_IEquatable_1_t3187_0_0_0_Types };
extern const Il2CppType Keyframe_t524_0_0_0;
static const Il2CppType* GenInst_Keyframe_t524_0_0_0_Types[] = { &Keyframe_t524_0_0_0 };
extern const Il2CppGenericInst GenInst_Keyframe_t524_0_0_0 = { 1, GenInst_Keyframe_t524_0_0_0_Types };
extern const Il2CppType CharacterInfo_t533_0_0_0;
static const Il2CppType* GenInst_CharacterInfo_t533_0_0_0_Types[] = { &CharacterInfo_t533_0_0_0 };
extern const Il2CppGenericInst GenInst_CharacterInfo_t533_0_0_0 = { 1, GenInst_CharacterInfo_t533_0_0_0_Types };
extern const Il2CppType GUILayoutOption_t558_0_0_0;
static const Il2CppType* GenInst_GUILayoutOption_t558_0_0_0_Types[] = { &GUILayoutOption_t558_0_0_0 };
extern const Il2CppGenericInst GenInst_GUILayoutOption_t558_0_0_0 = { 1, GenInst_GUILayoutOption_t558_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Int32_t359_0_0_0, &LayoutCache_t549_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2240_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2240_0_0_0_Types[] = { &KeyValuePair_2_t2240_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2240_0_0_0 = { 1, GenInst_KeyValuePair_2_t2240_0_0_0_Types };
static const Il2CppType* GenInst_GUIStyle_t553_0_0_0_Types[] = { &GUIStyle_t553_0_0_0 };
extern const Il2CppGenericInst GenInst_GUIStyle_t553_0_0_0 = { 1, GenInst_GUIStyle_t553_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &String_t_0_0_0, &GUIStyle_t553_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2250_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2250_0_0_0_Types[] = { &KeyValuePair_2_t2250_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2250_0_0_0 = { 1, GenInst_KeyValuePair_2_t2250_0_0_0_Types };
extern const Il2CppType DisallowMultipleComponent_t572_0_0_0;
static const Il2CppType* GenInst_DisallowMultipleComponent_t572_0_0_0_Types[] = { &DisallowMultipleComponent_t572_0_0_0 };
extern const Il2CppGenericInst GenInst_DisallowMultipleComponent_t572_0_0_0 = { 1, GenInst_DisallowMultipleComponent_t572_0_0_0_Types };
extern const Il2CppType Attribute_t477_0_0_0;
static const Il2CppType* GenInst_Attribute_t477_0_0_0_Types[] = { &Attribute_t477_0_0_0 };
extern const Il2CppGenericInst GenInst_Attribute_t477_0_0_0 = { 1, GenInst_Attribute_t477_0_0_0_Types };
extern const Il2CppType _Attribute_t2672_0_0_0;
static const Il2CppType* GenInst__Attribute_t2672_0_0_0_Types[] = { &_Attribute_t2672_0_0_0 };
extern const Il2CppGenericInst GenInst__Attribute_t2672_0_0_0 = { 1, GenInst__Attribute_t2672_0_0_0_Types };
extern const Il2CppType ExecuteInEditMode_t575_0_0_0;
static const Il2CppType* GenInst_ExecuteInEditMode_t575_0_0_0_Types[] = { &ExecuteInEditMode_t575_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteInEditMode_t575_0_0_0 = { 1, GenInst_ExecuteInEditMode_t575_0_0_0_Types };
extern const Il2CppType RequireComponent_t573_0_0_0;
static const Il2CppType* GenInst_RequireComponent_t573_0_0_0_Types[] = { &RequireComponent_t573_0_0_0 };
extern const Il2CppGenericInst GenInst_RequireComponent_t573_0_0_0 = { 1, GenInst_RequireComponent_t573_0_0_0_Types };
extern const Il2CppType ParameterModifier_t1345_0_0_0;
static const Il2CppType* GenInst_ParameterModifier_t1345_0_0_0_Types[] = { &ParameterModifier_t1345_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterModifier_t1345_0_0_0 = { 1, GenInst_ParameterModifier_t1345_0_0_0_Types };
extern const Il2CppType HitInfo_t597_0_0_0;
static const Il2CppType* GenInst_HitInfo_t597_0_0_0_Types[] = { &HitInfo_t597_0_0_0 };
extern const Il2CppGenericInst GenInst_HitInfo_t597_0_0_0 = { 1, GenInst_HitInfo_t597_0_0_0_Types };
extern const Il2CppType ParameterInfo_t681_0_0_0;
static const Il2CppType* GenInst_ParameterInfo_t681_0_0_0_Types[] = { &ParameterInfo_t681_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterInfo_t681_0_0_0 = { 1, GenInst_ParameterInfo_t681_0_0_0_Types };
extern const Il2CppType _ParameterInfo_t2723_0_0_0;
static const Il2CppType* GenInst__ParameterInfo_t2723_0_0_0_Types[] = { &_ParameterInfo_t2723_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterInfo_t2723_0_0_0 = { 1, GenInst__ParameterInfo_t2723_0_0_0_Types };
extern const Il2CppType Event_t237_0_0_0;
extern const Il2CppType TextEditOp_t615_0_0_0;
static const Il2CppType* GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0_Types[] = { &Event_t237_0_0_0, &TextEditOp_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0 = { 2, GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0 = { 2, GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2270_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2270_0_0_0_Types[] = { &KeyValuePair_2_t2270_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2270_0_0_0 = { 1, GenInst_KeyValuePair_2_t2270_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t615_0_0_0_Types[] = { &TextEditOp_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t615_0_0_0 = { 1, GenInst_TextEditOp_t615_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t615_0_0_0, &TextEditOp_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t615_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_KeyValuePair_2_t2270_0_0_0_Types[] = { &Object_t_0_0_0, &TextEditOp_t615_0_0_0, &KeyValuePair_2_t2270_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_KeyValuePair_2_t2270_0_0_0 = { 3, GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_KeyValuePair_2_t2270_0_0_0_Types };
static const Il2CppType* GenInst_Event_t237_0_0_0_Types[] = { &Event_t237_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t237_0_0_0 = { 1, GenInst_Event_t237_0_0_0_Types };
static const Il2CppType* GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Event_t237_0_0_0, &TextEditOp_t615_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2281_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2281_0_0_0_Types[] = { &KeyValuePair_2_t2281_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2281_0_0_0 = { 1, GenInst_KeyValuePair_2_t2281_0_0_0_Types };
extern const Il2CppType Byte_t664_0_0_0;
static const Il2CppType* GenInst_Byte_t664_0_0_0_Types[] = { &Byte_t664_0_0_0 };
extern const Il2CppGenericInst GenInst_Byte_t664_0_0_0 = { 1, GenInst_Byte_t664_0_0_0_Types };
extern const Il2CppType IComparable_1_t3270_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3270_0_0_0_Types[] = { &IComparable_1_t3270_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3270_0_0_0 = { 1, GenInst_IComparable_1_t3270_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3275_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3275_0_0_0_Types[] = { &IEquatable_1_t3275_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3275_0_0_0 = { 1, GenInst_IEquatable_1_t3275_0_0_0_Types };
extern const Il2CppType KeySizes_t717_0_0_0;
static const Il2CppType* GenInst_KeySizes_t717_0_0_0_Types[] = { &KeySizes_t717_0_0_0 };
extern const Il2CppGenericInst GenInst_KeySizes_t717_0_0_0 = { 1, GenInst_KeySizes_t717_0_0_0_Types };
extern const Il2CppType UInt32_t677_0_0_0;
static const Il2CppType* GenInst_UInt32_t677_0_0_0_Types[] = { &UInt32_t677_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt32_t677_0_0_0 = { 1, GenInst_UInt32_t677_0_0_0_Types };
extern const Il2CppType IComparable_1_t3287_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3287_0_0_0_Types[] = { &IComparable_1_t3287_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3287_0_0_0 = { 1, GenInst_IComparable_1_t3287_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3292_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3292_0_0_0_Types[] = { &IEquatable_1_t3292_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3292_0_0_0 = { 1, GenInst_IEquatable_1_t3292_0_0_0_Types };
extern const Il2CppType BigInteger_t722_0_0_0;
static const Il2CppType* GenInst_BigInteger_t722_0_0_0_Types[] = { &BigInteger_t722_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t722_0_0_0 = { 1, GenInst_BigInteger_t722_0_0_0_Types };
extern const Il2CppType ByteU5BU5D_t698_0_0_0;
static const Il2CppType* GenInst_ByteU5BU5D_t698_0_0_0_Types[] = { &ByteU5BU5D_t698_0_0_0 };
extern const Il2CppGenericInst GenInst_ByteU5BU5D_t698_0_0_0 = { 1, GenInst_ByteU5BU5D_t698_0_0_0_Types };
extern const Il2CppType Array_t_0_0_0;
static const Il2CppType* GenInst_Array_t_0_0_0_Types[] = { &Array_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_t_0_0_0 = { 1, GenInst_Array_t_0_0_0_Types };
extern const Il2CppType ICollection_t1058_0_0_0;
static const Il2CppType* GenInst_ICollection_t1058_0_0_0_Types[] = { &ICollection_t1058_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_t1058_0_0_0 = { 1, GenInst_ICollection_t1058_0_0_0_Types };
extern const Il2CppType IList_t1018_0_0_0;
static const Il2CppType* GenInst_IList_t1018_0_0_0_Types[] = { &IList_t1018_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_t1018_0_0_0 = { 1, GenInst_IList_t1018_0_0_0_Types };
extern const Il2CppType X509Certificate_t829_0_0_0;
static const Il2CppType* GenInst_X509Certificate_t829_0_0_0_Types[] = { &X509Certificate_t829_0_0_0 };
extern const Il2CppGenericInst GenInst_X509Certificate_t829_0_0_0 = { 1, GenInst_X509Certificate_t829_0_0_0_Types };
extern const Il2CppType IDeserializationCallback_t1796_0_0_0;
static const Il2CppType* GenInst_IDeserializationCallback_t1796_0_0_0_Types[] = { &IDeserializationCallback_t1796_0_0_0 };
extern const Il2CppGenericInst GenInst_IDeserializationCallback_t1796_0_0_0 = { 1, GenInst_IDeserializationCallback_t1796_0_0_0_Types };
extern const Il2CppType ClientCertificateType_t833_0_0_0;
static const Il2CppType* GenInst_ClientCertificateType_t833_0_0_0_Types[] = { &ClientCertificateType_t833_0_0_0 };
extern const Il2CppGenericInst GenInst_ClientCertificateType_t833_0_0_0 = { 1, GenInst_ClientCertificateType_t833_0_0_0_Types };
extern const Il2CppType UInt16_t1064_0_0_0;
static const Il2CppType* GenInst_UInt16_t1064_0_0_0_Types[] = { &UInt16_t1064_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt16_t1064_0_0_0 = { 1, GenInst_UInt16_t1064_0_0_0_Types };
extern const Il2CppType IComparable_1_t3331_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3331_0_0_0_Types[] = { &IComparable_1_t3331_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3331_0_0_0 = { 1, GenInst_IComparable_1_t3331_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3336_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3336_0_0_0_Types[] = { &IEquatable_1_t3336_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3336_0_0_0 = { 1, GenInst_IEquatable_1_t3336_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2318_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2318_0_0_0_Types[] = { &KeyValuePair_2_t2318_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2318_0_0_0 = { 1, GenInst_KeyValuePair_2_t2318_0_0_0_Types };
extern const Il2CppType IComparable_1_t3348_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3348_0_0_0_Types[] = { &IComparable_1_t3348_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3348_0_0_0 = { 1, GenInst_IComparable_1_t3348_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3353_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3353_0_0_0_Types[] = { &IEquatable_1_t3353_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3353_0_0_0 = { 1, GenInst_IEquatable_1_t3353_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_Boolean_t360_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t360_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_Boolean_t360_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_Boolean_t360_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t360_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_KeyValuePair_2_t2318_0_0_0_Types[] = { &Object_t_0_0_0, &Boolean_t360_0_0_0, &KeyValuePair_2_t2318_0_0_0 };
extern const Il2CppGenericInst GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_KeyValuePair_2_t2318_0_0_0 = { 3, GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_KeyValuePair_2_t2318_0_0_0_Types };
static const Il2CppType* GenInst_String_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &String_t_0_0_0, &Boolean_t360_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_String_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_String_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2330_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2330_0_0_0_Types[] = { &KeyValuePair_2_t2330_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2330_0_0_0 = { 1, GenInst_KeyValuePair_2_t2330_0_0_0_Types };
extern const Il2CppType X509ChainStatus_t966_0_0_0;
static const Il2CppType* GenInst_X509ChainStatus_t966_0_0_0_Types[] = { &X509ChainStatus_t966_0_0_0 };
extern const Il2CppGenericInst GenInst_X509ChainStatus_t966_0_0_0 = { 1, GenInst_X509ChainStatus_t966_0_0_0_Types };
extern const Il2CppType Capture_t986_0_0_0;
static const Il2CppType* GenInst_Capture_t986_0_0_0_Types[] = { &Capture_t986_0_0_0 };
extern const Il2CppGenericInst GenInst_Capture_t986_0_0_0 = { 1, GenInst_Capture_t986_0_0_0_Types };
extern const Il2CppType Group_t904_0_0_0;
static const Il2CppType* GenInst_Group_t904_0_0_0_Types[] = { &Group_t904_0_0_0 };
extern const Il2CppGenericInst GenInst_Group_t904_0_0_0 = { 1, GenInst_Group_t904_0_0_0_Types };
extern const Il2CppType Mark_t1009_0_0_0;
static const Il2CppType* GenInst_Mark_t1009_0_0_0_Types[] = { &Mark_t1009_0_0_0 };
extern const Il2CppGenericInst GenInst_Mark_t1009_0_0_0 = { 1, GenInst_Mark_t1009_0_0_0_Types };
extern const Il2CppType UriScheme_t1046_0_0_0;
static const Il2CppType* GenInst_UriScheme_t1046_0_0_0_Types[] = { &UriScheme_t1046_0_0_0 };
extern const Il2CppGenericInst GenInst_UriScheme_t1046_0_0_0 = { 1, GenInst_UriScheme_t1046_0_0_0_Types };
extern const Il2CppType Int64_t676_0_0_0;
static const Il2CppType* GenInst_Int64_t676_0_0_0_Types[] = { &Int64_t676_0_0_0 };
extern const Il2CppGenericInst GenInst_Int64_t676_0_0_0 = { 1, GenInst_Int64_t676_0_0_0_Types };
extern const Il2CppType UInt64_t1076_0_0_0;
static const Il2CppType* GenInst_UInt64_t1076_0_0_0_Types[] = { &UInt64_t1076_0_0_0 };
extern const Il2CppGenericInst GenInst_UInt64_t1076_0_0_0 = { 1, GenInst_UInt64_t1076_0_0_0_Types };
extern const Il2CppType SByte_t1077_0_0_0;
static const Il2CppType* GenInst_SByte_t1077_0_0_0_Types[] = { &SByte_t1077_0_0_0 };
extern const Il2CppGenericInst GenInst_SByte_t1077_0_0_0 = { 1, GenInst_SByte_t1077_0_0_0_Types };
extern const Il2CppType Int16_t1078_0_0_0;
static const Il2CppType* GenInst_Int16_t1078_0_0_0_Types[] = { &Int16_t1078_0_0_0 };
extern const Il2CppGenericInst GenInst_Int16_t1078_0_0_0 = { 1, GenInst_Int16_t1078_0_0_0_Types };
extern const Il2CppType Decimal_t1079_0_0_0;
static const Il2CppType* GenInst_Decimal_t1079_0_0_0_Types[] = { &Decimal_t1079_0_0_0 };
extern const Il2CppGenericInst GenInst_Decimal_t1079_0_0_0 = { 1, GenInst_Decimal_t1079_0_0_0_Types };
extern const Il2CppType Delegate_t384_0_0_0;
static const Il2CppType* GenInst_Delegate_t384_0_0_0_Types[] = { &Delegate_t384_0_0_0 };
extern const Il2CppGenericInst GenInst_Delegate_t384_0_0_0 = { 1, GenInst_Delegate_t384_0_0_0_Types };
extern const Il2CppType IComparable_1_t3378_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3378_0_0_0_Types[] = { &IComparable_1_t3378_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3378_0_0_0 = { 1, GenInst_IComparable_1_t3378_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3379_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3379_0_0_0_Types[] = { &IEquatable_1_t3379_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3379_0_0_0 = { 1, GenInst_IEquatable_1_t3379_0_0_0_Types };
extern const Il2CppType IComparable_1_t3382_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3382_0_0_0_Types[] = { &IComparable_1_t3382_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3382_0_0_0 = { 1, GenInst_IComparable_1_t3382_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3383_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3383_0_0_0_Types[] = { &IEquatable_1_t3383_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3383_0_0_0 = { 1, GenInst_IEquatable_1_t3383_0_0_0_Types };
extern const Il2CppType IComparable_1_t3380_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3380_0_0_0_Types[] = { &IComparable_1_t3380_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3380_0_0_0 = { 1, GenInst_IComparable_1_t3380_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3381_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3381_0_0_0_Types[] = { &IEquatable_1_t3381_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3381_0_0_0 = { 1, GenInst_IEquatable_1_t3381_0_0_0_Types };
extern const Il2CppType IComparable_1_t3376_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3376_0_0_0_Types[] = { &IComparable_1_t3376_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3376_0_0_0 = { 1, GenInst_IComparable_1_t3376_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3377_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3377_0_0_0_Types[] = { &IEquatable_1_t3377_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3377_0_0_0 = { 1, GenInst_IEquatable_1_t3377_0_0_0_Types };
extern const Il2CppType FieldInfo_t_0_0_0;
static const Il2CppType* GenInst_FieldInfo_t_0_0_0_Types[] = { &FieldInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldInfo_t_0_0_0 = { 1, GenInst_FieldInfo_t_0_0_0_Types };
extern const Il2CppType _FieldInfo_t2719_0_0_0;
static const Il2CppType* GenInst__FieldInfo_t2719_0_0_0_Types[] = { &_FieldInfo_t2719_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldInfo_t2719_0_0_0 = { 1, GenInst__FieldInfo_t2719_0_0_0_Types };
extern const Il2CppType MethodInfo_t_0_0_0;
static const Il2CppType* GenInst_MethodInfo_t_0_0_0_Types[] = { &MethodInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodInfo_t_0_0_0 = { 1, GenInst_MethodInfo_t_0_0_0_Types };
extern const Il2CppType _MethodInfo_t2721_0_0_0;
static const Il2CppType* GenInst__MethodInfo_t2721_0_0_0_Types[] = { &_MethodInfo_t2721_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodInfo_t2721_0_0_0 = { 1, GenInst__MethodInfo_t2721_0_0_0_Types };
extern const Il2CppType MethodBase_t679_0_0_0;
static const Il2CppType* GenInst_MethodBase_t679_0_0_0_Types[] = { &MethodBase_t679_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBase_t679_0_0_0 = { 1, GenInst_MethodBase_t679_0_0_0_Types };
extern const Il2CppType _MethodBase_t2720_0_0_0;
static const Il2CppType* GenInst__MethodBase_t2720_0_0_0_Types[] = { &_MethodBase_t2720_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBase_t2720_0_0_0 = { 1, GenInst__MethodBase_t2720_0_0_0_Types };
extern const Il2CppType ConstructorInfo_t687_0_0_0;
static const Il2CppType* GenInst_ConstructorInfo_t687_0_0_0_Types[] = { &ConstructorInfo_t687_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorInfo_t687_0_0_0 = { 1, GenInst_ConstructorInfo_t687_0_0_0_Types };
extern const Il2CppType _ConstructorInfo_t2717_0_0_0;
static const Il2CppType* GenInst__ConstructorInfo_t2717_0_0_0_Types[] = { &_ConstructorInfo_t2717_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorInfo_t2717_0_0_0 = { 1, GenInst__ConstructorInfo_t2717_0_0_0_Types };
extern const Il2CppType TableRange_t1114_0_0_0;
static const Il2CppType* GenInst_TableRange_t1114_0_0_0_Types[] = { &TableRange_t1114_0_0_0 };
extern const Il2CppGenericInst GenInst_TableRange_t1114_0_0_0 = { 1, GenInst_TableRange_t1114_0_0_0_Types };
extern const Il2CppType TailoringInfo_t1117_0_0_0;
static const Il2CppType* GenInst_TailoringInfo_t1117_0_0_0_Types[] = { &TailoringInfo_t1117_0_0_0 };
extern const Il2CppGenericInst GenInst_TailoringInfo_t1117_0_0_0 = { 1, GenInst_TailoringInfo_t1117_0_0_0_Types };
extern const Il2CppType Contraction_t1118_0_0_0;
static const Il2CppType* GenInst_Contraction_t1118_0_0_0_Types[] = { &Contraction_t1118_0_0_0 };
extern const Il2CppGenericInst GenInst_Contraction_t1118_0_0_0 = { 1, GenInst_Contraction_t1118_0_0_0_Types };
extern const Il2CppType Level2Map_t1120_0_0_0;
static const Il2CppType* GenInst_Level2Map_t1120_0_0_0_Types[] = { &Level2Map_t1120_0_0_0 };
extern const Il2CppGenericInst GenInst_Level2Map_t1120_0_0_0 = { 1, GenInst_Level2Map_t1120_0_0_0_Types };
extern const Il2CppType BigInteger_t1141_0_0_0;
static const Il2CppType* GenInst_BigInteger_t1141_0_0_0_Types[] = { &BigInteger_t1141_0_0_0 };
extern const Il2CppGenericInst GenInst_BigInteger_t1141_0_0_0 = { 1, GenInst_BigInteger_t1141_0_0_0_Types };
extern const Il2CppType Slot_t1191_0_0_0;
static const Il2CppType* GenInst_Slot_t1191_0_0_0_Types[] = { &Slot_t1191_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1191_0_0_0 = { 1, GenInst_Slot_t1191_0_0_0_Types };
extern const Il2CppType Slot_t1199_0_0_0;
static const Il2CppType* GenInst_Slot_t1199_0_0_0_Types[] = { &Slot_t1199_0_0_0 };
extern const Il2CppGenericInst GenInst_Slot_t1199_0_0_0 = { 1, GenInst_Slot_t1199_0_0_0_Types };
extern const Il2CppType StackFrame_t678_0_0_0;
static const Il2CppType* GenInst_StackFrame_t678_0_0_0_Types[] = { &StackFrame_t678_0_0_0 };
extern const Il2CppGenericInst GenInst_StackFrame_t678_0_0_0 = { 1, GenInst_StackFrame_t678_0_0_0_Types };
extern const Il2CppType Calendar_t1212_0_0_0;
static const Il2CppType* GenInst_Calendar_t1212_0_0_0_Types[] = { &Calendar_t1212_0_0_0 };
extern const Il2CppGenericInst GenInst_Calendar_t1212_0_0_0 = { 1, GenInst_Calendar_t1212_0_0_0_Types };
extern const Il2CppType ModuleBuilder_t1289_0_0_0;
static const Il2CppType* GenInst_ModuleBuilder_t1289_0_0_0_Types[] = { &ModuleBuilder_t1289_0_0_0 };
extern const Il2CppGenericInst GenInst_ModuleBuilder_t1289_0_0_0 = { 1, GenInst_ModuleBuilder_t1289_0_0_0_Types };
extern const Il2CppType _ModuleBuilder_t2712_0_0_0;
static const Il2CppType* GenInst__ModuleBuilder_t2712_0_0_0_Types[] = { &_ModuleBuilder_t2712_0_0_0 };
extern const Il2CppGenericInst GenInst__ModuleBuilder_t2712_0_0_0 = { 1, GenInst__ModuleBuilder_t2712_0_0_0_Types };
extern const Il2CppType Module_t1285_0_0_0;
static const Il2CppType* GenInst_Module_t1285_0_0_0_Types[] = { &Module_t1285_0_0_0 };
extern const Il2CppGenericInst GenInst_Module_t1285_0_0_0 = { 1, GenInst_Module_t1285_0_0_0_Types };
extern const Il2CppType _Module_t2722_0_0_0;
static const Il2CppType* GenInst__Module_t2722_0_0_0_Types[] = { &_Module_t2722_0_0_0 };
extern const Il2CppGenericInst GenInst__Module_t2722_0_0_0 = { 1, GenInst__Module_t2722_0_0_0_Types };
extern const Il2CppType ParameterBuilder_t1295_0_0_0;
static const Il2CppType* GenInst_ParameterBuilder_t1295_0_0_0_Types[] = { &ParameterBuilder_t1295_0_0_0 };
extern const Il2CppGenericInst GenInst_ParameterBuilder_t1295_0_0_0 = { 1, GenInst_ParameterBuilder_t1295_0_0_0_Types };
extern const Il2CppType _ParameterBuilder_t2713_0_0_0;
static const Il2CppType* GenInst__ParameterBuilder_t2713_0_0_0_Types[] = { &_ParameterBuilder_t2713_0_0_0 };
extern const Il2CppGenericInst GenInst__ParameterBuilder_t2713_0_0_0 = { 1, GenInst__ParameterBuilder_t2713_0_0_0_Types };
extern const Il2CppType TypeU5BU5D_t650_0_0_0;
static const Il2CppType* GenInst_TypeU5BU5D_t650_0_0_0_Types[] = { &TypeU5BU5D_t650_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeU5BU5D_t650_0_0_0 = { 1, GenInst_TypeU5BU5D_t650_0_0_0_Types };
extern const Il2CppType ILTokenInfo_t1279_0_0_0;
static const Il2CppType* GenInst_ILTokenInfo_t1279_0_0_0_Types[] = { &ILTokenInfo_t1279_0_0_0 };
extern const Il2CppGenericInst GenInst_ILTokenInfo_t1279_0_0_0 = { 1, GenInst_ILTokenInfo_t1279_0_0_0_Types };
extern const Il2CppType LabelData_t1281_0_0_0;
static const Il2CppType* GenInst_LabelData_t1281_0_0_0_Types[] = { &LabelData_t1281_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelData_t1281_0_0_0 = { 1, GenInst_LabelData_t1281_0_0_0_Types };
extern const Il2CppType LabelFixup_t1280_0_0_0;
static const Il2CppType* GenInst_LabelFixup_t1280_0_0_0_Types[] = { &LabelFixup_t1280_0_0_0 };
extern const Il2CppGenericInst GenInst_LabelFixup_t1280_0_0_0 = { 1, GenInst_LabelFixup_t1280_0_0_0_Types };
extern const Il2CppType GenericTypeParameterBuilder_t1277_0_0_0;
static const Il2CppType* GenInst_GenericTypeParameterBuilder_t1277_0_0_0_Types[] = { &GenericTypeParameterBuilder_t1277_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericTypeParameterBuilder_t1277_0_0_0 = { 1, GenInst_GenericTypeParameterBuilder_t1277_0_0_0_Types };
extern const Il2CppType TypeBuilder_t1271_0_0_0;
static const Il2CppType* GenInst_TypeBuilder_t1271_0_0_0_Types[] = { &TypeBuilder_t1271_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeBuilder_t1271_0_0_0 = { 1, GenInst_TypeBuilder_t1271_0_0_0_Types };
extern const Il2CppType _TypeBuilder_t2714_0_0_0;
static const Il2CppType* GenInst__TypeBuilder_t2714_0_0_0_Types[] = { &_TypeBuilder_t2714_0_0_0 };
extern const Il2CppGenericInst GenInst__TypeBuilder_t2714_0_0_0 = { 1, GenInst__TypeBuilder_t2714_0_0_0_Types };
extern const Il2CppType MethodBuilder_t1278_0_0_0;
static const Il2CppType* GenInst_MethodBuilder_t1278_0_0_0_Types[] = { &MethodBuilder_t1278_0_0_0 };
extern const Il2CppGenericInst GenInst_MethodBuilder_t1278_0_0_0 = { 1, GenInst_MethodBuilder_t1278_0_0_0_Types };
extern const Il2CppType _MethodBuilder_t2711_0_0_0;
static const Il2CppType* GenInst__MethodBuilder_t2711_0_0_0_Types[] = { &_MethodBuilder_t2711_0_0_0 };
extern const Il2CppGenericInst GenInst__MethodBuilder_t2711_0_0_0 = { 1, GenInst__MethodBuilder_t2711_0_0_0_Types };
extern const Il2CppType ConstructorBuilder_t1269_0_0_0;
static const Il2CppType* GenInst_ConstructorBuilder_t1269_0_0_0_Types[] = { &ConstructorBuilder_t1269_0_0_0 };
extern const Il2CppGenericInst GenInst_ConstructorBuilder_t1269_0_0_0 = { 1, GenInst_ConstructorBuilder_t1269_0_0_0_Types };
extern const Il2CppType _ConstructorBuilder_t2707_0_0_0;
static const Il2CppType* GenInst__ConstructorBuilder_t2707_0_0_0_Types[] = { &_ConstructorBuilder_t2707_0_0_0 };
extern const Il2CppGenericInst GenInst__ConstructorBuilder_t2707_0_0_0 = { 1, GenInst__ConstructorBuilder_t2707_0_0_0_Types };
extern const Il2CppType FieldBuilder_t1275_0_0_0;
static const Il2CppType* GenInst_FieldBuilder_t1275_0_0_0_Types[] = { &FieldBuilder_t1275_0_0_0 };
extern const Il2CppGenericInst GenInst_FieldBuilder_t1275_0_0_0 = { 1, GenInst_FieldBuilder_t1275_0_0_0_Types };
extern const Il2CppType _FieldBuilder_t2709_0_0_0;
static const Il2CppType* GenInst__FieldBuilder_t2709_0_0_0_Types[] = { &_FieldBuilder_t2709_0_0_0 };
extern const Il2CppGenericInst GenInst__FieldBuilder_t2709_0_0_0 = { 1, GenInst__FieldBuilder_t2709_0_0_0_Types };
extern const Il2CppType PropertyInfo_t_0_0_0;
static const Il2CppType* GenInst_PropertyInfo_t_0_0_0_Types[] = { &PropertyInfo_t_0_0_0 };
extern const Il2CppGenericInst GenInst_PropertyInfo_t_0_0_0 = { 1, GenInst_PropertyInfo_t_0_0_0_Types };
extern const Il2CppType _PropertyInfo_t2724_0_0_0;
static const Il2CppType* GenInst__PropertyInfo_t2724_0_0_0_Types[] = { &_PropertyInfo_t2724_0_0_0 };
extern const Il2CppGenericInst GenInst__PropertyInfo_t2724_0_0_0 = { 1, GenInst__PropertyInfo_t2724_0_0_0_Types };
extern const Il2CppType ResourceInfo_t1356_0_0_0;
static const Il2CppType* GenInst_ResourceInfo_t1356_0_0_0_Types[] = { &ResourceInfo_t1356_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceInfo_t1356_0_0_0 = { 1, GenInst_ResourceInfo_t1356_0_0_0_Types };
extern const Il2CppType ResourceCacheItem_t1357_0_0_0;
static const Il2CppType* GenInst_ResourceCacheItem_t1357_0_0_0_Types[] = { &ResourceCacheItem_t1357_0_0_0 };
extern const Il2CppGenericInst GenInst_ResourceCacheItem_t1357_0_0_0 = { 1, GenInst_ResourceCacheItem_t1357_0_0_0_Types };
extern const Il2CppType IContextProperty_t1753_0_0_0;
static const Il2CppType* GenInst_IContextProperty_t1753_0_0_0_Types[] = { &IContextProperty_t1753_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextProperty_t1753_0_0_0 = { 1, GenInst_IContextProperty_t1753_0_0_0_Types };
extern const Il2CppType Header_t1439_0_0_0;
static const Il2CppType* GenInst_Header_t1439_0_0_0_Types[] = { &Header_t1439_0_0_0 };
extern const Il2CppGenericInst GenInst_Header_t1439_0_0_0 = { 1, GenInst_Header_t1439_0_0_0_Types };
extern const Il2CppType ITrackingHandler_t1788_0_0_0;
static const Il2CppType* GenInst_ITrackingHandler_t1788_0_0_0_Types[] = { &ITrackingHandler_t1788_0_0_0 };
extern const Il2CppGenericInst GenInst_ITrackingHandler_t1788_0_0_0 = { 1, GenInst_ITrackingHandler_t1788_0_0_0_Types };
extern const Il2CppType IContextAttribute_t1775_0_0_0;
static const Il2CppType* GenInst_IContextAttribute_t1775_0_0_0_Types[] = { &IContextAttribute_t1775_0_0_0 };
extern const Il2CppGenericInst GenInst_IContextAttribute_t1775_0_0_0 = { 1, GenInst_IContextAttribute_t1775_0_0_0_Types };
extern const Il2CppType IComparable_1_t3612_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3612_0_0_0_Types[] = { &IComparable_1_t3612_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3612_0_0_0 = { 1, GenInst_IComparable_1_t3612_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3617_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3617_0_0_0_Types[] = { &IEquatable_1_t3617_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3617_0_0_0 = { 1, GenInst_IEquatable_1_t3617_0_0_0_Types };
extern const Il2CppType IComparable_1_t3384_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3384_0_0_0_Types[] = { &IComparable_1_t3384_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3384_0_0_0 = { 1, GenInst_IComparable_1_t3384_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3385_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3385_0_0_0_Types[] = { &IEquatable_1_t3385_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3385_0_0_0 = { 1, GenInst_IEquatable_1_t3385_0_0_0_Types };
extern const Il2CppType IComparable_1_t3636_0_0_0;
static const Il2CppType* GenInst_IComparable_1_t3636_0_0_0_Types[] = { &IComparable_1_t3636_0_0_0 };
extern const Il2CppGenericInst GenInst_IComparable_1_t3636_0_0_0 = { 1, GenInst_IComparable_1_t3636_0_0_0_Types };
extern const Il2CppType IEquatable_1_t3641_0_0_0;
static const Il2CppType* GenInst_IEquatable_1_t3641_0_0_0_Types[] = { &IEquatable_1_t3641_0_0_0 };
extern const Il2CppGenericInst GenInst_IEquatable_1_t3641_0_0_0 = { 1, GenInst_IEquatable_1_t3641_0_0_0_Types };
extern const Il2CppType TypeTag_t1495_0_0_0;
static const Il2CppType* GenInst_TypeTag_t1495_0_0_0_Types[] = { &TypeTag_t1495_0_0_0 };
extern const Il2CppGenericInst GenInst_TypeTag_t1495_0_0_0 = { 1, GenInst_TypeTag_t1495_0_0_0_Types };
extern const Il2CppType MonoType_t_0_0_0;
static const Il2CppType* GenInst_MonoType_t_0_0_0_Types[] = { &MonoType_t_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoType_t_0_0_0 = { 1, GenInst_MonoType_t_0_0_0_Types };
extern const Il2CppType Version_t937_0_0_0;
static const Il2CppType* GenInst_Version_t937_0_0_0_Types[] = { &Version_t937_0_0_0 };
extern const Il2CppGenericInst GenInst_Version_t937_0_0_0 = { 1, GenInst_Version_t937_0_0_0_Types };
static const Il2CppType* GenInst_DictionaryEntry_t1057_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &DictionaryEntry_t1057_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_DictionaryEntry_t1057_0_0_0_DictionaryEntry_t1057_0_0_0 = { 2, GenInst_DictionaryEntry_t1057_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1817_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1817_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1817_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1817_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1817_0_0_0_KeyValuePair_2_t1817_0_0_0_Types[] = { &KeyValuePair_2_t1817_0_0_0, &KeyValuePair_2_t1817_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1817_0_0_0_KeyValuePair_2_t1817_0_0_0 = { 2, GenInst_KeyValuePair_2_t1817_0_0_0_KeyValuePair_2_t1817_0_0_0_Types };
static const Il2CppType* GenInst_Int32_t359_0_0_0_Int32_t359_0_0_0_Types[] = { &Int32_t359_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_Int32_t359_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_Int32_t359_0_0_0_Int32_t359_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1864_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1864_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1864_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1864_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1864_0_0_0_KeyValuePair_2_t1864_0_0_0_Types[] = { &KeyValuePair_2_t1864_0_0_0, &KeyValuePair_2_t1864_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1864_0_0_0_KeyValuePair_2_t1864_0_0_0 = { 2, GenInst_KeyValuePair_2_t1864_0_0_0_KeyValuePair_2_t1864_0_0_0_Types };
static const Il2CppType* GenInst_RaycastResult_t139_0_0_0_RaycastResult_t139_0_0_0_Types[] = { &RaycastResult_t139_0_0_0, &RaycastResult_t139_0_0_0 };
extern const Il2CppGenericInst GenInst_RaycastResult_t139_0_0_0_RaycastResult_t139_0_0_0 = { 2, GenInst_RaycastResult_t139_0_0_0_RaycastResult_t139_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1952_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t1952_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1952_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t1952_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t1952_0_0_0_KeyValuePair_2_t1952_0_0_0_Types[] = { &KeyValuePair_2_t1952_0_0_0, &KeyValuePair_2_t1952_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t1952_0_0_0_KeyValuePair_2_t1952_0_0_0 = { 2, GenInst_KeyValuePair_2_t1952_0_0_0_KeyValuePair_2_t1952_0_0_0_Types };
static const Il2CppType* GenInst_UIVertex_t239_0_0_0_UIVertex_t239_0_0_0_Types[] = { &UIVertex_t239_0_0_0, &UIVertex_t239_0_0_0 };
extern const Il2CppGenericInst GenInst_UIVertex_t239_0_0_0_UIVertex_t239_0_0_0 = { 2, GenInst_UIVertex_t239_0_0_0_UIVertex_t239_0_0_0_Types };
static const Il2CppType* GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0_Types[] = { &Vector3_t12_0_0_0, &Vector3_t12_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0 = { 2, GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0_Types };
static const Il2CppType* GenInst_Color32_t347_0_0_0_Color32_t347_0_0_0_Types[] = { &Color32_t347_0_0_0, &Color32_t347_0_0_0 };
extern const Il2CppGenericInst GenInst_Color32_t347_0_0_0_Color32_t347_0_0_0 = { 2, GenInst_Color32_t347_0_0_0_Color32_t347_0_0_0_Types };
static const Il2CppType* GenInst_Vector2_t23_0_0_0_Vector2_t23_0_0_0_Types[] = { &Vector2_t23_0_0_0, &Vector2_t23_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector2_t23_0_0_0_Vector2_t23_0_0_0 = { 2, GenInst_Vector2_t23_0_0_0_Vector2_t23_0_0_0_Types };
static const Il2CppType* GenInst_Vector4_t317_0_0_0_Vector4_t317_0_0_0_Types[] = { &Vector4_t317_0_0_0, &Vector4_t317_0_0_0 };
extern const Il2CppGenericInst GenInst_Vector4_t317_0_0_0_Vector4_t317_0_0_0 = { 2, GenInst_Vector4_t317_0_0_0_Vector4_t317_0_0_0_Types };
static const Il2CppType* GenInst_UICharInfo_t396_0_0_0_UICharInfo_t396_0_0_0_Types[] = { &UICharInfo_t396_0_0_0, &UICharInfo_t396_0_0_0 };
extern const Il2CppGenericInst GenInst_UICharInfo_t396_0_0_0_UICharInfo_t396_0_0_0 = { 2, GenInst_UICharInfo_t396_0_0_0_UICharInfo_t396_0_0_0_Types };
static const Il2CppType* GenInst_UILineInfo_t394_0_0_0_UILineInfo_t394_0_0_0_Types[] = { &UILineInfo_t394_0_0_0, &UILineInfo_t394_0_0_0 };
extern const Il2CppGenericInst GenInst_UILineInfo_t394_0_0_0_UILineInfo_t394_0_0_0 = { 2, GenInst_UILineInfo_t394_0_0_0_UILineInfo_t394_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t615_0_0_0_Object_t_0_0_0_Types[] = { &TextEditOp_t615_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t615_0_0_0_Object_t_0_0_0 = { 2, GenInst_TextEditOp_t615_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0_Types[] = { &TextEditOp_t615_0_0_0, &TextEditOp_t615_0_0_0 };
extern const Il2CppGenericInst GenInst_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0 = { 2, GenInst_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2270_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2270_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2270_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2270_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2270_0_0_0_KeyValuePair_2_t2270_0_0_0_Types[] = { &KeyValuePair_2_t2270_0_0_0, &KeyValuePair_2_t2270_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2270_0_0_0_KeyValuePair_2_t2270_0_0_0 = { 2, GenInst_KeyValuePair_2_t2270_0_0_0_KeyValuePair_2_t2270_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t360_0_0_0_Object_t_0_0_0_Types[] = { &Boolean_t360_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t360_0_0_0_Object_t_0_0_0 = { 2, GenInst_Boolean_t360_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Boolean_t360_0_0_0_Boolean_t360_0_0_0_Types[] = { &Boolean_t360_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Boolean_t360_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_Boolean_t360_0_0_0_Boolean_t360_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2318_0_0_0_Object_t_0_0_0_Types[] = { &KeyValuePair_2_t2318_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2318_0_0_0_Object_t_0_0_0 = { 2, GenInst_KeyValuePair_2_t2318_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t2318_0_0_0_KeyValuePair_2_t2318_0_0_0_Types[] = { &KeyValuePair_2_t2318_0_0_0, &KeyValuePair_2_t2318_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2318_0_0_0_KeyValuePair_2_t2318_0_0_0 = { 2, GenInst_KeyValuePair_2_t2318_0_0_0_KeyValuePair_2_t2318_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeTypedArgument_t1326_0_0_0_CustomAttributeTypedArgument_t1326_0_0_0_Types[] = { &CustomAttributeTypedArgument_t1326_0_0_0, &CustomAttributeTypedArgument_t1326_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeTypedArgument_t1326_0_0_0_CustomAttributeTypedArgument_t1326_0_0_0 = { 2, GenInst_CustomAttributeTypedArgument_t1326_0_0_0_CustomAttributeTypedArgument_t1326_0_0_0_Types };
static const Il2CppType* GenInst_CustomAttributeNamedArgument_t1325_0_0_0_CustomAttributeNamedArgument_t1325_0_0_0_Types[] = { &CustomAttributeNamedArgument_t1325_0_0_0, &CustomAttributeNamedArgument_t1325_0_0_0 };
extern const Il2CppGenericInst GenInst_CustomAttributeNamedArgument_t1325_0_0_0_CustomAttributeNamedArgument_t1325_0_0_0 = { 2, GenInst_CustomAttributeNamedArgument_t1325_0_0_0_CustomAttributeNamedArgument_t1325_0_0_0_Types };
extern const Il2CppType ExecuteEvents_Execute_m19418_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_Execute_m19418_gp_0_0_0_0_Types[] = { &ExecuteEvents_Execute_m19418_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_Execute_m19418_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_Execute_m19418_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_ExecuteHierarchy_m19419_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_ExecuteHierarchy_m19419_gp_0_0_0_0_Types[] = { &ExecuteEvents_ExecuteHierarchy_m19419_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_ExecuteHierarchy_m19419_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_ExecuteHierarchy_m19419_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventList_m19421_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventList_m19421_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventList_m19421_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventList_m19421_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventList_m19421_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_CanHandleEvent_m19422_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_CanHandleEvent_m19422_gp_0_0_0_0_Types[] = { &ExecuteEvents_CanHandleEvent_m19422_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_CanHandleEvent_m19422_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_CanHandleEvent_m19422_gp_0_0_0_0_Types };
extern const Il2CppType ExecuteEvents_GetEventHandler_m19423_gp_0_0_0_0;
static const Il2CppType* GenInst_ExecuteEvents_GetEventHandler_m19423_gp_0_0_0_0_Types[] = { &ExecuteEvents_GetEventHandler_m19423_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ExecuteEvents_GetEventHandler_m19423_gp_0_0_0_0 = { 1, GenInst_ExecuteEvents_GetEventHandler_m19423_gp_0_0_0_0_Types };
extern const Il2CppType TweenRunner_1_t2640_gp_0_0_0_0;
static const Il2CppType* GenInst_TweenRunner_1_t2640_gp_0_0_0_0_Types[] = { &TweenRunner_1_t2640_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_TweenRunner_1_t2640_gp_0_0_0_0 = { 1, GenInst_TweenRunner_1_t2640_gp_0_0_0_0_Types };
extern const Il2CppType Dropdown_GetOrAddComponent_m19450_gp_0_0_0_0;
static const Il2CppType* GenInst_Dropdown_GetOrAddComponent_m19450_gp_0_0_0_0_Types[] = { &Dropdown_GetOrAddComponent_m19450_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dropdown_GetOrAddComponent_m19450_gp_0_0_0_0 = { 1, GenInst_Dropdown_GetOrAddComponent_m19450_gp_0_0_0_0_Types };
extern const Il2CppType IndexedSet_1_t2644_gp_0_0_0_0;
static const Il2CppType* GenInst_IndexedSet_1_t2644_gp_0_0_0_0_Types[] = { &IndexedSet_1_t2644_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2644_gp_0_0_0_0 = { 1, GenInst_IndexedSet_1_t2644_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_IndexedSet_1_t2644_gp_0_0_0_0_Int32_t359_0_0_0_Types[] = { &IndexedSet_1_t2644_gp_0_0_0_0, &Int32_t359_0_0_0 };
extern const Il2CppGenericInst GenInst_IndexedSet_1_t2644_gp_0_0_0_0_Int32_t359_0_0_0 = { 2, GenInst_IndexedSet_1_t2644_gp_0_0_0_0_Int32_t359_0_0_0_Types };
extern const Il2CppType ListPool_1_t2645_gp_0_0_0_0;
static const Il2CppType* GenInst_ListPool_1_t2645_gp_0_0_0_0_Types[] = { &ListPool_1_t2645_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ListPool_1_t2645_gp_0_0_0_0 = { 1, GenInst_ListPool_1_t2645_gp_0_0_0_0_Types };
extern const Il2CppType List_1_t3688_0_0_0;
static const Il2CppType* GenInst_List_1_t3688_0_0_0_Types[] = { &List_1_t3688_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t3688_0_0_0 = { 1, GenInst_List_1_t3688_0_0_0_Types };
extern const Il2CppType ObjectPool_1_t2646_gp_0_0_0_0;
static const Il2CppType* GenInst_ObjectPool_1_t2646_gp_0_0_0_0_Types[] = { &ObjectPool_1_t2646_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ObjectPool_1_t2646_gp_0_0_0_0 = { 1, GenInst_ObjectPool_1_t2646_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m19519_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m19519_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m19519_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m19519_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m19519_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponentsInChildren_m19520_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponentsInChildren_m19520_gp_0_0_0_0_Types[] = { &Component_GetComponentsInChildren_m19520_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponentsInChildren_m19520_gp_0_0_0_0 = { 1, GenInst_Component_GetComponentsInChildren_m19520_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m19522_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m19522_gp_0_0_0_0_Types[] = { &Component_GetComponents_m19522_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m19522_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m19522_gp_0_0_0_0_Types };
extern const Il2CppType Component_GetComponents_m19523_gp_0_0_0_0;
static const Il2CppType* GenInst_Component_GetComponents_m19523_gp_0_0_0_0_Types[] = { &Component_GetComponents_m19523_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Component_GetComponents_m19523_gp_0_0_0_0 = { 1, GenInst_Component_GetComponents_m19523_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponents_m19527_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponents_m19527_gp_0_0_0_0_Types[] = { &GameObject_GetComponents_m19527_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponents_m19527_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponents_m19527_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInChildren_m19528_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInChildren_m19528_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInChildren_m19528_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInChildren_m19528_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInChildren_m19528_gp_0_0_0_0_Types };
extern const Il2CppType GameObject_GetComponentsInParent_m19529_gp_0_0_0_0;
static const Il2CppType* GenInst_GameObject_GetComponentsInParent_m19529_gp_0_0_0_0_Types[] = { &GameObject_GetComponentsInParent_m19529_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GameObject_GetComponentsInParent_m19529_gp_0_0_0_0 = { 1, GenInst_GameObject_GetComponentsInParent_m19529_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_1_t2654_gp_0_0_0_0;
static const Il2CppType* GenInst_InvokableCall_1_t2654_gp_0_0_0_0_Types[] = { &InvokableCall_1_t2654_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_1_t2654_gp_0_0_0_0 = { 1, GenInst_InvokableCall_1_t2654_gp_0_0_0_0_Types };
extern const Il2CppType InvokableCall_2_t2655_gp_0_0_0_0;
extern const Il2CppType InvokableCall_2_t2655_gp_1_0_0_0;
static const Il2CppType* GenInst_InvokableCall_2_t2655_gp_0_0_0_0_InvokableCall_2_t2655_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2655_gp_0_0_0_0, &InvokableCall_2_t2655_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2655_gp_0_0_0_0_InvokableCall_2_t2655_gp_1_0_0_0 = { 2, GenInst_InvokableCall_2_t2655_gp_0_0_0_0_InvokableCall_2_t2655_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2655_gp_0_0_0_0_Types[] = { &InvokableCall_2_t2655_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2655_gp_0_0_0_0 = { 1, GenInst_InvokableCall_2_t2655_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_2_t2655_gp_1_0_0_0_Types[] = { &InvokableCall_2_t2655_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_2_t2655_gp_1_0_0_0 = { 1, GenInst_InvokableCall_2_t2655_gp_1_0_0_0_Types };
extern const Il2CppType InvokableCall_3_t2656_gp_0_0_0_0;
extern const Il2CppType InvokableCall_3_t2656_gp_1_0_0_0;
extern const Il2CppType InvokableCall_3_t2656_gp_2_0_0_0;
static const Il2CppType* GenInst_InvokableCall_3_t2656_gp_0_0_0_0_InvokableCall_3_t2656_gp_1_0_0_0_InvokableCall_3_t2656_gp_2_0_0_0_Types[] = { &InvokableCall_3_t2656_gp_0_0_0_0, &InvokableCall_3_t2656_gp_1_0_0_0, &InvokableCall_3_t2656_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2656_gp_0_0_0_0_InvokableCall_3_t2656_gp_1_0_0_0_InvokableCall_3_t2656_gp_2_0_0_0 = { 3, GenInst_InvokableCall_3_t2656_gp_0_0_0_0_InvokableCall_3_t2656_gp_1_0_0_0_InvokableCall_3_t2656_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2656_gp_0_0_0_0_Types[] = { &InvokableCall_3_t2656_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2656_gp_0_0_0_0 = { 1, GenInst_InvokableCall_3_t2656_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2656_gp_1_0_0_0_Types[] = { &InvokableCall_3_t2656_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2656_gp_1_0_0_0 = { 1, GenInst_InvokableCall_3_t2656_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_3_t2656_gp_2_0_0_0_Types[] = { &InvokableCall_3_t2656_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_3_t2656_gp_2_0_0_0 = { 1, GenInst_InvokableCall_3_t2656_gp_2_0_0_0_Types };
extern const Il2CppType InvokableCall_4_t2657_gp_0_0_0_0;
extern const Il2CppType InvokableCall_4_t2657_gp_1_0_0_0;
extern const Il2CppType InvokableCall_4_t2657_gp_2_0_0_0;
extern const Il2CppType InvokableCall_4_t2657_gp_3_0_0_0;
static const Il2CppType* GenInst_InvokableCall_4_t2657_gp_0_0_0_0_InvokableCall_4_t2657_gp_1_0_0_0_InvokableCall_4_t2657_gp_2_0_0_0_InvokableCall_4_t2657_gp_3_0_0_0_Types[] = { &InvokableCall_4_t2657_gp_0_0_0_0, &InvokableCall_4_t2657_gp_1_0_0_0, &InvokableCall_4_t2657_gp_2_0_0_0, &InvokableCall_4_t2657_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2657_gp_0_0_0_0_InvokableCall_4_t2657_gp_1_0_0_0_InvokableCall_4_t2657_gp_2_0_0_0_InvokableCall_4_t2657_gp_3_0_0_0 = { 4, GenInst_InvokableCall_4_t2657_gp_0_0_0_0_InvokableCall_4_t2657_gp_1_0_0_0_InvokableCall_4_t2657_gp_2_0_0_0_InvokableCall_4_t2657_gp_3_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2657_gp_0_0_0_0_Types[] = { &InvokableCall_4_t2657_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2657_gp_0_0_0_0 = { 1, GenInst_InvokableCall_4_t2657_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2657_gp_1_0_0_0_Types[] = { &InvokableCall_4_t2657_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2657_gp_1_0_0_0 = { 1, GenInst_InvokableCall_4_t2657_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2657_gp_2_0_0_0_Types[] = { &InvokableCall_4_t2657_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2657_gp_2_0_0_0 = { 1, GenInst_InvokableCall_4_t2657_gp_2_0_0_0_Types };
static const Il2CppType* GenInst_InvokableCall_4_t2657_gp_3_0_0_0_Types[] = { &InvokableCall_4_t2657_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_InvokableCall_4_t2657_gp_3_0_0_0 = { 1, GenInst_InvokableCall_4_t2657_gp_3_0_0_0_Types };
extern const Il2CppType CachedInvokableCall_1_t688_gp_0_0_0_0;
static const Il2CppType* GenInst_CachedInvokableCall_1_t688_gp_0_0_0_0_Types[] = { &CachedInvokableCall_1_t688_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_CachedInvokableCall_1_t688_gp_0_0_0_0 = { 1, GenInst_CachedInvokableCall_1_t688_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_1_t2658_gp_0_0_0_0;
static const Il2CppType* GenInst_UnityEvent_1_t2658_gp_0_0_0_0_Types[] = { &UnityEvent_1_t2658_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_1_t2658_gp_0_0_0_0 = { 1, GenInst_UnityEvent_1_t2658_gp_0_0_0_0_Types };
extern const Il2CppType UnityEvent_2_t2659_gp_0_0_0_0;
extern const Il2CppType UnityEvent_2_t2659_gp_1_0_0_0;
static const Il2CppType* GenInst_UnityEvent_2_t2659_gp_0_0_0_0_UnityEvent_2_t2659_gp_1_0_0_0_Types[] = { &UnityEvent_2_t2659_gp_0_0_0_0, &UnityEvent_2_t2659_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_2_t2659_gp_0_0_0_0_UnityEvent_2_t2659_gp_1_0_0_0 = { 2, GenInst_UnityEvent_2_t2659_gp_0_0_0_0_UnityEvent_2_t2659_gp_1_0_0_0_Types };
extern const Il2CppType UnityEvent_3_t2660_gp_0_0_0_0;
extern const Il2CppType UnityEvent_3_t2660_gp_1_0_0_0;
extern const Il2CppType UnityEvent_3_t2660_gp_2_0_0_0;
static const Il2CppType* GenInst_UnityEvent_3_t2660_gp_0_0_0_0_UnityEvent_3_t2660_gp_1_0_0_0_UnityEvent_3_t2660_gp_2_0_0_0_Types[] = { &UnityEvent_3_t2660_gp_0_0_0_0, &UnityEvent_3_t2660_gp_1_0_0_0, &UnityEvent_3_t2660_gp_2_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_3_t2660_gp_0_0_0_0_UnityEvent_3_t2660_gp_1_0_0_0_UnityEvent_3_t2660_gp_2_0_0_0 = { 3, GenInst_UnityEvent_3_t2660_gp_0_0_0_0_UnityEvent_3_t2660_gp_1_0_0_0_UnityEvent_3_t2660_gp_2_0_0_0_Types };
extern const Il2CppType UnityEvent_4_t2661_gp_0_0_0_0;
extern const Il2CppType UnityEvent_4_t2661_gp_1_0_0_0;
extern const Il2CppType UnityEvent_4_t2661_gp_2_0_0_0;
extern const Il2CppType UnityEvent_4_t2661_gp_3_0_0_0;
static const Il2CppType* GenInst_UnityEvent_4_t2661_gp_0_0_0_0_UnityEvent_4_t2661_gp_1_0_0_0_UnityEvent_4_t2661_gp_2_0_0_0_UnityEvent_4_t2661_gp_3_0_0_0_Types[] = { &UnityEvent_4_t2661_gp_0_0_0_0, &UnityEvent_4_t2661_gp_1_0_0_0, &UnityEvent_4_t2661_gp_2_0_0_0, &UnityEvent_4_t2661_gp_3_0_0_0 };
extern const Il2CppGenericInst GenInst_UnityEvent_4_t2661_gp_0_0_0_0_UnityEvent_4_t2661_gp_1_0_0_0_UnityEvent_4_t2661_gp_2_0_0_0_UnityEvent_4_t2661_gp_3_0_0_0 = { 4, GenInst_UnityEvent_4_t2661_gp_0_0_0_0_UnityEvent_4_t2661_gp_1_0_0_0_UnityEvent_4_t2661_gp_2_0_0_0_UnityEvent_4_t2661_gp_3_0_0_0_Types };
extern const Il2CppType Enumerable_Where_m19596_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_Where_m19596_gp_0_0_0_0_Types[] = { &Enumerable_Where_m19596_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m19596_gp_0_0_0_0 = { 1, GenInst_Enumerable_Where_m19596_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_Where_m19596_gp_0_0_0_0_Boolean_t360_0_0_0_Types[] = { &Enumerable_Where_m19596_gp_0_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_Where_m19596_gp_0_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_Enumerable_Where_m19596_gp_0_0_0_0_Boolean_t360_0_0_0_Types };
extern const Il2CppType Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0 = { 1, GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0_Boolean_t360_0_0_0_Types[] = { &Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0_Boolean_t360_0_0_0_Types };
extern const Il2CppType U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0 = { 1, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0_Boolean_t360_0_0_0_Types[] = { &U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0, &Boolean_t360_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0_Boolean_t360_0_0_0 = { 2, GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0_Boolean_t360_0_0_0_Types };
extern const Il2CppType Stack_1_t2669_gp_0_0_0_0;
static const Il2CppType* GenInst_Stack_1_t2669_gp_0_0_0_0_Types[] = { &Stack_1_t2669_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Stack_1_t2669_gp_0_0_0_0 = { 1, GenInst_Stack_1_t2669_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2670_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2670_gp_0_0_0_0_Types[] = { &Enumerator_t2670_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2670_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2670_gp_0_0_0_0_Types };
extern const Il2CppType IEnumerable_1_t2676_gp_0_0_0_0;
static const Il2CppType* GenInst_IEnumerable_1_t2676_gp_0_0_0_0_Types[] = { &IEnumerable_1_t2676_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IEnumerable_1_t2676_gp_0_0_0_0 = { 1, GenInst_IEnumerable_1_t2676_gp_0_0_0_0_Types };
extern const Il2CppType Array_InternalArray__IEnumerable_GetEnumerator_m19719_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19719_gp_0_0_0_0_Types[] = { &Array_InternalArray__IEnumerable_GetEnumerator_m19719_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19719_gp_0_0_0_0 = { 1, GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19719_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19731_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19731_gp_0_0_0_0_Array_Sort_m19731_gp_0_0_0_0_Types[] = { &Array_Sort_m19731_gp_0_0_0_0, &Array_Sort_m19731_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19731_gp_0_0_0_0_Array_Sort_m19731_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19731_gp_0_0_0_0_Array_Sort_m19731_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19732_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m19732_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19732_gp_0_0_0_0_Array_Sort_m19732_gp_1_0_0_0_Types[] = { &Array_Sort_m19732_gp_0_0_0_0, &Array_Sort_m19732_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19732_gp_0_0_0_0_Array_Sort_m19732_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19732_gp_0_0_0_0_Array_Sort_m19732_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19733_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19733_gp_0_0_0_0_Types[] = { &Array_Sort_m19733_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19733_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19733_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m19733_gp_0_0_0_0_Array_Sort_m19733_gp_0_0_0_0_Types[] = { &Array_Sort_m19733_gp_0_0_0_0, &Array_Sort_m19733_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19733_gp_0_0_0_0_Array_Sort_m19733_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19733_gp_0_0_0_0_Array_Sort_m19733_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19734_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19734_gp_0_0_0_0_Types[] = { &Array_Sort_m19734_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19734_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19734_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19734_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19734_gp_0_0_0_0_Array_Sort_m19734_gp_1_0_0_0_Types[] = { &Array_Sort_m19734_gp_0_0_0_0, &Array_Sort_m19734_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19734_gp_0_0_0_0_Array_Sort_m19734_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19734_gp_0_0_0_0_Array_Sort_m19734_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19735_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19735_gp_0_0_0_0_Array_Sort_m19735_gp_0_0_0_0_Types[] = { &Array_Sort_m19735_gp_0_0_0_0, &Array_Sort_m19735_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19735_gp_0_0_0_0_Array_Sort_m19735_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19735_gp_0_0_0_0_Array_Sort_m19735_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19736_gp_0_0_0_0;
extern const Il2CppType Array_Sort_m19736_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19736_gp_0_0_0_0_Array_Sort_m19736_gp_1_0_0_0_Types[] = { &Array_Sort_m19736_gp_0_0_0_0, &Array_Sort_m19736_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19736_gp_0_0_0_0_Array_Sort_m19736_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19736_gp_0_0_0_0_Array_Sort_m19736_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19737_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19737_gp_0_0_0_0_Types[] = { &Array_Sort_m19737_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19737_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19737_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m19737_gp_0_0_0_0_Array_Sort_m19737_gp_0_0_0_0_Types[] = { &Array_Sort_m19737_gp_0_0_0_0, &Array_Sort_m19737_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19737_gp_0_0_0_0_Array_Sort_m19737_gp_0_0_0_0 = { 2, GenInst_Array_Sort_m19737_gp_0_0_0_0_Array_Sort_m19737_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19738_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19738_gp_0_0_0_0_Types[] = { &Array_Sort_m19738_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19738_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19738_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19738_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19738_gp_1_0_0_0_Types[] = { &Array_Sort_m19738_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19738_gp_1_0_0_0 = { 1, GenInst_Array_Sort_m19738_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Array_Sort_m19738_gp_0_0_0_0_Array_Sort_m19738_gp_1_0_0_0_Types[] = { &Array_Sort_m19738_gp_0_0_0_0, &Array_Sort_m19738_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19738_gp_0_0_0_0_Array_Sort_m19738_gp_1_0_0_0 = { 2, GenInst_Array_Sort_m19738_gp_0_0_0_0_Array_Sort_m19738_gp_1_0_0_0_Types };
extern const Il2CppType Array_Sort_m19739_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19739_gp_0_0_0_0_Types[] = { &Array_Sort_m19739_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19739_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19739_gp_0_0_0_0_Types };
extern const Il2CppType Array_Sort_m19740_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Sort_m19740_gp_0_0_0_0_Types[] = { &Array_Sort_m19740_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Sort_m19740_gp_0_0_0_0 = { 1, GenInst_Array_Sort_m19740_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m19741_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m19741_gp_0_0_0_0_Types[] = { &Array_qsort_m19741_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m19741_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m19741_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m19741_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m19741_gp_0_0_0_0_Array_qsort_m19741_gp_1_0_0_0_Types[] = { &Array_qsort_m19741_gp_0_0_0_0, &Array_qsort_m19741_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m19741_gp_0_0_0_0_Array_qsort_m19741_gp_1_0_0_0 = { 2, GenInst_Array_qsort_m19741_gp_0_0_0_0_Array_qsort_m19741_gp_1_0_0_0_Types };
extern const Il2CppType Array_compare_m19742_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_compare_m19742_gp_0_0_0_0_Types[] = { &Array_compare_m19742_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_compare_m19742_gp_0_0_0_0 = { 1, GenInst_Array_compare_m19742_gp_0_0_0_0_Types };
extern const Il2CppType Array_qsort_m19743_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_qsort_m19743_gp_0_0_0_0_Types[] = { &Array_qsort_m19743_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_qsort_m19743_gp_0_0_0_0 = { 1, GenInst_Array_qsort_m19743_gp_0_0_0_0_Types };
extern const Il2CppType Array_Resize_m19746_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Resize_m19746_gp_0_0_0_0_Types[] = { &Array_Resize_m19746_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Resize_m19746_gp_0_0_0_0 = { 1, GenInst_Array_Resize_m19746_gp_0_0_0_0_Types };
extern const Il2CppType Array_TrueForAll_m19748_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_TrueForAll_m19748_gp_0_0_0_0_Types[] = { &Array_TrueForAll_m19748_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_TrueForAll_m19748_gp_0_0_0_0 = { 1, GenInst_Array_TrueForAll_m19748_gp_0_0_0_0_Types };
extern const Il2CppType Array_ForEach_m19749_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_ForEach_m19749_gp_0_0_0_0_Types[] = { &Array_ForEach_m19749_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ForEach_m19749_gp_0_0_0_0 = { 1, GenInst_Array_ForEach_m19749_gp_0_0_0_0_Types };
extern const Il2CppType Array_ConvertAll_m19750_gp_0_0_0_0;
extern const Il2CppType Array_ConvertAll_m19750_gp_1_0_0_0;
static const Il2CppType* GenInst_Array_ConvertAll_m19750_gp_0_0_0_0_Array_ConvertAll_m19750_gp_1_0_0_0_Types[] = { &Array_ConvertAll_m19750_gp_0_0_0_0, &Array_ConvertAll_m19750_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_ConvertAll_m19750_gp_0_0_0_0_Array_ConvertAll_m19750_gp_1_0_0_0 = { 2, GenInst_Array_ConvertAll_m19750_gp_0_0_0_0_Array_ConvertAll_m19750_gp_1_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m19751_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m19751_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m19751_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m19751_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m19751_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m19752_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m19752_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m19752_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m19752_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m19752_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLastIndex_m19753_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLastIndex_m19753_gp_0_0_0_0_Types[] = { &Array_FindLastIndex_m19753_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLastIndex_m19753_gp_0_0_0_0 = { 1, GenInst_Array_FindLastIndex_m19753_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m19754_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m19754_gp_0_0_0_0_Types[] = { &Array_FindIndex_m19754_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m19754_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m19754_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m19755_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m19755_gp_0_0_0_0_Types[] = { &Array_FindIndex_m19755_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m19755_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m19755_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindIndex_m19756_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindIndex_m19756_gp_0_0_0_0_Types[] = { &Array_FindIndex_m19756_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindIndex_m19756_gp_0_0_0_0 = { 1, GenInst_Array_FindIndex_m19756_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19757_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19757_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19757_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19757_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19757_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19758_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19758_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19758_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19758_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19758_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19759_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19759_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19759_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19759_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19759_gp_0_0_0_0_Types };
extern const Il2CppType Array_BinarySearch_m19760_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_BinarySearch_m19760_gp_0_0_0_0_Types[] = { &Array_BinarySearch_m19760_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_BinarySearch_m19760_gp_0_0_0_0 = { 1, GenInst_Array_BinarySearch_m19760_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m19761_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m19761_gp_0_0_0_0_Types[] = { &Array_IndexOf_m19761_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m19761_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m19761_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m19762_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m19762_gp_0_0_0_0_Types[] = { &Array_IndexOf_m19762_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m19762_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m19762_gp_0_0_0_0_Types };
extern const Il2CppType Array_IndexOf_m19763_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_IndexOf_m19763_gp_0_0_0_0_Types[] = { &Array_IndexOf_m19763_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_IndexOf_m19763_gp_0_0_0_0 = { 1, GenInst_Array_IndexOf_m19763_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m19764_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m19764_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m19764_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m19764_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m19764_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m19765_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m19765_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m19765_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m19765_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m19765_gp_0_0_0_0_Types };
extern const Il2CppType Array_LastIndexOf_m19766_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_LastIndexOf_m19766_gp_0_0_0_0_Types[] = { &Array_LastIndexOf_m19766_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_LastIndexOf_m19766_gp_0_0_0_0 = { 1, GenInst_Array_LastIndexOf_m19766_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindAll_m19767_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindAll_m19767_gp_0_0_0_0_Types[] = { &Array_FindAll_m19767_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindAll_m19767_gp_0_0_0_0 = { 1, GenInst_Array_FindAll_m19767_gp_0_0_0_0_Types };
extern const Il2CppType Array_Exists_m19768_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Exists_m19768_gp_0_0_0_0_Types[] = { &Array_Exists_m19768_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Exists_m19768_gp_0_0_0_0 = { 1, GenInst_Array_Exists_m19768_gp_0_0_0_0_Types };
extern const Il2CppType Array_AsReadOnly_m19769_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_AsReadOnly_m19769_gp_0_0_0_0_Types[] = { &Array_AsReadOnly_m19769_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_AsReadOnly_m19769_gp_0_0_0_0 = { 1, GenInst_Array_AsReadOnly_m19769_gp_0_0_0_0_Types };
extern const Il2CppType Array_Find_m19770_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_Find_m19770_gp_0_0_0_0_Types[] = { &Array_Find_m19770_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_Find_m19770_gp_0_0_0_0 = { 1, GenInst_Array_Find_m19770_gp_0_0_0_0_Types };
extern const Il2CppType Array_FindLast_m19771_gp_0_0_0_0;
static const Il2CppType* GenInst_Array_FindLast_m19771_gp_0_0_0_0_Types[] = { &Array_FindLast_m19771_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Array_FindLast_m19771_gp_0_0_0_0 = { 1, GenInst_Array_FindLast_m19771_gp_0_0_0_0_Types };
extern const Il2CppType InternalEnumerator_1_t2677_gp_0_0_0_0;
static const Il2CppType* GenInst_InternalEnumerator_1_t2677_gp_0_0_0_0_Types[] = { &InternalEnumerator_1_t2677_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_InternalEnumerator_1_t2677_gp_0_0_0_0 = { 1, GenInst_InternalEnumerator_1_t2677_gp_0_0_0_0_Types };
extern const Il2CppType ArrayReadOnlyList_1_t2678_gp_0_0_0_0;
static const Il2CppType* GenInst_ArrayReadOnlyList_1_t2678_gp_0_0_0_0_Types[] = { &ArrayReadOnlyList_1_t2678_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ArrayReadOnlyList_1_t2678_gp_0_0_0_0 = { 1, GenInst_ArrayReadOnlyList_1_t2678_gp_0_0_0_0_Types };
extern const Il2CppType U3CGetEnumeratorU3Ec__Iterator0_t2679_gp_0_0_0_0;
static const Il2CppType* GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2679_gp_0_0_0_0_Types[] = { &U3CGetEnumeratorU3Ec__Iterator0_t2679_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2679_gp_0_0_0_0 = { 1, GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2679_gp_0_0_0_0_Types };
extern const Il2CppType IList_1_t2680_gp_0_0_0_0;
static const Il2CppType* GenInst_IList_1_t2680_gp_0_0_0_0_Types[] = { &IList_1_t2680_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_IList_1_t2680_gp_0_0_0_0 = { 1, GenInst_IList_1_t2680_gp_0_0_0_0_Types };
extern const Il2CppType ICollection_1_t2681_gp_0_0_0_0;
static const Il2CppType* GenInst_ICollection_1_t2681_gp_0_0_0_0_Types[] = { &ICollection_1_t2681_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ICollection_1_t2681_gp_0_0_0_0 = { 1, GenInst_ICollection_1_t2681_gp_0_0_0_0_Types };
extern const Il2CppType Nullable_1_t1765_gp_0_0_0_0;
static const Il2CppType* GenInst_Nullable_1_t1765_gp_0_0_0_0_Types[] = { &Nullable_1_t1765_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Nullable_1_t1765_gp_0_0_0_0 = { 1, GenInst_Nullable_1_t1765_gp_0_0_0_0_Types };
extern const Il2CppType Comparer_1_t2688_gp_0_0_0_0;
static const Il2CppType* GenInst_Comparer_1_t2688_gp_0_0_0_0_Types[] = { &Comparer_1_t2688_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Comparer_1_t2688_gp_0_0_0_0 = { 1, GenInst_Comparer_1_t2688_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2689_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2689_gp_0_0_0_0_Types[] = { &DefaultComparer_t2689_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2689_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2689_gp_0_0_0_0_Types };
extern const Il2CppType GenericComparer_1_t2637_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericComparer_1_t2637_gp_0_0_0_0_Types[] = { &GenericComparer_1_t2637_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericComparer_1_t2637_gp_0_0_0_0 = { 1, GenInst_GenericComparer_1_t2637_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2690_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_0_0_0_0_Types[] = { &Dictionary_2_t2690_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_0_0_0_0 = { 1, GenInst_Dictionary_2_t2690_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_t2690_gp_1_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Types[] = { &Dictionary_2_t2690_gp_0_0_0_0, &Dictionary_2_t2690_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0 = { 2, GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3782_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3782_0_0_0_Types[] = { &KeyValuePair_2_t3782_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3782_0_0_0 = { 1, GenInst_KeyValuePair_2_t3782_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_CopyTo_m19920_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19920_gp_0_0_0_0_Types[] = { &Dictionary_2_t2690_gp_0_0_0_0, &Dictionary_2_t2690_gp_1_0_0_0, &Dictionary_2_Do_CopyTo_m19920_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19920_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19920_gp_0_0_0_0_Types };
extern const Il2CppType Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0;
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0_Types[] = { &Dictionary_2_t2690_gp_0_0_0_0, &Dictionary_2_t2690_gp_1_0_0_0, &Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0 = { 3, GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0_Object_t_0_0_0_Types[] = { &Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0, &Object_t_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0_Object_t_0_0_0 = { 2, GenInst_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0_Object_t_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_DictionaryEntry_t1057_0_0_0_Types[] = { &Dictionary_2_t2690_gp_0_0_0_0, &Dictionary_2_t2690_gp_1_0_0_0, &DictionaryEntry_t1057_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_DictionaryEntry_t1057_0_0_0 = { 3, GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_DictionaryEntry_t1057_0_0_0_Types };
extern const Il2CppType ShimEnumerator_t2691_gp_0_0_0_0;
extern const Il2CppType ShimEnumerator_t2691_gp_1_0_0_0;
static const Il2CppType* GenInst_ShimEnumerator_t2691_gp_0_0_0_0_ShimEnumerator_t2691_gp_1_0_0_0_Types[] = { &ShimEnumerator_t2691_gp_0_0_0_0, &ShimEnumerator_t2691_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ShimEnumerator_t2691_gp_0_0_0_0_ShimEnumerator_t2691_gp_1_0_0_0 = { 2, GenInst_ShimEnumerator_t2691_gp_0_0_0_0_ShimEnumerator_t2691_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2692_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2692_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2692_gp_0_0_0_0_Enumerator_t2692_gp_1_0_0_0_Types[] = { &Enumerator_t2692_gp_0_0_0_0, &Enumerator_t2692_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2692_gp_0_0_0_0_Enumerator_t2692_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2692_gp_0_0_0_0_Enumerator_t2692_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3795_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3795_0_0_0_Types[] = { &KeyValuePair_2_t3795_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3795_0_0_0 = { 1, GenInst_KeyValuePair_2_t3795_0_0_0_Types };
extern const Il2CppType ValueCollection_t2693_gp_0_0_0_0;
extern const Il2CppType ValueCollection_t2693_gp_1_0_0_0;
static const Il2CppType* GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0_Types[] = { &ValueCollection_t2693_gp_0_0_0_0, &ValueCollection_t2693_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2693_gp_1_0_0_0_Types[] = { &ValueCollection_t2693_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2693_gp_1_0_0_0 = { 1, GenInst_ValueCollection_t2693_gp_1_0_0_0_Types };
extern const Il2CppType Enumerator_t2694_gp_0_0_0_0;
extern const Il2CppType Enumerator_t2694_gp_1_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2694_gp_0_0_0_0_Enumerator_t2694_gp_1_0_0_0_Types[] = { &Enumerator_t2694_gp_0_0_0_0, &Enumerator_t2694_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2694_gp_0_0_0_0_Enumerator_t2694_gp_1_0_0_0 = { 2, GenInst_Enumerator_t2694_gp_0_0_0_0_Enumerator_t2694_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Enumerator_t2694_gp_1_0_0_0_Types[] = { &Enumerator_t2694_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2694_gp_1_0_0_0 = { 1, GenInst_Enumerator_t2694_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0_Types[] = { &ValueCollection_t2693_gp_0_0_0_0, &ValueCollection_t2693_gp_1_0_0_0, &ValueCollection_t2693_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0 = { 3, GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0_Types[] = { &ValueCollection_t2693_gp_1_0_0_0, &ValueCollection_t2693_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0 = { 2, GenInst_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_KeyValuePair_2_t3782_0_0_0_Types[] = { &Dictionary_2_t2690_gp_0_0_0_0, &Dictionary_2_t2690_gp_1_0_0_0, &KeyValuePair_2_t3782_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_KeyValuePair_2_t3782_0_0_0 = { 3, GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_KeyValuePair_2_t3782_0_0_0_Types };
static const Il2CppType* GenInst_KeyValuePair_2_t3782_0_0_0_KeyValuePair_2_t3782_0_0_0_Types[] = { &KeyValuePair_2_t3782_0_0_0, &KeyValuePair_2_t3782_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3782_0_0_0_KeyValuePair_2_t3782_0_0_0 = { 2, GenInst_KeyValuePair_2_t3782_0_0_0_KeyValuePair_2_t3782_0_0_0_Types };
static const Il2CppType* GenInst_Dictionary_2_t2690_gp_1_0_0_0_Types[] = { &Dictionary_2_t2690_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_Dictionary_2_t2690_gp_1_0_0_0 = { 1, GenInst_Dictionary_2_t2690_gp_1_0_0_0_Types };
extern const Il2CppType EqualityComparer_1_t2696_gp_0_0_0_0;
static const Il2CppType* GenInst_EqualityComparer_1_t2696_gp_0_0_0_0_Types[] = { &EqualityComparer_1_t2696_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_EqualityComparer_1_t2696_gp_0_0_0_0 = { 1, GenInst_EqualityComparer_1_t2696_gp_0_0_0_0_Types };
extern const Il2CppType DefaultComparer_t2697_gp_0_0_0_0;
static const Il2CppType* GenInst_DefaultComparer_t2697_gp_0_0_0_0_Types[] = { &DefaultComparer_t2697_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_DefaultComparer_t2697_gp_0_0_0_0 = { 1, GenInst_DefaultComparer_t2697_gp_0_0_0_0_Types };
extern const Il2CppType GenericEqualityComparer_1_t2636_gp_0_0_0_0;
static const Il2CppType* GenInst_GenericEqualityComparer_1_t2636_gp_0_0_0_0_Types[] = { &GenericEqualityComparer_1_t2636_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_GenericEqualityComparer_1_t2636_gp_0_0_0_0 = { 1, GenInst_GenericEqualityComparer_1_t2636_gp_0_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t3822_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t3822_0_0_0_Types[] = { &KeyValuePair_2_t3822_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t3822_0_0_0 = { 1, GenInst_KeyValuePair_2_t3822_0_0_0_Types };
extern const Il2CppType IDictionary_2_t2699_gp_0_0_0_0;
extern const Il2CppType IDictionary_2_t2699_gp_1_0_0_0;
static const Il2CppType* GenInst_IDictionary_2_t2699_gp_0_0_0_0_IDictionary_2_t2699_gp_1_0_0_0_Types[] = { &IDictionary_2_t2699_gp_0_0_0_0, &IDictionary_2_t2699_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_IDictionary_2_t2699_gp_0_0_0_0_IDictionary_2_t2699_gp_1_0_0_0 = { 2, GenInst_IDictionary_2_t2699_gp_0_0_0_0_IDictionary_2_t2699_gp_1_0_0_0_Types };
extern const Il2CppType KeyValuePair_2_t2701_gp_0_0_0_0;
extern const Il2CppType KeyValuePair_2_t2701_gp_1_0_0_0;
static const Il2CppType* GenInst_KeyValuePair_2_t2701_gp_0_0_0_0_KeyValuePair_2_t2701_gp_1_0_0_0_Types[] = { &KeyValuePair_2_t2701_gp_0_0_0_0, &KeyValuePair_2_t2701_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_KeyValuePair_2_t2701_gp_0_0_0_0_KeyValuePair_2_t2701_gp_1_0_0_0 = { 2, GenInst_KeyValuePair_2_t2701_gp_0_0_0_0_KeyValuePair_2_t2701_gp_1_0_0_0_Types };
extern const Il2CppType List_1_t2702_gp_0_0_0_0;
static const Il2CppType* GenInst_List_1_t2702_gp_0_0_0_0_Types[] = { &List_1_t2702_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_List_1_t2702_gp_0_0_0_0 = { 1, GenInst_List_1_t2702_gp_0_0_0_0_Types };
extern const Il2CppType Enumerator_t2703_gp_0_0_0_0;
static const Il2CppType* GenInst_Enumerator_t2703_gp_0_0_0_0_Types[] = { &Enumerator_t2703_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Enumerator_t2703_gp_0_0_0_0 = { 1, GenInst_Enumerator_t2703_gp_0_0_0_0_Types };
extern const Il2CppType Collection_1_t2704_gp_0_0_0_0;
static const Il2CppType* GenInst_Collection_1_t2704_gp_0_0_0_0_Types[] = { &Collection_1_t2704_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_Collection_1_t2704_gp_0_0_0_0 = { 1, GenInst_Collection_1_t2704_gp_0_0_0_0_Types };
extern const Il2CppType ReadOnlyCollection_1_t2705_gp_0_0_0_0;
static const Il2CppType* GenInst_ReadOnlyCollection_1_t2705_gp_0_0_0_0_Types[] = { &ReadOnlyCollection_1_t2705_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_ReadOnlyCollection_1_t2705_gp_0_0_0_0 = { 1, GenInst_ReadOnlyCollection_1_t2705_gp_0_0_0_0_Types };
extern const Il2CppType MonoProperty_GetterAdapterFrame_m20180_gp_0_0_0_0;
extern const Il2CppType MonoProperty_GetterAdapterFrame_m20180_gp_1_0_0_0;
static const Il2CppType* GenInst_MonoProperty_GetterAdapterFrame_m20180_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20180_gp_1_0_0_0_Types[] = { &MonoProperty_GetterAdapterFrame_m20180_gp_0_0_0_0, &MonoProperty_GetterAdapterFrame_m20180_gp_1_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_GetterAdapterFrame_m20180_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20180_gp_1_0_0_0 = { 2, GenInst_MonoProperty_GetterAdapterFrame_m20180_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20180_gp_1_0_0_0_Types };
extern const Il2CppType MonoProperty_StaticGetterAdapterFrame_m20181_gp_0_0_0_0;
static const Il2CppType* GenInst_MonoProperty_StaticGetterAdapterFrame_m20181_gp_0_0_0_0_Types[] = { &MonoProperty_StaticGetterAdapterFrame_m20181_gp_0_0_0_0 };
extern const Il2CppGenericInst GenInst_MonoProperty_StaticGetterAdapterFrame_m20181_gp_0_0_0_0 = { 1, GenInst_MonoProperty_StaticGetterAdapterFrame_m20181_gp_0_0_0_0_Types };
extern const Il2CppGenericInst* g_Il2CppGenericInstTable[522] = 
{
	&GenInst_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_EventSystem_t65_0_0_0,
	&GenInst_StandaloneInputModule_t66_0_0_0,
	&GenInst_TouchInputModule_t67_0_0_0,
	&GenInst_Image_t24_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0,
	&GenInst_String_t_0_0_0,
	&GenInst_Renderer_t30_0_0_0,
	&GenInst_CarController_t29_0_0_0,
	&GenInst_Rigidbody_t34_0_0_0,
	&GenInst_CarAIControl_t32_0_0_0,
	&GenInst_AudioSource_t38_0_0_0,
	&GenInst_Transform_t33_0_0_0,
	&GenInst_ParticleSystem_t55_0_0_0,
	&GenInst_WheelCollider_t56_0_0_0,
	&GenInst_Camera_t74_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t359_0_0_0,
	&GenInst_BaseInputModule_t106_0_0_0,
	&GenInst_RaycastResult_t139_0_0_0,
	&GenInst_IDeselectHandler_t337_0_0_0,
	&GenInst_ISelectHandler_t336_0_0_0,
	&GenInst_BaseEventData_t107_0_0_0,
	&GenInst_Entry_t111_0_0_0,
	&GenInst_IPointerEnterHandler_t324_0_0_0,
	&GenInst_IPointerExitHandler_t325_0_0_0,
	&GenInst_IPointerDownHandler_t326_0_0_0,
	&GenInst_IPointerUpHandler_t327_0_0_0,
	&GenInst_IPointerClickHandler_t328_0_0_0,
	&GenInst_IInitializePotentialDragHandler_t329_0_0_0,
	&GenInst_IBeginDragHandler_t330_0_0_0,
	&GenInst_IDragHandler_t331_0_0_0,
	&GenInst_IEndDragHandler_t332_0_0_0,
	&GenInst_IDropHandler_t333_0_0_0,
	&GenInst_IScrollHandler_t334_0_0_0,
	&GenInst_IUpdateSelectedHandler_t335_0_0_0,
	&GenInst_IMoveHandler_t338_0_0_0,
	&GenInst_ISubmitHandler_t339_0_0_0,
	&GenInst_ICancelHandler_t340_0_0_0,
	&GenInst_List_1_t323_0_0_0,
	&GenInst_IEventSystemHandler_t1889_0_0_0,
	&GenInst_PointerEventData_t57_0_0_0,
	&GenInst_AxisEventData_t141_0_0_0,
	&GenInst_BaseRaycaster_t140_0_0_0,
	&GenInst_GameObject_t52_0_0_0,
	&GenInst_ButtonState_t146_0_0_0,
	&GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0,
	&GenInst_SpriteRenderer_t367_0_0_0,
	&GenInst_RaycastHit_t100_0_0_0,
	&GenInst_Color_t83_0_0_0,
	&GenInst_Single_t358_0_0_0,
	&GenInst_ICanvasElement_t344_0_0_0,
	&GenInst_RectTransform_t95_0_0_0,
	&GenInst_Button_t168_0_0_0,
	&GenInst_Text_t97_0_0_0,
	&GenInst_RawImage_t248_0_0_0,
	&GenInst_Slider_t93_0_0_0,
	&GenInst_Scrollbar_t257_0_0_0,
	&GenInst_Toggle_t179_0_0_0,
	&GenInst_InputField_t231_0_0_0,
	&GenInst_ScrollRect_t263_0_0_0,
	&GenInst_Mask_t240_0_0_0,
	&GenInst_Dropdown_t186_0_0_0,
	&GenInst_OptionData_t180_0_0_0,
	&GenInst_Int32_t359_0_0_0,
	&GenInst_DropdownItem_t178_0_0_0,
	&GenInst_FloatTween_t163_0_0_0,
	&GenInst_Canvas_t197_0_0_0,
	&GenInst_GraphicRaycaster_t203_0_0_0,
	&GenInst_CanvasGroup_t373_0_0_0,
	&GenInst_Boolean_t360_0_0_0,
	&GenInst_Font_t191_0_0_0_List_1_t378_0_0_0,
	&GenInst_Font_t191_0_0_0,
	&GenInst_ColorTween_t160_0_0_0,
	&GenInst_CanvasRenderer_t196_0_0_0,
	&GenInst_Component_t78_0_0_0,
	&GenInst_Graphic_t194_0_0_0,
	&GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0,
	&GenInst_Sprite_t176_0_0_0,
	&GenInst_Type_t208_0_0_0,
	&GenInst_FillMethod_t209_0_0_0,
	&GenInst_SubmitEvent_t222_0_0_0,
	&GenInst_OnChangeEvent_t224_0_0_0,
	&GenInst_OnValidateInput_t226_0_0_0,
	&GenInst_ContentType_t218_0_0_0,
	&GenInst_LineType_t221_0_0_0,
	&GenInst_InputType_t219_0_0_0,
	&GenInst_TouchScreenKeyboardType_t391_0_0_0,
	&GenInst_CharacterValidation_t220_0_0_0,
	&GenInst_Char_t390_0_0_0,
	&GenInst_UILineInfo_t394_0_0_0,
	&GenInst_UICharInfo_t396_0_0_0,
	&GenInst_LayoutElement_t301_0_0_0,
	&GenInst_IClippable_t349_0_0_0,
	&GenInst_RectMask2D_t243_0_0_0,
	&GenInst_Direction_t253_0_0_0,
	&GenInst_Vector2_t23_0_0_0,
	&GenInst_Selectable_t169_0_0_0,
	&GenInst_Navigation_t247_0_0_0,
	&GenInst_Transition_t265_0_0_0,
	&GenInst_ColorBlock_t174_0_0_0,
	&GenInst_SpriteState_t267_0_0_0,
	&GenInst_AnimationTriggers_t164_0_0_0,
	&GenInst_Animator_t350_0_0_0,
	&GenInst_Direction_t271_0_0_0,
	&GenInst_MatEntry_t274_0_0_0,
	&GenInst_UIVertex_t239_0_0_0,
	&GenInst_Toggle_t179_0_0_0_Boolean_t360_0_0_0,
	&GenInst_IClipper_t353_0_0_0,
	&GenInst_AspectMode_t286_0_0_0,
	&GenInst_FitMode_t292_0_0_0,
	&GenInst_Corner_t294_0_0_0,
	&GenInst_Axis_t295_0_0_0,
	&GenInst_Constraint_t296_0_0_0,
	&GenInst_RectOffset_t302_0_0_0,
	&GenInst_TextAnchor_t412_0_0_0,
	&GenInst_LayoutRebuilder_t304_0_0_0,
	&GenInst_ILayoutElement_t355_0_0_0_Single_t358_0_0_0,
	&GenInst_Vector3_t12_0_0_0,
	&GenInst_Color32_t347_0_0_0,
	&GenInst_Vector4_t317_0_0_0,
	&GenInst_GcLeaderboard_t453_0_0_0,
	&GenInst_IAchievementDescriptionU5BU5D_t657_0_0_0,
	&GenInst_IAchievementU5BU5D_t659_0_0_0,
	&GenInst_IScoreU5BU5D_t593_0_0_0,
	&GenInst_IUserProfileU5BU5D_t589_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t360_0_0_0,
	&GenInst_Rigidbody2D_t509_0_0_0,
	&GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0,
	&GenInst_GUILayoutEntry_t554_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0,
	&GenInst_Type_t_0_0_0,
	&GenInst_GUILayer_t460_0_0_0,
	&GenInst_PersistentCall_t623_0_0_0,
	&GenInst_BaseInvokableCall_t620_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1326_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1325_0_0_0,
	&GenInst_StrongName_t1576_0_0_0,
	&GenInst_DateTime_t546_0_0_0,
	&GenInst_DateTimeOffset_t1651_0_0_0,
	&GenInst_TimeSpan_t969_0_0_0,
	&GenInst_Guid_t1673_0_0_0,
	&GenInst_CustomAttributeData_t1322_0_0_0,
	&GenInst_AxisTouchButton_t1_0_0_0,
	&GenInst_MonoBehaviour_t2_0_0_0,
	&GenInst_Behaviour_t418_0_0_0,
	&GenInst_Object_t62_0_0_0,
	&GenInst_KeyValuePair_2_t1817_0_0_0,
	&GenInst_ValueType_t1071_0_0_0,
	&GenInst_IConvertible_t1764_0_0_0,
	&GenInst_IComparable_t1763_0_0_0,
	&GenInst_IEnumerable_t878_0_0_0,
	&GenInst_ICloneable_t1774_0_0_0,
	&GenInst_IComparable_1_t2905_0_0_0,
	&GenInst_IEquatable_1_t2910_0_0_0,
	&GenInst_IFormattable_t1761_0_0_0,
	&GenInst_IComparable_1_t2920_0_0_0,
	&GenInst_IEquatable_1_t2925_0_0_0,
	&GenInst_Link_t1181_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_DictionaryEntry_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1817_0_0_0,
	&GenInst_IReflect_t2684_0_0_0,
	&GenInst__Type_t2682_0_0_0,
	&GenInst_MemberInfo_t_0_0_0,
	&GenInst_ICustomAttributeProvider_t1759_0_0_0,
	&GenInst__MemberInfo_t2683_0_0_0,
	&GenInst_String_t_0_0_0_VirtualAxis_t3_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t1832_0_0_0,
	&GenInst_String_t_0_0_0_VirtualButton_t6_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t1837_0_0_0,
	&GenInst_Touch_t72_0_0_0,
	&GenInst_Double_t675_0_0_0,
	&GenInst_IComparable_1_t2969_0_0_0,
	&GenInst_IEquatable_1_t2974_0_0_0,
	&GenInst_IComparable_1_t2982_0_0_0,
	&GenInst_IEquatable_1_t2987_0_0_0,
	&GenInst_Collider_t98_0_0_0,
	&GenInst_WheelEffects_t54_0_0_0,
	&GenInst_Quaternion_t45_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t359_0_0_0,
	&GenInst_KeyValuePair_2_t1864_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t359_0_0_0_Int32_t359_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_Int32_t359_0_0_0_KeyValuePair_2_t1864_0_0_0,
	&GenInst_String_t_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t1875_0_0_0,
	&GenInst_List_1_t403_0_0_0,
	&GenInst_List_1_t354_0_0_0,
	&GenInst_Int32_t359_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1952_0_0_0,
	&GenInst_Int32_t359_0_0_0_Object_t_0_0_0_Object_t_0_0_0,
	&GenInst_Int32_t359_0_0_0_Object_t_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_Int32_t359_0_0_0_Object_t_0_0_0_KeyValuePair_2_t1952_0_0_0,
	&GenInst_Int32_t359_0_0_0_PointerEventData_t57_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t363_0_0_0,
	&GenInst_RaycastHit2D_t369_0_0_0,
	&GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0,
	&GenInst_ICanvasElement_t344_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_List_1_t374_0_0_0,
	&GenInst_Font_t191_0_0_0_List_1_t378_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2013_0_0_0,
	&GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0,
	&GenInst_Graphic_t194_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_Canvas_t197_0_0_0_IndexedSet_1_t388_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2037_0_0_0,
	&GenInst_KeyValuePair_2_t2040_0_0_0,
	&GenInst_KeyValuePair_2_t2043_0_0_0,
	&GenInst_Enum_t665_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t360_0_0_0,
	&GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0,
	&GenInst_IClipper_t353_0_0_0_Int32_t359_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2096_0_0_0,
	&GenInst_Object_t_0_0_0_Single_t358_0_0_0,
	&GenInst_List_1_t312_0_0_0,
	&GenInst_List_1_t313_0_0_0,
	&GenInst_List_1_t314_0_0_0,
	&GenInst_List_1_t315_0_0_0,
	&GenInst_List_1_t316_0_0_0,
	&GenInst_List_1_t345_0_0_0,
	&GenInst_IAchievementDescription_t2653_0_0_0,
	&GenInst_IAchievement_t641_0_0_0,
	&GenInst_IScore_t596_0_0_0,
	&GenInst_IUserProfile_t2652_0_0_0,
	&GenInst_AchievementDescription_t591_0_0_0,
	&GenInst_UserProfile_t588_0_0_0,
	&GenInst_GcAchievementData_t581_0_0_0,
	&GenInst_Achievement_t590_0_0_0,
	&GenInst_GcScoreData_t582_0_0_0,
	&GenInst_Score_t592_0_0_0,
	&GenInst_Display_t490_0_0_0,
	&GenInst_IntPtr_t_0_0_0,
	&GenInst_ISerializable_t1793_0_0_0,
	&GenInst_ContactPoint_t505_0_0_0,
	&GenInst_ContactPoint2D_t510_0_0_0,
	&GenInst_IComparable_1_t3182_0_0_0,
	&GenInst_IEquatable_1_t3187_0_0_0,
	&GenInst_Keyframe_t524_0_0_0,
	&GenInst_CharacterInfo_t533_0_0_0,
	&GenInst_GUILayoutOption_t558_0_0_0,
	&GenInst_Int32_t359_0_0_0_LayoutCache_t549_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2240_0_0_0,
	&GenInst_GUIStyle_t553_0_0_0,
	&GenInst_String_t_0_0_0_GUIStyle_t553_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2250_0_0_0,
	&GenInst_DisallowMultipleComponent_t572_0_0_0,
	&GenInst_Attribute_t477_0_0_0,
	&GenInst__Attribute_t2672_0_0_0,
	&GenInst_ExecuteInEditMode_t575_0_0_0,
	&GenInst_RequireComponent_t573_0_0_0,
	&GenInst_ParameterModifier_t1345_0_0_0,
	&GenInst_HitInfo_t597_0_0_0,
	&GenInst_ParameterInfo_t681_0_0_0,
	&GenInst__ParameterInfo_t2723_0_0_0,
	&GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0,
	&GenInst_KeyValuePair_2_t2270_0_0_0,
	&GenInst_TextEditOp_t615_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_TextEditOp_t615_0_0_0_KeyValuePair_2_t2270_0_0_0,
	&GenInst_Event_t237_0_0_0,
	&GenInst_Event_t237_0_0_0_TextEditOp_t615_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2281_0_0_0,
	&GenInst_Byte_t664_0_0_0,
	&GenInst_IComparable_1_t3270_0_0_0,
	&GenInst_IEquatable_1_t3275_0_0_0,
	&GenInst_KeySizes_t717_0_0_0,
	&GenInst_UInt32_t677_0_0_0,
	&GenInst_IComparable_1_t3287_0_0_0,
	&GenInst_IEquatable_1_t3292_0_0_0,
	&GenInst_BigInteger_t722_0_0_0,
	&GenInst_ByteU5BU5D_t698_0_0_0,
	&GenInst_Array_t_0_0_0,
	&GenInst_ICollection_t1058_0_0_0,
	&GenInst_IList_t1018_0_0_0,
	&GenInst_X509Certificate_t829_0_0_0,
	&GenInst_IDeserializationCallback_t1796_0_0_0,
	&GenInst_ClientCertificateType_t833_0_0_0,
	&GenInst_UInt16_t1064_0_0_0,
	&GenInst_IComparable_1_t3331_0_0_0,
	&GenInst_IEquatable_1_t3336_0_0_0,
	&GenInst_KeyValuePair_2_t2318_0_0_0,
	&GenInst_IComparable_1_t3348_0_0_0,
	&GenInst_IEquatable_1_t3353_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_Boolean_t360_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_Object_t_0_0_0_Boolean_t360_0_0_0_KeyValuePair_2_t2318_0_0_0,
	&GenInst_String_t_0_0_0_Boolean_t360_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t2330_0_0_0,
	&GenInst_X509ChainStatus_t966_0_0_0,
	&GenInst_Capture_t986_0_0_0,
	&GenInst_Group_t904_0_0_0,
	&GenInst_Mark_t1009_0_0_0,
	&GenInst_UriScheme_t1046_0_0_0,
	&GenInst_Int64_t676_0_0_0,
	&GenInst_UInt64_t1076_0_0_0,
	&GenInst_SByte_t1077_0_0_0,
	&GenInst_Int16_t1078_0_0_0,
	&GenInst_Decimal_t1079_0_0_0,
	&GenInst_Delegate_t384_0_0_0,
	&GenInst_IComparable_1_t3378_0_0_0,
	&GenInst_IEquatable_1_t3379_0_0_0,
	&GenInst_IComparable_1_t3382_0_0_0,
	&GenInst_IEquatable_1_t3383_0_0_0,
	&GenInst_IComparable_1_t3380_0_0_0,
	&GenInst_IEquatable_1_t3381_0_0_0,
	&GenInst_IComparable_1_t3376_0_0_0,
	&GenInst_IEquatable_1_t3377_0_0_0,
	&GenInst_FieldInfo_t_0_0_0,
	&GenInst__FieldInfo_t2719_0_0_0,
	&GenInst_MethodInfo_t_0_0_0,
	&GenInst__MethodInfo_t2721_0_0_0,
	&GenInst_MethodBase_t679_0_0_0,
	&GenInst__MethodBase_t2720_0_0_0,
	&GenInst_ConstructorInfo_t687_0_0_0,
	&GenInst__ConstructorInfo_t2717_0_0_0,
	&GenInst_TableRange_t1114_0_0_0,
	&GenInst_TailoringInfo_t1117_0_0_0,
	&GenInst_Contraction_t1118_0_0_0,
	&GenInst_Level2Map_t1120_0_0_0,
	&GenInst_BigInteger_t1141_0_0_0,
	&GenInst_Slot_t1191_0_0_0,
	&GenInst_Slot_t1199_0_0_0,
	&GenInst_StackFrame_t678_0_0_0,
	&GenInst_Calendar_t1212_0_0_0,
	&GenInst_ModuleBuilder_t1289_0_0_0,
	&GenInst__ModuleBuilder_t2712_0_0_0,
	&GenInst_Module_t1285_0_0_0,
	&GenInst__Module_t2722_0_0_0,
	&GenInst_ParameterBuilder_t1295_0_0_0,
	&GenInst__ParameterBuilder_t2713_0_0_0,
	&GenInst_TypeU5BU5D_t650_0_0_0,
	&GenInst_ILTokenInfo_t1279_0_0_0,
	&GenInst_LabelData_t1281_0_0_0,
	&GenInst_LabelFixup_t1280_0_0_0,
	&GenInst_GenericTypeParameterBuilder_t1277_0_0_0,
	&GenInst_TypeBuilder_t1271_0_0_0,
	&GenInst__TypeBuilder_t2714_0_0_0,
	&GenInst_MethodBuilder_t1278_0_0_0,
	&GenInst__MethodBuilder_t2711_0_0_0,
	&GenInst_ConstructorBuilder_t1269_0_0_0,
	&GenInst__ConstructorBuilder_t2707_0_0_0,
	&GenInst_FieldBuilder_t1275_0_0_0,
	&GenInst__FieldBuilder_t2709_0_0_0,
	&GenInst_PropertyInfo_t_0_0_0,
	&GenInst__PropertyInfo_t2724_0_0_0,
	&GenInst_ResourceInfo_t1356_0_0_0,
	&GenInst_ResourceCacheItem_t1357_0_0_0,
	&GenInst_IContextProperty_t1753_0_0_0,
	&GenInst_Header_t1439_0_0_0,
	&GenInst_ITrackingHandler_t1788_0_0_0,
	&GenInst_IContextAttribute_t1775_0_0_0,
	&GenInst_IComparable_1_t3612_0_0_0,
	&GenInst_IEquatable_1_t3617_0_0_0,
	&GenInst_IComparable_1_t3384_0_0_0,
	&GenInst_IEquatable_1_t3385_0_0_0,
	&GenInst_IComparable_1_t3636_0_0_0,
	&GenInst_IEquatable_1_t3641_0_0_0,
	&GenInst_TypeTag_t1495_0_0_0,
	&GenInst_MonoType_t_0_0_0,
	&GenInst_Version_t937_0_0_0,
	&GenInst_DictionaryEntry_t1057_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_KeyValuePair_2_t1817_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1817_0_0_0_KeyValuePair_2_t1817_0_0_0,
	&GenInst_Int32_t359_0_0_0_Int32_t359_0_0_0,
	&GenInst_KeyValuePair_2_t1864_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1864_0_0_0_KeyValuePair_2_t1864_0_0_0,
	&GenInst_RaycastResult_t139_0_0_0_RaycastResult_t139_0_0_0,
	&GenInst_KeyValuePair_2_t1952_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t1952_0_0_0_KeyValuePair_2_t1952_0_0_0,
	&GenInst_UIVertex_t239_0_0_0_UIVertex_t239_0_0_0,
	&GenInst_Vector3_t12_0_0_0_Vector3_t12_0_0_0,
	&GenInst_Color32_t347_0_0_0_Color32_t347_0_0_0,
	&GenInst_Vector2_t23_0_0_0_Vector2_t23_0_0_0,
	&GenInst_Vector4_t317_0_0_0_Vector4_t317_0_0_0,
	&GenInst_UICharInfo_t396_0_0_0_UICharInfo_t396_0_0_0,
	&GenInst_UILineInfo_t394_0_0_0_UILineInfo_t394_0_0_0,
	&GenInst_TextEditOp_t615_0_0_0_Object_t_0_0_0,
	&GenInst_TextEditOp_t615_0_0_0_TextEditOp_t615_0_0_0,
	&GenInst_KeyValuePair_2_t2270_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2270_0_0_0_KeyValuePair_2_t2270_0_0_0,
	&GenInst_Boolean_t360_0_0_0_Object_t_0_0_0,
	&GenInst_Boolean_t360_0_0_0_Boolean_t360_0_0_0,
	&GenInst_KeyValuePair_2_t2318_0_0_0_Object_t_0_0_0,
	&GenInst_KeyValuePair_2_t2318_0_0_0_KeyValuePair_2_t2318_0_0_0,
	&GenInst_CustomAttributeTypedArgument_t1326_0_0_0_CustomAttributeTypedArgument_t1326_0_0_0,
	&GenInst_CustomAttributeNamedArgument_t1325_0_0_0_CustomAttributeNamedArgument_t1325_0_0_0,
	&GenInst_ExecuteEvents_Execute_m19418_gp_0_0_0_0,
	&GenInst_ExecuteEvents_ExecuteHierarchy_m19419_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventList_m19421_gp_0_0_0_0,
	&GenInst_ExecuteEvents_CanHandleEvent_m19422_gp_0_0_0_0,
	&GenInst_ExecuteEvents_GetEventHandler_m19423_gp_0_0_0_0,
	&GenInst_TweenRunner_1_t2640_gp_0_0_0_0,
	&GenInst_Dropdown_GetOrAddComponent_m19450_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2644_gp_0_0_0_0,
	&GenInst_IndexedSet_1_t2644_gp_0_0_0_0_Int32_t359_0_0_0,
	&GenInst_ListPool_1_t2645_gp_0_0_0_0,
	&GenInst_List_1_t3688_0_0_0,
	&GenInst_ObjectPool_1_t2646_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m19519_gp_0_0_0_0,
	&GenInst_Component_GetComponentsInChildren_m19520_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m19522_gp_0_0_0_0,
	&GenInst_Component_GetComponents_m19523_gp_0_0_0_0,
	&GenInst_GameObject_GetComponents_m19527_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInChildren_m19528_gp_0_0_0_0,
	&GenInst_GameObject_GetComponentsInParent_m19529_gp_0_0_0_0,
	&GenInst_InvokableCall_1_t2654_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2655_gp_0_0_0_0_InvokableCall_2_t2655_gp_1_0_0_0,
	&GenInst_InvokableCall_2_t2655_gp_0_0_0_0,
	&GenInst_InvokableCall_2_t2655_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t2656_gp_0_0_0_0_InvokableCall_3_t2656_gp_1_0_0_0_InvokableCall_3_t2656_gp_2_0_0_0,
	&GenInst_InvokableCall_3_t2656_gp_0_0_0_0,
	&GenInst_InvokableCall_3_t2656_gp_1_0_0_0,
	&GenInst_InvokableCall_3_t2656_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2657_gp_0_0_0_0_InvokableCall_4_t2657_gp_1_0_0_0_InvokableCall_4_t2657_gp_2_0_0_0_InvokableCall_4_t2657_gp_3_0_0_0,
	&GenInst_InvokableCall_4_t2657_gp_0_0_0_0,
	&GenInst_InvokableCall_4_t2657_gp_1_0_0_0,
	&GenInst_InvokableCall_4_t2657_gp_2_0_0_0,
	&GenInst_InvokableCall_4_t2657_gp_3_0_0_0,
	&GenInst_CachedInvokableCall_1_t688_gp_0_0_0_0,
	&GenInst_UnityEvent_1_t2658_gp_0_0_0_0,
	&GenInst_UnityEvent_2_t2659_gp_0_0_0_0_UnityEvent_2_t2659_gp_1_0_0_0,
	&GenInst_UnityEvent_3_t2660_gp_0_0_0_0_UnityEvent_3_t2660_gp_1_0_0_0_UnityEvent_3_t2660_gp_2_0_0_0,
	&GenInst_UnityEvent_4_t2661_gp_0_0_0_0_UnityEvent_4_t2661_gp_1_0_0_0_UnityEvent_4_t2661_gp_2_0_0_0_UnityEvent_4_t2661_gp_3_0_0_0,
	&GenInst_Enumerable_Where_m19596_gp_0_0_0_0,
	&GenInst_Enumerable_Where_m19596_gp_0_0_0_0_Boolean_t360_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0,
	&GenInst_Enumerable_CreateWhereIterator_m19597_gp_0_0_0_0_Boolean_t360_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0,
	&GenInst_U3CCreateWhereIteratorU3Ec__Iterator1D_1_t2667_gp_0_0_0_0_Boolean_t360_0_0_0,
	&GenInst_Stack_1_t2669_gp_0_0_0_0,
	&GenInst_Enumerator_t2670_gp_0_0_0_0,
	&GenInst_IEnumerable_1_t2676_gp_0_0_0_0,
	&GenInst_Array_InternalArray__IEnumerable_GetEnumerator_m19719_gp_0_0_0_0,
	&GenInst_Array_Sort_m19731_gp_0_0_0_0_Array_Sort_m19731_gp_0_0_0_0,
	&GenInst_Array_Sort_m19732_gp_0_0_0_0_Array_Sort_m19732_gp_1_0_0_0,
	&GenInst_Array_Sort_m19733_gp_0_0_0_0,
	&GenInst_Array_Sort_m19733_gp_0_0_0_0_Array_Sort_m19733_gp_0_0_0_0,
	&GenInst_Array_Sort_m19734_gp_0_0_0_0,
	&GenInst_Array_Sort_m19734_gp_0_0_0_0_Array_Sort_m19734_gp_1_0_0_0,
	&GenInst_Array_Sort_m19735_gp_0_0_0_0_Array_Sort_m19735_gp_0_0_0_0,
	&GenInst_Array_Sort_m19736_gp_0_0_0_0_Array_Sort_m19736_gp_1_0_0_0,
	&GenInst_Array_Sort_m19737_gp_0_0_0_0,
	&GenInst_Array_Sort_m19737_gp_0_0_0_0_Array_Sort_m19737_gp_0_0_0_0,
	&GenInst_Array_Sort_m19738_gp_0_0_0_0,
	&GenInst_Array_Sort_m19738_gp_1_0_0_0,
	&GenInst_Array_Sort_m19738_gp_0_0_0_0_Array_Sort_m19738_gp_1_0_0_0,
	&GenInst_Array_Sort_m19739_gp_0_0_0_0,
	&GenInst_Array_Sort_m19740_gp_0_0_0_0,
	&GenInst_Array_qsort_m19741_gp_0_0_0_0,
	&GenInst_Array_qsort_m19741_gp_0_0_0_0_Array_qsort_m19741_gp_1_0_0_0,
	&GenInst_Array_compare_m19742_gp_0_0_0_0,
	&GenInst_Array_qsort_m19743_gp_0_0_0_0,
	&GenInst_Array_Resize_m19746_gp_0_0_0_0,
	&GenInst_Array_TrueForAll_m19748_gp_0_0_0_0,
	&GenInst_Array_ForEach_m19749_gp_0_0_0_0,
	&GenInst_Array_ConvertAll_m19750_gp_0_0_0_0_Array_ConvertAll_m19750_gp_1_0_0_0,
	&GenInst_Array_FindLastIndex_m19751_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m19752_gp_0_0_0_0,
	&GenInst_Array_FindLastIndex_m19753_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m19754_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m19755_gp_0_0_0_0,
	&GenInst_Array_FindIndex_m19756_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19757_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19758_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19759_gp_0_0_0_0,
	&GenInst_Array_BinarySearch_m19760_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m19761_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m19762_gp_0_0_0_0,
	&GenInst_Array_IndexOf_m19763_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m19764_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m19765_gp_0_0_0_0,
	&GenInst_Array_LastIndexOf_m19766_gp_0_0_0_0,
	&GenInst_Array_FindAll_m19767_gp_0_0_0_0,
	&GenInst_Array_Exists_m19768_gp_0_0_0_0,
	&GenInst_Array_AsReadOnly_m19769_gp_0_0_0_0,
	&GenInst_Array_Find_m19770_gp_0_0_0_0,
	&GenInst_Array_FindLast_m19771_gp_0_0_0_0,
	&GenInst_InternalEnumerator_1_t2677_gp_0_0_0_0,
	&GenInst_ArrayReadOnlyList_1_t2678_gp_0_0_0_0,
	&GenInst_U3CGetEnumeratorU3Ec__Iterator0_t2679_gp_0_0_0_0,
	&GenInst_IList_1_t2680_gp_0_0_0_0,
	&GenInst_ICollection_1_t2681_gp_0_0_0_0,
	&GenInst_Nullable_1_t1765_gp_0_0_0_0,
	&GenInst_Comparer_1_t2688_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2689_gp_0_0_0_0,
	&GenInst_GenericComparer_1_t2637_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3782_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_CopyTo_m19920_gp_0_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0,
	&GenInst_Dictionary_2_Do_ICollectionCopyTo_m19924_gp_0_0_0_0_Object_t_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_DictionaryEntry_t1057_0_0_0,
	&GenInst_ShimEnumerator_t2691_gp_0_0_0_0_ShimEnumerator_t2691_gp_1_0_0_0,
	&GenInst_Enumerator_t2692_gp_0_0_0_0_Enumerator_t2692_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t3795_0_0_0,
	&GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0,
	&GenInst_ValueCollection_t2693_gp_1_0_0_0,
	&GenInst_Enumerator_t2694_gp_0_0_0_0_Enumerator_t2694_gp_1_0_0_0,
	&GenInst_Enumerator_t2694_gp_1_0_0_0,
	&GenInst_ValueCollection_t2693_gp_0_0_0_0_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0,
	&GenInst_ValueCollection_t2693_gp_1_0_0_0_ValueCollection_t2693_gp_1_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_0_0_0_0_Dictionary_2_t2690_gp_1_0_0_0_KeyValuePair_2_t3782_0_0_0,
	&GenInst_KeyValuePair_2_t3782_0_0_0_KeyValuePair_2_t3782_0_0_0,
	&GenInst_Dictionary_2_t2690_gp_1_0_0_0,
	&GenInst_EqualityComparer_1_t2696_gp_0_0_0_0,
	&GenInst_DefaultComparer_t2697_gp_0_0_0_0,
	&GenInst_GenericEqualityComparer_1_t2636_gp_0_0_0_0,
	&GenInst_KeyValuePair_2_t3822_0_0_0,
	&GenInst_IDictionary_2_t2699_gp_0_0_0_0_IDictionary_2_t2699_gp_1_0_0_0,
	&GenInst_KeyValuePair_2_t2701_gp_0_0_0_0_KeyValuePair_2_t2701_gp_1_0_0_0,
	&GenInst_List_1_t2702_gp_0_0_0_0,
	&GenInst_Enumerator_t2703_gp_0_0_0_0,
	&GenInst_Collection_1_t2704_gp_0_0_0_0,
	&GenInst_ReadOnlyCollection_1_t2705_gp_0_0_0_0,
	&GenInst_MonoProperty_GetterAdapterFrame_m20180_gp_0_0_0_0_MonoProperty_GetterAdapterFrame_m20180_gp_1_0_0_0,
	&GenInst_MonoProperty_StaticGetterAdapterFrame_m20181_gp_0_0_0_0,
};
