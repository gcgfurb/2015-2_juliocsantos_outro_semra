﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.MulticastDelegate
struct MulticastDelegate_t227;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t652;
// System.Object
struct Object_t;
// System.Delegate[]
struct DelegateU5BU5D_t1739;
// System.Delegate
struct Delegate_t384;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"

// System.Void System.MulticastDelegate::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void MulticastDelegate_GetObjectData_m6310 (MulticastDelegate_t227 * __this, SerializationInfo_t652 * ___info, StreamingContext_t653  ___context, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::Equals(System.Object)
extern "C" bool MulticastDelegate_Equals_m6311 (MulticastDelegate_t227 * __this, Object_t * ___obj, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Int32 System.MulticastDelegate::GetHashCode()
extern "C" int32_t MulticastDelegate_GetHashCode_m6312 (MulticastDelegate_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate[] System.MulticastDelegate::GetInvocationList()
extern "C" DelegateU5BU5D_t1739* MulticastDelegate_GetInvocationList_m6313 (MulticastDelegate_t227 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::CombineImpl(System.Delegate)
extern "C" Delegate_t384 * MulticastDelegate_CombineImpl_m6314 (MulticastDelegate_t227 * __this, Delegate_t384 * ___follow, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean System.MulticastDelegate::BaseEquals(System.MulticastDelegate)
extern "C" bool MulticastDelegate_BaseEquals_m6315 (MulticastDelegate_t227 * __this, MulticastDelegate_t227 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.MulticastDelegate System.MulticastDelegate::KPM(System.MulticastDelegate,System.MulticastDelegate,System.MulticastDelegate&)
extern "C" MulticastDelegate_t227 * MulticastDelegate_KPM_m6316 (Object_t * __this /* static, unused */, MulticastDelegate_t227 * ___needle, MulticastDelegate_t227 * ___haystack, MulticastDelegate_t227 ** ___tail, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Delegate System.MulticastDelegate::RemoveImpl(System.Delegate)
extern "C" Delegate_t384 * MulticastDelegate_RemoveImpl_m6317 (MulticastDelegate_t227 * __this, Delegate_t384 * ___value, const MethodInfo* method) IL2CPP_METHOD_ATTR;
