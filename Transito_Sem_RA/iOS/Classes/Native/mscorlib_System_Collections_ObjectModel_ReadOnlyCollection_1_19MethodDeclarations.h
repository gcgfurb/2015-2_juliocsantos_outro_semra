﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_ObjectModel_ReadOnlyCollection_1_2MethodDeclarations.h"

// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::.ctor(System.Collections.Generic.IList`1<T>)
#define ReadOnlyCollection_1__ctor_m14165(__this, ___list, method) (( void (*) (ReadOnlyCollection_1_t2052 *, Object_t*, const MethodInfo*))ReadOnlyCollection_1__ctor_m11331_gshared)(__this, ___list, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.ICollection<T>.Add(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m14166(__this, ___item, method) (( void (*) (ReadOnlyCollection_1_t2052 *, RectMask2D_t243 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Add_m11332_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.ICollection<T>.Clear()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m14167(__this, method) (( void (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Clear_m11333_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.IList<T>.Insert(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m14168(__this, ___index, ___item, method) (( void (*) (ReadOnlyCollection_1_t2052 *, int32_t, RectMask2D_t243 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_Insert_m11334_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.ICollection<T>.Remove(T)
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m14169(__this, ___item, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, RectMask2D_t243 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_Remove_m11335_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.IList<T>.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m14170(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2052 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_RemoveAt_m11336_gshared)(__this, ___index, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.IList<T>.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m14171(__this, ___index, method) (( RectMask2D_t243 * (*) (ReadOnlyCollection_1_t2052 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_get_Item_m11337_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.IList<T>.set_Item(System.Int32,T)
#define ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m14172(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2052 *, int32_t, RectMask2D_t243 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_IListU3CTU3E_set_Item_m11338_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m14173(__this, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m11339_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m14174(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2052 *, Array_t *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_CopyTo_m11340_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IEnumerable.GetEnumerator()
#define ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m14175(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IEnumerable_GetEnumerator_m11341_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Add(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Add_m14176(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2052 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Add_m11342_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Clear()
#define ReadOnlyCollection_1_System_Collections_IList_Clear_m14177(__this, method) (( void (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Clear_m11343_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Contains(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Contains_m14178(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Contains_m11344_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.IndexOf(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_IndexOf_m14179(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2052 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_IndexOf_m11345_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Insert(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Insert_m14180(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2052 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Insert_m11346_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.Remove(System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_Remove_m14181(__this, ___value, method) (( void (*) (ReadOnlyCollection_1_t2052 *, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_Remove_m11347_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.RemoveAt(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m14182(__this, ___index, method) (( void (*) (ReadOnlyCollection_1_t2052 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_RemoveAt_m11348_gshared)(__this, ___index, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.ICollection.get_IsSynchronized()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m14183(__this, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_IsSynchronized_m11349_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.ICollection.get_SyncRoot()
#define ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m14184(__this, method) (( Object_t * (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_ICollection_get_SyncRoot_m11350_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.get_IsFixedSize()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m14185(__this, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsFixedSize_m11351_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.get_IsReadOnly()
#define ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m14186(__this, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_IsReadOnly_m11352_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.get_Item(System.Int32)
#define ReadOnlyCollection_1_System_Collections_IList_get_Item_m14187(__this, ___index, method) (( Object_t * (*) (ReadOnlyCollection_1_t2052 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_get_Item_m11353_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::System.Collections.IList.set_Item(System.Int32,System.Object)
#define ReadOnlyCollection_1_System_Collections_IList_set_Item_m14188(__this, ___index, ___value, method) (( void (*) (ReadOnlyCollection_1_t2052 *, int32_t, Object_t *, const MethodInfo*))ReadOnlyCollection_1_System_Collections_IList_set_Item_m11354_gshared)(__this, ___index, ___value, method)
// System.Boolean System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::Contains(T)
#define ReadOnlyCollection_1_Contains_m14189(__this, ___value, method) (( bool (*) (ReadOnlyCollection_1_t2052 *, RectMask2D_t243 *, const MethodInfo*))ReadOnlyCollection_1_Contains_m11355_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::CopyTo(T[],System.Int32)
#define ReadOnlyCollection_1_CopyTo_m14190(__this, ___array, ___index, method) (( void (*) (ReadOnlyCollection_1_t2052 *, RectMask2DU5BU5D_t2051*, int32_t, const MethodInfo*))ReadOnlyCollection_1_CopyTo_m11356_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::GetEnumerator()
#define ReadOnlyCollection_1_GetEnumerator_m14191(__this, method) (( Object_t* (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_GetEnumerator_m11357_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::IndexOf(T)
#define ReadOnlyCollection_1_IndexOf_m14192(__this, ___value, method) (( int32_t (*) (ReadOnlyCollection_1_t2052 *, RectMask2D_t243 *, const MethodInfo*))ReadOnlyCollection_1_IndexOf_m11358_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::get_Count()
#define ReadOnlyCollection_1_get_Count_m14193(__this, method) (( int32_t (*) (ReadOnlyCollection_1_t2052 *, const MethodInfo*))ReadOnlyCollection_1_get_Count_m11359_gshared)(__this, method)
// T System.Collections.ObjectModel.ReadOnlyCollection`1<UnityEngine.UI.RectMask2D>::get_Item(System.Int32)
#define ReadOnlyCollection_1_get_Item_m14194(__this, ___index, method) (( RectMask2D_t243 * (*) (ReadOnlyCollection_1_t2052 *, int32_t, const MethodInfo*))ReadOnlyCollection_1_get_Item_m11360_gshared)(__this, ___index, method)
