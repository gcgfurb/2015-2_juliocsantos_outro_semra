﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Funcoes_Botoes
struct Funcoes_Botoes_t94;

#include "codegen/il2cpp-codegen.h"

// System.Void Funcoes_Botoes::.ctor()
extern "C" void Funcoes_Botoes__ctor_m406 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Funcoes_Botoes::Start()
extern "C" void Funcoes_Botoes_Start_m407 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Funcoes_Botoes::Update()
extern "C" void Funcoes_Botoes_Update_m408 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Funcoes_Botoes::Reiniciar()
extern "C" void Funcoes_Botoes_Reiniciar_m409 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Funcoes_Botoes::Sair()
extern "C" void Funcoes_Botoes_Sair_m410 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Funcoes_Botoes::VoltarInicio()
extern "C" void Funcoes_Botoes_VoltarInicio_m411 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Funcoes_Botoes::Pausar()
extern "C" void Funcoes_Botoes_Pausar_m412 (Funcoes_Botoes_t94 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
