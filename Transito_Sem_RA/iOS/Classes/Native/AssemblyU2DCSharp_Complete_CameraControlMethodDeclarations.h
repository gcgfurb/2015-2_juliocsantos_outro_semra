﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// Complete.CameraControl
struct CameraControl_t89;

#include "codegen/il2cpp-codegen.h"

// System.Void Complete.CameraControl::.ctor()
extern "C" void CameraControl__ctor_m369 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Complete.CameraControl::Awake()
extern "C" void CameraControl_Awake_m370 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Complete.CameraControl::FixedUpdate()
extern "C" void CameraControl_FixedUpdate_m371 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Complete.CameraControl::Move()
extern "C" void CameraControl_Move_m372 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Complete.CameraControl::FindAveragePosition()
extern "C" void CameraControl_FindAveragePosition_m373 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Complete.CameraControl::Zoom()
extern "C" void CameraControl_Zoom_m374 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Single Complete.CameraControl::FindRequiredSize()
extern "C" float CameraControl_FindRequiredSize_m375 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void Complete.CameraControl::SetStartPositionAndSize()
extern "C" void CameraControl_SetStartPositionAndSize_m376 (CameraControl_t89 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
