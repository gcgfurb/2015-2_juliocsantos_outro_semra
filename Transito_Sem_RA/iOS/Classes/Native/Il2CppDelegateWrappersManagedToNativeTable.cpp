﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif



#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"

extern "C" void pinvoke_delegate_wrapper_OnValidateInput_t226 ();
extern "C" void pinvoke_delegate_wrapper_StateChanged_t464 ();
extern "C" void pinvoke_delegate_wrapper_ReapplyDrivenProperties_t415 ();
extern "C" void pinvoke_delegate_wrapper_LogCallback_t485 ();
extern "C" void pinvoke_delegate_wrapper_CameraCallback_t487 ();
extern "C" void pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t489 ();
extern "C" void pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t513 ();
extern "C" void pinvoke_delegate_wrapper_PCMReaderCallback_t515 ();
extern "C" void pinvoke_delegate_wrapper_PCMSetPositionCallback_t517 ();
extern "C" void pinvoke_delegate_wrapper_FontTextureRebuildCallback_t534 ();
extern "C" void pinvoke_delegate_wrapper_WillRenderCanvases_t372 ();
extern "C" void pinvoke_delegate_wrapper_WindowFunction_t543 ();
extern "C" void pinvoke_delegate_wrapper_SkinChangedDelegate_t560 ();
extern "C" void pinvoke_delegate_wrapper_UnityAdsDelegate_t500 ();
extern "C" void pinvoke_delegate_wrapper_UnityAction_t198 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t746 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t846 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback_t821 ();
extern "C" void pinvoke_delegate_wrapper_CertificateValidationCallback2_t822 ();
extern "C" void pinvoke_delegate_wrapper_CertificateSelectionCallback_t805 ();
extern "C" void pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t806 ();
extern "C" void pinvoke_delegate_wrapper_MatchAppendEvaluator_t984 ();
extern "C" void pinvoke_delegate_wrapper_CostDelegate_t1019 ();
extern "C" void pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t892 ();
extern "C" void pinvoke_delegate_wrapper_MatchEvaluator_t1053 ();
extern "C" void pinvoke_delegate_wrapper_Swapper_t1082 ();
extern "C" void pinvoke_delegate_wrapper_AsyncCallback_t229 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1146 ();
extern "C" void pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1154 ();
extern "C" void pinvoke_delegate_wrapper_ReadDelegate_t1242 ();
extern "C" void pinvoke_delegate_wrapper_WriteDelegate_t1243 ();
extern "C" void pinvoke_delegate_wrapper_AddEventAdapter_t1328 ();
extern "C" void pinvoke_delegate_wrapper_GetterAdapter_t1343 ();
extern "C" void pinvoke_delegate_wrapper_CallbackHandler_t1522 ();
extern "C" void pinvoke_delegate_wrapper_PrimalityTest_t1709 ();
extern "C" void pinvoke_delegate_wrapper_MemberFilter_t1085 ();
extern "C" void pinvoke_delegate_wrapper_TypeFilter_t1335 ();
extern "C" void pinvoke_delegate_wrapper_CrossContextDelegate_t1710 ();
extern "C" void pinvoke_delegate_wrapper_HeaderHandler_t1711 ();
extern "C" void pinvoke_delegate_wrapper_ThreadStart_t1713 ();
extern "C" void pinvoke_delegate_wrapper_TimerCallback_t1632 ();
extern "C" void pinvoke_delegate_wrapper_WaitCallback_t1714 ();
extern "C" void pinvoke_delegate_wrapper_AppDomainInitializer_t1641 ();
extern "C" void pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1638 ();
extern "C" void pinvoke_delegate_wrapper_EventHandler_t1265 ();
extern "C" void pinvoke_delegate_wrapper_ResolveEventHandler_t1639 ();
extern "C" void pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t656 ();
extern const methodPointerType g_DelegateWrappersManagedToNative[47] = 
{
	pinvoke_delegate_wrapper_OnValidateInput_t226,
	pinvoke_delegate_wrapper_StateChanged_t464,
	pinvoke_delegate_wrapper_ReapplyDrivenProperties_t415,
	pinvoke_delegate_wrapper_LogCallback_t485,
	pinvoke_delegate_wrapper_CameraCallback_t487,
	pinvoke_delegate_wrapper_DisplaysUpdatedDelegate_t489,
	pinvoke_delegate_wrapper_AudioConfigurationChangeHandler_t513,
	pinvoke_delegate_wrapper_PCMReaderCallback_t515,
	pinvoke_delegate_wrapper_PCMSetPositionCallback_t517,
	pinvoke_delegate_wrapper_FontTextureRebuildCallback_t534,
	pinvoke_delegate_wrapper_WillRenderCanvases_t372,
	pinvoke_delegate_wrapper_WindowFunction_t543,
	pinvoke_delegate_wrapper_SkinChangedDelegate_t560,
	pinvoke_delegate_wrapper_UnityAdsDelegate_t500,
	pinvoke_delegate_wrapper_UnityAction_t198,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t746,
	pinvoke_delegate_wrapper_PrimalityTest_t846,
	pinvoke_delegate_wrapper_CertificateValidationCallback_t821,
	pinvoke_delegate_wrapper_CertificateValidationCallback2_t822,
	pinvoke_delegate_wrapper_CertificateSelectionCallback_t805,
	pinvoke_delegate_wrapper_PrivateKeySelectionCallback_t806,
	pinvoke_delegate_wrapper_MatchAppendEvaluator_t984,
	pinvoke_delegate_wrapper_CostDelegate_t1019,
	pinvoke_delegate_wrapper_RemoteCertificateValidationCallback_t892,
	pinvoke_delegate_wrapper_MatchEvaluator_t1053,
	pinvoke_delegate_wrapper_Swapper_t1082,
	pinvoke_delegate_wrapper_AsyncCallback_t229,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1146,
	pinvoke_delegate_wrapper_KeyGeneratedEventHandler_t1154,
	pinvoke_delegate_wrapper_ReadDelegate_t1242,
	pinvoke_delegate_wrapper_WriteDelegate_t1243,
	pinvoke_delegate_wrapper_AddEventAdapter_t1328,
	pinvoke_delegate_wrapper_GetterAdapter_t1343,
	pinvoke_delegate_wrapper_CallbackHandler_t1522,
	pinvoke_delegate_wrapper_PrimalityTest_t1709,
	pinvoke_delegate_wrapper_MemberFilter_t1085,
	pinvoke_delegate_wrapper_TypeFilter_t1335,
	pinvoke_delegate_wrapper_CrossContextDelegate_t1710,
	pinvoke_delegate_wrapper_HeaderHandler_t1711,
	pinvoke_delegate_wrapper_ThreadStart_t1713,
	pinvoke_delegate_wrapper_TimerCallback_t1632,
	pinvoke_delegate_wrapper_WaitCallback_t1714,
	pinvoke_delegate_wrapper_AppDomainInitializer_t1641,
	pinvoke_delegate_wrapper_AssemblyLoadEventHandler_t1638,
	pinvoke_delegate_wrapper_EventHandler_t1265,
	pinvoke_delegate_wrapper_ResolveEventHandler_t1639,
	pinvoke_delegate_wrapper_UnhandledExceptionEventHandler_t656,
};
