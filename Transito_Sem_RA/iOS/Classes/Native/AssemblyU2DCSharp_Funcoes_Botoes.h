﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.RectTransform
struct RectTransform_t95;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// Funcoes_Botoes
struct  Funcoes_Botoes_t94  : public MonoBehaviour_t2
{
	// UnityEngine.RectTransform Funcoes_Botoes::m_MenuPausa
	RectTransform_t95 * ___m_MenuPausa_2;
};
