﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Advertisements_UnityAdsDelegate_2_ge_0MethodDeclarations.h"

// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::.ctor(System.Object,System.IntPtr)
#define UnityAdsDelegate_2__ctor_m16023(__this, ___object, ___method, method) (( void (*) (UnityAdsDelegate_2_t501 *, Object_t *, IntPtr_t, const MethodInfo*))UnityAdsDelegate_2__ctor_m16024_gshared)(__this, ___object, ___method, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::Invoke(T1,T2)
#define UnityAdsDelegate_2_Invoke_m3604(__this, ___p1, ___p2, method) (( void (*) (UnityAdsDelegate_2_t501 *, String_t*, bool, const MethodInfo*))UnityAdsDelegate_2_Invoke_m16025_gshared)(__this, ___p1, ___p2, method)
// System.IAsyncResult UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::BeginInvoke(T1,T2,System.AsyncCallback,System.Object)
#define UnityAdsDelegate_2_BeginInvoke_m16026(__this, ___p1, ___p2, ___callback, ___object, method) (( Object_t * (*) (UnityAdsDelegate_2_t501 *, String_t*, bool, AsyncCallback_t229 *, Object_t *, const MethodInfo*))UnityAdsDelegate_2_BeginInvoke_m16027_gshared)(__this, ___p1, ___p2, ___callback, ___object, method)
// System.Void UnityEngine.Advertisements.UnityAdsDelegate`2<System.String,System.Boolean>::EndInvoke(System.IAsyncResult)
#define UnityAdsDelegate_2_EndInvoke_m16028(__this, ___result, method) (( void (*) (UnityAdsDelegate_2_t501 *, Object_t *, const MethodInfo*))UnityAdsDelegate_2_EndInvoke_m16029_gshared)(__this, ___result, method)
