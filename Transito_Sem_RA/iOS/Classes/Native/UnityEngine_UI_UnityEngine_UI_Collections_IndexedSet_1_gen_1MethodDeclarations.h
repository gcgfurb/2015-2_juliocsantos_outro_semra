﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::.ctor()
#define IndexedSet_1__ctor_m2366(__this, method) (( void (*) (IndexedSet_1_t284 *, const MethodInfo*))IndexedSet_1__ctor_m12980_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m14701(__this, method) (( Object_t * (*) (IndexedSet_1_t284 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12982_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Add(T)
#define IndexedSet_1_Add_m14702(__this, ___item, method) (( void (*) (IndexedSet_1_t284 *, Object_t *, const MethodInfo*))IndexedSet_1_Add_m12984_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Remove(T)
#define IndexedSet_1_Remove_m14703(__this, ___item, method) (( bool (*) (IndexedSet_1_t284 *, Object_t *, const MethodInfo*))IndexedSet_1_Remove_m12986_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m14704(__this, method) (( Object_t* (*) (IndexedSet_1_t284 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m12988_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Clear()
#define IndexedSet_1_Clear_m14705(__this, method) (( void (*) (IndexedSet_1_t284 *, const MethodInfo*))IndexedSet_1_Clear_m12990_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Contains(T)
#define IndexedSet_1_Contains_m14706(__this, ___item, method) (( bool (*) (IndexedSet_1_t284 *, Object_t *, const MethodInfo*))IndexedSet_1_Contains_m12992_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m14707(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t284 *, IClipperU5BU5D_t2091*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m12994_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Count()
#define IndexedSet_1_get_Count_m14708(__this, method) (( int32_t (*) (IndexedSet_1_t284 *, const MethodInfo*))IndexedSet_1_get_Count_m12996_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m14709(__this, method) (( bool (*) (IndexedSet_1_t284 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m12998_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::IndexOf(T)
#define IndexedSet_1_IndexOf_m14710(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t284 *, Object_t *, const MethodInfo*))IndexedSet_1_IndexOf_m13000_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m14711(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t284 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_Insert_m13002_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m14712(__this, ___index, method) (( void (*) (IndexedSet_1_t284 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m13004_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m14713(__this, ___index, method) (( Object_t * (*) (IndexedSet_1_t284 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m13006_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m14714(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t284 *, int32_t, Object_t *, const MethodInfo*))IndexedSet_1_set_Item_m13008_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m14715(__this, ___match, method) (( void (*) (IndexedSet_1_t284 *, Predicate_1_t2094 *, const MethodInfo*))IndexedSet_1_RemoveAll_m13010_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.IClipper>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m14716(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t284 *, Comparison_1_t2095 *, const MethodInfo*))IndexedSet_1_Sort_m13011_gshared)(__this, ___sortLayoutFunction, method)
