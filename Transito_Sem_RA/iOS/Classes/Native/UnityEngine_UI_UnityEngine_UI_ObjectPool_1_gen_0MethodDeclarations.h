﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_ObjectPool_1_gen_1MethodDeclarations.h"

// System.Void UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::.ctor(UnityEngine.Events.UnityAction`1<T>,UnityEngine.Events.UnityAction`1<T>)
#define ObjectPool_1__ctor_m2396(__this, ___actionOnGet, ___actionOnRelease, method) (( void (*) (ObjectPool_1_t305 *, UnityAction_1_t306 *, UnityAction_1_t306 *, const MethodInfo*))ObjectPool_1__ctor_m11902_gshared)(__this, ___actionOnGet, ___actionOnRelease, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::get_countAll()
#define ObjectPool_1_get_countAll_m14949(__this, method) (( int32_t (*) (ObjectPool_1_t305 *, const MethodInfo*))ObjectPool_1_get_countAll_m11904_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::set_countAll(System.Int32)
#define ObjectPool_1_set_countAll_m14950(__this, ___value, method) (( void (*) (ObjectPool_1_t305 *, int32_t, const MethodInfo*))ObjectPool_1_set_countAll_m11906_gshared)(__this, ___value, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::get_countActive()
#define ObjectPool_1_get_countActive_m14951(__this, method) (( int32_t (*) (ObjectPool_1_t305 *, const MethodInfo*))ObjectPool_1_get_countActive_m11908_gshared)(__this, method)
// System.Int32 UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::get_countInactive()
#define ObjectPool_1_get_countInactive_m14952(__this, method) (( int32_t (*) (ObjectPool_1_t305 *, const MethodInfo*))ObjectPool_1_get_countInactive_m11910_gshared)(__this, method)
// T UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::Get()
#define ObjectPool_1_Get_m2401(__this, method) (( LayoutRebuilder_t304 * (*) (ObjectPool_1_t305 *, const MethodInfo*))ObjectPool_1_Get_m11912_gshared)(__this, method)
// System.Void UnityEngine.UI.ObjectPool`1<UnityEngine.UI.LayoutRebuilder>::Release(T)
#define ObjectPool_1_Release_m2402(__this, ___element, method) (( void (*) (ObjectPool_1_t305 *, LayoutRebuilder_t304 *, const MethodInfo*))ObjectPool_1_Release_m11914_gshared)(__this, ___element, method)
