﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_67.h"

// System.Void System.Array/InternalEnumerator`1<System.Boolean>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17527_gshared (InternalEnumerator_1_t2320 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17527(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2320 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17527_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17528_gshared (InternalEnumerator_1_t2320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17528(__this, method) (( void (*) (InternalEnumerator_1_t2320 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17528_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Boolean>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17529_gshared (InternalEnumerator_1_t2320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17529(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2320 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17529_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Boolean>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17530_gshared (InternalEnumerator_1_t2320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17530(__this, method) (( void (*) (InternalEnumerator_1_t2320 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17530_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Boolean>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17531_gshared (InternalEnumerator_1_t2320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17531(__this, method) (( bool (*) (InternalEnumerator_1_t2320 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17531_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Boolean>::get_Current()
extern "C" bool InternalEnumerator_1_get_Current_m17532_gshared (InternalEnumerator_1_t2320 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17532(__this, method) (( bool (*) (InternalEnumerator_1_t2320 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17532_gshared)(__this, method)
