﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_17.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m12131_gshared (InternalEnumerator_1_t1913 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m12131(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1913 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m12131_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12132_gshared (InternalEnumerator_1_t1913 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12132(__this, method) (( void (*) (InternalEnumerator_1_t1913 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m12132_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12133_gshared (InternalEnumerator_1_t1913 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12133(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1913 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m12133_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m12134_gshared (InternalEnumerator_1_t1913 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m12134(__this, method) (( void (*) (InternalEnumerator_1_t1913 *, const MethodInfo*))InternalEnumerator_1_Dispose_m12134_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m12135_gshared (InternalEnumerator_1_t1913 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m12135(__this, method) (( bool (*) (InternalEnumerator_1_t1913 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m12135_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityEngine.EventSystems.RaycastResult>::get_Current()
extern "C" RaycastResult_t139  InternalEnumerator_1_get_Current_m12136_gshared (InternalEnumerator_1_t1913 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m12136(__this, method) (( RaycastResult_t139  (*) (InternalEnumerator_1_t1913 *, const MethodInfo*))InternalEnumerator_1_get_Current_m12136_gshared)(__this, method)
