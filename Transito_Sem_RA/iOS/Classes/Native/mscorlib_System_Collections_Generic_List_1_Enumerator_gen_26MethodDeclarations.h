﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.List`1<UnityEngine.Vector3>
struct List_1_t312;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_List_1_Enumerator_gen_26.h"
#include "UnityEngine_UnityEngine_Vector3.h"

// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::.ctor(System.Collections.Generic.List`1<T>)
extern "C" void Enumerator__ctor_m15023_gshared (Enumerator_t2108 * __this, List_1_t312 * ___l, const MethodInfo* method);
#define Enumerator__ctor_m15023(__this, ___l, method) (( void (*) (Enumerator_t2108 *, List_1_t312 *, const MethodInfo*))Enumerator__ctor_m15023_gshared)(__this, ___l, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.Reset()
extern "C" void Enumerator_System_Collections_IEnumerator_Reset_m15024_gshared (Enumerator_t2108 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_Reset_m15024(__this, method) (( void (*) (Enumerator_t2108 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_Reset_m15024_gshared)(__this, method)
// System.Object System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * Enumerator_System_Collections_IEnumerator_get_Current_m15025_gshared (Enumerator_t2108 * __this, const MethodInfo* method);
#define Enumerator_System_Collections_IEnumerator_get_Current_m15025(__this, method) (( Object_t * (*) (Enumerator_t2108 *, const MethodInfo*))Enumerator_System_Collections_IEnumerator_get_Current_m15025_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::Dispose()
extern "C" void Enumerator_Dispose_m15026_gshared (Enumerator_t2108 * __this, const MethodInfo* method);
#define Enumerator_Dispose_m15026(__this, method) (( void (*) (Enumerator_t2108 *, const MethodInfo*))Enumerator_Dispose_m15026_gshared)(__this, method)
// System.Void System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::VerifyState()
extern "C" void Enumerator_VerifyState_m15027_gshared (Enumerator_t2108 * __this, const MethodInfo* method);
#define Enumerator_VerifyState_m15027(__this, method) (( void (*) (Enumerator_t2108 *, const MethodInfo*))Enumerator_VerifyState_m15027_gshared)(__this, method)
// System.Boolean System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::MoveNext()
extern "C" bool Enumerator_MoveNext_m15028_gshared (Enumerator_t2108 * __this, const MethodInfo* method);
#define Enumerator_MoveNext_m15028(__this, method) (( bool (*) (Enumerator_t2108 *, const MethodInfo*))Enumerator_MoveNext_m15028_gshared)(__this, method)
// T System.Collections.Generic.List`1/Enumerator<UnityEngine.Vector3>::get_Current()
extern "C" Vector3_t12  Enumerator_get_Current_m15029_gshared (Enumerator_t2108 * __this, const MethodInfo* method);
#define Enumerator_get_Current_m15029(__this, method) (( Vector3_t12  (*) (Enumerator_t2108 *, const MethodInfo*))Enumerator_get_Current_m15029_gshared)(__this, method)
