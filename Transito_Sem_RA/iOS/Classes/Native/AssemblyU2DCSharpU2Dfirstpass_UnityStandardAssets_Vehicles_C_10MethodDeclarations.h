﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0
struct U3CStartU3Ec__Iterator0_t49;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::.ctor()
extern "C" void U3CStartU3Ec__Iterator0__ctor_m184 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m185 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Object UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CStartU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m186 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Boolean UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::MoveNext()
extern "C" bool U3CStartU3Ec__Iterator0_MoveNext_m187 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::Dispose()
extern "C" void U3CStartU3Ec__Iterator0_Dispose_m188 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.Vehicles.Car.SkidTrail/<Start>c__Iterator0::Reset()
extern "C" void U3CStartU3Ec__Iterator0_Reset_m189 (U3CStartU3Ec__Iterator0_t49 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
