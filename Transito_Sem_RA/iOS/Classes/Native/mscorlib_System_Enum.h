﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// System.Char[]
struct CharU5BU5D_t238;

#include "mscorlib_System_ValueType.h"

// System.Enum
struct  Enum_t665  : public ValueType_t1071
{
};
struct Enum_t665_StaticFields{
	// System.Char[] System.Enum::split_char
	CharU5BU5D_t238* ___split_char_0;
};
