﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::.ctor(TKey,TValue)
#define KeyValuePair_2__ctor_m16766(__this, ___key, ___value, method) (( void (*) (KeyValuePair_2_t2250 *, String_t*, GUIStyle_t553 *, const MethodInfo*))KeyValuePair_2__ctor_m11006_gshared)(__this, ___key, ___value, method)
// TKey System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Key()
#define KeyValuePair_2_get_Key_m16767(__this, method) (( String_t* (*) (KeyValuePair_2_t2250 *, const MethodInfo*))KeyValuePair_2_get_Key_m11007_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Key(TKey)
#define KeyValuePair_2_set_Key_m16768(__this, ___value, method) (( void (*) (KeyValuePair_2_t2250 *, String_t*, const MethodInfo*))KeyValuePair_2_set_Key_m11008_gshared)(__this, ___value, method)
// TValue System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::get_Value()
#define KeyValuePair_2_get_Value_m16769(__this, method) (( GUIStyle_t553 * (*) (KeyValuePair_2_t2250 *, const MethodInfo*))KeyValuePair_2_get_Value_m11009_gshared)(__this, method)
// System.Void System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::set_Value(TValue)
#define KeyValuePair_2_set_Value_m16770(__this, ___value, method) (( void (*) (KeyValuePair_2_t2250 *, GUIStyle_t553 *, const MethodInfo*))KeyValuePair_2_set_Value_m11010_gshared)(__this, ___value, method)
// System.String System.Collections.Generic.KeyValuePair`2<System.String,UnityEngine.GUIStyle>::ToString()
#define KeyValuePair_2_ToString_m16771(__this, method) (( String_t* (*) (KeyValuePair_2_t2250 *, const MethodInfo*))KeyValuePair_2_ToString_m11011_gshared)(__this, method)
