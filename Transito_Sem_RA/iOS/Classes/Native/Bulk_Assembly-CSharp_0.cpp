﻿#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <cstring>
#include <string.h>
#include <stdio.h>
#include <cmath>
#include <limits>
#include <assert.h>

// mset.Logo
struct Logo_t81;
// UnityEngine.Camera
struct Camera_t74;
// System.Object
struct Object_t;
// OrbitCamera
struct OrbitCamera_t85;
// UnityEngine.Rigidbody
struct Rigidbody_t34;
// AtualizaVida
struct AtualizaVida_t86;
// UnityEngine.Collision
struct Collision_t58;
// UnityEngine.Collider
struct Collider_t98;
// Complete.CameraControl
struct CameraControl_t89;
// Car_Health/<Arrumar>c__Iterator0
struct U3CArrumarU3Ec__Iterator0_t91;
// Car_Health/<Abastecer>c__Iterator1
struct U3CAbastecerU3Ec__Iterator1_t92;
// Car_Health
struct Car_Health_t87;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// Funcoes_Botoes
struct Funcoes_Botoes_t94;
// Principal
struct Principal_t96;
// System.String
struct String_t;

#include "class-internals.h"
#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array.h"
#include "AssemblyU2DCSharp_U3CModuleU3E.h"
#include "AssemblyU2DCSharp_U3CModuleU3EMethodDeclarations.h"
#include "AssemblyU2DCSharp_mset_Corner.h"
#include "AssemblyU2DCSharp_mset_CornerMethodDeclarations.h"
#include "AssemblyU2DCSharp_mset_Logo.h"
#include "AssemblyU2DCSharp_mset_LogoMethodDeclarations.h"
#include "mscorlib_System_Void.h"
#include "UnityEngine_UnityEngine_ColorMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector2MethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MonoBehaviourMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Color.h"
#include "UnityEngine_UnityEngine_Vector2.h"
#include "mscorlib_System_Single.h"
#include "UnityEngine_UnityEngine_Rect.h"
#include "UnityEngine_UnityEngine_MonoBehaviour.h"
#include "UnityEngine_UnityEngine_ResourcesMethodDeclarations.h"
#include "mscorlib_System_String.h"
#include "UnityEngine_UnityEngine_Object.h"
#include "UnityEngine_UnityEngine_Texture2D.h"
#include "UnityEngine_UnityEngine_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_ComponentMethodDeclarations.h"
#include "UnityEngine_UnityEngine_CameraMethodDeclarations.h"
#include "mscorlib_System_Boolean.h"
#include "UnityEngine_UnityEngine_Texture.h"
#include "UnityEngine_UnityEngine_TextureMethodDeclarations.h"
#include "mscorlib_System_Int32.h"
#include "UnityEngine_UnityEngine_Component.h"
#include "UnityEngine_UnityEngine_Camera.h"
#include "UnityEngine_UnityEngine_GUIMethodDeclarations.h"
#include "AssemblyU2DCSharp_OrbitCamera.h"
#include "AssemblyU2DCSharp_OrbitCameraMethodDeclarations.h"
#include "UnityEngine_UnityEngine_TransformMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RigidbodyMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3.h"
#include "UnityEngine_UnityEngine_Transform.h"
#include "UnityEngine_UnityEngine_Rigidbody.h"
#include "UnityEngine_UnityEngine_TimeMethodDeclarations.h"
#include "UnityEngine_UnityEngine_InputMethodDeclarations.h"
#include "UnityEngine_UnityEngine_KeyCode.h"
#include "UnityEngine_UnityEngine_QuaternionMethodDeclarations.h"
#include "UnityEngine_UnityEngine_MathfMethodDeclarations.h"
#include "UnityEngine_UnityEngine_PhysicsMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RaycastHitMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Vector3MethodDeclarations.h"
#include "UnityEngine_UnityEngine_DebugMethodDeclarations.h"
#include "UnityEngine_UnityEngine_Quaternion.h"
#include "UnityEngine_UnityEngine_RaycastHit.h"
#include "mscorlib_System_Object.h"
#include "AssemblyU2DCSharp_AtualizaVida.h"
#include "AssemblyU2DCSharp_AtualizaVidaMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_6MethodDeclarations.h"
#include "AssemblyU2DCSharp_Car_HealthMethodDeclarations.h"
#include "AssemblyU2DCSharpU2Dfirstpass_UnityStandardAssets_Vehicles_C_6.h"
#include "AssemblyU2DCSharp_Car_Health.h"
#include "UnityEngine_UnityEngine_Collision.h"
#include "UnityEngine_UnityEngine_Collider.h"
#include "UnityEngine_UnityEngine_GameObjectMethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1MethodDeclarations.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_gen_1.h"
#include "UnityEngine_UnityEngine_GameObject.h"
#include "AssemblyU2DCSharp_Complete_CameraControl.h"
#include "AssemblyU2DCSharp_Complete_CameraControlMethodDeclarations.h"
#include "UnityEngine_ArrayTypes.h"
#include "AssemblyU2DCSharp_Car_Health_U3CArrumarU3Ec__Iterator0.h"
#include "AssemblyU2DCSharp_Car_Health_U3CArrumarU3Ec__Iterator0MethodDeclarations.h"
#include "mscorlib_System_ObjectMethodDeclarations.h"
#include "UnityEngine_UnityEngine_WaitForSecondsMethodDeclarations.h"
#include "mscorlib_System_UInt32.h"
#include "UnityEngine_UnityEngine_WaitForSeconds.h"
#include "mscorlib_System_NotSupportedExceptionMethodDeclarations.h"
#include "mscorlib_System_NotSupportedException.h"
#include "AssemblyU2DCSharp_Car_Health_U3CAbastecerU3Ec__Iterator1.h"
#include "AssemblyU2DCSharp_Car_Health_U3CAbastecerU3Ec__Iterator1MethodDeclarations.h"
#include "UnityEngine_UnityEngine_Coroutine.h"
#include "UnityEngine_UI_UnityEngine_UI_Slider.h"
#include "UnityEngine_UI_UnityEngine_UI_SliderMethodDeclarations.h"
#include "AssemblyU2DCSharp_Funcoes_Botoes.h"
#include "AssemblyU2DCSharp_Funcoes_BotoesMethodDeclarations.h"
#include "UnityEngine_UnityEngine_RectTransform.h"
#include "UnityEngine_UnityEngine_ApplicationMethodDeclarations.h"
#include "AssemblyU2DCSharp_Principal.h"
#include "AssemblyU2DCSharp_PrincipalMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_GraphicMethodDeclarations.h"
#include "UnityEngine_UI_UnityEngine_UI_Text.h"
#include "UnityEngine_UI_UnityEngine_UI_Graphic.h"
#include "UnityEngine_UI_UnityEngine_UI_TextMethodDeclarations.h"

// !!0 UnityEngine.Component::GetComponent<System.Object>()
extern "C" Object_t * Component_GetComponent_TisObject_t_m350_gshared (Component_t78 * __this, const MethodInfo* method);
#define Component_GetComponent_TisObject_t_m350(__this, method) (( Object_t * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Camera>()
#define Component_GetComponent_TisCamera_t74_m425(__this, method) (( Camera_t74 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponent<UnityEngine.Rigidbody>()
#define Component_GetComponent_TisRigidbody_t34_m268(__this, method) (( Rigidbody_t34 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponent_TisObject_t_m350_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<System.Object>()
extern "C" Object_t * Component_GetComponentInChildren_TisObject_t_m353_gshared (Component_t78 * __this, const MethodInfo* method);
#define Component_GetComponentInChildren_TisObject_t_m353(__this, method) (( Object_t * (*) (Component_t78 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m353_gshared)(__this, method)
// !!0 UnityEngine.Component::GetComponentInChildren<UnityEngine.Camera>()
#define Component_GetComponentInChildren_TisCamera_t74_m444(__this, method) (( Camera_t74 * (*) (Component_t78 *, const MethodInfo*))Component_GetComponentInChildren_TisObject_t_m353_gshared)(__this, method)
#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif
// System.Void mset.Logo::.ctor()
extern "C" void Logo__ctor_m354 (Logo_t81 * __this, const MethodInfo* method)
{
	{
		Color_t83  L_0 = Color_get_white_m421(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___color_3 = L_0;
		Vector2_t23  L_1 = {0};
		Vector2__ctor_m257(&L_1, (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___logoPixelOffset_4 = L_1;
		Vector2_t23  L_2 = {0};
		Vector2__ctor_m257(&L_2, (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___logoPercentOffset_5 = L_2;
		__this->___placement_6 = 2;
		Rect_t84  L_3 = {0};
		Rect__ctor_m422(&L_3, (0.0f), (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		__this->___texRect_7 = L_3;
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void mset.Logo::Reset()
extern TypeInfo* Texture2D_t82_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral10;
extern "C" void Logo_Reset_m355 (Logo_t81 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Texture2D_t82_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(41);
		_stringLiteral10 = il2cpp_codegen_string_literal_from_index(10);
		s_Il2CppMethodIntialized = true;
	}
	{
		Object_t62 * L_0 = Resources_Load_m423(NULL /*static, unused*/, _stringLiteral10, /*hidden argument*/NULL);
		__this->___logoTexture_2 = ((Texture2D_t82 *)IsInstSealed(L_0, Texture2D_t82_il2cpp_TypeInfo_var));
		return;
	}
}
// System.Void mset.Logo::Start()
extern "C" void Logo_Start_m356 (Logo_t81 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void mset.Logo::updateTexRect()
extern const MethodInfo* Component_GetComponent_TisCamera_t74_m425_MethodInfo_var;
extern "C" void Logo_updateTexRect_m357 (Logo_t81 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisCamera_t74_m425_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483667);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	float V_1 = 0.0f;
	float V_2 = 0.0f;
	float V_3 = 0.0f;
	float V_4 = 0.0f;
	float V_5 = 0.0f;
	int32_t V_6 = {0};
	{
		Texture2D_t82 * L_0 = (__this->___logoTexture_2);
		bool L_1 = Object_op_Implicit_m424(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_01aa;
		}
	}
	{
		Texture2D_t82 * L_2 = (__this->___logoTexture_2);
		NullCheck(L_2);
		int32_t L_3 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(4 /* System.Int32 UnityEngine.Texture::get_width() */, L_2);
		V_0 = (((float)((float)L_3)));
		Texture2D_t82 * L_4 = (__this->___logoTexture_2);
		NullCheck(L_4);
		int32_t L_5 = (int32_t)VirtFuncInvoker0< int32_t >::Invoke(5 /* System.Int32 UnityEngine.Texture::get_height() */, L_4);
		V_1 = (((float)((float)L_5)));
		V_2 = (0.0f);
		V_3 = (0.0f);
		Camera_t74 * L_6 = Component_GetComponent_TisCamera_t74_m425(__this, /*hidden argument*/Component_GetComponent_TisCamera_t74_m425_MethodInfo_var);
		bool L_7 = Object_op_Implicit_m424(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		Camera_t74 * L_8 = Component_GetComponent_TisCamera_t74_m425(__this, /*hidden argument*/Component_GetComponent_TisCamera_t74_m425_MethodInfo_var);
		NullCheck(L_8);
		int32_t L_9 = Camera_get_pixelWidth_m426(L_8, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_9)));
		Camera_t74 * L_10 = Component_GetComponent_TisCamera_t74_m425(__this, /*hidden argument*/Component_GetComponent_TisCamera_t74_m425_MethodInfo_var);
		NullCheck(L_10);
		int32_t L_11 = Camera_get_pixelHeight_m427(L_10, /*hidden argument*/NULL);
		V_3 = (((float)((float)L_11)));
		goto IL_00a0;
	}

IL_0065:
	{
		Camera_t74 * L_12 = Camera_get_main_m288(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_13 = Object_op_Implicit_m424(NULL /*static, unused*/, L_12, /*hidden argument*/NULL);
		if (!L_13)
		{
			goto IL_0091;
		}
	}
	{
		Camera_t74 * L_14 = Camera_get_main_m288(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_14);
		int32_t L_15 = Camera_get_pixelWidth_m426(L_14, /*hidden argument*/NULL);
		V_2 = (((float)((float)L_15)));
		Camera_t74 * L_16 = Camera_get_main_m288(NULL /*static, unused*/, /*hidden argument*/NULL);
		NullCheck(L_16);
		int32_t L_17 = Camera_get_pixelHeight_m427(L_16, /*hidden argument*/NULL);
		V_3 = (((float)((float)L_17)));
		goto IL_00a0;
	}

IL_0091:
	{
		Camera_t74 * L_18 = Camera_get_current_m428(NULL /*static, unused*/, /*hidden argument*/NULL);
		bool L_19 = Object_op_Implicit_m424(NULL /*static, unused*/, L_18, /*hidden argument*/NULL);
		if (!L_19)
		{
			goto IL_00a0;
		}
	}

IL_00a0:
	{
		Vector2_t23 * L_20 = &(__this->___logoPixelOffset_4);
		float L_21 = (L_20->___x_1);
		Vector2_t23 * L_22 = &(__this->___logoPercentOffset_5);
		float L_23 = (L_22->___x_1);
		float L_24 = V_2;
		V_4 = ((float)((float)L_21+(float)((float)((float)((float)((float)L_23*(float)L_24))*(float)(0.01f)))));
		Vector2_t23 * L_25 = &(__this->___logoPixelOffset_4);
		float L_26 = (L_25->___y_2);
		Vector2_t23 * L_27 = &(__this->___logoPercentOffset_5);
		float L_28 = (L_27->___y_2);
		float L_29 = V_3;
		V_5 = ((float)((float)L_26+(float)((float)((float)((float)((float)L_28*(float)L_29))*(float)(0.01f)))));
		int32_t L_30 = (__this->___placement_6);
		V_6 = L_30;
		int32_t L_31 = V_6;
		if (L_31 == 0)
		{
			goto IL_0106;
		}
		if (L_31 == 1)
		{
			goto IL_0125;
		}
		if (L_31 == 2)
		{
			goto IL_0148;
		}
		if (L_31 == 3)
		{
			goto IL_016b;
		}
	}
	{
		goto IL_0192;
	}

IL_0106:
	{
		Rect_t84 * L_32 = &(__this->___texRect_7);
		float L_33 = V_4;
		Rect_set_x_m429(L_32, L_33, /*hidden argument*/NULL);
		Rect_t84 * L_34 = &(__this->___texRect_7);
		float L_35 = V_5;
		Rect_set_y_m430(L_34, L_35, /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_0125:
	{
		Rect_t84 * L_36 = &(__this->___texRect_7);
		float L_37 = V_2;
		float L_38 = V_4;
		float L_39 = V_0;
		Rect_set_x_m429(L_36, ((float)((float)((float)((float)L_37-(float)L_38))-(float)L_39)), /*hidden argument*/NULL);
		Rect_t84 * L_40 = &(__this->___texRect_7);
		float L_41 = V_5;
		Rect_set_y_m430(L_40, L_41, /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_0148:
	{
		Rect_t84 * L_42 = &(__this->___texRect_7);
		float L_43 = V_4;
		Rect_set_x_m429(L_42, L_43, /*hidden argument*/NULL);
		Rect_t84 * L_44 = &(__this->___texRect_7);
		float L_45 = V_3;
		float L_46 = V_5;
		float L_47 = V_1;
		Rect_set_y_m430(L_44, ((float)((float)((float)((float)L_45-(float)L_46))-(float)L_47)), /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_016b:
	{
		Rect_t84 * L_48 = &(__this->___texRect_7);
		float L_49 = V_2;
		float L_50 = V_4;
		float L_51 = V_0;
		Rect_set_x_m429(L_48, ((float)((float)((float)((float)L_49-(float)L_50))-(float)L_51)), /*hidden argument*/NULL);
		Rect_t84 * L_52 = &(__this->___texRect_7);
		float L_53 = V_3;
		float L_54 = V_5;
		float L_55 = V_1;
		Rect_set_y_m430(L_52, ((float)((float)((float)((float)L_53-(float)L_54))-(float)L_55)), /*hidden argument*/NULL);
		goto IL_0192;
	}

IL_0192:
	{
		Rect_t84 * L_56 = &(__this->___texRect_7);
		float L_57 = V_0;
		Rect_set_width_m431(L_56, L_57, /*hidden argument*/NULL);
		Rect_t84 * L_58 = &(__this->___texRect_7);
		float L_59 = V_1;
		Rect_set_height_m432(L_58, L_59, /*hidden argument*/NULL);
	}

IL_01aa:
	{
		return;
	}
}
// System.Void mset.Logo::OnGUI()
extern TypeInfo* GUI_t99_il2cpp_TypeInfo_var;
extern "C" void Logo_OnGUI_m358 (Logo_t81 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		GUI_t99_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(43);
		s_Il2CppMethodIntialized = true;
	}
	{
		Logo_updateTexRect_m357(__this, /*hidden argument*/NULL);
		Texture2D_t82 * L_0 = (__this->___logoTexture_2);
		bool L_1 = Object_op_Implicit_m424(NULL /*static, unused*/, L_0, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0032;
		}
	}
	{
		Color_t83  L_2 = (__this->___color_3);
		IL2CPP_RUNTIME_CLASS_INIT(GUI_t99_il2cpp_TypeInfo_var);
		GUI_set_color_m433(NULL /*static, unused*/, L_2, /*hidden argument*/NULL);
		Rect_t84  L_3 = (__this->___texRect_7);
		Texture2D_t82 * L_4 = (__this->___logoTexture_2);
		GUI_DrawTexture_m434(NULL /*static, unused*/, L_3, L_4, /*hidden argument*/NULL);
	}

IL_0032:
	{
		return;
	}
}
// System.Void OrbitCamera::.ctor()
extern "C" void OrbitCamera__ctor_m359 (OrbitCamera_t85 * __this, const MethodInfo* method)
{
	{
		__this->___autoRotateSpeed_5 = (1.0f);
		__this->___autoRotateSpeedFast_7 = (5.0f);
		__this->___autoRotateValue_8 = (1.0f);
		__this->___distance_9 = (1.5f);
		__this->___xSpeed_10 = (15.0f);
		__this->___ySpeed_11 = (15.0f);
		__this->___yMinLimit_12 = (-20.0f);
		__this->___yMaxLimit_13 = (80.0f);
		__this->___distanceMin_14 = (1.0f);
		__this->___distanceMax_15 = (3.0f);
		__this->___smoothTime_16 = (2.0f);
		__this->___autoTimer_17 = (5.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void OrbitCamera::Start()
extern const MethodInfo* Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var;
extern "C" void OrbitCamera_Start_m360 (OrbitCamera_t85 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483659);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t12  V_0 = {0};
	{
		bool L_0 = (__this->___autoRotateOn_3);
		__this->___rkeyActive_23 = L_0;
		__this->___autoRotateValue_8 = (1.0f);
		Transform_t33 * L_1 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t12  L_2 = Transform_get_eulerAngles_m318(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		float L_3 = ((&V_0)->___y_2);
		__this->___rotationYAxis_18 = L_3;
		float L_4 = ((&V_0)->___x_1);
		__this->___rotationXAxis_19 = L_4;
		float L_5 = (__this->___autoRotateSpeed_5);
		__this->___originalAutoRotateSpeed_6 = L_5;
		Rigidbody_t34 * L_6 = Component_GetComponent_TisRigidbody_t34_m268(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var);
		bool L_7 = Object_op_Implicit_m424(NULL /*static, unused*/, L_6, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0065;
		}
	}
	{
		Rigidbody_t34 * L_8 = Component_GetComponent_TisRigidbody_t34_m268(__this, /*hidden argument*/Component_GetComponent_TisRigidbody_t34_m268_MethodInfo_var);
		NullCheck(L_8);
		Rigidbody_set_freezeRotation_m435(L_8, 1, /*hidden argument*/NULL);
	}

IL_0065:
	{
		return;
	}
}
// System.Void OrbitCamera::Update()
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral11;
extern "C" void OrbitCamera_Update_m361 (OrbitCamera_t85 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		_stringLiteral11 = il2cpp_codegen_string_literal_from_index(11);
		s_Il2CppMethodIntialized = true;
	}
	{
		bool L_0 = (__this->___autoRotateOn_3);
		if (!L_0)
		{
			goto IL_002b;
		}
	}
	{
		float L_1 = (__this->___velocityX_20);
		float L_2 = (__this->___autoRotateSpeed_5);
		float L_3 = (__this->___autoRotateValue_8);
		float L_4 = Time_get_deltaTime_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		__this->___velocityX_20 = ((float)((float)L_1+(float)((float)((float)((float)((float)L_2*(float)L_3))*(float)L_4))));
	}

IL_002b:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_5 = Input_GetKeyUp_m436(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		if (!L_5)
		{
			goto IL_0058;
		}
	}
	{
		bool L_6 = (__this->___autoRotateOn_3);
		if (L_6)
		{
			goto IL_0058;
		}
	}
	{
		__this->___autoRotateOn_3 = 1;
		__this->___rkeyActive_23 = 1;
		goto IL_0080;
	}

IL_0058:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_7 = Input_GetKeyUp_m436(NULL /*static, unused*/, _stringLiteral11, /*hidden argument*/NULL);
		if (!L_7)
		{
			goto IL_0080;
		}
	}
	{
		bool L_8 = (__this->___autoRotateOn_3);
		if (!L_8)
		{
			goto IL_0080;
		}
	}
	{
		__this->___autoRotateOn_3 = 0;
		__this->___rkeyActive_23 = 0;
	}

IL_0080:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_9 = Input_GetKeyDown_m437(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (!L_9)
		{
			goto IL_00b4;
		}
	}
	{
		bool L_10 = (__this->___faster_22);
		if (L_10)
		{
			goto IL_00b4;
		}
	}
	{
		__this->___faster_22 = 1;
		float L_11 = (__this->___autoRotateSpeedFast_7);
		__this->___autoRotateSpeed_5 = L_11;
		__this->___autoRotateOn_3 = 1;
	}

IL_00b4:
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_12 = Input_GetKeyUp_m438(NULL /*static, unused*/, ((int32_t)304), /*hidden argument*/NULL);
		if (!L_12)
		{
			goto IL_00f3;
		}
	}
	{
		bool L_13 = (__this->___faster_22);
		if (!L_13)
		{
			goto IL_00f3;
		}
	}
	{
		__this->___faster_22 = 0;
		float L_14 = (__this->___originalAutoRotateSpeed_6);
		__this->___autoRotateSpeed_5 = L_14;
		bool L_15 = (__this->___rkeyActive_23);
		if (L_15)
		{
			goto IL_00f3;
		}
	}
	{
		__this->___autoRotateOn_3 = 0;
	}

IL_00f3:
	{
		bool L_16 = (__this->___autoRotateReverse_4);
		if (!L_16)
		{
			goto IL_010e;
		}
	}
	{
		__this->___autoRotateValue_8 = (-1.0f);
		goto IL_0119;
	}

IL_010e:
	{
		__this->___autoRotateValue_8 = (1.0f);
	}

IL_0119:
	{
		return;
	}
}
// System.Void OrbitCamera::LateUpdate()
extern TypeInfo* Input_t70_il2cpp_TypeInfo_var;
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern Il2CppCodeGenString* _stringLiteral12;
extern Il2CppCodeGenString* _stringLiteral13;
extern Il2CppCodeGenString* _stringLiteral14;
extern Il2CppCodeGenString* _stringLiteral15;
extern "C" void OrbitCamera_LateUpdate_m362 (OrbitCamera_t85 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Input_t70_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(19);
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		_stringLiteral12 = il2cpp_codegen_string_literal_from_index(12);
		_stringLiteral13 = il2cpp_codegen_string_literal_from_index(13);
		_stringLiteral14 = il2cpp_codegen_string_literal_from_index(14);
		_stringLiteral15 = il2cpp_codegen_string_literal_from_index(15);
		s_Il2CppMethodIntialized = true;
	}
	Quaternion_t45  V_0 = {0};
	Quaternion_t45  V_1 = {0};
	RaycastHit_t100  V_2 = {0};
	Vector3_t12  V_3 = {0};
	Vector3_t12  V_4 = {0};
	{
		Transform_t33 * L_0 = (__this->___target_2);
		bool L_1 = Object_op_Inequality_m216(NULL /*static, unused*/, L_0, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_01be;
		}
	}
	{
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		bool L_2 = Input_GetMouseButton_m439(NULL /*static, unused*/, 0, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_006b;
		}
	}
	{
		float L_3 = (__this->___velocityX_20);
		float L_4 = (__this->___xSpeed_10);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		float L_5 = Input_GetAxis_m240(NULL /*static, unused*/, _stringLiteral12, /*hidden argument*/NULL);
		float L_6 = (__this->___distance_9);
		__this->___velocityX_20 = ((float)((float)L_3+(float)((float)((float)((float)((float)((float)((float)L_4*(float)L_5))*(float)L_6))*(float)(0.02f)))));
		float L_7 = (__this->___velocityY_21);
		float L_8 = (__this->___ySpeed_11);
		float L_9 = Input_GetAxis_m240(NULL /*static, unused*/, _stringLiteral13, /*hidden argument*/NULL);
		__this->___velocityY_21 = ((float)((float)L_7+(float)((float)((float)((float)((float)L_8*(float)L_9))*(float)(0.02f)))));
	}

IL_006b:
	{
		float L_10 = (__this->___rotationYAxis_18);
		float L_11 = (__this->___velocityX_20);
		__this->___rotationYAxis_18 = ((float)((float)L_10+(float)L_11));
		float L_12 = (__this->___rotationXAxis_19);
		float L_13 = (__this->___velocityY_21);
		__this->___rotationXAxis_19 = ((float)((float)L_12-(float)L_13));
		float L_14 = (__this->___rotationXAxis_19);
		float L_15 = (__this->___yMinLimit_12);
		float L_16 = (__this->___yMaxLimit_13);
		float L_17 = OrbitCamera_ClampAngle_m363(NULL /*static, unused*/, L_14, L_15, L_16, /*hidden argument*/NULL);
		__this->___rotationXAxis_19 = L_17;
		float L_18 = (__this->___rotationXAxis_19);
		float L_19 = (__this->___rotationYAxis_18);
		Quaternion_t45  L_20 = Quaternion_Euler_m328(NULL /*static, unused*/, L_18, L_19, (0.0f), /*hidden argument*/NULL);
		V_0 = L_20;
		Quaternion_t45  L_21 = V_0;
		V_1 = L_21;
		float L_22 = (__this->___distance_9);
		IL2CPP_RUNTIME_CLASS_INIT(Input_t70_il2cpp_TypeInfo_var);
		float L_23 = Input_GetAxis_m240(NULL /*static, unused*/, _stringLiteral14, /*hidden argument*/NULL);
		float L_24 = (__this->___distanceMin_14);
		float L_25 = (__this->___distanceMax_15);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_26 = Mathf_Clamp_m281(NULL /*static, unused*/, ((float)((float)L_22-(float)((float)((float)L_23*(float)(5.0f))))), L_24, L_25, /*hidden argument*/NULL);
		__this->___distance_9 = L_26;
		Transform_t33 * L_27 = (__this->___target_2);
		NullCheck(L_27);
		Vector3_t12  L_28 = Transform_get_position_m224(L_27, /*hidden argument*/NULL);
		Transform_t33 * L_29 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_29);
		Vector3_t12  L_30 = Transform_get_position_m224(L_29, /*hidden argument*/NULL);
		bool L_31 = Physics_Linecast_m440(NULL /*static, unused*/, L_28, L_30, (&V_2), /*hidden argument*/NULL);
		if (!L_31)
		{
			goto IL_012b;
		}
	}
	{
		float L_32 = (__this->___distance_9);
		float L_33 = RaycastHit_get_distance_m441((&V_2), /*hidden argument*/NULL);
		__this->___distance_9 = ((float)((float)L_32-(float)L_33));
	}

IL_012b:
	{
		float L_34 = (__this->___distance_9);
		Vector3__ctor_m230((&V_3), (0.0f), (0.0f), ((-L_34)), /*hidden argument*/NULL);
		Quaternion_t45  L_35 = V_1;
		Vector3_t12  L_36 = V_3;
		Vector3_t12  L_37 = Quaternion_op_Multiply_m321(NULL /*static, unused*/, L_35, L_36, /*hidden argument*/NULL);
		Transform_t33 * L_38 = (__this->___target_2);
		NullCheck(L_38);
		Vector3_t12  L_39 = Transform_get_position_m224(L_38, /*hidden argument*/NULL);
		Vector3_t12  L_40 = Vector3_op_Addition_m279(NULL /*static, unused*/, L_37, L_39, /*hidden argument*/NULL);
		V_4 = L_40;
		Transform_t33 * L_41 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Quaternion_t45  L_42 = V_1;
		NullCheck(L_41);
		Transform_set_rotation_m309(L_41, L_42, /*hidden argument*/NULL);
		Transform_t33 * L_43 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12  L_44 = V_4;
		NullCheck(L_43);
		Transform_set_position_m231(L_43, L_44, /*hidden argument*/NULL);
		float L_45 = (__this->___velocityX_20);
		float L_46 = Time_get_deltaTime_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_47 = (__this->___smoothTime_16);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_48 = Mathf_Lerp_m275(NULL /*static, unused*/, L_45, (0.0f), ((float)((float)L_46*(float)L_47)), /*hidden argument*/NULL);
		__this->___velocityX_20 = L_48;
		float L_49 = (__this->___velocityY_21);
		float L_50 = Time_get_deltaTime_m218(NULL /*static, unused*/, /*hidden argument*/NULL);
		float L_51 = (__this->___smoothTime_16);
		float L_52 = Mathf_Lerp_m275(NULL /*static, unused*/, L_49, (0.0f), ((float)((float)L_50*(float)L_51)), /*hidden argument*/NULL);
		__this->___velocityY_21 = L_52;
		goto IL_01c8;
	}

IL_01be:
	{
		Debug_LogWarning_m341(NULL /*static, unused*/, _stringLiteral15, /*hidden argument*/NULL);
	}

IL_01c8:
	{
		return;
	}
}
// System.Single OrbitCamera::ClampAngle(System.Single,System.Single,System.Single)
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" float OrbitCamera_ClampAngle_m363 (Object_t * __this /* static, unused */, float ___angle, float ___min, float ___max, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	{
		float L_0 = ___angle;
		if ((!(((float)L_0) < ((float)(-360.0f)))))
		{
			goto IL_0014;
		}
	}
	{
		float L_1 = ___angle;
		___angle = ((float)((float)L_1+(float)(360.0f)));
	}

IL_0014:
	{
		float L_2 = ___angle;
		if ((!(((float)L_2) > ((float)(360.0f)))))
		{
			goto IL_0028;
		}
	}
	{
		float L_3 = ___angle;
		___angle = ((float)((float)L_3-(float)(360.0f)));
	}

IL_0028:
	{
		float L_4 = ___angle;
		float L_5 = ___min;
		float L_6 = ___max;
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_7 = Mathf_Clamp_m281(NULL /*static, unused*/, L_4, L_5, L_6, /*hidden argument*/NULL);
		return L_7;
	}
}
// System.Void AtualizaVida::.ctor()
extern "C" void AtualizaVida__ctor_m364 (AtualizaVida_t86 * __this, const MethodInfo* method)
{
	{
		__this->___m_KmLitros_4 = (7.0f);
		__this->___multiplicadorKML_5 = (0.04f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void AtualizaVida::Start()
extern "C" void AtualizaVida_Start_m365 (AtualizaVida_t86 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_KmLitros_4);
		float L_1 = (__this->___multiplicadorKML_5);
		__this->___m_KmLitros_4 = ((float)((float)L_0/(float)L_1));
		return;
	}
}
// System.Void AtualizaVida::Update()
extern "C" void AtualizaVida_Update_m366 (AtualizaVida_t86 * __this, const MethodInfo* method)
{
	{
		CarController_t29 * L_0 = (__this->___m_Carro_2);
		NullCheck(L_0);
		float L_1 = CarController_get_CurrentSpeed_m152(L_0, /*hidden argument*/NULL);
		if ((!(((float)L_1) > ((float)(0.0f)))))
		{
			goto IL_005c;
		}
	}
	{
		CarController_t29 * L_2 = (__this->___m_Carro_2);
		NullCheck(L_2);
		float L_3 = CarController_get_AccelInput_m156(L_2, /*hidden argument*/NULL);
		if ((!(((float)L_3) == ((float)(0.0f)))))
		{
			goto IL_003f;
		}
	}
	{
		CarController_t29 * L_4 = (__this->___m_Carro_2);
		NullCheck(L_4);
		float L_5 = CarController_get_BrakeInput_m149(L_4, /*hidden argument*/NULL);
		if ((!(((float)L_5) > ((float)(0.0f)))))
		{
			goto IL_005c;
		}
	}

IL_003f:
	{
		Car_Health_t87 * L_6 = (__this->___m_CarHealth_3);
		CarController_t29 * L_7 = (__this->___m_Carro_2);
		NullCheck(L_7);
		float L_8 = CarController_get_CurrentSpeed_m152(L_7, /*hidden argument*/NULL);
		float L_9 = (__this->___m_KmLitros_4);
		NullCheck(L_6);
		Car_Health_GastaCombustivel_m401(L_6, ((float)((float)L_8/(float)L_9)), /*hidden argument*/NULL);
	}

IL_005c:
	{
		return;
	}
}
// System.Void AtualizaVida::OnCollisionEnter(UnityEngine.Collision)
extern "C" void AtualizaVida_OnCollisionEnter_m367 (AtualizaVida_t86 * __this, Collision_t58 * ___ObjetoColidido, const MethodInfo* method)
{
	{
		Car_Health_t87 * L_0 = (__this->___m_CarHealth_3);
		CarController_t29 * L_1 = (__this->___m_Carro_2);
		NullCheck(L_1);
		float L_2 = CarController_get_CurrentSpeed_m152(L_1, /*hidden argument*/NULL);
		NullCheck(L_0);
		Car_Health_GastaMecanica_m398(L_0, ((float)((float)L_2*(float)(0.35f))), /*hidden argument*/NULL);
		return;
	}
}
// System.Void AtualizaVida::OnTriggerStay(UnityEngine.Collider)
extern TypeInfo* AtualizaVida_t86_il2cpp_TypeInfo_var;
extern TypeInfo* Dictionary_2_t88_il2cpp_TypeInfo_var;
extern const MethodInfo* Dictionary_2__ctor_m443_MethodInfo_var;
extern Il2CppCodeGenString* _stringLiteral16;
extern Il2CppCodeGenString* _stringLiteral17;
extern "C" void AtualizaVida_OnTriggerStay_m368 (AtualizaVida_t86 * __this, Collider_t98 * ___ObjetoColidido, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		AtualizaVida_t86_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(45);
		Dictionary_2_t88_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(46);
		Dictionary_2__ctor_m443_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483668);
		_stringLiteral16 = il2cpp_codegen_string_literal_from_index(16);
		_stringLiteral17 = il2cpp_codegen_string_literal_from_index(17);
		s_Il2CppMethodIntialized = true;
	}
	String_t* V_0 = {0};
	Dictionary_2_t88 * V_1 = {0};
	int32_t V_2 = 0;
	{
		Collider_t98 * L_0 = ___ObjetoColidido;
		NullCheck(L_0);
		GameObject_t52 * L_1 = Component_get_gameObject_m237(L_0, /*hidden argument*/NULL);
		NullCheck(L_1);
		String_t* L_2 = GameObject_get_tag_m442(L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		String_t* L_3 = V_0;
		if (!L_3)
		{
			goto IL_0085;
		}
	}
	{
		Dictionary_2_t88 * L_4 = ((AtualizaVida_t86_StaticFields*)AtualizaVida_t86_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_6;
		if (L_4)
		{
			goto IL_0041;
		}
	}
	{
		Dictionary_2_t88 * L_5 = (Dictionary_2_t88 *)il2cpp_codegen_object_new (Dictionary_2_t88_il2cpp_TypeInfo_var);
		Dictionary_2__ctor_m443(L_5, 2, /*hidden argument*/Dictionary_2__ctor_m443_MethodInfo_var);
		V_1 = L_5;
		Dictionary_2_t88 * L_6 = V_1;
		NullCheck(L_6);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_6, _stringLiteral16, 0);
		Dictionary_2_t88 * L_7 = V_1;
		NullCheck(L_7);
		VirtActionInvoker2< String_t*, int32_t >::Invoke(27 /* System.Void System.Collections.Generic.Dictionary`2<System.String,System.Int32>::Add(!0,!1) */, L_7, _stringLiteral17, 1);
		Dictionary_2_t88 * L_8 = V_1;
		((AtualizaVida_t86_StaticFields*)AtualizaVida_t86_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_6 = L_8;
	}

IL_0041:
	{
		Dictionary_2_t88 * L_9 = ((AtualizaVida_t86_StaticFields*)AtualizaVida_t86_il2cpp_TypeInfo_var->static_fields)->___U3CU3Ef__switchU24map0_6;
		String_t* L_10 = V_0;
		NullCheck(L_9);
		bool L_11 = (bool)VirtFuncInvoker2< bool, String_t*, int32_t* >::Invoke(32 /* System.Boolean System.Collections.Generic.Dictionary`2<System.String,System.Int32>::TryGetValue(!0,!1&) */, L_9, L_10, (&V_2));
		if (!L_11)
		{
			goto IL_0085;
		}
	}
	{
		int32_t L_12 = V_2;
		if (!L_12)
		{
			goto IL_0065;
		}
	}
	{
		int32_t L_13 = V_2;
		if ((((int32_t)L_13) == ((int32_t)1)))
		{
			goto IL_0075;
		}
	}
	{
		goto IL_0085;
	}

IL_0065:
	{
		Car_Health_t87 * L_14 = (__this->___m_CarHealth_3);
		NullCheck(L_14);
		Car_Health_AddMecanica_m399(L_14, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_0075:
	{
		Car_Health_t87 * L_15 = (__this->___m_CarHealth_3);
		NullCheck(L_15);
		Car_Health_AddCombustivel_m402(L_15, /*hidden argument*/NULL);
		goto IL_0085;
	}

IL_0085:
	{
		return;
	}
}
// System.Void Complete.CameraControl::.ctor()
extern "C" void CameraControl__ctor_m369 (CameraControl_t89 * __this, const MethodInfo* method)
{
	{
		__this->___m_DampTime_2 = (0.2f);
		__this->___m_ScreenEdgeBuffer_3 = (4.0f);
		__this->___m_MinSize_4 = (6.5f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Complete.CameraControl::Awake()
extern const MethodInfo* Component_GetComponentInChildren_TisCamera_t74_m444_MethodInfo_var;
extern "C" void CameraControl_Awake_m370 (CameraControl_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Component_GetComponentInChildren_TisCamera_t74_m444_MethodInfo_var = il2cpp_codegen_method_info_from_index(2147483669);
		s_Il2CppMethodIntialized = true;
	}
	{
		Camera_t74 * L_0 = Component_GetComponentInChildren_TisCamera_t74_m444(__this, /*hidden argument*/Component_GetComponentInChildren_TisCamera_t74_m444_MethodInfo_var);
		__this->___m_Camera_6 = L_0;
		return;
	}
}
// System.Void Complete.CameraControl::FixedUpdate()
extern "C" void CameraControl_FixedUpdate_m371 (CameraControl_t89 * __this, const MethodInfo* method)
{
	{
		CameraControl_Move_m372(__this, /*hidden argument*/NULL);
		CameraControl_Zoom_m374(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Complete.CameraControl::Move()
extern "C" void CameraControl_Move_m372 (CameraControl_t89 * __this, const MethodInfo* method)
{
	{
		CameraControl_FindAveragePosition_m373(__this, /*hidden argument*/NULL);
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Transform_t33 * L_1 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_1);
		Vector3_t12  L_2 = Transform_get_position_m224(L_1, /*hidden argument*/NULL);
		Vector3_t12  L_3 = (__this->___m_DesiredPosition_9);
		Vector3_t12 * L_4 = &(__this->___m_MoveVelocity_8);
		float L_5 = (__this->___m_DampTime_2);
		Vector3_t12  L_6 = Vector3_SmoothDamp_m445(NULL /*static, unused*/, L_2, L_3, L_4, L_5, /*hidden argument*/NULL);
		NullCheck(L_0);
		Transform_set_position_m231(L_0, L_6, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Complete.CameraControl::FindAveragePosition()
extern TypeInfo* Vector3_t12_il2cpp_TypeInfo_var;
extern "C" void CameraControl_FindAveragePosition_m373 (CameraControl_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Vector3_t12_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(47);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t12  V_0 = {0};
	int32_t V_1 = 0;
	int32_t V_2 = 0;
	Vector3_t12  V_3 = {0};
	{
		Initobj (Vector3_t12_il2cpp_TypeInfo_var, (&V_0));
		V_1 = 0;
		V_2 = 0;
		goto IL_0049;
	}

IL_0011:
	{
		TransformU5BU5D_t90* L_0 = (__this->___m_Targets_5);
		int32_t L_1 = V_2;
		NullCheck(L_0);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_0, L_1);
		int32_t L_2 = L_1;
		NullCheck((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_0, L_2, sizeof(Transform_t33 *))));
		GameObject_t52 * L_3 = Component_get_gameObject_m237((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_0, L_2, sizeof(Transform_t33 *))), /*hidden argument*/NULL);
		NullCheck(L_3);
		bool L_4 = GameObject_get_activeSelf_m446(L_3, /*hidden argument*/NULL);
		if (L_4)
		{
			goto IL_002d;
		}
	}
	{
		goto IL_0045;
	}

IL_002d:
	{
		Vector3_t12  L_5 = V_0;
		TransformU5BU5D_t90* L_6 = (__this->___m_Targets_5);
		int32_t L_7 = V_2;
		NullCheck(L_6);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_6, L_7);
		int32_t L_8 = L_7;
		NullCheck((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_6, L_8, sizeof(Transform_t33 *))));
		Vector3_t12  L_9 = Transform_get_position_m224((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_6, L_8, sizeof(Transform_t33 *))), /*hidden argument*/NULL);
		Vector3_t12  L_10 = Vector3_op_Addition_m279(NULL /*static, unused*/, L_5, L_9, /*hidden argument*/NULL);
		V_0 = L_10;
		int32_t L_11 = V_1;
		V_1 = ((int32_t)((int32_t)L_11+(int32_t)1));
	}

IL_0045:
	{
		int32_t L_12 = V_2;
		V_2 = ((int32_t)((int32_t)L_12+(int32_t)1));
	}

IL_0049:
	{
		int32_t L_13 = V_2;
		TransformU5BU5D_t90* L_14 = (__this->___m_Targets_5);
		NullCheck(L_14);
		if ((((int32_t)L_13) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_14)->max_length)))))))
		{
			goto IL_0011;
		}
	}
	{
		int32_t L_15 = V_1;
		if ((((int32_t)L_15) <= ((int32_t)0)))
		{
			goto IL_0067;
		}
	}
	{
		Vector3_t12  L_16 = V_0;
		int32_t L_17 = V_1;
		Vector3_t12  L_18 = Vector3_op_Division_m226(NULL /*static, unused*/, L_16, (((float)((float)L_17))), /*hidden argument*/NULL);
		V_0 = L_18;
	}

IL_0067:
	{
		Transform_t33 * L_19 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		NullCheck(L_19);
		Vector3_t12  L_20 = Transform_get_position_m224(L_19, /*hidden argument*/NULL);
		V_3 = L_20;
		float L_21 = ((&V_3)->___y_2);
		(&V_0)->___y_2 = L_21;
		Vector3_t12  L_22 = V_0;
		__this->___m_DesiredPosition_9 = L_22;
		return;
	}
}
// System.Void Complete.CameraControl::Zoom()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" void CameraControl_Zoom_m374 (CameraControl_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	float V_0 = 0.0f;
	{
		float L_0 = CameraControl_FindRequiredSize_m375(__this, /*hidden argument*/NULL);
		V_0 = L_0;
		Camera_t74 * L_1 = (__this->___m_Camera_6);
		Camera_t74 * L_2 = (__this->___m_Camera_6);
		NullCheck(L_2);
		float L_3 = Camera_get_orthographicSize_m447(L_2, /*hidden argument*/NULL);
		float L_4 = V_0;
		float* L_5 = &(__this->___m_ZoomSpeed_7);
		float L_6 = (__this->___m_DampTime_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_7 = Mathf_SmoothDamp_m448(NULL /*static, unused*/, L_3, L_4, L_5, L_6, /*hidden argument*/NULL);
		NullCheck(L_1);
		Camera_set_orthographicSize_m449(L_1, L_7, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Complete.CameraControl::FindRequiredSize()
extern TypeInfo* Mathf_t63_il2cpp_TypeInfo_var;
extern "C" float CameraControl_FindRequiredSize_m375 (CameraControl_t89 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		Mathf_t63_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(7);
		s_Il2CppMethodIntialized = true;
	}
	Vector3_t12  V_0 = {0};
	float V_1 = 0.0f;
	int32_t V_2 = 0;
	Vector3_t12  V_3 = {0};
	Vector3_t12  V_4 = {0};
	{
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12  L_1 = (__this->___m_DesiredPosition_9);
		NullCheck(L_0);
		Vector3_t12  L_2 = Transform_InverseTransformPoint_m282(L_0, L_1, /*hidden argument*/NULL);
		V_0 = L_2;
		V_1 = (0.0f);
		V_2 = 0;
		goto IL_0093;
	}

IL_001f:
	{
		TransformU5BU5D_t90* L_3 = (__this->___m_Targets_5);
		int32_t L_4 = V_2;
		NullCheck(L_3);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_3, L_4);
		int32_t L_5 = L_4;
		NullCheck((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_3, L_5, sizeof(Transform_t33 *))));
		GameObject_t52 * L_6 = Component_get_gameObject_m237((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_3, L_5, sizeof(Transform_t33 *))), /*hidden argument*/NULL);
		NullCheck(L_6);
		bool L_7 = GameObject_get_activeSelf_m446(L_6, /*hidden argument*/NULL);
		if (L_7)
		{
			goto IL_003b;
		}
	}
	{
		goto IL_008f;
	}

IL_003b:
	{
		Transform_t33 * L_8 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		TransformU5BU5D_t90* L_9 = (__this->___m_Targets_5);
		int32_t L_10 = V_2;
		NullCheck(L_9);
		IL2CPP_ARRAY_BOUNDS_CHECK(L_9, L_10);
		int32_t L_11 = L_10;
		NullCheck((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_9, L_11, sizeof(Transform_t33 *))));
		Vector3_t12  L_12 = Transform_get_position_m224((*(Transform_t33 **)(Transform_t33 **)SZArrayLdElema(L_9, L_11, sizeof(Transform_t33 *))), /*hidden argument*/NULL);
		NullCheck(L_8);
		Vector3_t12  L_13 = Transform_InverseTransformPoint_m282(L_8, L_12, /*hidden argument*/NULL);
		V_3 = L_13;
		Vector3_t12  L_14 = V_3;
		Vector3_t12  L_15 = V_0;
		Vector3_t12  L_16 = Vector3_op_Subtraction_m225(NULL /*static, unused*/, L_14, L_15, /*hidden argument*/NULL);
		V_4 = L_16;
		float L_17 = V_1;
		float L_18 = ((&V_4)->___y_2);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_19 = fabsf(L_18);
		float L_20 = Mathf_Max_m274(NULL /*static, unused*/, L_17, L_19, /*hidden argument*/NULL);
		V_1 = L_20;
		float L_21 = V_1;
		float L_22 = ((&V_4)->___x_1);
		float L_23 = fabsf(L_22);
		Camera_t74 * L_24 = (__this->___m_Camera_6);
		NullCheck(L_24);
		float L_25 = Camera_get_aspect_m450(L_24, /*hidden argument*/NULL);
		float L_26 = Mathf_Max_m274(NULL /*static, unused*/, L_21, ((float)((float)L_23/(float)L_25)), /*hidden argument*/NULL);
		V_1 = L_26;
	}

IL_008f:
	{
		int32_t L_27 = V_2;
		V_2 = ((int32_t)((int32_t)L_27+(int32_t)1));
	}

IL_0093:
	{
		int32_t L_28 = V_2;
		TransformU5BU5D_t90* L_29 = (__this->___m_Targets_5);
		NullCheck(L_29);
		if ((((int32_t)L_28) < ((int32_t)(((int32_t)((int32_t)(((Array_t *)L_29)->max_length)))))))
		{
			goto IL_001f;
		}
	}
	{
		float L_30 = V_1;
		float L_31 = (__this->___m_ScreenEdgeBuffer_3);
		V_1 = ((float)((float)L_30+(float)L_31));
		float L_32 = V_1;
		float L_33 = (__this->___m_MinSize_4);
		IL2CPP_RUNTIME_CLASS_INIT(Mathf_t63_il2cpp_TypeInfo_var);
		float L_34 = Mathf_Max_m274(NULL /*static, unused*/, L_32, L_33, /*hidden argument*/NULL);
		V_1 = L_34;
		float L_35 = V_1;
		return L_35;
	}
}
// System.Void Complete.CameraControl::SetStartPositionAndSize()
extern "C" void CameraControl_SetStartPositionAndSize_m376 (CameraControl_t89 * __this, const MethodInfo* method)
{
	{
		CameraControl_FindAveragePosition_m373(__this, /*hidden argument*/NULL);
		Transform_t33 * L_0 = Component_get_transform_m223(__this, /*hidden argument*/NULL);
		Vector3_t12  L_1 = (__this->___m_DesiredPosition_9);
		NullCheck(L_0);
		Transform_set_position_m231(L_0, L_1, /*hidden argument*/NULL);
		Camera_t74 * L_2 = (__this->___m_Camera_6);
		float L_3 = CameraControl_FindRequiredSize_m375(__this, /*hidden argument*/NULL);
		NullCheck(L_2);
		Camera_set_orthographicSize_m449(L_2, L_3, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Car_Health/<Arrumar>c__Iterator0::.ctor()
extern "C" void U3CArrumarU3Ec__Iterator0__ctor_m377 (U3CArrumarU3Ec__Iterator0_t91 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Car_Health/<Arrumar>c__Iterator0::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CArrumarU3Ec__Iterator0_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m378 (U3CArrumarU3Ec__Iterator0_t91 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object Car_Health/<Arrumar>c__Iterator0::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CArrumarU3Ec__Iterator0_System_Collections_IEnumerator_get_Current_m379 (U3CArrumarU3Ec__Iterator0_t91 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean Car_Health/<Arrumar>c__Iterator0::MoveNext()
extern TypeInfo* WaitForSeconds_t101_il2cpp_TypeInfo_var;
extern "C" bool U3CArrumarU3Ec__Iterator0_MoveNext_m380 (U3CArrumarU3Ec__Iterator0_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_0073;
	}

IL_0021:
	{
		Car_Health_t87 * L_2 = (__this->___U3CU3Ef__this_2);
		Car_Health_t87 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = (L_3->___m_MecanicaAtual_7);
		NullCheck(L_3);
		L_3->___m_MecanicaAtual_7 = ((float)((float)L_4+(float)(0.25f)));
		Car_Health_t87 * L_5 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_5);
		L_5->___m_AguardarMecanica_12 = 1;
		WaitForSeconds_t101 * L_6 = (WaitForSeconds_t101 *)il2cpp_codegen_object_new (WaitForSeconds_t101_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m451(L_6, (0.1f), /*hidden argument*/NULL);
		__this->___U24current_1 = L_6;
		__this->___U24PC_0 = 1;
		goto IL_0075;
	}

IL_0060:
	{
		Car_Health_t87 * L_7 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_7);
		L_7->___m_AguardarMecanica_12 = 0;
		__this->___U24PC_0 = (-1);
	}

IL_0073:
	{
		return 0;
	}

IL_0075:
	{
		return 1;
	}
	// Dead block : IL_0077: ldloc.1
}
// System.Void Car_Health/<Arrumar>c__Iterator0::Dispose()
extern "C" void U3CArrumarU3Ec__Iterator0_Dispose_m381 (U3CArrumarU3Ec__Iterator0_t91 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void Car_Health/<Arrumar>c__Iterator0::Reset()
extern TypeInfo* NotSupportedException_t76_il2cpp_TypeInfo_var;
extern "C" void U3CArrumarU3Ec__Iterator0_Reset_m382 (U3CArrumarU3Ec__Iterator0_t91 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t76_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t76 * L_0 = (NotSupportedException_t76 *)il2cpp_codegen_object_new (NotSupportedException_t76_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m333(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Car_Health/<Abastecer>c__Iterator1::.ctor()
extern "C" void U3CAbastecerU3Ec__Iterator1__ctor_m383 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method)
{
	{
		Object__ctor_m220(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Object Car_Health/<Abastecer>c__Iterator1::System.Collections.Generic.IEnumerator<object>.get_Current()
extern "C" Object_t * U3CAbastecerU3Ec__Iterator1_System_Collections_Generic_IEnumeratorU3CobjectU3E_get_Current_m384 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Object Car_Health/<Abastecer>c__Iterator1::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * U3CAbastecerU3Ec__Iterator1_System_Collections_IEnumerator_get_Current_m385 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method)
{
	{
		Object_t * L_0 = (__this->___U24current_1);
		return L_0;
	}
}
// System.Boolean Car_Health/<Abastecer>c__Iterator1::MoveNext()
extern TypeInfo* WaitForSeconds_t101_il2cpp_TypeInfo_var;
extern "C" bool U3CAbastecerU3Ec__Iterator1_MoveNext_m386 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		WaitForSeconds_t101_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(48);
		s_Il2CppMethodIntialized = true;
	}
	uint32_t V_0 = 0;
	bool V_1 = false;
	{
		int32_t L_0 = (__this->___U24PC_0);
		V_0 = L_0;
		__this->___U24PC_0 = (-1);
		uint32_t L_1 = V_0;
		if (L_1 == 0)
		{
			goto IL_0021;
		}
		if (L_1 == 1)
		{
			goto IL_0060;
		}
	}
	{
		goto IL_0073;
	}

IL_0021:
	{
		Car_Health_t87 * L_2 = (__this->___U3CU3Ef__this_2);
		Car_Health_t87 * L_3 = L_2;
		NullCheck(L_3);
		float L_4 = (L_3->___m_CombustivelAtual_6);
		NullCheck(L_3);
		L_3->___m_CombustivelAtual_6 = ((float)((float)L_4+(float)(0.25f)));
		Car_Health_t87 * L_5 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_5);
		L_5->___m_AguardarCombustivel_11 = 1;
		WaitForSeconds_t101 * L_6 = (WaitForSeconds_t101 *)il2cpp_codegen_object_new (WaitForSeconds_t101_il2cpp_TypeInfo_var);
		WaitForSeconds__ctor_m451(L_6, (0.1f), /*hidden argument*/NULL);
		__this->___U24current_1 = L_6;
		__this->___U24PC_0 = 1;
		goto IL_0075;
	}

IL_0060:
	{
		Car_Health_t87 * L_7 = (__this->___U3CU3Ef__this_2);
		NullCheck(L_7);
		L_7->___m_AguardarCombustivel_11 = 0;
		__this->___U24PC_0 = (-1);
	}

IL_0073:
	{
		return 0;
	}

IL_0075:
	{
		return 1;
	}
	// Dead block : IL_0077: ldloc.1
}
// System.Void Car_Health/<Abastecer>c__Iterator1::Dispose()
extern "C" void U3CAbastecerU3Ec__Iterator1_Dispose_m387 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method)
{
	{
		__this->___U24PC_0 = (-1);
		return;
	}
}
// System.Void Car_Health/<Abastecer>c__Iterator1::Reset()
extern TypeInfo* NotSupportedException_t76_il2cpp_TypeInfo_var;
extern "C" void U3CAbastecerU3Ec__Iterator1_Reset_m388 (U3CAbastecerU3Ec__Iterator1_t92 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		NotSupportedException_t76_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(37);
		s_Il2CppMethodIntialized = true;
	}
	{
		NotSupportedException_t76 * L_0 = (NotSupportedException_t76 *)il2cpp_codegen_object_new (NotSupportedException_t76_il2cpp_TypeInfo_var);
		NotSupportedException__ctor_m333(L_0, /*hidden argument*/NULL);
		il2cpp_codegen_raise_exception((Il2CppCodeGenException*)L_0);
	}
}
// System.Void Car_Health::.ctor()
extern "C" void Car_Health__ctor_m389 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		__this->___m_CombutivelMax_2 = (50.0f);
		__this->___m_MecanicaMax_3 = (50.0f);
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Single Car_Health::GetCombustivelAtual()
extern "C" float Car_Health_GetCombustivelAtual_m390 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_CombustivelAtual_6);
		return L_0;
	}
}
// System.Single Car_Health::GetMecanicaAtual()
extern "C" float Car_Health_GetMecanicaAtual_m391 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MecanicaAtual_7);
		return L_0;
	}
}
// System.Boolean Car_Health::IsMorto()
extern "C" bool Car_Health_IsMorto_m392 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_Morto_8);
		return L_0;
	}
}
// System.Boolean Car_Health::IsCombustivelBaixo()
extern "C" bool Car_Health_IsCombustivelBaixo_m393 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_CombustivelBaixo_9);
		return L_0;
	}
}
// System.Boolean Car_Health::IsMecanicaBaixa()
extern "C" bool Car_Health_IsMecanicaBaixa_m394 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		bool L_0 = (__this->___m_MecanicaBaixa_10);
		return L_0;
	}
}
// System.Void Car_Health::Start()
extern "C" void Car_Health_Start_m395 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Car_Health::OnEnable()
extern "C" void Car_Health_OnEnable_m396 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_CombutivelMax_2);
		__this->___m_CombustivelAtual_6 = L_0;
		float L_1 = (__this->___m_MecanicaMax_3);
		__this->___m_MecanicaAtual_7 = L_1;
		__this->___m_Morto_8 = 0;
		Car_Health_SetHealthUI_m404(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Car_Health::Update()
extern "C" void Car_Health_Update_m397 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		Car_Health_SetHealthUI_m404(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Car_Health::GastaMecanica(System.Single)
extern "C" void Car_Health_GastaMecanica_m398 (Car_Health_t87 * __this, float ___quantidade, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MecanicaAtual_7);
		if ((!(((float)L_0) < ((float)(11.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		bool L_1 = (__this->___m_MecanicaBaixa_10);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		__this->___m_MecanicaBaixa_10 = 1;
	}

IL_0022:
	{
		float L_2 = (__this->___m_MecanicaAtual_7);
		if ((!(((float)L_2) == ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		bool L_3 = (__this->___m_Morto_8);
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->___m_Morto_8 = 1;
	}

IL_0044:
	{
		float L_4 = (__this->___m_MecanicaAtual_7);
		float L_5 = ___quantidade;
		__this->___m_MecanicaAtual_7 = ((float)((float)L_4-(float)L_5));
		return;
	}
}
// System.Void Car_Health::AddMecanica()
extern "C" void Car_Health_AddMecanica_m399 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_MecanicaAtual_7);
		float L_1 = (__this->___m_MecanicaMax_3);
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_2 = (__this->___m_AguardarMecanica_12);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = Car_Health_Arrumar_m400(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m346(__this, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.IEnumerator Car_Health::Arrumar()
extern TypeInfo* U3CArrumarU3Ec__Iterator0_t91_il2cpp_TypeInfo_var;
extern "C" Object_t * Car_Health_Arrumar_m400 (Car_Health_t87 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CArrumarU3Ec__Iterator0_t91_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(49);
		s_Il2CppMethodIntialized = true;
	}
	U3CArrumarU3Ec__Iterator0_t91 * V_0 = {0};
	{
		U3CArrumarU3Ec__Iterator0_t91 * L_0 = (U3CArrumarU3Ec__Iterator0_t91 *)il2cpp_codegen_object_new (U3CArrumarU3Ec__Iterator0_t91_il2cpp_TypeInfo_var);
		U3CArrumarU3Ec__Iterator0__ctor_m377(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CArrumarU3Ec__Iterator0_t91 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CArrumarU3Ec__Iterator0_t91 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Car_Health::GastaCombustivel(System.Single)
extern "C" void Car_Health_GastaCombustivel_m401 (Car_Health_t87 * __this, float ___quantidade, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_CombustivelAtual_6);
		if ((!(((float)L_0) < ((float)(11.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		bool L_1 = (__this->___m_CombustivelBaixo_9);
		if (L_1)
		{
			goto IL_0022;
		}
	}
	{
		__this->___m_CombustivelBaixo_9 = 1;
	}

IL_0022:
	{
		float L_2 = (__this->___m_CombustivelAtual_6);
		if ((!(((float)L_2) == ((float)(0.0f)))))
		{
			goto IL_0044;
		}
	}
	{
		bool L_3 = (__this->___m_Morto_8);
		if (L_3)
		{
			goto IL_0044;
		}
	}
	{
		__this->___m_Morto_8 = 1;
	}

IL_0044:
	{
		float L_4 = (__this->___m_CombustivelAtual_6);
		float L_5 = ___quantidade;
		__this->___m_CombustivelAtual_6 = ((float)((float)L_4-(float)L_5));
		return;
	}
}
// System.Void Car_Health::AddCombustivel()
extern "C" void Car_Health_AddCombustivel_m402 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		float L_0 = (__this->___m_CombustivelAtual_6);
		float L_1 = (__this->___m_CombutivelMax_2);
		if ((!(((float)L_0) < ((float)L_1))))
		{
			goto IL_0029;
		}
	}
	{
		bool L_2 = (__this->___m_AguardarCombustivel_11);
		if (L_2)
		{
			goto IL_0029;
		}
	}
	{
		Object_t * L_3 = Car_Health_Abastecer_m403(__this, /*hidden argument*/NULL);
		MonoBehaviour_StartCoroutine_m346(__this, L_3, /*hidden argument*/NULL);
	}

IL_0029:
	{
		return;
	}
}
// System.Collections.IEnumerator Car_Health::Abastecer()
extern TypeInfo* U3CAbastecerU3Ec__Iterator1_t92_il2cpp_TypeInfo_var;
extern "C" Object_t * Car_Health_Abastecer_m403 (Car_Health_t87 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		U3CAbastecerU3Ec__Iterator1_t92_il2cpp_TypeInfo_var = il2cpp_codegen_type_info_from_index(50);
		s_Il2CppMethodIntialized = true;
	}
	U3CAbastecerU3Ec__Iterator1_t92 * V_0 = {0};
	{
		U3CAbastecerU3Ec__Iterator1_t92 * L_0 = (U3CAbastecerU3Ec__Iterator1_t92 *)il2cpp_codegen_object_new (U3CAbastecerU3Ec__Iterator1_t92_il2cpp_TypeInfo_var);
		U3CAbastecerU3Ec__Iterator1__ctor_m383(L_0, /*hidden argument*/NULL);
		V_0 = L_0;
		U3CAbastecerU3Ec__Iterator1_t92 * L_1 = V_0;
		NullCheck(L_1);
		L_1->___U3CU3Ef__this_2 = __this;
		U3CAbastecerU3Ec__Iterator1_t92 * L_2 = V_0;
		return L_2;
	}
}
// System.Void Car_Health::SetHealthUI()
extern "C" void Car_Health_SetHealthUI_m404 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		Slider_t93 * L_0 = (__this->___m_BarraCombustivel_4);
		float L_1 = (__this->___m_CombustivelAtual_6);
		NullCheck(L_0);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_0, L_1);
		Slider_t93 * L_2 = (__this->___m_BarraMecanica_5);
		float L_3 = (__this->___m_MecanicaAtual_7);
		NullCheck(L_2);
		VirtActionInvoker1< float >::Invoke(46 /* System.Void UnityEngine.UI.Slider::set_value(System.Single) */, L_2, L_3);
		return;
	}
}
// System.Void Car_Health::OnDeath()
extern "C" void Car_Health_OnDeath_m405 (Car_Health_t87 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Funcoes_Botoes::.ctor()
extern "C" void Funcoes_Botoes__ctor_m406 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Funcoes_Botoes::Start()
extern "C" void Funcoes_Botoes_Start_m407 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	{
		RectTransform_t95 * L_0 = (__this->___m_MenuPausa_2);
		bool L_1 = Object_op_Inequality_m216(NULL /*static, unused*/, L_0, (Object_t62 *)NULL, /*hidden argument*/NULL);
		if (!L_1)
		{
			goto IL_0030;
		}
	}
	{
		RectTransform_t95 * L_2 = (__this->___m_MenuPausa_2);
		Vector3_t12  L_3 = {0};
		Vector3__ctor_m230(&L_3, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_2);
		Transform_set_localScale_m452(L_2, L_3, /*hidden argument*/NULL);
	}

IL_0030:
	{
		return;
	}
}
// System.Void Funcoes_Botoes::Update()
extern "C" void Funcoes_Botoes_Update_m408 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Funcoes_Botoes::Reiniciar()
extern Il2CppCodeGenString* _stringLiteral18;
extern "C" void Funcoes_Botoes_Reiniciar_m409 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral18 = il2cpp_codegen_string_literal_from_index(18);
		s_Il2CppMethodIntialized = true;
	}
	{
		Application_LoadLevel_m453(NULL /*static, unused*/, _stringLiteral18, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Funcoes_Botoes::Sair()
extern "C" void Funcoes_Botoes_Sair_m410 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	{
		Application_Quit_m454(NULL /*static, unused*/, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Funcoes_Botoes::VoltarInicio()
extern "C" void Funcoes_Botoes_VoltarInicio_m411 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Funcoes_Botoes::Pausar()
extern "C" void Funcoes_Botoes_Pausar_m412 (Funcoes_Botoes_t94 * __this, const MethodInfo* method)
{
	{
		float L_0 = Time_get_timeScale_m455(NULL /*static, unused*/, /*hidden argument*/NULL);
		if ((((float)L_0) == ((float)(0.0f))))
		{
			goto IL_003d;
		}
	}
	{
		Time_set_timeScale_m456(NULL /*static, unused*/, (0.0f), /*hidden argument*/NULL);
		RectTransform_t95 * L_1 = (__this->___m_MenuPausa_2);
		Vector3_t12  L_2 = {0};
		Vector3__ctor_m230(&L_2, (1.0f), (1.0f), (1.0f), /*hidden argument*/NULL);
		NullCheck(L_1);
		Transform_set_localScale_m452(L_1, L_2, /*hidden argument*/NULL);
		goto IL_0066;
	}

IL_003d:
	{
		Time_set_timeScale_m456(NULL /*static, unused*/, (1.0f), /*hidden argument*/NULL);
		RectTransform_t95 * L_3 = (__this->___m_MenuPausa_2);
		Vector3_t12  L_4 = {0};
		Vector3__ctor_m230(&L_4, (0.0f), (0.0f), (0.0f), /*hidden argument*/NULL);
		NullCheck(L_3);
		Transform_set_localScale_m452(L_3, L_4, /*hidden argument*/NULL);
	}

IL_0066:
	{
		return;
	}
}
// System.Void Principal::.ctor()
extern "C" void Principal__ctor_m413 (Principal_t96 * __this, const MethodInfo* method)
{
	{
		MonoBehaviour__ctor_m212(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Principal::Start()
extern "C" void Principal_Start_m414 (Principal_t96 * __this, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Principal::Update()
extern "C" void Principal_Update_m415 (Principal_t96 * __this, const MethodInfo* method)
{
	{
		Principal_CombustivelBaixo_m416(__this, /*hidden argument*/NULL);
		Principal_MecanicaBaixa_m417(__this, /*hidden argument*/NULL);
		return;
	}
}
// System.Void Principal::CombustivelBaixo()
extern Il2CppCodeGenString* _stringLiteral19;
extern "C" void Principal_CombustivelBaixo_m416 (Principal_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral19 = il2cpp_codegen_string_literal_from_index(19);
		s_Il2CppMethodIntialized = true;
	}
	{
		AtualizaVida_t86 * L_0 = (__this->___m_Vidas_6);
		NullCheck(L_0);
		Car_Health_t87 * L_1 = (L_0->___m_CarHealth_3);
		NullCheck(L_1);
		bool L_2 = Car_Health_IsCombustivelBaixo_m393(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Principal_ExibirMesagemAlerta_m419(__this, _stringLiteral19, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Principal::MecanicaBaixa()
extern Il2CppCodeGenString* _stringLiteral20;
extern "C" void Principal_MecanicaBaixa_m417 (Principal_t96 * __this, const MethodInfo* method)
{
	static bool s_Il2CppMethodIntialized;
	if (!s_Il2CppMethodIntialized)
	{
		_stringLiteral20 = il2cpp_codegen_string_literal_from_index(20);
		s_Il2CppMethodIntialized = true;
	}
	{
		AtualizaVida_t86 * L_0 = (__this->___m_Vidas_6);
		NullCheck(L_0);
		Car_Health_t87 * L_1 = (L_0->___m_CarHealth_3);
		NullCheck(L_1);
		bool L_2 = Car_Health_IsMecanicaBaixa_m394(L_1, /*hidden argument*/NULL);
		if (!L_2)
		{
			goto IL_0020;
		}
	}
	{
		Principal_ExibirMesagemAlerta_m419(__this, _stringLiteral20, /*hidden argument*/NULL);
	}

IL_0020:
	{
		return;
	}
}
// System.Void Principal::ExibirMesagemPadrao(System.String)
extern "C" void Principal_ExibirMesagemPadrao_m418 (Principal_t96 * __this, String_t* ___mensagem, const MethodInfo* method)
{
	{
		return;
	}
}
// System.Void Principal::ExibirMesagemAlerta(System.String)
extern "C" void Principal_ExibirMesagemAlerta_m419 (Principal_t96 * __this, String_t* ___mensagem, const MethodInfo* method)
{
	{
		Text_t97 * L_0 = (__this->___m_MesagemTexto_5);
		Color_t83  L_1 = (__this->___m_CorMesagemAlerta_3);
		NullCheck(L_0);
		Graphic_set_color_m457(L_0, L_1, /*hidden argument*/NULL);
		Text_t97 * L_2 = (__this->___m_MesagemTexto_5);
		String_t* L_3 = ___mensagem;
		NullCheck(L_2);
		VirtActionInvoker1< String_t* >::Invoke(65 /* System.Void UnityEngine.UI.Text::set_text(System.String) */, L_2, L_3);
		return;
	}
}
// System.Void Principal::ExibirMesagemPerigo(System.String)
extern "C" void Principal_ExibirMesagemPerigo_m420 (Principal_t96 * __this, String_t* ___mensagem, const MethodInfo* method)
{
	{
		return;
	}
}
#ifdef __clang__
#pragma clang diagnostic pop
#endif
