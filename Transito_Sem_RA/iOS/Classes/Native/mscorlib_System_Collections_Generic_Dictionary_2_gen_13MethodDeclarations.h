﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.Generic.Dictionary`2<System.Int32,System.Object>
struct Dictionary_2_t1950;
// System.Collections.Generic.IEqualityComparer`1<System.Int32>
struct IEqualityComparer_1_t1949;
// System.Runtime.Serialization.SerializationInfo
struct SerializationInfo_t652;
// System.Object
struct Object_t;
// System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>[]
struct KeyValuePair_2U5BU5D_t2451;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.Int32,System.Object>>
struct IEnumerator_1_t2452;
// System.Collections.IDictionaryEnumerator
struct IDictionaryEnumerator_t869;
// System.Collections.Generic.Dictionary`2/ValueCollection<System.Int32,System.Object>
struct ValueCollection_t1954;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Runtime_Serialization_StreamingContext.h"
#include "mscorlib_System_Collections_Generic_KeyValuePair_2_gen_5.h"
#include "mscorlib_System_Collections_Generic_Dictionary_2_Enumerator__5.h"
#include "mscorlib_System_Collections_DictionaryEntry.h"

// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor()
extern "C" void Dictionary_2__ctor_m12665_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2__ctor_m12665(__this, method) (( void (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2__ctor_m12665_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2__ctor_m12667_gshared (Dictionary_2_t1950 * __this, Object_t* ___comparer, const MethodInfo* method);
#define Dictionary_2__ctor_m12667(__this, ___comparer, method) (( void (*) (Dictionary_2_t1950 *, Object_t*, const MethodInfo*))Dictionary_2__ctor_m12667_gshared)(__this, ___comparer, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Int32)
extern "C" void Dictionary_2__ctor_m12669_gshared (Dictionary_2_t1950 * __this, int32_t ___capacity, const MethodInfo* method);
#define Dictionary_2__ctor_m12669(__this, ___capacity, method) (( void (*) (Dictionary_2_t1950 *, int32_t, const MethodInfo*))Dictionary_2__ctor_m12669_gshared)(__this, ___capacity, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::.ctor(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2__ctor_m12671_gshared (Dictionary_2_t1950 * __this, SerializationInfo_t652 * ___info, StreamingContext_t653  ___context, const MethodInfo* method);
#define Dictionary_2__ctor_m12671(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1950 *, SerializationInfo_t652 *, StreamingContext_t653 , const MethodInfo*))Dictionary_2__ctor_m12671_gshared)(__this, ___info, ___context, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.get_Item(System.Object)
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_get_Item_m12673_gshared (Dictionary_2_t1950 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_get_Item_m12673(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_get_Item_m12673_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.set_Item(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_set_Item_m12675_gshared (Dictionary_2_t1950 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_set_Item_m12675(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1950 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_set_Item_m12675_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Add(System.Object,System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Add_m12677_gshared (Dictionary_2_t1950 * __this, Object_t * ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Add_m12677(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1950 *, Object_t *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Add_m12677_gshared)(__this, ___key, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Contains(System.Object)
extern "C" bool Dictionary_2_System_Collections_IDictionary_Contains_m12679_gshared (Dictionary_2_t1950 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Contains_m12679(__this, ___key, method) (( bool (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Contains_m12679_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.Remove(System.Object)
extern "C" void Dictionary_2_System_Collections_IDictionary_Remove_m12681_gshared (Dictionary_2_t1950 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_Remove_m12681(__this, ___key, method) (( void (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_Remove_m12681_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12683_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12683(__this, method) (( bool (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_IsSynchronized_m12683_gshared)(__this, method)
// System.Object System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12685_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12685(__this, method) (( Object_t * (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_System_Collections_ICollection_get_SyncRoot_m12685_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.get_IsReadOnly()
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12687_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12687(__this, method) (( bool (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_get_IsReadOnly_m12687_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Add(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12689_gshared (Dictionary_2_t1950 * __this, KeyValuePair_2_t1952  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12689(__this, ___keyValuePair, method) (( void (*) (Dictionary_2_t1950 *, KeyValuePair_2_t1952 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Add_m12689_gshared)(__this, ___keyValuePair, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Contains(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12691_gshared (Dictionary_2_t1950 * __this, KeyValuePair_2_t1952  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12691(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1950 *, KeyValuePair_2_t1952 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Contains_m12691_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12693_gshared (Dictionary_2_t1950 * __this, KeyValuePair_2U5BU5D_t2451* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12693(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1950 *, KeyValuePair_2U5BU5D_t2451*, int32_t, const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_CopyTo_m12693_gshared)(__this, ___array, ___index, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.ICollection<System.Collections.Generic.KeyValuePair<TKey,TValue>>.Remove(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12695_gshared (Dictionary_2_t1950 * __this, KeyValuePair_2_t1952  ___keyValuePair, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12695(__this, ___keyValuePair, method) (( bool (*) (Dictionary_2_t1950 *, KeyValuePair_2_t1952 , const MethodInfo*))Dictionary_2_System_Collections_Generic_ICollectionU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_Remove_m12695_gshared)(__this, ___keyValuePair, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Dictionary_2_System_Collections_ICollection_CopyTo_m12697_gshared (Dictionary_2_t1950 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_System_Collections_ICollection_CopyTo_m12697(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1950 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_System_Collections_ICollection_CopyTo_m12697_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12699_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12699(__this, method) (( Object_t * (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_System_Collections_IEnumerable_GetEnumerator_m12699_gshared)(__this, method)
// System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<TKey,TValue>> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.Generic.IEnumerable<System.Collections.Generic.KeyValuePair<TKey,TValue>>.GetEnumerator()
extern "C" Object_t* Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12701_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12701(__this, method) (( Object_t* (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_System_Collections_Generic_IEnumerableU3CSystem_Collections_Generic_KeyValuePairU3CTKeyU2CTValueU3EU3E_GetEnumerator_m12701_gshared)(__this, method)
// System.Collections.IDictionaryEnumerator System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::System.Collections.IDictionary.GetEnumerator()
extern "C" Object_t * Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12703_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12703(__this, method) (( Object_t * (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_System_Collections_IDictionary_GetEnumerator_m12703_gshared)(__this, method)
// System.Int32 System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Count()
extern "C" int32_t Dictionary_2_get_Count_m12705_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_get_Count_m12705(__this, method) (( int32_t (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_get_Count_m12705_gshared)(__this, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Item(TKey)
extern "C" Object_t * Dictionary_2_get_Item_m12707_gshared (Dictionary_2_t1950 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_get_Item_m12707(__this, ___key, method) (( Object_t * (*) (Dictionary_2_t1950 *, int32_t, const MethodInfo*))Dictionary_2_get_Item_m12707_gshared)(__this, ___key, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::set_Item(TKey,TValue)
extern "C" void Dictionary_2_set_Item_m12709_gshared (Dictionary_2_t1950 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_set_Item_m12709(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1950 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_set_Item_m12709_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Init(System.Int32,System.Collections.Generic.IEqualityComparer`1<TKey>)
extern "C" void Dictionary_2_Init_m12711_gshared (Dictionary_2_t1950 * __this, int32_t ___capacity, Object_t* ___hcp, const MethodInfo* method);
#define Dictionary_2_Init_m12711(__this, ___capacity, ___hcp, method) (( void (*) (Dictionary_2_t1950 *, int32_t, Object_t*, const MethodInfo*))Dictionary_2_Init_m12711_gshared)(__this, ___capacity, ___hcp, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::InitArrays(System.Int32)
extern "C" void Dictionary_2_InitArrays_m12713_gshared (Dictionary_2_t1950 * __this, int32_t ___size, const MethodInfo* method);
#define Dictionary_2_InitArrays_m12713(__this, ___size, method) (( void (*) (Dictionary_2_t1950 *, int32_t, const MethodInfo*))Dictionary_2_InitArrays_m12713_gshared)(__this, ___size, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyToCheck(System.Array,System.Int32)
extern "C" void Dictionary_2_CopyToCheck_m12715_gshared (Dictionary_2_t1950 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyToCheck_m12715(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1950 *, Array_t *, int32_t, const MethodInfo*))Dictionary_2_CopyToCheck_m12715_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.KeyValuePair`2<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::make_pair(TKey,TValue)
extern "C" KeyValuePair_2_t1952  Dictionary_2_make_pair_m12717_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_make_pair_m12717(__this /* static, unused */, ___key, ___value, method) (( KeyValuePair_2_t1952  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_make_pair_m12717_gshared)(__this /* static, unused */, ___key, ___value, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::pick_value(TKey,TValue)
extern "C" Object_t * Dictionary_2_pick_value_m12719_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_pick_value_m12719(__this /* static, unused */, ___key, ___value, method) (( Object_t * (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_pick_value_m12719_gshared)(__this /* static, unused */, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::CopyTo(System.Collections.Generic.KeyValuePair`2<TKey,TValue>[],System.Int32)
extern "C" void Dictionary_2_CopyTo_m12721_gshared (Dictionary_2_t1950 * __this, KeyValuePair_2U5BU5D_t2451* ___array, int32_t ___index, const MethodInfo* method);
#define Dictionary_2_CopyTo_m12721(__this, ___array, ___index, method) (( void (*) (Dictionary_2_t1950 *, KeyValuePair_2U5BU5D_t2451*, int32_t, const MethodInfo*))Dictionary_2_CopyTo_m12721_gshared)(__this, ___array, ___index, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Resize()
extern "C" void Dictionary_2_Resize_m12723_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_Resize_m12723(__this, method) (( void (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_Resize_m12723_gshared)(__this, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Add(TKey,TValue)
extern "C" void Dictionary_2_Add_m12725_gshared (Dictionary_2_t1950 * __this, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_Add_m12725(__this, ___key, ___value, method) (( void (*) (Dictionary_2_t1950 *, int32_t, Object_t *, const MethodInfo*))Dictionary_2_Add_m12725_gshared)(__this, ___key, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Clear()
extern "C" void Dictionary_2_Clear_m12727_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_Clear_m12727(__this, method) (( void (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_Clear_m12727_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKey(TKey)
extern "C" bool Dictionary_2_ContainsKey_m12729_gshared (Dictionary_2_t1950 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_ContainsKey_m12729(__this, ___key, method) (( bool (*) (Dictionary_2_t1950 *, int32_t, const MethodInfo*))Dictionary_2_ContainsKey_m12729_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsValue(TValue)
extern "C" bool Dictionary_2_ContainsValue_m12731_gshared (Dictionary_2_t1950 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ContainsValue_m12731(__this, ___value, method) (( bool (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_ContainsValue_m12731_gshared)(__this, ___value, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetObjectData(System.Runtime.Serialization.SerializationInfo,System.Runtime.Serialization.StreamingContext)
extern "C" void Dictionary_2_GetObjectData_m12733_gshared (Dictionary_2_t1950 * __this, SerializationInfo_t652 * ___info, StreamingContext_t653  ___context, const MethodInfo* method);
#define Dictionary_2_GetObjectData_m12733(__this, ___info, ___context, method) (( void (*) (Dictionary_2_t1950 *, SerializationInfo_t652 *, StreamingContext_t653 , const MethodInfo*))Dictionary_2_GetObjectData_m12733_gshared)(__this, ___info, ___context, method)
// System.Void System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::OnDeserialization(System.Object)
extern "C" void Dictionary_2_OnDeserialization_m12735_gshared (Dictionary_2_t1950 * __this, Object_t * ___sender, const MethodInfo* method);
#define Dictionary_2_OnDeserialization_m12735(__this, ___sender, method) (( void (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_OnDeserialization_m12735_gshared)(__this, ___sender, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::Remove(TKey)
extern "C" bool Dictionary_2_Remove_m12737_gshared (Dictionary_2_t1950 * __this, int32_t ___key, const MethodInfo* method);
#define Dictionary_2_Remove_m12737(__this, ___key, method) (( bool (*) (Dictionary_2_t1950 *, int32_t, const MethodInfo*))Dictionary_2_Remove_m12737_gshared)(__this, ___key, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::TryGetValue(TKey,TValue&)
extern "C" bool Dictionary_2_TryGetValue_m12739_gshared (Dictionary_2_t1950 * __this, int32_t ___key, Object_t ** ___value, const MethodInfo* method);
#define Dictionary_2_TryGetValue_m12739(__this, ___key, ___value, method) (( bool (*) (Dictionary_2_t1950 *, int32_t, Object_t **, const MethodInfo*))Dictionary_2_TryGetValue_m12739_gshared)(__this, ___key, ___value, method)
// System.Collections.Generic.Dictionary`2/ValueCollection<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::get_Values()
extern "C" ValueCollection_t1954 * Dictionary_2_get_Values_m12740_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_get_Values_m12740(__this, method) (( ValueCollection_t1954 * (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_get_Values_m12740_gshared)(__this, method)
// TKey System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTKey(System.Object)
extern "C" int32_t Dictionary_2_ToTKey_m12742_gshared (Dictionary_2_t1950 * __this, Object_t * ___key, const MethodInfo* method);
#define Dictionary_2_ToTKey_m12742(__this, ___key, method) (( int32_t (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_ToTKey_m12742_gshared)(__this, ___key, method)
// TValue System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ToTValue(System.Object)
extern "C" Object_t * Dictionary_2_ToTValue_m12744_gshared (Dictionary_2_t1950 * __this, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_ToTValue_m12744(__this, ___value, method) (( Object_t * (*) (Dictionary_2_t1950 *, Object_t *, const MethodInfo*))Dictionary_2_ToTValue_m12744_gshared)(__this, ___value, method)
// System.Boolean System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::ContainsKeyValuePair(System.Collections.Generic.KeyValuePair`2<TKey,TValue>)
extern "C" bool Dictionary_2_ContainsKeyValuePair_m12746_gshared (Dictionary_2_t1950 * __this, KeyValuePair_2_t1952  ___pair, const MethodInfo* method);
#define Dictionary_2_ContainsKeyValuePair_m12746(__this, ___pair, method) (( bool (*) (Dictionary_2_t1950 *, KeyValuePair_2_t1952 , const MethodInfo*))Dictionary_2_ContainsKeyValuePair_m12746_gshared)(__this, ___pair, method)
// System.Collections.Generic.Dictionary`2/Enumerator<TKey,TValue> System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::GetEnumerator()
extern "C" Enumerator_t1956  Dictionary_2_GetEnumerator_m12747_gshared (Dictionary_2_t1950 * __this, const MethodInfo* method);
#define Dictionary_2_GetEnumerator_m12747(__this, method) (( Enumerator_t1956  (*) (Dictionary_2_t1950 *, const MethodInfo*))Dictionary_2_GetEnumerator_m12747_gshared)(__this, method)
// System.Collections.DictionaryEntry System.Collections.Generic.Dictionary`2<System.Int32,System.Object>::<CopyTo>m__0(TKey,TValue)
extern "C" DictionaryEntry_t1057  Dictionary_2_U3CCopyToU3Em__0_m12749_gshared (Object_t * __this /* static, unused */, int32_t ___key, Object_t * ___value, const MethodInfo* method);
#define Dictionary_2_U3CCopyToU3Em__0_m12749(__this /* static, unused */, ___key, ___value, method) (( DictionaryEntry_t1057  (*) (Object_t * /* static, unused */, int32_t, Object_t *, const MethodInfo*))Dictionary_2_U3CCopyToU3Em__0_m12749_gshared)(__this /* static, unused */, ___key, ___value, method)
