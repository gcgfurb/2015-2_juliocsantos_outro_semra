﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Canvas/WillRenderCanvases
struct WillRenderCanvases_t372;

#include "UnityEngine_UnityEngine_Behaviour.h"

// UnityEngine.Canvas
struct  Canvas_t197  : public Behaviour_t418
{
};
struct Canvas_t197_StaticFields{
	// UnityEngine.Canvas/WillRenderCanvases UnityEngine.Canvas::willRenderCanvases
	WillRenderCanvases_t372 * ___willRenderCanvases_2;
};
