﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityStandardAssets.CrossPlatformInput.ButtonHandler
struct ButtonHandler_t4;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::.ctor()
extern "C" void ButtonHandler__ctor_m6 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::OnEnable()
extern "C" void ButtonHandler_OnEnable_m7 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetDownState()
extern "C" void ButtonHandler_SetDownState_m8 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetUpState()
extern "C" void ButtonHandler_SetUpState_m9 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisPositiveState()
extern "C" void ButtonHandler_SetAxisPositiveState_m10 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNeutralState()
extern "C" void ButtonHandler_SetAxisNeutralState_m11 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::SetAxisNegativeState()
extern "C" void ButtonHandler_SetAxisNegativeState_m12 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityStandardAssets.CrossPlatformInput.ButtonHandler::Update()
extern "C" void ButtonHandler_Update_m13 (ButtonHandler_t4 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
