﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>

// UnityEngine.Rigidbody
struct Rigidbody_t34;

#include "UnityEngine_UnityEngine_MonoBehaviour.h"

// UnityStandardAssets.Vehicles.Car.CarSelfRighting
struct  CarSelfRighting_t46  : public MonoBehaviour_t2
{
	// System.Single UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_WaitTime
	float ___m_WaitTime_2;
	// System.Single UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_VelocityThreshold
	float ___m_VelocityThreshold_3;
	// System.Single UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_LastOkTime
	float ___m_LastOkTime_4;
	// UnityEngine.Rigidbody UnityStandardAssets.Vehicles.Car.CarSelfRighting::m_Rigidbody
	Rigidbody_t34 * ___m_Rigidbody_5;
};
