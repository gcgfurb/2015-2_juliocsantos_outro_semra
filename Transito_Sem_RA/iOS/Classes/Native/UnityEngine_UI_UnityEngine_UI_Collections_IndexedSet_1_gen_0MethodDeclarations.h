﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_UI_Collections_IndexedSet_1_gen_2MethodDeclarations.h"

// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::.ctor()
#define IndexedSet_1__ctor_m2147(__this, method) (( void (*) (IndexedSet_1_t388 *, const MethodInfo*))IndexedSet_1__ctor_m12980_gshared)(__this, method)
// System.Collections.IEnumerator UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::System.Collections.IEnumerable.GetEnumerator()
#define IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m13958(__this, method) (( Object_t * (*) (IndexedSet_1_t388 *, const MethodInfo*))IndexedSet_1_System_Collections_IEnumerable_GetEnumerator_m12982_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Add(T)
#define IndexedSet_1_Add_m13959(__this, ___item, method) (( void (*) (IndexedSet_1_t388 *, Graphic_t194 *, const MethodInfo*))IndexedSet_1_Add_m12984_gshared)(__this, ___item, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Remove(T)
#define IndexedSet_1_Remove_m13960(__this, ___item, method) (( bool (*) (IndexedSet_1_t388 *, Graphic_t194 *, const MethodInfo*))IndexedSet_1_Remove_m12986_gshared)(__this, ___item, method)
// System.Collections.Generic.IEnumerator`1<T> UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::GetEnumerator()
#define IndexedSet_1_GetEnumerator_m13961(__this, method) (( Object_t* (*) (IndexedSet_1_t388 *, const MethodInfo*))IndexedSet_1_GetEnumerator_m12988_gshared)(__this, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Clear()
#define IndexedSet_1_Clear_m13962(__this, method) (( void (*) (IndexedSet_1_t388 *, const MethodInfo*))IndexedSet_1_Clear_m12990_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Contains(T)
#define IndexedSet_1_Contains_m13963(__this, ___item, method) (( bool (*) (IndexedSet_1_t388 *, Graphic_t194 *, const MethodInfo*))IndexedSet_1_Contains_m12992_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::CopyTo(T[],System.Int32)
#define IndexedSet_1_CopyTo_m13964(__this, ___array, ___arrayIndex, method) (( void (*) (IndexedSet_1_t388 *, GraphicU5BU5D_t2028*, int32_t, const MethodInfo*))IndexedSet_1_CopyTo_m12994_gshared)(__this, ___array, ___arrayIndex, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Count()
#define IndexedSet_1_get_Count_m13965(__this, method) (( int32_t (*) (IndexedSet_1_t388 *, const MethodInfo*))IndexedSet_1_get_Count_m12996_gshared)(__this, method)
// System.Boolean UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_IsReadOnly()
#define IndexedSet_1_get_IsReadOnly_m13966(__this, method) (( bool (*) (IndexedSet_1_t388 *, const MethodInfo*))IndexedSet_1_get_IsReadOnly_m12998_gshared)(__this, method)
// System.Int32 UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::IndexOf(T)
#define IndexedSet_1_IndexOf_m13967(__this, ___item, method) (( int32_t (*) (IndexedSet_1_t388 *, Graphic_t194 *, const MethodInfo*))IndexedSet_1_IndexOf_m13000_gshared)(__this, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Insert(System.Int32,T)
#define IndexedSet_1_Insert_m13968(__this, ___index, ___item, method) (( void (*) (IndexedSet_1_t388 *, int32_t, Graphic_t194 *, const MethodInfo*))IndexedSet_1_Insert_m13002_gshared)(__this, ___index, ___item, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAt(System.Int32)
#define IndexedSet_1_RemoveAt_m13969(__this, ___index, method) (( void (*) (IndexedSet_1_t388 *, int32_t, const MethodInfo*))IndexedSet_1_RemoveAt_m13004_gshared)(__this, ___index, method)
// T UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::get_Item(System.Int32)
#define IndexedSet_1_get_Item_m13970(__this, ___index, method) (( Graphic_t194 * (*) (IndexedSet_1_t388 *, int32_t, const MethodInfo*))IndexedSet_1_get_Item_m13006_gshared)(__this, ___index, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::set_Item(System.Int32,T)
#define IndexedSet_1_set_Item_m13971(__this, ___index, ___value, method) (( void (*) (IndexedSet_1_t388 *, int32_t, Graphic_t194 *, const MethodInfo*))IndexedSet_1_set_Item_m13008_gshared)(__this, ___index, ___value, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::RemoveAll(System.Predicate`1<T>)
#define IndexedSet_1_RemoveAll_m13972(__this, ___match, method) (( void (*) (IndexedSet_1_t388 *, Predicate_1_t2030 *, const MethodInfo*))IndexedSet_1_RemoveAll_m13010_gshared)(__this, ___match, method)
// System.Void UnityEngine.UI.Collections.IndexedSet`1<UnityEngine.UI.Graphic>::Sort(System.Comparison`1<T>)
#define IndexedSet_1_Sort_m13973(__this, ___sortLayoutFunction, method) (( void (*) (IndexedSet_1_t388 *, Comparison_1_t205 *, const MethodInfo*))IndexedSet_1_Sort_m13011_gshared)(__this, ___sortLayoutFunction, method)
