﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_0MethodDeclarations.h"

// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Vehicles.Car.WheelEffects>::.ctor(System.Array)
#define InternalEnumerator_1__ctor_m11486(__this, ___array, method) (( void (*) (InternalEnumerator_1_t1859 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m10902_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Vehicles.Car.WheelEffects>::System.Collections.IEnumerator.Reset()
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m11487(__this, method) (( void (*) (InternalEnumerator_1_t1859 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m10904_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<UnityStandardAssets.Vehicles.Car.WheelEffects>::System.Collections.IEnumerator.get_Current()
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m11488(__this, method) (( Object_t * (*) (InternalEnumerator_1_t1859 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m10906_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<UnityStandardAssets.Vehicles.Car.WheelEffects>::Dispose()
#define InternalEnumerator_1_Dispose_m11489(__this, method) (( void (*) (InternalEnumerator_1_t1859 *, const MethodInfo*))InternalEnumerator_1_Dispose_m10908_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<UnityStandardAssets.Vehicles.Car.WheelEffects>::MoveNext()
#define InternalEnumerator_1_MoveNext_m11490(__this, method) (( bool (*) (InternalEnumerator_1_t1859 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m10910_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<UnityStandardAssets.Vehicles.Car.WheelEffects>::get_Current()
#define InternalEnumerator_1_get_Current_m11491(__this, method) (( WheelEffects_t54 * (*) (InternalEnumerator_1_t1859 *, const MethodInfo*))InternalEnumerator_1_get_Current_m10912_gshared)(__this, method)
