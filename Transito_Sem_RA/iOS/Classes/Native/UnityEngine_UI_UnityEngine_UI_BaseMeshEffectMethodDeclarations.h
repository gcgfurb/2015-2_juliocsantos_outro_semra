﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// UnityEngine.UI.BaseMeshEffect
struct BaseMeshEffect_t319;
// UnityEngine.UI.Graphic
struct Graphic_t194;
// UnityEngine.Mesh
struct Mesh_t200;

#include "codegen/il2cpp-codegen.h"

// System.Void UnityEngine.UI.BaseMeshEffect::.ctor()
extern "C" void BaseMeshEffect__ctor_m1838 (BaseMeshEffect_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// UnityEngine.UI.Graphic UnityEngine.UI.BaseMeshEffect::get_graphic()
extern "C" Graphic_t194 * BaseMeshEffect_get_graphic_m1839 (BaseMeshEffect_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::OnEnable()
extern "C" void BaseMeshEffect_OnEnable_m1840 (BaseMeshEffect_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::OnDisable()
extern "C" void BaseMeshEffect_OnDisable_m1841 (BaseMeshEffect_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::OnDidApplyAnimationProperties()
extern "C" void BaseMeshEffect_OnDidApplyAnimationProperties_m1842 (BaseMeshEffect_t319 * __this, const MethodInfo* method) IL2CPP_METHOD_ATTR;
// System.Void UnityEngine.UI.BaseMeshEffect::ModifyMesh(UnityEngine.Mesh)
extern "C" void BaseMeshEffect_ModifyMesh_m1843 (BaseMeshEffect_t319 * __this, Mesh_t200 * ___mesh, const MethodInfo* method) IL2CPP_METHOD_ATTR;
