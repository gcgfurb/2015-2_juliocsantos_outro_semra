﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>


#include "codegen/il2cpp-codegen.h"
#include "System_System_Collections_Generic_Stack_1_gen_0MethodDeclarations.h"

// System.Void System.Collections.Generic.Stack`1<System.Type>::.ctor()
#define Stack_1__ctor_m3628(__this, method) (( void (*) (Stack_1_t670 *, const MethodInfo*))Stack_1__ctor_m11915_gshared)(__this, method)
// System.Boolean System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_IsSynchronized()
#define Stack_1_System_Collections_ICollection_get_IsSynchronized_m16823(__this, method) (( bool (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_IsSynchronized_m11916_gshared)(__this, method)
// System.Object System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.get_SyncRoot()
#define Stack_1_System_Collections_ICollection_get_SyncRoot_m16824(__this, method) (( Object_t * (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_System_Collections_ICollection_get_SyncRoot_m11917_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
#define Stack_1_System_Collections_ICollection_CopyTo_m16825(__this, ___dest, ___idx, method) (( void (*) (Stack_1_t670 *, Array_t *, int32_t, const MethodInfo*))Stack_1_System_Collections_ICollection_CopyTo_m11918_gshared)(__this, ___dest, ___idx, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.Generic.Stack`1<System.Type>::System.Collections.Generic.IEnumerable<T>.GetEnumerator()
#define Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m16826(__this, method) (( Object_t* (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_System_Collections_Generic_IEnumerableU3CTU3E_GetEnumerator_m11919_gshared)(__this, method)
// System.Collections.IEnumerator System.Collections.Generic.Stack`1<System.Type>::System.Collections.IEnumerable.GetEnumerator()
#define Stack_1_System_Collections_IEnumerable_GetEnumerator_m16827(__this, method) (( Object_t * (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_System_Collections_IEnumerable_GetEnumerator_m11920_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Peek()
#define Stack_1_Peek_m16828(__this, method) (( Type_t * (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_Peek_m11921_gshared)(__this, method)
// T System.Collections.Generic.Stack`1<System.Type>::Pop()
#define Stack_1_Pop_m3630(__this, method) (( Type_t * (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_Pop_m11922_gshared)(__this, method)
// System.Void System.Collections.Generic.Stack`1<System.Type>::Push(T)
#define Stack_1_Push_m3629(__this, ___t, method) (( void (*) (Stack_1_t670 *, Type_t *, const MethodInfo*))Stack_1_Push_m11923_gshared)(__this, ___t, method)
// System.Int32 System.Collections.Generic.Stack`1<System.Type>::get_Count()
#define Stack_1_get_Count_m16829(__this, method) (( int32_t (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_get_Count_m11924_gshared)(__this, method)
// System.Collections.Generic.Stack`1/Enumerator<T> System.Collections.Generic.Stack`1<System.Type>::GetEnumerator()
#define Stack_1_GetEnumerator_m16830(__this, method) (( Enumerator_t2255  (*) (Stack_1_t670 *, const MethodInfo*))Stack_1_GetEnumerator_m11925_gshared)(__this, method)
