﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>
struct Collection_1_t1917;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// UnityEngine.EventSystems.RaycastResult[]
struct RaycastResultU5BU5D_t1912;
// System.Collections.Generic.IEnumerator`1<UnityEngine.EventSystems.RaycastResult>
struct IEnumerator_1_t2447;
// System.Collections.Generic.IList`1<UnityEngine.EventSystems.RaycastResult>
struct IList_1_t1916;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UI_UnityEngine_EventSystems_RaycastResult.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::.ctor()
extern "C" void Collection_1__ctor_m12174_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1__ctor_m12174(__this, method) (( void (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1__ctor_m12174_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12175_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12175(__this, method) (( bool (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m12175_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m12176_gshared (Collection_1_t1917 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m12176(__this, ___array, ___index, method) (( void (*) (Collection_1_t1917 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m12176_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m12177_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m12177(__this, method) (( Object_t * (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m12177_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m12178_gshared (Collection_1_t1917 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m12178(__this, ___value, method) (( int32_t (*) (Collection_1_t1917 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m12178_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m12179_gshared (Collection_1_t1917 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m12179(__this, ___value, method) (( bool (*) (Collection_1_t1917 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m12179_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m12180_gshared (Collection_1_t1917 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m12180(__this, ___value, method) (( int32_t (*) (Collection_1_t1917 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m12180_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m12181_gshared (Collection_1_t1917 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m12181(__this, ___index, ___value, method) (( void (*) (Collection_1_t1917 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m12181_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m12182_gshared (Collection_1_t1917 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m12182(__this, ___value, method) (( void (*) (Collection_1_t1917 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m12182_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m12183_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m12183(__this, method) (( bool (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m12183_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m12184_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m12184(__this, method) (( Object_t * (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m12184_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m12185_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m12185(__this, method) (( bool (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m12185_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m12186_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m12186(__this, method) (( bool (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m12186_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m12187_gshared (Collection_1_t1917 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m12187(__this, ___index, method) (( Object_t * (*) (Collection_1_t1917 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m12187_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m12188_gshared (Collection_1_t1917 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m12188(__this, ___index, ___value, method) (( void (*) (Collection_1_t1917 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m12188_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Add(T)
extern "C" void Collection_1_Add_m12189_gshared (Collection_1_t1917 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_Add_m12189(__this, ___item, method) (( void (*) (Collection_1_t1917 *, RaycastResult_t139 , const MethodInfo*))Collection_1_Add_m12189_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Clear()
extern "C" void Collection_1_Clear_m12190_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_Clear_m12190(__this, method) (( void (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_Clear_m12190_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ClearItems()
extern "C" void Collection_1_ClearItems_m12191_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m12191(__this, method) (( void (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_ClearItems_m12191_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Contains(T)
extern "C" bool Collection_1_Contains_m12192_gshared (Collection_1_t1917 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_Contains_m12192(__this, ___item, method) (( bool (*) (Collection_1_t1917 *, RaycastResult_t139 , const MethodInfo*))Collection_1_Contains_m12192_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m12193_gshared (Collection_1_t1917 * __this, RaycastResultU5BU5D_t1912* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m12193(__this, ___array, ___index, method) (( void (*) (Collection_1_t1917 *, RaycastResultU5BU5D_t1912*, int32_t, const MethodInfo*))Collection_1_CopyTo_m12193_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m12194_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m12194(__this, method) (( Object_t* (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_GetEnumerator_m12194_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m12195_gshared (Collection_1_t1917 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m12195(__this, ___item, method) (( int32_t (*) (Collection_1_t1917 *, RaycastResult_t139 , const MethodInfo*))Collection_1_IndexOf_m12195_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m12196_gshared (Collection_1_t1917 * __this, int32_t ___index, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_Insert_m12196(__this, ___index, ___item, method) (( void (*) (Collection_1_t1917 *, int32_t, RaycastResult_t139 , const MethodInfo*))Collection_1_Insert_m12196_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m12197_gshared (Collection_1_t1917 * __this, int32_t ___index, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m12197(__this, ___index, ___item, method) (( void (*) (Collection_1_t1917 *, int32_t, RaycastResult_t139 , const MethodInfo*))Collection_1_InsertItem_m12197_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::Remove(T)
extern "C" bool Collection_1_Remove_m12198_gshared (Collection_1_t1917 * __this, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_Remove_m12198(__this, ___item, method) (( bool (*) (Collection_1_t1917 *, RaycastResult_t139 , const MethodInfo*))Collection_1_Remove_m12198_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m12199_gshared (Collection_1_t1917 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m12199(__this, ___index, method) (( void (*) (Collection_1_t1917 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m12199_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m12200_gshared (Collection_1_t1917 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m12200(__this, ___index, method) (( void (*) (Collection_1_t1917 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m12200_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Count()
extern "C" int32_t Collection_1_get_Count_m12201_gshared (Collection_1_t1917 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m12201(__this, method) (( int32_t (*) (Collection_1_t1917 *, const MethodInfo*))Collection_1_get_Count_m12201_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::get_Item(System.Int32)
extern "C" RaycastResult_t139  Collection_1_get_Item_m12202_gshared (Collection_1_t1917 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m12202(__this, ___index, method) (( RaycastResult_t139  (*) (Collection_1_t1917 *, int32_t, const MethodInfo*))Collection_1_get_Item_m12202_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m12203_gshared (Collection_1_t1917 * __this, int32_t ___index, RaycastResult_t139  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m12203(__this, ___index, ___value, method) (( void (*) (Collection_1_t1917 *, int32_t, RaycastResult_t139 , const MethodInfo*))Collection_1_set_Item_m12203_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m12204_gshared (Collection_1_t1917 * __this, int32_t ___index, RaycastResult_t139  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m12204(__this, ___index, ___item, method) (( void (*) (Collection_1_t1917 *, int32_t, RaycastResult_t139 , const MethodInfo*))Collection_1_SetItem_m12204_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m12205_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m12205(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m12205_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::ConvertItem(System.Object)
extern "C" RaycastResult_t139  Collection_1_ConvertItem_m12206_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m12206(__this /* static, unused */, ___item, method) (( RaycastResult_t139  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m12206_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m12207_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m12207(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m12207_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m12208_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m12208(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m12208_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.EventSystems.RaycastResult>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m12209_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m12209(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m12209_gshared)(__this /* static, unused */, ___list, method)
