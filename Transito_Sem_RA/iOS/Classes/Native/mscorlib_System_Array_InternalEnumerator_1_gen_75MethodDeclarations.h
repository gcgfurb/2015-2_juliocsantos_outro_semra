﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Array
struct Array_t;
// System.Object
struct Object_t;

#include "codegen/il2cpp-codegen.h"
#include "mscorlib_System_Array_InternalEnumerator_1_gen_75.h"

// System.Void System.Array/InternalEnumerator`1<System.Int16>::.ctor(System.Array)
extern "C" void InternalEnumerator_1__ctor_m17677_gshared (InternalEnumerator_1_t2340 * __this, Array_t * ___array, const MethodInfo* method);
#define InternalEnumerator_1__ctor_m17677(__this, ___array, method) (( void (*) (InternalEnumerator_1_t2340 *, Array_t *, const MethodInfo*))InternalEnumerator_1__ctor_m17677_gshared)(__this, ___array, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.Reset()
extern "C" void InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17678_gshared (InternalEnumerator_1_t2340 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17678(__this, method) (( void (*) (InternalEnumerator_1_t2340 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_Reset_m17678_gshared)(__this, method)
// System.Object System.Array/InternalEnumerator`1<System.Int16>::System.Collections.IEnumerator.get_Current()
extern "C" Object_t * InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17679_gshared (InternalEnumerator_1_t2340 * __this, const MethodInfo* method);
#define InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17679(__this, method) (( Object_t * (*) (InternalEnumerator_1_t2340 *, const MethodInfo*))InternalEnumerator_1_System_Collections_IEnumerator_get_Current_m17679_gshared)(__this, method)
// System.Void System.Array/InternalEnumerator`1<System.Int16>::Dispose()
extern "C" void InternalEnumerator_1_Dispose_m17680_gshared (InternalEnumerator_1_t2340 * __this, const MethodInfo* method);
#define InternalEnumerator_1_Dispose_m17680(__this, method) (( void (*) (InternalEnumerator_1_t2340 *, const MethodInfo*))InternalEnumerator_1_Dispose_m17680_gshared)(__this, method)
// System.Boolean System.Array/InternalEnumerator`1<System.Int16>::MoveNext()
extern "C" bool InternalEnumerator_1_MoveNext_m17681_gshared (InternalEnumerator_1_t2340 * __this, const MethodInfo* method);
#define InternalEnumerator_1_MoveNext_m17681(__this, method) (( bool (*) (InternalEnumerator_1_t2340 *, const MethodInfo*))InternalEnumerator_1_MoveNext_m17681_gshared)(__this, method)
// T System.Array/InternalEnumerator`1<System.Int16>::get_Current()
extern "C" int16_t InternalEnumerator_1_get_Current_m17682_gshared (InternalEnumerator_1_t2340 * __this, const MethodInfo* method);
#define InternalEnumerator_1_get_Current_m17682(__this, method) (( int16_t (*) (InternalEnumerator_1_t2340 *, const MethodInfo*))InternalEnumerator_1_get_Current_m17682_gshared)(__this, method)
