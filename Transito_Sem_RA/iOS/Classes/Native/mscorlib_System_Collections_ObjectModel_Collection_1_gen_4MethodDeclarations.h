﻿#pragma once

#include "il2cpp-config.h"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif

#include <stdint.h>
#include <assert.h>
#include <exception>

// System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>
struct Collection_1_t2131;
// System.Array
struct Array_t;
// System.Collections.IEnumerator
struct IEnumerator_t59;
// System.Object
struct Object_t;
// UnityEngine.Color32[]
struct Color32U5BU5D_t424;
// System.Collections.Generic.IEnumerator`1<UnityEngine.Color32>
struct IEnumerator_1_t2475;
// System.Collections.Generic.IList`1<UnityEngine.Color32>
struct IList_1_t2130;

#include "codegen/il2cpp-codegen.h"
#include "UnityEngine_UnityEngine_Color32.h"

// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::.ctor()
extern "C" void Collection_1__ctor_m15346_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1__ctor_m15346(__this, method) (( void (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1__ctor_m15346_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.Generic.ICollection<T>.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15347_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15347(__this, method) (( bool (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_System_Collections_Generic_ICollectionU3CTU3E_get_IsReadOnly_m15347_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.CopyTo(System.Array,System.Int32)
extern "C" void Collection_1_System_Collections_ICollection_CopyTo_m15348_gshared (Collection_1_t2131 * __this, Array_t * ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_CopyTo_m15348(__this, ___array, ___index, method) (( void (*) (Collection_1_t2131 *, Array_t *, int32_t, const MethodInfo*))Collection_1_System_Collections_ICollection_CopyTo_m15348_gshared)(__this, ___array, ___index, method)
// System.Collections.IEnumerator System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IEnumerable.GetEnumerator()
extern "C" Object_t * Collection_1_System_Collections_IEnumerable_GetEnumerator_m15349_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IEnumerable_GetEnumerator_m15349(__this, method) (( Object_t * (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_System_Collections_IEnumerable_GetEnumerator_m15349_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Add(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_Add_m15350_gshared (Collection_1_t2131 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Add_m15350(__this, ___value, method) (( int32_t (*) (Collection_1_t2131 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Add_m15350_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Contains(System.Object)
extern "C" bool Collection_1_System_Collections_IList_Contains_m15351_gshared (Collection_1_t2131 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Contains_m15351(__this, ___value, method) (( bool (*) (Collection_1_t2131 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Contains_m15351_gshared)(__this, ___value, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.IndexOf(System.Object)
extern "C" int32_t Collection_1_System_Collections_IList_IndexOf_m15352_gshared (Collection_1_t2131 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_IndexOf_m15352(__this, ___value, method) (( int32_t (*) (Collection_1_t2131 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_IndexOf_m15352_gshared)(__this, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Insert(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_Insert_m15353_gshared (Collection_1_t2131 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Insert_m15353(__this, ___index, ___value, method) (( void (*) (Collection_1_t2131 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Insert_m15353_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.Remove(System.Object)
extern "C" void Collection_1_System_Collections_IList_Remove_m15354_gshared (Collection_1_t2131 * __this, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_Remove_m15354(__this, ___value, method) (( void (*) (Collection_1_t2131 *, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_Remove_m15354_gshared)(__this, ___value, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_IsSynchronized()
extern "C" bool Collection_1_System_Collections_ICollection_get_IsSynchronized_m15355_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_IsSynchronized_m15355(__this, method) (( bool (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_IsSynchronized_m15355_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.ICollection.get_SyncRoot()
extern "C" Object_t * Collection_1_System_Collections_ICollection_get_SyncRoot_m15356_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_ICollection_get_SyncRoot_m15356(__this, method) (( Object_t * (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_System_Collections_ICollection_get_SyncRoot_m15356_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsFixedSize()
extern "C" bool Collection_1_System_Collections_IList_get_IsFixedSize_m15357_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsFixedSize_m15357(__this, method) (( bool (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsFixedSize_m15357_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_IsReadOnly()
extern "C" bool Collection_1_System_Collections_IList_get_IsReadOnly_m15358_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_IsReadOnly_m15358(__this, method) (( bool (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_System_Collections_IList_get_IsReadOnly_m15358_gshared)(__this, method)
// System.Object System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.get_Item(System.Int32)
extern "C" Object_t * Collection_1_System_Collections_IList_get_Item_m15359_gshared (Collection_1_t2131 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_System_Collections_IList_get_Item_m15359(__this, ___index, method) (( Object_t * (*) (Collection_1_t2131 *, int32_t, const MethodInfo*))Collection_1_System_Collections_IList_get_Item_m15359_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::System.Collections.IList.set_Item(System.Int32,System.Object)
extern "C" void Collection_1_System_Collections_IList_set_Item_m15360_gshared (Collection_1_t2131 * __this, int32_t ___index, Object_t * ___value, const MethodInfo* method);
#define Collection_1_System_Collections_IList_set_Item_m15360(__this, ___index, ___value, method) (( void (*) (Collection_1_t2131 *, int32_t, Object_t *, const MethodInfo*))Collection_1_System_Collections_IList_set_Item_m15360_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Add(T)
extern "C" void Collection_1_Add_m15361_gshared (Collection_1_t2131 * __this, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_Add_m15361(__this, ___item, method) (( void (*) (Collection_1_t2131 *, Color32_t347 , const MethodInfo*))Collection_1_Add_m15361_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Clear()
extern "C" void Collection_1_Clear_m15362_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_Clear_m15362(__this, method) (( void (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_Clear_m15362_gshared)(__this, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ClearItems()
extern "C" void Collection_1_ClearItems_m15363_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_ClearItems_m15363(__this, method) (( void (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_ClearItems_m15363_gshared)(__this, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Contains(T)
extern "C" bool Collection_1_Contains_m15364_gshared (Collection_1_t2131 * __this, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_Contains_m15364(__this, ___item, method) (( bool (*) (Collection_1_t2131 *, Color32_t347 , const MethodInfo*))Collection_1_Contains_m15364_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CopyTo(T[],System.Int32)
extern "C" void Collection_1_CopyTo_m15365_gshared (Collection_1_t2131 * __this, Color32U5BU5D_t424* ___array, int32_t ___index, const MethodInfo* method);
#define Collection_1_CopyTo_m15365(__this, ___array, ___index, method) (( void (*) (Collection_1_t2131 *, Color32U5BU5D_t424*, int32_t, const MethodInfo*))Collection_1_CopyTo_m15365_gshared)(__this, ___array, ___index, method)
// System.Collections.Generic.IEnumerator`1<T> System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::GetEnumerator()
extern "C" Object_t* Collection_1_GetEnumerator_m15366_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_GetEnumerator_m15366(__this, method) (( Object_t* (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_GetEnumerator_m15366_gshared)(__this, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IndexOf(T)
extern "C" int32_t Collection_1_IndexOf_m15367_gshared (Collection_1_t2131 * __this, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_IndexOf_m15367(__this, ___item, method) (( int32_t (*) (Collection_1_t2131 *, Color32_t347 , const MethodInfo*))Collection_1_IndexOf_m15367_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Insert(System.Int32,T)
extern "C" void Collection_1_Insert_m15368_gshared (Collection_1_t2131 * __this, int32_t ___index, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_Insert_m15368(__this, ___index, ___item, method) (( void (*) (Collection_1_t2131 *, int32_t, Color32_t347 , const MethodInfo*))Collection_1_Insert_m15368_gshared)(__this, ___index, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::InsertItem(System.Int32,T)
extern "C" void Collection_1_InsertItem_m15369_gshared (Collection_1_t2131 * __this, int32_t ___index, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_InsertItem_m15369(__this, ___index, ___item, method) (( void (*) (Collection_1_t2131 *, int32_t, Color32_t347 , const MethodInfo*))Collection_1_InsertItem_m15369_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::Remove(T)
extern "C" bool Collection_1_Remove_m15370_gshared (Collection_1_t2131 * __this, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_Remove_m15370(__this, ___item, method) (( bool (*) (Collection_1_t2131 *, Color32_t347 , const MethodInfo*))Collection_1_Remove_m15370_gshared)(__this, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveAt(System.Int32)
extern "C" void Collection_1_RemoveAt_m15371_gshared (Collection_1_t2131 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveAt_m15371(__this, ___index, method) (( void (*) (Collection_1_t2131 *, int32_t, const MethodInfo*))Collection_1_RemoveAt_m15371_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::RemoveItem(System.Int32)
extern "C" void Collection_1_RemoveItem_m15372_gshared (Collection_1_t2131 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_RemoveItem_m15372(__this, ___index, method) (( void (*) (Collection_1_t2131 *, int32_t, const MethodInfo*))Collection_1_RemoveItem_m15372_gshared)(__this, ___index, method)
// System.Int32 System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Count()
extern "C" int32_t Collection_1_get_Count_m15373_gshared (Collection_1_t2131 * __this, const MethodInfo* method);
#define Collection_1_get_Count_m15373(__this, method) (( int32_t (*) (Collection_1_t2131 *, const MethodInfo*))Collection_1_get_Count_m15373_gshared)(__this, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::get_Item(System.Int32)
extern "C" Color32_t347  Collection_1_get_Item_m15374_gshared (Collection_1_t2131 * __this, int32_t ___index, const MethodInfo* method);
#define Collection_1_get_Item_m15374(__this, ___index, method) (( Color32_t347  (*) (Collection_1_t2131 *, int32_t, const MethodInfo*))Collection_1_get_Item_m15374_gshared)(__this, ___index, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::set_Item(System.Int32,T)
extern "C" void Collection_1_set_Item_m15375_gshared (Collection_1_t2131 * __this, int32_t ___index, Color32_t347  ___value, const MethodInfo* method);
#define Collection_1_set_Item_m15375(__this, ___index, ___value, method) (( void (*) (Collection_1_t2131 *, int32_t, Color32_t347 , const MethodInfo*))Collection_1_set_Item_m15375_gshared)(__this, ___index, ___value, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::SetItem(System.Int32,T)
extern "C" void Collection_1_SetItem_m15376_gshared (Collection_1_t2131 * __this, int32_t ___index, Color32_t347  ___item, const MethodInfo* method);
#define Collection_1_SetItem_m15376(__this, ___index, ___item, method) (( void (*) (Collection_1_t2131 *, int32_t, Color32_t347 , const MethodInfo*))Collection_1_SetItem_m15376_gshared)(__this, ___index, ___item, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsValidItem(System.Object)
extern "C" bool Collection_1_IsValidItem_m15377_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_IsValidItem_m15377(__this /* static, unused */, ___item, method) (( bool (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_IsValidItem_m15377_gshared)(__this /* static, unused */, ___item, method)
// T System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::ConvertItem(System.Object)
extern "C" Color32_t347  Collection_1_ConvertItem_m15378_gshared (Object_t * __this /* static, unused */, Object_t * ___item, const MethodInfo* method);
#define Collection_1_ConvertItem_m15378(__this /* static, unused */, ___item, method) (( Color32_t347  (*) (Object_t * /* static, unused */, Object_t *, const MethodInfo*))Collection_1_ConvertItem_m15378_gshared)(__this /* static, unused */, ___item, method)
// System.Void System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::CheckWritable(System.Collections.Generic.IList`1<T>)
extern "C" void Collection_1_CheckWritable_m15379_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_CheckWritable_m15379(__this /* static, unused */, ___list, method) (( void (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_CheckWritable_m15379_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsSynchronized(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsSynchronized_m15380_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsSynchronized_m15380(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsSynchronized_m15380_gshared)(__this /* static, unused */, ___list, method)
// System.Boolean System.Collections.ObjectModel.Collection`1<UnityEngine.Color32>::IsFixedSize(System.Collections.Generic.IList`1<T>)
extern "C" bool Collection_1_IsFixedSize_m15381_gshared (Object_t * __this /* static, unused */, Object_t* ___list, const MethodInfo* method);
#define Collection_1_IsFixedSize_m15381(__this /* static, unused */, ___list, method) (( bool (*) (Object_t * /* static, unused */, Object_t*, const MethodInfo*))Collection_1_IsFixedSize_m15381_gshared)(__this /* static, unused */, ___list, method)
