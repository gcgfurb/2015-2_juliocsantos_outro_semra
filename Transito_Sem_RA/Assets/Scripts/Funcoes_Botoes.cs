﻿using UnityEngine;
using System.Collections;

public class Funcoes_Botoes : MonoBehaviour {
		
	public RectTransform m_MenuPausa;

	// Use this for initialization
	void Start () {
		if (m_MenuPausa != null)
			m_MenuPausa.localScale = new Vector3 (0f,0f,0f);
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public void Reiniciar(){
        Application.LoadLevel("Cena_01");
	}

	public void Sair () {
		Application.Quit ();
	}

	public void VoltarInicio(){
		
	}

	public void Pausar(){
		if (Time.timeScale != 0) {
			Time.timeScale = 0;
			m_MenuPausa.localScale = new Vector3 (1f, 1f, 1f);
		} else {
			Time.timeScale = 1;
			m_MenuPausa.localScale = new Vector3 (0f, 0f, 0f);
		}
	}
}
