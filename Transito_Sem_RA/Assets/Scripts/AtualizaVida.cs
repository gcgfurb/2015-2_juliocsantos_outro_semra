﻿using UnityEngine;
using System.Collections;
using UnityStandardAssets.Vehicles.Car;

public class AtualizaVida : MonoBehaviour {

	public CarController m_Carro;
	public Car_Health m_CarHealth;
	public float m_KmLitros = 7;
	public float multiplicadorKML = 0.04f;

	// Use this for initialization
	void Start () {
		m_KmLitros = (m_KmLitros/multiplicadorKML);

	}
	
	// Update is called once per frame
	void Update () {

		if (m_Carro.CurrentSpeed > 0 && (m_Carro.AccelInput != 0 || m_Carro.BrakeInput > 0)) {
			m_CarHealth.GastaCombustivel (m_Carro.CurrentSpeed / m_KmLitros);
		}
	}

	void  OnCollisionEnter (Collision ObjetoColidido){

		m_CarHealth.GastaMecanica (m_Carro.CurrentSpeed*0.35f);

	}

	void OnTriggerStay (Collider ObjetoColidido){
	
		switch (ObjetoColidido.gameObject.tag) {
		case "Plataforma_Mecanica":
			m_CarHealth.AddMecanica();
			break;
		case "Plataforma_Combustivel":
			m_CarHealth.AddCombustivel();
			break;
		}

	}
}
