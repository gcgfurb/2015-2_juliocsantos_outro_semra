﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Car_Health : MonoBehaviour {

	/*Accessadas pelo editor do unity*/
	public float m_CombutivelMax = 50f;
	public float m_MecanicaMax = 50f;               
	public Slider m_BarraCombustivel;
	public Slider m_BarraMecanica;

	/*Os valores desses atributos */
	private float m_CombustivelAtual;
	private float m_MecanicaAtual;
	
	private bool m_Morto;
	private bool m_CombustivelBaixo;
	private bool m_MecanicaBaixa;
	// identifica se esta abastecendo o veiculo
	private bool m_AguardarCombustivel;
	// identifica se esta consertando o veiculo
	private bool m_AguardarMecanica;

	public float GetCombustivelAtual(){
		return m_CombustivelAtual;
	}
	public float GetMecanicaAtual(){
		return m_MecanicaAtual;
	}
	public bool IsMorto(){
		return m_Morto;
	}
	public bool IsCombustivelBaixo(){
		return m_CombustivelBaixo;
	}
	public bool IsMecanicaBaixa(){
		return m_MecanicaBaixa;
	}

	// Use this for initialization
	void Start () {
	
	}

	private void OnEnable() {

		m_CombustivelAtual = m_CombutivelMax;
		m_MecanicaAtual = m_MecanicaMax;
		m_Morto = false;

		SetHealthUI ();
	}

	// Update is called once per frame
	void Update () {

		/* acredito que seria melhor remover 
		 * e chamar toda vez que modificar 
		 * uma das barras
		 * */
		SetHealthUI ();
	}

	public void GastaMecanica (float quantidade) {

		if (m_MecanicaAtual < 11f) {

			if (!m_MecanicaBaixa)
				m_MecanicaBaixa = true;

			if (m_MecanicaAtual == 0f && !m_Morto)
				m_Morto = true;
		}

		m_MecanicaAtual -= quantidade;

	}

	public void AddMecanica() {

		if (m_MecanicaAtual < m_MecanicaMax && !this.m_AguardarMecanica) {
				StartCoroutine (Arrumar());
		}
	}

	private IEnumerator Arrumar ()	{
		this.m_MecanicaAtual += 0.25f;
		this.m_AguardarMecanica = true;
		yield return new WaitForSeconds (0.1f);
		this.m_AguardarMecanica = false;
	}

	public void GastaCombustivel(float quantidade) {

		if (m_CombustivelAtual < 11f) {

			if (!m_CombustivelBaixo)
				m_CombustivelBaixo = true;

			if (m_CombustivelAtual == 0f && !m_Morto)
				m_Morto = true;
		}

		m_CombustivelAtual -= quantidade;
	}

	public void AddCombustivel(){

		if (m_CombustivelAtual < m_CombutivelMax && !this.m_AguardarCombustivel) {
			StartCoroutine (Abastecer());
		}
	}

	private IEnumerator Abastecer ()	{
		this.m_CombustivelAtual += 0.25f;
		this.m_AguardarCombustivel = true;
		yield return new WaitForSeconds (0.1f);
		this.m_AguardarCombustivel = false;
	}

	/**
	 * Atualiza os valores das barras  
	 * de Combustivel e mecanica
	 */
	private void SetHealthUI () {

		this.m_BarraCombustivel.value = m_CombustivelAtual;
		this.m_BarraMecanica.value = m_MecanicaAtual;
	}

	private void OnDeath () {
		//Debug.Log("Carro morreu");
	}
}
