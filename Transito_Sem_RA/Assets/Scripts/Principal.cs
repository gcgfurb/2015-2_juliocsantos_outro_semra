﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Principal : MonoBehaviour {

	public Color m_CorMesagemPadraso;
	public Color m_CorMesagemAlerta;
	public Color m_CorMesagemPerigo;
	public Text m_MesagemTexto;
	public AtualizaVida m_Vidas;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		CombustivelBaixo ();
		MecanicaBaixa ();

	}

	private void CombustivelBaixo(){
	
		if (m_Vidas.m_CarHealth.IsCombustivelBaixo()) {
			ExibirMesagemAlerta("Combutivel\nAcabando");
		}
	}

	private void MecanicaBaixa(){
		
		if (m_Vidas.m_CarHealth.IsMecanicaBaixa()) {
			ExibirMesagemAlerta("Veiculo Muito\nDanificado");
		}
	}

	private void ExibirMesagemPadrao(string mensagem) {
	
	}

	private void ExibirMesagemAlerta(string mensagem) {
		m_MesagemTexto.color = m_CorMesagemAlerta;
		m_MesagemTexto.text = mensagem;
	}

	private void ExibirMesagemPerigo(string mensagem) {
		
	}

}
